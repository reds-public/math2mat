function [var,size,nbsamples]=m2m_readvarfromfile(filename)
% Big endian is required by SystemVerilog
f=fopen(filename,"r","ieee-be");
nbsamples=fread(f,1,"int32");
size=fread(f,1,"int32");
var=zeros(nbsamples,size);
for i=1:nbsamples
	[v,count]=fread(f,size,"float32");
	var(i,:)=v;
end
fclose(f);
endfunction;
