
clear inputs;
clear outputs;
clear outputs_regen;

global interns;
global inputs;
m2m_parameters;

printf("\nCreation of input verification files:\n");
printf("-------------------------------------\n");

if generate_input
	m2m_generatesamples(filedir,nbsamples);
else
	for i=1:length(inputs)
		fname=sprintf("%sfile_%s.dat",filedir,inputs(i).name);
		[inputs(i).values,inputs(i).size,nbsamples]=m2m_readvarfromfile(fname);		
	endfor
endif

printf("\nGeneration of outputs...\n");
for i=1:nbsamples
	m2m_computesample;
endfor

printf("\nCreation of output verification files:\n");
printf("--------------------------------------\n");

for i=1:length(outputs)
	fname=sprintf("%sfile_%s.dat",filedir,outputs(i).name);
	m2m_writevartofile(fname,outputs(i).values);
	printf("file_%s.dat was written\n", outputs(i).name);
endfor
