
function m2m_writevartofile(filename,var)
% Big endian is required by SystemVerilog
f=fopen(filename,"w","ieee-be");
fwrite(f,length(var(:,1)),"int32");
fwrite(f,length(var(1,:)),"int32");
for i=1:length(var(:,1))
	for j=1:length(var(1,:))
		fwrite(f,var(i,j),"float64");
	end
end
fclose(f);
endfunction;
