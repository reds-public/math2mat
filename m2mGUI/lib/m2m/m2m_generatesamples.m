
function gen_input=m2m_generatesamples(filedir,nb)
global inputs;
for i=1:length(inputs)
	fname=sprintf("%sfile_%s.dat",filedir,inputs(i).name);
	%inputs(i).values=(-99*rand(nb,inputs(i).size))+100;
	inputs(i).values=(abs(inputs(i).max-inputs(i).min)*rand(nb,inputs(i).size))+inputs(i).min;
	m2m_writevartofile(fname,inputs(i).values);
	printf("file_%s.dat was written\n", inputs(i).name);
end
endfunction;

