// Objection class to manage end-of-test stop, and other
// objections if required.

// THIS VERSION OF THE FILE HAS HAD ALL DEBUG FUNCTIONALITY
// REMOVED TO MAKE IT EASIER TO READ.  The public interface
// and core functionality are unaltered.  Function set_debug()
// is still present, to preserve the interface, but it has
// no effect.

`ifndef OBJECTION__SV
`define OBJECTION__SV


`include "bases.sv"  // Needed for definition of Component class

class Objection;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  //                      PUBLIC INTERFACE                         //
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

  // This constant defines the name of the default objection,
  // which controls the completion of a test.  Not only is this
  // the default objection kind when creating an objection client,
  // it also has the special automatic property that it causes
  // simulation to execute $finish() when the last objection
  // to AUTO_FINISH is dropped.  Users can create other
  // objection kinds for their own purposes, but then need
  // to write code that calls the await() method to wait for
  // all objections to be dropped, and then implement any
  // required user-defined actions when await() returns.
  //
  `ifdef VCS
    static const string AUTO_FINISH = "AUTO_FINISH";
  `else
    const static string AUTO_FINISH = "AUTO_FINISH";
  `endif

  // CONSTRUCTOR
  //
  `ifdef INCA
    extern function new ( Component _parent,
                             string _name = Objection::AUTO_FINISH );
  `else
    extern function new ( Component _parent, string _name = AUTO_FINISH );
  `endif

  // FUNCTIONAL INTERFACE
  //
  extern function void  raise ();
  extern function void  drop  ();
  extern task           await ();

  // DIAGNOSTICS AND QUERY FUNCTIONS
  //
  extern function int       get_count  (bit all = 0);
  extern function string    get_name   ();
  extern function Component get_parent ();
  extern function void      set_debug  (int _debug, bit all = 0);

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  //                           PRIVATE                             //
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

  static local Objection objs[string];
  static local int next_unnamed_ID;

  local bit create_server;

  local int count;
  local event all_dropped;     // used only by the server

  local string name;           // used only by the server
  local Objection server;      // in a server, is null
  local Component parent;
  local int unnamed_ID;        // used only when parent==null

  local int debug;  // unused in this version with no debug

  extern local function string parent_name ();

endclass : Objection


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                       IMPLEMENTATIONS                         //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

function Component Objection::get_parent();
  return parent;
endfunction : get_parent

function string Objection::parent_name(); // this one is local
  if (parent == null)
    $sformat(parent_name, "<UNNAMED__%0d>", unnamed_ID);
  else
    return parent.get_hier_name();
endfunction : parent_name

function int Objection::get_count(bit all);
  return (all && (server != null)) ? server.count : count;
endfunction : get_count

function string Objection::get_name();
  return (server == null) ? name : server.name;
endfunction : get_name

function void Objection::set_debug(int _debug, bit all);
  if (all && (server != null))
    server.debug = _debug;
  else
    debug = _debug;
endfunction : set_debug

// _________________________________________________ Objection::new() __

function Objection::new(Component _parent, string _name);

  parent = _parent;
  count  = 0;

  // Look for an existing objection server with this name.
  // If one exists, we will use it.  If not, we will
  // detect server==null and therefore create a server too.
  server = objs[_name];

  if (server == null) begin
    // We need to make an objection server.  This needs care
    // because we use the same class to do it, and it must be
    // created in a different way.  We put ourself (the client)
    // on to the master objections table, and mark ourself as
    // a special case by setting the create_server flag.
    // These temporary patches will be fixed by the server
    // itself after creation (see below).
    create_server = 1;
    objs[_name] = this;

    // Make a new objection to act as a server for us
    // and for all future objections of this kind (name).
    server = new(null, _name);

    // OK, everything is now back to normal and the newly-
    // created server is the server for this client.

  end

  if (server.create_server) begin
    // This must be the call to new() that is trying to
    // create a server.

    // Only the server populates its name string
    name = _name;

    // Reset the flag in the original (first) client object:
    server.create_server = 0;
    // The newly created server needs some adjustment.
    // First, it must be installed on the list of servers:
    objs[_name] = this;
    // And it must be marked as being a server itself:
    server = null;

    // Special case for stop_test server...
    if (name == AUTO_FINISH)
      fork
        begin
          @(this.all_dropped);
          $display("Automatic $finish, all objections to %0s dropped",
                                                         AUTO_FINISH );
          $finish();
        end
      join_none

  end else begin
    // We are creating a client.

    if (parent == null)
      unnamed_ID = next_unnamed_ID++;

  end

endfunction : new

// _______________________________________________ Objection::raise() __

function void Objection::raise();

  assert (server != null) else
    $error("Attempt to use objection server %0s directly", name);

  count++;
  server.count++;

endfunction : raise

// ________________________________________________ Objection::drop() __

function void Objection::drop();

  assert (server != null) else
    $error("Attempt to use objection server %0s directly", name);
  assert (count > 0) else
    $error("Objection to %0s from %0s: drop() without raise()",
                      get_name(), parent_name() );
  count--;
  server.count--;

  if (server.count == 0)
    -> server.all_dropped;

endfunction : drop

// _______________________________________________ Objection::await() __

task Objection::await();

  assert (server != null) else
    $error("Attempt to use objection server %0s directly", name);

  @server.all_dropped;

endtask : await

`endif
