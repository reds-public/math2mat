//| Modifications |////////////////////////////////////////////////////////////
// Version   Author Date               Description
//  01.00     guo   20100408            add OVM reporting
//                  20100423            add OVM component
//  01.01     guo   20100429            add Reporting
//  01.02     guo   20100810            add Channel task await_empty
///////////////////////////////////////////////////////////////////////////////

// Base class definitions used in exercises exC5, exC6, exC7.
// You do not need to alter this file.

`ifndef BASES__SV
`define BASES__SV

`include "macros.sv"
//-- 20100429 v01.01 -- guo -- add -- include OVM
//`include "ovm.svh"

`ifdef M2M_USE_OVM
//-- 20100409 v01.00 -- guo -- add -- include OVM
`ifdef INCA
  // Cadence IUS
  `include "ovm.svh"
`else
  // Synopsys VCS and Mentor Questa
  import ovm_pkg::*;
`endif

`else
//-- 20100409 v01.00 -- guo -- add -- include OVM
`ifdef INCA
  // Cadence IUS
  `include "uvm.svh"
`else
  // Synopsys VCS and Mentor Questa
  import uvm_pkg::*;
`endif
`endif

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//-- 20100429 v01.01 -- guo -- change -- add Reporting
//// This file defines three classes.
// This file defines four classes.

// Component is the virtual base class for all testbench components.

// Transaction is the virtual base class for all transaction data objects.

// Channel is the non-parameterised, concrete FIFO channel class.
// It is not strictly a base class, since there is no need to extend it,
// but it is in this file for convenience.

//-- 20100429 v01.01 -- guo -- add -- add Reporting
// Reporting has been added to provied a unified reporting scheme for the whole
// testbench. Component derives from it, so all components of the testbench
// can use its functions.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

//-- 20100429 v01.01 -- guo -- add --
//    add own reporting class using ovm reporting functions, the functions
//    of this class can be re-implemented to become independent of OVM



`define M2M_REPORT_INFO(mess)       this.reporting_info(.id(this.get_hier_name()),.message(mess),.filename(`__FILE__),.line(`__LINE__));

`define M2M_REPORT_ERROR(mess)       this.reporting_error(.id(this.get_hier_name()),.message(mess),.filename(`__FILE__),.line(`__LINE__));

`define M2M_REPORT_VARIABLE(variable,value)  this.reporting_variable(variable,value);



class Reporting;


  // Function: new
  //
  // Creates a new report object with the given name. This method also creates
  // a new <ovm_report_handler> object to which most tasks are delegated.

//  extern function new(string name = "");


  extern function void reporting_variable (string variable, int value);
  
  // Function: report_info

  extern function void reporting_info(  string id,
                                        string message,
                                    string filename = "",
                                    int line = 0);

  // Function: report_error

  extern function void reporting_error( string id,
                                    string message,
                                    string filename = "",
                                    int line = 0);
  // Function: reporting_summarize
  //
  // Outputs statistical information on the reports issued by the central report
  // server.
  //
  // The run_test method in ovm_top calls this method.

  extern function void reporting_summarize();

endclass : Reporting

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100423 v01.00 -- guo -- change -- add OVM capabilities to Component
//virtual class Component;
//virtual class Component extends ovm_component;
virtual class Component extends Reporting;

  extern function           new (string _instance_name, Component _parent);
  `PURE virtual task        body(); `ENDPURETASK
  extern function    string get_name();
//-- 20100423 v01.00 -- guo -- change -- add OVM capabilities to Component
 extern function Component get_parent();
//  extern function ovm_component get_parent();
  extern function    string get_hier_name();
  extern task               run();

  local     string    instance_name;
  local     Component parent;
  protected Component children[$];

endclass : Component

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

virtual class Transaction;

  extern        function             new(Component owner = null);
  `PURE virtual function      string psprint();                  `ENDPUREFUNCTION
  `PURE virtual function Transaction copy();                     `ENDPUREFUNCTION
  `PURE virtual function         bit compare(Transaction other); `ENDPUREFUNCTION

  virtual function void sample_cov();
    // this function intentionally does nothing, but can be overridden
    int dummy;
  endfunction

  local static int       next_ID;
  protected    int       ID;
  protected    Component owner;

endclass : Transaction

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

class Channel;

  // Public methods of the Channel class
  //
  extern function     new(int _bound = 0);
  extern task         put(Transaction t);
  extern task         get(output Transaction t);
  extern function bit try_get(output Transaction t);
  extern function bit try_put(Transaction t);
  extern task         peek(output Transaction t);
  extern function bit try_peek(output Transaction t);

  // Private (local) data members and methods, used for the
  // internal implementation
  //
  local Transaction fifo[$];
  local event e_get, e_put;
  local int bound;
  //
  extern local function Transaction pop();
  extern local function        void push(Transaction t);
  extern local function         bit empty();
  extern local function         bit full();
  extern local task                 await_not_full();
  extern local task                 await_not_empty();
//-- 20100810 v01.02 --  guo -- add --
  extern local task                 await_empty();

endclass : Channel

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                    METHOD IMPLEMENTATIONS                   //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

function void Reporting::reporting_variable( string variable, int value);

	string msg;
	$sformat(msg,"M2MVar(%s)=%0d",variable,value);
	$display(msg);

endfunction : reporting_variable


function void Reporting::reporting_info(  string id,
                                        string message,
                                        string filename = "",
                                        int line = 0);
`ifdef M2M_USE_OVM
  ovm_top.ovm_report_info(  .id(id),
                    .message(message),
//                    .verbosity(OVM_MEDIUM),
                    .filename(filename),
                    .line(line));
`else                    
  uvm_top.uvm_report_info(  .id(id),
                    .message(message),
//                    .verbosity(OVM_MEDIUM),
                    .filename(filename),
                    .line(line));
`endif                    
endfunction : reporting_info

function void Reporting::reporting_error( string id,
                                        string message,
                                        string filename = "",
                                        int line = 0);
`ifdef M2M_USE_OVM
  ovm_top.ovm_report_error( .id(id),
                .message(message),
//                  .verbosity(OVM_LOW),
                .filename(filename),
                .line(line));
`else
  uvm_top.uvm_report_error( .id(id),
                .message(message),
//                  .verbosity(OVM_LOW),
                .filename(filename),
                .line(line));
`endif
endfunction : reporting_error

function void Reporting::reporting_summarize();
`ifdef M2M_USE_OVM
  ovm_top.report_summarize( .file(0) );
`else
  uvm_top.report_summarize( .file(0) );
`endif
endfunction : reporting_summarize

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//-- 20100429 v01.01 -- guo -- change --
//# ** Warning: ../src_SV/bases.sv(190): (vlog-LRM-2217) No default specified for 'owner'.  Default must match the value specified in class at ../src_SV/bases.sv(112) for strict LRM compliance.
//function Transaction::new(Component owner);
function Transaction::new(Component owner=null);
  this.owner = owner;
  ID = next_ID++;
endfunction : new

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

function string Component::get_name();
  return instance_name;
endfunction : get_name

function Component Component::get_parent();
  return parent;
endfunction : get_parent

function string Component::get_hier_name();
  if (parent == null)
    return instance_name;
  else
    return {parent.get_hier_name(), ".", instance_name};
endfunction : get_hier_name

task Component::run();

  // Launch all my children, if any (they will in turn do likewise)
  //
  foreach (children[i])
      children[i].run();

  // Launch my own body() method
  //
  fork
    body();
  join_none

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100408 v01.00 -- guo -- change -- add OVM reporting
//  $display("component %s body() task launched", get_hier_name() );
//  ovm_report_info(
//    .id( this.get_hier_name() ),
//    .message( {"component ", get_hier_name(), " body() task launched"} ),
////        .verbosity( OVM_LOW ),
//    .filename( `__FILE__ ),
//    .line( `__LINE__ )
//  );
  reporting_info(
    .id( this.get_hier_name() ),
    .message( {"component ", get_hier_name(), " body() task launched"} ),
    .filename( `__FILE__ ),
    .line( `__LINE__ )
  );


  // If I'm a top-level instance, I know that all body() methods
  // have started - so it's appropriate to wait for them to finish
  //
  if (parent == null)  // This is a top-level instance.
    wait fork;
  // NOTE: Because of some tool limitations, this use of "wait fork"
  //       must NOT be inside a begin...end block of any kind.

endtask : run

function Component::new (string _instance_name, Component _parent);
//-- 20100423 v01.00 -- guo -- add -- add ovm capabilities
//  super.new(_instance_name, _parent);

  instance_name = _instance_name;
  parent = _parent;
  if (parent != null)
    // This is not a top-level component.
    //  Add it to its parent's list of children.
    parent.children.push_back(this);

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100408 -- guo -- change -- v01.00 -- add OVM reporting
//  $display("Constructed %s", get_hier_name());
//  this.ovm_report_info(
//    .id( this.get_hier_name() ),
//    .message( {"Constructed ", get_hier_name()} ),
////        .verbosity( OVM_LOW ),
//    .filename( `__FILE__ ),
//    .line( `__LINE__ )
//  );
  this.reporting_info(
    .id( this.get_hier_name() ),
    .message( {"Constructed ", get_hier_name()} ),
    .filename( `__FILE__ ),
    .line( `__LINE__ )
  );

endfunction : new

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

function Transaction Channel::pop();
  -> e_get;
  return fifo.pop_front();
endfunction : pop

function void Channel::push(Transaction t);
  fifo.push_back(t);
  -> e_put;
endfunction : push

function bit Channel::empty();
  return (fifo.size() == 0);
endfunction : empty

function bit Channel::full();
  return (bound && (fifo.size() >= bound));
endfunction : full

task Channel::await_not_empty();
  while (empty()) @e_put;
endtask : await_not_empty

task Channel::await_not_full();
  while (full()) @e_get;
endtask : await_not_full

//-- 20100810 v01.02 -- guo -- add --
task Channel::await_empty();
//  $display("--guo-- ENTER await empty @get with fifo size = %0d", fifo.size() );
  while ( !empty() ) begin
//    $display("--guo--1Await empty @get with fifo size = %0d", fifo.size() );
    @e_get;
//    $display("--guo--2Await empty @get with fifo size = %0d", fifo.size() );
  end
//  $display("--guo-- EXIT await empty @get with fifo size = %0d", fifo.size() );
endtask : await_empty

//-- 20100429 v01.01 -- guo -- change --
//# ** Warning: ../src_SV/bases.sv(313): (vlog-LRM-2217) No default specified for '_bound'.  Default must match the value specified in class at ../src_SV/bases.sv(134) for strict LRM compliance.
//function Channel::new(int _bound);
function Channel::new(int _bound=0);
  bound = _bound;
endfunction : new

task Channel::put(Transaction t);
  await_not_full();
  push(t);
endtask : put

task Channel::get(output Transaction t);
  await_not_empty();
  t = pop();
endtask : get

function bit Channel::try_get(output Transaction t);
  if (empty()) return 0;
  t = pop();
  return 1;
endfunction : try_get

function bit Channel::try_put(Transaction t);
  if (full()) return 0;
  push(t);
  return 1;
endfunction : try_put

task Channel::peek(output Transaction t);
  await_not_empty();
  t = fifo[0];
endtask : peek

function bit Channel::try_peek(output Transaction t);
  if (empty()) return 0;
  t = fifo[0];
  return 1;
endfunction : try_peek

`endif
