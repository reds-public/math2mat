//| Modifications |////////////////////////////////////////////////////////////
// Version   Author Date               Description
//  01.00     guo   20100419            add OVM reporting
//  01.01     guo   20100429            add Reporting
///////////////////////////////////////////////////////////////////////////////

// Objection class to manage end-of-test stop, and other
// objections if required.

`ifndef OBJECTION__SV
`define OBJECTION__SV

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                      DEBUG FLAG VALUES                        //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

`define OBJECTION_DEBUG_CREATE        1
`define OBJECTION_DEBUG_PEAK          2
`define OBJECTION_DEBUG_ALL_DROPPED   4
`define OBJECTION_DEBUG_AWAIT         8
`define OBJECTION_DEBUG_RELEASE      16
`define OBJECTION_DEBUG_RAISE        32
`define OBJECTION_DEBUG_DROP         64

// Default debug action is to track peak values of objection count.
// This should help to identify "runaway" use of the objection
// mechanism in which raise() is repeatedly called without
// matching drop() calls.  For well-behaved usage, it will
// generate only a few debug messages near the start of the test.

`ifndef OBJECTION_DEBUG
  `define OBJECTION_DEBUG  `OBJECTION_DEBUG_PEAK
`endif

`include "bases.sv"  // Needed for definition of Component class

//-- 20100409 -- guo -- add -- include OVM
//`ifdef INCA
//  // Cadence IUS
//  `include "ovm.svh"
//`else
//  // Synopsys VCS and Mentor Questa
//  import ovm_pkg::*;
//`endif

//-- 20100429 v01.01 -- guo -- change -- add own Reporting class
//-- 20100409 v01.00 -- guo -- change -- add report capabilities to Objection
//class Objection;
//class Objection extends ovm_report_object;
class Objection extends Reporting;

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  //                      PUBLIC INTERFACE                         //
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

  // This constant defines the name of the default objection,
  // which controls the completion of a test.  Not only is this
  // the default objection kind when creating an objection client,
  // it also has the special automatic property that it causes
  // simulation to execute $finish() when the last objection
  // to AUTO_FINISH is dropped.  Users can create other
  // objection kinds for their own purposes, but then need
  // to write code that calls the await() method to wait for
  // all objections to be dropped, and then implement any
  // required user-defined actions when await() returns.
  //
  `ifdef VCS
    static const string AUTO_FINISH = "AUTO_FINISH";
  `else
    const static string AUTO_FINISH = "AUTO_FINISH";
  `endif

  // CONSTRUCTOR
  //
  `ifdef INCA
    extern function new ( Component _parent,
                             string _name = Objection::AUTO_FINISH );
  `else
    extern function new ( Component _parent, string _name = AUTO_FINISH );
  `endif

  // FUNCTIONAL INTERFACE
  //
  extern function void  raise ();
  extern function void  drop  ();
  extern task           await ();

  // DIAGNOSTICS AND QUERY FUNCTIONS
  //
  extern function int       get_count  (bit all = 0);
  extern function string    get_name   ();
  extern function Component get_parent ();
  extern function void      set_debug  (int _debug, bit all = 0);

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
  //                           PRIVATE                             //
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

  static local Objection objs[string];
  static local int next_unnamed_ID;

  local bit create_server;

  local int count;
  local int peak;
  local event all_dropped;     // used only by the server

  local string name;           // used only by the server
  local Objection server;      // in a server, is null
  local Component parent;
  local int unnamed_ID;        // used only when parent==null

  local int debug = `OBJECTION_DEBUG;

  extern local function string parent_name ();

endclass : Objection


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                       IMPLEMENTATIONS                         //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

function Component Objection::get_parent();
  return parent;
endfunction : get_parent

function string Objection::parent_name(); // this one is local
  if (parent == null)
    $sformat(parent_name, "<UNNAMED__%0d>", unnamed_ID);
  else
    return parent.get_hier_name();
endfunction : parent_name

//-- 20100429 v01.01 -- guo -- change --
//# ** Warning: objection.sv(135): (vlog-LRM-2217) No default specified for 'all'.  Default must match the value specified in class at objection.sv(90) for strict LRM compliance.
//function int Objection::get_count(bit all);
function int Objection::get_count(bit all=0);
  return (all && (server != null)) ? server.count : count;
endfunction : get_count

function string Objection::get_name();
  return (server == null) ? name : server.name;
endfunction : get_name

//-- 20100429 v01.01 -- guo -- change --
//# ** Warning: objection.sv(146): (vlog-LRM-2217) No default specified for 'all'.  Default must match the value specified in class at objection.sv(93) for strict LRM compliance.
//function void Objection::set_debug(int _debug, bit all);
function void Objection::set_debug(int _debug, bit all=0);
  if (all && (server != null))
    server.debug = _debug;
  else
    debug = _debug;
endfunction : set_debug

// _________________________________________________ Objection::new() __

//-- 20100429 v01.01 -- guo -- change --
//# ** Warning: objection.sv(158): (vlog-LRM-2217) No default specified for '_name'.  Default must match the value specified in class at objection.sv(79) for strict LRM compliance.
function Objection::new(Component _parent, string _name=AUTO_FINISH);

  parent = _parent;
  count  = 0;
  peak   = 0;

  // Look for an existing objection server with this name.
  // If one exists, we will use it.  If not, we will
  // detect server==null and therefore create a server too.
  server = objs[_name];

  if (server == null) begin
    // We need to make an objection server.  This needs care
    // because we use the same class to do it, and it must be
    // created in a different way.  We put ourself (the client)
    // on to the master objections table, and mark ourself as
    // a special case by setting the create_server flag.
    // These temporary patches will be fixed by the server
    // itself after creation (see below).
    create_server = 1;
    objs[_name] = this;

    // Make a new objection to act as a server for us
    // and for all future objections of this kind (name).
    server = new(null, _name);

    // OK, everything is now back to normal and the newly-
    // created server is the server for this client.

  end

  if (server.create_server) begin
    // This must be the call to new() that is trying to
    // create a server.

    if (debug & `OBJECTION_DEBUG_CREATE)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100409 v01.00 -- guo -- comment -- remove SV reporting
//      $display("Creating objection server %0s", _name);
//-- 20100409 v01.00 -- guo -- add -- OVM reporting
//      this.ovm_report_error(
//        .id({"objection.", name}),
//        .message({"Creating objection server ", _name}),
////        .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
    this.reporting_error(
      .id({"objection.", name}),
      .message({"Creating objection server ", _name}),
      .filename(`__FILE__),
      .line(`__LINE__)
    );

    // Only the server populates its name string
    name = _name;

    // Reset the flag in the original (first) client object:
    server.create_server = 0;
    // The newly created server needs some adjustment.
    // First, it must be installed on the list of servers:
    objs[_name] = this;
    // And it must be marked as being a server itself:
    server = null;

    // Special case for stop_test server...
    if (name == AUTO_FINISH)
      fork
        begin
          @(this.all_dropped);

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//          $display("Automatic $finish, all objections to %0s dropped",
//                                                         AUTO_FINISH );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//          this.ovm_report_info(
//            .id({"objection.", name}),
//            .message({"Automatic $finish, all objections to ", name, " dropped"}),
////        .verbosity(OVM_LOW),
//            .filename(`__FILE__),
//            .line(`__LINE__)
//          );
        this.reporting_info(
          .id({"objection.", name}),
          .message({"Automatic $finish, all objections to ", name, " dropped"}),
          .filename(`__FILE__),
          .line(`__LINE__)
        );

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//          this.report_summarize(0);
          this.reporting_summarize();

          $finish();
        end
      join_none

  end else begin
    // We are creating a client.
    if (parent == null)
      unnamed_ID = next_unnamed_ID++;
    if (debug & `OBJECTION_DEBUG_CREATE)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//      $display("Created objection client %0s for %0s",
//                                     get_name(), parent_name() );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//      this.ovm_report_info(
//        .id({"objection.", name}),
//        .message({"Created objection client ", get_name(), " for ", parent_name(), "."}),
////        .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
    this.reporting_info(
          .id({"objection.", name}),
          .message({"Created objection client ", get_name(), " for ", parent_name(), "."}),
          .filename(`__FILE__),
          .line(`__LINE__)
        );


  end

endfunction : new

// _______________________________________________ Objection::raise() __

function void Objection::raise();

//-- 20100419 v01.00 -- guo -- add -- string for reporting
  string msg;

  assert (server != null) else

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100409 v01.00 -- guo -- comment -- remove SV reporting
//    $error("Attempt to use objection server %0s directly", name);
//-- 20100409 v01.00 -- guo -- add -- OVM reporting
//      this.ovm_report_error(
//        .id({"objection.", get_name()}),
//        .message({"Attempt to use objection server ", name, " directly."}),
////                  .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
      this.reporting_error(
          .id({"objection.", get_name()}),
          .message({"Attempt to use objection server ", name, " directly."}),
          .filename(`__FILE__),
          .line(`__LINE__)
      );

  count++;
  server.count++;

  if ((debug & `OBJECTION_DEBUG_RAISE) ||
      (server.debug & `OBJECTION_DEBUG_RAISE)) begin
    string s;
    $sformat(s, "Objections to %0s raised", get_name());
    if (server.debug & `OBJECTION_DEBUG_RAISE)
      $sformat(s, "%0s to %0d", s, server.count);
    if (debug & `OBJECTION_DEBUG_RAISE)
      $sformat(s, "%0s by %0s with %0d", s, parent_name(), count);

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//    $display(s);
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_info(
//      .id({"objection.", get_name()}),
//      .message(s),
////        .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_info(
      .id({"objection.", get_name()}),
      .message(s),
      .filename(`__FILE__),
      .line(`__LINE__)
    );

  end

  if (count > peak) begin
    peak = count;
    if (debug & `OBJECTION_DEBUG_PEAK)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//      $display("Objections to %0s from %0s: new peak = %0d",
//                           get_name(), parent_name(),  peak );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
      $sformat(msg, "Objections to %0s from %0s: new peak = %0d", get_name(), parent_name(),  peak);
//      this.ovm_report_info(
//        .id({"objection.", get_name()}),
//        .message(msg),
////        .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
    this.reporting_info(
      .id({"objection.", get_name()}),
      .message(msg),
      .filename(`__FILE__),
      .line(`__LINE__)
    );

  end
  if (server.count > server.peak) begin
    server.peak = server.count;
    if (server.debug & `OBJECTION_DEBUG_PEAK)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//      $display("Objection %0s new global peak = %0d",
//                          get_name(),             server.peak);
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
      $sformat(msg, "Objections to %0s: new global peak = %0d", get_name(),  peak);
//      this.ovm_report_info(
//        .id({"objection.", get_name()}),
//        .message(msg),
////        .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
    this.reporting_info(
      .id({"objection.", get_name()}),
      .message(msg),
      .filename(`__FILE__),
      .line(`__LINE__)
    );

  end
endfunction : raise

// ________________________________________________ Objection::drop() __

function void Objection::drop();
  assert (server != null) else

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100409 v01.00 -- guo -- comment -- remove SV reporting
//    $error("Attempt to use objection server %0s directly", name);
//-- 20100409 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_error(
//      .id({"objection.", get_name()}),
//      .message({"Attempt to use objection server ", name, " directly."}),
////                  .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_error(
          .id({"objection.", get_name()}),
          .message({"Attempt to use objection server ", name, " directly."}),
          .filename(`__FILE__),
          .line(`__LINE__)
    );

  assert (count > 0) else

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100409 v01.00 -- guo -- comment -- remove SV reporting
//    $error("Objection to %0s from %0s: drop() without raise()",
//                      get_name(), parent_name() );
//-- 20100409 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_error(
//      .id({"objection.", get_name()}),
//      .message({"Objection to ", get_name(), " from ", parent_name(), ": drop() without raise()."}),
////                  .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_error(
          .id({"objection.", get_name()}),
          .message({"Objection to ", get_name(), " from ", parent_name(), ": drop() without raise()."}),
          .filename(`__FILE__),
          .line(`__LINE__)
    );

  count--;
  server.count--;

  if ((debug & `OBJECTION_DEBUG_DROP) ||
      (server.debug & `OBJECTION_DEBUG_DROP)) begin
    string s;
    $sformat(s, "Objections to %0s dropped", get_name());
    if (server.debug & `OBJECTION_DEBUG_DROP)
      $sformat(s, "%0s to %0d", s, server.count);
    if (debug & `OBJECTION_DEBUG_DROP)
      $sformat(s, "%0s by %0s with %0d", s, parent_name(), count);

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//    $display(s);
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_info(
//      .id({"objection.", get_name()}),
//      .message({s}),
////        .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_info(
      .id({"objection.", get_name()}),
      .message({s}),
      .filename(`__FILE__),
      .line(`__LINE__)
    );

  end

  if (count == 0) begin
    if (debug & `OBJECTION_DEBUG_ALL_DROPPED)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//      $display("No objection to %0s from %0s",
//                             get_name(), parent_name() );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//      this.ovm_report_info(
//        .id({"objection.", get_name()}),
//        .message({"No objection to ", get_name, " from ", parent_name(), "."}),
////          .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
      this.reporting_info(
      .id({"objection.", get_name()}),
      .message({"No objection to ", get_name, " from ", parent_name(), "."}),
      .filename(`__FILE__),
      .line(`__LINE__)
      );

  end
  if (server.count == 0) begin
    if (server.debug & `OBJECTION_DEBUG_ALL_DROPPED)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//      $display("No further objection to %0s", get_name() );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//      this.ovm_report_info(
//        .id({"objection.", get_name()}),
//        .message({"No further objection to ", get_name(), "."}),
////          .verbosity(OVM_LOW),
//        .filename(`__FILE__),
//        .line(`__LINE__)
//      );
      this.reporting_info(
       .id({"objection.", get_name()}),
      .message({"No further objection to ", get_name(), "."}),
      .filename(`__FILE__),
      .line(`__LINE__)
      );


    fork
      -> server.all_dropped;
    join_none  // Allow the current thread to run to a wait
  end
endfunction : drop

// _______________________________________________ Objection::await() __

task Objection::await();
  assert (server != null) else
    $error("Attempt to use objection server %0s directly", name);

        //-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100409 v01.00 -- guo -- comment -- remove SV reporting
//    $error("Attempt to use objection server %0s directly", name);
//-- 20100409 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_error(
//      .id({"objection.", get_name()}),
//      .message({"Attempt to use objection server ", name, " directly."}),
////        .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_error(
          .id({"objection.", get_name()}),
          .message({"Attempt to use objection server ", name, " directly."}),
          //                  .verbosity(OVM_LOW),
          .filename(`__FILE__),
          .line(`__LINE__)
    );


  if (debug & `OBJECTION_DEBUG_AWAIT)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
//    $display("%0s waiting on objection %0s", parent_name(), get_name() );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_info(
//      .id({"objection.", get_name()}),
//      .message({parent_name(), " waiting on objection ", get_name(), "."}),
////        .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_info(
        .id({"objection.", get_name()}),
        .message({parent_name(), " waiting on objection ", get_name(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
    );

  @server.all_dropped;
  if (debug & `OBJECTION_DEBUG_RELEASE)

//-- 20100429 v01.01 -- guo -- change -- add Reporting
//-- 20100329 v01.00 -- guo -- comment -- remove SV reporting
    $display("%0s released by objection %0s", parent_name(), get_name() );
//-- 20100329 v01.00 -- guo -- add -- OVM reporting
//    this.ovm_report_info(
//      .id({"objection.", get_name()}),
//      .message({parent_name(), " released by objection ", get_name(), "."}),
////        .verbosity(OVM_LOW),
//      .filename(`__FILE__),
//      .line(`__LINE__)
//    );
    this.reporting_info(
        .id({"objection.", get_name()}),
        .message({parent_name(), " released by objection ", get_name(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
    );

endtask : await

`endif
