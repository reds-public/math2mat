///////////////////////////////////////////////////////////////////////////////
// HES-SO//Valais Wallis, University of Applied Sciences Western Switzerland
// ISI, Institute Systems Engineering
//
// File         : M2M_internal_classes.sv
//
// Description  :
//
// Author       : Oliver Gubler
// Date         : 16.08.2010
// Version      : 01.00
//
// Use          : testbench for Math2Mat
//
//| Modifications |////////////////////////////////////////////////////////////
// Version   Author Date               Description
//  01.00     guo   20100816            initial version
///////////////////////////////////////////////////////////////////////////////

`ifndef M2M_INT_CLASSES__SV
`define M2M_INT_CLASSES__SV

`include "bases.sv"

/**
 * class M2M_Internal_Trans
 *
 * Data class to represent the result of the operation.
 */
class M2M_Internal_Trans extends Transaction;

  T_data Data;
  
  function new(Component owner = null);
    super.new(owner);
  endfunction : new

  function Transaction copy();
    M2M_Internal_Trans clone;
    clone = new this;
    return clone;
  endfunction : copy

  function M2M_Internal_Trans m2m_copy();
    M2M_Internal_Trans clone;
    clone = new this;
    return clone;
  endfunction : m2m_copy

  function bit compare(Transaction other);
    M2M_Internal_Trans that;
    assert ($cast(that, other)) else begin

      string id_string;
      id_string.itoa(ID);
      owner.reporting_error(
        .id({"transaction.", id_string}),
        .message({"Can't compare ", this.psprint(), " with other ", other.psprint(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
      );
    end

    return (bitdiff(other) <= `NUMBER_BIT_PRECISION);
  
  endfunction : compare
  
  
  function int bitdiff(Transaction other);
    M2M_Internal_Trans that;
    
    assert ($cast(that, other)) else begin
      string id_string;
      id_string.itoa(ID);
      owner.reporting_error(
        .id({"transaction.", id_string}),
        .message({"Can't calculate bits of ", this.psprint(), " with other ", other.psprint(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
      );
    end
   
    // Bus size is 32 bits
    if(`M2M_BUS_SIZE == 32) begin
    
      // Check the sign of the Data
      if(that.Data[31:31]  != this.Data[31:31]) begin
        return 32;
      end
      
      // check the exp if it's greater than 1
      else if (abs(that.Data[30:23], this.Data[30:23]) > 1) begin
        for(int i = 23; i <= 30; i++) begin
          if (that.Data[i] != this.Data[i]) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's equal to 1
      else if (abs(that.Data[30:23], this.Data[30:23]) == 1) begin
        if(that.Data[30:23] > this.Data[30:23]) begin
          that.Data[22:0] = (2**22)-1 - that.Data[22:0];
        end 
        else begin
          this.Data[22:0] = (2**22)-1 - this.Data[22:0];
        end
        for(int i = 0; i <= 22; i++) begin
          if (that.Data[i] != this.Data[i] ) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's smaller than 1
      else if (abs(that.Data[22:0], this.Data[22:0]) > 0) begin
        for(int i = 0; i <= 22; i++) begin
          if (that.Data[i] != this.Data[i] ) begin
            return i+1;
          end
        end
      end

    // Bus size is 64 bits
    else  
      
      // Check the sign of the Data
      if(that.Data[63:52]  != this.Data[63:52]) begin
        return 64;
      end
      
      // check the exp if it's greater than 1
      else if (abs(that.Data[62:52], this.Data[62:52]) > 1) begin
        for(int i = 52; i <= 62; i++) begin
          if (that.Data[i] != this.Data[i]) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's equal to 1
      else if (abs(that.Data[62:52], this.Data[62:52]) == 1) begin
        if(that.Data[62:52] > this.Data[62:52]) begin
          that.Data[51:0] = (2**51)-1 - that.Data[51:0];
        end 
        else begin
          this.Data[51:0] = (2**51)-1 - this.Data[51:0];
        end
        for(int i = 0; i <= 51; i++) begin
          if (that.Data[i] != this.Data[i] ) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's smaller than 1
      else if (abs(that.Data[51:0], this.Data[51:0]) > 0) begin
        for(int i = 0; i <= 51; i++) begin
          if (that.Data[i] != this.Data[i] ) begin
            return i+1;
          end
        end
      end
    end
    
    return 0;
    
  endfunction : bitdiff
  
  
  function int abs(int a, int b);
    if(a-b > 0)
      return a-b;
    else
      return b-a;
  endfunction : abs
  
  
  function string psprint();
    $sformat(psprint, "M2M_Internal_Trans : Data = 0x%h = %g", Data, $bitstoshortreal(Data));
  endfunction : psprint

endclass : M2M_Internal_Trans

/**
 * Passive monitor BFM that can observe the internal interface,
 * which is simply "m2m_bus_size" data bits. Data is latched
 * on the rising edge of the clocking block's clock signal.
 */
class M2M_Internal_Monitor extends Component;

//
`ifdef NO_MODPORT_QUALIFICATION
  typedef virtual M2M_internal_intf        M2M_int_mon_hook;
`else
  typedef virtual M2M_internal_intf.mon_mp M2M_int_mon_hook;
`endif

  Channel source[1:`NUMBER_OF_DUT_INTERNALS];
  M2M_int_mon_hook hook;

  M2M_Internal_Trans template;

  function new(
    string _name,
    Component _parent,
    M2M_int_mon_hook _hook);

    super.new(_name, _parent);
    hook = _hook;
    template = new(this);

  endfunction : new

  task body();

    // Every time there is a rising edge on the clock
    forever @(hook.mon_cb) begin

      for (int i = 1; i <= `NUMBER_OF_DUT_INTERNALS; i++) begin
       // Collect information in the transaction object
         template.Data = hook.mon_cb.Data[i];

        /**
         * Try to put a copy of the monitored transaction on to the internals
         * channel.  This put attempt MUST succeed in zero time - we use an
         * assertion to check that the try_put succeeded.
         */
        if ( (hook.mon_cb.Data_Valid[i]==1) && (hook.mon_cb.Data_Ready[i]==1) ) begin
          assert ( source[i].try_put(template.copy()) )
          else begin
            `M2M_REPORT_ERROR({"Channel ",
                        this.get_hier_name(),
                        ".source blocked, lost data: ",
                        template.psprint(),
                        "."})
          end //: assert

        end //: if

      end //: for

    end //: forever

  endtask : body

endclass : M2M_Internal_Monitor


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// M2M_Int_Component
// ~~~~~~~~~~~~~~~~~
/**
 * Re-usable wrapper for the internal monitor BFM.It only
 * contains the monitor subcomponent and an unbounded
 * FIFO for the monitor's output.
 */
class M2M_Internal_Component extends Component;

  // Variables for the monitor and channel instances.
  // The channel is conventionally named "analysis".
  M2M_Internal_Monitor  monitor;
  Channel               analysis[1:`NUMBER_OF_DUT_INTERNALS];

  function new(
             string             _name,
             Component          _parent,
    virtual  M2M_internal_intf  _hook);

    super.new( _name, _parent );

    // Create the monitor and unbounded channel.
    for (int i = 1; i <= `NUMBER_OF_DUT_INTERNALS; i++) begin
      analysis[i] = new(0);
    end //: for

    monitor  = new("internal monitor", this, _hook);

    // Connect the analysis channel to the monitor BFM.
    monitor.source = analysis;

  endfunction : new

  task body();
    // Like most wrapper components, this one has
    // no useful code in its body() method.

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".body"} )

  endtask

endclass : M2M_Internal_Component

`endif
