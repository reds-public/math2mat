`ifndef MACROS__SV
`define MACROS__SV

/**********************************************************

This file defines a collection of macros making it possible
to write code that works around various tool issues affecting
the Doulos lab exercise files.

For each possible tool restriction, a macro is defined if
that restriction is applicable.

The restrictions currently supported by this file are:

NO_INSTANCE_CONSTANTS
NO_PURE_KEYWORD
NO_INTF_CALL_FROM_CLASS
ASSOC_ARRAY_STAR
NO_MODPORT_QUALIFICATION

Detailed descriptions of each are found later in the file.
Some of these macros cause other convenience macros to be
defined; these convenience macros should be used in user
code to make it portable across simulators.

The `ifdefs below specify which restrictions are currently
active for each major simulator.  DON'T FORGET TO KEEP THEM
UP-TO-DATE for new simulator versions!

************************************************************/

  `ifdef VCS
    // Restrictions for Synopsys VCS 2006.06-SP2
    `define NO_INSTANCE_CONSTANTS
    `define NO_PURE_KEYWORD
    `define NO_INTF_CALL_FROM_CLASS
    `define ASSOC_ARRAY_STAR
  `endif

  `ifdef INCA
    // Restrictions for Cadence IUS 6.2
    `define NO_MODPORT_QUALIFICATION
    `define NO_PURE_KEYWORD
  `endif

  `ifdef MODEL_TECH
    // Restrictions for Mentor Questa 6.3d
  `endif

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// NO_INSTANCE_CONSTANTS
// ~~~~~~~~~~~~~~~~~~~~~
// Defined when the "const" qualifier for constant data members
// of a class is not supported.  Note that "static const" is
// supported by all tools that we use, and is not affected.
// Affects the definition of macro CONST.
// Typical usage:
// 
// class C;
//   const int n;   // Not supported by all tools
//   `CONST int n;  // Portable

  `ifdef NO_INSTANCE_CONSTANTS
    // instance constants not supported: just remove the 
    // const attribute.  We lose read-only checking by
    // compiler, but functionality should still be OK for
    // correctly-written classes.
    `define CONST
  `else
    // We can use the const attribute for instance constants.
    `define CONST const
  `endif


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// NO_PURE_KEYWORD
// ~~~~~~~~~~~~~~~
// Defined for tools that don't support IEEE P1800-2009 "pure"
// keyword for virtual methods.  Affects the definition of 
// macros PURE, ENDPUREFUNCTION, ENDPURETASK.
//
// Example usage:
//
//  virtual class C;
//    pure virtual function bit F;  // not supported by all tools
//    pure virtual task T;          // not supported by all tools
//    `PURE virtual function bit F; `ENDPUREFUNCTION  // Portable
//    `PURE virtual task T; `ENDPURETASK              // Portable

  `ifdef NO_PURE_KEYWORD
    // Can't use "pure" keyword.  Pure virtual methods
    // are simply ordinary methods with no body.
    `define PURE 
    `define ENDPUREFUNCTION endfunction
    `define ENDPURETASK     endtask
  `else
    // SV-2009 compliant: use "pure" keyword on pure virtual
    // method prototypes, no endfunction/endtask.
    `define PURE pure 
    `define ENDPUREFUNCTION
    `define ENDPURETASK
  `endif


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// NO_INTF_CALL_FROM_CLASS
// ~~~~~~~~~~~~~~~~~~~~~~~
// Defined for tools that do not support calling from a class
// into a task or function imported from an interface.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ASSOC_ARRAY_STAR
// ~~~~~~~~~~~~~~~~
// Defined for tools that only support wildcard-integer as the
// indexing type for an associative array.
//
// Example usage:
//
//  typedef enum {A, B, C, D} E;
//
//  `ifdef ASSOC_ARRAY_STAR
//    int Counter[*];
//  `else
//    int Counter[E];
//  `endif
//  ...
//  Counter[A] = 4;  // OK, portable.


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// NO_MODPORT_QUALIFICATION
// ~~~~~~~~~~~~~~~~~~~~~~~~
// Defined for tools that do not support copying of a virtual 
// interface that does not have a modport specified into a VI
// variable that does have a modport specified.  For example:
//
//   interface INTF;
//     ...
//     modport MP(...);
//   endinterface
//
//   class C;
//     virtual INTF vi;
//     virtual INTF.MP vi_mp;
//     ...
//     vi_mp = vi;  // should be OK, not supported by all tools
//
// For such tools it may be necessary to drop the modport
// qualification on the more restrictive variable, thereby
// losing the stronger checking but preserving most functionality
// (except modport expressions):
//
//   class C;
//     virtual INTF vi;
//     `ifdef NO_MODPORT_QUALIFICATION
//     virtual INTF vi_mp;  // weaker checking, otherwise OK
//     `else
//     virtual INTF.MP vi_mp;  // uses the modport
//     `endif
//     ...
//     vi_mp = vi;  // portable across tools


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


`endif
