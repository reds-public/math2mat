/**
 * \brief This file containts the testbench of the Math2mat project
 * 
 * RCSO-ISYS, HES-SO
 * Project : Math2mat
 *
 * This testbench only instantiates the environment that will create the entire
 * structure. It then do nothing, as the objection mechanism is used to
 * detect the end of simulation.
 *
 * \file M2M_tb.sv
 * \author S. Masle, O. Gubler, Y. Thoma
 * \par Team: 
 *            \li <a href="http://www.reds.ch">REDS Institute, HEIG-VD</a>
 *            \li <a href="http://isi.hevs.ch">ISI Institute, HEVS</a>
 *
 * \version 1.0
 * \date 04.02.2011
 *****************************************************************************/
 
 
`include "common.sv"
`include "M2M_input_classes.sv"
`include "M2M_output_classes.sv"
`include "M2M_env.sv"
`include "M2M_internal_classes.sv"

/**
 * This module only creates the environment. It then let everything run.  
 * Because we are using the objection* mechanism, there will be an automatic
 * $finish when all objections have been dropped.
*/
module M2M_TB;


  /*
   * Program body, that creates the environment, and then do nothing.
   */
  initial begin

    // Handle to the class-based environment
    M2M_env tb;

    // Handle to a template object for stimulus generation
    //M2M_Input_Trans M2M_in_tr;

    // Create the environment object, hook it to the test harness
    tb = new("tb", null,
                   M2M_harness.M2M_in_intf,
                   M2M_harness.M2M_out_intf,
                   M2M_harness.M2M_int_intf);


    // Create and install a new template object
    //M2M_in_tr = new(tb.M2M_in_comp.stim_gen);
    //tb.set_stim_template( M2M_in_tr );

    // Reconfigure the stimulus generator's run length
    //tb.set_num_trans(1000);

    // Let everything run.  Because we are using the objection
    // mechanism, there will be an automatic $finish when all
    // objections to STOP_TEST have been dropped.
    tb.run();

    // This $finish is no longer required, thanks to the
    // objection mechanism.  Indeed, it will never be reached,
    // because the run() method of this testbench will
    // never terminate.
    //
    $finish();

  end

endmodule : M2M_TB
