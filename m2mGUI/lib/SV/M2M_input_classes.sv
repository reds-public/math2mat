/**
 * \brief This file containts the classes that act on the input of the DUT
 * 
 * RCSO-ISYS, HES-SO
 * Project : Math2mat
 *
 * The classes described in this file are:
 *   \li M2M_Input_Trans : Input transaction to the driver
 *   \li M2M_Input_Stim : Stimuli generator
 *   \li M2M_Input_Driver : Driver for the DUT
 *   \li M2M_Input_Monitor : Monitor for the input stimuli
 *   \li M2M_Input_Component : A component that instantiates the others
 * These classes allow to create the stimuli, use them to act on the DUT,
 * and to monitor them in order to inform the checker.
 *
 * \file M2M_input_classes.sv
 * \author S. Masle, O. Gubler, Y. Thoma
 * \par Team: 
 *            \li <a href="http://www.reds.ch">REDS Institute, HEIG-VD</a>
 *            \li <a href="http://isi.hevs.ch">ISI Institute, HEVS</a>
 *
 * \version 1.0
 * \date 04.02.2011
 *****************************************************************************/
 
`ifndef M2M_INPUT_CLASSES__SV
`define M2M_INPUT_CLASSES__SV

`include "macros.sv"
`include "bases.sv"
`include "objection.sv"


//! Type of data to be used in the generated system. Usually a 32-bit vector.
typedef logic [`M2M_BUS_SIZE-1:0] T_data;

/**
* This class contains two parameters used to carry input data. This transaction
* class is used everywhere DUT's input data is needed, for giving data to the
* DUT as well as just reading data on a bus.
* This transaction class just contains the data value and the corresponding bit
* "data_valid" that indicates if the data is valid or not.
*/
class M2M_Input_Trans extends Transaction;

  //! Data carried by the transaction 
  T_data data;
  
  //! Indicates if the data is valid (signal sent to the DUT)
  logic data_valid;

  /**
   * Empty constructor.
   * It only calls the super class constructor
   */
  function new(Component owner = null);
    super.new(owner);
  endfunction : new

   /**
    * copy a transaction
    * \return The copy of the current transaction
    */
  function Transaction copy();
    M2M_Input_Trans clone;
    clone = new this;
    return clone;
  endfunction : copy


  /**
  * Compares the current transaction to the one given in parameter.
  * If the two transaction are not of the same class, an error is reported.
  *
  * \param other the transaction to compare with the current one.
  * \return bit 1'b1 is the transactions are identical, 1'b0 otherwise.
  */
  function bit compare(Transaction other);
    M2M_Input_Trans that;
    // Check that the two transactions are of the same class
    assert ($cast(that, other)) else begin
      // Reports an error
      string id_string;
      id_string.itoa(ID);
      owner.reporting_error(
        .id({"transaction.", id_string}),
        .message({"Can't compare M2M_Input_Trans with other ", other.psprint()}),
        .filename(`__FILE__),
        .line(`__LINE__)
      );
      return 1'b0;
    end

    if (that.data   !== data ) return 1'b0;
    return 1'b1;
  endfunction : compare

  /**
   * Returns a string containing information about the transaction.
   * \return a string with info about the transaction
   */
  function string psprint();
    $sformat(psprint, "M2M_Input_Trans : data = 0x%h = %g", data, $bitstoshortreal(data));
  endfunction : psprint

endclass : M2M_Input_Trans


/**
* Class M2M_Input_Stim takes the samples in the input files. It puts these samples
* in the channel corresponding to the input number.
*/
class M2M_Input_Stim extends Component;

  /**
  * Array of channels, one channel per DUT input
  */
  Channel sink [1:`NUMBER_OF_DUT_INPUTS];

  /**
  * integer to know how much we have to shift when some inputs are parallel
  *
  * ex: inputs : a - b[1] - b[2] - b[3] - c
  * 
  * index of c = 5 => c is the 3rd input => index = 3 + offset where offset = 2 (size of b - 1)
  */
  int offset = 0;

  /**
   * Objection mechanism so that this object can express its
   * refusal to allow the test to stop
   */
  Objection objection_to_stop;

  /**
   * Constructor. It calls the super class constructor, and constructs an
   * objection.
   * \param _name Name of the component
   * \param _parent Parent object of the component
   */
  function new(string _name, Component _parent);
    super.new( _name, _parent );
    // Construct the objection
    objection_to_stop = new(this);
  endfunction : new

  /**
  * this task opens input files and launches put_stim(...) task as many times as there is inputs
  */
  task body();

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".body began"})

    // We are about to start work, so we don't want the test to stop!
    objection_to_stop.raise();

    for (int i = 1; i <= `NUMBER_OF_INPUTS; i++) begin

        int fd;     //file descriptor
        T_data data;  //data read from the file
        string fileName;    //file name that will change each loop
        logic [31:0] samples_Number; //number of samples in the file
        logic [31:0] vector_Length;  //length of the vector of each sample

        //separate each case to form the name of the file because "%d" replaces leading zeroes by spaces
        //we can have until 999 inputs without having troubles, which seems enough
        if (i < 10)
            $sformat(fileName, "iofiles/file_input%1d.dat", i);
        else if (i < 100)
            $sformat(fileName, "iofiles/file_input%2d.dat", i);
        else
            $sformat(fileName, "iofiles/file_input%3d.dat", i);

        fd = $fopen(fileName, "rb");
        assert (fd) else
            $fatal(1, "Could not open %s", fileName);

        void'($fread(samples_Number, fd));
        void'($fread(vector_Length, fd));

        fork
            automatic int file_descriptor = fd;
            automatic int input_nbr = i;
            automatic int offset_input = offset;
            automatic logic [31:0] nbr_samples = samples_Number;
            automatic logic [31:0] vect_length = vector_Length;

			// Starts a new task for managing the input i
            put_stim(file_descriptor, input_nbr, offset_input, nbr_samples, vect_length);
        join_none

        if (INPUT_PARALLEL[i] == 1'b1) begin
            offset += vector_Length - 1;
        end

    end

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".body finished"})

    // We have finished our work, so we no longer have any objection
    // to stopping the test
    objection_to_stop.drop();

  endtask

  /**
  * this task read an input file, initialises a new M2M_Input_Trans transaction with the values contained
  * in the file, and puts it in the rigth channel.
  *
  * \param integer fd file descriptor of the file that contains the samples.
  * \param integer input_nbr the number of the input corresponding to the file opened (one file per input).
  * \param offset offset to add to the input number to know the channel to fill.
  * \param logic[31:0] samples_Number number of samples in the file, samples can be scalar or vector.
  * \param logic[31:0] vector_Length length of the vector in input, 1 if scalar, >1 if vector.
  */
  task put_stim (input integer fd, input_nbr, offset, logic  [31:0] samples_Number, logic  [31:0] vector_Length);

      M2M_Input_Trans template;
      logic [`M2M_BUS_SIZE-1:0] data;           //data read from the file

      objection_to_stop.raise();

      template = new(this);

      //if inputs are parallel, read the file and send each data in a different channel
      if (INPUT_PARALLEL[input_nbr] == 1'b1) begin
          repeat (samples_Number) begin
              for (int k = 0; k < vector_Length; k++) begin
                  void'($fread(data, fd));
                  template.data = data;
                  template.data_valid = 1'b1;
                  sink[offset+input_nbr+k].put(template.copy());
              end
          end
          for (int k = 0; k < vector_Length; k++) begin
              template.data = 0;
              template.data_valid = 1'b0;
              sink[offset+input_nbr+k].put(template.copy());
          end
      end
      //else send all data in the same channel
      else begin
          repeat (samples_Number*vector_Length) begin
              void'($fread(data, fd));
              template.data = data;
              template.data_valid = 1'b1;
              sink[offset+input_nbr].put(template.copy());
          end
          template.data = 0;
          template.data_valid = 1'b0;
          sink[offset+input_nbr].put(template.copy());
      end

      $fclose(fd);

      objection_to_stop.drop();
  endtask

endclass : M2M_Input_Stim

/**
* Class M2M_Input_Driver representing bus-functional model that
* can drive M2M_input transactions through a virtual interface when
* the testbench calls the drive() method of this class.
*/
class M2M_Input_Driver extends Component;

  // Virtual interface variable to hold a reference
  // to the test access interface that exists in our test
  // harness module
  //
  `ifdef NO_MODPORT_QUALIFICATION
    typedef virtual M2M_input_intf        M2M_in_drv_hook;
  `else
    typedef virtual M2M_input_intf.drv_mp M2M_in_drv_hook;
  `endif

  M2M_in_drv_hook hook;

  //! Storage for the read and write cycle counts
  int trans_count;

  //! One channel for each input
  Channel source[1:`NUMBER_OF_DUT_INPUTS];

  //! array of queues that holds the input values.
  T_data Input_values[1:`NUMBER_OF_DUT_INPUTS][$];

  //! array of queues that holds the valid signal for each input value.
  logic  Input_valid [1:`NUMBER_OF_DUT_INPUTS][$];


  /**
   * Objection mechanism so that this object can express its
   * refusal to allow the test to stop
   */
  Objection objection_to_stop;

  function new(string _name, Component _parent,
               M2M_in_drv_hook _hook);
    super.new( _name, _parent );
    hook = _hook;
    objection_to_stop = new(this);
  endfunction : new

  task body();
    forever begin

      `M2M_REPORT_INFO({"task ", get_hier_name(), ".body began"})

      // We have some data to work on, so we don't want the test to stop.
      objection_to_stop.raise();

      //launch 2 parallel task, one to get the stimuli, the other to drive
      //the stimuli. We have 2 task because we can't drive a signal if the
      //corresponding ready signal is not equal to 1'b1, but we need to
      //continue to receive the stimuli and put them in queues.
      fork
          get_stim();
          drive();
      join

      `M2M_REPORT_INFO({"task ", get_hier_name(), ".body finished"})
      
      // As long as there is more data immediately available,
      // we keep looping.  If there's no data immediately available,
      // we idle the bus and release our objection to end-of-test.
      objection_to_stop.drop();

      // Note that this body loop never exits.  That doesn't matter,
      // because the test will be stopped by some other means
      // at a time when we do not object to the stop.
   end

  endtask

  /**
   * This task waits for N clock cycles
   * \param N Number of clock cycles to wait
   */
  task cycles(int N = 1);
    `ifdef NO_INTF_CALL_FROM_CLASS
      if (N >= 0) repeat(N) @(hook.drv_cb);
    `else
      hook.cycles(N);
    `endif
  endtask : cycles

  /**
  * this task gets a value from a channel, and puts it in a queue. This queue
  * will be treated by the drive() task.
  */
  task get_stim();
      M2M_Input_Trans current;
      Transaction tr;

      `M2M_REPORT_INFO({"task ", get_hier_name(), ".get_stim began"})

      do begin
          for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin : get_trans_loop
              if (!source[i].empty()) begin
                  source[i].get(tr);
                  assert ($cast(current, tr)) else

					`M2M_REPORT_ERROR({"M2M_Driver ", get_hier_name(), " got bad transaction: ", tr.psprint()})
					
                  Input_values[i].push_back(current.data);
                  Input_valid[i].push_back(current.data_valid);
              end
          end : get_trans_loop

          //wait one clock cycle before trying to get a transaction again
          cycles(1);

      end while (!sources_Empty());

      `M2M_REPORT_INFO({"task ", get_hier_name(), ".get_stim finished"})
  endtask

  /**
  * function that returns 1 if all the source channels are empty
  */
  function bit sources_Empty();
      bit all_empty = 1;

      for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin
          all_empty &= source[i].empty();
      end

      return all_empty;
  endfunction


  /**
  * function that returns 1 if all the queues are empty
  */
  function bit queues_Empty();
      bit all_empty = 1;

      for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++)
          if (Input_values[i].size() != 0)
              all_empty = 0;

      return all_empty;
  endfunction


  /**
  * this task looks if there is a least one element in each queue.  If it is, the task drive the values
  * in the queues to the corresponding interface's signals if the DUT is ready to accept new data.
  */
  task drive();

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".drive began"})

    cycles(1);

//	for (int i=1;i<= `NUMBER_OF_DUT_OUTPUTS; i++) begin
//	    hook.drv_cb.Result_Ready[i] <= 1'b0; //The testbench is always ready do get results
//	end
	
    do begin

        cycles(1);

        //if reset, put the DUT input signals to 0
        if (hook.drv_cb.reset) begin
            for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin : reset_loop
                hook.drv_cb.Input_Value[i] <= 32'b0;
                hook.drv_cb.Input_Valid[i] <= 1'b0;
            end : reset_loop
            
			for (int i=1;i<= `NUMBER_OF_DUT_OUTPUTS; i++) begin
			    hook.drv_cb.Result_Ready[i] <= 1'b1; //The testbench is always ready do get results
			end

        end else begin
        //else drive the signals to the DUT inputs only if the input is ready to accept new data and the queue is not empty
            for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin : drive_loop
                if (hook.drv_cb.Input_Ready[i] && Input_values[i].size()) begin
                    // YTA: Adding Stress factor. STRESS_FACTOR= 100 means that the design
                    // is fully stressed.
                	if ($urandom_range(1000)<=1000.0*`INPUT_FREQ/`SYSTEM_FREQ) begin
	                    logic [`M2M_BUS_SIZE-1:0] data = Input_values[i].pop_front();
	                    logic data_valid  = Input_valid[i].pop_front();
	                    hook.drv_cb.Input_Value[i] <= data;
	                    hook.drv_cb.Input_Valid[i] <= data_valid;
	                end
	                else begin
	                    hook.drv_cb.Input_Value[i] <= 32'b0;
	                    hook.drv_cb.Input_Valid[i] <= 1'b0;
	                end
                end
            end : drive_loop
            
            
        //else drive the signals to the DUT inputs only if the input is ready to accept new data and the queue is not empty
            for (int i = 1; i <= `NUMBER_OF_DUT_OUTPUTS; i++) begin : output_loop
                    // YTA: Adding Stress factor. STRESS_FACTOR= 100 means that the design
                    // is fully stressed.
                	if ($urandom_range(1000)<=1000.0*`OUTPUT_FREQ/`SYSTEM_FREQ) begin
	                    hook.drv_cb.Result_Ready[i] <= 1'b1;
	                end
	                else begin
	                    hook.drv_cb.Result_Ready[i] <= 1'b0;
	                end
            end : output_loop

            // Increment the transaction count.
            trans_count++;

        end
    end while(!queues_Empty());

   `M2M_REPORT_INFO({"task ", get_hier_name(), ".drive finished"})

  endtask : drive

  /**
  * Function permitting the testbench to ask this
  * class how many read or write transactions it has
  * completed so far.
  */
  function int get_trans_count();
    return trans_count;
  endfunction : get_trans_count

endclass : M2M_Input_Driver

//-- 20100423 v01.00 -- guo -- change -- (re)enable Input Monitor
//____________________________________________ class M2M_Input_Monitor _________
//
//
class M2M_Input_Monitor extends Component;

  // Virtual interface variable to hold a reference
  // to the test access interface that exists in our test
  // harness module
  //
  `ifdef NO_MODPORT_QUALIFICATION
    typedef virtual M2M_input_intf        M2M_in_mon_hook;
  `else
    typedef virtual M2M_input_intf.mon_mp M2M_in_mon_hook;
  `endif

  M2M_in_mon_hook hook;

  M2M_Input_Trans template;

  Channel source[1:`NUMBER_OF_DUT_INPUTS];

  function new( string          _name,
                Component       _parent,
                M2M_in_mon_hook _hook);
    super.new( _name, _parent );
    hook = _hook;
    template = new(this);
  endfunction : new

  task body();

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".body began"})

    forever @(hook.mon_cb) begin : body_loop

      for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin : input_loop
        // Collect information in the transaction object
        template.data = hook.mon_cb.Input_Value[i];
        template.data_valid = hook.mon_cb.Input_Valid[i];

        // Try to put a copy of the monitored transaction
        // on to the analysis channel.  This put attempt
        // MUST succeed in zero time - we use an assertion
        // to check that the try_put succeeded.
        if (hook.mon_cb.Input_Valid[i] && hook.mon_cb.Input_Ready[i]) begin

            assert ( source[i].try_put(template.copy()) ) begin
//              $display("************put transaction in M2M_Input_Monitor input channel, time = %t", $time);
            end
          else begin

            `M2M_REPORT_ERROR({"Channel ",
                        this.get_hier_name(),
                        ".source blocked, lost data: ",
                        template.psprint(),
                        "."})
          end

        end

      end : input_loop

    end : body_loop

  endtask : body

  // Function permitting the testbench to ask this
  // class how many transactions it has completed so far.
  //
  // function int get_trans_count();
    // return trans_count;
  // endfunction : get_trans_count

endclass : M2M_Input_Monitor

//_____________________________________________ class M2M_Input_Component ______
/**
* This class realises all necessary connections between all the input blocks
*/
class M2M_Input_Component extends Component;

  M2M_Input_Stim    stim_gen;
  M2M_Input_Driver  driver;
  M2M_Input_Monitor monitor;
  Channel           stimuli[1:`NUMBER_OF_DUT_INPUTS],
                    analysis[1:`NUMBER_OF_DUT_INPUTS];

  function new(string _name, Component _parent,
               virtual M2M_input_intf _hook);

    super.new( _name, _parent );

    for (int i = 1; i <= `NUMBER_OF_DUT_INPUTS; i++) begin
      stimuli[i]     = new(._bound(0));
    end
    stim_gen = new("stim_gen", this);
    driver   = new("driver", this, _hook);
//-- 20100507 v01.02 -- guo -- add -- add Input Monitor
    foreach (analysis[i]) begin
      analysis[i]  = new(._bound(0));
    end
    monitor  = new("monitor", this, _hook);

    driver.source = stimuli;
    stim_gen.sink = stimuli;
//-- 20100503 v01.02 -- guo -- add -- add Input Monitor
    monitor.source = analysis;

  endfunction : new

  /**
   * This task does nothing. It exists because M2M_Input_Component
   * inherites from Component, and so has to override this task.
   */
  task body();

    `M2M_REPORT_INFO({"task ", get_hier_name(), ".body"})

  endtask

endclass : M2M_Input_Component

`endif
