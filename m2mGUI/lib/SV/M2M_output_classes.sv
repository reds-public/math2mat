///////////////////////////////////////////////////////////////////////////////
// HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
// Institut REDS, Reconfigurable & Embedded Digital Systems
//
// File         : M2M_output_classes.sv
//
// Description  :
//
// Author       : S. Masle
// Date         : 19.10.2009
// Version      : 0.0
//
// Use          : testbench for Math2Mat
//
//| Modifications |////////////////////////////////////////////////////////////
// Version   Author Date               Description
//  01.00     guo   20100408            add OVM reporting
//  01.01     guo   20100429            add Reporting
///////////////////////////////////////////////////////////////////////////////

`ifndef M2M_OUT_CLASSES__SV
`define M2M_OUT_CLASSES__SV

`include "bases.sv"

// M2M_Out_Trans
// ~~~~~~~~~~~~~
/**
* Data class to represent the result of the operation.
*/
class M2M_Out_Trans extends Transaction;

  T_data Result;
  
  function new(Component owner = null);
    super.new(owner);
  endfunction : new

  function Transaction copy();
    M2M_Out_Trans clone;
    clone = new this;
    return clone;
  endfunction : copy

  function M2M_Out_Trans m2m_copy();
    M2M_Out_Trans clone;
    clone = new this;
    return clone;
  endfunction : m2m_copy

  function bit compare(Transaction other);
    M2M_Out_Trans that;
    assert ($cast(that, other)) else begin

      string id_string;
      id_string.itoa(ID);
      owner.reporting_error(
        .id({"transaction.", id_string}),
        .message({"Can't compare ", this.psprint(), " with other ", other.psprint(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
      );
    end
    
    return (bitdiff(other) <= `NUMBER_BIT_PRECISION);
  
  endfunction : compare
  
  function int bitdiff(Transaction other);
    M2M_Out_Trans that;
    
    assert ($cast(that, other)) else begin
      string id_string;
      id_string.itoa(ID);
      owner.reporting_error(
        .id({"transaction.", id_string}),
        .message({"Can't calculate bits of ", this.psprint(), " with other ", other.psprint(), "."}),
        .filename(`__FILE__),
        .line(`__LINE__)
      );
    end
   
    // Bus size is 32 bits
    if(`M2M_BUS_SIZE == 32) begin
    
      // Check the sign of the result
      if(that.Result[31:31]  != this.Result[31:31]) begin
        return 32;
      end
      
      // check the exp if it's greater than 1
      else if (abs(that.Result[30:23], this.Result[30:23]) > 1) begin
        for(int i = 23; i <= 30; i++) begin
          if (that.Result[i] != this.Result[i]) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's equal to 1
      else if (abs(that.Result[30:23], this.Result[30:23]) == 1) begin
        if(that.Result[30:23] > this.Result[30:23]) begin
          that.Result[22:0] = (2**22)-1 - that.Result[22:0];
        end 
        else begin
          this.Result[22:0] = (2**22)-1 - this.Result[22:0];
        end
        for(int i = 0; i <= 22; i++) begin
          if (that.Result[i] != this.Result[i] ) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's smaller than 1
      else if (abs(that.Result[22:0], this.Result[22:0]) > 0) begin
        for(int i = 0; i <= 22; i++) begin
          if (that.Result[i] != this.Result[i] ) begin
            return i+1;
          end
        end
      end

    // Bus size is 64 bits
    else  
      
      // Check the sign of the result
      if(that.Result[63:52]  != this.Result[63:52]) begin
        return 64;
      end
      
      // check the exp if it's greater than 1
      else if (abs(that.Result[62:52], this.Result[62:52]) > 1) begin
        for(int i = 52; i <= 62; i++) begin
          if (that.Result[i] != this.Result[i]) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's equal to 1
      else if (abs(that.Result[62:52], this.Result[62:52]) == 1) begin
        if(that.Result[62:52] > this.Result[62:52]) begin
          that.Result[51:0] = (2**51)-1 - that.Result[51:0];
        end 
        else begin
          this.Result[51:0] = (2**51)-1 - this.Result[51:0];
        end
        for(int i = 0; i <= 51; i++) begin
          if (that.Result[i] != this.Result[i] ) begin
            return i+1;
          end
        end
      end
      
      // check the mantisse if it's smaller than 1
      else if (abs(that.Result[51:0], this.Result[51:0]) > 0) begin
        for(int i = 0; i <= 51; i++) begin
          if (that.Result[i] != this.Result[i] ) begin
            return i+1;
          end
        end
      end
    end
    
    return 0;
    
  endfunction : bitdiff

  
  function int abs(int a, int b);
    if(a-b > 0)
      return a-b;
    else
      return b-a;
  endfunction : abs
 
  
  
  function string psprint();
    $sformat(psprint, "M2M_Out_Trans : Result = 0x%h = %g", Result, $bitstoshortreal(Result));
  endfunction : psprint

endclass : M2M_Out_Trans

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// M2M_Out_Monitor
// ~~~~~~~~~~~~~~~
/**
* Passive monitor BFM that can observe the output interface,
* which is simply "m2m_bus_size" data bits. Data is latched
* on the rising edge of the clocking block's clock signal.
*/
class M2M_Out_Monitor extends Component;

  //
  `ifdef NO_MODPORT_QUALIFICATION
    typedef virtual M2M_output_intf        M2M_out_mon_hook;
  `else
    typedef virtual M2M_output_intf.mon_mp M2M_out_mon_hook;
  `endif

  Channel source[1:`NUMBER_OF_DUT_OUTPUTS];
  
  // Inactivity counter
  int inactivity[1:`NUMBER_OF_DUT_OUTPUTS];
  
  int should_stop;
  
  M2M_out_mon_hook hook;

  M2M_Out_Trans template;
  
  int INACTIVITY_TIMEOUT=1000;

  function new(
      string _name, Component _parent,
      M2M_out_mon_hook _hook
  );
    super.new(_name, _parent);
    hook = _hook;
    template = new(this);
    for (int i = 1; i <= `NUMBER_OF_DUT_OUTPUTS; i++)
      inactivity[i]=0;
  endfunction : new

  task body();

    // Every time there is a rising edge on the clock
    forever @(hook.mon_cb) begin
      should_stop=1;
      for (int i = 1; i <= `NUMBER_OF_DUT_OUTPUTS; i++) begin
          if (inactivity[i]<`INACTIVITY_TIMEOUT)
              should_stop=0;
          // Collect information in the transaction object
          template.Result = hook.mon_cb.Result[i];

          // Try to put a copy of the monitored transaction
          // on to the output channel.  This put attempt
          // MUST succeed in zero time - we use an assertion
          // to check that the try_put succeeded.
          if (hook.mon_cb.Result_Valid[i] && hook.mon_cb.Result_Ready[i]) begin
              inactivity[i]=0;
              assert ( source[i].try_put(template.copy()) )
//                $display("************put transaction in M2M_Out_Monitor output channel, time = %t", $time);
              else

                `M2M_REPORT_ERROR({"Channel ",
                            this.get_hier_name(),
                            ".source blocked, lost data: ",
                            template.psprint(),
                            "."})
          end
          else
              inactivity[i]++;
          
      end
      
      
      if (should_stop) begin
      Objection objection_to_stop = new(this);
          string timeout_string;
          timeout_string.itoa(`INACTIVITY_TIMEOUT);
          `M2M_REPORT_ERROR({"Output monitor detected no activity for the last ",
                            timeout_string,
                            " clock cycles. Simulation aborted."})
        objection_to_stop.reporting_summarize();        
      	$finish;
      end
      

    end

  endtask : body

endclass : M2M_Out_Monitor

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// M2M_Out_Component
// ~~~~~~~~~~~~~~~~~
/**
* Re-usable wrapper for the output monitor BFM.It only
* contains the monitor subcomponent and an unbounded
* FIFO for the monitor's output.
*/
class M2M_Out_Component extends Component;

  // Variables for the monitor and channel instances.
  // The channel is conventionally named "analysis".
  M2M_Out_Monitor monitor;
  Channel         analysis[1:`NUMBER_OF_DUT_OUTPUTS];

  function new(string _name, Component _parent,
               virtual M2M_output_intf _hook);

    super.new( _name, _parent );

    // Create the monitor and unbounded channel.
    for (int i = 1; i <= `NUMBER_OF_DUT_OUTPUTS; i++)
      analysis[i] = new(0);
    monitor  = new("monitor", this, _hook);

    // Connect the analysis channel to the monitor BFM.
    monitor.source = analysis;

  endfunction : new

  task body();
    // Like most wrapper components, this one has
    // no useful code in its body() method.

      `M2M_REPORT_INFO({"task ", get_hier_name(), ".body"} )

  endtask

endclass : M2M_Out_Component

`endif
