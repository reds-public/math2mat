///////////////////////////////////////////////////////////////////////////////
// HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
// Institut REDS, Reconfigurable & Embedded Digital Systems
//
// File         : M2M_harness.sv
//
// Description  :
//
// Author       : S. Masle
// Date         : 19.10.2009
// Version      : 0.0
//
// Use          : testbench for Math2Mat
//
//| Modifications |////////////////////////////////////////////////////////////
// Version   Author Date               Description
//  01.02     guo   20100510            add constant clock_ticks
//  01.03     guo   20100813            add internal monitor binding into DUT
///////////////////////////////////////////////////////////////////////////////

`include "common.sv"

//define the size of the bus
typedef logic [`M2M_BUS_SIZE-1:0] T_data;

// Definition of 2 interfaces that will contain all the input and output signals
// that the testbench manipulates.
// These interfaces will be instanciated into the test harness, and referenced
// through virtual interface variables in the testbench.

/**
* Definition of the output interface. The interface defines all the signals used
* between the DUT and the monitor.
* Also contains a clocking block that will be exported in the modport.
*
* @param bit CLK The input clock used by the clocking block.
*/
interface M2M_output_intf(input bit CLK);

  //Array of logic that should be connected to the valid result signal of the DUT.
  logic  Result_Valid[1:`NUMBER_OF_DUT_INPUTS];

  //Array of T_Data that should be the DUT result signals. One entry in the array
  //per DUT result output.
  T_data Result[1:`NUMBER_OF_DUT_INPUTS];
  
  //Array of T_Data that should be the DUT result signals. One entry in the array
  //per DUT result output.
  logic Result_Ready[1:`NUMBER_OF_DUT_OUTPUTS];

  default clocking mon_cb @(posedge CLK);
    input #1step Result_Valid, Result, Result_Ready;
  endclocking : mon_cb

  modport mon_mp (clocking mon_cb);

endinterface : M2M_output_intf

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
* Definition of the input interface. The interface defines all the signals used
* between the DUT and the driver, and between the DUT and the monitor.
* Also contains two clocking block that will be exported in the modport.
*
* @param bit CLK The input clock used by the clocking block.
* @param bit rst The reset signal of the testbench.
*/
interface M2M_input_intf (input bit CLK, rst);

  // Array of T_Data that should be connected to the data inputs of the DUT.
  T_data Input_Value[1:`NUMBER_OF_DUT_INPUTS];

  // Array of logic that should be connected to the data valid signals of the DUT.
  logic  Input_Valid[1:`NUMBER_OF_DUT_INPUTS];

  // Array of logic that should be connected to the data input ready signals of the DUT.
  logic  Input_Ready[1:`NUMBER_OF_DUT_INPUTS];
  
  // Array of logic that should be connected to the data output ready signals of the DUT.
  logic  Result_Ready[1:`NUMBER_OF_DUT_OUTPUTS];
  logic  reset;

  assign reset = rst;

  // Clocking blocks to give access to all signals
  //
  clocking drv_cb @(posedge CLK);
    input  #1step Input_Ready, reset;
    output #1 Input_Value, Input_Valid, Result_Ready;
  endclocking : drv_cb

  default clocking mon_cb @(posedge CLK);
    input #1step Input_Value, Input_Valid, Input_Ready, Result_Ready, reset;
  endclocking : mon_cb

  /**
  * task to give testbench access to ##N behaviour
  */
  task automatic cycles(int N);
    repeat (N) @(mon_cb);
  endtask : cycles

  // modports to provide controlled access from testbench
  //
  modport drv_mp ( clocking drv_cb, import cycles );
  modport mon_mp ( clocking mon_cb, import cycles );

endinterface : M2M_input_intf


//-- guo -- add -- internal singnals monitoring --------------------------------

/**
 * Definition of the internal signals interface. This interface defines all the
 * signals to check inside the DUT.
 * Also contains a clocking block that will be exported in the modport.
 *
 * @param bit CLK The input clock used by the clocking block.
 */
interface M2M_internal_intf(input bit CLK);

  /**
   * Array of logic that should be connected to the valid signals inside
   * the DUT.
   */
  logic  Data_Valid[1:`NUMBER_OF_DUT_INTERNALS];

  /**
   * Array of logic that should be connected to the ready signals inside
   * the DUT.
   */
  logic  Data_Ready[1:`NUMBER_OF_DUT_INTERNALS];

  /**
   * Array of T_Data that should be the DUT internal data signals. One entry
   * in the array per DUT internal data signal to monitor.
   */
  T_data Data[1:`NUMBER_OF_DUT_INTERNALS];

  default clocking mon_cb @(posedge CLK);
    input #1step Data_Valid, Data_Ready, Data;
  endclocking : mon_cb

  modport mon_mp (clocking mon_cb);

endinterface : M2M_internal_intf


///////////////////////////////////////////////////////////
/**
* Test harness module containing instance of DUT, clock
* generator and instance of test access interface.
*/
module M2M_harness();


  /**
   * Array of logic that should be connected to the valid signals inside
   * the DUT.
   */
  logic  Internal_Data_Valid[1:`NUMBER_OF_DUT_INTERNALS];

  /**
   * Array of logic that should be connected to the ready signals inside
   * the DUT.
   */
  logic  Internal_Data_Ready[1:`NUMBER_OF_DUT_INTERNALS];

  /**
   * Array of T_Data that should be the DUT internal data signals. One entry
   * in the array per DUT internal data signal to monitor.
   */
  T_data Internal_Data[1:`NUMBER_OF_DUT_INTERNALS];
  
  bit reset;

  // clock generator
  //
  bit CLK;
//-- 20100510 v01.02 -- guo --change -- introduce constant clock_ticks
//  always #5 CLK = ~CLK;
  always #(`CLOCK_TICKS/2) CLK = ~CLK;

  initial begin
    reset = 1;
    #120;
    reset = 0;
  end

  // test access interfaces
  //
  M2M_input_intf  M2M_in_intf(CLK, reset);
  M2M_output_intf M2M_out_intf(CLK);
//-- 20100816 v01.03 -- guo --add --
  M2M_internal_intf M2M_int_intf(CLK);

  //////////////////////////////////////////////////////////////////
  //////////      GENERATED : connection of the DUT       //////////
  //////////////////////////////////////////////////////////////////
  //<instance DUT>
  
 assign M2M_out_intf.Result_Ready = M2M_in_intf.Result_Ready;

  //-- guo -- add -- internal singnals monitoring --------------------------------
  /**
   * example how internal signals are connected to the interface
   * this has to be generated like the dut instance above

  assign M2M_int_intf.mon_mp.Data[1]         = DUT.a_m2m_0;
  assign M2M_int_intf.mon_mp.Data_Ready[1]   = DUT.a_m2m_0_ready;
  assign M2M_int_intf.mon_mp.Data_Valid[1]   = DUT.a_m2m_0_valid;
   */
   
endmodule : M2M_harness

