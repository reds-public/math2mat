-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Package units : 
-- Package containing the description of the components : 
-- Add2, Mult2, Div2, Sqrt, Delay, Compare
-------------------------------------------------------------------

--***********************************************
--         PACKAGE cellule
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use work.pkg_definition.all;

package pkg_cell is
   component Mult2 is	
	generic
	   (m_g       : integer := 32; 
		pipe_g    : integer := 0;
		latency_g : integer := 0;
		type_g    : integer := 0;
		wValid_g  : positive := 1);
	port
	   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
		clk_i   : in std_logic;
		reset_i : in std_logic;
		stall_i : in std_logic;
		D1_i    : in std_logic_vector (m_g-1 downto 0);
		D2_i    : in std_logic_vector (m_g-1 downto 0); 
		m_o     : out std_logic_vector(m_g-1 downto 0);
		valid_o : out std_logic_vector(wValid_g-1 downto 0);
		ready_o : out std_logic);
	end component;

	component Add2 is	
	generic
	   (m_g       : integer := 32; 
		pipe_g    : integer := 0;
		latency_g : integer := 0;
		wValid_g  : positive := 1);
	port
	   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
		clk_i   : in std_logic;
		reset_i : in std_logic;
		stall_i : in std_logic;
		D1_i    : in std_logic_vector (m_g-1 downto 0);
		D2_i    : in std_logic_vector (m_g-1 downto 0); 
		m_o     : out std_logic_vector(m_g-1 downto 0);
		valid_o : out std_logic_vector(wValid_g-1 downto 0);
		ready_o : out std_logic);
	end component;

	component Div2 is	
	generic
	   (m_g       : integer := 32; 
		pipe_g    : integer := 0;
		latency_g : integer := 0;
		type_g    : integer := 0;
		wValid_g  : positive := 1);
	port
	   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
		clk_i   : in std_logic;
		reset_i : in std_logic;
		stall_i : in std_logic;
		D1_i    : in std_logic_vector (m_g-1 downto 0);
		D2_i    : in std_logic_vector (m_g-1 downto 0); 
		m_o     : out std_logic_vector(m_g-1 downto 0);
		valid_o : out std_logic_vector(wValid_g-1 downto 0);
		ready_o : out std_logic);
	end component;

	component Sqrt is	
	generic
	   (m_g      : integer := 32; 
		pipe_g    : integer := 0;
		latency_g : integer := 0;
		type_g    : integer := 0;
		wValid_g  : positive := 1);
	port
	   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
		clk_i   : in std_logic;
		reset_i : in std_logic;
		stall_i : in std_logic;
		D1_i    : in std_logic_vector (m_g-1 downto 0);
		m_o     : out std_logic_vector(m_g-1 downto 0);
		valid_o : out std_logic_vector(wValid_g-1 downto 0);
		ready_o : out std_logic);
	end component;

	component Delay is
	generic
	   (delay_g  : integer := 0;
		 m_g      : integer := 32;
		 wValid_g : positive := 1);
	port
   (reset_i  : in std_logic;
    clk_i    : in std_logic;
    stall_i  : in std_logic;
    m_i      : in std_logic_vector(m_g-1 downto 0);
    valid_i  : in std_logic_vector(wValid_g-1 downto 0);
    m_o      : out std_logic_vector(m_g-1 downto 0);
    valid_o  : out std_logic_vector(wValid_g-1 downto 0));
	end component;
	
   component Compare is
   generic
      (latency_g : integer := 0;
       m_g       : integer := 32;
       wValid_g  : integer := 1);
   port
      (valid_i    : in std_logic_vector(wValid_g-1 downto 0);
       reset_i    : in std_logic;
       clk_i      : in std_logic;
       stall_i    : in std_logic;
       d1_i       : in std_logic_vector(m_g-1 downto 0);
       d2_i       : in std_logic_vector(m_g-1 downto 0);
       valid_o    : out std_logic_vector(wValid_g-1 downto 0);
       ready_o    : out std_logic;
       smaller_o  : out std_logic;
       bigger_o   : out std_logic;
       equal_o    : out std_logic);
   end component Compare;	
end package pkg_cell;