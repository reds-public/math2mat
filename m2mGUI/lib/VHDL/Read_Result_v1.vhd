-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Read_Result.vhd
-- Description  : 
-- 
-- Auteur       : SMS
-- Date         : 19.04.2010
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity Read_Result is
  generic( 
    Data_Width_g : integer := 32
  );
  port(Clock_i   : in std_logic;
       Reset_i : in std_logic;
       Data_i  : in std_logic_vector(Data_Width_g-1 downto 0);
       Valid_i : in std_logic;
       Ready_i : in std_logic;
       Data_o  : out std_logic_vector(Data_Width_g-1 downto 0);
       Valid_o : out std_logic;
       Ready_o : out std_logic
  );
end Read_Result;

architecture comport of Read_Result is
    
    constant Wait_Ready_0 : std_logic := '0';
    constant Wait_Ready_1 : std_logic := '1';
    
    signal Current_State, Future_State : std_logic;
    signal Data_s : std_logic_vector(Data_Width_g-1 downto 0);
    signal Valid_s : std_logic;


begin

    Ready_o <= Ready_i;
    
    process(Ready_i, Current_State, Data_i, Valid_i, Data_s, Valid_s)
    begin
        case Current_State is
            when Wait_Ready_0 => 
				if (Ready_i = '0') then
					 Future_State <= Wait_Ready_1;
				 else
					 Future_State <= Current_State;
				 end if;
				 Data_o <= Data_i;
				 Valid_o <= Valid_i;
								 
            when Wait_Ready_1 => 
				if (Ready_i = '1') then
					Future_State <= Wait_Ready_0;
				else
					Future_State <= Current_State;
				end if;
				Data_o <= Data_s;
				Valid_o <= Valid_s;
								 
            when others => 
				Future_State <= Current_State;
        end case;
    end process;
    
    process(Clock_i, Reset_i)
    begin
        if Reset_i = '1' then
            Current_State <= Wait_Ready_0;
            Data_s <= (others => '0');
            Valid_s <= '0';
        elsif Rising_Edge(Clock_i) then
            Current_State <= Future_State;
            if Current_State = Wait_Ready_0 then
                Data_s <= Data_i;
                Valid_s <= Valid_i;
            end if;
        end if;
    end process;

end comport;