-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Cell Compare : 
-- Compare two numbers
--     - m_g    = 32 : single pr�cision
--     - m_g    = 64 : double pr�cision 
--     - latency_g   : latency time 
--     - wValid_g    : width of the signals valid_i and valid_o 
------------------------------------------------------------------

--***********************************************
--                Compare
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Compare is
generic
   (latency_g : integer := 2;
    m_g       : integer := 32;
    wValid_g  : integer := 1);
port
   (valid_i    : in std_logic_vector(wValid_g-1 downto 0);
    reset_i    : in std_logic;
    clk_i      : in std_logic;
    stall_i    : in std_logic;
    d1_i       : in std_logic_vector(m_g-1 downto 0);
    d2_i       : in std_logic_vector(m_g-1 downto 0);
    valid_o    : out std_logic_vector(wValid_g-1 downto 0);
    ready_o    : out std_logic;
    smaller_o  : out std_logic;
    bigger_o   : out std_logic;
    equal_o    : out std_logic);
end entity Compare;

architecture struct of Compare is

component Pipe is	
  generic ( w : positive := 1;
            n : natural := 0 );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic); 
end component;

signal nstall_s, ready_s : std_logic;
signal valid_s : std_logic_vector(wValid_g-1 downto 0);
signal sig1_s, sig2_s, clear_s, set_s : std_logic;
signal smaller_s : std_logic;
signal bigger_s : std_logic;
signal equal_s : std_logic;
constant wE : integer := 5 + m_g/10;
constant wF : integer := m_g-1-wE;

begin
nstall_s <= not(stall_i);

process(d1_i, d2_i)
begin
   if (d1_i(wE+wF) /= d2_i(wE+wF)) then
      if (d1_i(wE+wF) = '1') then
         smaller_s <= '1';
         bigger_s  <= '0';
         equal_s   <= '0';
      else
         smaller_s <= '0';
         bigger_s  <= '1';
         equal_s   <= '0';
      end if;
   elsif (d1_i(wE+wF-1 downto wF) > d2_i(wE+wF-1 downto wF)) then
      smaller_s <= '0';
      bigger_s  <= '1';
      equal_s   <= '0';
   elsif (d1_i(wE+wF-1 downto wF) < d2_i(wE+wF-1 downto wF)) then
      smaller_s <= '1';
      bigger_s  <= '0';
      equal_s   <= '0';
   else
      if (d1_i(wF-1 downto 0) > d2_i(wF-1 downto 0)) then
         smaller_s <= '0';
         bigger_s  <= '1';
         equal_s   <= '0';
      elsif (d1_i(wF-1 downto 0) < d2_i(wF-1 downto 0)) then
         smaller_s <= '1';
         bigger_s  <= '0';
         equal_s   <= '0';
      else
         smaller_s <= '0';
         bigger_s  <= '0';
         equal_s   <= '1';
      end if;           
   end if;    
end process;

OutputInst : Pipe
   generic map (w => 3,
                n => latency_g)
   port map (input(0)  => smaller_s, 
             input(1)  => bigger_s, 
             input(2)  => equal_s, 
             output(0) => smaller_o, 
             output(1) => bigger_o, 
             output(2) => equal_o,
             clk       => clk_i,
             en        => nstall_s,
             reset     => reset_i); 


validInst : Pipe
   generic map (w => wValid_g,
                n => latency_g)
   port map (input  => valid_i, 
             output => valid_s,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i); 

valid_o <= valid_s;

ready_comb : if latency_g = 0 generate
    -- RS flip-flop for signal ready_o with the inputs valid_i and valid_o  
    clear_s <= valid_i(0) and not(valid_s(0));
    set_s   <= valid_s(0) or reset_i;
    sig1_s  <= not(clear_s or sig2_s);
    sig2_s  <= not(set_s or sig1_s);
    ready_o <= nstall_s and sig1_s;
end generate;

ready_pipe : if latency_g > 0 generate
    ready_o <= nstall_s;
end generate;
    

end architecture struct;