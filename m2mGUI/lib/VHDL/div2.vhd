-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

----------------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--               Philippe Crausaz
--          at - 04.02.2009
--
-- Unit Div2 : 
-- Division (single and double precision). (double only for algorithm SRT4)
--     - m_g    = 32 : single pr�cision
--     - m_g    = 64 : double pr�cision 
--
--     - type_g = 0 : algorithm SRT4
--     - type_g = 1 : algorithm Array of subtractors
--     - type_g = 2 : algorithm Successive estimates
--
--     - pipe_g = 0 : combinatorial version
--     - pipe_g = 1 : pipelined version 
--
--     - latency_g  : latency time of the combinatorial version.
----------------------------------------------------------------------------

--***********************************************
--                    Div2
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Div2 is
generic
   (m_g       : integer := 32; 
    pipe_g    : integer := 0;
    latency_g : integer := 0;
    type_g    : integer := 0;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (m_g-1 downto 0);
    D2_i    : in std_logic_vector (m_g-1 downto 0); 
    m_o     : out std_logic_vector(m_g-1 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end Div2;

architecture struct of Div2 is

constant wE : integer := 5 + m_g/10; -- exponent width (only for m_g = 32 or 64)
constant wF : integer := m_g-1-wE;   -- mantissa(fraction) width
type Ttypeconst is array (0 to 2) of integer;
constant typeconst : Ttypeconst := ((wF+6)/2+2,30,1); -- delay for the different pipelined version 
                                                      -- (0: SRT4, 1: Array sub., 2: Succ. approx.) 

signal m_s    : std_logic_vector(m_g-1 downto 0);
signal nstall_s : std_logic;
signal valid_s : std_logic_vector(wValid_g-1 downto 0);
signal sig1_s, sig2_s, clear_s, set_s : std_logic;

component DivFP_0 is
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    b_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component DivFP_1 is
generic
   (m_g    : integer := 32;
    pipe_g : integer := 0;
    type_g : integer := 1);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    b_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

begin  
nstall_s <= not(stall_i); -- nstall_s = enable

SRT4_ALGO : if type_g = 0 generate           
div_inst : DivFP_0
generic map
   (wF => wF,
    wE => wE,
    pipe_g => pipe_g)
port map
   (clk_i => clk_i,
    reset_i => reset_i,
    en_i => nstall_s,
    a_i => D1_i,
    b_i => D2_i,
    y_o => m_s
    ); 
end generate SRT4_ALGO;

xxx_ALGO : if type_g > 0 generate
div_inst : DivFP_1
generic map
   (m_g    => m_g,
    pipe_g => pipe_g,
    type_g => type_g)
port map
   (clk_i => clk_i,
    reset_i => reset_i,
    en_i => nstall_s,
    a_i => D1_i,
    b_i => D2_i,
    y_o => m_s
    ); 
end generate xxx_ALGO;

-- combinatorial version : output register and delay for the signal "valid_i"
combGen : if pipe_g = 0 generate 
mInst : Pipe
   generic map (w => m_g,
                n => 1)  -- output register for combinatorial version
   port map (input  => m_s, 
             output => m_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);

validInst : Pipe
   generic map (w => wValid_g,
                n => latency_g) -- valid_o after latency time
   port map (input  => valid_i, 
             output => valid_s,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i); 
 
valid_o <= valid_s;

-- RS flip-flop for signal ready_o with the inputs valid_i and valid_o             
clear_s <= valid_i(0) and not(valid_s(0));
set_s   <= valid_s(0) or reset_i;
sig1_s  <= not(clear_s or sig2_s);
sig2_s  <= not(set_s or sig1_s);
-- YTA: modified this to comply with the pipeline version
--ready_o <= nstall_s and sig1_s;             
ready_o <= nstall_s;                  
end generate combGen;

-- pipelined version : delay for signal valid_i
pipeGen : if (latency_g = 0 and pipe_g > 0) generate
PipeInst : Pipe
   generic map (w => wValid_g,
                n => typeconst(type_g))
   port map (input  => valid_i, 
             output => valid_o,
             clk       => clk_i,
             en        => nstall_s,
             reset     => reset_i);
   m_o <= m_s;
   ready_o <= nstall_s;   
end generate pipeGen;
end struct;

--***********************************************
--               ALGORITHME SRT4
--***********************************************
--                    DivFP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity DivFP_0 is
generic
   (wE    : integer := 8;
    wF    : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(wE+wF downto 0);
    b_i     : in std_logic_vector(wE+wF downto 0);
    y_o     : out std_logic_vector(wE+wF downto 0));
end entity DivFP_0;

architecture compute of DivFP_0 is

component FPDiv_Division_Clk is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF downto 0);
         fB  : in  std_logic_vector(wF downto 0);
         fR  : out std_logic_vector(wF+3 downto 0);
         clk : in  std_logic;
         en  : in  std_logic);
end component;

component FPDiv_Division is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF downto 0);
         fB  : in  std_logic_vector(wF downto 0);
         fR  : out std_logic_vector(wF+3 downto 0) );
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

-- I/Os signals (sign, exponent, mantissa)
signal a_s      : std_logic_vector(wE+wF downto 0);
signal b_s      : std_logic_vector(wE+wF downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(wE downto 0);
signal a_mant_s : std_logic_vector(wF downto 0);
signal b_sign_s : std_logic;
signal b_exp_s  : unsigned(wE downto 0);
signal b_mant_s : std_logic_vector(wF downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(wE downto 0);
signal y_mant_s : unsigned(wF+1 downto 0);

-- signals for computation
signal y_mant_calc_s    : std_logic_vector(wF+3 downto 0);
signal y_exp_calc_s     : unsigned(wE downto 0);
signal exceptions_s     : std_logic_vector(2 downto 0); -- NaN, infinite, 0
signal y_s              : std_logic_vector(wE+wF downto 0);
signal y_mant_norm_s    : std_logic_vector(wF+1 downto 0);
signal exp_max_s        : std_logic;
signal exp_min_s        : std_logic;
signal exceptions_out_s : std_logic_vector(2 downto 0);

-- signals for the pipeline
signal y_exp_calc_reg_s : std_logic_vector(wE downto 0);
signal exceptions_reg_s : std_logic_vector(2 downto 0);
signal y_sign_reg_s     : std_logic;
signal y_exp_reg_s      : unsigned(wE downto 0);
signal y_mant_norm_reg_s : std_logic_vector(wF+1 downto 0);

begin	

-- separation of the signals : sign, exponent, mantissa
-- exponent on 9 bits  : carry(1) & exp(8)
-- mantissa on 24 bits : hidden(1) & mant(23)
a_sign_s <= a_s(wE+wF);
a_exp_s  <= '0' & unsigned(a_s(wE+wF-1 downto wF));
a_mant_s <= '1' & a_s(wF-1 downto 0);
b_sign_s <= b_s(wE+wF);
b_exp_s  <= '0' & unsigned(b_s(wE+wF-1 downto wF));
b_mant_s <= '1' & b_s(wF-1 downto 0);

y_sign_s <= a_sign_s xor b_sign_s;
y_exp_calc_s  <= a_exp_s - b_exp_s;

-- test if input signals are exceptions 
exceptions_input : process(a_exp_s, b_exp_s, a_mant_s, b_mant_s)
begin
   if ((a_exp_s = '0'&(wE-1 downto 0 => '1') and a_mant_s > '1'&(wF-1 downto 0 => '0')) or 
       (b_exp_s = '0'&(wE-1 downto 0 => '1') and b_mant_s > '1'&(wF-1 downto 0 => '0'))) then
      exceptions_s <= "100"; -- a or b = NaN, y = NaN
   elsif (b_exp_s = (wE downto 0 => '0')) then -- b = 0, y = NaN
      exceptions_s <= "100";   -- number/0 = NaN
   elsif (a_exp_s = (wE downto 0 => '0')) then
      exceptions_s <= "001"; -- 0/number = 0
   elsif (b_exp_s = '0'&(wE-1 downto 0 => '1')) then
      if (a_exp_s = '0'&(wE-1 downto 0 => '1')) then
         exceptions_s <= "100"; -- infinity/infinity = NaN
      else
         exceptions_s <= "001"; -- number/infinity = 0
      end if;
   elsif (a_exp_s = '0'&(wE-1 downto 0 => '1')) then
      exceptions_s <= "010"; -- infinity/number = infinity
   else
      exceptions_s <= "000";       
   end if;
end process; 

-- combinatorial version of the division
combinatorial : if pipe_g = 0 generate
   a_s <= a_i;
   b_s <= b_i;

   div_FP : FPDiv_Division
   generic map( wF => wF)
   port map(fA  => a_mant_s,
            fB  => b_mant_s,
            fR  => y_mant_calc_s); 
           
   y_sign_reg_s       <= y_sign_s;
   y_exp_calc_reg_s   <= std_logic_vector(y_exp_calc_s);
   y_mant_norm_reg_s  <= y_mant_norm_s;
   y_exp_reg_s        <= y_exp_s;
   exceptions_reg_s   <= exceptions_s;

   y_o <= y_s;
end generate combinatorial;

-- pipelined version of the division
pipeline : if pipe_g > 0 generate
   input_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   a_s  <= (others => '0');
		   b_s  <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
		      a_s  <= a_i;
		      b_s  <= b_i;
		   end if;
      end if;
   end process;

   div_FP : FPDiv_Division_Clk
   generic map( wF => wF)
   port map(fA  => a_mant_s,
            fB  => b_mant_s,
            fR  => y_mant_calc_s,
            clk => clk_i,
            en  => en_i);

-- number of pipeline registers is dependent on the number of bits of the mantissa
   sign_pipe : Pipe
      generic map (w => 1,
                   n => (wF+6)/2)
      port map (input(0)  => y_sign_s, 
                output(0) => y_sign_reg_s,
                clk       => clk_i,
                en        => en_i,
                reset     => reset_i);
             
   exp_pipe : Pipe
      generic map (w => wE+1,
                   n => (wF+6)/2-1)
      port map (input  => std_logic_vector(y_exp_calc_s), 
                output => y_exp_calc_reg_s,
                clk    => clk_i,
                en        => en_i,
                reset     => reset_i);
             
   exceptions_pipe : Pipe
      generic map (w => 3,
                   n => (wF+6)/2)
      port map (input  => exceptions_s, 
                output => exceptions_reg_s,
                clk    => clk_i,
                en        => en_i,
                reset     => reset_i);            
             
   signaux_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
         y_mant_norm_reg_s <= (others => '0');
         y_exp_reg_s       <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
            y_mant_norm_reg_s <= y_mant_norm_s;
            y_exp_reg_s       <= y_exp_s;
         end if;
      end if;
   end process; 
   
   output_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   y_o <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
		      y_o <= y_s;
		   end if;
      end if;
   end process;         
end generate pipeline;

-- normalization of the result
norm : process(y_mant_calc_s,y_exp_calc_reg_s)
begin
   if (y_mant_calc_s(wF+3) = '1') then
      y_mant_norm_s <= y_mant_calc_s(wF+2 downto 1);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) + ("00"&(wE-2 downto 0 => '1'));
   else
      y_mant_norm_s <= y_mant_calc_s(wF+1 downto 0);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) + ("00"&(wE-2 downto 1 => '1')&'0');    
   end if;
end process norm;

-- round to nearest
round : process(y_mant_norm_reg_s)
begin
   if (y_mant_norm_reg_s(1)) = '1' then
      y_mant_s <= unsigned(y_mant_norm_reg_s) + "1";
   else
      y_mant_s <= unsigned(y_mant_norm_reg_s);
   end if;
end process round;  
 
exp_max_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '1') else '0'; -- exponent max if infinity
exp_min_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '0') else '0'; -- exponent min if zero

-- limit the result in the bounds of the single or double precision
exceptions_output : process(y_exp_reg_s, exp_max_s, exp_min_s, exceptions_reg_s)
begin
   if (exceptions_reg_s = "000") then
      if (((exp_max_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and not y_exp_reg_s(wE-1))) = '1') then
         exceptions_out_s <= "010";
      elsif (((exp_min_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and y_exp_reg_s(wE-1) )) = '1') then
         exceptions_out_s <= "001";
      else
         exceptions_out_s <= exceptions_reg_s;
      end if;
   else
      exceptions_out_s <= exceptions_reg_s;
   end if;
end process;

-- output multiplex : result of computation or exception
process (exceptions_out_s, y_sign_reg_s, y_exp_reg_s, y_mant_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & (wE+wF-1 downto 0 => '0'); -- zero
      when "010"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 0 => '0'); -- infinity
      when "100"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 1 => '0') & '1'; -- NaN
      when others => y_s <= std_logic_vector(y_sign_reg_s & y_exp_reg_s(wE-1 downto 0) & y_mant_s(wF+1 downto 2));
   end case;
end process;  
    
end compute;

--***********************************************
--               FPDiv_division
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FPDiv_Division is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF downto 0);
         fB  : in  std_logic_vector(wF downto 0);
         fR  : out std_logic_vector(wF+3 downto 0) );
end entity;

architecture arch of FPDiv_Division is
    
component FPDiv_SRT4_Step is
  generic ( wF : positive );
  port ( x  : in  std_logic_vector(wF+2 downto 0);
         d  : in  std_logic_vector(wF downto 0);
         d3 : in  std_logic_vector(wF+2 downto 0);
         q  : out std_logic_vector(2 downto 0);
         w  : out std_logic_vector(wF+2 downto 0) );
end component;
    
  constant nDigit : positive := (wF+6)/2;

  signal fB3 : std_logic_vector(wF+2 downto 0);
  signal w  : std_logic_vector(nDigit*(wF+3)-1 downto 0);
  signal q  : std_logic_vector(nDigit*3-1 downto 0);
  signal qP : std_logic_vector(2*nDigit-1 downto 0);
  signal qM : std_logic_vector(2*nDigit downto 1);

  signal fR0 : std_logic_vector(2*nDigit-1 downto 0);
begin
  fB3 <= ("00" & fB) + ("0" & fB & "0");

  w(nDigit*(wF+3)-1 downto (nDigit-1)*(wF+3)) <= "00" & fA;
  srt : for i in nDigit-1 downto 1 generate
    step : FPDiv_SRT4_Step
      generic map ( wF => wF )
      port map ( x  => w((i+1)*(wF+3)-1 downto i*(wF+3)),
                 d  => fB,
                 d3 => fB3,
                 q  => q((i+1)*3-1 downto i*3),
                 w  => w(i*(wF+3)-1 downto (i-1)*(wF+3)) );
    qP(2*i+1 downto 2*i)   <= q(i*3+1 downto i*3);
    qM(2*i+2 downto 2*i+1) <= q(i*3+2) & "0";
  end generate;

  q(2 downto 0) <= "000" when w(wF+2 downto 0) = (wF+1 downto 0 => '0') else
                   w(wF+2) & "10";
  qP(1 downto 0) <= q(1 downto 0);
  qM(2 downto 1) <= q(2) & "0";

  fR0 <= qP - (qM(2*nDigit-1 downto 1) & "0");

  round_odd : if wF mod 2 = 1 generate
    fR <= fR0(2*nDigit-1 downto 1);
  end generate;
  round_even : if wF mod 2 = 0 generate
    fR <= fR0(2*nDigit-1 downto 3) & (fR0(2) or fR0(1));
  end generate;
end architecture;

--***********************************************
--               FPDiv_division_clk
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FPDiv_Division_Clk is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF downto 0);
         fB  : in  std_logic_vector(wF downto 0);
         fR  : out std_logic_vector(wF+3 downto 0);
         clk : in  std_logic;
         en  : in  std_logic);
end entity;

architecture arch of FPDiv_Division_Clk is
    
component FPDiv_SRT4_Step is
  generic ( wF : positive );
  port ( x  : in  std_logic_vector(wF+2 downto 0);
         d  : in  std_logic_vector(wF downto 0);
         d3 : in  std_logic_vector(wF+2 downto 0);
         q  : out std_logic_vector(2 downto 0);
         w  : out std_logic_vector(wF+2 downto 0) );
end component;
    
  constant nDigit : positive := (wF+6)/2;

  signal fB1 : std_logic_vector(nDigit*(wF+1) downto 0);
  signal fB3 : std_logic_vector(nDigit*(wF+3) downto 0);
  signal x  : std_logic_vector(nDigit*(wF+3)-1 downto 0);
  signal w  : std_logic_vector((nDigit-1)*(wF+3)-1 downto 0);
  signal q  : std_logic_vector(nDigit*3-1 downto 0);
  signal qP : std_logic_vector(nDigit*(nDigit+1)-1 downto 0);
  signal qM : std_logic_vector(nDigit*(nDigit+1) downto 1);

  signal fR0 : std_logic_vector(2*nDigit-1 downto 0);

begin
  fB1(nDigit*(wF+1)-1 downto (nDigit-1)*(wF+1)) <= fB;
  fB3(nDigit*(wF+3)-1 downto (nDigit-1)*(wF+3)) <= ("00" & fB) + ("0" & fB & "0");
  
  x(nDigit*(wF+3)-1 downto (nDigit-1)*(wF+3)) <= "00" & fA;
  srt : for i in nDigit-1 downto 1 generate
    step : FPDiv_SRT4_Step
      generic map ( wF => wF )
      port map ( x  => x((i+1)*(wF+3)-1 downto i*(wF+3)),
                 d  => fB1((i+1)*(wF+1)-1 downto i*(wF+1)),
                 d3 => fB3((i+1)*(wF+3)-1 downto i*(wF+3)),
                 q  => q(i*3+2 downto i*3),
                 w  => w(i*(wF+3)-1 downto (i-1)*(wF+3)) );
    qP((nDigit-i-1)*(nDigit-i)+1 downto (nDigit-i-1)*(nDigit-i))   <= q(i*3+1 downto i*3);
    qM((nDigit-i-1)*(nDigit-i)+2 downto (nDigit-i-1)*(nDigit-i)+1) <= q(i*3+2) & "0";

    process(clk)
    begin
      if clk'event and clk='1' then
         if en = '1' then
            fB1(i*(wF+1)-1 downto (i-1)*(wF+1)) <= fB1((i+1)*(wF+1)-1 downto i*(wF+1));
            fB3(i*(wF+3)-1 downto (i-1)*(wF+3)) <= fB3((i+1)*(wF+3)-1 downto i*(wF+3));
            qP((nDigit-i+1)*(nDigit-i+2)-1 downto (nDigit-i)*(nDigit-i+1)+2) <=
              qP((nDigit-i)*(nDigit-i+1)-1 downto (nDigit-i-1)*(nDigit-i));
            qM((nDigit-i+1)*(nDigit-i+2) downto (nDigit-i)*(nDigit-i+1)+3) <=
              qM((nDigit-i)*(nDigit-i+1) downto (nDigit-i-1)*(nDigit-i)+1);
            x(i*(wF+3)-1 downto (i-1)*(wF+3)) <= w(i*(wF+3)-1 downto (i-1)*(wF+3));
         end if;
      end if;
    end process;
    
  end generate;

  q(2 downto 0) <= "000" when x(wF+2 downto 0) = (wF+1 downto 0 => '0') else
                   x(wF+2) & "10";
  qP((nDigit-1)*nDigit+1 downto (nDigit-1)*nDigit)   <= q(1 downto 0);
  qM((nDigit-1)*nDigit+2 downto (nDigit-1)*nDigit+1) <= q(2) & "0";

  fR0 <=  qP(nDigit*(nDigit+1)-1 downto (nDigit-1)*nDigit) -
         (qM(nDigit*(nDigit+1)-1 downto (nDigit-1)*nDigit+1) & "0");

  round_odd : if wF mod 2 = 1 generate
    fR <= fR0(2*nDigit-1 downto 1);
  end generate;
  round_even : if wF mod 2 = 0 generate
    fR <= fR0(2*nDigit-1 downto 3) & (fR0(2) or fR0(1));
  end generate;

end architecture;

--***********************************************
--               FPDiv_SRT4_Step
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity FPDiv_SRT4_Step is
  generic ( wF : positive );
  port ( x  : in  std_logic_vector(wF+2 downto 0);
         d  : in  std_logic_vector(wF downto 0);
         d3 : in  std_logic_vector(wF+2 downto 0);
         q  : out std_logic_vector(2 downto 0);
         w  : out std_logic_vector(wF+2 downto 0) );
end entity;

architecture arch of FPDiv_SRT4_Step is
  signal sel : std_logic_vector(4 downto 0);
  signal q0 : std_logic_vector(2 downto 0);
  signal qd : std_logic_vector(wF+3 downto 0);
  signal x0 : std_logic_vector(wF+3 downto 0);
  signal w0 : std_logic_vector(wF+4 downto 1);
begin
  sel <= x(wF+2 downto wF-1) & d(wF-1);
  with sel select
    q0 <= "001" when "00010" | "00011",
          "010" when "00100" | "00101" | "00111",
          "011" when "00110" | "01000" | "01001" | "01010" | "01011" | "01101" | "01111",
          "101" when "11000" | "10110" | "10111" | "10100" | "10101" | "10011" | "10001",
          "110" when "11010" | "11011" | "11001",
          "111" when "11100" | "11101",
          "000" when "00000" | "00001" | "11110" | "11111",
          "---" when others;

  with q0 select
    qd <= "000" & d                 when "001" | "111",
          "00" & d & "0"            when "010" | "110",
          "0" & d3                  when "011" | "101",
          (wF+3 downto 0 => '0') when "000",
          (wF+3 downto 0 => '-') when others;

  x0 <= x & "0";
  with q0(2) select
    w0 <= x0 - qd when '0',
          x0 + qd when others;

  q <= q0;
  w <= w0(wF+2 downto 1) & "0";
end architecture;

--***********************************************
--             OTHER ALGORITHMS
--***********************************************

--***********************************************
--               Division FP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
--use work.definition.all;

entity DivFP_1 is
generic
   (m_g    : integer := 32;
    pipe_g : integer := 1;
	 type_g : integer := 2);
port
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    b_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end entity DivFP_1;

architecture comp of DivFP_1 is

component divintcomb is
   generic  ( AN : integer := 20;
              BN : integer := 4;
				  pipe_g : integer :=0);
   port ( A : IN unsigned(AN-1 downto 0);
          B : IN unsigned(BN-1 downto 0);
			 clk_i : std_logic;
			 reset_i : std_logic;
			 en_i  : std_logic;
		  Q : OUT unsigned(AN-BN-1 downto 0);
		  OVF : OUT std_logic;
		  remainder_o : out unsigned(BN downto 0));
end component divintcomb;

component ZsurX is
generic ( m_g : integer := 24);
PORT(clk_i,reset_i: IN std_logic;
     B_i : IN unsigned (m_g-1 downto 0);
	  A_i : IN unsigned((2*m_g)-1 downto 0);
     Y_o : OUT unsigned (m_g-1 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in std_logic );
end component;

type Tpipeconst is array (0 to 4) of integer;
constant pipeconst : Tpipeconst := (0,28,1,58,1);
signal a : std_logic_vector(m_g-1 downto 0);
signal b : std_logic_vector(m_g-1 downto 0);
signal GND : unsigned(23 downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(8 downto 0);
signal a_mant_s : unsigned(23 downto 0);
signal a_mant_48_s : unsigned(47 downto 0);

signal b_sign_s : std_logic;
signal b_exp_s  : unsigned(8 downto 0);
signal b_mant_s : unsigned(23 downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(7 downto 0);
signal y_mant_s : unsigned(22 downto 0);

signal y_exp_calc_s   : unsigned(8 downto 0);
signal y_mant_calc_s  : unsigned(23 downto 0);
signal y_mant_round_s : unsigned(23 downto 0);

signal y_sign_reg_s : std_logic;
signal y_exp_calc_reg_s : std_logic_vector(8 downto 0);
signal y_s : std_logic_vector(31 downto 0);
signal exceptions_s : unsigned(2 downto 0); -- NaN, infini, 0
signal exceptions_reg_s : std_logic_vector(2 downto 0);
signal a_eq_0, b_eq_0, a_eq_inf, b_eq_inf, a_eq_NaN, b_eq_NaN : std_logic;
signal exceptions_out_s : unsigned(2 downto 0);
signal exp_max_s,exp_min_s : std_logic;
signal remainder : unsigned(24 downto 0);
signal zsurx_y_r : std_logic;
signal ovf : std_logic;

begin 

GND <= (others => '0');

--------------------------------------------------------------------------
-- input register for a_i and b_i                                       --
--------------------------------------------------------------------------
input_register : if pipe_g/=0 generate
process (clk_i,reset_i)
begin
if reset_i='1' then
   a <= (others => '0');
   b <= (others => '0');
elsif rising_edge(clk_i) then
   if en_i = '1' then
      a <= a_i;
      b <= b_i;
   end if;      
end if;
end process;
end generate;
input_comb : if pipe_g=0 generate
   a <= a_i;
   b <= b_i;
end generate;
   
---------------------------------------------------------------------------
-- separation of signals : sign, exponent, mantissa                      --
---------------------------------------------------------------------------
a_sign_s <= a(31);
a_exp_s  <= '0' & unsigned(a(30 downto 23)); -- on travaille avec 1 bit de plus
a_mant_s <= '1' & unsigned(a(22 downto 0)); -- ajoute le bit cach�
b_sign_s <= b(31);
b_exp_s  <= '0' & unsigned(b(30 downto 23));
b_mant_s <= '1' & unsigned(b(22 downto 0));

---------------------------------------------------------------------------
-- correction of the mantissa of a (multiplied by 2**23)             --
---------------------------------------------------------------------------
a_mant_48_s <= GND(23 downto 23) & a_mant_s & GND(22 downto 0);

---------------------------------------------------------------------------
-- exceptions handling input                                              --
---------------------------------------------------------------------------

-- A         expA    B      ExpB   Result
------------------------------------------------
-- 0       	 0       0      0      NaN
-- A         value   0      0      NaN
-- infini    255     0      0      NaN
-- 0         0       B      value  0
-- A         value   B      value  result
-- infini    255     B      value  infini
-- 0         0       infini 255    0
-- A         value   infini 255    0
-- infini    255     infini 255    NaN
-- NaN       255	 B      value  NaN
-- A         value   NaN    255    NaN

-- coding numbers :
--         exp   mant
-- infini  255   0
-- NaN     255   <>0   (Not A Number)
-- 0       0     0
-- d�norm  0     <>0

-- A and B are normalized
A_eq_0 <= '1' when a_exp_s="000000000" else '0';
A_eq_inf <= '1' when ((a_exp_s="011111111") and (a_mant_s= "100000000000000000000000")) else '0';
--A_eq_NaN <= '1' when ((a_exp_s="011111111") and (a_eq_inf='0')) else '0';
A_eq_NaN <= '1' when ((a_exp_s="011111111") and (a_mant_s/= "100000000000000000000000")) else '0';

B_eq_0 <= '1' when b_exp_s="000000000" else '0';
B_eq_inf <=  '1' when ((b_exp_s="011111111") and (b_mant_s= "100000000000000000000000")) else '0';
--B_eq_NaN <= '1' when ((b_exp_s="011111111") and (b_eq_inf='0')) else '0';
B_eq_NaN <= '1' when ((b_exp_s="011111111") and (b_mant_s/= "100000000000000000000000")) else '0';


exceptions_input : process(a_eq_0, b_eq_0, a_eq_inf, b_eq_inf, a_eq_NaN, b_eq_NaN)
begin
if ((B_eq_NaN='1') or (A_eq_NaN='1')) then
   exceptions_s <= "100"; --NaN
   elsif b_eq_0='1' then -- B=0 ==> division par 0
      exceptions_s <= "100"; -- NaN
   elsif b_eq_inf='1' then -- B = infini
      if a_eq_inf='1' then -- A = infini
         exceptions_s <= "100"; -- NaN
      else
         exceptions_s <= "001"; --0
      end if;
   else -- B is a number
      if a_eq_0='1' then
          exceptions_s <= "001"; -- 0
      elsif a_eq_inf='1' then
          exceptions_s <= "010"; -- infini
      else
          exceptions_s <= "000"; -- pas d'exception
      end if;
   end if;   
   end process;
     
---------------------------------------------------------------------------
-- computation of sign and exponent of the result                        --
---------------------------------------------------------------------------
y_sign_s <= a_sign_s xor b_sign_s;
y_exp_calc_s  <= a_exp_s - b_exp_s + "1111111";
   
   
---------------------------------------------------------------------------			
-- mantissa divider                                                      --
---------------------------------------------------------------------------

type_1 : if type_g=1 generate
    divintcomb_inst : divintcomb
	generic map (AN=>48,
				 BN=>24,
				 pipe_g => pipe_g)
     port map ( A =>a_mant_48_s,
				B =>b_mant_s,
				Q =>y_mant_calc_s,
				clk_i => clk_i,
				reset_i => reset_i,
				en_i => en_i,
				OVF => ovf,
				remainder_o => remainder); 
end generate;

type_2 : if type_g=2 generate
       zsurx_inst : zsurx
	generic map (m_g=>24)
     port map ( A_i =>a_mant_48_s,
				B_i =>b_mant_s,
				Y_o =>y_mant_calc_s,
				clk_i => clk_i,
				reset_i => reset_i); 

end generate;				
   
----------------------------------------------------------------------------
-- Rounding (in bloc divintcomb)              --
----------------------------------------------------------------------------
y_mant_round_s <= y_mant_calc_s;   
   
---------------------------------------------------------------------------
--pipe for sign and exponent of the result, and exception               --
---------------------------------------------------------------------------
-- signe
sign_pipe : Pipe
  generic map (w => 1,
               n => pipeconst(pipe_g))
  port map (input(0)  => y_sign_s, 
            output(0) => y_sign_reg_s,
            clk       => clk_i,
            en        => en_i,
			reset     => reset_i);
             
-- exponent			 
exp_pipe : Pipe
  generic map (w => 9,
               n => pipeconst(pipe_g))
  port map (input  => std_logic_vector(y_exp_calc_s), 
            output => y_exp_calc_reg_s,
            clk    => clk_i,
            en     => en_i,
			reset  => reset_i);     
			
--exceptions
exc_pipe : Pipe
  generic map (w => 3,
               n => pipeconst(pipe_g))
  port map (input  => std_logic_vector(exceptions_s), 
            output => exceptions_reg_s,
            clk    => clk_i,
            en     => en_i,
			reset  => reset_i);     
		
----------------------------------------------------------------------------
-- normalisation                                                          --
----------------------------------------------------------------------------
norm : process(y_mant_round_s,y_exp_calc_reg_s,ovf,remainder,b_mant_s)
begin
   if ovf='1' then
      y_mant_s <= y_mant_round_s(23 downto 1);
      y_exp_s  <= unsigned(y_exp_calc_reg_s(7 downto 0))+1;
   elsif (y_mant_round_s(23) = '1') then
      -- il est normalis�
      y_mant_s <= y_mant_round_s(22 downto 0); -- removal of hidden bit
      y_exp_s  <= unsigned(y_exp_calc_reg_s(7 downto 0)); -- exponent on 8 bits
   else
      if (remainder*2 >= b_mant_s/2) then
         y_mant_s <= y_mant_round_s(21 downto 0) & '1';
      else
         y_mant_s <= y_mant_round_s(21 downto 0) & '0';
      end if;
      y_exp_s  <= unsigned(y_exp_calc_reg_s(7 downto 0))-1;      
   end if;
end process norm;			
			
			
---------------------------------------------------------------------------
-- output exceptions                                                     --
---------------------------------------------------------------------------   
exp_max_s <= '1' when y_exp_calc_reg_s(7 downto 0) = "11111111" else '0';
exp_min_s <= '1' when y_exp_calc_reg_s(7 downto 0) = "00000000" else '0';
  
exceptions_output : process(y_exp_calc_reg_s, exp_max_s, exp_min_s, exceptions_reg_s)
begin
if (exceptions_reg_s = "000") then  -- pas d'exception d'entree
if (((exp_max_s and not y_exp_calc_reg_s(8)) or (y_exp_calc_reg_s(8) and not y_exp_calc_reg_s(7))) = '1') then
exceptions_out_s <= "010";
elsif (((exp_min_s and not y_exp_calc_reg_s(8)) or (y_exp_calc_reg_s(8) and y_exp_calc_reg_s(7) )) = '1') then
exceptions_out_s <= "001";
else
exceptions_out_s <= exceptions_s;
end if;
else
exceptions_out_s <= exceptions_s;
end if;
end process;   

---------------------------------------------------------------------------
-- final register + output exceptions handling                           --
---------------------------------------------------------------------------
process (exceptions_out_s, y_sign_reg_s, y_exp_s, y_mant_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & "0000000000000000000000000000000";
      when "010"  => y_s <= y_sign_reg_s & "1111111100000000000000000000000";
      when "100"  => y_s <= y_sign_reg_s & "1111111100000000000000000000001";
      when others => y_s <= std_logic_vector(y_sign_reg_s & std_logic_vector(y_exp_s) & std_logic_vector(y_mant_s));
   end case;
end process;

output_register : if pipe_g/=0 generate
process (clk_i,reset_i)
begin
   if reset_i = '1' then
      y_o <= (others => '0');
   elsif rising_edge(clk_i) then
      if en_i = '1' then
         y_o <= y_s;
      end if;
   end if;
end process;
end generate;
output_comb : if pipe_g=0 generate
   y_o <= y_s;
end generate;
end architecture comp;

--***********************************************
--          ARRAY OF SOUSTRACTEURS
--***********************************************
--***********************************************
--               Divintcomb
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

---------------------------------------------------------------
-- sub1bit                                                   --
---------------------------------------------------------------

entity sub1bit is
generic (pipe_g : integer := 0);
port (clk_i : IN std_logic;
      reset_i: IN std_logic;
      en_i : in std_logic;
	  D,S : IN std_logic;
      BI : IN std_logic;
	  N : IN std_logic;
	  BO : OUT std_logic;
	  R : OUT std_logic);
end entity sub1bit;

architecture impl of sub1bit is
begin
no_ff : if pipe_g/=3 generate
   R <= ((D xor S xor BI) and not N) or (D and N);
end generate;

ff : if pipe_g=3 generate
   process (clk_i,reset_i)
   begin
   if reset_i='1' then
      R <= '0';
   elsif rising_edge(clk_i) then
      if en_i = '1' then
         R <= ((D xor S xor BI) and not N) or (D and N);
      end if;
   end if;
end process;
end generate;
   
BO <= (not D and S) or (not D and BI) or (S and BI);
end architecture;

-----------------------------------------------------------------
-- divintcomb                                                  --
-----------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity divintcomb is
   generic  ( AN : integer := 8;
              BN : integer := 4;
			  pipe_g : integer := 4);
   port ( A : IN unsigned(AN-1 downto 0);
          B : IN unsigned(BN-1 downto 0);
		  clk_i : IN std_logic;
		  reset_i : IN std_logic;
		  en_i : in std_logic;
		  Q : OUT unsigned(AN-BN-1 downto 0);
		  OVF : OUT std_logic;
		  remainder_o : out unsigned(BN downto 0));
end entity divintcomb;

architecture impl of divintcomb is

component sub1bit is
generic (pipe_g : integer);
port (clk_i: IN std_logic;
      reset_i : IN std_logic;
      en_i  : in std_logic;
	  D,S : IN std_logic;
      BI : IN std_logic;
	  N : IN std_logic;
	  BO : OUT std_logic;
	  R : OUT std_logic);
end component;

type TBI is array (0 to AN-BN) of unsigned(BN+1 downto 0);
type TR is array (0 to AN-BN+1) of unsigned(BN+1 downto 0);
--type Tareg is array ( 0 to AN-BN+3) of unsigned(AN-1 downto 0);
--type Tbreg is array ( 0 to AN-BN+3) of unsigned(BN downto 0);
type Tareg is array ( 0 to 2*(AN-BN)+3) of unsigned(AN-1 downto 0);
type Tbreg is array ( 0 to 2*(AN-BN)+3) of unsigned(BN downto 0);
--type Tqint is array (0 to AN-BN+1) of unsigned(AN-BN downto 0);
type Tqint is array (0 to 2*(AN-BN)+1) of unsigned(AN-BN downto 0);
type Tpipeconst is array (0 to 4) of integer;

constant pipeconst : Tpipeconst := (0,1,0,2,2);

signal B_1 : unsigned(BN downto 0);
signal BI_1,BI_1_reg : TBI;
signal R_1,R_1_reg : TR;
signal Q_int : unsigned (AN-BN downto 0);
signal Q_pipe : unsigned (AN-BN downto 0);
signal remainder : unsigned (BN downto 0);
signal round_1 : std_logic;
signal a_reg : Tareg;
signal b_reg : Tbreg;
signal q_int_reg : TQint;


begin

B_1  <= '0' & B;


--------------------------------------------------------------------
-- pipe on a and b                                               --
--------------------------------------------------------------------
b_reg(0) <= B_1;
a_reg(0) <= A;

ab_noreg : if pipe_g=0 generate
   for_j : for j in 0 to 2*(AN-BN)+2 generate
	     a_reg(j+1) <= a;
		 b_reg(j+1) <= b_1;
		 end generate;
end generate;

ab_reg : if pipe_g/=0 generate
--   for_j : for j in 0 to (AN-BN+2) generate
   for_j : for j in 0 to 2*(AN-BN)+2 generate
      process(clk_i,reset_i)
	  begin
	  if reset_i='1' then
	     a_reg(j+1) <= (others => '0');
		  b_reg(j+1) <= (others => '0');
	  elsif rising_edge(clk_i) then
        if en_i = '1' then
	        a_reg(j+1) <= a_reg(j);
		     b_reg(j+1) <= b_reg(j);
		  end if;
	  end if;
	  end process;
	  end generate;
end generate;


R_1(0)(BN+1 downto 1)<="00" & A(AN-1 downto (AN-BN+1));

-------------------------------------------------------------------
-- generate the array of soustracteur                              --
-------------------------------------------------------------------
gen_Q : for j in 0 to (AN-BN) generate
   BI_1(j)(0) <= '0';
	--R_1(j)(0)<=A_reg(j)(AN-BN-j);
	R_1(j)(0)<=A_reg(pipeconst(pipe_g)*j)(AN-BN-j);
	Q_int(AN-BN-j) <= not BI_1_reg(j)(BN+1); 
   gen_Q0: for i in 0 to BN generate 
      subq0 : sub1bit 
	       generic map ( pipe_g => pipe_g)
	       port map (
	       clk_i => clk_i,
		   reset_i => reset_i,
		   en_i => en_i,
           BI => BI_1_reg(j)(i), 
		   --S => B_reg(j+1)(i), 
		   S => B_reg(pipeconst(pipe_g)*j+1)(i),  
		   D => R_1_reg(j)(i), 
		   BO => BI_1(j)(i+1),
		   R => R_1(j+1)(i+1),
		   N => BI_1_reg(j)(BN+1));
   end generate;
end generate;       

-------------------------------------------------------------------
-- pipes on R_1 if pipe_g/=0  (yellow bloc)                      --
-------------------------------------------------------------------
r_1_pipe : if (pipe_g /= 0) generate
   r1_reg : for j in 0 to (AN-BN+1) generate
   process (clk_i,reset_i)
      begin
	  if reset_i='1' then
		 R_1_reg(j) <= (others => '0');
	  elsif rising_edge(clk_i) then
       if en_i = '1' then	      
		    R_1_reg(j) <= R_1(j);
		 end if;
      end if;		 
	  end process;
	  end generate;
end generate r_1_pipe; 

r_1_n_pipe : if (pipe_g = 0) generate
   R_1_reg <= R_1;
end generate r_1_n_pipe;  


----------------------------------------------------------------------------------
-- pipe on BI_1  if pipe_g=2 (FF only at the end of of the chain) (green bloc)  --
----------------------------------------------------------------------------------
BI_1_pipe_1 : if (pipe_g/=2) and (pipe_g/=4) generate
   BI_1_reg <= BI_1;
end generate;

BI_1_pipe_24 : if (pipe_g=2) generate
   BI_reg : for j in 0 to (AN-BN) generate
      BI_1_reg(j)(BN downto 0) <= BI_1(j)(BN downto 0);
      process(clk_i,reset_i)
      begin
      if reset_i='1' then
	        BI_1_reg(j)(BN+1) <= '0';
      elsif rising_edge(clk_i) then
         if en_i = '1' then          
	        BI_1_reg(j)(BN+1) <= BI_1(j)(BN+1);
	      end if;
      end if;
      end process;
   end generate BI_reg;
end generate BI_1_pipe_24;   
   
-------------------------------------------------------------------
-- pipe on BI_1 if pipe_g=4  (FF between each soustracteur)      --
-------------------------------------------------------------------

BI_1_pipe_4 : if (pipe_g=4) generate
   BI_reg : for j in 0 to (AN-BN) generate
   process(clk_i,reset_i)
   begin
   if reset_i='1' then 
	     BI_1_reg(j) <= (others => '0');
	elsif rising_edge(clk_i) then
	    if en_i = '1' then
          BI_1_reg(j) <= BI_1(j);
       end if;          
	end if;
   end process;
   end generate BI_reg;
end generate BI_1_pipe_4;

-- si pipe_g=3, le FF est directement instanti� dans sub1bit

----------------------------------------------------------------------------
-- pipeline on Q_int                                                     --
----------------------------------------------------------------------------
qint_no_reg : if pipe_g=0 generate
   q_pipe <= q_int;
   for_j : for j in 0 to AN-BN+1 generate
      q_int_reg(j) <= q_int;
   end generate;
end generate;


qint_reg : if pipe_g/=0 generate
   q_int_reg(0) <= q_int;
   for_j : for j in 0 to AN-BN generate
      process(clk_i,reset_i)
	  begin
	  if reset_i='1' then
	     --q_int_reg(j+1) <= (others => '0');
		 q_pipe(j) <= '0';--(others => '0');
	  elsif rising_edge(clk_i) then
        if en_i = '1' then
	     --q_int_reg(j+1) <= q_int_reg(j);
		 --q_pipe(j) <= q_int_reg(j+1)(j);
		    q_pipe(j) <= q_int_reg((pipeconst(pipe_g)*j)+1)(j); 
		  end if;
	  end if;
	  end process;
	  end generate;
	  
	 for_j2 : for j in 0 to 2*(AN-BN) generate
      process(clk_i,reset_i)
	  begin
	  if reset_i='1' then
	     q_int_reg(j+1) <= (others => '0');
	  elsif rising_edge(clk_i) then
        if en_i = '1' then	      
	        q_int_reg(j+1) <= q_int_reg(j);
	     end if;
	  end if;
	  end process;
	  end generate; 
	  
end generate;
		 
  


-----------------------------------------------------------------------
-- output register                                                   --
-----------------------------------------------------------------------
reg_f : if pipe_g/=0 generate
   -- registre final  
   process(clk_i,reset_i)
   begin
   if reset_i='1' then
      Q <= (others => '0');
      OVF <= '0';
   elsif rising_edge(clk_i) then
      if en_i = '1' then       
         if round_1='0' then
            Q <= Q_pipe(AN-BN-1 downto 0);  
         else
            Q <= Q_pipe(AN-BN-1 downto 0)+1;
         end if;
         OVF <= Q_pipe(AN-BN); 
      end if;          
   end if;
   end process;
end generate;

no_reg_f : if pipe_g=0 generate
   Q <= Q_int(AN-BN-1 downto 0) when round_1='0' else Q_int(AN-BN-1 downto 0)+1;	
   OVF <= Q_int(AN-BN);
end generate;


---------------------------------------------------------------------------
-- register on remainder                                                 --
---------------------------------------------------------------------------    
reg_rem: if pipe_g/=0 generate
process(clk_i,reset_i)
begin
if reset_i='1' then
remainder <= (others => '0');
remainder_o <= (others => '0');
elsif rising_edge(clk_i) then
   if en_i = '1' then    
      remainder <= R_1_reg(AN-BN+1)(BN+1 downto 1);
      remainder_o <= remainder; 
   end if;      
end if;
end process;
end generate;

n_reg_rem : if pipe_g=0 generate
  remainder <= R_1_reg(AN-BN+1)(BN+1 downto 1); 
  remainder_o <= remainder;
end generate;  


------------------------------------------------------------------------------
-- round                                                                    --
------------------------------------------------------------------------------
round_1 <= '1' when ((Q_pipe(AN-BN-1)='1') and (remainder>("00" & B_reg(AN-BN+3)(BN-1 downto 0)))) else '0';
	
end architecture;

--***********************************************
--          SUCCESSIVE APPROXIMATIONS
--***********************************************
--***********************************************
--                   ZsurX
--***********************************************
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity blocbase is
generic ( m_g : integer := 24);
PORT(clk_i,
     reset_i: IN std_logic;
     X_i : IN unsigned (m_g-1 downto 0);
	  Z_i : IN unsigned((2*m_g)-1 downto 0);
	  pow : integer range 0 to 32;
	  bit_prec : IN std_logic;
	  IN_0 : IN unsigned((2*m_g)-1 downto 0);
	  IN_1 : IN unsigned((2*m_g)-1 downto 0);
     Y_o : OUT std_logic;
	  OUT_0 : OUT unsigned((2*m_g)-1 downto 0);
	  OUT_1 : OUT unsigned((2*m_g)-1 downto 0));
end blocbase;

architecture impl of blocbase is
signal b : unsigned ((2*m_g)-1 downto 0);
signal A,C : unsigned ((2*m_g)-1 downto 0);
begin

B <= IN_1 when (bit_prec = '1') else IN_0;
C <= X_i * (2**pow);
A <= B + C;
Y_o <= '1' when (Z_i>=A) else '0';
OUT_0 <= B;
OUT_1 <= A;
end;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ZsurX is
generic ( m_g : integer := 8);
PORT(clk_i,reset_i: IN std_logic;
     B_i : IN unsigned (m_g-1 downto 0);
	  A_i : IN unsigned((2*m_g)-1 downto 0);
     Y_o : OUT unsigned (m_g-1 downto 0));
end ZsurX;

architecture Behavioral of ZsurX is
component blocbase is
generic ( m_g : integer := 24);
PORT(clk_i,
     reset_i: IN std_logic;
     X_i : IN unsigned (m_g-1 downto 0);
	  Z_i : IN unsigned((2*m_g)-1 downto 0);
	  pow : IN integer range 0 to 32;
	  bit_prec : IN std_logic;
	  IN_0 : IN unsigned((2*m_g)-1 downto 0);
	  IN_1 : IN unsigned((2*M_g)-1 downto 0);
     Y_o : OUT std_logic;
	  OUT_0 : OUT unsigned((2*m_g)-1 downto 0);
	  OUT_1 : OUT unsigned((2*m_g)-1 downto 0));
end component;
type TIN is array (0 to m_g) of unsigned((2*m_g)-1 downto 0);

signal Y_s : unsigned(m_g downto 0);
signal IN_0 : TIN;
signal IN_1 : TIN;

begin

gen : for j in m_g-1 downto 0 generate
   blocbase_inst : blocbase 
                generic map (m_g => m_g)
                port map (
					 clk_i => clk_i,
                reset_i => reset_i,
					 X_i => unsigned(B_i),
					 Z_i => unsigned(A_i),
					 pow => j,
					 bit_prec =>y_s(j+1),
					 IN_0 =>IN_0(j+1),
					 IN_1 =>IN_1(j+1),
					 Y_o => Y_s(j),
					 OUT_0 =>IN_0(j),
					 OUT_1 =>IN_1(j));
end generate;
  
IN_0(m_g) <= (others => '0');
IN_1(m_g) <= (others => '0');
y_s(m_g) <= '0';  
y_o <= y_s(m_g-1 downto 0);
end Behavioral;
