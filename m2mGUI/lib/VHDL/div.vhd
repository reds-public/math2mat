-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
------------------------------------------------------------------------------

--***********************************************
--               div2_comb_srt4_32
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_comb_srt4_32 is
generic
   (latency_g : integer := 1;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (31 downto 0);
    D2_i    : in std_logic_vector (31 downto 0); 
    m_o     : out std_logic_vector(31 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_comb_srt4_32;

architecture struct of div2_comb_srt4_32 is

begin  
  
div_inst : Div2
generic map
   (m_g       => 32,
    pipe_g    => 0,
    latency_g => latency_g,
    type_g    => 0,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_comb_srt4_64
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_comb_srt4_64 is
generic
   (latency_g : integer := 1;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (63 downto 0);
    D2_i    : in std_logic_vector (63 downto 0); 
    m_o     : out std_logic_vector(63 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_comb_srt4_64;

architecture struct of div2_comb_srt4_64 is
begin 
div_inst : Div2
generic map
   (m_g       => 64,
    pipe_g    => 0,
    latency_g => latency_g,
    type_g    => 0,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_pipe_srt4_32
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_pipe_srt4_32 is
generic
   (latency_g : integer := 0;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (31 downto 0);
    D2_i    : in std_logic_vector (31 downto 0); 
    m_o     : out std_logic_vector(31 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_pipe_srt4_32;

architecture struct of div2_pipe_srt4_32 is

begin  
div_inst : Div2
generic map
   (m_g       => 32,
    pipe_g    => 1,
    latency_g => 0,
    type_g    => 0,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_pipe_srt4_64
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_pipe_srt4_64 is
generic
   (latency_g : integer := 0;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (63 downto 0);
    D2_i    : in std_logic_vector (63 downto 0); 
    m_o     : out std_logic_vector(63 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_pipe_srt4_64;

architecture struct of div2_pipe_srt4_64 is

begin  
div_inst : Div2
generic map
   (m_g       => 64,
    pipe_g    => 1,
    latency_g => 0,
    type_g    => 0,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_comb_array_32
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_comb_array_32 is
generic
   (latency_g : integer := 1;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (31 downto 0);
    D2_i    : in std_logic_vector (31 downto 0); 
    m_o     : out std_logic_vector(31 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_comb_array_32;

architecture struct of div2_comb_array_32 is

begin
div_inst : Div2
generic map
   (m_g       => 32,
    pipe_g    => 0,
    latency_g => latency_g,
    type_g    => 1,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_pipe_array_32
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_pipe_array_32 is
generic
   (latency_g : integer := 0;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (31 downto 0);
    D2_i    : in std_logic_vector (31 downto 0); 
    m_o     : out std_logic_vector(31 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_pipe_array_32;

architecture struct of div2_pipe_array_32 is

begin  
div_inst : Div2
generic map
   (m_g       => 32,
    pipe_g    => 1,
    latency_g => 0,
    type_g    => 1,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;

--***********************************************
--               div2_comb_succ_32
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_cell.all;

entity div2_comb_succ_32 is
generic
   (latency_g : integer := 1;
    wValid_g  : integer := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (31 downto 0);
    D2_i    : in std_logic_vector (31 downto 0); 
    m_o     : out std_logic_vector(31 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end div2_comb_succ_32;

architecture struct of div2_comb_succ_32 is

begin 
div_inst : Div2
generic map
   (m_g       => 32,
    pipe_g    => 0,
    latency_g => latency_g,
    type_g    => 2,
    wValid_g  => wValid_g)
port map
   (valid_i 	=> valid_i,
   	clk_i 	  => clk_i,
	 reset_i		=> reset_i,
	 stall_i  => stall_i,
   	D1_i    	=> D1_i,
   	D2_i    	=> D2_i,
   	m_o      => m_o,
   	valid_o  => valid_o,
   	ready_o  => ready_o
  	);
end struct;
