-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che 
--          at - 04.02.2009
--
-- Misc : 
-- file containing miscellanous components :
--    - Pipe : shift register
------------------------------------------------------

--***********************************************
--                  Pipe
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 

entity Pipe is
  generic ( w : positive := 1;
            n : natural := 0 );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end entity;

architecture comp of Pipe is

type data_vec_typ is array (0 to n) of std_logic_vector(w-1 downto 0); 
signal val_s  : data_vec_typ;

begin
val_s(0) <= input;
  
reg : if n > 0 generate
pipe_gen : for i in 0 to n-1 generate
   process(clk, reset)
   begin
      if reset = '1' then
         val_s(i+1) <= (others => '0');
      elsif rising_edge(clk) then
         if en = '1' then
            val_s(i+1) <= val_s(i);
         end if;
      end if;
   end process;
end generate; 
end generate;                           

output <= val_s(n);

end architecture;