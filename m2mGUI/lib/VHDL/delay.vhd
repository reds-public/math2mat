-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

---------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Unit Delay : 
-- Shift regiter.
--     - m_g    = 32 : single pr�cision
--     - m_g    = 64 : double pr�cision 
--
--      delay_g = number of registers
---------------------------------------------------------

--***********************************************
--                Delay
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Delay is
generic
   (delay_g  : integer := 0;
    m_g      : integer := 32;
    wValid_g : positive := 1);
port
   (reset_i  : in std_logic;
    clk_i    : in std_logic;
    stall_i  : in std_logic;
    m_i      : in std_logic_vector(m_g-1 downto 0);
    valid_i  : in std_logic_vector(wValid_g-1 downto 0);
    m_o      : out std_logic_vector(m_g-1 downto 0);
    valid_o  : out std_logic_vector(wValid_g-1 downto 0));
end entity Delay;

architecture struct of Delay is

component Pipe is	
  generic ( w : positive := 1;
            n : natural := 0 );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic); 
end component;

signal nstall_s : std_logic;

begin
nstall_s <= not(stall_i);
mInst : Pipe
   generic map (w => m_g,
                n => delay_g)
   port map (input  => m_i, 
             output => m_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);
             
validInst : Pipe
   generic map (w => wValid_g,
                n => delay_g)
   port map (input  => valid_i, 
             output => valid_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);             

end architecture struct;