-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Counter_Rd.vhd
-- Description  : simple counter for m2m loops, without loading
-- 
-- Auteur       : SMS
-- Date         : 13.04.2010
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.log_pkg.all;

entity Counter_Rd is
    generic(Size_g : integer := 64);
    port(Clock_i   : in std_logic;
         Reset_i : in std_logic;
         En_i    : in std_logic;
         Ready_i : in std_logic;
         Count_o : out std_logic_vector(ilogup(Size_g)-1 downto 0)
        );
end Counter_Rd;

architecture comport of Counter_Rd is

    signal Count_s     : unsigned(ilogup(size_g)-1 downto 0);
    signal Count_Next_s  : unsigned(ilogup(size_g)-1 downto 0);
		signal Count_Mem_s : unsigned(ilogup(size_g)-1 downto 0);
    signal Ready_s     : std_logic;

begin

	Count_s <=  Count_s      when Ready_s = '1' and En_i = '0' else 
							Count_Next_s when Ready_s = '1' and En_i = '1' else 
							Count_Mem_s;

	Count_o <= std_logic_vector(Count_s);
	
    process(Clock_i, Reset_i)
    begin
        if Reset_i = '1' then
					Count_Next_s <= (others => '0');
        elsif Rising_Edge(Clock_i) then
					if Count_s = (Size_g-1) then
						Count_Next_s <= (others => '0');
					else
						Count_Next_s <= Count_s + 1;
					end if;
        end if;
    end process;

    process(Clock_i, Reset_i)
    begin
       if (Reset_i = '1') then
					Count_Mem_s <= (others => '0');
					Ready_s <= '1';
			elsif Rising_Edge(Clock_i) then
				if (Ready_i = '1') then
					Count_Mem_s <= Count_s;
				end if;
				Ready_s <= Ready_i;
			end if;
		end process;
    
end comport;