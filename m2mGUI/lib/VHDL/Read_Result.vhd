-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Read_Result.vhd
-- Description  : 
-- 
-- Auteur       : SMS
-- Date         : 19.04.2010
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity Read_Result is
  generic( 
    Data_Width_g : integer := 32
  );
  port(Clock_i   : in std_logic;
       Reset_i : in std_logic;
       Data_i  : in std_logic_vector(Data_Width_g-1 downto 0);
       Valid_i : in std_logic;
       Ready_i : in std_logic;
       Data_o  : out std_logic_vector(Data_Width_g-1 downto 0);
       Valid_o : out std_logic;
       Ready_o : out std_logic
  );
end Read_Result;

architecture comport of Read_Result is

begin

		Data_o <= Data_i;
		Valid_o <= Valid_i;
    Ready_o <= Ready_i;
    
end comport;