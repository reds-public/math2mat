-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------
--                            Negation.vhd                           --
-----------------------------------------------------------------------
-- Auteur      : Trolliet Gregory                                    --
-- Date        : 23.04.2009                                          --
-- Description : Inverse le signe du nombre                          --
-- Modification: 02.12.2011. YTA.                                    --
--               Compliant with the standard interface.              -- 
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Negation is
		generic ( 
      wValid_g    :  integer := 1;
      data_size_g :  integer := 32;
      wE_g        :  integer := 7;
      wF_g        :  integer := 24
    );
		port (
			clk_i  : in  std_logic;
			reset_i : in  std_logic;
			valid_i : in  std_logic_vector(wValid_g-1 downto 0);
			stall_i : in  std_logic;
			D1_i	  : in  std_logic_vector(data_size_g-1 downto 0);
			m_o     : out std_logic_vector(data_size_g-1 downto 0);
			valid_o : out std_logic_vector(wValid_g-1 downto 0);
			ready_o : out std_logic );
end Negation;

architecture comp of Negation is
	
	signal nB_neg: std_logic_vector(wE_g+wF_g+2 downto 0);
	
begin

	valid_o <= valid_i;
	ready_o <= not stall_i;
	
	m_o(wE_g+wF_g) <= not D1_i(wE_g+wF_g);
	m_o(wE_g+wF_g-1 downto 0) <= D1_i(wE_g+wF_g-1 downto 0);
	
end comp;

