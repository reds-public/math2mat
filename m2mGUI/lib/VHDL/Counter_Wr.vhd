-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Counter_Wr.vhd
-- Description  : Manage the write adresse in the memory
-- 
-- Auteur       : DMO
-- Date         : 03.02.2011
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.log_pkg.all;
	
entity Counter_Wr is
  generic( 
		Data_Size_g : integer := 32;
    Mem_Size_g   : integer := 1024
	);
  port( 
		Clock_i       : in  std_logic;
    Reset_i       : in  std_logic;
    Data_i        : in  std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
    Data_Valid_i  : in  std_logic;
    Ready_i       : in  std_logic;
    Cond_i        : in  std_logic;
    Read_i        : in  std_logic;
    Adr_Rd_i      : in  std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
    Fifo_Ready_i  : in  std_logic;
    Data_o        : out std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
    Data_Valid_o  : out std_logic;
    Ready_o       : out std_logic
  );
end Counter_Wr;

architecture comport of Counter_Wr is

  signal Valid_pres_s     : std_logic;
  signal Valid_fut_s      : std_logic;
  
  signal Cond_pres_s      : std_logic;
  signal Cond_fut_s       : std_logic;
  
  signal Fifo_Ready_pres_s: std_logic;
  signal Fifo_Ready_fut_s : std_logic;

	signal Data_Pres_s		  : std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
	signal Data_Fut_s		    : std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
	
	signal Ptr_ecrit_pres_s : unsigned(ilogup(Mem_Size_g)-1 downto 0);
	signal Ptr_ecrit_fut_s  : unsigned(ilogup(Mem_Size_g)-1 downto 0);
    
  signal Cell_free_pres_s : std_logic_vector(Mem_Size_g-1 downto 0);
	signal Cell_free_fut_s  : std_logic_vector(Mem_Size_g-1 downto 0);
  
  signal Write_s          : std_logic;
    
begin

	Data_Fut_s  <= Data_i;
  Valid_fut_s <= Data_Valid_i;
  Cond_fut_s  <= Cond_i;
  Fifo_Ready_fut_s <= Fifo_ready_i;
  
  Write_s <= '1' when Cond_pres_s = '1' or (Valid_pres_s = '0' and Fifo_Ready_pres_s = '1') else
             '0';
  
  -- Gestion des flag de cellule memoire libre
	process(Read_i, Ptr_ecrit_pres_s, Adr_Rd_i, Cell_free_pres_s, Ready_i, Write_s) begin
    Cell_free_fut_s <= Cell_free_pres_s;
    if(Write_s = '1' and Ready_i = '1') then
      Cell_free_fut_s(to_integer(Ptr_ecrit_pres_s)) <= '1';
    end if;
    if(Read_i = '1') then
      Cell_free_fut_s(to_integer(unsigned(Adr_Rd_i))) <= '0';
    end if;
  end process;
    
  -- Gestion du pointeur d'ecriture
	process(Ptr_ecrit_pres_s, Cell_free_pres_s, Write_s, Ready_i) begin
    Ptr_ecrit_fut_s <= Ptr_ecrit_pres_s;
    if(Write_s = '1' and Ready_i = '1') then
      if(Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s)) = '0' and Ptr_ecrit_pres_s = Mem_Size_g-1) then
        Ptr_ecrit_fut_s <= (others => '0');
      elsif(Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s)) = '0') then
        Ptr_ecrit_fut_s <= Ptr_ecrit_pres_s + 1;
      end if;
    end if;
  end process;   
    
  -- Synchronisation
	process(Clock_i, Reset_i) begin
		if Reset_i = '1' then
			Ptr_ecrit_pres_s  <= (others => '0');
			Data_Pres_s       <= (others => '0');
      Cell_free_pres_s  <= (others => '0');
      Valid_pres_s      <= '0';
      Cond_pres_s       <= '0';
      Fifo_Ready_pres_s <= '0';
		elsif Rising_Edge(Clock_i) then
			Ptr_ecrit_pres_s <= Ptr_ecrit_fut_s;
			Data_Pres_s <= Data_Fut_s;
      Cell_free_pres_s <= Cell_free_fut_s;
      Valid_pres_s <= Valid_fut_s;
      Cond_pres_s <= Cond_fut_s;
      Fifo_Ready_pres_s <= Fifo_Ready_fut_s;
		end if;
  end process;

	Data_o  <= Data_Pres_s when (Cond_pres_s = '0' or Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s)) = '1') else
			       std_logic_vector(Ptr_ecrit_pres_s);
              
  Ready_o <= '0' when Write_s = '1' and Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s)) = '0' and Ready_i = '1' else
             '1';
  
  Data_Valid_o <= Valid_pres_s when Cell_free_pres_s(to_integer(unsigned(Data_Pres_s))) = '1' and Cond_pres_s = '0' else 
                  not Cell_free_pres_s(to_integer(unsigned(Data_Pres_s))) when (Cond_pres_s = '0' or Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s)) = '1') else
                  '0' when Ready_i = '0' else
                  not Cell_free_pres_s(to_integer(Ptr_ecrit_pres_s));
	
end comport;