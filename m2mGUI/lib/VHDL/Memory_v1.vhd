-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Memory.vhd
-- Description  : 
-- 
-- Auteur       : SMS
-- Date         : 12.04.2010
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 1.0       DMO    29.03.2011         Add signal for checking write address
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.log_pkg.all;

entity Memory is
    generic( 
      Data_Width_g : integer := 32;
      Mem_Size_g   : integer := 128
    );
    port(
      Clock_i      : in std_logic;
      Reset_i      : in std_logic;
      Wr_i         : in std_logic;
      Rd_i         : in std_logic;
      Adr_Wr_i     : in std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
      Adr_Rd_i     : in std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);
      Adr_Valid_i  : in std_logic;
      Data_i       : in std_logic_vector(Data_Width_g-1 downto 0);
      Ready_o      : out std_logic;
      Data_Valid_o : out std_logic;
      Data_o       : out std_logic_vector(Data_Width_g-1 downto 0)
    );
end Memory;

architecture comport of Memory is

    type Mem is array (0 to Mem_Size_g-1) of std_logic_vector(Data_Width_g-1 downto 0);
    
    signal Mem_s        : Mem;
    signal Valid_s      : std_logic_vector(Mem_Size_g-1 downto 0);
    signal Nb_cell_free : unsigned(ilogup(Mem_Size_g)-1 downto 0);

begin

  process(Clock_i, Reset_i)
  begin
    if Reset_i = '1' then
      Valid_s <= (others => '0');
      Data_Valid_o <= '0';
      Data_o <= (others => '0');
      Nb_cell_free <= (others => '0');
    elsif Rising_Edge(Clock_i) then
      if Wr_i = '1' and Adr_Valid_i = '1' then
        if Valid_s(To_Integer(Unsigned(Adr_Wr_i))) = '0' then 
          Mem_s(To_Integer(Unsigned(Adr_Wr_i))) <= Data_i;
          Valid_s(To_Integer(Unsigned(Adr_Wr_i))) <= '1';
        end if;
        Nb_cell_free <= Nb_cell_free + 1;
      end if;
      if Rd_i = '1' then
        Data_o <= Mem_s(To_Integer(Unsigned(Adr_Rd_i)));
        if Valid_s(To_Integer(Unsigned(Adr_Rd_i))) = '1' then
          Valid_s(To_Integer(Unsigned(Adr_Rd_i))) <= '0';
          Nb_cell_free <= Nb_cell_free - 1;
        end if;
        Data_Valid_o <= Valid_s(To_Integer(Unsigned(Adr_Rd_i)));
      else
        Data_Valid_o <= '0';
      end if;
    end if;
  end process;

end comport;