-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Fichier :  Log_pkg.vhd
-- Auteur  :  E. Messerli
-- Date    :  10.03.2008
--
-- Utilise dans   : 
-----------------------------------------------------------------------
-- Fonctionnement vu de l'exterieur : 
--   Paquetage du projet: Sorties_Serie
--   Contient les fonctions suivants:
--     ilogup   Logarithme entier arrondi en haut
--     ilog     Logarithme entier arrondi en bas
--
-----------------------------------------------------------------------
-- Ver  Date     Qui  Commentaires
-- 0.0  10.03.08 EMI  Version initiale
-- 0.1  25.03.10 GCD  Version uniquement avec les fonctions log
-----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

package log_pkg is

	-- integer logarithm (rounded up) [MR version]
	function ilogup (x : natural; base : natural := 2) return natural;

	-- integer logarithm (rounded down) [MR version]
	function ilog (x : natural; base : natural := 2) return natural;

end log_pkg;

package body log_pkg is


	-- integer logarithm (rounded up) [MR version]
	function ilogup (x : natural; base : natural := 2) return natural is
	  variable y : natural := 1;
	begin
	  while x > base ** y loop
	    y := y + 1;
	  end loop;
	  return y;
	end ilogup;

	-- integer logarithm (rounded down) [MR version]
	function ilog (x : natural; base : natural := 2) return natural is
	  variable y : natural := 1;
	begin
	  while x > base ** y loop
	    y := y + 1;
	  end loop;
	  if x<base**y then
	  	y:=y-1;
	  end if;
	  return y;
	end ilog;

end log_pkg;