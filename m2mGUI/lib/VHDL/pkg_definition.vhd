-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Package definition: 
-- Constants and functions
-----------------------------------------------------------

--***********************************************
--         PACKAGE definition
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
package pkg_definition is 
   constant m_c : integer := 32;
      
   Constant latency1_c : Integer := 1;
   Constant delay1_c : Integer := 4;

   Constant latency2_c : Integer := 1;
   Constant delay2_c : Integer := 1;
  
   Constant latency3_c : Integer := 0;
   Constant delay3_c : Integer := 0;
   
   function count_zeros(signal mant_s : std_logic_vector) return unsigned;
 
end pkg_definition;

package body pkg_definition is
   -- compte le nombre de zero depuis la gauche    
   function count_zeros(signal mant_s : std_logic_vector) return unsigned is
   variable count : unsigned(5 downto 0);
   begin
      count := "000000";
      for i in mant_s'range loop
         case mant_s(i) is
            when '0' => count := count + "1";
            when others => exit;         
         end case;    
      end loop;   
      return count;  
   end count_zeros;

end pkg_definition;


