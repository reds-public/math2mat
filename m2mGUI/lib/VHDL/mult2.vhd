-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Unit Mult2 : 
-- Multiplication (single and double precision). (double only with carry save adder)
--     - m_g    = 32 : single pr�cision
--     - m_g    = 64 : double pr�cision 
--
--     - type_g = 0 : carry save adder
--     - type_g = 1 : wired multiplier
--
--     - pipe_g = 0 : combinatorial version
--     - pipe_g = 1 : pipelined version
--
--     - latency_g  : latency time of the combinatorial version.
-------------------------------------------------------------------------------------

--***********************************************
--                  Mult2
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Mult2 is
generic
   (m_g       : integer := 32; 
    pipe_g    : integer := 0;
    latency_g : integer := 0;
    type_g    : integer := 0;
    wValid_g  : positive := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (m_g-1 downto 0);
    D2_i    : in std_logic_vector (m_g-1 downto 0); 
    m_o     : out std_logic_vector(m_g-1 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end Mult2;

architecture struct of Mult2 is

constant wE : integer := 5 + m_g/10; -- exponent width (only for m_g = 32 or 64)
constant wF : integer := m_g-1-wE;   -- mantissa(fraction) width

type Ttypeconst is array (0 to 1) of integer;
constant typeconst : Ttypeconst := (7,5); -- delay for the different pipelined version
                                          -- (0: csa, 1: wired mulplier)

signal m_s  : std_logic_vector(m_g-1 downto 0);
signal nstall_s : std_logic;
signal valid_s : std_logic_vector(wValid_g-1 downto 0);
signal sig1_s, sig2_s, clear_s, set_s : std_logic;

component MultFP is	
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0;
    type_g : integer := 0);
port
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    b_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

begin 
nstall_s <= not(stall_i); -- nstall_s = enable

MultFPinst : MultFP
generic map
   (wF => wF,
    wE => wE,
    pipe_g => pipe_g,
    type_g => type_g)
port map
   (clk_i => clk_i,
    reset_i => reset_i,
    en_i => nstall_s,
    a_i => D1_i,
    b_i => D2_i,
    y_o => m_s
    ); 

-- combinatorial version : output register and delay for the signal valid_i
combGen : if pipe_g = 0 generate 
mInst : Pipe
   generic map (w => m_g,
                n => 1) -- output register for combinatorial version
   port map (input  => m_s, 
             output => m_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);

validInst : Pipe
   generic map (w => wValid_g,
                n => latency_g) -- valid_o after latency time
   port map (input  => valid_i, 
             output => valid_s,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);
             
valid_o <= valid_s;

-- RS flip-flop for signal ready_o with the inputs valid_i and valid_o             
clear_s <= valid_i(0) and not(valid_s(0));
set_s   <= valid_s(0) or reset_i;
sig1_s  <= not(clear_s or sig2_s);
sig2_s  <= not(set_s or sig1_s);
-- YTA: modified this to comply with the pipeline version
--ready_o <= nstall_s and sig1_s;             
ready_o <= nstall_s;                            
end generate combGen;

-- pipelined version : delay for signal valid_i
pipeGen : if (latency_g = 0 and pipe_g > 0) generate
PipeInst : Pipe
   generic map (w => wValid_g,
                n => typeconst(type_g))
   port map (input  => valid_i, 
             output => valid_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);
   m_o <= m_s;
   ready_o <= nstall_s;
end generate pipeGen;            

end struct;

--***********************************************
--              Multiplication FP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity MultFP is
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0;
    type_g : integer := 0);
port
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(wE+wF downto 0);
    b_i     : in std_logic_vector(wE+wF downto 0);
    y_o     : out std_logic_vector(wE+wF downto 0));
end entity MultFP;

architecture comp of MultFP is

component csa is
generic(pipe_g : integer := 0);
port(clk_i   : in std_logic;
     reset_i : in std_logic;
     en_i    : in std_logic;
     a_i     : in std_logic_vector (wF downto 0);
     b_i     : in std_logic_vector (wF downto 0);
     y_o     : out std_logic_vector(2*wF+1 downto 0));
end component csa;

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

-- I/Os signals (sign, exponent, mantissa)
signal a_s      : unsigned(wE+wF downto 0);
signal b_s      : unsigned(wE+wF downto 0);
signal y_s      : std_logic_vector(wE+wF downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(wE downto 0);
signal a_mant_s : unsigned(wF downto 0);
signal b_sign_s : std_logic;
signal b_exp_s  : unsigned(wE downto 0);
signal b_mant_s : unsigned(wF downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(wE downto 0);
signal y_mant_s : unsigned(wF-1 downto 0);

-- signals for computation
signal y_exp_calc_s     : unsigned(wE downto 0);
signal y_mant_calc_s    : unsigned(2*wF+1 downto 0);
signal y_mant_calc2_s   : std_logic_vector(2*wF+1 downto 0);
signal y_mant_round_s   : unsigned(wF+1 downto 0);
signal exp_max_s        : std_logic;
signal exp_min_s        : std_logic;
signal exceptions_s     : unsigned(2 downto 0); -- NaN, infini, 0
signal exceptions_out_s : std_logic_vector(2 downto 0);

-- signals for pipeline
signal y_sign_reg_s       : std_logic;
signal y_mant_reg_s       : unsigned(wF-1 downto 0);
signal y_exp_reg_s        : unsigned(wE downto 0);
signal y_exp_calc_reg_s   : std_logic_vector(wE downto 0);
signal y_mant_calc_reg_s  : unsigned(2*wF+1 downto 0);
signal y_mant_round_reg_s : unsigned(wF+1 downto 0);
signal exceptions_reg_s   : std_logic_vector(2 downto 0);

begin  

-- separation of signals : sign, exponent, mantissa
-- exponent on 9 bits  : carry(1) & exp(8)
-- mantissa on 24 bits : hidden(1) & mant(23)
a_sign_s <= a_s(wE+wF);
a_exp_s  <= '0' & a_s(wE+wF-1 downto wF);
a_mant_s <= '1' & a_s(wF-1 downto 0);
b_sign_s <= b_s(wE+wF);
b_exp_s  <= '0' & b_s(wE+wF-1 downto wF);
b_mant_s <= '1' & b_s(wF-1 downto 0);

y_sign_s <= a_sign_s xor b_sign_s;
y_exp_calc_s  <= a_exp_s + b_exp_s;

-- test if input signals are exceptions 
exceptions_input : process(a_exp_s, b_exp_s, a_mant_s, b_mant_s)
begin
   if ((a_exp_s = '0'&(wE-1 downto 0 => '1') and a_mant_s > '1'&(wF-1 downto 0 => '0')) or  
       (b_exp_s = '0'&(wE-1 downto 0 => '1') and b_mant_s > '1'&(wF-1 downto 0 => '0'))) then
      exceptions_s <= "100"; -- a or b = NaN, y = NaN   
   elsif (a_exp_s = (wE downto 0 => '0') or b_exp_s = (wE downto 0 => '0')) then 
      if (a_exp_s < '0'&(wE-1 downto 0 => '1') and b_exp_s < '0'&(wE-1 downto 0 => '1')) then 
         exceptions_s <= "001"; -- 0*Number=0
      else
         exceptions_s <= "100"; -- 0*NaN=NaN
      end if;
   elsif (a_exp_s = '0'&(wE-1 downto 0 => '1') or b_exp_s = '0'&(wE-1 downto 0 => '1')) then 
      exceptions_s <= "010"; -- number*infinity=infinity
   else
      exceptions_s <= "000";       
   end if;
end process;             

-- combinatorial version of the multiplier
combinatorial : if pipe_g = 0 generate
   a_s <= unsigned(a_i);
   b_s <= unsigned(b_i);
   
   -- multiplier carry save adder
   mult_0 : if type_g = 0 and wF = 23 generate
   pipe_gen : csa
   generic map(pipe_g => pipe_g)
   port map(clk_i   => clk_i,
	         reset_i => reset_i,
	         en_i    => en_i,
	         A_i     => std_logic_vector(a_mant_s),
	         B_i     => std_logic_vector(b_mant_s),
	         y_o 	   => y_mant_calc2_s); 
	
   y_mant_calc_s <= unsigned(y_mant_calc2_s);
   end generate mult_0;
   
   -- wired multiplier
   mult_1 : if type_g = 1 generate   
      y_mant_calc_s    <= a_mant_s * b_mant_s;
   end generate mult_1;
 
   y_sign_reg_s     <= y_sign_s;
   y_exp_calc_reg_s <= std_logic_vector(y_exp_calc_s);
   y_mant_round_reg_s <= y_mant_round_s;
   y_mant_reg_s   <= y_mant_s;
   y_exp_reg_s    <= y_exp_s;
   exceptions_reg_s <= std_logic_vector(exceptions_s);

   y_o <= y_s;
end generate combinatorial;

-- pipelined version of the multiplier
pipeline : if pipe_g > 0 generate
reg_input : process (clk_i,reset_i)
begin
   if reset_i = '1' then
		a_s  <= (others => '0');
		b_s  <= (others => '0');
   elsif rising_edge(clk_i) then
      if en_i = '1' then
		   a_s  <= unsigned(a_i);
		   b_s  <= unsigned(b_i);
      end if;
   end if;
end process;

-- multiplier carry save adder
mult_0 : if type_g = 0 and wF=23 generate
pipe_gen : csa
   generic map(pipe_g => pipe_g)
   port map(clk_i   => clk_i,
	         reset_i => reset_i,
	         en_i    => en_i,
	         A_i     => std_logic_vector(a_mant_s),
	         B_i     => std_logic_vector(b_mant_s),
	         y_o 	   => y_mant_calc2_s); 
	
y_mant_calc_s <= unsigned(y_mant_calc2_s);
end generate mult_0; 

-- wired multiplier
mult_1 : if type_g = 1 generate
reg_m : process (clk_i,reset_i)
begin
   if rising_edge(clk_i) then
      if en_i = '1' then
         y_mant_calc_s <= a_mant_s * b_mant_s;
      end if;
   end if;
end process;
end generate mult_1;

-- number of pipeline registers is dependent on the type (wired or csa multiplier)
sign_pipe : Pipe
   generic map (w => 1,
                n => 5-(type_g*2))
   port map (input(0)  => y_sign_s, 
             output(0) => y_sign_reg_s,
             clk       => clk_i,
             en        => en_i,
             reset     => reset_i);
             
exp_pipe : Pipe
   generic map (w => wE+1,
                n => 4-(type_g*2))
   port map (input  => std_logic_vector(y_exp_calc_s), 
             output => y_exp_calc_reg_s,
             clk    => clk_i,
             en        => en_i,
             reset  => reset_i);
             
exceptions_pipe : Pipe
   generic map (w => 3,
                n => 5-(type_g*2))
   port map (input  => std_logic_vector(exceptions_s), 
             output => exceptions_reg_s,
             clk    => clk_i,
             en     => en_i,
             reset  => reset_i);
             
reg_signaux : process (clk_i,reset_i)
begin
   if reset_i = '1' then
      y_mant_round_reg_s <= (others => '0');
      y_mant_reg_s       <= (others => '0');
      y_exp_reg_s        <= (others => '0');
      y_o                <= (others => '0');
   elsif rising_edge(clk_i) then
      if en_i = '1' then
         y_mant_round_reg_s <= y_mant_round_s;
         y_mant_reg_s       <= y_mant_s;
         y_exp_reg_s        <= y_exp_s;
         y_o                <= y_s;
      end if;
   end if;
end process;
        
end generate pipeline;

-- round to nearest
round : process(y_mant_calc_s)
begin
   if (y_mant_calc_s(wF-1)) = '1' then
      y_mant_round_s <= y_mant_calc_s(2*wF+1 downto wF) + "1";
   else
      y_mant_round_s <= y_mant_calc_s(2*wF+1 downto wF);
   end if;
end process round;

-- normalisation of the result
norm : process(y_mant_round_reg_s,y_exp_calc_reg_s)
begin
   if (y_mant_round_reg_s(wF+1) = '1') then
      y_mant_s <= y_mant_round_reg_s(wF downto 1);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) - ((wE-2 downto 1 => '1')&'0');
   else
      y_mant_s <= y_mant_round_reg_s(wF-1 downto 0);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) - (wE-2 downto 0 => '1');    
   end if;
end process norm;

exp_max_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '1') else '0'; -- exponent max if infinity
exp_min_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '0') else '0'; -- exponent min if zero

-- limit the result in the bounds of the single or double precision
exceptions_output : process(y_exp_reg_s, exp_max_s, exp_min_s, exceptions_reg_s)
begin
   if (exceptions_reg_s = "000") then
      if (((exp_max_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and not y_exp_reg_s(wE-1))) = '1') then
         exceptions_out_s <= "010";
      elsif (((exp_min_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and y_exp_reg_s(wE-1) )) = '1') then
         exceptions_out_s <= "001";
      else
         exceptions_out_s <= exceptions_reg_s;
      end if;
   else
      exceptions_out_s <= exceptions_reg_s;
   end if;
end process;

-- output multiplex : result of computation or exception
process (exceptions_out_s, y_sign_reg_s, y_exp_reg_s, y_mant_reg_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & (wE+wF-1 downto 0 => '0'); -- zero
      when "010"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 0 => '0'); -- infinity
      when "100"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 1 => '0') & '1'; -- NaN
      when others => y_s <= std_logic_vector(y_sign_reg_s & y_exp_reg_s(wE-1 downto 0) & y_mant_reg_s);
   end case;
end process; 

end architecture comp;

--***********************************************
--  carry save adder
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;
 
entity csa is
generic(pipe_g : integer := 0);
port(clk_i   : in std_logic;
     reset_i : in std_logic;
     en_i    : in std_logic;
     a_i     : in std_logic_vector (23 downto 0);
     b_i     : in std_logic_vector (23 downto 0);
     y_o     : out std_logic_vector(47 downto 0));
end entity csa;

architecture comp of csa is

-- cell full adder
procedure FA
(signal x, y, z : in std_logic ; 
 signal c, s    : out std_logic) is
begin 
   c <= (x and y) or (x and z) or (y and z) ;
   s <= x xor y xor z ; 
end FA ;

type pp_type is array(23 downto 0) of std_logic_vector(23 downto 0);
type pp_s_type is array(23 downto 0) of std_logic_vector(47 downto 0);
signal pp_s : pp_s_type;
type s0_type is array(15 downto 0) of std_logic_vector(48 downto 0);
type s1_type is array(15 downto 0) of std_logic_vector(47 downto 0);
type s2_type is array(9 downto 0) of std_logic_vector(48 downto 0);
type s3_type is array(9 downto 0) of std_logic_vector(47 downto 0);
type s4_type is array(5 downto 0) of std_logic_vector(48 downto 0);
type s5_type is array(5 downto 0) of std_logic_vector(47 downto 0);
type s6_type is array(3 downto 0) of std_logic_vector(48 downto 0);
type s7_type is array(3 downto 0) of std_logic_vector(47 downto 0);
type s8_type is array(1 downto 0) of std_logic_vector(48 downto 0);
type s9_type is array(1 downto 0) of std_logic_vector(48 downto 0);
type s10_type is array(1 downto 0) of std_logic_vector(47 downto 0);
type s11_type is array(1 downto 0) of std_logic_vector(47 downto 0);
type s12_type is array(1 downto 0) of std_logic_vector(48 downto 0);
type s13_type is array(1 downto 0) of std_logic_vector(47 downto 0);
type s14_type is array(1 downto 0) of std_logic_vector(48 downto 0);

signal s0_s : s0_type;
signal s1_s : s1_type;
signal s2_s : s2_type;
signal s3_s : s3_type;
signal s4_s : s4_type;
signal s5_s : s5_type;
signal s6_s : s6_type;
signal s7_s : s7_type;
signal s8_s : s8_type;
signal s9_s : s9_type;
signal s10_s : s10_type;
signal s11_s : s11_type;
signal s12_s : s12_type;
signal s13_s : s13_type;
signal s14_s : s14_type;

signal s5_reg_s: s5_type;
signal test1_s : std_logic_vector(47 downto 0);
signal test2_s : std_logic_vector(47 downto 0);

signal a_s : unsigned(47 downto 0);
signal b_s : unsigned(47 downto 0);
signal y_s : unsigned(47 downto 0);

begin	
-- computation of the partial products
process(a_i,b_i)
variable pp : pp_type;
begin
   for i in 0 to 23 loop
      for j in 0 to 23 loop
         pp(i)(j) := a_i(j) and b_i(i);
      end loop;
      pp_s(0) <= "000000000000000000000000" & pp(0);      
      for i in 1 to 23 loop  
         pp_s(i) <= (23-i downto 0 => '0') & pp(i) & (i-1 downto 0 => '0');
      end loop;
   end loop; 
end process;   

first_range : for i in 0 to 7 generate
   FA_GEN : for j in 0 to 47 generate
      s0_s(i*2+1)(0) <= '0';  
      s0_s(i*2)(48)  <= '0';   
      FA(pp_s(i*3)(j),pp_s(i*3+1)(j),pp_s(i*3+2)(j),s0_s(i*2+1)(j+1),s0_s(i*2)(j));
   end generate;
end generate;

process(s0_s)
begin
   for i in 0 to 15 loop   
      s1_s(i) <= s0_s(i)(47 downto 0);
   end loop;
end process;

second_range : for i in 0 to 4 generate
   FA_GEN : for j in 0 to 47 generate
      s2_s(i*2+1)(0) <= '0';  
      s2_s(i*2)(48)  <= '0';   
      FA(s1_s(i*3)(j),s1_s(i*3+1)(j),s1_s(i*3+2)(j),s2_s(i*2+1)(j+1),s2_s(i*2)(j));           
   end generate;
end generate;

process(s2_s)
begin
   for i in 0 to 9 loop   
      s3_s(i) <= s2_s(i)(47 downto 0);
   end loop;
end process;

third_range : for i in 0 to 2 generate
   FA_GEN : for j in 0 to 47 generate
      s4_s(i*2+1)(0) <= '0';  
      s4_s(i*2)(48)  <= '0';   
      FA(s3_s(i*3)(j),s3_s(i*3+1)(j),s3_s(i*3+2)(j),s4_s(i*2+1)(j+1),s4_s(i*2)(j));           
   end generate;
end generate;

process(s4_s)
begin
   for i in 0 to 5 loop   
      s5_s(i) <= s4_s(i)(47 downto 0);
   end loop;
end process;

fourth_range : for i in 0 to 1 generate
   FA_GEN : for j in 0 to 47 generate
      s6_s(i*2+1)(0) <= '0';  
      s6_s(i*2)(48)  <= '0';   
      FA(s5_reg_s(i*3)(j),s5_reg_s(i*3+1)(j),s5_reg_s(i*3+2)(j),s6_s(i*2+1)(j+1),s6_s(i*2)(j));           
   end generate;
end generate;

process(s6_s)
begin
   for i in 0 to 3 loop   
      s7_s(i) <= s6_s(i)(47 downto 0);
   end loop;
end process; 

FA_GEN5 : for j in 0 to 47 generate
   s8_s(1)(0) <= '0';  
   s8_s(0)(48)  <= '0';   
   FA(s7_s(0)(j),s7_s(1)(j),s7_s(2)(j),s8_s(1)(j+1),s8_s(0)(j));  
end generate;

FA_GEN6 : for j in 0 to 47 generate
   s9_s(1)(0) <= '0';  
   s9_s(0)(48)  <= '0';   
   FA(s7_s(3)(j),test1_s(j),test2_s(j),s9_s(1)(j+1),s9_s(0)(j));  
end generate;    

process(s8_s, s9_s)
begin
   for i in 0 to 1 loop   
      s10_s(i) <= s8_s(i)(47 downto 0);
      s11_s(i) <= s9_s(i)(47 downto 0);
   end loop;
end process;

FA_GEN7 : for j in 0 to 47 generate
   s12_s(1)(0) <= '0';  
   s12_s(0)(48)  <= '0';   
   FA(s10_s(0)(j),s10_s(1)(j),s11_s(0)(j),s12_s(1)(j+1),s12_s(0)(j));  
end generate; 

process(s12_s)
begin
   for i in 0 to 1 loop   
      s13_s(i) <= s12_s(i)(47 downto 0);
   end loop;
end process;   

FA_GEN8 : for j in 0 to 47 generate
   s14_s(1)(0) <= '0';  
   s14_s(0)(48)  <= '0';   
   FA(s13_s(0)(j),s13_s(1)(j),s11_s(1)(j),s14_s(1)(j+1),s14_s(0)(j));  
end generate; 

y_s <= a_s + b_s;

combinatorial : if pipe_g = 0 generate
   a_s <= unsigned(s14_s(0)(47 downto 0));
   b_s <= unsigned(s14_s(1)(47 downto 0));
   y_o <= std_logic_vector(y_s);
   s5_reg_s <= s5_s;
   test1_s <= s3_s(9);
   test2_s <= s1_s(15);
end generate combinatorial;

pipeline : if pipe_g = 1 generate
process(clk_i, reset_i)
begin
   if reset_i = '1' then
      a_s <= (others => '0');
      b_s <= (others => '0');
      y_o <= (others => '0');
		--s5_reg_s <=(others => '0');
		test1_s <= (others => '0');
		test2_s <= (others => '0');		
   elsif rising_edge(clk_i) then
      if en_i = '1' then
         a_s <= unsigned(s14_s(0)(47 downto 0));
         b_s <= unsigned(s14_s(1)(47 downto 0));
         y_o <= std_logic_vector(y_s);
		   s5_reg_s <= s5_s;
		   test1_s <= s3_s(9);
		   test2_s <= s1_s(15);
      end if;
   end if;
end process;
end generate pipeline;                    
end comp; 
