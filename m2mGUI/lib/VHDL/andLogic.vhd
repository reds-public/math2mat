-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : andLogic.vhd
--
-- Description  : 
-- 
-- Auteur       : SMS
-- Date         : 27.10.2009
-- Version      : 0.0
-- 
-- Utilise      : 
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;

entity andLogic is
	generic ( wValid_g : integer := 1 );
    port(
		clk_i 	: in std_logic;
		reset_i : in std_logic;
		valid_i : in std_logic_vector(wValid_g-1 downto 0);
		stall_i : in std_logic;
		D1_i	: in std_logic;
		D2_i	: in std_logic;
		m_o     : out std_logic;
		valid_o : out std_logic_vector(wValid_g-1 downto 0);
		ready_o : out std_logic 
    );
end andLogic;

architecture comp of andLogic is
begin
    
    Valid_o  <= valid_i;
	
    ready_o  <= not stall_i;
    
    m_o <= D1_i and D2_i;
			 
end architecture;