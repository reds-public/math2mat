-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Unit Add2 : 
-- Addition / subtraction (single and double precision).
--    - m_g    = 32 : single precision
--    - m_g    = 64 : double precision 
--
--    - pipe_g = 0 : combinatorial version 
--    - pipe_g = 1 : pipelined version
--   
--    - latency_g  : latency time of the combinatorial version. 
------------------------------------------------------------------------------

--***********************************************
--                  Add2
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Add2 is
generic
   (m_g       : integer := 32; 
    pipe_g    : integer := 0;
    latency_g : integer := 0;
    wValid_g  : positive := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (m_g-1 downto 0);
    D2_i    : in std_logic_vector (m_g-1 downto 0); 
    m_o     : out std_logic_vector(m_g-1 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end Add2;

architecture struct of Add2 is

signal m_s  : std_logic_vector(m_g-1 downto 0);
signal nstall_s, ready_s : std_logic;
signal valid_s : std_logic_vector(wValid_g-1 downto 0);
signal sig1_s, sig2_s, clear_s, set_s : std_logic;

constant wE : integer := m_g/(m_g/32+3)-(m_g/32-1); -- exponent width (only for m_g = 32 or 64)
constant wF : integer := m_g-1-wE;   -- mantissa(fraction) width

component AddFP is	
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0);
port
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    b_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

begin  
nstall_s <= not(stall_i); -- nstall_s = enable

AddFPinst : AddFP
generic map
   (wF => wF,
    wE => wE,
    pipe_g => pipe_g)
port map
   (clk_i   => clk_i,
    reset_i => reset_i,
    en_i    => nstall_s,
    a_i     => D1_i,
    b_i     => D2_i,
    y_o     => m_s
    ); 

-- combinatorial version : output register and delay for the signal valid_i
combGen : if pipe_g = 0 generate 
mInst : Pipe
   generic map (w => m_g,
                n => 1)   -- output register for combinatorial version
   port map (input  => m_s, 
             output => m_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);

validInst : Pipe
   generic map (w => wValid_g,
                n => latency_g) -- valid_o after latency time
   port map (input  => valid_i, 
             output => valid_s,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);

valid_o <= valid_s;             

-- RS flip-flop for signal ready_o with the inputs valid_i and valid_o
clear_s <= valid_i(0) and not(valid_s(0));
set_s   <= valid_s(0) or reset_i;
sig1_s  <= not(clear_s or sig2_s);
sig2_s  <= not(set_s or sig1_s);
-- YTA: modified this to comply with the pipeline version
--ready_o <= nstall_s and sig1_s;               
ready_o<= nstall_s;
end generate combGen;

-- pipelined version : delay for signal valid_i
pipeGen : if (latency_g = 0 and pipe_g > 0) generate
PipeInst : Pipe
   generic map (w => wValid_g,
                n => 7)
   port map (input  => valid_i, 
             output => valid_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);
   m_o <= m_s;
   ready_o <= nstall_s;
end generate pipeGen;
end struct;

--***********************************************
--          Addition / Subtraction FP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use ieee.std_logic_unsigned.all;
use work.pkg_definition.all;

entity AddFP is
generic
   (wE    : integer := 8;
    wF    : integer := 23;
    pipe_g : integer := 1);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(wE+wF downto 0);
    b_i     : in std_logic_vector(wE+wF downto 0);
    y_o     : out std_logic_vector(wE+wF downto 0));
end entity AddFP;

architecture comp of AddFP is 

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

component AbsDiff is
generic (wE : integer := 8);
port ( A, B : in Std_Logic_Vector ( wE-1 downto 0 ) ; -- A, B: diffend
       D    : out Std_Logic_Vector ( 5 downto 0 ) ) ; -- D: Distance
end component ;

-- I/Os signals (sign, exponent, mantissa)
signal a_s      : unsigned(wE+wF downto 0);
signal b_s      : unsigned(wE+wF downto 0);
signal y_s      : std_logic_vector(wE+wF downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(wE-1 downto 0);
signal a_mant_s : unsigned(wF+2 downto 0); 
signal b_sign_s : std_logic;
signal b_exp_s  : unsigned(wE-1 downto 0);
signal b_mant_s : unsigned(wF+2 downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(wE downto 0);
signal y_mant_s : std_logic_vector(wF+2 downto 0);

-- signals for computation
signal sign_s         : std_logic;
signal diff_exp_s     : std_logic_vector(5 downto 0);
signal small_mant_s   : std_logic_vector(wF+2 downto 0);
signal big_mant_s     : unsigned(wF+2 downto 0);
signal exp_s          : unsigned(wE-1 downto 0);
signal mant_s         : unsigned(wF+2 downto 0);
signal res_mant_s     : unsigned(wF+2 downto 0);
signal nb_zeros_s     : unsigned(5 downto 0);
signal nb_zeros_moins1_s : std_logic_vector(5 downto 0);
signal norm_mant_s    : std_logic_vector(wF+2 downto 0);
signal exceptions_s : std_logic_vector(1 downto 0); -- NaN, infini
signal exceptions_out_s : std_logic_vector(2 downto 0);

-- signals for pipeline
signal diff_exp_reg_s : std_logic_vector(5 downto 0);
signal exp_reg_s      : std_logic_vector(wE-1 downto 0);
signal small_mant_reg_s : std_logic_vector(wF+2 downto 0);
signal y_sign_reg_s   : std_logic;
signal big_mant_reg_s : std_logic_vector(wF+2 downto 0);
signal sign_reg_s     : std_logic;
signal mant_reg_s     : std_logic_vector(wF+2 downto 0);
signal mant_reg2_s    : std_logic_vector(wF+2 downto 0);
signal norm_mant_reg_s : std_logic_vector(wF+2 downto 0);
signal y_exp_reg_s    : std_logic_vector(wE downto 0);
signal nb_zeros_reg_s : unsigned(5 downto 0);
signal nb_zeros_moins1_reg_s : std_logic_vector(5 downto 0);
signal res_mant_reg_s : unsigned(wF+2 downto 0);
signal exceptions_reg_s : std_logic_vector(1 downto 0);

signal y_exp_t_s    : unsigned(wE downto 0);
signal y_mant_av_s : std_logic_vector(wF+2 downto 0);


begin 

-- separation of the signals : sign, exponent, mantissa
-- mantissa on 26 bits : carry(1) & hidden(1) & fraction(23) & round(1)
-- if number = 0 (exponent = 0), the hidden bit of the mantissa is set to 0  
a_sign_s <= a_s(wE+wF);
a_exp_s  <= unsigned(a_s(wE+wF-1 downto wF));
a_mant_s <= ("00" & unsigned(a_s(wF-1 downto 0)) & "0") when a_s(wF+wE-1 downto wF) = (wE-1 downto 0 => '0') else 
            ("01" & unsigned(a_s(wF-1 downto 0)) & "0");
b_sign_s <= b_s(wE+wF);
b_exp_s  <= unsigned(b_s(wE+wF-1 downto wF));
b_mant_s <= ("00" & unsigned(b_s(wF-1 downto 0)) & "0") when b_s(wF+wE-1 downto wF) = (wE-1 downto 0 => '0') else 
            ("01" & unsigned(b_s(wF-1 downto 0)) & "0");

sign_s <= a_sign_s xor b_sign_s;

-- test if input signals are exceptions 
exceptions_input : process(a_exp_s, b_exp_s, a_mant_s, b_mant_s, sign_s)
begin
   if ((a_exp_s = (wE-1 downto 0 => '1') and a_mant_s > ("01"&(wF+1 downto 0 => '0'))) or  
       (b_exp_s = (wE-1 downto 0 => '1') and b_mant_s > ("01"&(wF+1 downto 0 => '0')))) then
      exceptions_s <= "10"; -- a or b = NaN, y = NaN
   elsif (a_exp_s = (wE-1 downto 0 => '1')) then 
      if (b_exp_s = (wE-1 downto 0 => '1') and sign_s = '1') then
         exceptions_s <= "10"; -- infinity-infinity=NaN
      else
         exceptions_s <= "01"; -- infinity+number=infinity
      end if;
   elsif (b_exp_s = (wE-1 downto 0 => '1')) then
      exceptions_s <= "01";
   else
      exceptions_s <= "00";       
   end if;
end process;

-- combinatorial version of the adder
combinatorial : if pipe_g = 0 generate
   a_s <= unsigned(a_i);
   b_s <= unsigned(b_i);
   
   diff_exp_reg_s        <= diff_exp_s;
   small_mant_reg_s      <= small_mant_s;
   y_sign_reg_s          <= y_sign_s;
   sign_reg_s            <= sign_s;
   exp_reg_s             <= std_logic_vector(exp_s);
   big_mant_reg_s        <= std_logic_vector(big_mant_s);
   mant_reg_s            <= std_logic_vector(mant_s);
   mant_reg2_s           <= mant_reg_s;
   nb_zeros_reg_s        <= nb_zeros_s;
   nb_zeros_moins1_reg_s <= nb_zeros_moins1_s;
   norm_mant_reg_s       <= norm_mant_s;
   exceptions_reg_s      <= exceptions_s;
   res_mant_reg_s        <= res_mant_s;
   y_exp_reg_s           <= std_logic_vector(y_exp_s);
   
   y_o <= y_s;

end generate combinatorial;

-- pipelined version of the adder
pipeline : if pipe_g > 0 generate
signaux : process (clk_i,reset_i)
begin
   if reset_i = '1' then
		a_s                   <= (others => '0');
		b_s                   <= (others => '0');
		diff_exp_reg_s        <= (others => '0');
		small_mant_reg_s      <= (others => '0');
		mant_reg_s            <= (others => '0');
		mant_reg2_s           <= (others => '0');
      nb_zeros_reg_s        <= (others => '0');
      nb_zeros_moins1_reg_s <= (others => '0');
		norm_mant_reg_s       <= (others => '0');
		res_mant_reg_s        <= (others => '0');
		y_exp_reg_s           <= (others => '0');
   elsif rising_edge(clk_i) then
      if en_i = '1' then
		   a_s                   <= unsigned(a_i);
		   b_s                   <= unsigned(b_i);
		   diff_exp_reg_s        <= diff_exp_s;
		   small_mant_reg_s      <= small_mant_s;
		   mant_reg_s            <= std_logic_vector(mant_s);
		   mant_reg2_s           <= mant_reg_s;
         nb_zeros_reg_s        <= nb_zeros_s;
         nb_zeros_moins1_reg_s <= nb_zeros_moins1_s;
		   norm_mant_reg_s       <= norm_mant_s;
		   res_mant_reg_s        <= res_mant_s;
		   y_exp_reg_s           <= std_logic_vector(y_exp_s);
      end if;
	end if;
end process;

y_sign_pipe : Pipe
   generic map (w => 1,
                n => 5)
   port map (input(0)  => y_sign_s, 
             output(0) => y_sign_reg_s,
             clk       => clk_i,
             en        => en_i,             
             reset     => reset_i);
             
sign_pipe : Pipe
   generic map (w => 1,
                n => 2)
   port map (input(0)  => sign_s, 
             output(0) => sign_reg_s,
             clk       => clk_i,
             en        => en_i,             
             reset     => reset_i);             
             
exp_pipe : Pipe
   generic map (w => wE,
                n => 4)
   port map (input  => std_logic_vector(exp_s), 
             output => exp_reg_s,
             clk    => clk_i,
             en     => en_i,             
             reset  => reset_i);
             
bigmant_pipe : Pipe
   generic map (w => wF+3,
                n => 2)
   port map (input  => std_logic_vector(big_mant_s), 
             output => big_mant_reg_s,
             clk    => clk_i,
             en     => en_i,             
             reset  => reset_i);                       

exceptions_pipe : Pipe
   generic map (w => 2,
                n => 5)
   port map (input  => std_logic_vector(exceptions_s), 
             output => exceptions_reg_s,
             clk    => clk_i,
             en     => en_i,
             reset  => reset_i);

output : process (clk_i,reset_i)
begin
   if reset_i = '1' then
      y_o <= (others => '0');
   elsif rising_edge(clk_i) then
      if en_i = '1' then
         y_o <= y_s;
      end if;
   end if;
end process;

end generate pipeline;

-- test of the biggest number
bigger : process (a_exp_s, b_exp_s, a_mant_s, b_mant_s, a_sign_s, b_sign_s) 
begin
   if (a_exp_s > b_exp_s) then
      small_mant_s <= std_logic_vector(b_mant_s);
      big_mant_s   <= a_mant_s;
      exp_s        <= a_exp_s;
      y_sign_s     <= a_sign_s;
   elsif (a_exp_s < b_exp_s) then
      small_mant_s <= std_logic_vector(a_mant_s);
      big_mant_s   <= b_mant_s;
      exp_s        <= b_exp_s;
      y_sign_s     <= b_sign_s;
   else
      if (a_mant_s < b_mant_s) then -- YTA avoiding results being -0
         small_mant_s <= std_logic_vector(a_mant_s);
         big_mant_s   <= b_mant_s;
         exp_s        <= b_exp_s;
         y_sign_s     <= b_sign_s; --a_sign_s and b_sign_s; correction ph.crausaz/EIAFR 1 march 2011
      else
         small_mant_s <= std_logic_vector(b_mant_s);
         big_mant_s   <= a_mant_s;
         exp_s        <= a_exp_s;
         y_sign_s     <= a_sign_s;       
      end if;      
   end if;    
end process bigger;

-- computation of the absolute value from the difference of the exponents 
distance : AbsDiff 
generic map(wE => wE)
port map (std_logic_vector(a_exp_s), std_logic_vector(b_exp_s), diff_exp_s ) ; 

-- right shift of the smallest number 
shift : process (diff_exp_reg_s, small_mant_reg_s)
begin
   if (diff_exp_reg_s /= "000000") then
      res_mant_s <= unsigned(shr(small_mant_reg_s, diff_exp_reg_s));
   else
      res_mant_s <= unsigned(small_mant_reg_s);
   end if;
end process shift;

-- Addition / Subtraction
addsub : process (res_mant_reg_s, big_mant_reg_s, sign_reg_s)
begin
   if (sign_reg_s = '0') then       
      mant_s <= res_mant_reg_s + unsigned(big_mant_reg_s);
   else
      mant_s <= unsigned(big_mant_reg_s) - res_mant_reg_s;
   end if;
end process addsub;

-- computation of the number of zero left
nb_zeros_s <= count_zeros(mant_reg_s);
nb_zeros_moins1_s <= std_logic_vector(count_zeros(mant_reg_s(wF+1 downto 0)));

-- normalization of the result
norm : process (mant_reg2_s, nb_zeros_reg_s, exp_reg_s, nb_zeros_moins1_reg_s)
begin
   if (nb_zeros_reg_s = "000000") then
      y_exp_t_s  <= "0" & unsigned(exp_reg_s) + "1"; 
      norm_mant_s <= shr(mant_reg2_s, "1");   
   elsif (nb_zeros_reg_s = "000001") then
      y_exp_t_s  <= "0" & unsigned(exp_reg_s);
      norm_mant_s <= mant_reg2_s;
   elsif (nb_zeros_reg_s = wF+3) then --if mantissa = 0, result is forced to 0
      y_exp_t_s <= (others => '0');
      norm_mant_s <= mant_reg2_s;
   else
      norm_mant_s <= shl(mant_reg2_s, nb_zeros_moins1_reg_s); 
      y_exp_t_s  <= "0" & unsigned(exp_reg_s) - unsigned(nb_zeros_moins1_reg_s);
   end if;   
end process norm;

-- correction of the matissa after round if carry='1'
process(y_mant_av_s(wF+2),y_exp_t_s) --mod phcrausaz 6.04.11 
begin
   if y_mant_av_s(wF+2)='1' then -- carry = 1 ==> add 1 to exp (test is done after round) 
      y_exp_s <= unsigned(y_exp_t_s) + "1";
   else
      y_exp_s <= y_exp_t_s;   
   end if;	  
end process; -- end of mod phcrausaz 6.04.11

-- round to nearest norm_mant_s
round_av : process (norm_mant_s)
begin
   if (norm_mant_s(0) = '1') then
      y_mant_av_s <= std_logic_vector(unsigned(norm_mant_s) + "10");
   else
      y_mant_av_s <= norm_mant_s; 
   end if; 
end process round_av;

-- round to nearest
round : process (norm_mant_reg_s)
begin
   if (norm_mant_reg_s(0) = '1') then
      y_mant_s <= std_logic_vector(unsigned(norm_mant_reg_s) + "10");
   else
      y_mant_s <= norm_mant_reg_s; 
   end if; 
end process round;

-- limit the result in the bounds of the single or double precision
exceptions_output : process(exceptions_reg_s, y_exp_reg_s)
begin
   if (exceptions_reg_s = "00") then
      if y_exp_reg_s = ('0' & (wE-1 downto 0 => '1')) then
         exceptions_out_s <= "010";
      elsif y_exp_reg_s(wE) = '1' then
         exceptions_out_s <= "001";
      else
         exceptions_out_s <= "000";
      end if;  
   else
      exceptions_out_s <= exceptions_reg_s & '0';
   end if;
end process;

-- output multiplex : result of computation or exception
process (exceptions_out_s, y_sign_reg_s, y_exp_reg_s, y_mant_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & (wE+wF-1 downto 0 => '0'); -- zero
      when "010"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 0 => '0'); -- infinity
      when "100"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 1 => '0') & '1'; -- NaN
      when others => y_s <= y_sign_reg_s & y_exp_reg_s(wE-1 downto 0) & y_mant_s(wF downto 1);
   end case;
end process;

end architecture comp;

--***********************************************
--                AbsDiff
--
-- Description : Return the absolute value of the 
--               difference of the exponents.
--               For exponent on 8 or 11 bits.
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity AbsDiff is
generic(wE : integer := 8);
port ( A, B : in Std_Logic_Vector ( wE-1 downto 0 ) ; -- A,B: addends
       D : out Std_Logic_Vector ( 5 downto 0 ) ) ; -- D: Distance
end AbsDiff ;

architecture structural of AbsDiff is

signal diff, diff1 : unsigned(wE-1 downto 0);

begin

diff <= unsigned(A)-unsigned(B);
diff1 <= not(diff) + "1" when diff(wE-1) = '1' else diff;

process (diff1,A) 

    -- YTA for generic code
	function m_or(val: unsigned(wE-1 downto 0)) return std_logic is
	begin
		for i in wE-1 downto 6 loop
			if val(i)='1' then
				return '1';
			end if;
		end loop;
		return '0';
	end m_or;
	
begin
   if (A'length = 8) then
      if (diff1(7) or diff1(6) or diff1(5)) = '0' then
         D <= '0'& std_logic_vector(diff1(4 downto 0));
      else 
         D <= "011111"; 
      end if;
   elsif m_or(diff1)='0' then
--   elsif (diff1(10) or diff1(9) or diff1(8) or diff(7) or diff(6)) = '0' then
      D <= std_logic_vector(diff1(5 downto 0));
   else 
      D <= "111111";                                                                               
   end if; 
end process;
end structural;
