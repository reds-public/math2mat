-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Init_Input.vhd
-- Description  : initialisation bloc for loops
-- 
-- Auteur       : SMS
-- Date         : 08.04.2010
-- Version      : 0.0
-- 
-- Utilise      : Math2Mat
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 1.0	     DMO	03.02.2010		   Modification des timmings des signaux de Loop
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;

entity Init_Input is
  generic ( 
		Data_Size_g : integer := 32
	);
  port ( 
		Clock_i           : in std_logic;
    Reset_i           : in std_logic;
    Data_i            : in std_logic_vector(Data_Size_g-1 downto 0);
    Data_Valid_i      : in std_logic; 
		Data_Ready_o      : out std_logic;
    Data_Loop_i       : in std_logic_vector(Data_Size_g-1 downto 0);
    Data_Loop_Valid_i : in std_logic;
		Data_Loop_Ready_o : out std_logic;
    Cond_Loop_i       : in std_logic;
    Data_o            : out std_logic_vector(Data_Size_g-1 downto 0);
    Data_Valid_o      : out std_logic;
		Data_Ready_i      : in std_logic    
    );
end Init_Input;

architecture comport of Init_Input is

	signal Data_Loop_Pres_s			  :	std_logic_vector(Data_Size_g-1 downto 0);
	signal Data_Loop_Fut_s			  :	std_logic_vector(Data_Size_g-1 downto 0);
		
	signal Data_Loop_Pres_Valid_s	:	std_logic;
	signal Data_Loop_Fut_Valid_s	:	std_logic;
	
begin

	Data_Loop_Fut_s <= Data_Loop_i;
	Data_Loop_Fut_Valid_s <= Data_Loop_Valid_i;
	
	Data_o <= Data_Loop_Pres_s when Cond_Loop_i = '1' else
            Data_i;

	Data_Valid_o <= Data_Loop_Pres_Valid_s when Cond_Loop_i = '1' else
                  Data_Valid_i and not Reset_i;

	Data_Ready_o <= '0' when Cond_Loop_i = '1' else
                  Data_Ready_i or Reset_i;
					
	Data_Loop_Ready_o <= '1';

	process(Clock_i, Reset_i) begin
		if Reset_i = '1' then
			Data_Loop_Pres_s <= (others => '0');
			Data_Loop_Pres_Valid_s <= '0';
		elsif Rising_Edge(Clock_i) then
			Data_Loop_Pres_s <= Data_Loop_Fut_s;
			Data_Loop_Pres_Valid_s <= Data_Loop_Fut_Valid_s;
		end if;
  end process;
	
end comport;