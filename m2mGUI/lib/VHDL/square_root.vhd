-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-------------------------------------------------------------------
-- Math2mat project
-- Created:
--          by - Samuel T�che
--          at - 04.02.2009
--
-- Unit Sqrt : 
-- Square root (single and double precision).
--     - m_g    = 32 : single pr�cision
--     - m_g    = 64 : double pr�cision 
--
--     - type_g = 0 : algorithm SRT2
--     - type_g = 1 : algorithm non-restoring
--
--     - pipe_g = 0 : combinatorial version
--     - pipe_g = 1 : pipelined version
--
--     - latency_g  : latency time of the combinatorial version.
-------------------------------------------------------------------

--***********************************************
--                    Sqrt
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 
use work.pkg_definition.all;

entity Sqrt is
generic
   (m_g       : integer := 32; 
    pipe_g    : integer := 0;
    latency_g : integer := 0;
    type_g    : integer := 0;
    wValid_g  : positive := 1);
port
   (valid_i : in std_logic_vector(wValid_g-1 downto 0);
    clk_i   : in std_logic;
    reset_i : in std_logic;
    stall_i : in std_logic;
    D1_i    : in std_logic_vector (m_g-1 downto 0);
    m_o     : out std_logic_vector(m_g-1 downto 0);
    valid_o : out std_logic_vector(wValid_g-1 downto 0);
    ready_o : out std_logic);
end Sqrt;

architecture struct of Sqrt is

constant wE : integer := 5 + m_g/10; -- exponent width (only for m_g = 32 or 64)
constant wF : integer := m_g-1-wE;   -- mantissa(fraction) width

type Ttypeconst is array (0 to 1) of integer;
constant typeconst : Ttypeconst := ((wF+2)/2+3,wF+5); -- delay for the different pipelined version
                                                      -- (0: SRT2, 1: non-restoring)

signal m_s    : std_logic_vector(m_g-1 downto 0);
signal nstall_s : std_logic;
signal valid_s : std_logic_vector(wValid_g-1 downto 0);
signal sig1_s, sig2_s, clear_s, set_s : std_logic;

component SqrtFP_0 is -- SRT2 algo
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component SqrtFP_1 is -- non-restoring algo
generic
   (wE     : integer := 8;
    wF     : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(m_g-1 downto 0);
    y_o     : out std_logic_vector(m_g-1 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural );
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

begin  
nstall_s <= not(stall_i); -- nstall_s = enable

SRT2_ALGO : if type_g = 0 generate          
sqrt_inst : SqrtFP_0
generic map
   (wF => wF,
    wE => wE,
    pipe_g => pipe_g)
port map
   (clk_i => clk_i,
    reset_i => reset_i,
    en_i => nstall_s,
    a_i => D1_i,
    y_o => m_s
    ); 
end generate;

NR_ALGO : if type_g = 1 generate          
sqrt_inst : SqrtFP_1
generic map
   (wF => wF,
    wE => wE,
    pipe_g => pipe_g)
port map
   (clk_i => clk_i,
    reset_i => reset_i,
    en_i  => nstall_s,
    a_i => D1_i,
    y_o => m_s
    ); 
end generate;

-- combinatorial version : output register and delay for the signal valid_i
combGen : if pipe_g = 0 generate 
mInst : Pipe
   generic map (w => m_g,
                n => 1) -- output register for combinatorial version
   port map (input  => m_s, 
             output => m_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);

validInst : Pipe
   generic map (w => wValid_g,
                n => latency_g) -- valid_o after latency time
   port map (input  => valid_i, 
             output => valid_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i); 

valid_o <= valid_s;

-- RS flip-flop for signal ready_o with the inputs valid_i and valid_o             
clear_s <= valid_i(0) and not(valid_s(0));
set_s   <= valid_s(0) or reset_i;
sig1_s  <= not(clear_s or sig2_s);
sig2_s  <= not(set_s or sig1_s);
ready_o <= nstall_s and sig1_s;
end generate combGen;

-- pipelined version : delay for signal valid_i
pipeGen : if (latency_g = 0 and pipe_g > 0) generate
PipeInst : Pipe
   generic map (w => wValid_g,
                n => typeconst(type_g))
   port map (input  => valid_i, 
             output => valid_o,
             clk    => clk_i,
             en     => nstall_s,
             reset  => reset_i);
   m_o <= m_s;
   ready_o <= nstall_s;
end generate pipeGen;
end struct;


--***********************************************
--               ALGORITHME SRT2
--***********************************************
--                   SqrtFP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity SqrtFP_0 is
generic
   (wE    : integer := 8;
    wF    : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(wE+wF downto 0);
    y_o     : out std_logic_vector(wE+wF downto 0));
end entity SqrtFP_0;

architecture comp of SqrtFP_0 is

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

component FPSqrt_Sqrt is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF+1 downto 0);
         fR  : out std_logic_vector(wF+3 downto 0) );
end component;

component FPSqrt_Sqrt_Clk is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF+1 downto 0);
         fR  : out std_logic_vector(wF+3 downto 0);
         clk : in  std_logic;
         en  : in  std_logic);
end component;

-- I/Os signals (sign, exponent, mantissa)
signal a_s      : std_logic_vector(wE+wF downto 0);
signal y_s      : std_logic_vector(wE+wF downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(wE-1 downto 0);
signal a_mant_s : std_logic_vector(wF+1 downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(wE downto 0);
signal y_mant_s : unsigned(wF+1 downto 0);

-- signals for computation
signal exp1_s           : unsigned(wE downto 0);
signal y_exp_calc_s     : unsigned(wE downto 0);
signal y_mant_calc_s    : std_logic_vector(wF+3 downto 0);
signal y_mant_norm_s    : std_logic_vector(wF+1 downto 0);
signal exp_max_s        : std_logic;
signal exp_min_s        : std_logic;
signal exceptions_s     : std_logic_vector(2 downto 0);
signal exceptions_out_s : std_logic_vector(2 downto 0);

-- signals for pipeline
signal y_sign_reg_s      : std_logic;
signal y_exp_calc_reg_s  : std_logic_vector(wE downto 0);
signal y_mant_norm_reg_s : std_logic_vector(wF+1 downto 0);
signal y_exp_reg_s       : unsigned(wE downto 0);
signal exceptions_reg_s  : std_logic_vector(2 downto 0);

begin	

-- separation of signals : sign, exponent, mantissa
-- mantissa on 25 bits : - if exponent is even : mant
--                       - if exponent is odd  : mant / 2 
a_sign_s <= a_s(wE+wF);
a_exp_s  <= unsigned(a_s(wE+wF-1 downto wF));
a_mant_s <= '1' & a_s(wF-1 downto 0) & '0' when a_exp_s(0) = '0' else
            "01" & a_s(wF-1 downto 0);

y_sign_s <= a_sign_s;

-- if exponent is even : y_exp = exp / 2 
-- if exponent is odd  : y_exp = exp / 2 + 1
exp1_s <= "00" & a_exp_s(wE-1 downto 1);
y_exp_calc_s  <= exp1_s + ((wE-1 downto 0 => '0') & a_exp_s(0));

-- test if input signals are exceptions
exceptions_input : process(a_exp_s, a_s, a_sign_s)
begin
   if (a_exp_s = (wE-1 downto 0 => '0')) then
      exceptions_s <= "001"; -- sqrt(0)=0
   elsif (a_sign_s = '1') then
      exceptions_s <= "100"; -- sqrt(negative number)=NaN
   elsif (a_exp_s = (wE-1 downto 0 => '1')) then
      if (a_s(wF-1 downto 0) > (wF-1 downto 0 => '0')) then
         exceptions_s <= "100"; -- sqrt(NaN)=NaN
      else
         exceptions_s <= "010"; -- sqrt(infinity)=infinity
      end if;
   else
      exceptions_s <= "000";       
   end if;
end process; 

-- combinatorial version of the square root
combinatorial : if pipe_g = 0 generate
   a_s <= a_i;

   sqrt_FP : FPSqrt_Sqrt
   generic map( wF => wF)
   port map(fA  => a_mant_s,
            fR  => y_mant_calc_s); 
           
   y_sign_reg_s       <= y_sign_s;
   y_exp_calc_reg_s   <= std_logic_vector(y_exp_calc_s);
   y_mant_norm_reg_s  <= y_mant_norm_s;
   y_exp_reg_s        <= y_exp_s;
   exceptions_reg_s   <= exceptions_s;

   y_o <= y_s;
end generate combinatorial;

-- pipelined version of the square root
pipeline : if pipe_g > 0 generate
   input_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   a_s  <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
		      a_s  <= a_i;
         end if;
      end if;
   end process;

   sqrt_FP : FPSqrt_Sqrt_Clk
   generic map( wF => wF)
   port map(fA  => a_mant_s,
            fR  => y_mant_calc_s,
            clk => clk_i,
            en  => en_i);

-- number of pipeline registers is dependent on the number of bits of the mantissa
   sign_pipe : Pipe
      generic map (w => 1,
                   n => (wF+2)/2+1)
      port map (input(0)  => y_sign_s, 
                output(0) => y_sign_reg_s,
                clk       => clk_i,
                en        => en_i,
                reset     => reset_i);
             
   exp_pipe : Pipe
      generic map (w => wE+1,
                   n => (wF+2)/2)
      port map (input  => std_logic_vector(y_exp_calc_s), 
                output => y_exp_calc_reg_s,
                clk    => clk_i,
                en        => en_i,
                reset  => reset_i);
             
   exceptions_pipe : Pipe
      generic map (w => 3,
                   n => (wF+2)/2+1)
      port map (input  => exceptions_s, 
                output => exceptions_reg_s,
                clk    => clk_i,
                en     => en_i,
                reset  => reset_i);            
             
   process (clk_i,reset_i)
   begin
      if reset_i = '1' then
         y_mant_norm_reg_s <= (others => '0');
         y_exp_reg_s       <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
            y_mant_norm_reg_s <= y_mant_norm_s;
            y_exp_reg_s       <= y_exp_s;
         end if;
      end if;
   end process; 
   
   output_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   y_o <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
            y_o <= y_s;
         end if;
      end if;
   end process;         
end generate pipeline;

-- normalization of the result
norm : process(y_mant_calc_s,y_exp_calc_reg_s)
begin
   if (y_mant_calc_s(wF+3) = '1') then
      y_mant_norm_s <= y_mant_calc_s(wF+2 downto 1);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) + ("001"&(wE-3 downto 0 => '0'));
   else
      y_mant_norm_s <= y_mant_calc_s(wF+1 downto 0);
      y_exp_s  <= unsigned(y_exp_calc_reg_s) + ("000"&(wE-3 downto 0 => '1'));    
   end if;
end process norm;

-- round to nearest
round : process(y_mant_norm_reg_s)
begin
   if (y_mant_norm_reg_s(1)) = '1' then
      y_mant_s <= unsigned(y_mant_norm_reg_s) + "1";
   else
      y_mant_s <= unsigned(y_mant_norm_reg_s);
   end if;
end process round;  
 
exp_max_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '1') else '0'; -- exponent max if infinity
exp_min_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '0') else '0'; -- exponent min if zero

-- limit the result in the bounds of the single or double precision
exceptions_output : process(y_exp_reg_s, exp_max_s, exp_min_s, exceptions_reg_s)
begin
   if (exceptions_reg_s = "000") then
      if (((exp_max_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and not y_exp_reg_s(wE-1))) = '1') then
         exceptions_out_s <= "010";
      elsif (((exp_min_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and y_exp_reg_s(wE-1) )) = '1') then
         exceptions_out_s <= "001";
      else
         exceptions_out_s <= exceptions_reg_s;
      end if;
   else
      exceptions_out_s <= exceptions_reg_s;
   end if;
end process;

-- output multiplex : result of computation or exception
process (exceptions_out_s, y_sign_reg_s, y_exp_reg_s, y_mant_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & (wE+wF-1 downto 0 => '0'); -- zero
      when "010"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 0 => '0'); -- infinity
      when "100"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 1 => '0') & '1'; -- NaN
      when others => y_s <= std_logic_vector(y_sign_reg_s & y_exp_reg_s(wE-1 downto 0) & y_mant_s(wF+1 downto 2));
   end case;
end process;  
    
end comp;

--***********************************************
--                 FPSqrt_Sqrt
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FPSqrt_Sqrt is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF+1 downto 0);
         fR  : out std_logic_vector(wF+3 downto 0) );
end entity;

architecture arch of FPSqrt_Sqrt is
    
component FPSqrt_SRT2_Step is
  generic ( wF : positive;
            step  : positive );
  port ( x : in  std_logic_vector(wF+3 downto 0);
         s : in  std_logic_vector(wF+3 downto wF+4-step);
         d : out std_logic;
         w : out std_logic_vector(wF+3 downto 0) );
end component;    
    
  signal w  : std_logic_vector((wF+3)*(wF+4)-1 downto 0);
  signal d  : std_logic_vector(wF+3 downto 0);
  signal s  : std_logic_vector((wF+3)*(wF+4)-1 downto 0);
begin
  w((wF+3)*(wF+4)-1 downto (wF+2)*(wF+4)) <= "11" & fA;
  d(wF+3) <= '0';
  s((wF+3)*(wF+4)-1) <= '1';
  sqrt : for i in wF+2 downto 1 generate
    step : FPSqrt_SRT2_Step
      generic map ( wF => wF,
                    step  => wF+3-i )
      port map ( x => w((i+1)*(wF+4)-1 downto i*(wF+4)),
                 s => s((i+1)*(wF+4)-1 downto i*(wF+4)+i+1),
                 d => d(i),
                 w => w(i*(wF+4)-1 downto (i-1)*(wF+4)) );
    first : if i = wF+2 generate
      s(i*(wF+4)-1 downto (i-1)*(wF+4)+i) <= (not d(i)) & '1';
    end generate;
    regular : if i < wF+2 generate
      s(i*(wF+4)-1 downto (i-1)*(wF+4)+i) <= s((i+1)*(wF+4)-1 downto i*(wF+4)+i+2) & (not d(i)) & '1';
    end generate;
  end generate;
  d(0) <= w(wF+3);

  fR <= s(wF+3 downto 2) & (not d(0)) & '1';
end architecture;

--***********************************************
--               FPSqrt_Sqrt_Clk
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FPSqrt_Sqrt_Clk is
  generic ( wF : positive );
  port ( fA  : in  std_logic_vector(wF+1 downto 0);
         fR  : out std_logic_vector(wF+3 downto 0);
         clk : in  std_logic;
         en  : in  std_logic);
end entity;

architecture arch of FPSqrt_Sqrt_Clk is
    
component FPSqrt_SRT2_Step is
  generic ( wF : positive;
            step  : positive );
  port ( x : in  std_logic_vector(wF+3 downto 0);
         s : in  std_logic_vector(wF+3 downto wF+4-step);
         d : out std_logic;
         w : out std_logic_vector(wF+3 downto 0) );
end component;
    
  signal x  : std_logic_vector((wF+3)*(wF+4)-1 downto 0);
  signal w  : std_logic_vector((wF+2)*(wF+4)-1 downto 0);
  signal d  : std_logic_vector(wF+3 downto 0);
  signal s  : std_logic_vector((wF+3)*(wF+4)-1 downto 0);
begin
  x((wF+3)*(wF+4)-1 downto (wF+2)*(wF+4)) <= "11" & fA;
  d(wF+3) <= '0';
  s((wF+3)*(wF+4)-1) <= '1';
  sqrt : for i in wF+2 downto 1 generate
    step : FPSqrt_SRT2_Step
      generic map ( wF => wF,
                    step  => wF+3-i )
      port map ( x => x((i+1)*(wF+4)-1 downto i*(wF+4)),
                 s => s((i+1)*(wF+4)-1 downto i*(wF+4)+i+1),
                 d => d(i),
                 w => w(i*(wF+4)-1 downto (i-1)*(wF+4)) );
    
    reg : if (wF+2-i) mod 2 = 1 generate
      process(clk)
      begin
        if clk'event and clk='1' then
           if en = '1' then
              s(i*(wF+4)-1 downto (i-1)*(wF+4)+i) <= s((i+1)*(wF+4)-1 downto i*(wF+4)+i+2) & (not d(i)) & '1';
              x(i*(wF+4)-1 downto (i-1)*(wF+4)) <= w(i*(wF+4)-1 downto (i-1)*(wF+4));
            end if;
        end if;
      end process;
    end generate;

    noreg : if (wF+2-i) mod 2 = 0 generate
      first : if i = wF+2 generate
        s(i*(wF+4)-1 downto (i-1)*(wF+4)+i) <= (not d(i)) & '1';
      end generate;
      regular : if i < wF+2 generate
        s(i*(wF+4)-1 downto (i-1)*(wF+4)+i) <= s((i+1)*(wF+4)-1 downto i*(wF+4)+i+2) & (not d(i)) & '1';
      end generate;
      x(i*(wF+4)-1 downto (i-1)*(wF+4)) <= w(i*(wF+4)-1 downto (i-1)*(wF+4));
    end generate;
  end generate;
  
  d(0) <= x(wF+3);

  fR <= s(wF+3 downto 2) & (not d(0)) & '1';
end architecture;

--***********************************************
--             FPSqrt_SRT2_Step
--***********************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FPSqrt_SRT2_Step is
  generic ( wF : positive;
            step  : positive );
  port ( x : in  std_logic_vector(wF+3 downto 0);
         s : in  std_logic_vector(wF+3 downto wF+4-step);
         d : out std_logic;
         w : out std_logic_vector(wF+3 downto 0) );
end entity;

architecture arch of FPSqrt_SRT2_Step is
  signal d0 : std_logic;
  signal x0 : std_logic_vector(wF+4 downto 0);
  signal s0 : std_logic_vector(wF+4 downto wF+4-step);
  signal ds : std_logic_vector(wF+4 downto wF+2-step);
  signal x1 : std_logic_vector(wF+4 downto wF+2-step);
  signal w1 : std_logic_vector(wF+4 downto wF+2-step);
  signal w0 : std_logic_vector(wF+4 downto 0);
begin
  d0 <= x(wF+3);

  x0 <= x & "0";
  
  s0 <= "0" & s;
  ds <= s0(wF+4 downto wF+5-step) & (not d0) & d0 & "1";

  x1 <= x0(wF+4 downto wF+2-step);
  with d0 select
    w1 <= x1 - ds when '0',
          x1 + ds when others;
  w0(wF+4 downto wF+2-step) <= w1;
  zeros : if step <= wF+1 generate
    w0(wF+1-step downto 0) <= x0(wF+1-step downto 0);
  end generate;

  d <= d0;
  w <= w0(wF+3 downto 0);
end architecture;


--***********************************************
--          NON-RESTORING ALGORITHM
--***********************************************
--                   SqrtFP
--***********************************************
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity SqrtFP_1 is
generic
   (wE    : integer := 8;
    wF    : integer := 23;
    pipe_g : integer := 0);
port 
   (clk_i   : in std_logic;
    reset_i : in std_logic;
    en_i    : in std_logic;
    a_i     : in std_logic_vector(wE+wF downto 0);
    y_o     : out std_logic_vector(wE+wF downto 0));
end entity SqrtFP_1;

architecture comp of SqrtFP_1 is

component sqrt_comb is 
  generic
      (wF       : integer := 23); 
  port( mant_i   : in  std_logic_vector(wF+2 downto 0);
        mant_o   : out std_logic_vector(wF+3 downto 0));
end component;

component sqrt_pipe is
  generic
      (wF       : integer := 23);  
  port( clk_i    : in  std_logic;
        reset_i  : in  std_logic;
        en_i     : in  std_logic;
        mant_i   : in  std_logic_vector(wF+2 downto 0);
        mant_o   : out std_logic_vector(wF+3 downto 0));
end component;

component Pipe is
  generic ( w : positive := 1;
            n : natural := 0);
  port ( input  : in  std_logic_vector(w-1 downto 0);
         output : out std_logic_vector(w-1 downto 0);
         clk    : in  std_logic;
         en     : in  std_logic;
         reset  : in  std_logic);
end component;

-- I/Os signals (sign, exponent, mantissa)
signal a_s      : std_logic_vector(wF+wE downto 0);
signal y_s      : std_logic_vector(wF+wE downto 0);
signal a_sign_s : std_logic;
signal a_exp_s  : unsigned(wE-1 downto 0);
signal a_mant_s : std_logic_vector(wF+2 downto 0);
signal y_sign_s : std_logic;
signal y_exp_s  : unsigned(wE downto 0);
signal y_mant_s : unsigned(wF+1 downto 0);

-- signals for computation
signal exp1_s           : unsigned(wE downto 0);
signal y_exp_calc_s     : unsigned(wE downto 0);
signal y_mant_calc_s    : std_logic_vector(wF+3 downto 0);
signal y_mant_norm_s    : std_logic_vector(wF+1 downto 0);
signal exp_max_s        : std_logic;
signal exp_min_s        : std_logic;
signal exceptions_s     : std_logic_vector(2 downto 0);
signal exceptions_out_s : std_logic_vector(2 downto 0);

-- signals for pipeline
signal y_sign_reg_s      : std_logic;
signal y_exp_calc_reg_s  : std_logic_vector(wE downto 0);
signal y_mant_norm_reg_s : std_logic_vector(wF+1 downto 0);
signal y_exp_reg_s       : unsigned(wE downto 0);
signal exceptions_reg_s  : std_logic_vector(2 downto 0);

begin	

-- separation of signals : sign, exponent, mantissa
-- mantissa on 26 bits : - if exponent is even : mant
--                       - if exponent is odd  : mant / 2 
a_sign_s <= a_s(wE+wF);
a_exp_s  <= unsigned(a_s(wE+wF-1 downto wF));
a_mant_s <= "01" & a_s(wF-1 downto 0) &'0' when a_exp_s(0) = '0' else
            "001" & a_s(wF-1 downto 0);

y_sign_s <= a_sign_s;

-- if exponent is even : y_exp = exp / 2 
-- if exponent is odd  : y_exp = exp / 2 + 1
exp1_s <= "00" & a_exp_s(wE-1 downto 1);
y_exp_calc_s  <= exp1_s + ((wE-1 downto 0 => '0') & a_exp_s(0));

-- test if input signals are exceptions 
exceptions_input : process(a_exp_s, a_s, a_sign_s)
begin
   if (a_exp_s = (wE-1 downto 0 => '0')) then
      exceptions_s <= "001"; -- sqrt(0)=0
   elsif (a_sign_s = '1') then
      exceptions_s <= "100"; -- sqrt(negative number)=NaN
   elsif (a_exp_s = (wE-1 downto 0 => '1')) then
      if (a_s(wF-1 downto 0) > (wF-1 downto 0 => '0')) then
         exceptions_s <= "100"; -- sqrt(NaN)=NaN
      else
         exceptions_s <= "010"; -- sqrt(infinity)=infinity
      end if;
   else
      exceptions_s <= "000";       
   end if;
end process; 

-- combinatorial version of the square root
combinatorial : if pipe_g = 0 generate
   a_s <= a_i;

   sqrt_FP : sqrt_comb
   generic map(wF   => wF)    
   port map(mant_i  => a_mant_s,
            mant_o  => y_mant_calc_s); 
           
   y_sign_reg_s       <= y_sign_s;
   y_exp_calc_reg_s   <= std_logic_vector(y_exp_calc_s);
   y_mant_norm_reg_s  <= y_mant_norm_s;
   y_exp_reg_s        <= y_exp_s;
   exceptions_reg_s   <= exceptions_s;

   y_o <= y_s;
end generate combinatorial;

-- pipelined version of the square root
pipeline : if pipe_g > 0 generate
   input_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   a_s  <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
		      a_s  <= a_i;
         end if;
      end if;
   end process;

-- number of pipeline registers is dependent on the number of bits of the mantissa
   sqrt_FP : sqrt_pipe
   generic map(wF   => wF)   
   port map(clk_i   => clk_i,
            reset_i => reset_i,
            en_i    => en_i,
            mant_i  => a_mant_s,
            mant_o  => y_mant_calc_s); 

   sign_pipe : Pipe
      generic map (w => 1,
                   n => wF+3)
      port map (input(0)  => y_sign_s, 
                output(0) => y_sign_reg_s,
                clk       => clk_i,
                en        => en_i,
                reset     => reset_i);
             
   exp_pipe : Pipe
      generic map (w => wE+1,
                   n => wF+2)
      port map (input  => std_logic_vector(y_exp_calc_s), 
                output => y_exp_calc_reg_s,
                clk    => clk_i,
                en        => en_i,
                reset  => reset_i);
             
   exceptions_pipe : Pipe
      generic map (w => 3,
                   n => wF+3)
      port map (input  => exceptions_s, 
                output => exceptions_reg_s,
                clk    => clk_i,
                en        => en_i,
                reset  => reset_i);            
             
   process (clk_i,reset_i)
   begin
      if reset_i = '1' then
         y_mant_norm_reg_s <= (others => '0');
         y_exp_reg_s       <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
            y_mant_norm_reg_s <= y_mant_norm_s;
            y_exp_reg_s       <= y_exp_s;
         end if;
      end if;
   end process; 
   
   output_reg : process (clk_i,reset_i)
   begin
      if reset_i = '1' then
		   y_o <= (others => '0');
      elsif rising_edge(clk_i) then
         if en_i = '1' then
		      y_o <= y_s;
		   end if;
      end if;
   end process;         
end generate pipeline;

-- normalisation of the result
y_mant_norm_s <= y_mant_calc_s(wF+1 downto 0);
y_exp_s   <= unsigned(y_exp_calc_reg_s) + ("000"&(wE-3 downto 0 => '1'));

-- arrondi au plus proche
round : process(y_mant_norm_reg_s)
begin
   if (y_mant_norm_reg_s(0)) = '1' then
      y_mant_s <= unsigned(y_mant_norm_reg_s) + "1";
   else
      y_mant_s <= unsigned(y_mant_norm_reg_s);
   end if;
end process round;  

exp_max_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '1') else '0'; -- exponent max if infinity
exp_min_s <= '1' when y_exp_reg_s(wE-1 downto 0) = (wE-1 downto 0 => '0') else '0'; -- exponent min if zero

-- limit the result in the bounds of the simple or double precision
exceptions_output : process(y_exp_reg_s, exp_max_s, exp_min_s, exceptions_reg_s)
begin
   if (exceptions_reg_s = "000") then
      if (((exp_max_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and not y_exp_reg_s(wE-1))) = '1') then
         exceptions_out_s <= "010";
      elsif (((exp_min_s and not y_exp_reg_s(wE)) or (y_exp_reg_s(wE) and y_exp_reg_s(wE-1) )) = '1') then
         exceptions_out_s <= "001";
      else
         exceptions_out_s <= exceptions_reg_s;
      end if;
   else
      exceptions_out_s <= exceptions_reg_s;
   end if;
end process;

-- output multiplex : result of computation or exception
process (exceptions_out_s, y_sign_reg_s, y_exp_reg_s, y_mant_s)
begin
   case exceptions_out_s is
      when "001"  => y_s <= y_sign_reg_s & (wE+wF-1 downto 0 => '0'); -- zero
      when "010"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 0 => '0'); -- infinity
      when "100"  => y_s <= y_sign_reg_s & (wE-1 downto 0 => '1') & (wF-1 downto 1 => '0') & '1'; -- NaN
      when others => y_s <= std_logic_vector(y_sign_reg_s & y_exp_reg_s(wE-1 downto 0) & y_mant_s(wF downto 1));
   end case;
end process;  
    
end comp;

-------------------------------------------------
--  combinatorial version (non-restoring)
-------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sqrt_comb is 
  generic
      (wF       : integer := 23);
  port(mant_i   : in  std_logic_vector(wF+2 downto 0);
       mant_o   : out std_logic_vector(wF+3 downto 0));
end sqrt_comb;

architecture comp of sqrt_comb is

component sqrt_algo is 
  generic(step:positive;
          wF  :integer); 
  port( first_i : in std_logic_vector(wF+4 downto 0);
        deuxI_i : in std_logic_vector(wF+4 downto step-1);
        x_i  : in std_logic_vector(wF+4 downto step);
        q_i  : in std_logic;
        r2_i : in std_logic_vector(wF+4 downto step-1);
        r2_o : out std_logic_vector(wF+4 downto step-2);
        q_o  : out std_logic;
        x_o  : out std_logic_vector(wF+4 downto step-1));
end component;

constant step : integer := wF+3;

type data_vec is array (step downto 0) of std_logic_vector(wF+4 downto 0);   
signal x_s : data_vec;
signal r2_s : data_vec;
signal q_s : std_logic_vector(wF+3 downto 0);    
signal deuxI_s : data_vec; 
signal output_s : std_logic_vector(wF+4 downto 0);
signal first_s : data_vec;

    
begin
-- initialisation
r2_s(step) <= mant_i & "00";
q_s(step) <= '1';
deuxI_s(step) <= "001"&(wF+1 downto 0 => '0');
x_s(step)(wF+4 downto wF+3) <= "00";
first_s(step) <= r2_s(step)(wF+1 downto 0) & "000";

sqrt_gen : for i in step downto 2 generate

sqrt_inst : sqrt_algo
   generic map(step => i,
               wF   => wF)
   port map (first_i => first_s(i),
             deuxI_i => deuxI_s(i)(wF+4 downto i-1),
             x_i  => x_s(i)(wF+4 downto i),
             q_i  => q_s(i),
             r2_i => r2_s(i)(wF+4 downto i-1),
             r2_o => r2_s(i-1)(wF+4 downto i-2),
             q_o  => q_s(i-1),
             x_o  => x_s(i-1)(wF+4 downto i-1));
  
deuxI_s(i-1) <= '0' & deuxI_s(i)(wF+4 downto 1);
first_s(i-1) <= first_s(i)(wF+2 downto 0) & "00";
end generate;             

last : process(x_s(1), q_s(1))
begin
   if (q_s(1) = '0') then
      output_s <= x_s(1)(wF+4 downto 2) & "01";
   else
      output_s <= x_s(1)(wF+4 downto 1) & '1';
   end if;
end process;
mant_o <= output_s(wF+4 downto 1);
end comp;

-------------------------------------------------
--  pipelined version (non-restoring)
-------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sqrt_pipe is
  generic
      (wF       : integer := 23);  
  port(clk_i    : in  std_logic;
       reset_i  : in  std_logic;
       en_i     : in  std_logic;
       mant_i   : in  std_logic_vector(wF+2 downto 0);
       mant_o   : out std_logic_vector(wF+3 downto 0));
end sqrt_pipe;

architecture comp of sqrt_pipe is

component sqrt_algo is 
  generic(step:positive;
          wF  :integer); 
  port( first_i : in std_logic_vector(wF+4 downto 0);
        deuxI_i : in std_logic_vector(wF+4 downto step-1);
        x_i  : in std_logic_vector(wF+4 downto step);
        q_i  : in std_logic;
        r2_i : in std_logic_vector(wF+4 downto step-1);
        r2_o : out std_logic_vector(wF+4 downto step-2);
        q_o  : out std_logic;
        x_o  : out std_logic_vector(wF+4 downto step-1));
end component;

constant step : integer := wF+3;

type data_vec is array (step downto 0) of std_logic_vector(wF+4 downto 0);   
signal x_s : data_vec;
signal xo_s : data_vec;
signal r2_s : data_vec;
signal r2o_s : data_vec;
signal q_s : std_logic_vector(wF+3 downto 0); 
signal qo_s : std_logic_vector(wF+3 downto 0);   
signal deuxI_s : data_vec; 
signal output_s : std_logic_vector(wF+4 downto 0);
signal first_s : data_vec;

    
begin
-- initialisation
r2_s(step) <= mant_i & "00";
q_s(step) <= '1';
deuxI_s(step) <= "001"&(wF+1 downto 0 => '0');
x_s(step)(wF+4 downto wF+3) <= "00";
first_s(step) <= r2_s(step)(wF+1 downto 0) & "000";

sqrt_gen : for i in step downto 2 generate

sqrt_inst : sqrt_algo
   generic map(step => i,
               wF   => wF)
   port map (first_i => first_s(i),
             deuxI_i => deuxI_s(i)(wF+4 downto i-1),
             x_i  => x_s(i)(wF+4 downto i),
             q_i  => q_s(i),
             r2_i => r2_s(i)(wF+4 downto i-1),
             r2_o => r2o_s(i)(wF+4 downto i-2),
             q_o  => qo_s(i),
             x_o  => xo_s(i)(wF+4 downto i-1));
  
pipeline : process(clk_i)
begin
   if rising_edge(clk_i) then
      if en_i = '1' then
         deuxI_s(i-1) <= '0' & deuxI_s(i)(wF+4 downto 1);
         r2_s(i-1) <= r2o_s(i);
         x_s(i-1) <= xo_s(i);
         q_s(i-1) <= qo_s(i);  
         first_s(i-1) <= first_s(i)(wF+2 downto 0) & "00"; 
      end if;   
   end if; 
end process pipeline;
end generate;             

last : process(x_s(1), q_s(1))
begin
   if (q_s(1) = '0') then
      output_s <= x_s(1)(wF+4 downto 2) & "01";
   else
      output_s <= x_s(1)(wF+4 downto 1) & '1';
   end if;
end process;
mant_o <= output_s(wF+4 downto 1);
end comp;
  
-------------------------------------------------
-- algorithm non-restoring square root
-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sqrt_algo is 
  generic(step:positive;
          wF : integer); 
  port( first_i : in std_logic_vector(wF+4 downto 0);
        deuxI_i : in std_logic_vector(wF+4 downto step-1);
        x_i  : in std_logic_vector(wF+4 downto step);
        q_i  : in std_logic;
        r2_i : in std_logic_vector(wF+4 downto step-1);
        r2_o : out std_logic_vector(wF+4 downto step-2);
        q_o  : out std_logic;
        x_o  : out std_logic_vector(wF+4 downto step-1));
end sqrt_algo;

architecture comp of sqrt_algo is
    
signal r_s : unsigned(wF+4 downto step-1);
signal r2_s : unsigned(wF+4 downto step-2);
signal q_s : std_logic;  
signal r1_s : unsigned(wF+4 downto step-2);
    
begin
   -- r(i+1) = 2*r(i)- 2*X(i)*q(i+1)-2^(-i-1)   
   r_s <= unsigned(r2_i) + unsigned(x_i(wF+3 downto step) & "00") - unsigned(deuxI_i) when q_i = '0' else
          unsigned(r2_i) - unsigned(x_i(wF+3 downto step) & "00") - unsigned(deuxI_i);
   -- add the next bit
   r1_s <= r_s & first_i(wF+4);
   -- computation of 2*r(i+1) for the next step
   r2_s <= r1_s(wF+3 downto step-2) & first_i(wF+3);
   -- deduction of q(i+1) with the sign of 2r(i+1)
   q_s <= not(r_s(wF+4));
   r2_o <= std_logic_vector(r2_s);
   q_o <= q_s;   
process (q_i, x_i)
begin
   -- add the new bit to the result x
   if (q_i = '0') then
      x_o <= x_i(wF+4 downto step+1) & "01";
   else
      x_o <= x_i(wF+4 downto step) & '1';
   end if;
end process;
  
end comp;
