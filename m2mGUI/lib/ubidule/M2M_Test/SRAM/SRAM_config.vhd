-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- file:    SRAM_config.vhd
-- creator: Andres Upegui
--          name.surname@heig-vd.ch
-- date:    June 2007
--
-- description: This package defines a configuration bitstring
--              type that is used by the testbench in order to 
--              configure the external SRAM initial state. It 
--              defines also a constant of this type that is 
--              used in the testbench for configuring the 
--              SRAM from the VLIO interface. 
--
-- version: 1.0  | Initial version
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package SRAM_config_pack is   
   
   constant SRAM_config_size: integer:= 8; -- number of 32-bit words  
          
   type SRAM_config_word is record
       device: std_logic;   -- '1' -> external SRAM, '0' -> Ubichip 
       address: std_logic_vector(18 downto 0); -- SRAM address
       data: std_logic_vector(31 downto 0); -- 32-bit SRAM data
   end record;
   
   type SRAM_config_bits is array (0 to SRAM_config_size-1) of SRAM_config_word;
   
   constant my_SRAM_config : SRAM_config_bits:= (    

    ('1',"0000000000000000000",X"0000_0000"),
    ('1',"0000000000000000001",X"0000_0001"),
    ('1',"0000000000000000010",X"0000_0002"),
    ('1',"0000000000000000011",X"0000_0003"),
    ('1',"0000000000000000100",X"0000_0004"),
    ('1',"0000000000000000101",X"0000_0005"),
    ('1',"0000000000000000110",X"0000_0006"),
    ('1',"0000000000000000111",X"0000_0007")
    
    );  
    
end package;
    