-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- file:	SRAM_Ctrl.vhd
-- creator:	Andres Upegui
--			name.surname@heig-vd.ch
-- date:	May	2007
--
-- description:	This unit serves as	controller of the
--				external SRAM. Currently it	supports the 
--				access from	the	external CPU, by converting
--				the	VLIO bus access	to SRAM.
--				In a near future, it must also support the 
--				access from	the	sequencer and the ubicell
--				array. 
--				 
--
-- version:	1.0	 | Initial version
-- version: 1.1  | Removing of the state machine, everything being now
--                 combinational (Y.T.)
----------------------------------------------------------------------------

library	ieee;
use	ieee.std_logic_1164.all;
use	ieee.std_logic_arith.all;
use	ieee.std_logic_unsigned.all;

entity SRAM_Ctrl is
   port(
	clk:			in	std_logic;					-- Clock input	(active	with rising	edge)
	reset:		in	std_logic;					 --	Reset input	(synchronous and active	high)

	-- signals from	and	to the CPU_VLIO_Ctrl --
	SRAM_en		   : in	Std_Logic;		-- Enables SRAM	access.	
	SRAM_rdy	   : out  Std_Logic;	-- SRAM	access ready.	
	-- signals from	the	CPU	--
	CPU_RDnWR :	in	std_logic;					-- Read	('1') /	Write ('0')	signal for the configuration
	CPU_DQM	 :	in	std_logic_vector(3 downto 0);  -- DQM from the CPU
	CPU_address: in	std_logic_vector(25	downto 0); -- address from the CPU
	CPU_data_in: in	std_logic_vector(31	downto 0); -- data_from	the	CPU

	-- SRAM	interface signals --
	SRAM_nCS :	out		std_logic;		-- Chip	select to the SRAM
	SRAM_nOE_int :	out		std_logic;	 --	Output enable to the SRAM
	SRAM_nWE :	out		std_logic;					-- Read	('1') /	Write ('0')	signal for the configuration
	SRAM_DQM  :	out		std_logic_vector(3 downto 0);  -- DQM to the SRAM
	SRAM_address: out	std_logic_vector(25 downto 0) -- Address to	the	SRAM
	); 
end	SRAM_Ctrl;

architecture behavioral	of SRAM_Ctrl is

signal RDnWR: std_logic;
begin
	

	RDnWR <=CPU_RDnWR;

	Outputs: process(SRAM_en, RDnWR)
	begin			  
		SRAM_nCS <=	'1';
		SRAM_nOE_int <=	'1';
		SRAM_nWE <=	'1';		
		SRAM_rdy <=	'0';
			if (SRAM_en='1'	and	RDnWR='1') then
				-- read access
				SRAM_nCS <=	'0';
				SRAM_nOE_int <=	'0';
				SRAM_rdy <=	'1';		  
			elsif (SRAM_en='1' and RDnWR='0') then
				-- write access
				SRAM_nCS <=	'0';
				SRAM_nWE <=	'0';	
				SRAM_rdy <=	'1';		
			end	if;
	end	process Outputs;

	SRAM_DQM <=CPU_DQM;
--		SRAM_address <=	CPU_address(20 downto 2) when config_mode='1' else -- to	be further multiplexed with	the	ubicell	array access
      SRAM_address <=	"00000" & CPU_address(20 downto 0);
end	behavioral;