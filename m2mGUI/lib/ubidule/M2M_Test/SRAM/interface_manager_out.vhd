-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- file:    CPU_VLIO_ctrl.vhd
-- creator: Andres Upegui
--          name.surname@heig-vd.ch
-- date:    May 2007
--
-- description: This unit manages the access to the ubichip configuration
--              from the CPU unit through a VLIO interface. It supports
--              the access to the ubichip configuration bits and to the 
--              external SRAM contents. It supports also the burst access
--              mode of the VLIO bus (not tested). 
--
-- version: 1.0  | Initial version
--
-- version: 1.1  | bugs on the RDY signal fixed
--
-- version 1.2   | four states have been removed in order to make the 
--                 access more efficient, and in order to avoid writing
--                 errors.
-- version 2.0   | The possibility to read/write on ubichip is removed
--
-- version 3.0   | Add CPU_address in the sensivity list at Next_State_Decoder
----------------------------------------------------------------------------

library Ieee;
use Ieee.Std_Logic_1164.all;
use Ieee.Numeric_Std.all;

entity interface_manager_out is
 port(
      clk_i    : in std_logic;
      reset_i  : in std_logic;

      ready_i  : in std_logic;
      valid_o  : out std_logic
   );
end interface_manager_out;


architecture behavioral of interface_manager_out is
   type state is (WAIT_READY_H,
                          WAIT_1,
                          WAIT_2,
                          MASK_VALID,
                          WAIT_H,
                          UNMASK_VALID,
                          WAIT_L_2
                         ); 

   signal current_state : state;
   signal next_state    : state;

begin                
   Next_State_Decoder : process(current_state, ready_i)
   begin
         next_state <= WAIT_C_READY_L;

         case current_state is

            when WAIT_H =>
               if ready_i = '1' then
                  next_state <= MASK_VALID;
               else
                  next_state <= WAIT_H;
               end if;

--             when WAIT_1 =>
--                next_state <= MASK_C_VALID;

            when MASK_VALID =>
                  --next_state <= WAIT_C_H;
                  next_state <= UNMASK_VALID;

            when WAIT_L =>
               if ready_i = '1' then
                  --next_state <= WAIT_2;
                  next_state <= UNMASK_VALID;
               else
                  next_state <= WAIT_H;
               end if;


            when UNMASK_VALID =>
               next_state <= WAIT_READY_L_2;

            when WAIT_L_2 =>
               if ready_i = '0' then
                  next_state <= MASK_VALID;
               else
                  next_state <= WAIT_READY_L_2;
               end if;
  
         end case;             
   end process Next_State_Decoder;

   State_update : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         current_state <= WAIT_READY_L;
      elsif clk_i'event and clk_i='1' then
         current_state <= next_state;
      end if;
   end process State_update;


   Output_Decoder: process (current_state)
   begin   
      --- Synchonous outputs ---
      valid_o <= '1';

      case current_state is

         when WAIT_READY_L => 

         when WAIT_1 =>

         when MASK_VALID =>
            valid_o <= '0';

         when WAIT_H =>
            valid_o <= '0';

         when WAIT_2 =>
            

         when UNMASK_VALID =>

         when WAIT_READY_L_2 =>
            valid_o <= '0';

      end case;
   end process Output_Decoder;

end behavioral;
