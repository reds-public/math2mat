-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- file:    M2M_Test_top.vhd
-- creator: J. Manuel Moreno
--          moreno@eel.upc.edu
-- date:    December 2008
--
-- description:	This unit constitutes the top-level component of the Ubichip. 
--			It includes the functional section of the system, provided
--			in the ubi_top unit, the multiplexing resources 
--			needed for the GPIOs and the I/O pads of the chip
--               
--
-- version: 1.0  | Initial version
-- version: 2.0  | Modified by Mikhail Nagoga for M2M_Test_top component needs
--                 November 2010
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity M2M_Test_top is
port(	
		-- SYSTEM section	
		pad_clk_i:							in std_logic;
		pad_reset_i:						in std_logic;


		-- GPIO section
		pad_gpout_1_o						: out std_logic;
		
		-- LED section
		pad_LED_o							: out std_logic;
		
		-- CPU section
		pad_CPU_nCS_i:						in std_logic;
		pad_CPU_nOE_i:						in std_logic;
		pad_CPU_nPWE_i:						in std_logic;
		pad_CPU_RDnWR_i:						in std_logic;
		pad_CPU_RDY_o:						out std_logic;
      
		pad_CPU_DQM_3_i:						in std_logic;
		pad_CPU_DQM_2_i:						in std_logic;
		pad_CPU_DQM_1_i:						in std_logic;
		pad_CPU_DQM_0_i:						in std_logic;
      
		pad_CPU_address_25_i:					in std_logic;
		pad_CPU_address_24_i:					in std_logic;
		pad_CPU_address_23_i:					in std_logic;
		pad_CPU_address_22_i:					in std_logic;
		pad_CPU_address_21_i:					in std_logic;
		pad_CPU_address_20_i:					in std_logic;
		pad_CPU_address_19_i:					in std_logic;
		pad_CPU_address_18_i:					in std_logic;
		pad_CPU_address_17_i:					in std_logic;
		pad_CPU_address_16_i:					in std_logic;
		pad_CPU_address_15_i:					in std_logic;
		pad_CPU_address_14_i:					in std_logic;
		pad_CPU_address_13_i:					in std_logic;
		pad_CPU_address_12_i:					in std_logic;
		pad_CPU_address_11_i:					in std_logic;
		pad_CPU_address_10_i:					in std_logic;
		pad_CPU_address_9_i:					in std_logic;
		pad_CPU_address_8_i:					in std_logic;
		pad_CPU_address_7_i:					in std_logic;
		pad_CPU_address_6_i:					in std_logic;
		pad_CPU_address_5_i:					in std_logic;
		pad_CPU_address_4_i:					in std_logic;
		pad_CPU_address_3_i:					in std_logic;
		pad_CPU_address_2_i:					in std_logic;
		pad_CPU_address_1_i:					in std_logic;
		pad_CPU_address_0_i:					in std_logic;
      
		pad_CPU_data_31_io:					inout std_logic;
		pad_CPU_data_30_io:					inout std_logic;
		pad_CPU_data_29_io:					inout std_logic;
		pad_CPU_data_28_io:					inout std_logic;
		pad_CPU_data_27_io:					inout std_logic;
		pad_CPU_data_26_io:					inout std_logic;
		pad_CPU_data_25_io:					inout std_logic;
		pad_CPU_data_24_io:					inout std_logic;
		pad_CPU_data_23_io:					inout std_logic;
		pad_CPU_data_22_io:					inout std_logic;
		pad_CPU_data_21_io:					inout std_logic;
		pad_CPU_data_20_io:					inout std_logic;
		pad_CPU_data_19_io:					inout std_logic;
		pad_CPU_data_18_io:					inout std_logic;
		pad_CPU_data_17_io:					inout std_logic;
		pad_CPU_data_16_io:					inout std_logic;
		pad_CPU_data_15_io:					inout std_logic;
		pad_CPU_data_14_io:					inout std_logic;
		pad_CPU_data_13_io:					inout std_logic;
		pad_CPU_data_12_io:					inout std_logic;
		pad_CPU_data_11_io:					inout std_logic;
		pad_CPU_data_10_io:					inout std_logic;
		pad_CPU_data_9_io:					inout std_logic;
		pad_CPU_data_8_io:					inout std_logic;
		pad_CPU_data_7_io:					inout std_logic;
		pad_CPU_data_6_io:					inout std_logic;
		pad_CPU_data_5_io:					inout std_logic;
		pad_CPU_data_4_io:					inout std_logic;
		pad_CPU_data_3_io:					inout std_logic;
		pad_CPU_data_2_io:					inout std_logic;
		pad_CPU_data_1_io:					inout std_logic;
		pad_CPU_data_0_io:					inout std_logic;
		
		-- SRAM section
		pad_SRAM_DQM_3_o:			out std_logic;
		pad_SRAM_DQM_2_o:			out std_logic;
		pad_SRAM_DQM_1_o:			out std_logic;
		pad_SRAM_DQM_0_o:			out std_logic;
      
		pad_SRAM_address_25_o:				out std_logic;
		pad_SRAM_address_24_o:				out std_logic;
		pad_SRAM_address_23_o:				out std_logic;
		pad_SRAM_address_22_o:				out std_logic;
		pad_SRAM_address_21_o:				out std_logic;
		pad_SRAM_address_20_o:				out std_logic;
		pad_SRAM_address_19_o:				out std_logic;
		pad_SRAM_address_18_o:				out std_logic;
		pad_SRAM_address_17_o:				out std_logic;
		pad_SRAM_address_16_o:				out std_logic;
		pad_SRAM_address_15_o:				out std_logic;
		pad_SRAM_address_14_o:				out std_logic;
		pad_SRAM_address_13_o:				out std_logic;
		pad_SRAM_address_12_o:				out std_logic;
		pad_SRAM_address_11_o:				out std_logic;
		pad_SRAM_address_10_o:		out std_logic;
		pad_SRAM_address_9_o:		out std_logic;
		pad_SRAM_address_8_o:		out std_logic;
		pad_SRAM_address_7_o:		out std_logic;
		pad_SRAM_address_6_o:		out std_logic;
		pad_SRAM_address_5_o:		out std_logic;
		pad_SRAM_address_4_o:		out std_logic;
		pad_SRAM_address_3_o:		out std_logic;
		pad_SRAM_address_2_o:		out std_logic;
		pad_SRAM_address_1_o:		out std_logic;
		pad_SRAM_address_0_o:		out std_logic;
      
		pad_SRAM_data_31_io:					inout std_logic;
		pad_SRAM_data_30_io:					inout std_logic;
		pad_SRAM_data_29_io:					inout std_logic;
		pad_SRAM_data_28_io:					inout std_logic;
		pad_SRAM_data_27_io:					inout std_logic;
		pad_SRAM_data_26_io:					inout std_logic;
		pad_SRAM_data_25_io:					inout std_logic;
		pad_SRAM_data_24_io:					inout std_logic;
		pad_SRAM_data_23_io:					inout std_logic;
		pad_SRAM_data_22_io:					inout std_logic;
		pad_SRAM_data_21_io:			inout std_logic;
		pad_SRAM_data_20_io:			inout std_logic;
		pad_SRAM_data_19_io:			inout std_logic;
		pad_SRAM_data_18_io:			inout std_logic;
		pad_SRAM_data_17_io:			inout std_logic;
		pad_SRAM_data_16_io:			inout std_logic;
		pad_SRAM_data_15_io:			inout std_logic;
		pad_SRAM_data_14_io:			inout std_logic;
		pad_SRAM_data_13_io:			inout std_logic;
		pad_SRAM_data_12_io:			inout std_logic;
		pad_SRAM_data_11_io:			inout std_logic;
		pad_SRAM_data_10_io:			inout std_logic;
		pad_SRAM_data_9_io:			inout std_logic;
		pad_SRAM_data_8_io:			inout std_logic;
		pad_SRAM_data_7_io:			inout std_logic;
		pad_SRAM_data_6_io:			inout std_logic;
		pad_SRAM_data_5_io:			inout std_logic;
		pad_SRAM_data_4_io:			inout std_logic;
		pad_SRAM_data_3_io:			inout std_logic;
		pad_SRAM_data_2_io:			inout std_logic;
		pad_SRAM_data_1_io:			inout std_logic;
		pad_SRAM_data_0_io:			inout std_logic;
      
		pad_SRAM_nCS_o:				out std_logic;
		pad_SRAM_nOE_o:				out std_logic;
		pad_SRAM_nWE_o:				out std_logic
);
end M2M_Test_top;

architecture structural of M2M_Test_top is

   component M2M_Test
      port(
			clk_i:        in    std_logic;              -- Clock input  (active with rising edge)
			reset_i:      in    std_logic;               -- Reset input (synchronous and active high)
			
			is_M2M_finished_o : out std_logic;
			LED_o					: out std_logic;

			-- CPU interface signals --
			CPU_nCS_i :   in    std_logic;
			CPU_nOE_i :   in    std_logic;
			CPU_nPWE_i :  in    std_logic;
			CPU_RDnWR_i : in    std_logic;              -- Read ('1') / Write ('0') signal for the configuration
			CPU_RDY_en_o :   out   std_logic;
			CPU_DQM_i  :  in    std_logic_vector(3 downto 0);
			CPU_address_i: in   std_logic_vector(25 downto 0);
			CPU_data_out_o: out   std_logic_vector(31 downto 0);
			CPU_data_in_i: in   std_logic_vector(31 downto 0);	
			active_CPU_data_out_o: out std_logic; -- tristate control for the CPU_data bus, active high
			-- SRAM interface signals --
			SRAM_DQM_o  : out   std_logic_vector(3 downto 0);
			SRAM_address_o: out std_logic_vector(25 downto 0);
			SRAM_data_in_i: in  std_logic_vector(31 downto 0);
			SRAM_data_out_o: out std_logic_vector(31 downto 0);
			active_SRAM_data_out_o: out std_logic; -- tristate control for the SRAM_data bus, active high
			SRAM_nCS_o: out std_logic;  -- Chip select active low
			SRAM_nOE_o: out std_logic;  -- Output enable active low
			SRAM_nWE_o: out std_logic   -- Write enable active low 
          ); 
   end component;

component in_pad
port(
		I: in std_logic;
		O: out std_logic
);
end component;

component inout_pad
port(
		I: in std_logic;
		O: out std_logic;
		IO: inout std_logic;
		EN: in std_logic
);
end component;

component out_pad
port(
		I: in std_logic;
		O: out std_logic
);
end component;

component out3s_pad
port(
		I: in std_logic;
		O: out std_logic;
		EN: in std_logic
);
end component;


signal clk_s: std_logic;              -- Clock input  (active with rising edge)
signal reset_s: std_logic;               -- Reset input (synchronous and active high)
--signal system_reset_s: std_logic;  -- global Soft-reset
--signal system_enable_s: std_logic; -- global soft-enable

signal is_M2M_finished_s: std_logic;
signal LED_s: std_logic;

signal CPU_nCS_s: std_logic;
signal CPU_nOE_s: std_logic;
signal CPU_nPWE_s: std_logic;
signal CPU_RDnWR_s: std_logic;              -- Read ('1') / Write ('0') signal for the configuration
signal CPU_RDY_EN_s: std_logic;

signal one_s:std_logic;
signal CPU_DQM_s: std_logic_vector(3 downto 0);
signal CPU_address_s: std_logic_vector(25 downto 0);
signal CPU_data_out_s: std_logic_vector(31 downto 0);
signal CPU_data_in_s: std_logic_vector(31 downto 0);	
signal active_CPU_data_out_s: std_logic; -- tristate control for the CPU_data bus, active high

signal SRAM_DQM_s: std_logic_vector(3 downto 0);
signal SRAM_address_s: std_logic_vector(25 downto 0);
signal SRAM_data_in_s: std_logic_vector(31 downto 0);
signal SRAM_data_out_s: std_logic_vector(31 downto 0);
signal active_SRAM_data_out_s: std_logic; -- tristate control for the SRAM_data bus, active high
signal SRAM_nCS_s: std_logic;  -- Chip select active low
signal SRAM_nOE_s: std_logic;  -- Output enable active low
signal SRAM_nWE_s: std_logic;  -- Write enable active low 

signal enable_SRAM_data_out_s: std_logic;



begin

	-- Functional ubichip instance
	M2M_Test_inst: M2M_Test
	port map(
				clk_i => clk_s,
				reset_i => reset_s,
				
			
				is_M2M_finished_o => is_M2M_finished_s,
				LED_o			    	=> LED_s,

				CPU_nCS_i => CPU_nCS_s,
				CPU_nOE_i => CPU_nOE_s,
				CPU_nPWE_i => CPU_nPWE_s,
				CPU_RDnWR_i => CPU_RDnWR_s,
				CPU_RDY_EN_o => CPU_RDY_EN_s,
				CPU_DQM_i => CPU_DQM_s,
				CPU_address_i => CPU_address_s,
				CPU_data_out_o => CPU_data_out_s,
				CPU_data_in_i => CPU_data_in_s,
				active_CPU_data_out_o => active_CPU_data_out_s,
				SRAM_DQM_o => SRAM_DQM_s,
				SRAM_address_o => SRAM_address_s,
				SRAM_data_in_i => SRAM_data_in_s,
				SRAM_data_out_o => SRAM_data_out_s,
				SRAM_nCS_o => SRAM_nCS_s,
				SRAM_nOE_o => SRAM_nOE_s,
				SRAM_nWE_o => SRAM_nWE_s,
				active_SRAM_data_out_o => active_SRAM_data_out_s
	);
	
	-- SYSTEM pins
	clk_in_pin: in_pad
	port map( I => pad_clk_i, O => clk_s);
	
	reset_in_pin: in_pad
	port map( I => pad_reset_i, O => reset_s);
	
	gpio_1_pin: out_pad
	port map( I => is_M2M_finished_s, O => pad_gpout_1_o);
	
	LED_pin: out_pad
	port map( I => LED_s, O => pad_LED_o);

	
	-- CPU VLIO pins
	
	CPU_nCS_pin: in_pad
	port map( I => pad_CPU_nCS_i, O => CPU_nCS_s);
	
	CPU_nOE_pin: in_pad
	port map( I => pad_CPU_nOE_i, O => CPU_nOE_s);
	
	CPU_nPWE_pin: in_pad
	port map( I => pad_CPU_nPWE_i, O => CPU_nPWE_s);
	
	CPU_RDnWR_pin: in_pad
	port map ( I => pad_CPU_RDnWR_i, O => CPU_RDnWR_s);
	
	one_s <= '1';
	CPU_RDY_pad: out3s_pad
	port map( I => one_s, O => pad_CPU_RDY_o, EN => CPU_RDY_EN_s);
			
	CPU_DQM_3_pad: in_pad
	port map( I => pad_CPU_DQM_3_i, O => CPU_DQM_s(3));
	
	CPU_DQM_2_pad: in_pad
	port map( I => pad_CPU_DQM_2_i, O => CPU_DQM_s(2));
	
	CPU_DQM_1_pad: in_pad
	port map( I => pad_CPU_DQM_1_i, O => CPU_DQM_s(1));
	
	CPU_DQM_0_pad: in_pad
	port map( I => pad_CPU_DQM_0_i, O => CPU_DQM_s(0));






   -- CPU address
	
	CPU_address_25_pad: in_pad
	port map( I => pad_CPU_address_25_i, O => CPU_address_s(25));
	
	CPU_address_24_pad: in_pad
	port map( I => pad_CPU_address_24_i, O => CPU_address_s(24));

	CPU_address_23_pad: in_pad
	port map( I => pad_CPU_address_23_i, O => CPU_address_s(23));

	CPU_address_22_pad: in_pad
	port map( I => pad_CPU_address_22_i, O => CPU_address_s(22));

	CPU_address_21_pad: in_pad
	port map( I => pad_CPU_address_21_i, O => CPU_address_s(21));

	CPU_address_20_pad: in_pad
	port map( I => pad_CPU_address_20_i, O => CPU_address_s(20));
	
	CPU_address_19_pad: in_pad
	port map( I => pad_CPU_address_19_i, O => CPU_address_s(19));

	CPU_address_18_pad: in_pad
	port map( I => pad_CPU_address_18_i, O => CPU_address_s(18));

	CPU_address_17_pad: in_pad
	port map( I => pad_CPU_address_17_i, O => CPU_address_s(17));

	CPU_address_16_pad: in_pad
	port map( I => pad_CPU_address_16_i, O => CPU_address_s(16));

	CPU_address_15_pad: in_pad
	port map( I => pad_CPU_address_15_i, O => CPU_address_s(15));
	
	CPU_address_14_pad: in_pad
	port map( I => pad_CPU_address_14_i, O => CPU_address_s(14));

	CPU_address_13_pad: in_pad
	port map( I => pad_CPU_address_13_i, O => CPU_address_s(13));

	CPU_address_12_pad: in_pad
	port map( I => pad_CPU_address_12_i, O => CPU_address_s(12));

	CPU_address_11_pad: in_pad
	port map( I => pad_CPU_address_11_i, O => CPU_address_s(11));

	CPU_address_10_pad: in_pad
	port map( I => pad_CPU_address_10_i, O => CPU_address_s(10));
	
	CPU_address_9_pad: in_pad
	port map( I => pad_CPU_address_9_i, O => CPU_address_s(9));

	CPU_address_8_pad: in_pad
	port map( I => pad_CPU_address_8_i, O => CPU_address_s(8));

	CPU_address_7_pad: in_pad
	port map( I => pad_CPU_address_7_i, O => CPU_address_s(7));

	CPU_address_6_pad: in_pad
	port map( I => pad_CPU_address_6_i, O => CPU_address_s(6));

	CPU_address_5_pad: in_pad
	port map( I => pad_CPU_address_5_i, O => CPU_address_s(5));

	CPU_address_4_pad: in_pad
	port map( I => pad_CPU_address_4_i, O => CPU_address_s(4));
	
	CPU_address_3_pad: in_pad
	port map( I => pad_CPU_address_3_i, O => CPU_address_s(3));

	CPU_address_2_pad: in_pad
	port map( I => pad_CPU_address_2_i, O => CPU_address_s(2));

	CPU_address_1_pad: in_pad
	port map( I => pad_CPU_address_1_i, O => CPU_address_s(1));

	CPU_address_0_pad: in_pad
	port map( I => pad_CPU_address_0_i, O => CPU_address_s(0));





   -- CPU data

	CPU_data_31_pad: inout_pad
	port map( I => CPU_data_out_s(31), O => CPU_data_in_s(31), IO => pad_CPU_data_31_io, EN => active_CPU_data_out_s);

	CPU_data_30_pad: inout_pad
	port map( I => CPU_data_out_s(30), O => CPU_data_in_s(30), IO => pad_CPU_data_30_io, EN => active_CPU_data_out_s);

	CPU_data_29_pad: inout_pad
	port map( I => CPU_data_out_s(29), O => CPU_data_in_s(29), IO => pad_CPU_data_29_io, EN => active_CPU_data_out_s);

	CPU_data_28_pad: inout_pad
	port map( I => CPU_data_out_s(28), O => CPU_data_in_s(28), IO => pad_CPU_data_28_io, EN => active_CPU_data_out_s);

	CPU_data_27_pad: inout_pad
	port map( I => CPU_data_out_s(27), O => CPU_data_in_s(27), IO => pad_CPU_data_27_io, EN => active_CPU_data_out_s);

	CPU_data_26_pad: inout_pad
	port map( I => CPU_data_out_s(26), O => CPU_data_in_s(26), IO => pad_CPU_data_26_io, EN => active_CPU_data_out_s);

	CPU_data_25_pad: inout_pad
	port map( I => CPU_data_out_s(25), O => CPU_data_in_s(25), IO => pad_CPU_data_25_io, EN => active_CPU_data_out_s);

	CPU_data_24_pad: inout_pad
	port map( I => CPU_data_out_s(24), O => CPU_data_in_s(24), IO => pad_CPU_data_24_io, EN => active_CPU_data_out_s);

	CPU_data_23_pad: inout_pad
	port map( I => CPU_data_out_s(23), O => CPU_data_in_s(23), IO => pad_CPU_data_23_io, EN => active_CPU_data_out_s);

	CPU_data_22_pad: inout_pad
	port map( I => CPU_data_out_s(22), O => CPU_data_in_s(22), IO => pad_CPU_data_22_io, EN => active_CPU_data_out_s);

	CPU_data_21_pad: inout_pad
	port map( I => CPU_data_out_s(21), O => CPU_data_in_s(21), IO => pad_CPU_data_21_io, EN => active_CPU_data_out_s);

	CPU_data_20_pad: inout_pad
	port map( I => CPU_data_out_s(20), O => CPU_data_in_s(20), IO => pad_CPU_data_20_io, EN => active_CPU_data_out_s);

	CPU_data_19_pad: inout_pad
	port map( I => CPU_data_out_s(19), O => CPU_data_in_s(19), IO => pad_CPU_data_19_io, EN => active_CPU_data_out_s);

	CPU_data_18_pad: inout_pad
	port map( I => CPU_data_out_s(18), O => CPU_data_in_s(18), IO => pad_CPU_data_18_io, EN => active_CPU_data_out_s);

	CPU_data_17_pad: inout_pad
	port map( I => CPU_data_out_s(17), O => CPU_data_in_s(17), IO => pad_CPU_data_17_io, EN => active_CPU_data_out_s);

	CPU_data_16_pad: inout_pad
	port map( I => CPU_data_out_s(16), O => CPU_data_in_s(16), IO => pad_CPU_data_16_io, EN => active_CPU_data_out_s);

	CPU_data_15_pad: inout_pad
	port map( I => CPU_data_out_s(15), O => CPU_data_in_s(15), IO => pad_CPU_data_15_io, EN => active_CPU_data_out_s);

	CPU_data_14_pad: inout_pad
	port map( I => CPU_data_out_s(14), O => CPU_data_in_s(14), IO => pad_CPU_data_14_io, EN => active_CPU_data_out_s);

	CPU_data_13_pad: inout_pad
	port map( I => CPU_data_out_s(13), O => CPU_data_in_s(13), IO => pad_CPU_data_13_io, EN => active_CPU_data_out_s);

	CPU_data_12_pad: inout_pad
	port map( I => CPU_data_out_s(12), O => CPU_data_in_s(12), IO => pad_CPU_data_12_io, EN => active_CPU_data_out_s);

	CPU_data_11_pad: inout_pad
	port map( I => CPU_data_out_s(11), O => CPU_data_in_s(11), IO => pad_CPU_data_11_io, EN => active_CPU_data_out_s);

	CPU_data_10_pad: inout_pad
	port map( I => CPU_data_out_s(10), O => CPU_data_in_s(10), IO => pad_CPU_data_10_io, EN => active_CPU_data_out_s);

	CPU_data_9_pad: inout_pad
	port map( I => CPU_data_out_s(9), O => CPU_data_in_s(9), IO => pad_CPU_data_9_io, EN => active_CPU_data_out_s);

	CPU_data_8_pad: inout_pad
	port map( I => CPU_data_out_s(8), O => CPU_data_in_s(8), IO => pad_CPU_data_8_io, EN => active_CPU_data_out_s);

	CPU_data_7_pad: inout_pad
	port map( I => CPU_data_out_s(7), O => CPU_data_in_s(7), IO => pad_CPU_data_7_io, EN => active_CPU_data_out_s);

	CPU_data_6_pad: inout_pad
	port map( I => CPU_data_out_s(6), O => CPU_data_in_s(6), IO => pad_CPU_data_6_io, EN => active_CPU_data_out_s);

	CPU_data_5_pad: inout_pad
	port map( I => CPU_data_out_s(5), O => CPU_data_in_s(5), IO => pad_CPU_data_5_io, EN => active_CPU_data_out_s);

	CPU_data_4_pad: inout_pad
	port map( I => CPU_data_out_s(4), O => CPU_data_in_s(4), IO => pad_CPU_data_4_io, EN => active_CPU_data_out_s);

	CPU_data_3_pad: inout_pad
	port map( I => CPU_data_out_s(3), O => CPU_data_in_s(3), IO => pad_CPU_data_3_io, EN => active_CPU_data_out_s);

	CPU_data_2_pad: inout_pad
	port map( I => CPU_data_out_s(2), O => CPU_data_in_s(2), IO => pad_CPU_data_2_io, EN => active_CPU_data_out_s);

	CPU_data_1_pad: inout_pad
	port map( I => CPU_data_out_s(1), O => CPU_data_in_s(1), IO => pad_CPU_data_1_io, EN => active_CPU_data_out_s);

	CPU_data_0_pad: inout_pad
	port map( I => CPU_data_out_s(0), O => CPU_data_in_s(0), IO => pad_CPU_data_0_io, EN => active_CPU_data_out_s);
	
	-- SRAM DQM
	
	SRAM_DQM_3_gpout_65_pad: out_pad
	port map( I => SRAM_DQM_s(3), O => pad_SRAM_DQM_3_o);	

	SRAM_DQM_2_gpout_66_pad: out_pad
	port map( I => SRAM_DQM_s(2), O => pad_SRAM_DQM_2_o);	

	SRAM_DQM_1_gpout_67_pad: out_pad
	port map( I => SRAM_DQM_s(1), O => pad_SRAM_DQM_1_o);

	SRAM_DQM_0_gpout_68_pad: out_pad
	port map( I => SRAM_DQM_s(0), O => pad_SRAM_DQM_0_o);



   -- SRAM address

	SRAM_address_25_pad: out_pad
	port map( I => SRAM_address_s(25), O => pad_SRAM_address_25_o);

	SRAM_address_24_pad: out_pad
	port map( I => SRAM_address_s(24), O => pad_SRAM_address_24_o);

	SRAM_address_23_pad: out_pad
	port map( I => SRAM_address_s(23), O => pad_SRAM_address_23_o);

	SRAM_address_22_pad: out_pad
	port map( I => SRAM_address_s(22), O => pad_SRAM_address_22_o);

	SRAM_address_21_pad: out_pad
	port map( I => SRAM_address_s(21), O => pad_SRAM_address_21_o);

	SRAM_address_20_pad: out_pad
	port map( I => SRAM_address_s(20), O => pad_SRAM_address_20_o);

	SRAM_address_19_pad: out_pad
	port map( I => SRAM_address_s(19), O => pad_SRAM_address_19_o);

	SRAM_address_18_pad: out_pad
	port map( I => SRAM_address_s(18), O => pad_SRAM_address_18_o);

	SRAM_address_17_pad: out_pad
	port map( I => SRAM_address_s(17), O => pad_SRAM_address_17_o);

	SRAM_address_16_pad: out_pad
	port map( I => SRAM_address_s(16), O => pad_SRAM_address_16_o);

	SRAM_address_15_pad: out_pad
	port map( I => SRAM_address_s(15), O => pad_SRAM_address_15_o);

	SRAM_address_14_pad: out_pad
	port map( I => SRAM_address_s(14), O => pad_SRAM_address_14_o);

	SRAM_address_13_pad: out_pad
	port map( I => SRAM_address_s(13), O => pad_SRAM_address_13_o);

	SRAM_address_12_pad: out_pad
	port map( I => SRAM_address_s(12), O => pad_SRAM_address_12_o);

	SRAM_address_11_pad: out_pad
	port map( I => SRAM_address_s(11), O => pad_SRAM_address_11_o);
	
	SRAM_address_10_pad: out_pad
	port map( I => SRAM_address_s(10), O => pad_SRAM_address_10_o);
									
	SRAM_address_9_pad: out_pad
	port map( I => SRAM_address_s(9), O => pad_SRAM_address_9_o);

	SRAM_address_8_pad: out_pad
	port map( I => SRAM_address_s(8), O => pad_SRAM_address_8_o);

	SRAM_address_7_pad: out_pad
	port map( I => SRAM_address_s(7), O => pad_SRAM_address_7_o);
	
	SRAM_address_6_pad: out_pad
	port map( I => SRAM_address_s(6), O => pad_SRAM_address_6_o);

	SRAM_address_5_pad: out_pad
	port map( I => SRAM_address_s(5), O => pad_SRAM_address_5_o);

	SRAM_address_4_pad: out_pad
	port map( I => SRAM_address_s(4), O => pad_SRAM_address_4_o);

	SRAM_address_3_pad: out_pad
	port map( I => SRAM_address_s(3), O => pad_SRAM_address_3_o);

	SRAM_address_2_pad: out_pad
	port map( I => SRAM_address_s(2), O => pad_SRAM_address_2_o);

	SRAM_address_1_pad: out_pad
	port map( I => SRAM_address_s(1), O => pad_SRAM_address_1_o);

	SRAM_address_0_pad: out_pad
	port map( I => SRAM_address_s(0), O => pad_SRAM_address_0_o);







   -- SRAM data
   
   enable_SRAM_data_out_s <=	active_SRAM_data_out_s;

	SRAM_data_31_pad: inout_pad
	port map(I => SRAM_data_out_s(31), O => SRAM_data_in_s(31), IO => pad_SRAM_data_31_io, EN => enable_SRAM_data_out_s);

	SRAM_data_30_pad: inout_pad
	port map(I => SRAM_data_out_s(30), O => SRAM_data_in_s(30), IO => pad_SRAM_data_30_io, EN => enable_SRAM_data_out_s);

	SRAM_data_29_pad: inout_pad
	port map(I => SRAM_data_out_s(29), O => SRAM_data_in_s(29), IO => pad_SRAM_data_29_io, EN => enable_SRAM_data_out_s);

	SRAM_data_28_pad: inout_pad
	port map(I => SRAM_data_out_s(28), O => SRAM_data_in_s(28), IO => pad_SRAM_data_28_io, EN => enable_SRAM_data_out_s);

	SRAM_data_27_pad: inout_pad
	port map(I => SRAM_data_out_s(27), O => SRAM_data_in_s(27), IO => pad_SRAM_data_27_io, EN => enable_SRAM_data_out_s);

	SRAM_data_26_pad: inout_pad
	port map(I => SRAM_data_out_s(26), O => SRAM_data_in_s(26), IO => pad_SRAM_data_26_io, EN => enable_SRAM_data_out_s);

	SRAM_data_25_pad: inout_pad
	port map(I => SRAM_data_out_s(25), O => SRAM_data_in_s(25), IO => pad_SRAM_data_25_io, EN => enable_SRAM_data_out_s);

	SRAM_data_24_pad: inout_pad
	port map(I => SRAM_data_out_s(24), O => SRAM_data_in_s(24), IO => pad_SRAM_data_24_io, EN => enable_SRAM_data_out_s);

	SRAM_data_23_pad: inout_pad
	port map(I => SRAM_data_out_s(23), O => SRAM_data_in_s(23), IO => pad_SRAM_data_23_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_22_pad: inout_pad
	port map(I => SRAM_data_out_s(22), O => SRAM_data_in_s(22), IO => pad_SRAM_data_22_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_21_pad: inout_pad
	port map(I => SRAM_data_out_s(21), O => SRAM_data_in_s(21), IO => pad_SRAM_data_21_io, EN => enable_SRAM_data_out_s);

	SRAM_data_20_pad: inout_pad
	port map(I => SRAM_data_out_s(20), O => SRAM_data_in_s(20), IO => pad_SRAM_data_20_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_19_pad: inout_pad
	port map(I => SRAM_data_out_s(19), O => SRAM_data_in_s(19), IO => pad_SRAM_data_19_io, EN => enable_SRAM_data_out_s);

	SRAM_data_18_pad: inout_pad
	port map(I => SRAM_data_out_s(18), O => SRAM_data_in_s(18), IO => pad_SRAM_data_18_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_17_pad: inout_pad
	port map(I => SRAM_data_out_s(17), O => SRAM_data_in_s(17), IO => pad_SRAM_data_17_io, EN => enable_SRAM_data_out_s);

	SRAM_data_16_pad: inout_pad
	port map(I => SRAM_data_out_s(16), O => SRAM_data_in_s(16), IO => pad_SRAM_data_16_io, EN => enable_SRAM_data_out_s);

	SRAM_data_15_pad: inout_pad
	port map(I => SRAM_data_out_s(15), O => SRAM_data_in_s(15), IO => pad_SRAM_data_15_io, EN => enable_SRAM_data_out_s);

	SRAM_data_14_pad: inout_pad
	port map(I => SRAM_data_out_s(14), O => SRAM_data_in_s(14), IO => pad_SRAM_data_14_io, EN => enable_SRAM_data_out_s);

	SRAM_data_13_pad: inout_pad
	port map(I => SRAM_data_out_s(13), O => SRAM_data_in_s(13), IO => pad_SRAM_data_13_io, EN => enable_SRAM_data_out_s);	

	SRAM_data_12_pad: inout_pad
	port map(I => SRAM_data_out_s(12), O => SRAM_data_in_s(12), IO => pad_SRAM_data_12_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_11_pad: inout_pad
	port map(I => SRAM_data_out_s(11), O => SRAM_data_in_s(11), IO => pad_SRAM_data_11_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_10_pad: inout_pad
	port map(I => SRAM_data_out_s(10), O => SRAM_data_in_s(10), IO => pad_SRAM_data_10_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_9_pad: inout_pad
	port map(I => SRAM_data_out_s(9), O => SRAM_data_in_s(9), IO => pad_SRAM_data_9_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_8_pad: inout_pad
	port map(I => SRAM_data_out_s(8), O => SRAM_data_in_s(8), IO => pad_SRAM_data_8_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_7_pad: inout_pad
	port map(I => SRAM_data_out_s(7), O => SRAM_data_in_s(7), IO => pad_SRAM_data_7_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_6_pad: inout_pad
	port map(I => SRAM_data_out_s(6), O => SRAM_data_in_s(6), IO => pad_SRAM_data_6_io, EN => enable_SRAM_data_out_s);

	SRAM_data_5_pad: inout_pad
	port map(I => SRAM_data_out_s(5), O => SRAM_data_in_s(5), IO => pad_SRAM_data_5_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_4_pad: inout_pad
	port map(I => SRAM_data_out_s(4), O => SRAM_data_in_s(4), IO => pad_SRAM_data_4_io, EN => enable_SRAM_data_out_s);

	SRAM_data_3_pad: inout_pad
	port map(I => SRAM_data_out_s(3), O => SRAM_data_in_s(3), IO => pad_SRAM_data_3_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_2_pad: inout_pad
	port map(I => SRAM_data_out_s(2), O => SRAM_data_in_s(2), IO => pad_SRAM_data_2_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_1_pad: inout_pad
	port map(I => SRAM_data_out_s(1), O => SRAM_data_in_s(1), IO => pad_SRAM_data_1_io, EN => enable_SRAM_data_out_s);
	
	SRAM_data_0_pad: inout_pad
	port map(I => SRAM_data_out_s(0), O => SRAM_data_in_s(0), IO => pad_SRAM_data_0_io, EN => enable_SRAM_data_out_s);
	





	SRAM_nCS_pad: out_pad
	port map( I => SRAM_nCS_s, O => pad_SRAM_nCS_o);
	
								
	SRAM_nOE_pad: out_pad
	port map( I => SRAM_nOE_s, O => pad_SRAM_nOE_o);
	

	SRAM_nWE_pad: out_pad
	port map( I => SRAM_nWE_s, O => pad_SRAM_nWE_o);
	
								
	
end structural;

		
