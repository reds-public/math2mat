-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- file:    pkg_M2M_ctrl.vhd
-- creator: Mikhail Nagoga
--          name.surname@heig-vd.ch
-- date:    November 2010
--
-- description: The package for ISE
--
-- version: 1.0  | Initial version

----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use Ieee.Numeric_Std.all;


package pkg_M2M_ctrl is
   
   constant FIFOSIZE_c     : positive := 4;
   constant inc_PTR_SRAM_c : positive := 4;

end package pkg_M2M_ctrl;

package body pkg_M2M_ctrl is
end pkg_M2M_ctrl;
