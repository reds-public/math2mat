-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fifo_Std_ubidule is
	generic (
		FIFOSIZE  : integer := 32;
		DATAWIDTH : integer := 32
	);
	port (
		Clock_i      : in    std_logic;
		Reset_i      : in    std_logic;
		Wr_Req_i     : in    std_logic;
		Rd_Req_i     : in    std_logic;
		Data_i       : in    std_logic_vector(DATAWIDTH-1 downto 0);
		Fifo_Full_o  : out   std_logic;
		Fifo_Empty_o : out   std_logic;
		Data_o       : out   std_logic_vector(DATAWIDTH-1 downto 0)
	);
end fifo_Std_ubidule;

architecture behave of fifo_Std_ubidule is

	type   fifo_type is array(0 to FIFOSIZE-1) of std_logic_vector(DATAWIDTH-1 downto 0);
	signal fifo: fifo_type;
	signal fifo_counter_in: integer range 0 to FIFOSIZE-1;
	signal fifo_counter_out: integer range 0 to FIFOSIZE-1;
	signal fifo_full: std_logic;
	signal fifo_empty: std_logic;
	signal fifo_rd: std_logic;
	signal fifo_wr: std_logic;
	signal fifo_out: std_logic_vector(DATAWIDTH-1 downto 0);

begin

	Data_o       <= fifo_out;
	Fifo_Empty_o <= fifo_empty;

	Fifo_Full_o  <= fifo_full;

	fifo_wr      <= Wr_Req_i;
	fifo_rd      <= Rd_Req_i;
	
	fifo_out     <= fifo(fifo_counter_out);
    
	process(Clock_i, Reset_i)
	begin
		if Reset_i = '1' then
			fifo_counter_in  <= 0;
			fifo_counter_out <= 0;
			fifo_full        <= '0';
			fifo_empty       <= '1';
		--	fifo_out<=(others=>'0');
		elsif rising_edge(Clock_i) then
			if fifo_rd = '1' and fifo_empty = '0' then
				if fifo_wr = '0' then
					if ((fifo_counter_out+1) mod FIFOSIZE) = fifo_counter_in then
						fifo_empty <= '1';
					end if;
					fifo_full <= '0';
				end if;
				fifo_counter_out <= (fifo_counter_out+1) mod FIFOSIZE;
			--	fifo_out<=fifo(fifo_counter_out);
			end if;
			if fifo_wr = '1' and fifo_full = '0' then
				fifo_empty <= '0';
				if ((fifo_counter_in+1) mod FIFOSIZE) = fifo_counter_out then
					fifo_full <= '1';
				end if;
				fifo_counter_in       <= (fifo_counter_in+1) mod FIFOSIZE;
				fifo(fifo_counter_in) <= Data_i;
			end if;
		end if;
	end process;

end behave;
