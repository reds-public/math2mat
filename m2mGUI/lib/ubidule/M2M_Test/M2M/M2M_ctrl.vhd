-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

	----------------------------------------------------------------------------
--
-- file:    M2M_ctrl.vhd
-- creator: Mikhail Nagoga (Михаил Нагога)
--          name.surname@heig-vd.ch
-- date:    Octobre 2010
--
-- description: M2M controler
--
-- version: 1.0  | Initial version
----------------------------------------------------------------------------

library Ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use ieee.std_logic_unsigned.all;

-- define types like M2M_Data etc.
use work.m2m_inout_definition_pkg.all;

-- define FIFOSIZE_c, inc_PTR_c
use work.pkg_M2M_ctrl.all;


entity M2M_ctrl is
 port(
   clk_i            : in  Std_Logic; -- Master clock.
   reset_i          : in  Std_Logic; -- Asychronus general reset.

   conf_en_i          : in  std_logic; -- enable configuration of registres
   data_conf_in_i     : in std_logic_vector(31 downto 0); -- configuration data
   data_conf_out_o    : out std_logic_vector(31 downto 0);

   CPU_RDnWR_i:in std_logic;
   CPU_address_i : in std_logic_vector(25 downto 0);
	
	-- GPIO & LED interface
	is_M2M_finished_o	: out std_logic;
	LED_o		: out std_logic;

   -- SRAM interface
   SRAM_INPUT_i: in std_logic_vector(31 downto 0); -- SRAM data bus
   SRAM_OUTPUT_o: out std_logic_vector(31 downto 0); -- SRAM data bus
   SRAM_A_o: out std_logic_vector(25 downto 0); --SRAM address bus
   SRAM_DQM_o: out std_logic_vector(3 downto 0); -- Byte selection lines
   SRAM_nCS_o: out std_logic;  -- Chip enable
   SRAM_nOE_o: out std_logic;  -- Output enable active low
   SRAM_nWE_o: out std_logic;  -- Write enable active low

   -- M2M interface
   M2M_Input_o        : out M2M_Data(M2M_Nb_Inputs_c-1 downto 0);
   M2M_Input_Valid_o  : out M2M_Control(M2M_Nb_Inputs_c-1 downto 0);
   M2M_Input_Ready_i  : in M2M_Control(M2M_Nb_Inputs_c-1 downto 0);

   M2M_Result_i       : in M2M_Data(M2M_Nb_Outputs_c-1 downto 0);
   M2M_Result_Valid_i : in M2M_Control(M2M_Nb_Outputs_c-1 downto 0);
   M2M_Result_Ready_o : out M2M_Control(M2M_Nb_Outputs_c-1 downto 0)
   );
end M2M_ctrl;


architecture behavioral of M2M_ctrl is

   component fifo_Std_ubidule
      generic(
         FIFOSIZE  : integer := FIFOSIZE_c;
         DATAWIDTH : integer := 32
      );
      port(
         Clock_i      : in    std_logic;
         Reset_i      : in    std_logic;
         Wr_Req_i     : in    std_logic;
         Rd_Req_i     : in    std_logic;
         Data_i       : in    std_logic_vector(DATAWIDTH-1 downto 0);
         Fifo_Full_o  : out   std_logic;
         Fifo_Empty_o : out   std_logic;
         Data_o       : out   std_logic_vector(DATAWIDTH-1 downto 0)
      );
   end component;


   component interface_manager
      port(
         clk_i                : in std_logic;
         reset_i              : in std_logic;

         ready_i              : in std_logic;
         fifo_empty_i         : in std_logic;
         all_fifos_in_full_i  : in std_logic;

         rd_fifo_o            : out std_logic;
         valid_o              : out std_logic
      );
   end component;


   constant nb_inputs_c : std_logic_vector  := std_logic_vector(to_unsigned(M2M_Input_o'length,8));
   constant nb_outputs_c : std_logic_vector := std_logic_vector(to_unsigned(M2M_Result_i'length,8));

   -- number of tests to do, configurable via VLIO interface
   signal nb_test_reg : std_logic_vector(31 downto 0);
   signal size_fifo_reg : std_logic_vector(31 downto 0);
   signal latence_reg : std_logic_vector(31 downto 0);

   -- counters
   signal nb_test_cnt : std_logic_vector(31 downto 0);
   signal size_fifo_cnt : std_logic_vector(31 downto 0);
   signal nb_inputs_cnt : std_logic_vector(7 downto 0);
   signal nb_outputs_cnt : std_logic_vector(7 downto 0);
   signal nb_outputs_rev_cnt : std_logic_vector(7 downto 0);
   signal latence_cnt : std_logic_vector(31 downto 0);


   -- counter's initialization 
   signal init_nb_test_cnt_s : std_logic;
   signal init_size_fifo_cnt_s : std_logic;
   signal init_nb_inputs_cnt_s : std_logic;
   signal init_nb_outputs_cnt_s : std_logic;
   signal init_latence_cnt_s : std_logic;
   
   -- inc/dec counters
   signal dec_nb_test_cnt : std_logic_vector(31 downto 0);
   signal dec_size_fifo_cnt : std_logic_vector(31 downto 0);
   signal dec_nb_inputs_cnt: std_logic_vector(7 downto 0);
   signal dec_nb_outputs_cnt: std_logic_vector(7 downto 0);
   signal inc_latence_cnt   : std_logic_vector(31 downto 0);

   -- signals for activate the counters
   signal dec_nb_test_cnt_s : std_logic;
   signal dec_size_fifo_cnt_s : std_logic;
   signal dec_nb_inputs_cnt_s : std_logic;
   signal dec_nb_outputs_cnt_s : std_logic;
   signal inc_latence_cnt_s   : std_logic;

   signal wr_latence_counter_reg_s : std_logic;

   signal start_reg: std_logic_vector(0 downto 0); -- start condition (rising edge)
   signal reset_start_reg_s : std_logic;
   

   type state is (WAIT_START_L,
                  WAIT_START_H,
                  RESET_START_REG,

						WRITE_LATENCE_REG,

                  IS_NB_TEST_CNT_ZERO,

                  IS_CNT_SIZE_FIFO_ZERO,
                  IS_NB_INPUTS_ZERO,
                  READ_SRAM,
                  DEC_NB_INPUTS,
                  DEC_CNT_SIZE_FIFO,

                  WAIT_FIFO_OUTPUTS_FULL,

				  TRUC,
				  TRUC2,
                  IS_CNT_SIZE_FIFO_ZERO_2,
                  IS_NB_OUTPUTS_ZERO_2,
                  WRITE_SRAM,
                  DEC_NB_OUTPUTS_2,
                  DEC_CNT_SIZE_FIFO_2
                 ); 

   signal current_state : state;
   signal next_state : state;


   -- pointers to the SRAM
   signal PTR_reg : std_logic_vector(25 downto 0);
   signal PTR2_init_reg : std_logic_vector(25 downto 0);
   signal PTR2_reg : std_logic_vector(25 downto 0);
   signal PTR_incremented_reg : std_logic_vector(25 downto 0);
   signal PTR_decremented_reg : std_logic_vector(25 downto 0);
   signal PTR2_incremented_reg : std_logic_vector(25 downto 0);
   signal inc_PTR_s : std_logic;
   signal dec_PTR_s : std_logic;
   signal inc_PTR2_s : std_logic;
   signal init_PTR_s : std_logic;
   signal init_PTR2_s : std_logic;

   signal ptr2_active_s : std_logic;
   
   signal read_SRAM_s  : std_logic;
   signal write_SRAM_s : std_logic;

   signal SRAM_nCS_s : std_logic;
   
   signal SRAM_data_reg : std_logic_vector(31 downto 0);


------------------------------------
---------   FIFO signals   ---------
------------------------------------

   signal Wr_FIFO_In_s     : std_logic_vector(M2M_Nb_Inputs_c-1 downto 0);
   signal Rd_FIFO_In_s     : std_logic_vector(M2M_Nb_Inputs_c-1 downto 0);
   signal FIFO_In_full_s   : std_logic_vector(M2M_Nb_Inputs_c-1 downto 0);
   signal FIFO_In_empty_s  : std_logic_vector(M2M_Nb_Inputs_c-1 downto 0);

   signal Wr_FIFO_Out_s     : std_logic_vector(M2M_Nb_Outputs_c-1 downto 0);
   signal Rd_FIFO_Out_s     : std_logic_vector(M2M_Nb_Outputs_c-1 downto 0);
   signal FIFO_Out_full_s   : std_logic_vector(M2M_Nb_Outputs_c-1 downto 0);
   signal FIFO_Out_empty_s  : std_logic_vector(M2M_Nb_Outputs_c-1 downto 0);

   signal all_fifos_output_full_s : std_logic;
   signal all_fifos_in_full_s    : std_logic;

   -- output of all output FIFOs
   signal FIFO_Out_data_s : M2M_Data(M2M_Nb_Outputs_c-1 downto 0);
   -- multiplexed last signal, the selector is nb_outputs_rev_cnt
   signal FIFO_Out_multiplexed_s : std_logic_vector(31 downto 0);


   signal sel_reg : std_logic_vector(M2M_Nb_Inputs_c-1 downto 0);
   signal shift_sel_reg_s : std_logic;

   signal sel_out_reg : std_logic_vector(M2M_Nb_Outputs_c-1 downto 0);
   signal shift_sel_out_reg_s : std_logic;

   signal en_write_fifos_s : std_logic;
   signal en_read_fifos_s  : std_logic;

begin

   FIFO_In_gen : for i in 0 to M2M_Nb_Inputs_c-1 generate
      FIFO_In_inst : fifo_Std_ubidule
      port map(
         Clock_i        => clk_i,
         Reset_i        => reset_i,
         Wr_Req_i       => Wr_FIFO_In_s(i),
         Rd_Req_i       => Rd_FIFO_In_s(i),
         Data_i         => SRAM_data_reg, 
         Fifo_Full_o    => FIFO_In_full_s(i),
         Fifo_Empty_o   => FIFO_In_empty_s(i),
         Data_o         => M2M_Input_o(i)
      );
   end generate;

   Interface_Manager_gen : for i in 0 to M2M_Nb_Inputs_c-1 generate
      i_m_inst : interface_manager
      port map(
         clk_i          => clk_i,
         reset_i        => reset_i,

         ready_i        => M2M_Input_Ready_i(i),
         fifo_empty_i   => FIFO_In_empty_s(i),
         all_fifos_in_full_i    => all_fifos_in_full_s,
      
         rd_fifo_o      => Rd_FIFO_In_s(i),
         valid_o        => M2M_Input_Valid_o(i)
      );
   end generate;

   Wr_FIFO_signal_gen : for i in 0 to M2M_Nb_Inputs_c-1 generate
      Wr_FIFO_In_s(i) <= sel_reg(i) and en_write_fifos_s;
   end generate;

   FIFO_Out_gen : for i in 0 to M2M_Nb_Outputs_c-1 generate
      FIFO_Out_inst : fifo_Std_ubidule
      port map(
         Clock_i        => clk_i,
         Reset_i        => reset_i,
         Wr_Req_i       => Wr_FIFO_Out_s(i),
         Rd_Req_i       => Rd_FIFO_Out_s(i),
         Data_i         => M2M_Result_i(i), 
         Fifo_Full_o    => FIFO_Out_full_s(i),
         Fifo_Empty_o   => FIFO_Out_empty_s(i),
         Data_o         => FIFO_Out_data_s(i)
      );
   end generate;

   Wr_FIFO_out_gen : for i in 0 to M2M_Nb_Outputs_c-1 generate
      Wr_FIFO_Out_s(i) <= M2M_Result_Valid_i(i);
   end generate;

   nb_outputs_rev_cnt <= M2M_Nb_Outputs_c - nb_outputs_cnt;

   gen_mux : process (FIFO_Out_data_s, nb_outputs_rev_cnt) is
   begin
      FIFO_Out_multiplexed_s <= (others => '0');
      for i in FIFO_Out_data_s'range loop
         if i = to_integer(unsigned(nb_outputs_rev_cnt)) then
            FIFO_Out_multiplexed_s <= FIFO_Out_data_s(i);
         end if;
      end loop;
   end process gen_mux;

--FIFO_Out_multiplexed_s <= FIFO_Out_data_s(to_integer(unsigned(nb_outputs_rev_cnt)));

   process(FIFO_Out_full_s)
      variable all_fifos_output_full_v : Std_logic := '1';
   begin
      all_fifos_output_full_v := FIFO_Out_full_s(0);

      for I in 0 to FIFO_Out_full_s'length-1 loop
         all_fifos_output_full_v := all_fifos_output_full_v and FIFO_Out_full_s(I);
      end loop;
      all_fifos_output_full_s <= all_fifos_output_full_v;
   end process;


   process(FIFO_In_full_s)
      variable all_fifos_in_full_v : std_logic := '1';
   begin
      all_fifos_in_full_v := FIFO_In_full_s(0);

      for I in 0 to FIFO_In_full_s'length-1 loop
         all_fifos_in_full_v := all_fifos_in_full_v and FIFO_In_full_s(I);
      end loop;
      all_fifos_in_full_s <= all_fifos_in_full_v;
   end process;


   M2M_Result_Ready_gen : for i in 0 to M2M_Nb_Outputs_c-1 generate
      M2M_Result_Ready_o(i) <= '1';
   end generate;



------------------------------------------------
------------------------------------------------

   Shift_register_sel : process(clk_i, reset_i)
   begin
       if reset_i = '1' then
         sel_reg <= std_logic_vector( to_unsigned(1,M2M_Nb_Inputs_c)); 
       elsif clk_i'event and clk_i='1' then
           if  shift_sel_reg_s = '1' then
               if sel_reg( M2M_Nb_Inputs_c-1 ) = '0' then
                  sel_reg <= sel_reg( (M2M_Nb_Inputs_c-1 - 1) downto 0) & '0'; -- left shift, LSB replaced by 0
               else
                  sel_reg <= sel_reg( (M2M_Nb_Inputs_c-1 - 1) downto 0) & '1'; -- left shift, LSB replaced by 1
               end if;
           end if; 
       end if;
   end process Shift_register_sel;

   Shift_register_sel_out : process(clk_i, reset_i)
   begin
       if reset_i = '1' then
         sel_out_reg <= std_logic_vector( to_unsigned(1,M2M_Nb_Outputs_c)); 
       elsif clk_i'event and clk_i='1' then
           if  shift_sel_out_reg_s = '1' then
               if sel_out_reg( M2M_Nb_Outputs_c-1 ) = '0' then
                  sel_out_reg <= sel_out_reg( (M2M_Nb_Outputs_c-1 - 1) downto 0) & '0'; -- left shift, LSB replaced by 0
               else
                  sel_out_reg <= sel_out_reg( (M2M_Nb_Outputs_c-1 - 1) downto 0) & '1'; -- left shift, LSB replaced by 1
               end if;
           end if; 
       end if;
   end process Shift_register_sel_out;


------------------------------------------------
------------------------------------------------

   Number_test_register : process(clk_i, reset_i)
       begin
       if reset_i = '1' then
         nb_test_reg <= (others => '0'); 
       elsif clk_i'event and clk_i='1' then
           if (conf_en_i = '1' and CPU_address_i(15 downto 11) = "11100" and CPU_RDnWR_i = '0') then
              nb_test_reg <= data_conf_in_i;
           end if; 
       end if;
    end process Number_test_register;


   Number_test_counter : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         nb_test_cnt <= (others => '0'); 
      elsif clk_i'event and clk_i='1' then
         if init_nb_test_cnt_s = '1' then
            nb_test_cnt <= nb_test_reg; -- initialization
         elsif dec_nb_test_cnt_s = '1' then
            nb_test_cnt <= dec_nb_test_cnt; -- decrementation
         end if;
      end if;
   end process Number_test_counter;

   dec_nb_test_cnt <= nb_test_cnt - 1;

------------------------------------------------
------------------------------------------------

   Size_fifo_register : process(clk_i, reset_i)
       begin
       if reset_i = '1' then
         size_fifo_reg <= std_logic_vector(to_unsigned(FIFOSIZE_c,32)); 
--        elsif clk_i'event and clk_i='1' then
--            if (conf_en = '1' and CPU_address_i(15 downto 11) = "11100" and CPU_RDnWR_i = '0') then
--               size_fifo_reg <= data_conf_in;
--            end if; 
       end if;
    end process Size_fifo_register;


   Size_fifo_counter : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         size_fifo_cnt <= (others => '0'); 
      elsif clk_i'event and clk_i='1' then
         if init_size_fifo_cnt_s = '1' then
            size_fifo_cnt <= size_fifo_reg; -- initialization
         elsif dec_size_fifo_cnt_s = '1' then
            size_fifo_cnt <= dec_size_fifo_cnt; -- decrementation
         end if;
      end if;
   end process Size_fifo_counter;

   dec_size_fifo_cnt <= size_fifo_cnt - 1;

------------------------------------------------
------------------------------------------------


   latence_register : process(clk_i, reset_i)
       begin
       if reset_i = '1' then
         latence_reg <= (others => '0'); 
       elsif clk_i'event and clk_i='1' then
           if wr_latence_counter_reg_s = '1' then
              latence_reg <= latence_cnt;
           end if; 
       end if;
    end process latence_register;

   latence_counter : process(clk_i, reset_i)
       begin
       if reset_i = '1' then
         latence_cnt <= (others => '0'); 
      elsif clk_i'event and clk_i='1' then
         if init_latence_cnt_s = '1' then
            latence_cnt <= (others => '0'); -- initialization
         elsif inc_latence_cnt_s = '1' then
            latence_cnt  <= inc_latence_cnt; -- incrementation
         end if;
      end if;
    end process latence_counter;

   inc_latence_cnt <= latence_cnt + 1;

------------------------------------------------
------------------------------------------------

   Number_inputs_counter : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         nb_inputs_cnt <= (others => '0'); 
      elsif clk_i'event and clk_i='1' then
         if init_nb_inputs_cnt_s = '1' then
            nb_inputs_cnt <= nb_inputs_c; -- initialization
         elsif dec_nb_inputs_cnt_s = '1' then
            nb_inputs_cnt  <= dec_nb_inputs_cnt; -- decrementation
         end if;
      end if;
   end process Number_inputs_counter;

   dec_nb_inputs_cnt <= nb_inputs_cnt - 1;

------------------------------------------------
------------------------------------------------


   Number_outputs_counter : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         nb_outputs_cnt <= (others => '0'); 
      elsif clk_i'event and clk_i='1' then
         if init_nb_outputs_cnt_s = '1' then
            nb_outputs_cnt <= nb_outputs_c; -- initialization
         elsif dec_nb_outputs_cnt_s = '1' then
            nb_outputs_cnt  <= dec_nb_outputs_cnt; -- decrementation
         end if;
      end if;
   end process Number_outputs_counter;

   dec_nb_outputs_cnt <= nb_outputs_cnt - 1;
    

   Start_register : process(clk_i, reset_i)
       begin
       if reset_i = '1' then
         start_reg <= (others => '0');
       elsif clk_i'event and clk_i='1' then
         if reset_start_reg_s = '1' then
            start_reg <= (others => '0'); --reset
         elsif (conf_en_i = '1' and CPU_address_i(15 downto 11) = "11111" and CPU_RDnWR_i = '0') then
            start_reg <= data_conf_in_i(0 downto 0); -- load CPU data
         end if;
       end if;
    end process Start_register;

 
    Register_output_decoder : process(CPU_address_i, nb_test_reg, latence_reg, start_reg)
       begin

          data_conf_out_o <= (others => '0');
              
          if (CPU_address_i(15 downto 11) = "11100") then -- M2M_CTRL_REGS_NB_TESTS          0xE000
             data_conf_out_o <= nb_test_reg;
          elsif (CPU_address_i(15 downto 11) = "11101") then -- M2M_CTRL_LATENCE 		      0xE800
             data_conf_out_o <= latence_reg;
          elsif (CPU_address_i(15 downto 11) = "11110") then -- M2M_CTRL_PTR                 0xF000
             data_conf_out_o(25 downto 0) <= PTR2_init_reg;
          elsif (CPU_address_i(15 downto 11) = "11111") then -- M2M_CTRL_REGS_START          0xF400
             data_conf_out_o <= X"0000000" & "000" & start_reg;
          end if; 
        
       end process Register_output_decoder;
       
       
       
       
   Next_State_Decoder: process(
                                 current_state,

                                 start_reg,

                                 nb_test_cnt,
                                 nb_inputs_cnt,
                                 nb_outputs_cnt,

                                 all_fifos_output_full_s
                              )
   begin

         next_state <= WAIT_START_L;


         case current_state is

            when WAIT_START_L => 
               if start_reg(0) = '1' then
                  next_state <= WAIT_START_L;
               else
                  next_state <= WAIT_START_H;
               end if;
               
            when WAIT_START_H =>
               if start_reg(0) = '1' then
                  next_state <= RESET_START_REG;
               else
                  next_state <= WAIT_START_H;
               end if;

            when RESET_START_REG =>
               next_state <= IS_NB_TEST_CNT_ZERO;
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
				when WRITE_LATENCE_REG =>
					next_state <= WAIT_START_L;

 
            when IS_NB_TEST_CNT_ZERO =>
               if nb_test_cnt = 0 then
                  next_state <= WRITE_LATENCE_REG;
               else
                  next_state <= IS_CNT_SIZE_FIFO_ZERO;
               end if;
               
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

            when IS_CNT_SIZE_FIFO_ZERO =>
               if size_fifo_cnt = 0 then
                  next_state <= WAIT_FIFO_OUTPUTS_FULL;
               else
                  next_state <= IS_NB_INPUTS_ZERO;
               end if;

            when IS_NB_INPUTS_ZERO =>
               if nb_inputs_cnt = 0 then
                  next_state <= DEC_CNT_SIZE_FIFO;
               else
                  next_state <= READ_SRAM;
               end if;

            when READ_SRAM =>
               next_state <= DEC_NB_INPUTS;

            when DEC_NB_INPUTS =>
               next_state <= IS_NB_INPUTS_ZERO;

            when DEC_CNT_SIZE_FIFO =>
               next_state <= IS_CNT_SIZE_FIFO_ZERO;

---------------------------------------------------------
---------------------------------------------------------

            when WAIT_FIFO_OUTPUTS_FULL =>
               if all_fifos_output_full_s = '0' then
                  next_state <= WAIT_FIFO_OUTPUTS_FULL;
               else
                  next_state <= IS_CNT_SIZE_FIFO_ZERO_2;
               end if;

---------------------------------------------------------
---------------------------------------------------------
			when TRUC =>
				next_state <= TRUC2;

			when TRUC2 =>
				next_state <= IS_NB_TEST_CNT_ZERO;



            when IS_CNT_SIZE_FIFO_ZERO_2 =>
               if size_fifo_cnt = 0 then
                  next_state <= TRUC;  -------------************* avant c'etait WAIT_START_L
               else
                  next_state <= IS_NB_OUTPUTS_ZERO_2;
               end if;

            when IS_NB_OUTPUTS_ZERO_2 =>
               if nb_outputs_cnt = 0 then
                  next_state <= DEC_CNT_SIZE_FIFO_2;
               else
                  next_state <= WRITE_SRAM;
               end if;

            when WRITE_SRAM =>
               next_state <= DEC_NB_OUTPUTS_2;

            when DEC_NB_OUTPUTS_2 =>
               next_state <= IS_NB_OUTPUTS_ZERO_2;

            when DEC_CNT_SIZE_FIFO_2 =>
               next_state <= IS_CNT_SIZE_FIFO_ZERO_2;

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------



         end case;
   end process Next_State_Decoder;


   State_update: process(clk_i, reset_i)
   begin 
      if reset_i = '1' then
         current_state <= WAIT_START_L;
      elsif clk_i'event and clk_i='1' then
            current_state <= next_state;  
      end if;
   end process State_update;


   Output_Decoder: process (current_state)
   begin   
      --- Synchonous outputs ---
      reset_start_reg_s <= '0';
		
		init_latence_cnt_s <= '0';
		wr_latence_counter_reg_s <= '0';
		inc_latence_cnt_s <= '0';

      read_SRAM_s <= '0';
      write_SRAM_s <= '0';

      inc_PTR_s      <= '0';
      inc_PTR2_s     <= '0';
      ptr2_active_s  <= '0';

      init_PTR_s        <= '0';
      init_PTR2_s       <= '0';

      shift_sel_reg_s <= '0';
      shift_sel_out_reg_s <= '0';

      en_write_fifos_s <= '0';
      en_read_fifos_s  <= '0';


      init_nb_inputs_cnt_s <= '0';
      dec_nb_inputs_cnt_s <= '0';

      init_size_fifo_cnt_s <= '0';
      dec_size_fifo_cnt_s <= '0';

      init_nb_test_cnt_s <= '0';
      dec_nb_test_cnt_s <= '0';

      init_nb_outputs_cnt_s <= '0';
      dec_nb_outputs_cnt_s <= '0';
		
		is_M2M_finished_o <= '0';
		
		LED_o <= '0';
		
		dec_PTR_s <= '0';


      case current_state is

         when WAIT_START_L =>
				is_M2M_finished_o <= '1';
				LED_o             <= '1';
            
            init_PTR_s        <= '1';
            init_PTR2_s       <= '1';
            
         when WAIT_START_H =>
				is_M2M_finished_o <= '1';
				LED_o <= '1';

            init_PTR_s        <= '1';
            init_PTR2_s       <= '1';

         when RESET_START_REG =>
            reset_start_reg_s    <= '1';
				
				init_latence_cnt_s	<= '1';

            init_nb_test_cnt_s   <= '1';
            init_size_fifo_cnt_s <= '1';
            init_nb_inputs_cnt_s <= '1';

            read_SRAM_s          <= '1'; -- load SRAM data (PTR_s position) in the sram_data_reg (prefetch!?)
            inc_PTR_s            <= '1';


			when WRITE_LATENCE_REG =>
				wr_latence_counter_reg_s <= '1';

----------------------------------------------
--   Writing the content of the SRAM in     --
--   input fifos                            --
----------------------------------------------

         when IS_NB_TEST_CNT_ZERO =>
            -- nothing happens
            init_size_fifo_cnt_s <= '1'; --------------> attention
            reset_start_reg_s    <= '1';
				
				inc_latence_cnt_s <= '1';


         when IS_CNT_SIZE_FIFO_ZERO =>
            inc_latence_cnt_s <= '1';

         when IS_NB_INPUTS_ZERO =>
            inc_latence_cnt_s <= '1';

         when READ_SRAM =>
            read_SRAM_s       <= '1';
            inc_PTR_s         <= '1';
            en_write_fifos_s  <= '1';
				inc_latence_cnt_s <= '1';

         when DEC_NB_INPUTS =>
            dec_nb_inputs_cnt_s  <= '1';
            shift_sel_reg_s      <= '1';
				inc_latence_cnt_s <= '1';

         when DEC_CNT_SIZE_FIFO =>
            dec_nb_test_cnt_s    <= '1';
            dec_size_fifo_cnt_s  <= '1';
            init_nb_inputs_cnt_s <= '1';
				inc_latence_cnt_s <= '1';

----------------------------------------------
----------------------------------------------

         when WAIT_FIFO_OUTPUTS_FULL =>
            init_nb_outputs_cnt_s   <= '1';
            init_size_fifo_cnt_s    <= '1';
            --reset_start_reg_s       <= '1'; ---------------------> attention
				inc_latence_cnt_s <= '1';

----------------------------------------------
--   Writing the content of output FIFOs    --
--   in the SRAM                            --
----------------------------------------------
		when TRUC =>
			dec_PTR_s <= '1';
			--read_SRAM_s <= '1';
			
		when TRUC2 =>
			--dec_PTR_s <= '1';
			inc_PTR_s <= '1';
			read_SRAM_s <= '1';


         when IS_CNT_SIZE_FIFO_ZERO_2 =>
            inc_latence_cnt_s <= '1';

         when IS_NB_OUTPUTS_ZERO_2 =>
            inc_latence_cnt_s <= '1';
            ptr2_active_s <= '1'; ----------------------------------------------- attention

         when WRITE_SRAM =>
            write_SRAM_s    <= '1';
            inc_PTR2_s       <= '1';
            en_read_fifos_s <= '1';

            ptr2_active_s   <= '1';
				inc_latence_cnt_s <= '1';

         when DEC_NB_OUTPUTS_2 =>
            dec_nb_outputs_cnt_s <= '1';
            shift_sel_out_reg_s  <= '1';
				inc_latence_cnt_s <= '1';

         when DEC_CNT_SIZE_FIFO_2 =>
            dec_size_fifo_cnt_s     <= '1';
            init_nb_outputs_cnt_s   <= '1';
				inc_latence_cnt_s <= '1';

      end case;
   end process Output_Decoder;



   Rd_FIFO_Out_gen : for i in 0 to M2M_Nb_Outputs_c-1 generate
      Rd_FIFO_Out_s(i) <= sel_out_reg(i) and en_read_fifos_s;
   end generate;


--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------


   PTR_SRAM_register: process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         PTR_reg <= (others => '0');
      elsif clk_i'event and clk_i ='1' then
         if init_PTR_s = '1' then
            PTR_reg <= (others => '0');
         elsif inc_PTR_s = '1' then
            PTR_reg <= PTR_incremented_reg;
         elsif dec_PTR_s = '1' then
            PTR_reg <= PTR_decremented_reg;
         end if;
      end if;
   end process;
      
   -- ultra important : for simulation the value is 1 (not 4!)
-- define constant, define 2 packages one for sim, other for xilinx ise
   PTR_incremented_reg <= PTR_reg + std_logic_vector(to_unsigned(inc_PTR_SRAM_c,PTR_reg'length));
   PTR_decremented_reg <= PTR_reg + not (std_logic_vector(to_unsigned(inc_PTR_SRAM_c,PTR_reg'length))) + 1;

--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
   PTR2_init_SRAM_register: process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         PTR2_init_reg <= (others => '0');
      elsif (conf_en_i = '1' and CPU_address_i(15 downto 11) = "11110" and CPU_RDnWR_i = '0') then
         PTR2_init_reg <= data_conf_in_i(25 downto 0);
      end if;
   end process;


   PTR2_SRAM_register: process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         --PTR2_reg <= std_logic_vector(to_unsigned(64,PTR_reg'length));
         PTR2_reg <= (others => '0');
      elsif clk_i'event and clk_i ='1' then
         if init_PTR2_s = '1' then
            PTR2_reg <= PTR2_init_reg;
         elsif inc_PTR2_s = '1' then
            PTR2_reg <= PTR2_incremented_reg;
         end if;
      end if;
   end process;
      
   -- ultra important : for simulation the value is 1 (not 4!)
-- define constant, define 2 packages one for sim, other for xilinx ise
   PTR2_incremented_reg <= PTR2_reg + std_logic_vector(to_unsigned(inc_PTR_SRAM_c,PTR_reg'length));

--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------


   SRAM_data_register : process(clk_i, reset_i)
   begin

      if reset_i = '1' then
         SRAM_data_reg <= (others => '0');
      elsif clk_i'event and clk_i = '1' then
         if SRAM_nCS_s = '0' then
            SRAM_data_reg <= SRAM_INPUT_i;
         end if;
      end if;

   end process;


   SRAM_A_o <= PTR_reg when ptr2_active_s = '0' else PTR2_reg;

   SRAM_OUTPUT_o(31 downto 0) <= FIFO_Out_multiplexed_s;

   SRAM_nCS_s <= '0' when read_SRAM_s = '1' or write_SRAM_s = '1' else '1'; -- Chip enable active 0
   SRAM_nCS_o <= SRAM_nCS_s;

   SRAM_nOE_o <= not read_SRAM_s; -- Output enable active 0
   
   SRAM_nWE_o <= not write_SRAM_s; -- Write enable active 0


   SRAM_DQM_o <= "0000";



end behavioral;
