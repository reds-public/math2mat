-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- file:    interface_manager.vhd
-- creator: Mikhail Nagoga
--          name.surname@heig-vd.ch
-- date:    November 2010
--
-- description: The unit is the interface between output of 'input FIFO' and
--              input of M2M. There is a fsm waiting all input FIFOs apearing
--              in the full state. After this fsm is waiting the corresponding
--              input FIFO becomes empty.
--
-- version: 1.0  | Initial version

----------------------------------------------------------------------------

library Ieee;
use Ieee.Std_Logic_1164.all;
use Ieee.Numeric_Std.all;

entity interface_manager is
 port(
      clk_i          : in std_logic;
      reset_i        : in std_logic;

      ready_i        : in std_logic;
      fifo_empty_i   : in std_logic;
      all_fifos_in_full_i : in std_logic;

      rd_fifo_o      : out std_logic;
      valid_o        : out std_logic
   );
end interface_manager;


architecture behavioral of interface_manager is
   type state is (
					WAIT_FULL,
					WAIT_EMPTY
                 ); 

   signal current_state : state;
   signal next_state    : state;
   
   signal rd_fifo_s : std_logic;

begin                
   Next_State_Decoder : process(current_state, fifo_empty_i, all_fifos_in_full_i)
   begin
         next_state <= WAIT_FULL;

         case current_state is

			when WAIT_FULL =>
				if all_fifos_in_full_i = '0' then
					next_state <= WAIT_FULL;
				else
					next_state <= WAIT_EMPTY;
				end if;
				
			when WAIT_EMPTY =>
				if fifo_empty_i = '0' then
					next_state <= WAIT_EMPTY;
				else
					next_state <= WAIT_FULL;
				end if;
            
  
         end case;
   end process Next_State_Decoder;

   State_update : process(clk_i, reset_i)
   begin
      if reset_i = '1' then
         current_state <= WAIT_FULL;
      elsif clk_i'event and clk_i='1' then
         current_state <= next_state;
      end if;
   end process State_update;


   Output_Decoder: process (current_state)
   begin   
      --- Synchonous outputs ---
      valid_o <= '0';
      rd_fifo_s <= '0';

      case current_state is

		when WAIT_FULL =>
			
		when WAIT_EMPTY =>
		  valid_o <= '1';
		  rd_fifo_s <= '1';
		  
      end case;
   end process Output_Decoder;

   rd_fifo_o <= rd_fifo_s and not fifo_empty_i and ready_i;
   
end behavioral;
