-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- file:    M2M_Test.vhd
-- creator: Andres Upegui
--          name.surname@heig-vd.ch
-- date:    June 2007
--
-- description: This unit is the top level entity of the
--              ubichip. It includes the configurable section,
--              defined as the component Reconfig_array, and 
--              the controllers for the memory bus accesses:
--              external SRAM and CPU VLIO (Variable Latency 
--              Input put).
--
-- description version 2.0: M2M_Test component of the test platform of M2M
--                          Contains: M2M
--                                    M2M controler
--                                    CPU VLIO controler
--                                    SRAM controler
--                                    Synchronisation registers
--                                    SRAM acces multiplexing between M2M controler
--                                         and SRAM controler
--               
--
-- version: 1.0  | Initial version
-- version: 2.0  | Modified by Mikhail Nagoga
--                 November 2010
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.m2m_inout_definition_pkg.all;

   entity M2M_Test is
      port(
         clk_i:        in    std_logic;              -- Clock input  (active with rising edge)
         reset_i:      in    std_logic;               -- Reset input (synchronous and active high)
			
			is_M2M_finished_o : out std_logic;
			LED_o					: out std_logic;

         -- CPU interface signals --
         CPU_nCS_i :   in    std_logic;
         CPU_nOE_i :   in    std_logic;
         CPU_nPWE_i :  in    std_logic;
         CPU_RDnWR_i : in    std_logic;              -- Read ('1') / Write ('0') signal for the configuration
         CPU_RDY_en_o :   out   std_logic;
         CPU_DQM_i  :  in    std_logic_vector(3 downto 0);
         CPU_address_i: in   std_logic_vector(25 downto 0);
         CPU_data_out_o: out   std_logic_vector(31 downto 0);
         CPU_data_in_i: in   std_logic_vector(31 downto 0);	
         active_CPU_data_out_o: out std_logic; -- tristate control for the CPU_data bus, active high

         -- SRAM interface signals --
         SRAM_DQM_o  : out   std_logic_vector(3 downto 0);
         SRAM_address_o: out std_logic_vector(25 downto 0);
         SRAM_data_in_i: in  std_logic_vector(31 downto 0);
         SRAM_data_out_o: out std_logic_vector(31 downto 0);
         active_SRAM_data_out_o: out std_logic; -- tristate control for the SRAM_data bus, active high

         SRAM_nCS_o: out std_logic;  -- Chip select active low

         SRAM_nOE_o: out std_logic;  -- Output enable active low
         SRAM_nWE_o: out std_logic  -- Write enable active low          
          ); 
   end M2M_Test;

architecture structural of M2M_Test is

   signal SRAM_en_s : std_logic;

   signal SRAM_rdy_s : std_logic;
   signal CPU_RDY_en_s : std_logic;

   signal reset_s :std_logic;

   signal CPU_nCS_reg  :   std_logic;
   signal CPU_nOE_reg :    std_logic;
   signal CPU_nPWE_reg :   std_logic;
   signal CPU_RDnWR_reg :  std_logic;   
   signal CPU_DQM_reg  :   std_logic_vector(3 downto 0);


   signal SRAM_Ctrl_s: std_logic; -- if high, the bloc SRAM_Ctrl is the controller of the SRAM
   signal M2M_ctrl_SRAM_s: std_logic; -- if high, the bloc M2M_ctrl is the controller of the SRAM


   signal SRAM_Ctrl_A_s :std_logic_vector(25 downto 0);
   signal SRAM_Ctrl_DQM_s : std_logic_vector(3 downto 0);
   signal SRAM_Ctrl_nCS_s : std_logic;
   signal SRAM_Ctrl_nOE_s : std_logic;
   signal SRAM_Ctrl_nWE_s : std_logic;

   signal M2M_ctrl_SRAM_IN_s   : std_logic_vector(31 downto 0);
   signal M2M_ctrl_SRAM_OUT_s  : std_logic_vector(31 downto 0);
   signal M2M_ctrl_SRAM_A_s    : std_logic_vector(25 downto 0);
   signal M2M_ctrl_SRAM_DQM_s: std_logic_vector(3 downto 0);
   signal M2M_ctrl_SRAM_nCS_s : std_logic;
   signal M2M_ctrl_SRAM_nOE_s : std_logic;
   signal M2M_ctrl_SRAM_nWE_s : std_logic;

   signal SRAM_data_reg: std_logic_vector(31 downto 0);

   signal CPU_data_reg: std_logic_vector(31 downto 0);
   signal CPU_address_reg: std_logic_vector(25 downto 0);


   signal data_conf_in_s: std_logic_vector(31 downto 0);
   signal data_conf_out_s:std_logic_vector(31 downto 0);

   signal M2M_config_en_s : std_logic; -- for configuring M2M_ctrl


   -----------------------------------------------------------------------------	

   --	signal reset_in_n: std_logic;
   --	signal locked: std_logic;
   signal clk:std_logic;

   signal M2M_Input_s : M2M_Data(M2M_Nb_Inputs_c-1 downto 0);
   signal M2M_Input_Valid_s : M2M_Control(M2M_Nb_Inputs_c-1 downto 0);
   signal M2M_Input_Ready_s : M2M_Control(M2M_Nb_Inputs_c-1 downto 0);

   signal M2M_Result_s : M2M_Data(M2M_Nb_Outputs_c-1 downto 0);
   signal M2M_Result_Valid_s : M2M_Control(M2M_Nb_Outputs_c-1 downto 0);
   signal M2M_Result_Ready_s :M2M_Control(M2M_Nb_Outputs_c-1 downto 0);

	--signal clk_s : std_logic;
begin

   reset_s <= not reset_i; -- for skipping the DCM

	CPU_RDY_en_o <= CPU_RDY_en_s;

  
--   m2mdcm_inst : entity work.m2mdcm
--   port map(
--			 CLKIN_IN        => clk_i,
--          RST_IN          => reset_s,
--          CLKIN_IBUFG_OUT => open,
--          CLK0_OUT        => clk_s,
--          LOCKED_OUT      => open
--			);
--
--	process(clk_s)
--	begin
--		if(clk_s'event and clk_s='1') then
--			CPU_nCS_reg <= CPU_nCS_i;
--			CPU_nOE_reg <= CPU_nOE_i;
--			CPU_nPWE_reg <= CPU_nPWE_i;
--			CPU_RDnWR_reg <= CPU_RDnWR_i;  
--			CPU_DQM_reg <= CPU_DQM_i;
--			CPU_address_reg <= CPU_address_i;
--			if(CPU_nPWE_i ='0') then
--				CPU_data_reg <= CPU_data_in_i;
--			end if;
--
--		end if;
--	end process;
--
--   m2m_generic_wrapper_inst : entity work.m2m_generic_wrapper
--      port map(
--         Clk_i          => clk_s,
--         Reset_i        => reset_s,
--
--         Input_i        => M2M_Input_s,
--         Input_Valid_i  => M2M_Input_Valid_s,
--         Input_Ready_o  => M2M_Input_Ready_s,
--         Result_o       => M2M_Result_s,
--         Result_Valid_o => M2M_Result_Valid_s,
--         Result_Ready_i => M2M_Result_Ready_s
--      );
--
-- 
---- TODO virer work??
--   M2M_ctrl_inst: entity work.M2M_ctrl
--      port map(
--            clk_i   => clk_s,
--            reset_i => reset_s,
--
--            -- configuration of internes registres
--            conf_en_i => M2M_config_en_s,
--            data_conf_in_i => data_conf_in_s,
--            data_conf_out_o=> data_conf_out_s,
--
--            CPU_RDnWR_i => CPU_RDnWR_reg,
--            CPU_address_i => CPU_address_reg,
--				
--	
--				-- GPIO & LED interface
--				is_M2M_finished_o	=> is_M2M_finished_o,
--				LED_o		=> LED_o,
--
--            -- SRAM interface
--            SRAM_INPUT_i   => M2M_ctrl_SRAM_IN_s,
--            SRAM_OUTPUT_o  => M2M_ctrl_SRAM_OUT_s,
--            SRAM_A_o       => M2M_ctrl_SRAM_A_s,
--            SRAM_DQM_o     => M2M_ctrl_SRAM_DQM_s,
--            SRAM_nCS_o     => M2M_ctrl_SRAM_nCS_s,
--            SRAM_nOE_o     => M2M_ctrl_SRAM_nOE_s,
--            SRAM_nWE_o     => M2M_ctrl_SRAM_nWE_s,
--
--            -- M2M interface
--            M2M_Input_o       => M2M_Input_s,
--            M2M_Input_Valid_o => M2M_Input_Valid_s,
--            M2M_Input_Ready_i => M2M_Input_Ready_s,
--
--            M2M_Result_i         => M2M_Result_s,
--            M2M_Result_Valid_i   => M2M_Result_Valid_s,
--            M2M_Result_Ready_o   => M2M_Result_Ready_s
--            );
-- 
--	VLIO_inst: entity work.CPU_VLIO_ctrl
--		port map(   
--				clk_i => clk_s,
--				reset_i => reset_s,      
--				CPU_address_i => CPU_address_reg,    
--				CPU_nCS_i => CPU_nCS_reg, 
--				CPU_nOE_i => CPU_nOE_reg, 
--				CPU_nPWE_i => CPU_nPWE_reg, 
--				CPU_RDnWR_i => CPU_RDnWR_reg, 
--				CPU_RDY_en_o =>  CPU_RDY_en_s,
--            M2M_config_en_o => M2M_config_en_s,
--				SRAM_en_o=>   SRAM_en_s,
--				SRAM_rdy_i=>  SRAM_rdy_s
--				);
--
--					
--	SRAM_inst: entity work.SRAM_ctrl 
--		port map(
--				clk => clk_s,
--				reset => reset_s,
--
--				SRAM_en => SRAM_en_s,
--				SRAM_rdy => SRAM_rdy_s,
--				CPU_RDnWR => CPU_RDnWR_reg,
--				CPU_DQM => CPU_DQM_reg,
--				CPU_address => CPU_address_reg,
--				CPU_data_in => CPU_data_reg,
--
--
--				SRAM_nCS => SRAM_Ctrl_nCS_s,
--				SRAM_nOE_int => SRAM_Ctrl_nOE_s,
--				SRAM_nWE => SRAM_Ctrl_nWE_s,
--				SRAM_DQM => SRAM_Ctrl_DQM_s,
--				SRAM_address => SRAM_Ctrl_A_s
--				); 
--   
--	-- Multiplexing SRAM access for SRAM controller and sequencer
--
--   SRAM_Ctrl_s <= '1' when SRAM_Ctrl_nCS_s = '0' else
--      '0';
--   M2M_ctrl_SRAM_s <= not M2M_ctrl_SRAM_nCS_s;
--
--
--	SRAM_nOE_o <= M2M_ctrl_SRAM_nOE_s when M2M_ctrl_SRAM_s = '1' else
--				SRAM_Ctrl_nOE_s when SRAM_Ctrl_s = '1' else
--				'1';
--
--
--	SRAM_nCS_o <= SRAM_Ctrl_nCS_s and M2M_ctrl_SRAM_nCS_s;
--
--
--
--
--
--	SRAM_nWE_o <= M2M_ctrl_SRAM_nWE_s when M2M_ctrl_SRAM_s = '1' else
--			SRAM_Ctrl_nWE_s when SRAM_Ctrl_s = '1' else
--			'1'; 
--
--   SRAM_DQM_o <= "0000";
--
--
--	SRAM_address_o <= M2M_ctrl_SRAM_A_s  when M2M_ctrl_SRAM_s = '1' else
--					SRAM_Ctrl_A_s when SRAM_Ctrl_s = '1' else -- uncomment for 19 address bits
----					"0000000"&CTL_SRAM_A when ctl_SRAM = '1' else	-- uncomment for 26 address bits				
--					(others =>'0');
--
--   M2M_ctrl_SRAM_IN_s <= SRAM_data_in_i;
--
----   -- TODO genere un latch! virer le second when. (ajouté others => '0' marche ou marche pas!?)
----	SRAM_DATA_out <= M2M_ctrl_SRAM_OUT when (M2M_ctrl_SRAM_nCS = '0' and M2M_ctrl_SRAM_nWE='0') else
----               SRAM_data_out_int when (CTL_SRAM_OEn = '1' and CTL_SRAM_WEn = '0') else
----               (others => '0');
----
----	active_SRAM_data_out <=	'1' when ((M2M_ctrl_SRAM_nCS = '0' and M2M_ctrl_SRAM_nWE='0') or (CTL_SRAM_OEn = '1' and CTL_SRAM_WEn = '0'))
----							else '0';
--
--	SRAM_DATA_out_o <= M2M_ctrl_SRAM_OUT_s when (M2M_ctrl_SRAM_nOE_s = '1' and M2M_ctrl_SRAM_nWE_s='0') else
--               CPU_data_reg when (SRAM_Ctrl_nOE_s = '1' and SRAM_Ctrl_nWE_s = '0') else
--               (others => '0');
--
--	active_SRAM_data_out_o <=	'1' when ((M2M_ctrl_SRAM_nOE_s = '1' and M2M_ctrl_SRAM_nWE_s='0') or (SRAM_Ctrl_nOE_s = '1' and SRAM_Ctrl_nWE_s = '0'))
--							else '0';
--
--   
--
--   -- data configuration from CPU
--   data_conf_in_s <= CPU_data_reg(15 downto 0);
--
--   process(clk_s)
--   begin
--      if(clk_s'event and clk_s='1') then
--         if(SRAM_en_s='1') then
--            SRAM_data_reg <= SRAM_data_in_i;
--         end if;
--      end if;
--   end process;
--   
--   active_CPU_data_out_o <= '1' when (CPU_nOE_reg = '0' and CPU_nCS_reg ='0') else
--                          '0';
--
--   CPU_data_out_o <= SRAM_data_reg when CPU_address_reg(25)='1' else 
--                   X"0000" & data_conf_out_s when CPU_address_reg(15 downto 13)="111" else
--                  (others =>'0');
--
--end structural;

	process(clk_i)
	begin
		if(clk_i'event and clk_i='1') then
			CPU_nCS_reg <= CPU_nCS_i;
			CPU_nOE_reg <= CPU_nOE_i;
			CPU_nPWE_reg <= CPU_nPWE_i;
			CPU_RDnWR_reg <= CPU_RDnWR_i;  
			CPU_DQM_reg <= CPU_DQM_i;
			CPU_address_reg <= CPU_address_i;
			if(CPU_nPWE_i ='0') then
				CPU_data_reg <= CPU_data_in_i;
			end if;

		end if;
	end process;

   m2m_generic_wrapper_inst : entity work.m2m_generic_wrapper
      port map(
         Clock_i        => clk_i,
         Reset_i        => reset_s,

         Input_i        => M2M_Input_s,
         Input_Valid_i  => M2M_Input_Valid_s,
         Input_Ready_o  => M2M_Input_Ready_s,
         Result_o       => M2M_Result_s,
         Result_Valid_o => M2M_Result_Valid_s,
         Result_Ready_i => M2M_Result_Ready_s
      );

 
   M2M_ctrl_inst: entity work.M2M_ctrl
      port map(
            clk_i   => clk_i,
            reset_i => reset_s,

            -- configuration of internes registres
            conf_en_i => M2M_config_en_s,
            data_conf_in_i => data_conf_in_s,
            data_conf_out_o=> data_conf_out_s,

            CPU_RDnWR_i => CPU_RDnWR_reg,
            CPU_address_i => CPU_address_reg,
				
	
				-- GPIO & LED interface
				is_M2M_finished_o	=> is_M2M_finished_o,
				LED_o		=> LED_o,

            -- SRAM interface
            SRAM_INPUT_i   => M2M_ctrl_SRAM_IN_s,
            SRAM_OUTPUT_o  => M2M_ctrl_SRAM_OUT_s,
            SRAM_A_o       => M2M_ctrl_SRAM_A_s,
            SRAM_DQM_o     => M2M_ctrl_SRAM_DQM_s,
            SRAM_nCS_o     => M2M_ctrl_SRAM_nCS_s,
            SRAM_nOE_o     => M2M_ctrl_SRAM_nOE_s,
            SRAM_nWE_o     => M2M_ctrl_SRAM_nWE_s,

            -- M2M interface
            M2M_Input_o       => M2M_Input_s,
            M2M_Input_Valid_o => M2M_Input_Valid_s,
            M2M_Input_Ready_i => M2M_Input_Ready_s,

            M2M_Result_i         => M2M_Result_s,
            M2M_Result_Valid_i   => M2M_Result_Valid_s,
            M2M_Result_Ready_o   => M2M_Result_Ready_s
            );
 
	VLIO_inst: entity work.CPU_VLIO_ctrl
		port map(   
				clk_i => clk_i,
				reset_i => reset_s,      
				CPU_address_i => CPU_address_reg,    
				CPU_nCS_i => CPU_nCS_reg, 
				CPU_nOE_i => CPU_nOE_reg, 
				CPU_nPWE_i => CPU_nPWE_reg, 
				CPU_RDnWR_i => CPU_RDnWR_reg, 
				CPU_RDY_en_o =>  CPU_RDY_en_s,
            M2M_config_en_o => M2M_config_en_s,
				SRAM_en_o=>   SRAM_en_s,
				SRAM_rdy_i=>  SRAM_rdy_s
				);

					
	SRAM_inst: entity work.SRAM_ctrl 
		port map(
				clk => clk_i,
				reset => reset_s,

				SRAM_en => SRAM_en_s,
				SRAM_rdy => SRAM_rdy_s,
				CPU_RDnWR => CPU_RDnWR_reg,
				CPU_DQM => CPU_DQM_reg,
				CPU_address => CPU_address_reg,
				CPU_data_in => CPU_data_reg,


				SRAM_nCS => SRAM_Ctrl_nCS_s,
				SRAM_nOE_int => SRAM_Ctrl_nOE_s,
				SRAM_nWE => SRAM_Ctrl_nWE_s,
				SRAM_DQM => SRAM_Ctrl_DQM_s,
				SRAM_address => SRAM_Ctrl_A_s
				); 
   
	-- Multiplexing SRAM access for SRAM controller and sequencer

   SRAM_Ctrl_s <= '1' when SRAM_Ctrl_nCS_s = '0' else
      '0';
   M2M_ctrl_SRAM_s <= not M2M_ctrl_SRAM_nCS_s;


	SRAM_nOE_o <= M2M_ctrl_SRAM_nOE_s when M2M_ctrl_SRAM_s = '1' else
				SRAM_Ctrl_nOE_s when SRAM_Ctrl_s = '1' else
				'1';


	SRAM_nCS_o <= SRAM_Ctrl_nCS_s and M2M_ctrl_SRAM_nCS_s;





	SRAM_nWE_o <= M2M_ctrl_SRAM_nWE_s when M2M_ctrl_SRAM_s = '1' else
			SRAM_Ctrl_nWE_s when SRAM_Ctrl_s = '1' else
			'1'; 

   SRAM_DQM_o <= "0000";


	SRAM_address_o <= M2M_ctrl_SRAM_A_s  when M2M_ctrl_SRAM_s = '1' else
					SRAM_Ctrl_A_s when SRAM_Ctrl_s = '1' else
					(others =>'0');

   M2M_ctrl_SRAM_IN_s <= SRAM_data_in_i;

	SRAM_DATA_out_o <= M2M_ctrl_SRAM_OUT_s when (M2M_ctrl_SRAM_nOE_s = '1' and M2M_ctrl_SRAM_nWE_s='0') else
               CPU_data_reg when (SRAM_Ctrl_nOE_s = '1' and SRAM_Ctrl_nWE_s = '0') else
               (others => '0');

	active_SRAM_data_out_o <=	'1' when ((M2M_ctrl_SRAM_nOE_s = '1' and M2M_ctrl_SRAM_nWE_s='0') or (SRAM_Ctrl_nOE_s = '1' and SRAM_Ctrl_nWE_s = '0'))
							else '0';

   

   -- data configuration from CPU
   data_conf_in_s <= CPU_data_reg;

   process(clk_i)
   begin
      if(clk_i'event and clk_i='1') then
         if(SRAM_en_s='1') then
            SRAM_data_reg <= SRAM_data_in_i;
         end if;
      end if;
   end process;
   
   active_CPU_data_out_o <= '1' when (CPU_nOE_reg = '0' and CPU_nCS_reg ='0') else
                          '0';

   CPU_data_out_o <= SRAM_data_reg when CPU_address_reg(25)='1' else 
                   data_conf_out_s when CPU_address_reg(15 downto 13)="111" else
                  (others =>'0');

end structural;

