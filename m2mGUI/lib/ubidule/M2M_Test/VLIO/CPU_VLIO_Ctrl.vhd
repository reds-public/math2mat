-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- file:    CPU_VLIO_ctrl.vhd
-- creator: Andres Upegui
--          name.surname@heig-vd.ch
-- date:    May 2007
--
-- description: This unit manages the access to the ubichip configuration
--              from the CPU unit through a VLIO interface. It supports
--              the access to the ubichip configuration bits and to the 
--              external SRAM contents. It supports also the burst access
--              mode of the VLIO bus (not tested). 
--
-- version: 1.0  | Initial version
--
-- version: 1.1  | bugs on the RDY signal fixed
--
-- version 1.2   | four states have been removed in order to make the 
--                 access more efficient, and in order to avoid writing
--                 errors.
-- version 2.0   | The possibility to read/write on ubichip is removed
--
-- version 3.0   | Add CPU_address in the sensivity list at Next_State_Decoder
----------------------------------------------------------------------------

library Ieee;
use Ieee.Std_Logic_1164.all;
use Ieee.Numeric_Std.all;

entity CPU_VLIO_ctrl is
 port(
      clk_i            : in  Std_Logic; -- Master clock.
      reset_i          : in  Std_Logic; -- Asychronus general reset.
      
      CPU_nCS_i        : in  Std_Logic; -- Chip select.
      CPU_nOE_i        : in  Std_Logic; -- Output enable.
      CPU_RDnWR_i      : in  Std_Logic; -- Operation type.  
      CPU_nPWE_i       : in  Std_Logic; -- Write enable. 
      CPU_Rdy_en_o     : out Std_Logic; -- Enables Data ready cpu command signal.    
      CPU_address_i    : in  std_logic_vector(25 downto 0); -- address from CPU

      M2M_config_en_o  : out std_logic; -- enable config for M2M_ctrl
      
      SRAM_en_o        : out Std_Logic; -- Enables SRAM access. 
      SRAM_rdy_i       : in  Std_Logic -- SRAM access ready.  
   );
end CPU_VLIO_ctrl;


architecture behavioral of CPU_VLIO_ctrl is

   type state is (IDLE,
                  RD_SRAM, RD_SRAM_wait_2,
                  WR_SRAM, WR_SRAM_wait_2,
                  RD_M2M_ctrl, RD_M2M_ctrl_2,
                  WR_M2M_ctrl, WR_M2M_ctrl_2
                 ); 
   signal current_state : state;
   signal next_state : state;   
   signal CPU_Rdy_en_s :std_logic;

begin                


   Next_State_Decoder: process(CPU_nCS_i, CPU_nOE_i, CPU_RDnWR_i, CPU_nPWE_i, SRAM_rdy_i, CPU_address_i, current_state)
   begin
      if CPU_nCS_i = '1' then 
         next_state <= IDLE;
      else
         next_state <= current_state;

         case current_state is

            when IDLE => 
               if (CPU_RDnWR_i='1' and CPU_address_i(25)='1' and CPU_nOE_i='0')then -- reads external SRAM
                  next_state <= RD_SRAM; 
               elsif (CPU_RDnWR_i='0' and CPU_address_i(25)='1' and CPU_nPWE_i='0')then -- writes external SRAM
                  next_state <= WR_SRAM;
               elsif (CPU_RDnWR_i='1' and CPU_address_i(25)='0'and CPU_nOE_i='0') then -- reads M2M_ctrl
                  next_state <= RD_M2M_ctrl; 
               elsif (CPU_RDnWR_i='0' and CPU_address_i(25)='0' and CPU_nPWE_i='0') then -- writes M2M_ctrl
                  next_state <= WR_M2M_ctrl;
               end if;

            when RD_SRAM =>
               if SRAM_rdy_i ='1' then
                  next_state <= RD_SRAM_wait_2; 
                  --next_state <= IDLE; 
               end if;

            when RD_SRAM_wait_2 =>
               if CPU_nOE_i='1' then
                  next_state <= IDLE;  
               end if;      


            when WR_SRAM => 
               next_state <= WR_SRAM_wait_2;   
               --next_state <= IDLE; 

            when WR_SRAM_wait_2 => 
               if CPU_nPWE_i='1' then
                  next_state <= IDLE; 
               end if;

            when RD_M2M_ctrl => 
               next_state <= RD_M2M_ctrl_2;                
               --next_state <= IDLE; 
               
            when RD_M2M_ctrl_2 =>   
            if CPU_nOE_i='1' then
                  next_state <= IDLE;  
            end if;            
               
            when WR_M2M_ctrl =>                
                  next_state <= WR_M2M_ctrl_2;  
                  --next_state <= IDLE; 

            when WR_M2M_ctrl_2 =>
               if CPU_nPWE_i='1' then
                  next_state <= IDLE; 
               end if;

         end case;
      end if;               
   end process Next_State_Decoder;

   State_update: process(clk_i)
   begin 
      if clk_i'event and clk_i='1' then
         if (reset_i = '1') then
            current_state <= IDLE;
         else
            current_state <= next_state;  
         end if;
      end if;
   end process State_update;


   Output_Decoder: process (current_state)
   begin   
      --- Synchonous outputs ---
      SRAM_en_o <= '0';
      CPU_Rdy_en_s <= '0';
      M2M_config_en_o <= '0';

      case current_state is

         when IDLE => 
            -- nothing happens

         when RD_SRAM =>
            SRAM_en_o <= '1';           
            CPU_Rdy_en_s <= '1';

         when RD_SRAM_wait_2 => 
            SRAM_en_o <= '1';
            --CPU_Rdy_en_int <= '1';

         when WR_SRAM => 
            SRAM_en_o <= '1';
            CPU_Rdy_en_s <= '1';

         when WR_SRAM_wait_2 =>
            --CPU_Rdy_en_int <= '1';



         when RD_M2M_ctrl => 
            M2M_config_en_o <= '1';
            CPU_Rdy_en_s <= '1';
            
         when RD_M2M_ctrl_2 => 
            M2M_config_en_o <= '1';
            --CPU_Rdy_en_int <= '1';

         when WR_M2M_ctrl =>
            M2M_config_en_o <= '1';
            CPU_Rdy_en_s <= '1';   
            
         when WR_M2M_ctrl_2 =>
            M2M_config_en_o <= '1';
         -- Uconfig_en <= '1';
         -- CPU_Rdy_en_int <= '1';

      end case;
   end process Output_Decoder;


   CPU_RDY_en_o <= CPU_Rdy_en_s and (not CPU_nCS_i);

end behavioral;