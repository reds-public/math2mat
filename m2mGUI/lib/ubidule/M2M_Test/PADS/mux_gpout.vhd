-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- Functional description of the multiplexer used for the general purpose inputs
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux_gpout is
port(
		I: in std_logic_vector(39 downto 0);
		O: out std_logic_vector(19 downto 0);
		sel: in std_logic_vector(1 downto 0)
);
end mux_gpout;

architecture behavioural of mux_gpout is

begin
  
	with sel select
		O <=	I(19 downto 0) when "00",
				I(39 downto 20) when "01",
				I(37 downto 36)&I(33 downto 32)&I(29 downto 28)&I(25 downto 24)&I(21 downto 20)&I(17 downto 16)&I(13 downto 12)&I(9 downto 8)&I(5 downto 4)&I(1 downto 0) when "10",
				I(39 downto 38)&I(35 downto 34)&I(31 downto 30)&I(27 downto 26)&I(23 downto 22)&I(19 downto 18)&I(15 downto 14)&I(11 downto 10)&I(7 downto 6)&I(3 downto 2) when others;
	
end behavioural;