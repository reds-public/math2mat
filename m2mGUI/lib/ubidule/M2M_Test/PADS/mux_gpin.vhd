-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-- Functional description of the multiplexer used for the general purpose inputs
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux_gpin is
port(
		I: in std_logic_vector(19 downto 0);
		O: out std_logic_vector(39 downto 0);
		sel: in std_logic_vector(1 downto 0)
);
end mux_gpin;

architecture behavioural of mux_gpin is

begin
  
	with sel select
		O <=	X"00000"&I when "00",
				I&X"00000" when "01",
				"00"&I(19 downto 18)&"00"&I(17 downto 16)&"00"&I(15 downto 14)&"00"&I(13 downto 12)&"00"&I(11 downto 10)&"00"&I(9 downto 8)&"00"&I(7 downto 6)&"00"&I(5 downto 4)&"00"&I(3 downto 2)&"00"&I(1 downto 0) when "10",
				I(19 downto 18)&"00"&I(17 downto 16)&"00"&I(15 downto 14)&"00"&I(13 downto 12)&"00"&I(11 downto 10)&"00"&I(9 downto 8)&"00"&I(7 downto 6)&"00"&I(5 downto 4)&"00"&I(3 downto 2)&"00"&I(1 downto 0)&"00" when others;
	
end behavioural;