/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief ApplicationActionBarAdvisor class
 *
 * Project:  Math2Mat
 *
 * @file ApplicationActionBarAdvisor.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: auto-generated RCP class. This class describes all the menus of the application
 *
 */

package m2m.frontend;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import m2m.frontend.actions.ClearConsoleAction;
import m2m.frontend.actions.ClearProject;
import m2m.frontend.actions.CommentAction;
import m2m.frontend.actions.CommentUncommentAction;
import m2m.frontend.actions.ConfigureExternalToolsAction;
import m2m.frontend.actions.ConfigureOptimisationAction;
import m2m.frontend.actions.ConfigureSimulationAction;
import m2m.frontend.actions.CopyAction;
import m2m.frontend.actions.CutAction;
import m2m.frontend.actions.DeleteAction;
import m2m.frontend.actions.GenerateVHDL;
import m2m.frontend.actions.LaunchOctaveAction;
import m2m.frontend.actions.LaunchOctaveWithRegeneratedFileAction;
import m2m.frontend.actions.LaunchOptimisationAction;
import m2m.frontend.actions.LaunchRunAllUbiduleAction;
import m2m.frontend.actions.LaunchUbiduleVerification;
import m2m.frontend.actions.LaunchVerifAction;
import m2m.frontend.actions.LaunchXilinxUbiduleAction;
import m2m.frontend.actions.NewProjectAction;
import m2m.frontend.actions.OpenFileAction;
import m2m.frontend.actions.PasteAction;
import m2m.frontend.actions.ReOpenFile;
import m2m.frontend.actions.RedoAction;
import m2m.frontend.actions.RunAll;
import m2m.frontend.actions.SaveAsFileAction;
import m2m.frontend.actions.SaveFileAction;
import m2m.frontend.actions.SelectAllAction;
import m2m.frontend.actions.UncommentAction;
import m2m.frontend.actions.UndoAction;
import m2m.frontend.actions.ValidateProjectsAction;
import m2m.frontend.actions.ViewDotFlatFile;
import m2m.frontend.actions.ViewDotTreeFile;
import m2m.frontend.actions.ViewDynamicStructure;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of the
 * actions added to a workbench window. Each window will be populated with
 * new actions.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor 
{
	private static ApplicationActionBarAdvisor instance;
	
    // Actions - important to allocate these only in makeActions, and then use them
    // in the fill methods.  This ensures that the actions aren't recreated
    // when fillActionBars is called with FILL_PROXY.
    private IWorkbenchAction exitAction;
    private IWorkbenchAction aboutAction;
    private UndoAction undoAction;
    private RedoAction redoAction;
    private CutAction cutAction;
    private CopyAction copyAction;
    private PasteAction pasteAction;
    private DeleteAction deleteAction;
    private SelectAllAction selectAllAction;
    private NewProjectAction newProjectAction;
    private NewProjectAction newProjectFromFileAction;
    private OpenFileAction openFileAction;
    private SaveFileAction saveFileAction;
    private SaveAsFileAction saveAsFileAction;
    private LaunchOctaveAction launchOctaveAction;
    private LaunchOptimisationAction launchOptimisationAction;
    private LaunchVerifAction launchVerifAction;
    private LaunchUbiduleVerification launchUbiduleVerification;
    private LaunchXilinxUbiduleAction launchXilinxUbiduleAction;
    private LaunchRunAllUbiduleAction launchRunAllUbiduleAction;
 //   private LaunchDotGeneration launchDotGeneration;
    private ViewDotTreeFile viewDotTreeFile;
    private ViewDotFlatFile viewDotFlatFile;
    private GenerateVHDL generateVHDL;
    private ClearProject clearProject;
    private ClearConsoleAction clearConsole;
    private ConfigureOptimisationAction configureOptimisationAction;
    private ConfigureSimulationAction configureSimulationAction;
    private ConfigureExternalToolsAction configureExternalToolsAction;
    private RunAll runAllAction;
    private CommentUncommentAction commentUncommentAction;
    private CommentAction commentAction;
    private UncommentAction uncommentAction;
 //   private GenerateMath2MatCode generateM2MCodeAction;
    private LaunchOctaveWithRegeneratedFileAction launchOctaveWithRegenAction;
 //   private CompareOctaveResults compareOctaveResults;
    private ViewDynamicStructure viewDynamicStructure;
    private ValidateProjectsAction validateProjectsAction;
    private ReOpenFile[] reOpenFile = new ReOpenFile[5];
    private MenuManager commentMenu = new MenuManager("&Comment/Uncomment", IWorkbenchActionConstants.M_WINDOW);
    private MenuManager ubiduleMenu = new MenuManager("&Ubidule test", IWorkbenchActionConstants.M_WINDOW);
    
    private MenuManager fileMenu;

    private ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
        configurer.getWindowConfigurer( ).getWindow();
    }
    
    public UndoAction getUndoAction () {
    	return undoAction;
    }
    
    protected void makeActions(final IWorkbenchWindow window) {

        // Creates the actions and registers them.
        // Registering is needed to ensure that key bindings work.
        // The corresponding commands keybindings are defined in the plugin.xml file.
        // Registering also provides automatic disposal of the actions when
        // the window is closed.

		exitAction = ActionFactory.QUIT.create(window);
        register(exitAction);
        
        aboutAction = ActionFactory.ABOUT.create(window);
        register(aboutAction);
        
        undoAction = new UndoAction(window, "Undo");
        register(undoAction);
        
        redoAction = new RedoAction(window, "Redo");
        register(redoAction);
        
        cutAction = new CutAction(window, "Cut");
        register(cutAction);
        
        copyAction = new CopyAction(window, "Copy");
        register(copyAction);
        
        pasteAction = new PasteAction(window, "Paste");
        register(pasteAction);
        
        deleteAction = new DeleteAction(window, "Delete");
        register(deleteAction);
        
        selectAllAction = new SelectAllAction(window, "Select All");
        register(selectAllAction);
        
        commentUncommentAction = new CommentUncommentAction(window, "Toggle Comment");
        register(commentUncommentAction);
        
        commentAction = new CommentAction(window, "Comment");
        register(commentAction);
        
        uncommentAction = new UncommentAction(window, "Uncomment");
        register(uncommentAction);

        newProjectAction = new NewProjectAction(window, "New empty project",true);
        register(newProjectAction);

        newProjectFromFileAction = new NewProjectAction(window, "New project from .m file",false);
        register(newProjectFromFileAction);
        
        openFileAction = new OpenFileAction(window, "Open");
        register(openFileAction);
        
        saveFileAction = new SaveFileAction(window, "Save");
        register(saveFileAction);
        
        saveAsFileAction = new SaveAsFileAction(window, "Save As...");
        register(saveFileAction);
        
        validateProjectsAction = new ValidateProjectsAction(window, "Validate Projects");
        register(validateProjectsAction);
        
        launchOptimisationAction = new LaunchOptimisationAction(window, "Launch Optimization");
        register(launchOptimisationAction);
        
        launchOctaveAction = new LaunchOctaveAction(window, "Launch test generation (with Octave)");
        register(launchOctaveAction);
        
        launchVerifAction = new LaunchVerifAction(window, "Launch Verification");
        register(launchVerifAction);
        
        launchUbiduleVerification = new LaunchUbiduleVerification(window, "Launch ubidule verification");
        register(launchUbiduleVerification);
        
        launchXilinxUbiduleAction = new  LaunchXilinxUbiduleAction(window, "Launch ubidule files generation");
        register(launchXilinxUbiduleAction);
        
        launchRunAllUbiduleAction = new LaunchRunAllUbiduleAction(window, "Launch entire ubidule process");
        register(launchRunAllUbiduleAction);
        
     //   launchDotGeneration = new LaunchDotGeneration(window, "Launch dot file generation");
     //   register(launchDotGeneration);
        
        viewDotTreeFile = new ViewDotTreeFile(window, "Show the tree view");
        register(viewDotTreeFile);
        
        viewDotFlatFile = new ViewDotFlatFile(window, "Show the flat view");
        register(viewDotFlatFile);
        
        viewDynamicStructure = new ViewDynamicStructure(window, "Show dynamic view"); 
        register(viewDynamicStructure);
        
        clearConsole = new ClearConsoleAction(window,"Clear the console");
        register(clearConsole);
        
        generateVHDL = new GenerateVHDL(window, "Generate VHDL");
        register(generateVHDL);
        
        clearProject = new ClearProject(window, "Clear project");
        register(clearProject);
        
        configureOptimisationAction = new ConfigureOptimisationAction(window, "Optimization");
        register(configureOptimisationAction);
        
        configureSimulationAction = new ConfigureSimulationAction(window, "Simulation");
        register(configureSimulationAction);
        
        configureExternalToolsAction = new ConfigureExternalToolsAction(window, "External tools");
        register(configureExternalToolsAction);
        
        runAllAction = new RunAll(window, "Run all");
        register(runAllAction);
        
     //   generateM2MCodeAction = new GenerateMath2MatCode(window, "Generate new Octave Code");
     //   register(generateM2MCodeAction);
        
        launchOctaveWithRegenAction = new LaunchOctaveWithRegeneratedFileAction(window, "Test regenerated Octave code");
        register(launchOctaveWithRegenAction);
        
  //      compareOctaveResults = new CompareOctaveResults(window, "Compare the results given by the original file and the regenerated file");
   //     register(compareOctaveResults);

        for(int i = 0; i < reOpenFile.length; i++) {
        	reOpenFile[i] = new ReOpenFile(window, "No file");
        	register(reOpenFile[i]);
        }
        
        showOpenMenu(false);
    }
    
    public void repopulateFileMenu() {

    	fileMenu.removeAll();
    	
        // File
        fileMenu.add(newProjectAction);
        fileMenu.add(newProjectFromFileAction);
        fileMenu.add(new Separator());
        fileMenu.add(openFileAction);
        fileMenu.add(saveFileAction);
        fileMenu.add(saveAsFileAction);
        
        fileMenu.add(new Separator());
        
        GUIProperties GUIProps = GUIProperties.getReference();
        String[] rof = GUIProps.getReOpenFile();
        
		for(int i = 0; i < reOpenFile.length; i++) 
			if (!(rof[i].equalsIgnoreCase("null") || rof[i].equalsIgnoreCase("No file"))){
				reOpenFile[i].setPath(rof[i].equalsIgnoreCase("null") ? "No File": rof[i]);	
				fileMenu.add(reOpenFile[i]);
		}
        
        fileMenu.add(new Separator());
        fileMenu.add(exitAction);
	
    }
    
    
    protected void fillMenuBar(IMenuManager menuBar) {

    	MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);
    	MenuManager configMenu = new MenuManager("&Configure...", IWorkbenchActionConstants.M_WINDOW);
        fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_WINDOW);
        MenuManager editMenu = new MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT); //.M_FILE);
        MenuManager toolMenu = new MenuManager("&Tools", IWorkbenchActionConstants.M_WINDOW);
        MenuManager viewMenu = new MenuManager("&View", IWorkbenchActionConstants.M_WINDOW);
        
        menuBar.add(fileMenu);
        // Add a group marker indicating where action set menus will appear.
        menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
        menuBar.add(editMenu);
        menuBar.add(viewMenu);
        menuBar.add(toolMenu);
        menuBar.add(configMenu);
        menuBar.add(helpMenu);
        
        repopulateFileMenu();
        

       // viewMenu.add(launchDotGeneration);
        viewMenu.add(viewDotTreeFile);
        viewMenu.add(viewDotFlatFile);
        viewMenu.add(viewDynamicStructure);
        viewMenu.add(new Separator());
        viewMenu.add(clearConsole);
       
        // Tools
    //    toolMenu.add(launchOptimisationAction);
        toolMenu.add(generateVHDL);
        toolMenu.add(launchOctaveAction);
        toolMenu.add(launchOctaveWithRegenAction);
        toolMenu.add(launchVerifAction);
        toolMenu.add(runAllAction);
        toolMenu.add(new Separator());
        toolMenu.add(clearProject);
     //   toolMenu.add(generateM2MCodeAction);
     //   toolMenu.add(compareOctaveResults);
        toolMenu.add(new Separator());
        toolMenu.add(ubiduleMenu);
        ubiduleMenu.add(launchXilinxUbiduleAction);
        ubiduleMenu.add(launchUbiduleVerification);
        ubiduleMenu.add(launchRunAllUbiduleAction);

        toolMenu.add(new Separator());
        toolMenu.add(validateProjectsAction);
        
        // Edit
        editMenu.add(undoAction);
        editMenu.add(redoAction);
        editMenu.add(new Separator());
        editMenu.add(cutAction);
        editMenu.add(copyAction);
        editMenu.add(pasteAction);
        editMenu.add(deleteAction);
        editMenu.add(new Separator());
        editMenu.add(selectAllAction);
        editMenu.add(new Separator());
        editMenu.add(commentMenu);
        
        commentMenu.add(commentUncommentAction);
        commentMenu.add(commentAction);
        commentMenu.add(uncommentAction);
        
        // Help
        helpMenu.add(aboutAction);
        
        // Configuration
        configMenu.add(configureExternalToolsAction);
        configMenu.add(configureOptimisationAction);
        configMenu.add(configureSimulationAction);
        
    }
    
    protected void fillCoolBar(ICoolBarManager coolBar) {
        IToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
        coolBar.add(new ToolBarContributionItem(toolbar, "main"));
        toolbar.add(newProjectAction);
        toolbar.add(openFileAction);
        toolbar.add(saveFileAction);
        toolbar.add(new Separator());
        toolbar.add(generateVHDL);
        toolbar.add(launchOptimisationAction);
        toolbar.add(launchOctaveAction);
        toolbar.add(launchVerifAction);
        toolbar.add(runAllAction);
        toolbar.add(launchRunAllUbiduleAction);
    }
    
    
    /**
     * Get the instance of the ApplicationActionBarAdvisor.
     * 
     * @param configurer
     * @return the instance of the ApplicationActionBarAdvisor
     */
    public static ApplicationActionBarAdvisor getInstance(IActionBarConfigurer configurer) {
        if (instance == null) 
            instance = new ApplicationActionBarAdvisor(configurer);
        return instance;
    }
    
    
    /**
     * Get the instance of the ApplicationActionBarAdvisor.
     * 
     * @return the instance of the ApplicationActionBarAdvisor
     */
    public static ApplicationActionBarAdvisor getInstance() {
        return instance;
    }    
    
    
	/**
	 * Get the tab of re-open file.
	 * 
	 * @return the tab of re-open file
	 */
	public ReOpenFile[] getReOpenFile() {
		return reOpenFile;
	}
	
	
	/**
	 * Enable/disable the configureOptimisationAction.
	 * 
	 * @param value the value to set to the state of the action
	 */
	public void setEnabledConfigureOptimisationAction(boolean value) {
		runAllAction.setEnabled(value);
	}
	
	
	/**
	 * Enable/disable the configureSimulationAction.
	 * 
	 * @param value the value to set to the state of the action
	 */
	public void setEnableConfigureSimulationAction(boolean value) {
		configureSimulationAction.setEnabled(value);
	}
	
	public ConfigureSimulationAction getConfigureSimulationAction() {
		return configureSimulationAction;
	}
	
	
	/**
	 * Show allowed or hide not allowed actions when there is an open file.
	 * 
	 * @param value Show or hide actions
	 */
	public void showOpenMenu(boolean value)
	{
		/* Show/Hide toolMenu actions */
        launchOptimisationAction.setEnabled(value);
        launchOctaveAction.setEnabled(value);
        launchVerifAction.setEnabled(value);
        runAllAction.setEnabled(value);
        //launchDotGeneration.setEnabled(value);
        viewDotTreeFile.setEnabled(value);
        viewDotFlatFile.setEnabled(value);
        viewDynamicStructure.setEnabled(value);
        generateVHDL.setEnabled(value);
        clearProject.setEnabled(value);
        //generateM2MCodeAction.setEnabled(value);
        launchOctaveWithRegenAction.setEnabled(value);
        //compareOctaveResults.setEnabled(value);
        launchUbiduleVerification.setEnabled(value);
        launchXilinxUbiduleAction.setEnabled(value);
        launchRunAllUbiduleAction.setEnabled(value);
        
        /* Show/Hide editMenu actions */
        undoAction.setEnabled(value);
        redoAction.setEnabled(value);
        cutAction.setEnabled(value);
        copyAction.setEnabled(value);
        pasteAction.setEnabled(value);
        deleteAction.setEnabled(value);
        selectAllAction.setEnabled(value);
        commentUncommentAction.setEnabled(value);
        commentAction.setEnabled(value);
        uncommentAction.setEnabled(value);
        
        /* Show/Hide general actions */
        configureSimulationAction.setEnabled(value);
        configureOptimisationAction.setEnabled(value);
        saveFileAction.setEnabled(value);
        saveAsFileAction.setEnabled(value);
	}
	
}
