/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Properties of the graphical interface.
 *
 * Project:  Math2Mat
 *
 * @file GUIProperties.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Gets the properties of the GUI in the properties file, ie. all the properties that do not depend on a project.
 * It is the properties of external tools.
 * 
 */

package m2m.frontend;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.Platform;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

//import org.xml.sax.SAXException;
import java.io.IOException;

import m2m.backend.utils.XMLUtils;

public class GUIProperties {

	private static GUIProperties singleProp = null;
	
	static public GUIProperties getReference() {
		if (singleProp==null)
			singleProp=new GUIProperties();
		return singleProp;
	}
	

	private String validateDir;
	
	public String getValidateDir() {
		return validateDir;
	}
	
	public void setValidateDir(String dir) {
		validateDir = dir;
	}
	
	private String[] reOpenFile = new String[5];
	
	public String getXmlTagName() {
		return "ReOpenFile";
	}

	public boolean fromXml(Element fstNmElmnt) {
		setValidateDir(XMLUtils.getTextValue(fstNmElmnt, "validateDir",""));
		for(int findex=1;findex<6;findex++) {
			this.setReOpenFileAtIndex(XMLUtils.getTextValue(fstNmElmnt,"File"+findex,"No file"),findex-1);
		}
		return true;
	}
	
	public Element toXml(Document dom) {
		Element el = dom.createElement(getXmlTagName());
		el.appendChild(XMLUtils.createTextElement(dom, "validateDir", validateDir));

		for(int i=1;i<6;i++) {
			Element fEl = dom.createElement("File"+i);
			Text path = dom.createTextNode(reOpenFile[i-1]);
			fEl.appendChild(path);
			el.appendChild(fEl);
		}
		return el;
	}

	/**
	 * Get the path of a specific reopen file
	 * 
	 * @param index int the index of the reopen file
	 * @return the path of a specific reopen file
	 */
	public String getReOpenFileAtIndex(int index) {
		return reOpenFile[index];
	}
	
	/**
	 * Get all path of reopen files
	 * 
	 * @return all path of reopen file
	 */
	public String[] getReOpenFile() {
		return reOpenFile;
	}
	
	/**
	 * Set the path of a specific reopen file
	 * 
	 * @param path  String  the value of the path of the reopen file
	 * @param index int		the index of the reopen file
	 */
	public void setReOpenFileAtIndex(String value, int index) {
		reOpenFile[index] = value;
	}
	
	
	/**
	 * Check if the property file exists and is not empty. If it is the case, it gets the information it contains.
	 */
	private GUIProperties() {
		this.validateDir=new String("");
		for(int i=0;i<5;i++)
			reOpenFile[i]=new String("No file");
		readSettings();
	}
	
	protected void finalize() {
		
	}
	
	public boolean writeSettings() {
		//load the GUI property file
		File file = Platform.getBundle("m2mgui").getBundleContext().getDataFile("m2mprop");

//		File file = new File(new File("").getAbsolutePath()+"/M2MProperties.prop");	
		return writeSettings(file);
	}
	
	public boolean readSettings() {
		File file = Platform.getBundle("m2mgui").getBundleContext().getDataFile("m2mprop");
	//	String location=Platform.getBundle("m2mgui").getLocation();
	//	int pos=location.indexOf("file:");
	//	location=location.substring(pos+5);
	//	System.out.println("GetLocation2: "+location);			
	
		
		if (file==null)
			return false;
		if (file.length()==0)
			return false;
		return readSettings(file);
	}
	
public boolean readSettings(File f) {
		
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			Document dom = db.parse(f);

			dom.getDocumentElement().normalize();
			  System.out.println("Root element " + dom.getDocumentElement().getNodeName());
			  
			  Element docEl = dom.getDocumentElement();

			  NodeList reOpenLst = docEl.getElementsByTagName(this.getXmlTagName());
			  if (reOpenLst != null && reOpenLst.getLength()>0) {
				  Element fstNmElmnt = (Element) reOpenLst.item(0);
				  this.fromXml(fstNmElmnt);
			  }
				
			  
			  

		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		}catch(SAXException se) {
			se.printStackTrace();
			return false;
		}catch(IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean writeSettings(File f) {
		//get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			Document dom = db.newDocument();
		
		
			
			//create the root element 
			Element rootEl = dom.createElement("M2MSettings");
			dom.appendChild(rootEl);
			

			rootEl.appendChild(this.toXml(dom));
			
            /////////////////
            //Output the XML

            //set up a transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(dom);
            trans.transform(source, result);
            String xmlString = sw.toString();
            
            try{
                // Create file 
                FileWriter fstream = new FileWriter(f);
                    BufferedWriter out = new BufferedWriter(fstream);
                out.write(xmlString);
                //Close the output stream
                out.close();
                }catch (Exception e){//Catch exception if any
                  System.err.println("Error: " + e.getMessage());
                }
            
		}
		catch(ParserConfigurationException pce) {
			//dump it
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			return false;
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
}
