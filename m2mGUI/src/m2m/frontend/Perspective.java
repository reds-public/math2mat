/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Eclipse RCP class. Window placement and initialisation.
 *
 * Project:  Math2Mat
 *
 * @file Perspective.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This is an Eclipse RCP class. This class places the sub-windows in the main window.
 * 
 *   It also initialize the BuildingBlocksManager.
 *
 */

package m2m.frontend;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import m2m.backend.buildingblocks.BuildingBlocksManager;

import m2m.backend.project.M2MProject;
import m2m.frontend.dynamicview.MyGraphicalEditor;
import m2m.frontend.dynamicview.MyGraphicalProperty;
import m2m.frontend.view.M2MConsole;
import m2m.frontend.view.NavigationView;


public class Perspective implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);

		layout.addStandaloneView(NavigationView.ID,  false, IPageLayout.LEFT, 0.25f, editorArea);
		layout.getViewLayout(NavigationView.ID).setCloseable(false);
		layout.getViewLayout(NavigationView.ID).setMoveable(false);
		
		IFolderLayout folderLayout = layout.createFolder(M2MConsole.ID, IPageLayout.BOTTOM, 0.8f, editorArea);
		folderLayout.addView(M2MConsole.ID);
		
		/* Create and locate the dynamic view */
		layout.addPlaceholder(MyGraphicalEditor.ID, IPageLayout.BOTTOM, 0.4f, editorArea);
		layout.getViewLayout(MyGraphicalEditor.ID).setMoveable(false);

		/* Create and locate the dynamic property view */
		layout.addPlaceholder(MyGraphicalProperty.ID, IPageLayout.LEFT, 0.3f, MyGraphicalEditor.ID);
		layout.getViewLayout(MyGraphicalProperty.ID).setMoveable(false);
		
		/* Initialization of the manager */
//		String path = f.getAbsolutePath() + "/lib/blocks";
		String path = M2MProject.getLibPath()+"/blocks";
		BuildingBlocksManager.getInstance().setDirName(path);
		BuildingBlocksManager.getInstance().setPackageName("m2m.backend.buildingblocks.blocks");
		BuildingBlocksManager.getInstance().loadAll();
	}
}
