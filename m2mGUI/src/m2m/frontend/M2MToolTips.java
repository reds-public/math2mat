/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file M2MToolTips.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Mar 11, 2011
 *
 * Author: Yann Thoma
 *
 * Description: This class only stores the String of the various
 *              tooltips, in order to centralize the descriptions.
 * 
 */
package m2m.frontend;

/**
 * @author ythoma
 *
 */
public class M2MToolTips {
	
	public final static String SIMPROPERTIES_CALCPRECISION =
		"The calculation precision defines how error calculations are detected. It corresponds to the number of bits of the mantissa that can differ between the expected and the real results.";
	
	public final static String SIMPROPERTIES_NBSAMPLES=
		"The number of samples to test. One sample corresponds to a single call to the function.";
	
	public final static String SIMPROPERTIES_SYSFREQ=
		"The system frequency is the frequency at which the system should work.\nThe three frequencies are used in order to more or less stress the system. For each input of the system (each input variable), a valid input is applied at a random rate with probability (input frequency/system frequency). For each output of the system (each output variable), a ready signal is applied at a random rate with probability (output frequency/system frequency).";

	public final static String SIMPROPERTIES_INPUTFREQ=
		"The input frequency corresponds to the rate at which input data should be applied to the system.The three frequencies are used in order to more or less stress the system. For each input of the system (each input variable), a valid input is applied at a random rate with probability (input frequency/system frequency). For each output of the system (each output variable), a ready signal is applied at a random rate with probability (output frequency/system frequency).";

	public final static String SIMPROPERTIES_OUTPUTFREQ=
		"The output frequency corresponds to the rate at which output data should be retrieved.The three frequencies are used in order to more or less stress the system. For each input of the system (each input variable), a valid input is applied at a random rate with probability (input frequency/system frequency). For each output of the system (each output variable), a ready signal is applied at a random rate with probability (output frequency/system frequency).";
	
	public final static String SIMPROPERTIES_INACTIVITY=
		"The period of inactivity allows to automatically end the simulation if no activity is detected on the output for a certain number of clock cycles.";

	public final static String SIMPROPERTIES_INPUTMINIMUM=
		"The minimum generated value for the input.";
	
	public final static String SIMPROPERTIES_INPUTMAXIMUM=
		"The maximum generated value for the input.";
}
