/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Run all action.
 *
 * Project:  Math2Mat
 *
 * @file RunAll.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 12.10.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action runs all the tools in the correct order, i.e. launches the parser, launches optimisation,
 * generates the VHDL, launches the test generation, and finishes with the verification.
 * 
 */

package m2m.frontend.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ExternalRuns;
import m2m.backend.processing.ProgressMonitoring;
import m2m.frontend.GraphicalProgressMonitoring;
import m2m.frontend.ErrorProcessing;

public class RunAll extends M2MAction 
{
	/**
	 * Default contructor of a RunAll action.
	 * @param label the label of the RunAll action
	 */
	public RunAll(IWorkbenchWindow window, String label) {
		super(window,label);
		setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/icon_play.png"));
		
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_RUNALL);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_RUNALL);
	}
	
	
	public void execute(IProgressMonitor monitor)
	{
		Errors.clearError();
		monitor.beginTask("Running all",ProgressMonitoring.workingTimeAll(editor.getM2MProject()));
		GraphicalProgressMonitoring mon=new GraphicalProgressMonitoring();
		mon.setIProgressMonitor(monitor);
		editor.getM2MProject().monitoring=mon;
		ExternalRuns.runAll(editor.getM2MProject());
		ErrorProcessing.processLastError(editor.getShell().getDisplay());
		
		editor.getM2MProject().monitoring=new ProgressMonitoring();
				
	}
	/**
	 * Method executed when the action is called
	 */
	public void run() 
	{	

		if (!getEditor("Error","Select the tab with the m2m code"))
			return;
		
		if (editor.getParseDone())
		{
			activateConsole();


			IRunnableWithProgress op = new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor) {
					execute(monitor);
				}
			};
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
			Shell shell = win != null ? win.getShell() : null;
			try {
				new ProgressMonitorDialog(shell).run(true, false, op);
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		else
			MessageDialog.openError(window.getShell(), "Parsing error", "Parse the file to get inputs and outputs before trying to launch Octave");
	}
}