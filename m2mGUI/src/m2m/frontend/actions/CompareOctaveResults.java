/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file CompareOctaveResults.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 31 mai 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.frontend.actions;

import org.eclipse.ui.IWorkbenchWindow;

import m2m.frontend.actions.M2MAction;

/**
 * @author masles
 *
 */
public class CompareOctaveResults extends M2MAction {
	
	public CompareOctaveResults(IWorkbenchWindow window, String label) {
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_COMPAREOCTAVERESULTS);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_COMPAREOCTAVERESULTS);
		//setImageDescriptor(m2mgui.Activator.getImageDescriptor("/icons/parser.gif"));
	}

	/**
	 * Method executed when the action is called
	 */
	public void run() {
		if (!getEditor("Error","There is no opened file to parse"))
			return;
		activateConsole();
		
		editor.getM2MProject().compareOctaveResult();			
	}
	
}
