/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to launch verification.
 *
 * Project:  Math2Mat
 *
 * @file LaunchVerifAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action generates the testbench and launches it with QuestaSim to check for errors.
 *
 */

package m2m.frontend.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ExternalRuns;
import m2m.backend.processing.ProgressMonitoring;
import m2m.frontend.ErrorProcessing;
import m2m.frontend.GraphicalProgressMonitoring;
import m2m.frontend.actions.M2MAction;


public class LaunchVerifAction extends M2MAction 
{
	
	public LaunchVerifAction(IWorkbenchWindow window, String label) 
	{
		super(window,label);
		setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/verif.gif"));
		
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_VERIF);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_VERIF);
	}

	
	
	
	

	public void execute(IProgressMonitor monitor)
	{
		

		Errors.clearError();

		monitor.beginTask("Running Simulation",editor.getM2MProject().getSimulationProperties().getNumberOfSamples());
		GraphicalProgressMonitoring mon=new GraphicalProgressMonitoring();
		mon.setIProgressMonitor(monitor);
		editor.getM2MProject().monitoring=mon;		
		if (ExternalRuns.runQuesta(editor.getM2MProject()))
		{
			if (ExternalRuns.getNbSimErrors()!=0) {
//				ErrorProcessing.displayError(editor.getShell().getDisplay(),"Simulation contains "+ExternalRuns.getNbSimErrors()+" errors!");
			//	return;
			}
			
		}
		ErrorProcessing.processLastError(editor.getShell().getDisplay());
		
		
		editor.getM2MProject().monitoring=new ProgressMonitoring();
				
	}
	/**
	 * Method executed when the action is called
	 */
	public void run() 
	{	

		if (!getEditor("Error","Select the tab with the code you want to transform in VHDL"))
			return;

		boolean octaveOK=editor.getM2MProject().isOctaveUpToDate();
		boolean vhdlOK=editor.getM2MProject().isVHDLUpToDate();
		if (!(octaveOK && vhdlOK)) {
			MessageDialog.openError(window.getShell(), "Error", "Please generate VHDL and octave files before launch verification");
			return;
		}
		if (!vhdlOK) {
			MessageDialog.openError(window.getShell(), "Error", "Please generate VHDL files before launch verification");
			return;
		}
		if (!octaveOK) {
			MessageDialog.openError(window.getShell(), "Error", "Please generate Octave files before launch verification");
			return;
		}
		activateConsole();


		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) {
				execute(monitor);
			}
		};
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		Shell shell = win != null ? win.getShell() : null;
		try {
			new ProgressMonitorDialog(shell).run(true, false, op);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		/*
			Shell shell=new Shell();


		    // Create a smooth progress bar
		    ProgressBar pb1 = new ProgressBar(shell, SWT.HORIZONTAL | SWT.SMOOTH);
		    pb1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		    pb1.setMinimum(0);
		    pb1.setMaximum(30);
		    pb1.setSelection(10);
		    shell.open();
		 */

		/* Launch the octaveThread */
		//		RunAllThread runAllThread = new RunAllThread(editor,editor.getM2MProject());
		//		runAllThread.start();	
	} 
	
	
	
	/**
	 * Method executed when the action is called
	 */
/*	public synchronized void run() 
	{	
			
		/* Launch the questaThread 
		QuestaThread questaThread = new QuestaThread(editor);
		questaThread.start();	    
	}*/
}