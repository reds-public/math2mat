/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to show the dynamic view.
 *
 * Project:  Math2Mat
 *
 * @file ViewDynamicStructure.java
 * @author Daniel Molla
 *
 * @version 1.0
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This action opens the dynmic view.
 *
 */
package m2m.frontend.actions;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


import m2m.frontend.actions.M2MAction;
import m2m.frontend.dynamicview.Model;
import m2m.frontend.dynamicview.MyGraphicalEditor;
import m2m.frontend.dynamicview.MyGraphicalProperty;

public class ViewDynamicStructure extends M2MAction 
{
	/**
	 * Constructor of the dynamic view action.
	 * @param label the label to the action to set
	 */
	public ViewDynamicStructure(IWorkbenchWindow window, String label) {
		super(window,label);
		setId(ICommandIds.CMD_DYNAMICVIEW);
		setActionDefinitionId(ICommandIds.CMD_DYNAMICVIEW);
	}
	
	
	/**
	 * Method executed when the action is called.
	 */
	public void run() 
	{	
		if (!getEditor("Error","Select the tab with the code you want to transform in VHDL"))
			return;
		activateConsole();
		
		/* Show the dynamic view */
   		if(editor.getM2MProject().getStructTreatment().getTop() != null) {
	        // TODO remettre le traitement de l'exception
   			//try {       
	        	editor.getM2MProject().getStructTreatment().setProject(editor.getM2MProject());
	    		editor.setModel(new Model (editor.getM2MProject().getStructTreatment(), editor.getM2MProject()));    			
		        try {		       
		    		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(); 	
		        	/* Creation of two editors */
		            page.showView(MyGraphicalEditor.ID);     
		            page.showView(MyGraphicalProperty.ID);   
		        } 
		        catch (Exception e) {
		            e.printStackTrace();
		        }
	        //} 
	        //catch (Exception e) 
	        //{
	        //	System.out.println("Cannot launch the dynamic view!");
			//}
   		}
        else
        	System.out.println("Cannot launch the dynamic view! Please parse the octave code.");		
	}
}
