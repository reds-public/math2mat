/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to reopens a precedent file.
 *
 * Project:  Math2Mat
 *
 * @file ReOpenFile.java
 * @author Daniel Molla
 *
 * @version 1.0
 *
 * @date: 26.10.2010
 *
 * Author: Daniel Molla
 *
 * Description: This action reopens a precedent file.
 * 
 */
package m2m.frontend.actions;


import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;

import m2m.frontend.actions.M2MAction;


public class ReOpenFile extends M2MAction
{
	/**
	 * Path of the file to reopen.
	 */
	private String path;
	
	
	/**
	 * Constructor of the dynamic view action.
	 * @param label the label to the action to set
	 */
	public ReOpenFile(IWorkbenchWindow window, String value) 
	{
		super(window,value);
		setId(ICommandIds.CMD_REOPEN);
		setActionDefinitionId(ICommandIds.CMD_REOPEN);
		path = value;
	}

	/**
	 * Method executed when the action is called.
	 */
	public void run() 
	{      
       	if (!OpenFileAction.openFile(path))
       		MessageDialog.openError(window.getShell(), "File error", "File project is corrupted\nThe source file path does not refer to a file");
	}
	
	/**
	 * Set the path of the ReOpenfile action.
	 * @param path the path of the ReOpenfile action to set
	 */
	public void setPath(String path) {
		this.path = path;
        setText(path);    
	}
	
	
	/**
	 * Get the pat of the ReOpenFile action.
	 * @return the path of the ReOpenfile action 
	 */
	public String getPath() {
		return path;
	}
}
