/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Saves text modifications.
 *
 * Project:  Math2Mat
 *
 * @file TextChange.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 03.05.2010
 *
 * Author: Sebastien Masle
 *
 * Description: This class saves modifications that occurred on a text. Useful for undo and redo actions.
 *
 */

package m2m.frontend.actions;

public class TextChange {
	// The starting offset of the change
	private int start;
	
	// The length of the change
	private int length;
	
	// The replaced text
	String replacedText;
	
	/**
	* Constructs a TextChange
	* 
	* @param start
	*          the starting offset of the change
	* @param length
	*          the length of the change
	* @param replacedText
	*          the text that was replaced
	*/
	public TextChange(int start, int length, String replacedText) {
		this.start = start;
		this.length = length;
		this.replacedText = replacedText;
	}
	
	/**
	* Returns the start
	* 
	* @return int
	*/
	public int getStart() {
		return start;
	}
	
	/**
	* Returns the length
	* 
	* @return int
	*/
	public int getLength() {
		return length;
	}
	
	/**
	* Returns the replacedText
	* 
	* @return String
	*/
	public String getReplacedText() {
		return replacedText;
	}
}
