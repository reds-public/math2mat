/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to configure external tools.
 *
 * Project:  Math2Mat
 *
 * @file ConfigureExternalToolsAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action opens a new window to configure the path of external tools.
 *
 */

package m2m.frontend.actions;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;

import m2m.frontend.actions.M2MAction;
import m2m.backend.project.ExternalToolsProperties;

public class ConfigureExternalToolsAction extends M2MAction{
	
	private String selectedPath;

	public ConfigureExternalToolsAction(IWorkbenchWindow window, String label) {
		super(window,label);
        
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CONFOPTIM);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_CONFOPTIM);
	}
	
	/**
	 * Method executed when the action is called
	 */
	public void run() {
		Display display = window.getShell().getDisplay();
		final Shell shell = new Shell(display);
		shell.setText("External tools configuration");
		ExternalToolsPropertiesDlg dlg=new ExternalToolsPropertiesDlg(shell);
		dlg.open();
	}
	
	

	class ExternalToolsPropertiesDlg extends Dialog {
		
		private Text ubiduleIP;
		private Text isePath;
		private Text xilinxLicense;
		private Text libPath;
		private Text octavePath;
		private Text vsimPath;
		private Text dotPath;
		private Combo simulationFiles;
		private Combo simulator;
		

		public ExternalToolsPropertiesDlg(Shell parentShell) {
			super(parentShell);
		}

		protected void configureShell(Shell shell) {
			super.configureShell(shell);
		    shell.setText("External tools properties");
		}

		
		@Override
		public void create() {
			super.create();
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			GridLayout layout = new GridLayout();
			parent.setLayout(layout);
		
		GridLayout pathLayout = new GridLayout();
		pathLayout.numColumns = 3;
		
		GridLayout buttonLayout = new GridLayout();
		buttonLayout.numColumns = 2;
		
		Group pathExec = new Group(parent, SWT.SHADOW_ETCHED_IN);
		//pathExec.setText("External tools configuration");
		pathExec.setBounds(parent.getClientArea());
		pathExec.setLayout(layout);
		
		Composite pathComp = new Composite(pathExec, SWT.FILL);
		pathComp.setLayout(pathLayout);
		
		Composite buttonComp = new Composite(pathExec, SWT.RIGHT);
		buttonComp.setLayout(buttonLayout);

		
		///////////////////////////////////////////////////
		//            Library path selection              //
		///////////////////////////////////////////////////
		Label libPathLabel = new Label(pathComp, SWT.NONE);
		libPathLabel.setText("Lib path :");
		libPathLabel.setBounds(parent.getClientArea());
		libPathLabel.setLocation(10, 25);
		libPathLabel.pack();
		
		//Octave path entry placement
		GridData libPathGridDataText = new GridData();
		libPathGridDataText.widthHint = 250;
		libPathGridDataText.heightHint = 15;
		
		//create Octave path entry
		libPath = new Text(pathComp, SWT.NONE);
		libPath.setBounds(105, 23, 1500, 25);
		libPath.setLocation(105, 23);
		libPath.setLayoutData(libPathGridDataText);
		libPath.pack();
		
		//create "browse" button for the Octave path entry
		Button findlibPath = new Button(pathComp, SWT.CENTER);
		findlibPath.setBounds(parent.getBounds());
		findlibPath.setText("Browse...");
		findlibPath.setLocation(250, 23);
		findlibPath.pack();
		
		//"browse" button's listener
		class openListener implements Listener {
			private Text textEntry = null;

			public openListener(Text textEntry) {
				this.textEntry = textEntry;
			}
			
			@Override
			public void handleEvent(Event event) {
				FileDialog dialog = new FileDialog(new Shell(), SWT.OPEN);//java.awt.FileDialog.LOAD
		        dialog.setText("Choose path...");
		        selectedPath = dialog.open();
		        if(selectedPath != null)
		        	textEntry.setText(selectedPath);
			}
		};

		//"browse" button's listener
		class openDirListener implements Listener {
			private Text textEntry = null;
			private Shell shell;

			public openDirListener(Text textEntry,Shell shell) {
				this.textEntry = textEntry;
				this.shell=shell;
			}
			
			@Override
			public void handleEvent(Event event) {
				DirectoryDialog dlg = new DirectoryDialog(shell);
				dlg.setText("Choose a directory");
				dlg.setMessage("Please choose a directory");
				dlg.setFilterPath(textEntry.getText());
				String dir=dlg.open();
				if (dir!=null)
		        	textEntry.setText(dir);
			}
		};
		
		findlibPath.addListener(SWT.Selection, new openDirListener(libPath,this.getShell()));

		
		
		///////////////////////////////////////////////////
		//            Octave path selection              //
		///////////////////////////////////////////////////
		Label octavePathLabel = new Label(pathComp, SWT.NONE);
		octavePathLabel.setText("Octave path :");
		octavePathLabel.setBounds(parent.getClientArea());
		octavePathLabel.setLocation(10, 25);
		octavePathLabel.pack();
		
		//Octave path entry placement
		GridData octaveGridDataText = new GridData();
		octaveGridDataText.widthHint = 250;
		octaveGridDataText.heightHint = 15;
		
		//create Octave path entry
		octavePath  = new Text(pathComp, SWT.NONE);
		octavePath.setBounds(105, 23, 1500, 25);
		octavePath.setLocation(105, 23);
		octavePath.setLayoutData(octaveGridDataText);
		octavePath.pack();
		
		//create "browse" button for the Octave path entry
		Button findOctavePath = new Button(pathComp, SWT.CENTER);
		findOctavePath.setBounds(parent.getBounds());
		findOctavePath.setText("Browse...");
		findOctavePath.setLocation(250, 23);
		findOctavePath.pack();
		
		
		findOctavePath.addListener(SWT.Selection, new openListener(octavePath));

		///////////////////////////////////////////////////
		//            Vsim path selection              //
		///////////////////////////////////////////////////
		Label vsimPathLabel = new Label(pathComp, SWT.NONE);
		vsimPathLabel.setText("Vsim path :");
		vsimPathLabel.setBounds(parent.getClientArea());
		vsimPathLabel.setLocation(10, 50);
		vsimPathLabel.pack();
		
		//Octave path entry placement
		GridData vsimGridDataText = new GridData();
		vsimGridDataText.widthHint = 250;
		vsimGridDataText.heightHint = 15;
		
		//create Octave path entry
		vsimPath = new Text(pathComp, SWT.NONE);
		vsimPath.setBounds(parent.getBounds());
		vsimPath.setLocation(105, 50);
		vsimPath.setLayoutData(vsimGridDataText);
		vsimPath.pack();
		
		//create "browse" button for the Octave path entry
		Button findVsimPath = new Button(pathComp, SWT.NONE);
		findVsimPath.setBounds(parent.getBounds());
		findVsimPath.setText("Browse...");
		findVsimPath.setLocation(250, 50);
		findVsimPath.pack();
		
		findVsimPath.addListener(SWT.Selection, new openListener(vsimPath));
		

		///////////////////////////////////////////////////
		//            ISE path selection              //
		///////////////////////////////////////////////////
		Label isePathLabel = new Label(pathComp, SWT.NONE);
		isePathLabel.setText("Xilinx ISE path :");
		isePathLabel.setBounds(parent.getClientArea());
		isePathLabel.setLocation(10, 50);
		isePathLabel.pack();
		
		//ISE path entry placement
		GridData iseGridDataText = new GridData();
		iseGridDataText.widthHint = 250;
		iseGridDataText.heightHint = 15;
		
		//create ISE path entry
		isePath = new Text(pathComp, SWT.NONE);
		isePath.setBounds(parent.getBounds());
		isePath.setLocation(105, 50);
		isePath.setLayoutData(iseGridDataText);
		isePath.pack();
		
		//create "browse" button for the ISE path entry
		Button findIsePath = new Button(pathComp, SWT.NONE);
		findIsePath.setBounds(parent.getBounds());
		findIsePath.setText("Browse...");
		findIsePath.setLocation(250, 50);
		findIsePath.pack();
		
		findIsePath.addListener(SWT.Selection, new openDirListener(isePath,this.getShell()));
		


		///////////////////////////////////////////////////
		//            dot path selection              //
		///////////////////////////////////////////////////
		Label dotPathLabel = new Label(pathComp, SWT.NONE);
		dotPathLabel.setText("Dot path :");
		dotPathLabel.setBounds(parent.getClientArea());
		dotPathLabel.setLocation(10, 50);
		dotPathLabel.pack();
		
		//dot path entry placement
		GridData dotGridDataText = new GridData();
		dotGridDataText.widthHint = 250;
		dotGridDataText.heightHint = 15;
		
		//create dot path entry
		dotPath = new Text(pathComp, SWT.NONE);
		dotPath.setBounds(parent.getBounds());
		dotPath.setLocation(105, 50);
		dotPath.setLayoutData(dotGridDataText);
		dotPath.pack();
		
		//create "browse" button for the dot path entry
		Button findDotPath = new Button(pathComp, SWT.NONE);
		findDotPath.setBounds(parent.getBounds());
		findDotPath.setText("Browse...");
		findDotPath.setLocation(250, 50);
		findDotPath.pack();
		
		findDotPath.addListener(SWT.Selection, new openListener(dotPath));
		
		


		///////////////////////////////////////////////////
		//            Simulation files selection         //
		///////////////////////////////////////////////////
		Label simFilesLabel = new Label(pathComp, SWT.NONE);
		simFilesLabel.setText("Simulation files :");
		simFilesLabel.setBounds(parent.getClientArea());
		simFilesLabel.setLocation(10, 50);
		simFilesLabel.pack();
		
		//dot path entry placement
		GridData simFilesText = new GridData();
		simFilesText.widthHint = 250;
		simFilesText.heightHint = 15;
				
		//create dot path entry
		simulationFiles = new Combo(pathComp, SWT.NONE);
		simulationFiles.add("Original OVM");
		simulationFiles.add("M2M OVM");
		simulationFiles.add("Original UVM");
		
		Label emptyLabel = new Label(pathComp, SWT.NONE);
		emptyLabel.setText("");
		

		///////////////////////////////////////////////////
		//            Simulator selection                //
		///////////////////////////////////////////////////
		Label simulatorLabel = new Label(pathComp, SWT.NONE);
		simulatorLabel.setText("Simulator :");
		simulatorLabel.setBounds(parent.getClientArea());
		simulatorLabel.setLocation(10, 50);
		simulatorLabel.pack();
		
		//dot path entry placement
		GridData simulatorText = new GridData();
		simulatorText.widthHint = 250;
		simulatorText.heightHint = 15;
				
		//create dot path entry
		simulator = new Combo(pathComp, SWT.NONE);
		for(int i=0;i<ExternalToolsProperties.simulatorTypes.length;i++)
			simulator.add(ExternalToolsProperties.simulatorTypes[i]);

		Label emptyLabel0 = new Label(pathComp, SWT.NONE);
		emptyLabel0.setText("");
		
		///////////////////////////////////////////////////
		//            Xilinx license                     //
		///////////////////////////////////////////////////
		Label xilinxLicenseLabel = new Label(pathComp, SWT.NONE);
		xilinxLicenseLabel.setText("Xilinx license :");
		xilinxLicenseLabel.setBounds(parent.getClientArea());
		xilinxLicenseLabel.setLocation(10, 50);
		xilinxLicenseLabel.pack();
		
		//dot path entry placement
		GridData xilinxLicenseText = new GridData();
		xilinxLicenseText.widthHint = 250;
		xilinxLicenseText.heightHint = 15;
				
		//create dot path entry
		xilinxLicense = new Text(pathComp, SWT.NONE);
		xilinxLicense.setBounds(parent.getBounds());
		xilinxLicense.setLocation(105, 50);
		xilinxLicense.setLayoutData(xilinxLicenseText);
		xilinxLicense.pack();

		Label emptyLabel1 = new Label(pathComp, SWT.NONE);
		emptyLabel1.setText("");

		///////////////////////////////////////////////////
		//            Ubidule IP                         //
		///////////////////////////////////////////////////
		Label ubiduleIPLabel = new Label(pathComp, SWT.NONE);
		ubiduleIPLabel.setText("Ubidule IP :");
		ubiduleIPLabel.setBounds(parent.getClientArea());
		ubiduleIPLabel.setLocation(10, 50);
		ubiduleIPLabel.pack();
		
		//dot path entry placement
		GridData ubiduleIPText = new GridData();
		ubiduleIPText.widthHint = 250;
		ubiduleIPText.heightHint = 15;
				
		//create dot path entry
		ubiduleIP = new Text(pathComp, SWT.NONE);
		ubiduleIP.setBounds(parent.getBounds());
		ubiduleIP.setLocation(105, 50);
		ubiduleIP.setLayoutData(ubiduleIPText);
		ubiduleIP.pack();

		Label emptyLabel2 = new Label(pathComp, SWT.NONE);
		emptyLabel2.setText("");

		
		
		
		libPath.setText(ExternalToolsProperties.getReference().getLibPath());
		isePath.setText(ExternalToolsProperties.getReference().getIsePath());
		vsimPath.setText(ExternalToolsProperties.getReference().getVsimPath());
		octavePath.setText(ExternalToolsProperties.getReference().getOctavePath());
		dotPath.setText(ExternalToolsProperties.getReference().getDotPath());
		simulationFiles.select(ExternalToolsProperties.getReference().getSimulationFiles());
		simulator.select(ExternalToolsProperties.getReference().getSimulator());
		xilinxLicense.setText(ExternalToolsProperties.getReference().getXilinxLicense());
		ubiduleIP.setText(ExternalToolsProperties.getReference().getUbiduleIP());
		

			return parent;
		}


		protected void okPressed() {
			ExternalToolsProperties props = ExternalToolsProperties.getReference();

			if (libPath.getText().compareTo(props.getLibPath())!=0) {
				MessageBox mess=new MessageBox(window.getShell(),SWT.OK);
				mess.setMessage("You changed the location of the lib files. Please restart the application.");
				mess.setText("Important notice");
				mess.open();
			}
			
			props.setLibPath(libPath.getText());
			props.setOctavePath(octavePath.getText());
			props.setVsimPath(vsimPath.getText());
			props.setIsePath(isePath.getText());
			props.setDotPath(dotPath.getText());
			props.setSimulationFiles(simulationFiles.getSelectionIndex());
			props.setSimulator(simulator.getSelectionIndex());
			props.setXilinxLicense(xilinxLicense.getText());
			props.setUbiduleIP(ubiduleIP.getText());
			
			this.close();
		}
		
	}
	
}
