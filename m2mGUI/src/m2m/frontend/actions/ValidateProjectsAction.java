/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ValidateProjectsAction.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Jan 14, 2011
 *
 * Author: Yann Thoma
 *
 * Description:
 * 
 */
package m2m.frontend.actions;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ProjectsValidator;
import m2m.frontend.GUIProperties;
import m2m.frontend.GraphicalProgressMonitoring;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * @author ythoma
 *
 */
public class ValidateProjectsAction extends M2MAction {

	public ValidateProjectsAction(IWorkbenchWindow window, String label) 
	{
		super(window,label);
		
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_VALIDATEPROJECTS);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_VALIDATEPROJECTS);
	}

	
	private String path;

	public void execute(IProgressMonitor monitor)
	{
		Errors.clearError();
		ProjectsValidator validator=new ProjectsValidator();
		int nbWorkingSteps=validator.nbWorkingSteps(path);
		int nbProjects=validator.nbProjects;
		monitor.beginTask("Testing "+nbProjects+" projects",nbWorkingSteps);
		GraphicalProgressMonitoring mon=new GraphicalProgressMonitoring();
		mon.setIProgressMonitor(monitor);
		validator.testDir(path,mon);
		monitor.done();				
	}

	
	/**
	 * Method executed when the action is called
	 */
	public synchronized void run() 
	{
		DirectoryDialog dlg=new DirectoryDialog(window.getShell());

		dlg.setFilterPath(GUIProperties.getReference().getValidateDir());
		path=dlg.open();
		if (path==null)
			return;
		File file=new File(path);
		if (!file.isDirectory())
			return;
		
		GUIProperties.getReference().setValidateDir(path);
		GUIProperties.getReference().writeSettings();

    	activateConsole();
			
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) {
				execute(monitor);
			}
		};
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		Shell shell = win != null ? win.getShell() : null;
		try {
			new ProgressMonitorDialog(shell).run(true, true, op);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   	}
}
