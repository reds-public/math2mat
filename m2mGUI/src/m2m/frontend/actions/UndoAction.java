/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Undo action.
 *
 * Project:  Math2Mat
 *
 * @file UndoAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 03.05.2010
 *
 * Author: Sebastien Masle
 *
 * Description: This action undoes actions that have been done.
 *
 */

package m2m.frontend.actions;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;

public class UndoAction extends M2MAction implements IWorkbenchAction {

	public UndoAction(IWorkbenchWindow window, String label) {
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_UNDO);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_UNDO);
		setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/Undo.png"));
	}

	/**
	 * Method executed when the action is called
	 */
	public void run(){
		if (!getEditor("Error","Select a correct M2M editor"))
			return;
		editor.undo();
	}
	
	public void setEnable(boolean enable) {
		this.setEnable(enable);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
