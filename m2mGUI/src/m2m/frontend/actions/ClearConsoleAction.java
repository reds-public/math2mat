/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ClearConsole.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Mar 14, 2011
 *
 * Author: Yann Thoma
 *
 * Description: This action allows to clear the current console.
 * 
 */
package m2m.frontend.actions;

import m2m.frontend.view.M2MConsole;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * @author ythoma
 *
 */
public class ClearConsoleAction extends M2MAction {


	public ClearConsoleAction(IWorkbenchWindow window, String label) {
		super(window,label);
		
	    // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CLEARCONSOLE);
		
	    // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_CLEARCONSOLE);
		// setImageDescriptor(com.reds.m2mgui.Activator.getImageDescriptor("/icons/octave.gif"));
	}
	
	public void run() {
		/* Clear the console */
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		if((M2MConsole)page.findView(M2MConsole.ID) != null)
		{
			((M2MConsole)page.findView(M2MConsole.ID)).clearConsole();
		}	
	}

}
