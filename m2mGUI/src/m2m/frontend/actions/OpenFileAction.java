/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to open a file.
 *
 * Project:  Math2Mat
 *
 * @file OpenFileAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action opens a source file or a project file. It also places the name of the file in the navigation view.
 *
 */

package m2m.frontend.actions;

import java.io.File;


import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.FileStoreEditorInput;

import m2m.backend.octaveparser.ParsingException;
import m2m.backend.project.M2MProject;
import m2m.frontend.ApplicationActionBarAdvisor;
import m2m.frontend.actions.M2MAction;
import m2m.frontend.GUIProperties;
import m2m.frontend.view.Editor;
import m2m.frontend.view.M2MConsole;
import m2m.frontend.view.M2MFileStoreEditorInput;
import m2m.frontend.view.NavigationView;


public class OpenFileAction extends M2MAction {
	
	public OpenFileAction(IWorkbenchWindow window, String label) {
		super(window,label);
        
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_OPEN);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_OPEN);
		setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/open.gif"));
	}

	/**
	 * Method executed when the action is called
	 */
	public void run() {
		//open file dialog
		FileDialog dialog = new FileDialog(this.window.getShell(), SWT.OPEN);
        dialog.setText("Open File");
        String[] filterExt = { "*.m2m"};
        String[] filterNames = {"Math2Mat project (*.m2m)"};
		dialog.setFilterExtensions(filterExt);
		dialog.setFilterNames(filterNames);
        String path= dialog.open();
        
        /* Check il a path has been selected */
        if(path == null)
        	return;
        if (!openFile(path))
        	MessageDialog.openError(window.getShell(), "File error", "File project is corrupted\nThe source file path does not refer to a file");
    	
	}
	
	static public boolean openFile(String path) {

		if (path==null)
			return false;
		
        File file = new File(path);

        if (!createProject(file))
        	return false;
        
        GUIProperties GUIProps = GUIProperties.getReference();
        		
        /* Set the external tools properties */
        String[] rof = new String[5];
        rof = GUIProps.getReOpenFile();
        int k = 4;
        if(rof != null)
        {
    		for(int i = 0; i < rof.length; i++)
    			if(rof[i].equalsIgnoreCase(path))
    			{
    				k = i;
    				break;
    			}
    		
			for(int i = 0; i < rof.length ; i++)
				GUIProps.setReOpenFileAtIndex(rof[i].equalsIgnoreCase("null") ? "No file" : rof[i], i);	
		
			for(int i = k; i > 0; i--)
				GUIProps.setReOpenFileAtIndex(rof[i-1], i);	
        }
        
        GUIProps.setReOpenFileAtIndex(path, 0);		
		
        /* Write the M2MProperties.prop file */
		GUIProps.writeSettings();
				
		/* Update reopen Action */
		ReOpenFile[] rofActionTab = ApplicationActionBarAdvisor.getInstance().getReOpenFile();
		for(int i = 0; i < rofActionTab.length; i++)
				rofActionTab[i].setPath(GUIProps.getReOpenFileAtIndex(i) == null ? "No File" : GUIProps.getReOpenFileAtIndex(i));
		ApplicationActionBarAdvisor.getInstance().repopulateFileMenu();
        
        return true;
	}

	
	static public boolean createProject(File file) {
		M2MProject m2mProject = new M2MProject();
		m2mProject.openProject(file.getAbsolutePath());

		boolean reload=false;
		if (!m2mProject.sourceFileExist()) {
			m2mProject.checkFolders();
			m2mProject.restoreOctave();
		}
		else if (!m2mProject.checkSourceFile()) {
			
			if (MessageDialog.openQuestion(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Warning", "The Octave seems to have been modified outside of Math2mat. Reload?")) {
				reload=true;
			}
			else {
				m2mProject.restoreOctave();
			}
		}
		
    	if (m2mProject.getSourceFile() != null) {
    	
        	//open a new editor with the file's content
        	IWorkbenchPage page= PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				IFileStore fileStore= EFS.getStore(file.toURI());
				
				//test if the project is already opened
				
				IEditorReference myReferences[];
				//get all opened editors with ID = Editor.ID
				myReferences = page.findEditors(new FileStoreEditorInput(fileStore), Editor.ID, org.eclipse.ui.IWorkbenchPage.MATCH_ID);
				for (int i = 0; i < myReferences.length; i++) {
					Editor editor = (Editor)myReferences[i].getEditor(true);
					if (editor.getM2MProject().compare(m2mProject))
					{	
						page.activate(editor);				
						return true;
					}
				}
				
				page.openEditor(new M2MFileStoreEditorInput(fileStore,m2mProject), Editor.ID, true);
				NavigationView.addList(file.getAbsolutePath());
			
				/* Open the console */
				try {
					page.showView(M2MConsole.ID);
				} catch (PartInitException e1) {
					e1.printStackTrace();
				}
				
				Editor editor = ((Editor)page.getActiveEditor());

				if (reload) {
					/* Initialisation of the structure */
					editor.getM2MProject().clearStructTreatment();
					editor.setParseDone(true);
					
					/* Parsing of the text of the editor */
					try {
						editor.getM2MProject().getStructTreatment().parse(editor.getStyledText().getText());
						editor.getM2MProject().getStructTreatment().modifyNumType(editor.getM2MProject().getProperties().getOptimisationProperties().getOptimisationDataType());
					}
					catch (ParsingException e1) {
						editor.setParseDone(false);
					}
				}
				else
					editor.setParseDone(true);
					
			} catch (CoreException e) {
				e.printStackTrace();
				return false;
			}
    	} else {
    		return false;
    	}
    	return true;
	}
}
