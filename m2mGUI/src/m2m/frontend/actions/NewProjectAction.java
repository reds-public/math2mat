/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to create a new project.
 *
 * Project:  Math2Mat
 *
 * @file NewProjectAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action creates a new project file and all the folder hierarchy, and opens the new project.
 *
 */

package m2m.frontend.actions;

import java.io.File;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import m2m.backend.project.M2MProject;
import m2m.frontend.actions.M2MAction;
import m2m.frontend.view.Editor;
import m2m.frontend.view.M2MFileStoreEditorInput;
import m2m.frontend.view.NavigationView;


public class NewProjectAction extends M2MAction {
	
	boolean empty;
	
	public NewProjectAction(IWorkbenchWindow window, String label,boolean empty) {
		super(window,label);
        this.empty=empty;
        
        if (empty) {
        	// The id is used to refer to the action in a menu or toolbar
        	setId(ICommandIds.CMD_NEW);
        	// Associate the action with a pre-defined command, to allow key bindings.
        	setActionDefinitionId(ICommandIds.CMD_NEW);
        }
        else {
        	// The id is used to refer to the action in a menu or toolbar
        	setId(ICommandIds.CMD_NEWFROMFILE);
        	// Associate the action with a pre-defined command, to allow key bindings.
        	setActionDefinitionId(ICommandIds.CMD_NEWFROMFILE);
        }
		setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/new.gif"));
	}

	/**
	 * Method executed when the action is called
	 */
	public void run() {
		
		String fileName = new String();
		
		String path;
			//open a file dialog box
			FileDialog dialog = new FileDialog(window.getShell(), SWT.SAVE);
			dialog.setText("Create new project");
			String[] filterExt = { "*.m2m"};
			dialog.setFilterExtensions(filterExt);
			path = dialog.open();
			if (path == null)
				return;
			
	        //test if the name of the file the user wants to save contains the chosen extension or not
	        //if not, add the extension to the file name
	        if ((path.lastIndexOf(".") == -1 ? "" : path.substring(path.lastIndexOf(".")+1, path.length())).equalsIgnoreCase(dialog.getFilterExtensions()[dialog.getFilterIndex()].substring(2))) {
	        	fileName = path;
	        } else {
	        	String filterExtension = dialog.getFilterExtensions()[dialog.getFilterIndex()];
	        	fileName = path+filterExtension.substring(1, filterExtension.length());
	        }

		File file = new File(fileName);
		
		String octaveFileName="";

		if (!file.exists()) {
			if (!empty) {
				//open file dialog
				FileDialog dlg = new FileDialog(this.window.getShell(), SWT.OPEN);
		        dlg.setText("Choose the Octave/Matlab file");
		        String[] filterExt1 = { "*.m"};
		        String[] filterNames = {"Source file (*.m)"};
				dlg.setFilterExtensions(filterExt1);
				dlg.setFilterNames(filterNames);
		        octaveFileName= dlg.open();
		        
		        /* Check if a path has been selected */
		        if(octaveFileName == null)
		        	return;
		        if (octaveFileName.isEmpty())
		        	return;		        
			}
			
        	createProject(file,octaveFileName);
	    }
        else {
    		MessageBox mess=new MessageBox(window.getShell(), SWT.ICON_ERROR | SWT.OK);
    		mess.setMessage("The project file already exists! Pleae choose another one.");
    		mess.setText("Error");
    		mess.open();
        }
	}
	
	private void createProject(File file,String octaveFileName) {
//		String projectPath = file.getAbsolutePath().replace("\\", "/").substring(0, file.getAbsolutePath().replace("\\", "/").lastIndexOf("/")+1);
//		File sourceFile = new File(projectPath + M2MProject.getSourceDirName()+"/" + file.getName().substring(0, file.getName().lastIndexOf(".")+1)+"m");
    	
		M2MProject project=new M2MProject();
		if (empty) {
			if (!project.CreateEmptyProject(file.getAbsolutePath())) {
				MessageBox mess=new MessageBox(window.getShell(), SWT.ICON_ERROR | SWT.OK);
				mess.setMessage("The project can not be created. It seems that there is a problem accessing the choosen directory.");
				mess.setText("Error");
				mess.open();
				return;
			}
		}
		else {
			if (!project.CreateNewProjectFromMath(file.getAbsolutePath(),octaveFileName)) {
				MessageBox mess=new MessageBox(window.getShell(), SWT.ICON_ERROR | SWT.OK);
				mess.setMessage("The project can not be created. It seems that there is a problem accessing the choosen directory.");
				mess.setText("Error");
				mess.open();
				return;
			}	
		}
	    //open a new editor for the file
		IWorkbenchPage page= PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			IFileStore fileStore= EFS.getStore(file.toURI());
			Editor editor=(Editor)page.openEditor(new M2MFileStoreEditorInput(fileStore,project), Editor.ID, true);
			NavigationView.addList(file.getAbsolutePath());
			editor.setParseDone(true);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}