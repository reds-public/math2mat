/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to launch Octave.
 *
 * Project:  Math2Mat
 *
 * @file LaunchOctaveAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action launches Octave to generate files for the testbench.
 *
 */

package m2m.frontend.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ExternalRuns;
import m2m.backend.processing.ProgressMonitoring;
import m2m.frontend.ErrorProcessing;
import m2m.frontend.GraphicalProgressMonitoring;
import m2m.frontend.actions.M2MAction;


public class LaunchOctaveWithRegeneratedFileAction extends M2MAction {
	
	public LaunchOctaveWithRegeneratedFileAction(IWorkbenchWindow window, String label) 
	{
		super(window,label);
		
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_OCTAVEREGEN);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_OCTAVEREGEN);
	}
	
	

	public void execute(IProgressMonitor monitor)
	{	
		boolean codeRegen = true;
		if (codeRegen)
			if (!editor.getM2MProject().reGenerateOctave())
				return;

		Errors.clearError();
		monitor.beginTask("Running Octave",100+editor.getM2MProject().getStructTreatment().getInput().size()+
				editor.getM2MProject().getStructTreatment().getOutput().size());
		GraphicalProgressMonitoring mon=new GraphicalProgressMonitoring();
		mon.setIProgressMonitor(monitor);
		editor.getM2MProject().monitoring=mon;

		ExternalRuns.runOctave(editor.getM2MProject(), codeRegen);
		ErrorProcessing.processLastError(editor.getShell().getDisplay());

		if (codeRegen)
			editor.getM2MProject().compareOctaveResult();
		
		synchronized(editor) {
			editor.notifyAll();
		}
		
		editor.getM2MProject().monitoring=new ProgressMonitoring();
				
	}
	/**
	 * Method executed when the action is called
	 */
	public void run() 
	{	

		if (!getEditor("Error","Select the tab with the m2m code"))
			return;
		
		if (editor.getParseDone())
		{
			activateConsole();


			IRunnableWithProgress op = new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor) {
					execute(monitor);
				}
			};
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
			Shell shell = win != null ? win.getShell() : null;
			try {
				new ProgressMonitorDialog(shell).run(true, false, op);
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		else
			MessageDialog.openError(window.getShell(), "Parsing error", "Parse the file to get inputs and outputs before trying to launch Octave");
	}
	
	
}