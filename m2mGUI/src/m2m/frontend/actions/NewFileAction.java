/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to create a new project.
 *
 * Project:  Math2Mat
 *
 * @file NewFileAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action creates a new project file as well as the source file, and opens the new project.
 *
 */

package m2m.frontend.actions;

/*
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.FileStoreEditorInput;

import m2mgui.view.Editor;
import m2mgui.view.NavigationView;

import m2mproject.ProjectProperties;
*/

import org.eclipse.ui.IWorkbenchWindow;

import m2m.frontend.actions.M2MAction;

public class NewFileAction extends M2MAction {

	public NewFileAction(IWorkbenchWindow window, String label) {
		super(window,label);
		
	}
/*	
	public NewFileAction(IWorkbenchWindow window, String label) {
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_NEW);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_NEW);
		setImageDescriptor(m2mgui.Activator.getImageDescriptor("/icons/new.gif"));
	}
*/
	/**
	 * Method executed when the action is called
	 */
	/*
	public void run() {
		//open a file dialog box
		FileDialog dialog = new FileDialog(window.getShell(), SWT.SAVE);
		dialog.setText("Create new project");
        String path = dialog.open();
        if (path == null)
            return;

        //create a new file
        File file = new File(path);
        
        //get the file extension
        String ext = (file.getName().lastIndexOf(".") == -1 ? "" :
        	file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length()));
        
        if (ext.equalsIgnoreCase("m2m")) {
        	
        	File sourceFile = new File(file.getPath().substring(0, file.getPath().lastIndexOf(".")+1)+"m");
        	
	        try {
		        FileWriter fileWriter = new FileWriter(file);
		        FileWriter sourceFileWriter = new FileWriter(sourceFile);
		        sourceFileWriter.close();
		        
		        //create the base content of the project file
		        ProjectProperties properties = new ProjectProperties();
		        properties.setSourceFilePath(sourceFile.getName());
		        PropertiesXML propertiesXML = new PropertiesXML(properties);
		        fileWriter.write(propertiesXML.toXML());
		        fileWriter.close();
		    } catch (IOException e) {
		        MessageBox messageBox = new MessageBox(window.getShell(), SWT.ICON_ERROR | SWT.OK);
		        messageBox.setMessage("File I/O Error.");
		        messageBox.setText("Error");
		        messageBox.open();
		        return;
		    }
		
		    //open a new editor for the file
			IWorkbenchPage page= PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				IFileStore fileStore= EFS.getStore(file.toURI());
				page.openEditor(new FileStoreEditorInput(fileStore), Editor.ID, true);
				NavigationView.addList(file.getAbsolutePath());
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}
	*/
}