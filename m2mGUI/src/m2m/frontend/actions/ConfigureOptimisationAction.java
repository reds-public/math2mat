/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to configure optimisation process.
 *
 * Project:  Math2Mat
 *
 * @file ConfigureOptimisationAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action opens a new window to configure optimisation properties.
 *
 */

package m2m.frontend.actions;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.ui.IWorkbenchWindow;

import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.project.M2MProject;
import m2m.backend.project.OptimisationProperties;
import m2m.frontend.actions.M2MAction;
import m2m.frontend.view.Editor;


public class ConfigureOptimisationAction extends M2MAction{
	

	public ConfigureOptimisationAction(IWorkbenchWindow window, String label) {
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CONFOPTIM);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_CONFOPTIM);
		//setImageDescriptor(m2mgui.Activator.getImageDescriptor("/icons/open.gif"));
	}
	
	/**
	 * Method executed when the action is called
	 */
	public void run() {
		

		if (!getEditor("Error","Select the tab with the code you want to transform in VHDL"))
			return;

		Display display = window.getShell().getDisplay();
		final Shell shell = new Shell(display);
		shell.setText("Simulation configuration");
		OptimisationPropertiesDlg dlg=new OptimisationPropertiesDlg(shell,editor.getM2MProject());
		dlg.setOptimisationProperties(editor.getM2MProject().getOptimisationProperties());
		dlg.open();
		if (dlg.pressedOk()) {
			editor.setModified(true);
		}
//			editor.saveProject();
		
	}
	
	
	

	class OptimisationPropertiesDlg extends Dialog {


		/**
		 * Optimisation properties.
		 */
		private OptimisationProperties optimProp;
		/**
		 * Current opened editor.
		 */
		private Editor editor;
		
		/**
		 * Combobox that contains all optimisation choices.
		private Combo optimChoice; */
		/**
		 * Combobox that contains all data size choices.
		 */
		private Combo dataTypeChoice;
		/**
		 * Text widget to enter the wish frequency.	 
		private Text freq; */
		
		/**
		 * Combobox that contains all fifo types.
		 */
		private Combo fifoChoice;
		
		/**
		 * Spinner widget to enter the Fifo size
		 */
		private Spinner fifoSize;

		/**
		 * Checkbox to select wether the Fifo size should be optimized
		 */
		private Button optFifo;

		/**
		 * Checkbox to select wether the Fifos for compensation have to be added
		 */
		private Button fifoComp;
		
		/**
		 * Warning for fifos optimization
		 */
		private Label warning;
		
		/**
		 * Project for which the optimisation properties are to be set
		 */
		private M2MProject project;
		
		/**
		 * Indicates if the user pressed on the OK button
		 */
		private boolean pressedOnOk;
		

		public OptimisationPropertiesDlg(Shell parentShell,M2MProject project) {
			super(parentShell);
			pressedOnOk=false;
			this.project=project;
		}

		protected void configureShell(Shell shell) {
			super.configureShell(shell);
		    shell.setText("Optimisation properties");
		}

		
		@Override
		public void create() {
			super.create();
		}

		@Override
		protected Control createDialogArea(Composite parent) {

			//create global GridLayout
			GridLayout globalGridLayout = new GridLayout();
			globalGridLayout.numColumns = 1;
			
			//create global Group
			Group globalGroup = new Group(parent, SWT.NONE);
			globalGroup.setLayout(globalGridLayout);
			
			//create GridLayout
			GridLayout gridLayout = new GridLayout();
			gridLayout.numColumns = 3;
			gridLayout.verticalSpacing = 5;
			
			// create main group
			Group optimGroup = new Group(globalGroup, SWT.SHADOW_ETCHED_IN);
			optimGroup.setText("Optimisation settings");
			optimGroup.setBounds(globalGroup.getClientArea());
			optimGroup.setLayout(gridLayout); 
			
			/* create label for optimisation type combo
			Label optimLabel = new Label(optimGroup, SWT.NONE);
			optimLabel.setText("Optimisation type :");
			optimLabel.setBounds(optimGroup.getClientArea());
			optimLabel.setLocation(10, 25);
			optimLabel.pack(); */
			
			/* "optimisation type" combo placement 
			GridData gridChoice = new GridData();
			gridChoice.horizontalSpan = 2; */
			
			/* "optimisation type" combobox
			optimChoice = new Combo(optimGroup, SWT.UP | SWT.TOP);
			optimChoice.add("None");
			optimChoice.add("Parallel");
			optimChoice.add("Pipeline");
			optimChoice.setLayoutData(gridChoice);
			optimChoice.pack(); */
			
			//data format label placement
			GridData gridFormat = new GridData();
			gridFormat.horizontalSpan = 2;
			
			//data format label
			Label formatLabel = new Label(optimGroup, SWT.NONE);
			formatLabel.setText("Data type :");
			formatLabel.setBounds(optimGroup.getClientArea());
//			formatLabel.setLayoutData(gridFormat);
			formatLabel.pack();
			
			//"data type" combobox
			dataTypeChoice = new Combo(optimGroup, SWT.UP | SWT.TOP);
			dataTypeChoice.add(NumType.FLOAT32.name());
			dataTypeChoice.add(NumType.FLOAT64.name());
			dataTypeChoice.setLayoutData(gridFormat);
			dataTypeChoice.pack();
	/*		
			//significant text entry placement
			GridData gridSignificant = new GridData();
			gridSignificant.widthHint = 100;
			gridSignificant.verticalSpan = 8;
			gridSignificant.verticalAlignment = SWT.BOTTOM;
			
			//significant text entry
			significant = new Text(optimGroup, SWT.RIGHT);
			significant.setLayoutData(gridSignificant);
			significant.pack();

			//exponent text entry placement
			GridData gridExponent = new GridData();
			gridExponent.widthHint = 100;
			gridExponent.verticalSpan = 8;
			gridExponent.verticalAlignment = SWT.BOTTOM;
			
			//exponent text entry
			exponent = new Text(optimGroup, SWT.RIGHT);
			exponent.setLayoutData(gridExponent);
			exponent.pack();
			
			//significant label placement
			GridData gridManticeLabel = new GridData();
			gridManticeLabel.widthHint = 208;
			gridManticeLabel.horizontalSpan = 2;
			
			//significant label
			Label significantLabel = new Label(optimGroup, SWT.RIGHT);
			significantLabel.setText("Significant");
			significantLabel.setLayoutData(gridManticeLabel);
			significantLabel.pack();
			
			//exponent label placement
			GridData gridExponentLabel = new GridData();
			gridExponentLabel.widthHint = 80;
			
			//exponent label
			Label exponentLabel = new Label(optimGroup, SWT.RIGHT);
			exponentLabel.setText("Exponent");
			exponentLabel.setLayoutData(gridExponentLabel);
			exponentLabel.pack();
	*/		
			//frequency label placement
			GridData gridFreqLabel = new GridData();
			gridFreqLabel.verticalSpan = 8;
			gridFreqLabel.verticalAlignment = SWT.BOTTOM;
			
			/* frequency label
			Label freqLabel = new Label(optimGroup, SWT.NONE);
			freqLabel.setText("Wished frequency :");
			freqLabel.setLayoutData(gridFreqLabel);
			freqLabel.pack(); */
			
			/* frequency text entry placement
			GridData gridFreq = new GridData();
			gridFreq.widthHint = 100;
			gridFreq.verticalSpan = 8;
			gridFreq.verticalAlignment = SWT.BOTTOM;
			gridFreq.horizontalAlignment = SWT.RIGHT; *7
			
			/* frequency text entry
			freq = new Text(optimGroup, SWT.RIGHT);
			freq.setLayoutData(gridFreq);
			freq.pack(); */
			
			/* frequency unit label placement
			GridData gridFreqUnit = new GridData();
			gridFreqUnit.verticalSpan = 8;
			gridFreqUnit.verticalAlignment = SWT.BOTTOM;
			gridFreqUnit.horizontalAlignment = SWT.LEFT; */
			
			/* frequency unit label
			Label freqUnit = new Label(optimGroup, SWT.LEFT);
			freqUnit.setText("MHz");
			freqUnit.setLayoutData(gridFreqUnit);
			freqUnit.pack(); */
			
			//fifo label placement
			GridData gridFifoLabel = new GridData();
			gridFifoLabel.verticalSpan = 8;
			gridFifoLabel.verticalAlignment = SWT.BOTTOM;
			
			//create label for fifo type combo
			Label fifoLabel = new Label(optimGroup, SWT.NONE);
			fifoLabel.setText("Fifo type :");
			fifoLabel.setLayoutData(gridFifoLabel);
			fifoLabel.pack();
			
			//"fifo type" combo placement
			GridData gridFifoChoice = new GridData();
			gridFifoChoice.verticalSpan = 8;
			gridFifoChoice.horizontalSpan = 2;
			gridFifoChoice.verticalAlignment = SWT.BOTTOM;
			
			//"fifo type" combobox
			fifoChoice = new Combo(optimGroup, SWT.UP | SWT.TOP);
			fifoChoice.add("Standard");
			fifoChoice.add("Altera");
			fifoChoice.add("Xilinx");
			fifoChoice.setLayoutData(gridFifoChoice);
			fifoChoice.pack();
			
			
			//Fifo size label placement
			GridData gridFifoSizeLabel = new GridData();
			gridFifoSizeLabel.verticalSpan = 8;
			gridFifoSizeLabel.verticalAlignment = SWT.BOTTOM;
			
			//Fifo size label
			Label fifoSizeLabel = new Label(optimGroup, SWT.NONE);
			fifoSizeLabel.setText("Fifo size :");
			fifoSizeLabel.setLayoutData(gridFifoSizeLabel);
			fifoSizeLabel.pack();
			
			//Fifo size text entry placement
			GridData gridFifoSize = new GridData();
			gridFifoSize.widthHint = 100;
			gridFifoSize.horizontalSpan = 2;
			gridFifoSize.verticalSpan = 8;
			gridFifoSize.verticalAlignment = SWT.BOTTOM;
			gridFifoSize.horizontalAlignment = SWT.LEFT;

			//Fifo size entry
			fifoSize = new Spinner(optimGroup, SWT.LEFT);
			fifoSize.setLayoutData(gridFifoSize);
			fifoSize.pack();
			
			
		/*	//opt Fifo label placement
			GridData gridOptFifoLabel = new GridData();
			gridOptFifoLabel.verticalSpan = 8;
			gridOptFifoLabel.verticalAlignment = SWT.BOTTOM;
			
			//opt Fifo label
			Label optFifoLabel = new Label(optimGroup, SWT.NONE);
			optFifoLabel.setText("Optimize FIFOs :");
			optFifoLabel.setLayoutData(gridOptFifoLabel);
			optFifoLabel.pack();
			*/

			//opt Fifo checkbox placement
			GridData gridOptFifo = new GridData();
			gridOptFifo.widthHint = 200;
			gridOptFifo.horizontalSpan = 3;
			gridOptFifo.verticalSpan = 8;
			gridOptFifo.verticalAlignment = SWT.BOTTOM;
			gridOptFifo.horizontalAlignment = SWT.LEFT;
			
			//Fifo size text entry
			optFifo = new Button(optimGroup, SWT.CHECK | SWT.LEFT);
			optFifo.setText("Optimize Fifos");
			optFifo.setLayoutData(gridOptFifo);
			optFifo.pack();
			optFifo.addMouseListener(new MouseListener() {	
				@Override
				public void mouseUp(MouseEvent e) {
					warning.setVisible(!optFifo.getSelection());
				}

				@Override
				public void mouseDoubleClick(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseDown(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			});

			//opt Fifo checkbox placement
			GridData gridFifoComp = new GridData();
			gridFifoComp.widthHint = 200;
			gridFifoComp.horizontalSpan = 3;
			gridFifoComp.verticalSpan = 8;
			gridFifoComp.verticalAlignment = SWT.BOTTOM;
			gridFifoComp.horizontalAlignment = SWT.LEFT;
			
			
			//Fifo size text entry
			fifoComp = new Button(optimGroup, SWT.CHECK | SWT.LEFT);
			fifoComp.setText("Add Compensation Fifos");
			fifoComp.setLayoutData(gridFifoComp);
			fifoComp.pack();
			

			//warning label placement
			GridData gridWarningLabel = new GridData();
			gridWarningLabel.widthHint = 280;
			gridWarningLabel.horizontalSpan = 3;
			gridWarningLabel.verticalSpan = 8;
			gridWarningLabel.verticalAlignment = SWT.BOTTOM;
			gridWarningLabel.horizontalAlignment = SWT.LEFT;
			
			//warning label
			warning = new Label(optimGroup, SWT.CHECK | SWT.LEFT);
			warning.setText("Warning: make sure that the fifos value is sufficient.");
			warning.setForeground(ColorConstants.red);
			warning.setLayoutData(gridWarningLabel);
			warning.setVisible(!optFifo.getSelection());
			warning.pack();
			
			this.refreshOptimisationPage();
	    		
			return parent;
		}



		protected void okPressed() {
			saveOptimisationProperties();
			pressedOnOk=true;
			this.close();
		}
		
		public boolean pressedOk() {
			return pressedOnOk;
		}
		/**
		 * Save optimisation properties into the project file.
		 */
		private void saveOptimisationProperties() {
			/* if (optimChoice.getSelectionIndex() != -1) {
				optimProp.setOptimisationType(optimChoice.getItem(optimChoice.getSelectionIndex()));
			} */
			if (dataTypeChoice.getSelectionIndex() != -1) {
				try {
					NumType newType=NumType.getNum(dataTypeChoice.getItem(dataTypeChoice.getSelectionIndex()));
					if (optimProp.getOptimisationDataType()!=newType) {
						optimProp.setOptimisationDataType(newType);
						project.getStructTreatment().modifyNumType(newType);
					}
				}
				catch (NumberFormatException e) {
					MessageDialog.openError(editor.getSite().getShell(), "Error", "Data size should be an integer!");
				}
			}
			/* if (freq.getText() != null) {
				try {
					optimProp.setOptimisationFrequency(Integer.parseInt(freq.getText()));}
				catch (NumberFormatException e) {
					MessageDialog.openError(editor.getSite().getShell(), "Error", "Frequency should be an integer!");
				}
			} */
			if (fifoChoice.getSelectionIndex() != -1) {
				optimProp.setFifoType(fifoChoice.getItem(fifoChoice.getSelectionIndex()));
			}
			optimProp.setFifoSize(fifoSize.getSelection());
			optimProp.setOptimizeFifo(optFifo.getSelection());
			optimProp.setFifoCompensation(fifoComp.getSelection());
		}

		/**
		 * Set optimisation properties.
		 * 
		 * @param optimProp Optimisation properties.
		 */
		public void setOptimisationProperties(OptimisationProperties optimProp) {
			this.optimProp = optimProp;
		}

		/**
		 * Refresh the page.
		 */
		public void refreshOptimisationPage() {
			if (optimProp instanceof OptimisationProperties) {
				//optimChoice.setText(optimProp.getOptimisationtype());
				dataTypeChoice.setText(String.valueOf(optimProp.getOptimisationDataType().name()));
				//freq.setText(String.valueOf(optimProp.getOptimisationfrequency()));
				fifoChoice.setText(optimProp.getFifoType());
				fifoSize.setValues(optimProp.getFifoSize(),0,1000,0,1,1);
				optFifo.setSelection(optimProp.getOptimizeFifo());
				warning.setVisible(!optFifo.getSelection());
				fifoComp.setSelection(optimProp.getFifoCompensation());
			}
		}
	}
	
}
