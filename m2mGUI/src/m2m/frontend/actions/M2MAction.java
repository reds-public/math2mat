/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file M2MAction.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Nov 23, 2010
 *
 * Author: Yann Thoma
 *
 * Description: Generic Action that allows subclasses to be rapidly defined
 * 
 */
package m2m.frontend.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import m2m.frontend.view.Editor;
import m2m.frontend.view.M2MConsole;

/**
 * @author ythoma
 *
 */
public abstract class M2MAction extends Action {
	

	protected M2MAction(IWorkbenchWindow window, String label) {
		setEnabled(true);
		this.window = window;
		setText(label);
	}
	
	protected Editor editor;

	protected IWorkbenchWindow window;
	
	protected boolean getEditor(String messTitle,String errMess) {
		editor = null;
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		//get the current editor
		if (editorPart instanceof Editor) {
			editor = (Editor)editorPart;
		} else {
			MessageDialog.openError(window.getShell(), messTitle, errMess);
			return false;
		}
		return true;
	}
	
	protected void activateConsole() {		
		//activate the console
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			page.showView(M2MConsole.ID);
		} catch (PartInitException e1) {
			e1.printStackTrace();
		}
	}
}
