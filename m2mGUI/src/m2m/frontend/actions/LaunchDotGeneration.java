/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to generate the dot file.
 *
 * Project:  Math2Mat
 *
 * @file LaunchDotGeneration.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 12.10.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action generates the dot file.
 *
 */

package m2m.frontend.actions;

import org.eclipse.ui.IWorkbenchWindow;

import m2m.frontend.actions.M2MAction;

public class LaunchDotGeneration extends M2MAction {
	

	public LaunchDotGeneration(IWorkbenchWindow window, String label) 
	{
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_DOTGEN);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_DOTGEN);
	}

	/**
	 * Method executed when the action is called
	 */
	public void run() 
	{

		if (!getEditor("Error","There is no opened file to parse"))
			return;
		activateConsole();
		
		editor.getM2MProject().launchDotGeneration();
	}
}
