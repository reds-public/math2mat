/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Defines command IDs.
 *
 * Project:  Math2Mat
 *
 * @file ICommandIds.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Interface defining the application's command IDs.
 * Key bindings can be defined for specific commands.
 * To associate an action with a command, use IAction.setActionDefinitionId(commandId).
 *
 * @see org.eclipse.jface.action.IAction#setActionDefinitionId(String)
 *
 */

package m2m.frontend.actions;

public interface ICommandIds {

    public static final String CMD_OPEN                 = "m2mgui.open";
    public static final String CMD_REOPEN               = "m2mgui.reopen";
    public static final String CMD_SAVE                 = "m2mgui.save";
    public static final String CMD_SAVEAS               = "m2mgui.saveas";
    public static final String CMD_NEW                  = "m2mgui.new";
    public static final String CMD_NEWFROMFILE          = "m2mgui.newfromfile";
    public static final String CMD_CUT                  = "m2mgui.cut";
    public static final String CMD_COPY                 = "m2mgui.copy";
    public static final String CMD_PASTE                = "m2mgui.paste";
    public static final String CMD_DELETE               = "m2mgui.delete";
    public static final String CMD_SELECTALL            = "m2mgui.selectall";
    public static final String CMD_UNDO                 = "m2mgui.undo";
    public static final String CMD_REDO                 = "m2mgui.redo";
    public static final String CMD_OCTAVE               = "m2mgui.octave";
    public static final String CMD_OPTIM                = "m2mgui.optimisation";
    public static final String CMD_VERIF                = "m2mgui.verification";
    public static final String CMD_VERIFUBIDULE         = "m2mgui.verificationubidule";
    public static final String CMD_XILINXUBIDULE        = "m2mgui.xilinxubidule";
    public static final String CMD_RUNALLUBIDULE        = "m2mgui.runallubidule";
    public static final String CMD_CONFOCTAVE           = "m2mgui.confoctave";
    public static final String CMD_CONFOPTIM            = "m2mgui.confoptimisation";
    public static final String CMD_CONFSIM              = "m2mgui.confsimulation";
    public static final String CMD_DOTGEN               = "m2mgui.dotgeneration";
    public static final String CMD_DOTTREEVIEW          = "m2mgui.dottreeview";
    public static final String CMD_DOTFLATVIEW          = "m2mgui.dotflatview";
    public static final String CMD_VHDLGEN              = "m2mgui.vhdlgeneration";
    public static final String CMD_CLEAR                = "m2mgui.clear";
    public static final String CMD_CLEARCONSOLE         = "m2mgui.clearconsole";
    public static final String CMD_RUNALL               = "m2mgui.clear";
    public static final String CMD_COMMENTUNCOMMENT     = "m2mgui.commentuncomment";
    public static final String CMD_COMMENT              = "m2mgui.comment";
    public static final String CMD_UNCOMMENT            = "m2mgui.uncomment";
    public static final String CMD_GENERATECODE         = "m2mgui.generatemath2matcode";
    public static final String CMD_COMPAREOCTAVERESULTS = "m2mgui.compareoctaveresults";
    public static final String CMD_OCTAVEREGEN          = "m2mgui.launchoctavewithregeneratedfile";
    public static final String CMD_DYNAMICVIEW 			= "m2mgui.dynamicstructureview";
    public static final String CMD_VALIDATEPROJECTS     = "m2mgui.validateprojects";

}
