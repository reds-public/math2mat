/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file LaunchXilinxUbiduleAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Dec 9, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.frontend.actions;

import java.lang.reflect.InvocationTargetException;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ExternalRuns;
import m2m.backend.processing.ProgressMonitoring;
import m2m.frontend.ErrorProcessing;
import m2m.frontend.GraphicalProgressMonitoring;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * @author ythoma
 *
 */
public class LaunchXilinxUbiduleAction extends M2MAction {


	/**
	 * Constructor of the LaunchUbiduleVerification action.
	 * @param window the IWorkbenchWindow of the application
	 * @param label the label to set to the function
	 */
	public LaunchXilinxUbiduleAction(IWorkbenchWindow window, String label) 
	{
		super(window,label);
		setEnabled(true);
		this.window = window;
		setText(label);
	//	setImageDescriptor(m2m.frontend.Activator.getImageDescriptor("/icons/verif_ubidule.png"));
		
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_XILINXUBIDULE);
		
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_XILINXUBIDULE);
	}


	public void execute(IProgressMonitor monitor)
	{

		Errors.clearError();
		monitor.beginTask("Running all",100+editor.getM2MProject().getSimulationProperties().getNumberOfSamples());
		GraphicalProgressMonitoring mon=new GraphicalProgressMonitoring();
		mon.setIProgressMonitor(monitor);
		editor.getM2MProject().monitoring=mon;
		ExternalRuns.runXilinxUbidule(editor.getM2MProject());
		ErrorProcessing.processLastError(editor.getShell().getDisplay());
		
		editor.getM2MProject().monitoring=new ProgressMonitoring();
				
	}
	
	/**
	 * Method executed when the action is called
	 */
	public synchronized void run() 
	{	
		if (!getEditor("Error","Select the tab with the code you want to transform in VHDL"))
			return;
		

		boolean vhdlOK=editor.getM2MProject().isVHDLUpToDate();
		if (!vhdlOK) {
			MessageDialog.openError(window.getShell(), "Error", "Please generate VHDL files before launching the verification");
			return;
		}
		
	    activateConsole();

		activateConsole();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) {
				execute(monitor);
			}
		};
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		Shell shell = win != null ? win.getShell() : null;
		try {
			new ProgressMonitorDialog(shell).run(true, false, op);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
