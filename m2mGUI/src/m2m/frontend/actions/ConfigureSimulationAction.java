/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Action to configure simulation process.
 *
 * Project:  Math2Mat
 *
 * @file ConfigureSimulationAction.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This action opens a new window to configure simulation properties.
 *
 */

package m2m.frontend.actions;

import java.util.Vector;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IWorkbenchWindow;

import m2m.backend.project.SimulationProperties;
import m2m.backend.structure.Element;
import m2m.frontend.M2MToolTips;
import m2m.frontend.actions.M2MAction;


public class ConfigureSimulationAction extends M2MAction {

	public ConfigureSimulationAction(IWorkbenchWindow window, String label) {
		super(window,label);
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CONFSIM);
        // Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_CONFSIM);
		//setImageDescriptor(m2mgui.Activator.getImageDescriptor("/icons/open.gif"));
	}
	
	/**
	 * Method executed when the action is called
	 */
	public void run() {

		if (!getEditor("Error","Select the tab with the code you want to transform in VHDL"))
			return;

		Display display = window.getShell().getDisplay();
		final Shell shell = new Shell(display);
		shell.setText("Simulation configuration");
		SimulationPropertiesDlg dlg=new SimulationPropertiesDlg(shell);
		dlg.setSimulationProperties(editor.getM2MProject().getSimulationProperties());
		dlg.open();
		if (dlg.pressedOk()) {
			editor.setModified(true);
		}
//			editor.saveProject();
	}

	class SimulationPropertiesDlg extends Dialog {

		
		/**
		 * Simulation properties.
		 */
		private SimulationProperties simProp;
		/**
		 * Text widget to enter the calculation precision.
		 */
		private Text calcPres;
		/**
		 * Text widget to enter the number of samples to generate.
		 */
		private Text nbSamplesEntry;
		/**
		 * Text widget to enter the system frequency.
		 */
		private Text systemFreqEntry;
		/**
		 * Text widget to enter the input frequency.
		 */
		private Text inputFreqEntry;
		/**
		 * Text widget to enter the output frequency.
		 */
		private Text outputFreqEntry;
		/**
		 * Text widget to enter the inactivity timeout.
		 */
		private Text inactivityTimeoutEntry;
		/**
		 * Vector of Text widget to enter the minimum input value.
		 */
		private Vector<Text> minValEntry = new Vector<Text>();
		/**
		 * Vector of Text widget to enter the maximum input value.
		 */
		private Vector<Text> maxValEntry = new Vector<Text>();
		/**
		 * Indicates if the user pressed on the OK button
		 */
		private boolean pressedOnOk;
		

		public SimulationPropertiesDlg(Shell parentShell) {
			super(parentShell);
			pressedOnOk=false;
		}

		protected void configureShell(Shell shell) {
			super.configureShell(shell);
		    shell.setText("Simulation properties");
		}

		
		@Override
		public void create() {
			super.create();
		}

		@Override
		protected Control createDialogArea(Composite parent) {

			//create global GridLayout
			GridLayout globalGridLayout = new GridLayout();
			globalGridLayout.numColumns = 1;
		
			//create global Group
			Group globalGroup = new Group(parent, SWT.NONE);
			globalGroup.setLayout(globalGridLayout);
			
			//create main GridLayout
			GridLayout gridLayout = new GridLayout();
			gridLayout.numColumns = 3;
			gridLayout.verticalSpacing = 5;
			
			//create date format Group
			Group formatGroup = new Group(globalGroup, SWT.SHADOW_ETCHED_IN);
			formatGroup.setText("Data format");
			formatGroup.setBounds(globalGroup.getClientArea());
			formatGroup.setLayout(gridLayout);

			
			//calculation precision label placement
			GridData gridCalc = new GridData();
			gridCalc.verticalSpan = 8;
			gridCalc.verticalAlignment = SWT.BOTTOM;
			
			//calculation precision label
			Label calcLabel = new Label(formatGroup, SWT.NONE);
			calcLabel.setText("Number bit precision :");
			calcLabel.setLayoutData(gridCalc);
			calcLabel.pack();
			
			//calculation precision text entry placement
			GridData gridCalcPres = new GridData();
			gridCalcPres.widthHint = 100;
			gridCalcPres.verticalSpan = 8;
			gridCalcPres.verticalAlignment = SWT.BOTTOM;
			
			//text precision text entry
			calcPres = new Text(formatGroup, SWT.NONE);
			calcPres.setLayoutData(gridCalcPres);
			calcPres.pack();

			calcLabel.setToolTipText(M2MToolTips.SIMPROPERTIES_CALCPRECISION);
			calcPres.setToolTipText(M2MToolTips.SIMPROPERTIES_CALCPRECISION);

			//rating and sampling group
			Group freqGroup = new Group(globalGroup, SWT.SHADOW_ETCHED_IN);
			freqGroup.setText("Rating and sampling");
			freqGroup.setLocation(5, 200);
			freqGroup.setBounds(globalGroup.getClientArea());

			//create main GridLayout
			GridLayout freqGridLayout = new GridLayout();
			freqGridLayout.numColumns = 3;
			freqGridLayout.verticalSpacing = 5;
			
			freqGroup.setLayout(freqGridLayout);
			
	/*
			//Stress factor placement
			GridData gridStressFactor = new GridData();
			gridStressFactor.verticalSpan = 8;
			gridStressFactor.verticalAlignment = SWT.BOTTOM;
			
			//Stress factor label
			Label stressFactorLabel = new Label(freqGroup, SWT.NONE);
			stressFactorLabel.setText("Stress factor :");
			stressFactorLabel.setLayoutData(gridStressFactor);
			stressFactorLabel.pack();
			
			//Stress factor text entry placement
			GridData gridstressFactor = new GridData();
			gridstressFactor.widthHint = 100;
		//	gridstressFactor.horizontalSpan = 2;
			gridstressFactor.verticalSpan = 8;
			gridstressFactor.verticalAlignment = SWT.BOTTOM;

			//Stress factor entry
			stressFactor = new Spinner(freqGroup, SWT.NONE);
			stressFactor.setLayoutData(gridStressFactor);
		
//			stressFactor.pack();

			Label emptyLabel = new Label(freqGroup, SWT.NONE);
			emptyLabel.setText("");
			emptyLabel.setLayoutData(gridStressFactor);
			emptyLabel.pack();*/
			
			
			//number of samples label placement
			GridData gridNumberOfSample = new GridData();
			gridNumberOfSample.verticalSpan = 8;
			gridNumberOfSample.verticalAlignment = SWT.BOTTOM;
			
			//number of samples label
			Label nbSamplesLabel = new Label(freqGroup, SWT.NONE);
			nbSamplesLabel.setLayoutData(gridNumberOfSample);
			nbSamplesLabel.setText("Number of samples :");
			nbSamplesLabel.pack();
			
			//number of samples text entry placement
			GridData gridNbSamplesEntry = new GridData();
			gridNbSamplesEntry.widthHint = 100;
			gridNbSamplesEntry.verticalSpan = 8;
			gridNbSamplesEntry.horizontalSpan = 2;
			gridNbSamplesEntry.verticalAlignment = SWT.BOTTOM;

			//number of samples text entry
			nbSamplesEntry = new Text(freqGroup, SWT.RIGHT);
			nbSamplesEntry.setLayoutData(gridNbSamplesEntry);
			nbSamplesEntry.pack();

			nbSamplesLabel.setToolTipText(M2MToolTips.SIMPROPERTIES_NBSAMPLES);
			nbSamplesEntry.setToolTipText(M2MToolTips.SIMPROPERTIES_NBSAMPLES);
						
			
			//system frequency label placement
			GridData gridSystFreq = new GridData();
			gridSystFreq.verticalSpan = 8;
			gridSystFreq.verticalAlignment = SWT.BOTTOM;
			
			//system frequency label
			Label systFreq = new Label(freqGroup, SWT.NONE);
			systFreq.setLayoutData(gridSystFreq);
			systFreq.setText("System frequency :");
			systFreq.pack();

			//stimuli read frequency text entry placement
			GridData gridSystFreqEntry = new GridData();
			gridSystFreqEntry.widthHint = 100;
			gridSystFreqEntry.verticalSpan = 8;
			gridSystFreqEntry.verticalAlignment = SWT.BOTTOM;
			
			//stimuli read frequency text entry
			systemFreqEntry = new Text(freqGroup, SWT.RIGHT);
			systemFreqEntry.setLayoutData(gridSystFreqEntry);
			systemFreqEntry.pack();

			//stimuli read frequency unit label placement
			GridData gridSystFreqUnit = new GridData();
			gridSystFreqUnit.verticalSpan = 8;
			gridSystFreqUnit.verticalAlignment = SWT.BOTTOM;
			
			//stimuli read frequency unit label
			Label systFreqUnit = new Label(freqGroup, SWT.NONE);
			systFreqUnit.setLayoutData(gridSystFreqUnit);
			systFreqUnit.setText("MHz");
			systFreqUnit.pack();

			systFreq.setToolTipText(M2MToolTips.SIMPROPERTIES_SYSFREQ);
			systemFreqEntry.setToolTipText(M2MToolTips.SIMPROPERTIES_SYSFREQ);
			

			//stimuli read frequency label placement
			GridData gridReadStimFreqSample = new GridData();
			gridReadStimFreqSample.verticalSpan = 8;
			gridReadStimFreqSample.verticalAlignment = SWT.BOTTOM;
			
			//stimuli read frequency label
			Label readFreq = new Label(freqGroup, SWT.NONE);
			readFreq.setLayoutData(gridReadStimFreqSample);
			readFreq.setText("Input frequency :");
			readFreq.pack();

			//stimuli read frequency text entry placement
			GridData gridReadStimFreqEntry = new GridData();
			gridReadStimFreqEntry.widthHint = 100;
			gridReadStimFreqEntry.verticalSpan = 8;
			gridReadStimFreqEntry.verticalAlignment = SWT.BOTTOM;
			
			//stimuli read frequency text entry
			inputFreqEntry = new Text(freqGroup, SWT.RIGHT);
			inputFreqEntry.setLayoutData(gridReadStimFreqEntry);
			inputFreqEntry.pack();

			//stimuli read frequency unit label placement
			GridData gridReadStimFreqUnit = new GridData();
			gridReadStimFreqUnit.verticalSpan = 8;
			gridReadStimFreqUnit.verticalAlignment = SWT.BOTTOM;
			
			//stimuli read frequency unit label
			Label readFreqUnit = new Label(freqGroup, SWT.NONE);
			readFreqUnit.setLayoutData(gridReadStimFreqUnit);
			readFreqUnit.setText("MHz");
			readFreqUnit.pack();
			
			readFreq.setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTFREQ);
			inputFreqEntry.setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTFREQ);
			
			//reference sample frequency label placement
			GridData gridRefSample = new GridData();
			gridRefSample.verticalSpan = 8;
			gridRefSample.verticalAlignment = SWT.BOTTOM;
			
			//reference sample frequency label
			Label refSampleFreq = new Label(freqGroup, SWT.NONE);
			refSampleFreq.setText("Output frequency :");
			refSampleFreq.setLayoutData(gridRefSample);
			refSampleFreq.pack();
			
			//reference sample frequency text entry placement
			GridData gridRefSampleEntry = new GridData();
			gridRefSampleEntry.widthHint = 100;
			gridRefSampleEntry.verticalSpan = 8;
			gridRefSampleEntry.verticalAlignment = SWT.BOTTOM;
			
			//reference sample frequency text entry
			outputFreqEntry = new Text(freqGroup, SWT.RIGHT);
			outputFreqEntry.setLayoutData(gridRefSampleEntry);
			outputFreqEntry.pack();

			//reference sample frequency unit label placement
			GridData gridRefSampleUnit = new GridData();
			gridRefSampleUnit.verticalSpan = 8;
			gridRefSampleUnit.verticalAlignment = SWT.BOTTOM;
			
			//reference sample frequency unit label
			Label refSampleUnit = new Label(freqGroup, SWT.NONE);
			refSampleUnit.setText("MHz");
			refSampleUnit.setLayoutData(gridRefSampleUnit);
			refSampleUnit.pack();
			
			refSampleFreq.setToolTipText(M2MToolTips.SIMPROPERTIES_OUTPUTFREQ);
			outputFreqEntry.setToolTipText(M2MToolTips.SIMPROPERTIES_OUTPUTFREQ);
			
			//inactivity timeout label placement
			GridData gridInactivityTimeout = new GridData();
			gridInactivityTimeout.verticalSpan = 8;
			gridInactivityTimeout.verticalAlignment = SWT.BOTTOM;
			
			//inactivity timeout label
			Label inactivityTimeoutFreq = new Label(freqGroup, SWT.NONE);
			inactivityTimeoutFreq.setText("Inactivity timeout :");
			inactivityTimeoutFreq.setLayoutData(gridInactivityTimeout);
			inactivityTimeoutFreq.pack();
			
			//inactivity timeout text entry placement
			GridData gridInactivityTimeoutEntry = new GridData();
			gridInactivityTimeoutEntry.widthHint = 100;
			gridInactivityTimeoutEntry.verticalSpan = 8;
			gridInactivityTimeoutEntry.verticalAlignment = SWT.BOTTOM;
			
			//inactivity timeout text entry
			inactivityTimeoutEntry = new Text(freqGroup, SWT.RIGHT);
			inactivityTimeoutEntry.setLayoutData(gridInactivityTimeoutEntry);
			inactivityTimeoutEntry.pack();
			
			//inactivity timeout unit label placement
			GridData gridInactivityTimeoutUnit = new GridData();
			gridInactivityTimeoutUnit.verticalSpan = 8;
			gridInactivityTimeoutUnit.verticalAlignment = SWT.BOTTOM;
			
			//inactivity timeout unit label
			Label inactivityTimeoutUnit = new Label(freqGroup, SWT.NONE);
			inactivityTimeoutUnit.setText("Clock cycles");
			inactivityTimeoutUnit.setLayoutData(gridInactivityTimeoutUnit);
			inactivityTimeoutUnit.pack();

			inactivityTimeoutFreq.setToolTipText(M2MToolTips.SIMPROPERTIES_INACTIVITY);
			inactivityTimeoutEntry.setToolTipText(M2MToolTips.SIMPROPERTIES_INACTIVITY);
			
			
			GridLayout inputGridLayout = new GridLayout();
			inputGridLayout.numColumns = 3;
			inputGridLayout.verticalSpacing = 5;
			
			/*
			//create input Group
			Group inputGroup = new Group(globalGroup, SWT.V_SCROLL);
			inputGroup.setText("Inputs range value");
			inputGroup.setBounds(globalGroup.getClientArea());
			inputGroup.setLayout(inputGridLayout);	
			
			
			Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();
			rangeValTable = new Table(inputGroup, SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
			rangeValTable.setHeaderVisible(true);  
			rangeValTable.setLinesVisible(true);		
		    
			String[] titles = {"Input", "Min", "Max"};
			for (int i=0; i<titles.length; i++) {
				TableColumn column = new TableColumn (rangeValTable, SWT.NONE);
				column.setText (titles [i]);
				column.pack();
			}
			
			for (Element var : inputs) {
				TableItem item = new TableItem (rangeValTable, SWT.NONE);
				item.setText(0, var.getName());
			}
			
		    final TableEditor editor = new TableEditor(rangeValTable);
		    editor.horizontalAlignment = SWT.LEFT;
		    editor.grabHorizontal = true;
		    editor.minimumWidth = 50;
		     
		    rangeValTable.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                        // Clean up any previous editor control
                        Control oldEditor = editor.getEditor();
                        if (oldEditor != null) 
                        	oldEditor.dispose();
        
                        // Identify the selected row
                        TableItem item = (TableItem)e.item;
                        if (item == null) 
                        	return;
        
                        // The control that will be the editor must be a child of the Table
                        Text newEditorMin = new Text(rangeValTable, SWT.NONE);
                        newEditorMin.setText(item.getText(1));
                        newEditorMin.addModifyListener(new ModifyListener() {
                                public void modifyText(ModifyEvent e) {
                                        Text text = (Text)editor.getEditor();
                                        editor.getItem().setText(1, text.getText());
                                }
                        });
                        newEditorMin.selectAll();
                        newEditorMin.setFocus();
                        editor.setEditor(newEditorMin, item, 1);
                        
                        // The control that will be the editor must be a child of the Table
                        Text newEditorMax = new Text(rangeValTable, SWT.NONE);
                        newEditorMax.setText(item.getText(2));
                        newEditorMax.addModifyListener(new ModifyListener() {
                                public void modifyText(ModifyEvent e) {
                                        Text text = (Text)editor.getEditor();
                                        editor.getItem().setText(2, text.getText());
                                }
                        });
                        newEditorMax.selectAll();
                        newEditorMax.setFocus();
                        editor.setEditor(newEditorMax, item, 2);
                }
		    });
			*/
						
			Group inputGroup = new Group(globalGroup, SWT.SHADOW_ETCHED_IN);
			inputGroup.setText("Input range value");
			inputGroup.setBounds(globalGroup.getClientArea());
			inputGroup.setLayout(inputGridLayout);	
				
			//reference input label placement
			GridData gridRefInput = new GridData();
			gridRefInput.verticalSpan = 3;
			gridRefInput.verticalAlignment = SWT.BOTTOM;
			
			//reference input label
			Label refInput = new Label(inputGroup, SWT.NONE);
			refInput.setText("Input");
			refInput.setLayoutData(gridRefInput);
			refInput.pack();
			
			//reference minimum value text entry placement
			GridData gridRefMin = new GridData();
			gridRefMin.verticalSpan = 3;
			gridRefMin.verticalAlignment = SWT.BOTTOM;
					
			//reference minimum label
			Label refMin = new Label(inputGroup, SWT.NONE);
			refMin.setText("Minimum");
			refMin.setLayoutData(gridRefInput);
			refMin.pack();
			
			//reference minimum value text entry placement
			GridData gridRefMax = new GridData();
			gridRefMax.verticalSpan = 3;
			gridRefMax.verticalAlignment = SWT.BOTTOM;
					
			//reference minimum label
			Label refMax = new Label(inputGroup, SWT.NONE);
			refMax.setText("Maximum");
			refMax.setLayoutData(gridRefInput);
			refMax.pack();
			
			minValEntry.clear();
			maxValEntry.clear();
			Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();		
			for(Element var : inputs)
			{
				//reference input name label placement
				GridData gridRefName = new GridData();
				gridRefName.verticalSpan = 3;
				gridRefName.verticalAlignment = SWT.BOTTOM;
				
				//reference input name label
				Label refName= new Label(inputGroup, SWT.NONE);
				refName.setText(var.getName());
				refName.setLayoutData(gridRefName);
				refName.pack();
				
				//reference minimum value text entry placement
				GridData gridRefMinValEntry = new GridData();
				gridRefMinValEntry.widthHint = 50;
				gridRefMinValEntry.verticalSpan = 3;
				gridRefMinValEntry.verticalAlignment = SWT.BOTTOM;
						
				//minimum text entry
				minValEntry.add(new Text(inputGroup, SWT.RIGHT));
				minValEntry.lastElement().setLayoutData(gridRefMinValEntry);
				minValEntry.lastElement().pack();
				
				//reference maximum value text entry placement
				GridData gridRefMaxValEntry = new GridData();
				gridRefMaxValEntry.widthHint = 50;
				gridRefMaxValEntry.verticalSpan = 3;
				gridRefMaxValEntry.verticalAlignment = SWT.BOTTOM;
						
				//maximum text entry
				maxValEntry.add(new Text(inputGroup, SWT.RIGHT));
				maxValEntry.lastElement().setLayoutData(gridRefMaxValEntry);
				maxValEntry.lastElement().pack();
			}
			
			/*
			
			minValEntry.clear();
			maxValEntry.clear();
			Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();
			
			for(Element var : inputs)
			{
				//create input Group
				Group inputGroup = new Group(globalGroup, SWT.SHADOW_ETCHED_IN);
				inputGroup.setText(var.getName() +  " range value");
				inputGroup.setBounds(globalGroup.getClientArea());
				inputGroup.setLayout(inputGridLayout);	
				
				//reference minimum value label placement
				GridData gridRefMinVal = new GridData();
				gridRefMinVal.verticalSpan = 5;
				gridRefMinVal.verticalAlignment = SWT.BOTTOM;
				
				//reference minimum value  label
				Label refMinVal= new Label(inputGroup, SWT.NONE);
				refMinVal.setText("Minimum value :");
				refMinVal.setLayoutData(gridRefMinVal);
				refMinVal.pack();
				
				//reference minimum value text entry placement
				GridData gridRefMinValEntry = new GridData();
				gridRefMinValEntry.widthHint = 100;
				gridRefMinValEntry.verticalSpan = 5;
				gridRefMinValEntry.verticalAlignment = SWT.BOTTOM;
						
				//minimum text entry
				minValEntry.add(new Text(inputGroup, SWT.RIGHT));
				minValEntry.lastElement().setLayoutData(gridRefMinValEntry);
				minValEntry.lastElement().pack();
				
				//maximum value label placement
				GridData gridMinValLabel = new GridData();
				gridMinValLabel.verticalSpan = 5;
				gridMinValLabel.verticalAlignment = SWT.BOTTOM;
				
				//maximum value label
				Label minValLabel  = new Label(inputGroup, SWT.NONE);
				minValLabel.setText("Integer");
				minValLabel.setLayoutData(gridMinValLabel);
				minValLabel.pack();
				
				refMinVal.setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTMINIMUM);
				minValEntry.lastElement().setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTMINIMUM);		
				
				//reference maximum value label placement
				GridData gridRefMaxVal = new GridData();
				gridRefMaxVal.verticalSpan = 5;
				gridRefMaxVal.verticalAlignment = SWT.BOTTOM;
				
				//reference maximum value label
				Label refMaxVal= new Label(inputGroup, SWT.NONE);
				refMaxVal.setText("Maximum value :");
				refMaxVal.setLayoutData(gridRefMaxVal);
				refMaxVal.pack();
				
				//reference maximum value text entry placement
				GridData gridRefMaxValEntry = new GridData();
				gridRefMaxValEntry.widthHint = 100;
				gridRefMaxValEntry.verticalSpan = 5;
				gridRefMaxValEntry.verticalAlignment = SWT.BOTTOM;
						
				//maximum text entry
				maxValEntry.add(new Text(inputGroup, SWT.RIGHT));
				maxValEntry.lastElement().setLayoutData(gridRefMaxValEntry);
				maxValEntry.lastElement().pack();		
				
				//maximum value label placement
				GridData gridMaxValLabel = new GridData();
				gridMaxValLabel.verticalSpan = 5;
				gridMaxValLabel.verticalAlignment = SWT.BOTTOM;
				
				//maximum value label
				Label maxValLabel  = new Label(inputGroup, SWT.NONE);
				maxValLabel.setText("Integer");
				maxValLabel.setLayoutData(gridMaxValLabel);
				maxValLabel.pack();
				
				refMaxVal.setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTMAXIMUM);
				maxValEntry.lastElement().setToolTipText(M2MToolTips.SIMPROPERTIES_INPUTMAXIMUM);	
			}	
			
			*/
			
			this.refreshSimulationPage();
	    		
			return parent;
		}


		private boolean validate() {
			try {
				if (calcPres.getText() != null) {
					simProp.setSimulationCalcPrecision(Integer.parseInt(calcPres.getText()));
				}
				if (nbSamplesEntry.getText() != null) {
					simProp.setNumberOfSamples(Integer.parseInt(nbSamplesEntry.getText()));
				}
				if (inputFreqEntry.getText() != null) {
					simProp.setSystemFrequency(Float.parseFloat(systemFreqEntry.getText()));
				}
				if (inputFreqEntry.getText() != null) {
					simProp.setInputFrequency(Float.parseFloat(inputFreqEntry.getText()));
				}
				if (outputFreqEntry.getText() != null) {
					simProp.setOutputFrequency(Float.parseFloat(outputFreqEntry.getText()));
				}
				if (outputFreqEntry.getText() != null) {
					simProp.setInactivityTimeout(Integer.parseInt(inactivityTimeoutEntry.getText()));
				}
				
				Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();
				
				/* Set minimum value parameter for each input */
				for(Text txt : minValEntry) {
					if (txt.getText() != null) {
						int minValue = Integer.parseInt(txt.getText());
						String name = inputs.elementAt(minValEntry.indexOf(txt)).getName();
						simProp.setInputSimParamMin(name, minValue);
					}
				}
				
				/* Set maximum value parameter for each input */
				for(Text txt : maxValEntry) {
					if (txt.getText() != null) {
						int maxValue = Integer.parseInt(txt.getText());
						String name = inputs.elementAt(maxValEntry.indexOf(txt)).getName();
						simProp.setInputSimParamMax(name, maxValue);
					}
				}
				
				return true;
			}
			catch (NumberFormatException e){

				MessageBox mess=new MessageBox(this.getShell(), SWT.ICON_ERROR | SWT.OK);
				mess.setMessage("Error in the one of the input field");
				mess.setText("Error");
				mess.open();
				return false;
			}
		}

		protected void okPressed() {
			saveSimulationProperties();
			pressedOnOk=true;
			this.close();
		}
		
		public boolean pressedOk() {
			return pressedOnOk;
		}
		
		/**
		 * Save simulation properties into the project file.
		 */
		private void saveSimulationProperties() {
			if (!validate())
				return;
			
			if (calcPres.getText() != null) {
				simProp.setSimulationCalcPrecision(Integer.parseInt(calcPres.getText()));
			}
			if (nbSamplesEntry.getText() != null) {
				simProp.setNumberOfSamples(Integer.parseInt(nbSamplesEntry.getText()));
			}
			if (inputFreqEntry.getText() != null) {
				simProp.setSystemFrequency(Float.parseFloat(systemFreqEntry.getText()));
			}
			if (inputFreqEntry.getText() != null) {
				simProp.setInputFrequency(Float.parseFloat(inputFreqEntry.getText()));
			}
			if (outputFreqEntry.getText() != null) {
				simProp.setOutputFrequency(Float.parseFloat(outputFreqEntry.getText()));
			}
			if (inactivityTimeoutEntry.getText() != null) {
				simProp.setInactivityTimeout(Integer.parseInt(inactivityTimeoutEntry.getText()));
			}
			
			Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();
			
			/* Set minimum value parameter for each input */
			for(Text txt : minValEntry) {
				if (txt.getText() != null) {
					int minValue = Integer.parseInt(txt.getText());
					String name = inputs.elementAt(minValEntry.indexOf(txt)).getName();
					simProp.setInputSimParamMin(name, minValue);
				}
			}
			
			/* Set maximum value parameter for each input */
			for(Text txt : maxValEntry) {
				if (txt.getText() != null) {
					int maxValue = Integer.parseInt(txt.getText());
					String name = inputs.elementAt(maxValEntry.indexOf(txt)).getName();
					simProp.setInputSimParamMax(name, maxValue);
				}
			}
		}

		/**
		 * Set simulation properties.
		 * 
		 * @param simProp Simulation properties.
		 */
		public void setSimulationProperties(SimulationProperties simProp) {
			this.simProp = simProp;
			this.simProp.initInputsSimParam(editor.getM2MProject().getStructTreatment().getTop().getInput());
		}

		/**
		 * Refresh the page.
		 */
		public void refreshSimulationPage() {
			if (simProp instanceof SimulationProperties) {
				calcPres.setText(String.valueOf(simProp.getSimulationCalcPrecision()));
				nbSamplesEntry.setText(String.valueOf(simProp.getNumberOfSamples()));
				systemFreqEntry.setText(String.valueOf(simProp.getSystemFrequency()));
				inputFreqEntry.setText(String.valueOf(simProp.getInputFrequency()));
				outputFreqEntry.setText(String.valueOf(simProp.getOutputFrequency()));
				inactivityTimeoutEntry.setText(String.valueOf(simProp.getInactivityTimeout()));
				
				/* Get the minimum and maximum value parameters for each input */
				Vector<Element> inputs = editor.getM2MProject().getStructTreatment().getTop().getInput();
				for(Element el : inputs) {
					String name = el.getName();
					int index = inputs.indexOf(el);
					int minValue = simProp.getInputSimParamMin(name);
					int maxValue = simProp.getInputSimParamMax(name);
					minValEntry.elementAt(index).setText(String.valueOf(minValue));
					maxValEntry.elementAt(index).setText(String.valueOf(maxValue));
				}
			}
		}
	}
}
