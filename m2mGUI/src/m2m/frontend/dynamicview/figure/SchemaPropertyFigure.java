/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical property representation of a schema.
 *
 * Project:  Math2Mat
 *
 * @file SchemaPropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to show the graphical property representation of a schema.
 */
package m2m.frontend.dynamicview.figure;

import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.CheckBox;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;


public class SchemaPropertyFigure extends Figure implements ActionListener
{
	/**
	 * The schema model of the figure.
	 */
	private Schema schema;
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The CheckBox for using 32 bits architecture. 
	 */
    private CheckBox checkBox32Bits = new CheckBox("FLOAT32 architecture");
	/**
	 * The CheckBox for using an 64 bits architecture.
	 */
    private CheckBox checkBox64Bits = new CheckBox("FLOAT64 architecture");
    /**
     * The CheckBox for adding compensation fifo.
     */
    private CheckBox checkBoxFifoOpti = new CheckBox("Optimize Fifos");
    /**
     * The CheckBox for adding compensation fifo.
     */
    private CheckBox checkBoxFifoComp = new CheckBox("Add Compensation Fifos");
	/**
	 * The label name of the represented "loop".
	 */
    private Label warning = new Label();
    
    /**
     * Default contructor of a SchemaPropertyFigure.
     */  
    public SchemaPropertyFigure(Schema schema) 
    {
    	this.schema = schema;
    	
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
    	
        /* Initialisation of the checkBox32Bits */
    	add(checkBox32Bits);
    	setConstraint(checkBox32Bits, new Rectangle(0, 0, -1, -1));
    	checkBox32Bits.addActionListener(this);
    	
        /* Initialisation of the checkBox64Bits */
    	add(checkBox64Bits);
    	setConstraint(checkBox64Bits, new Rectangle(0, 15, -1, -1));
    	checkBox64Bits.addActionListener(this);
    	
        /* Initialisation of the checkBox64Bits */
    	add(checkBoxFifoOpti);
    	setConstraint(checkBoxFifoOpti, new Rectangle(0, 45, -1, -1));
    	checkBoxFifoOpti.addActionListener(this);
    	
        /* Initialisation of the checkBox64Bits */
    	add(checkBoxFifoComp);
    	setConstraint(checkBoxFifoComp, new Rectangle(0, 60, -1, -1));
    	checkBoxFifoComp.addActionListener(this);
    	
        /* Initialisation of the warning label */
        warning.setForegroundColor(ColorConstants.black);
    	add(warning, ToolbarLayout.VERTICAL); 
    	setConstraint(warning, new Rectangle(0, 90, -1, -1));
    	warning.setText("Warning: make sure that the fifos value is sufficient.");
    	warning.setForegroundColor(ColorConstants.red);
    	
    	
        /* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
    }
    
    
    /**
     * Set the graphical property of a specific Schema.
     */
    public void setProperty()
    {
    	/* Initialize the state of checkBoxes */
    	checkBox32Bits.setSelected(schema.getDataType() == NumType.FLOAT32);
    	checkBox64Bits.setSelected(schema.getDataType() == NumType.FLOAT64);
    	checkBoxFifoComp.setSelected(schema.getFifoComp());
    	checkBoxFifoOpti.setSelected(schema.getFifoOpti());  	
    	warning.setVisible(!schema.getFifoOpti());
    }


    /**
     * Set the choosed architecture of the schema.
     * arg The source of the event
     */
	@Override
	public void actionPerformed(ActionEvent arg) 
	{
		/* Update architecture type */
		if(arg.getSource() == checkBox32Bits)
			schema.setDataType(NumType.FLOAT32);
		else if (arg.getSource() == checkBox64Bits)
			schema.setDataType(NumType.FLOAT64);
    	checkBox32Bits.setSelected(schema.getDataType() == NumType.FLOAT32);
    	checkBox64Bits.setSelected(schema.getDataType() == NumType.FLOAT64);
	
    	/* Update compensation fifo */
    	if(arg.getSource() == checkBoxFifoComp)
    		schema.setFifoComp(checkBoxFifoComp.isSelected());    	
    	
    	/* Update compensation fifo */
    	if(arg.getSource() == checkBoxFifoOpti)
    		schema.setFifoOpti(checkBoxFifoOpti.isSelected());  
    	
    	schema.setModified(true);
    	warning.setVisible(!schema.getFifoOpti());
	}
}

