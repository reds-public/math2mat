/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical representation of an operation.
 *
 * Project:  Math2Mat
 *
 * @file OperationFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is the graphical representation of an operation.
 */
package m2m.frontend.dynamicview.figure;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

public class OperationFigure extends Figure
{
	public static final int WIDTH  = 19;
	public static final int HEIGHT = 19;
		
	/**
	 * The label containing the name of the operation.
	 */
	private Label labelName = new Label();
	/**
	 * The shape representing the operation.
	 */
    Shape ellipse = new Ellipse();
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;

    
    /**
     * Contructor of an OperationFigure.
     */
    public OperationFigure() 
    {
    	/* Set the type of layout manager */
        layout = new XYLayout();
        setLayoutManager(layout);   
        
        /* Initialisation ot the ellipse */
        ellipse.setLayoutManager(new ToolbarLayout());
        ellipse.setBackgroundColor(ColorConstants.lightGreen);
		add(ellipse);
		
        /* Initialisation ot the labelName */
    	labelName.setForegroundColor(ColorConstants.black);
    	ellipse.add(labelName, ToolbarLayout.ALIGN_CENTER); 
    }
    
    
    /**
     * Set the name of the operation.
     * @param name The name to the figure to set
     */
    public void setName(String name) 
    {
        labelName.setText(name);
    }
    
    
    /**
     * Set the layout of the figure.
     * @param rect The layout to the figure to set
     */
    public void setLayout(Rectangle rect) 
    {
        getParent().setConstraint(this, rect);
        ellipse.setSize(rect.width, rect.height);
    }
}
