/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Container of a node property figure.
 *
 * Project:  Math2Mat
 *
 * @file PropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: The figure is the container of a node property figure. It draws the correct node property figure corresponding to the selected node.
 */
package m2m.frontend.dynamicview.figure;

import m2m.frontend.dynamicview.model.Connection;
import m2m.frontend.dynamicview.model.GraphicFunction;
import m2m.frontend.dynamicview.model.GraphicIf;
import m2m.frontend.dynamicview.model.GraphicLoop;
import m2m.frontend.dynamicview.model.GraphicLoopIterator;
import m2m.frontend.dynamicview.model.GraphicOperation;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.PointConnection;
import m2m.frontend.dynamicview.model.Schema;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;


public class PropertyFigure extends Figure
{
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
    /**
     * PointConnection property figure of the figure.
     */
    private PointConnectionPropertyFigure pcPropertyFigure;
    /**
     * Schema property figure of the figure.
     */
    private SchemaPropertyFigure schemaPropertyFigure;
    /**
     * GraphicFunction property figure of the figure.
     */
    private FunctionPropertyFigure functionPropertyFigure;
    /**
     * GraphicIf property figure of the figure.
     */
    private IfPropertyFigure ifPropertyFigure;  
    /**
     * GraphicOperation property figure of the figure.
     */
    private OperationPropertyFigure operationPropertyFigure;
    /**
     * GraphicFor property figure of the figure.
     */
    private LoopPropertyFigure forPropertyFigure;
    /**
     * GraphicFor property figure of the figure.
     */
    private LoopIteratorPropertyFigure loopIteratorPropertyFigure;
    /**
     * Connection property figure of the figure.
     */
    private ConnectionPropertyFigure connectionPropertyFigure;
    
    
    /**
     * Default contructor of a SchemaPropertyFigure.
     */
    public PropertyFigure(Schema schema) 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
    	
    	/* Initialisation of the PointConnectionPropertyFigure */
        pcPropertyFigure = new PointConnectionPropertyFigure();
        add(pcPropertyFigure);
        setConstraint(pcPropertyFigure, new Rectangle(0, 0, -1, -1));
        pcPropertyFigure.setVisible(false);
        
        /* Initialisation of the SchemaPropertyFigure */
        schemaPropertyFigure = new SchemaPropertyFigure(schema);
        add(schemaPropertyFigure);
        setConstraint(schemaPropertyFigure, new Rectangle(0, 0, -1, -1));
        schemaPropertyFigure.setVisible(false);
        
        /* Initialisation of the FunctionPropertyFigure */
        functionPropertyFigure = new FunctionPropertyFigure();
        add(functionPropertyFigure);
        setConstraint(functionPropertyFigure, new Rectangle(0, 0, -1, -1));
        functionPropertyFigure.setVisible(false);

        /* Initialisation of the IfPropertyFigure */
        ifPropertyFigure = new IfPropertyFigure();
        add(ifPropertyFigure);
        setConstraint(ifPropertyFigure, new Rectangle(0, 0, -1, -1));
        ifPropertyFigure.setVisible(false);       
        
        /* Initialisation of the OperationPropertyFigure */
        operationPropertyFigure = new OperationPropertyFigure(schema);
        add(operationPropertyFigure);
        setConstraint(operationPropertyFigure, new Rectangle(0, 0, -1, -1));
        operationPropertyFigure.setVisible(false);
        
        /* Initialisation of the ForPropertyFigure */
        forPropertyFigure = new LoopPropertyFigure();
        add(forPropertyFigure);
        setConstraint(forPropertyFigure, new Rectangle(0, 0, -1, -1));
        forPropertyFigure.setVisible(false);
        
        /* Initialisation of the LoopIteratorPropertyFigure */
        loopIteratorPropertyFigure = new LoopIteratorPropertyFigure(schema);
        add(loopIteratorPropertyFigure);
        setConstraint(loopIteratorPropertyFigure, new Rectangle(0, 0, -1, -1));
        loopIteratorPropertyFigure.setVisible(false);
        
        /* Initialisation of the ConnectionPropertyFigure */
        connectionPropertyFigure = new ConnectionPropertyFigure();
        add(connectionPropertyFigure);
        setConstraint(connectionPropertyFigure, new Rectangle(0, 0, -1, -1));
        connectionPropertyFigure.setVisible(false);
        
    	/* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
        setBorder(new LineBorder(3));   
    }
    
    
    /**
     * Set the properties for a specific node
     * @param node The node to the figure to set properties
     */
    public void setProperty(Node node) 
    {
    	/* Mask all property figure */
    	pcPropertyFigure.setVisible(false);
    	schemaPropertyFigure.setVisible(false);
    	functionPropertyFigure.setVisible(false);
    	ifPropertyFigure.setVisible(false);	
    	operationPropertyFigure.setVisible(false);
    	forPropertyFigure.setVisible(false);
    	loopIteratorPropertyFigure.setVisible(false);
    	connectionPropertyFigure.setVisible(false);
    	
    	/* Draw the correct property figure corresponding to the selected node */
    	if(node instanceof Schema)
    	{
    		schemaPropertyFigure.setVisible(true);		
    		schemaPropertyFigure.setProperty();
    	}
    	else if(node instanceof GraphicFunction)
    	{
    		functionPropertyFigure.setVisible(true);		
    		functionPropertyFigure.setProperty((GraphicFunction) node);
    	}
    	else if(node instanceof PointConnection)
    	{
    		pcPropertyFigure.setVisible(true);		
    		pcPropertyFigure.setProperty((PointConnection) node);
    	}
    	else if(node instanceof GraphicIf)
    	{
    		ifPropertyFigure.setVisible(true);		
    		ifPropertyFigure.setProperty((GraphicIf) node);
    	}
    	else if(node instanceof GraphicOperation)
    	{
    		operationPropertyFigure.setVisible(true);		
    		operationPropertyFigure.setProperty((GraphicOperation) node);
    	}
    	else if(node instanceof GraphicLoop)
    	{
    		forPropertyFigure.setVisible(true);		
    		forPropertyFigure.setProperty((GraphicLoop) node);
    	}
    	else if(node instanceof GraphicLoopIterator)
    	{
    		loopIteratorPropertyFigure.setVisible(true);		
    		loopIteratorPropertyFigure.setProperty((GraphicLoopIterator) node);
    	}
    }
    
    /**
     * Set the properties for a specific connection
     * @param conn The connection to the figure to set properties
     */
    public void setProperty(Connection conn) 
    {
    	/* Mask all property figures */
    	pcPropertyFigure.setVisible(false);
    	schemaPropertyFigure.setVisible(false);
    	functionPropertyFigure.setVisible(false);
    	ifPropertyFigure.setVisible(false);	
    	operationPropertyFigure.setVisible(false);
    	forPropertyFigure.setVisible(false);
    	loopIteratorPropertyFigure.setVisible(false);
    	
    	/*  Set the properties the connection */
		connectionPropertyFigure.setVisible(true);		
		connectionPropertyFigure.setProperty((Connection) conn);
    }
    
}
