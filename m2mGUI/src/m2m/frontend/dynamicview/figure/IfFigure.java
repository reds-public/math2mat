/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical representation of a "if".
 *
 * Project:  Math2Mat
 *
 * @file IfFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is the graphical representation of a "if".
 */
package m2m.frontend.dynamicview.figure;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class IfFigure extends Figure
{
	/**
	 * Position of the true InternPointConnection.
	 */
	public static final int TRUE_POINT_X_POSITION = 0;
	public static final int TRUE_POINT_Y_POSITION = 8;
	/**
	 * Position of the false InternPointConnection.
	 */
	public static final int FALSE_POINT_X_POSITION = 0;
	public static final int FALSE_POINT_Y_POSITION = 27;
	/**
	 * Position of the out InternPointConnection.
	 */
	public static final int OUT_POINT_X_POSITION = 35;
	public static final int OUT_POINT_Y_POSITION = 17;
	/**
	 * The size of the figure. 
	 */
	public static final int HEIGHT = 40;
	public static final int WIDTH  = 41;
	
	
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The label containing the name of the true InternPointConnection.
	 */
	private Label labelTrue = new Label();
	/**
	 * The label containing the name of the false InternPointConnection.
	 */
	private Label labelFalse = new Label();
    

    
    /**
     * Default contructor of an IfFigure.
     */
    public IfFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);   
         
        /* Initialisation of the labelTrue */
        labelTrue.setForegroundColor(ColorConstants.black);
        labelTrue.setText("0");
        add(labelTrue, ToolbarLayout.VERTICAL); 
    	setConstraint(labelTrue, new Rectangle(8, TRUE_POINT_Y_POSITION-5, -1, -1)); 
    
    	/* Initialisation of the labelFalse */
    	labelFalse.setForegroundColor(ColorConstants.black);
    	labelFalse.setText("1");
    	add(labelFalse, ToolbarLayout.VERTICAL); 
    	setConstraint(labelFalse, new Rectangle(8, FALSE_POINT_Y_POSITION-5, -1, -1)); 
    	
    	/* Create and locate the four corner of the multiplexer */
		Shape rectangle1 = new RectangleFigure();
		rectangle1.setSize(0, 0);
		rectangle1.setLocation(new Point(0, 0));
		
		Shape rectangle2 = new RectangleFigure();
		rectangle2.setSize(0, 0);
		rectangle2.setLocation(new Point(0, 40));
		
		Shape rectangle3 = new RectangleFigure();
		rectangle3.setSize(0, 0);
		rectangle3.setLocation(new Point(40, 9));
		
		Shape rectangle4 = new RectangleFigure();
		rectangle4.setSize(0, 0);
		rectangle4.setLocation(new Point(40, 31));
		
		/* Create and draw lines between the four corner of the multiplexer */
    	PolylineConnection c1 = new PolylineConnection();
    	c1.setSourceAnchor(new ChopboxAnchor(rectangle1));
    	c1.setTargetAnchor(new ChopboxAnchor(rectangle2));
    	
    	PolylineConnection c2 = new PolylineConnection();
    	c2.setSourceAnchor(new ChopboxAnchor(rectangle1));
    	c2.setTargetAnchor(new ChopboxAnchor(rectangle3));
    	
    	PolylineConnection c3 = new PolylineConnection();
    	c3.setSourceAnchor(new ChopboxAnchor(rectangle3));
    	c3.setTargetAnchor(new ChopboxAnchor(rectangle4));
    	
    	PolylineConnection c4 = new PolylineConnection();
    	c4.setSourceAnchor(new ChopboxAnchor(rectangle2));
    	c4.setTargetAnchor(new ChopboxAnchor(rectangle4));
    	 
    	add(c1);
    	add(c2);
    	add(c3);
    	add(c4);
    	
    	add(rectangle1);
    	add(rectangle2);
    	add(rectangle3);
    	add(rectangle4);
    }
    
    
    /**
     * Set the layout of the figure.
     * @param rect The layout to set to the figure
     */
    public void setLayout(Rectangle rect) 
    {
        getParent().setConstraint(this, rect);
    }
}
