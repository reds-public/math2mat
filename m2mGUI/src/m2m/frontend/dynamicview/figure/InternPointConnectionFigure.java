/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical representation of an intern point connection.
 *
 * Project:  Math2Mat
 *
 * @file InternPointConnectionFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is the graphical representation of an intern point connection.
 */
package m2m.frontend.dynamicview.figure;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

public class InternPointConnectionFigure extends Figure
{
	/**
	 * The size of the figure.
	 */
	public static final int WIDTH = 5;
	public static final int HEIGHT = 5;
	
	
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
    
    
    /**
     * Default contructor of a InternPointConnection.
     */
    public InternPointConnectionFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);     
        
    	/* Initialisation of the figure */
        setOpaque(true);
        setBackgroundColor(ColorConstants.blue);
        setForegroundColor(ColorConstants.black);
        setBorder(new LineBorder(1));
    }
    
    
    /**
     * Set the layout of the figure.
     * @param rect the layout to the figure to set
     */
    public void setLayout(Rectangle rect) 
    {
        getParent().setConstraint(this, rect);
    }
}
