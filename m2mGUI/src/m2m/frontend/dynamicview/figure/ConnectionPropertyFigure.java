/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical property representation of a connection.
 *
 * Project:  Math2Mat
 *
 * @file ConnectionPropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to show the graphical property of a connection.
 */
package m2m.frontend.dynamicview.figure;

import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.Button;
import org.eclipse.draw2d.CheckBox;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;


public class ConnectionPropertyFigure extends Figure implements ActionListener 
{
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The Connection model of the figure.
	 */
	private Connection conn;
    /**
	 * The label name of the source node represented connection.
	 */
    private Label labelSourceName = new Label();
	/**
	 * The label name of the target node represented connection.
	 */
    private Label labelTargetName = new Label();
    /**
     * The button use to delete all bend points of the connection.
     */
    private Button btnDeleteBendPoints;   
    /**
     * The button use to route connections.
     */
    private Button btnRoute;
	/**
	 * The CheckBox for routing automatically the connection.
	 */
    private CheckBox checkBoxAutoRoute = new CheckBox("Automatic route");
    
    
    /**
     * Default contructor of a ConnectionPropertyFigure.
     */  
    public ConnectionPropertyFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
    	
        /* Initialisation of the labelSourceName */
        labelSourceName.setForegroundColor(ColorConstants.black);
    	add(labelSourceName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelSourceName, new Rectangle(0, 0, -1, -1));
    	labelSourceName.setText("Unknown");
    	
    	/* Initialisation of the labelTargetName */
    	labelTargetName.setForegroundColor(ColorConstants.black);
    	add(labelTargetName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelTargetName, new Rectangle(0, 15, -1, -1));
    	labelTargetName.setText("Unknown");
    	
    	/* Initialisation of the btnDeleteBendPoints */
    	btnDeleteBendPoints = new Button("Delete all bend points");
    	add(btnDeleteBendPoints); 
        setConstraint(btnDeleteBendPoints, new Rectangle(0, 45, -1, -1));
        btnDeleteBendPoints.addActionListener(this);
        
    	/* Initialisation of the btnRoute */
        btnRoute = new Button("Route connections");
    	add(btnRoute); 
        setConstraint(btnRoute, new Rectangle(0, 75, -1, -1));
        btnRoute.addActionListener(this);
        
        /* Initialisation of the checkBoxAutoRoute */
    	add(checkBoxAutoRoute);
    	setConstraint(checkBoxAutoRoute, new Rectangle(0, 105, -1, -1));
    	checkBoxAutoRoute.addActionListener(this);	
    	
    	/* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
    }
    
    
    /**
     * Set the graphical property of a specific connection.
     * @param conn The specific connection to the figure to set property
     */
    public void setProperty(Connection conn)
    {
    	this.conn = conn;
    	
    	/* Get and show the name of source and target nodes of the connection */
    	labelSourceName.setText("Source : " + conn.getSource().getName());
    	labelTargetName.setText("Target : " + conn.getTarget().getName());
    	
    	/* Initialize the state of buttons and the checkBox */
    	btnDeleteBendPoints.setEnabled(!conn.getBendPoints().isEmpty());
    	checkBoxAutoRoute.setSelected(conn.getMustBeRoute());
    }

    
    /**
     * Remove all bend points of the connection if the source is the btnDeleteBendPoints button.
     * Remove connections of the schema if the source is the btnRoute button.
     * Enable/Disable the atomatic routing of the connection if the source is the checkBoxAutoRoute.
     * @param arg The source of the event
     */
	@Override
	public void actionPerformed(ActionEvent arg) 
	{
		/* Remove all the bend points of the connection */
		if(arg.getSource() == btnDeleteBendPoints)
		{
			conn.removeAllBendPoint();
			btnDeleteBendPoints.setEnabled(false);
		}		
		/* Route connections */
		else if(arg.getSource() == btnRoute)
		{
			conn.route();	
		}
		/* Enable/Disable the atomatic routing of the connection */
		else if(arg.getSource() == checkBoxAutoRoute)
		{
			conn.setMustBeRoute(checkBoxAutoRoute.isSelected());
		}

	}
}

