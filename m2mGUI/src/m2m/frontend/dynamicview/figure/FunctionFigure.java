/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical representation of a function.
 *
 * Project:  Math2Mat
 *
 * @file FunctionFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is the graphical representation of a function.
 */
package m2m.frontend.dynamicview.figure;

import org.eclipse.draw2d.Button;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

public class FunctionFigure extends Figure
{	
	/**
	 * The size of the reduct button. 
	 */
	public static final int WIDTH_BTN  = 15;
	public static final int HEIGHT_BTN = 15;
	/** 
	 * The space between two reduct nodes.
	 */
	public static final int SPACE_REDUCTION = 20;
	
	
	/**
	 * The label containing the name of the function.
	 */
	private Label labelName = new Label();
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
    /**
     * The button used to reduct the function.
     */
    private Button btnMinimize;


    /**
     * Default contructor of a FunctionFigure.
     */
    public FunctionFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);   
        
        /* Initialization of the label */
    	labelName.setForegroundColor(ColorConstants.black);
    	add(labelName); 
    	setConstraint(labelName, new Rectangle(0, 0, -1, -1));
    	
    	/* Initialization of the button */
    	btnMinimize = new Button("-");
    	add(btnMinimize);  	
    	
    	/* Initialization of the figure */
        setOpaque(true);
        setBackgroundColor(ColorConstants.white);
    	setBorder(new LineBorder(2));  
    }
    
    
    /**
     * Set the name of the function.
     * @param text The name to the figure to set
     */
    public void setName(String text) 
    {
        labelName.setText(text);
    }
    
    
    /**
     * Set the layout of the figure.
     * @param rect The layout to the figure to set.
     */
    public void setLayout(Rectangle rect)
    {
        getParent().setConstraint(this, rect);    	
        setConstraint(btnMinimize, new Rectangle(rect.width-WIDTH_BTN-3, 0, WIDTH_BTN, HEIGHT_BTN));
    }

    
    /**
     * Get the button of the figure.
     * @return The button of the figure
     */
    public Button getBtnMinimize()
    {
    	return btnMinimize;
    }
}
