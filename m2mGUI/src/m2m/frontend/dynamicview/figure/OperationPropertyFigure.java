/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical property representation of an operation.
 *
 * Project:  Math2Mat
 *
 * @file OperationPropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to show the graphical property of an operation.
 */
package m2m.frontend.dynamicview.figure;

import java.util.ArrayList;

import m2m.backend.structure.Operation;
import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.CheckBox;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;


import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.buildingblocks.BuildingBlocksManager;

public class OperationPropertyFigure extends Figure implements ActionListener
{
	/**
	 * The Schema model of the figure.
	 */
	private Schema schema;
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The label containing the name of the represent operation.
	 */
    private Label labelName = new Label();
	/**
	 * The label containing the description of the selected building block.
	 */
    private Label labelDescriptionBlock = new Label();
    /**
     * List of possible buildingBlocks.
     */
    private ArrayList<BuildingBlock> blockList = new ArrayList<BuildingBlock>();
    /**
     * List of checkBox. Each checkBox represents a buildingBlock.
     */
    private ArrayList<CheckBox> checkBoxesBlockList = new ArrayList<CheckBox>();
	/**
	 * The CheckBox for monitor parameter.
	 */
    private CheckBox checkBoxSelectAll = new CheckBox("Select for all same operation");
    /**
     * The graphic operation correspoding to the figure property.
     */
	private GraphicOperation graphicOperation;
    
	
    /**
     * Default contructor of a OperationPropertyFigure.
     */
    public OperationPropertyFigure(Schema schema) 
    {
        this.schema = schema;
        
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
        
        /* Initialisation of the labelName */
        labelName.setForegroundColor(ColorConstants.black);
    	add(labelName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelName, new Rectangle(0, 0, -1, -1));
    	labelName.setText("Unknown");

        /* Initialisation of the labelDescriptionBlock */
    	labelDescriptionBlock.setForegroundColor(ColorConstants.black);
    	add(labelDescriptionBlock, ToolbarLayout.VERTICAL); 
    	setConstraint(labelDescriptionBlock, new Rectangle(0, 150, -1, -1));
    	labelDescriptionBlock.setText("Unknown");
    	
        /* Initialisation of the checkBoxSelectAll */
    	add(checkBoxSelectAll);
    	setConstraint(checkBoxSelectAll, new Rectangle(0, 30, -1, -1));
    	checkBoxSelectAll.addActionListener(this);
    	
        /* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
    }
    
    
    /**
     * Set the graphical property of a specific GraphicOperation.
     * @param graphicOperation The specific GraphicOperation to the figure to set property
     */
    public void setProperty(GraphicOperation graphicOperation)
    {
    	
    	int space = 30;
    	this.graphicOperation = graphicOperation;
    	labelName.setText("Name : " + graphicOperation.getName());

		if(((Operation)(graphicOperation.getElement())).getBlock() == null) {
	    	if(getChildren().contains(checkBoxSelectAll))
	    		remove(checkBoxSelectAll); 
	    	if(getChildren().contains(labelDescriptionBlock))
	    		remove(labelDescriptionBlock); 
	    	for(CheckBox cb : checkBoxesBlockList)
		    	if(getChildren().contains(cb))
		    		remove(cb); 	
			System.out.println("There is no bloc for this operation. Please check the lib path.");
		}	
		else {
			add(labelDescriptionBlock, ToolbarLayout.VERTICAL); 
	    	for(CheckBox cb : checkBoxesBlockList)
	    		if(getChildren().contains(cb))
	    			remove(cb);
			checkBoxesBlockList.removeAll(checkBoxesBlockList); 
			/* Get all possible BuildingBlocks for this operation */
	    	blockList.removeAll(blockList);
	    	Operation opElement = (Operation)(graphicOperation.getElement());		
	    	for(BuildingBlock block : BuildingBlocksManager.getInstance().blockNamed(opElement.getOpName()))
	    		if(block.numType() == schema.getDataType())
	    			blockList.add(block);   	 			 	
	    	
	    	if(blockList.size() != 0)
	    	{
	    		/* Add and initialize the checkBoxSelectAll */
	    		add(checkBoxSelectAll); 
	    		setConstraint(checkBoxSelectAll, new Rectangle(0, space, -1, -1));
	    		checkBoxSelectAll.setSelected(graphicOperation.getSelectAll());
	    		space += 15;
	    	}  		
	    	
	    	/* Create CheckBoxes for the blockList */
	    	for(BuildingBlock block : blockList)
	    	{		
	    		checkBoxesBlockList.add(new CheckBox(block.entityName()));
	    		add(checkBoxesBlockList.get(blockList.indexOf(block)));
	        	setConstraint(checkBoxesBlockList.get(blockList.indexOf(block)), new Rectangle(20, space, -1, -1));
	        	checkBoxesBlockList.get(blockList.indexOf(block)).addActionListener(this);
	    		if(opElement.getBlock() == block)
	    			checkBoxesBlockList.get(blockList.indexOf(block)).setSelected(true);
	        	space += 15;
	    	}
	    		
	    	if(!blockList.contains(opElement.getBlock()))
			{
				/* Set the default buildingBlock */
				checkBoxesBlockList.get(0).setSelected(true);
				opElement.setBlock(blockList.get(0));
			}
			
			setConstraint(labelDescriptionBlock, new Rectangle(0, space+15, -1, -1));
			labelDescriptionBlock.setText("--------------------------------------\n" +
										  "Function name\t : " + opElement.getBlock().functionName() + "\n" +
										  "Entity name\t : " + opElement.getBlock().entityName() + "\n" +
	 									  "Author\t\t : " + opElement.getBlock().author() + "\n" +
	 									  "Description\t : " + opElement.getBlock().description()+ "\n" +
	 									  "--------------------------------------\n");
		}
    }


    /**
     * Set the correct selected building block to the element operation. 
     * @param arg The source of the event
     */
	@Override
	public void actionPerformed(ActionEvent arg) 
	{
		/* Update the state of all checkBoxesBlockList */
		if(checkBoxesBlockList.contains(arg.getSource()))
			for(int i = 0; i < blockList.size(); i++)
				if(checkBoxesBlockList.get(i) == arg.getSource())
				{
					checkBoxesBlockList.get(i).setSelected(true);
					((Operation)(graphicOperation.getElement())).setBlock(blockList.get(i));
					labelDescriptionBlock.setText("--------------------------------------\n" +
							  "Function name\t : " + ((Operation)(graphicOperation.getElement())).getBlock().functionName() + "\n" +
							  "Entity name\t : " + ((Operation)(graphicOperation.getElement())).getBlock().entityName() + "\n" +
							  "Author\t\t : " + ((Operation)(graphicOperation.getElement())).getBlock().author() + "\n" +
							  "Description\t : " + ((Operation)(graphicOperation.getElement())).getBlock().description()+ "\n" +
							  "--------------------------------------\n");
				}
				else
					checkBoxesBlockList.get(i).setSelected(false);		
		
		/* Get the selected building block */
		BuildingBlock block = null;
    	for(CheckBox cb : checkBoxesBlockList)	
    		if(cb.isSelected())
    			block = blockList.get(checkBoxesBlockList.indexOf(cb));
    	
    	/* Set the building block for all same operations if the is checkBoxSelectAll selected */
		if(checkBoxSelectAll.isSelected())
		{
	    	for(Node node : schema.getAllNodes())
	    	{
				if(node instanceof GraphicOperation && ((Operation)(node.getElement())).getOpName() == block.functionName())
				{
					((Operation)(node.getElement())).setBlock(block);
					((GraphicOperation)node).setSelectAll(true);
				}
				else if(node instanceof GraphicLoopIterator && ((GraphicLoopIterator)node).getIterOperation().getOpName() == block.functionName())
				{
					((GraphicLoopIterator)node).getIterOperation().setBlock(block);
					((GraphicLoopIterator)node).setSelectAll(true);
				}
	    	}
		}
		else {
	    	for(Node node : schema.getAllNodes())
	    	{
				if(node instanceof GraphicOperation && ((Operation)(node.getElement())).getOpName() == block.functionName())
					((GraphicOperation)node).setSelectAll(false);
				else if(node instanceof GraphicLoopIterator && ((GraphicLoopIterator)node).getIterOperation().getOpName() == block.functionName())
					((GraphicLoopIterator)node).setSelectAll(false);
	    	}
		}
		
		graphicOperation.setModified(true);
	}
}
