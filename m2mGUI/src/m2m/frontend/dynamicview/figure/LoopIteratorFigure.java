/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical representation of a loop iterator.
 *
 * Project:  Math2Mat
 *
 * @file LoopIteratorFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is the graphical representation of a loop iterator.
 */
package m2m.frontend.dynamicview.figure;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LoopIteratorFigure extends Figure
{
	/**
	 * The position of the out InternPointConnection.
	 */
	public static final int OUT_POINT_X_POSITION = 33;
	public static final int OUT_POINT_Y_POSITION = 15;
	/**
	 * The size of the figure.
	 */
	public static final int WIDTH  = 40;
	public static final int HEIGHT = 35;
	
	
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The label containing the name of the loop iterator.
	 */
	private Label labelName = new Label();
	/**
	 * The label containing the start initialisation of the loop iterator.
	 */
	private Label labelStart = new Label();
	/**
	 * The label containing the incrementation of the loop iterator.
	 */
	private Label labelInc = new Label();
	/**
	 * The label containing the end condition of the loop iterator.
	 */
	private Label labelEnd = new Label();
	
    
    /**
     * Default contructor of an LoopIteratorFigure.
     */
    public LoopIteratorFigure() 
    {
        layout = new XYLayout();
        setLayoutManager(layout);   
         
        /* Initialisation of the labelName */
        labelName.setForegroundColor(ColorConstants.black);
        add(labelName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelName, new Rectangle(WIDTH/2, HEIGHT/2, -1, -1)); 
    	
    	/* Initialisation of the labelStart */
    	labelStart.setForegroundColor(ColorConstants.black);
    	labelStart.setText("Start");
    	add(labelStart, ToolbarLayout.VERTICAL); 
    	setConstraint(labelStart, new Rectangle(8, 0, -1, -1)); 
    	
    	/* Initialisation of the labelInc */
    	labelInc.setForegroundColor(ColorConstants.black);
    	labelInc.setText("Inc");
    	add(labelInc, ToolbarLayout.VERTICAL); 
    	setConstraint(labelInc, new Rectangle(8, 10, -1, -1)); 
    	
    	/* Initialisation of the labelEnd */
    	labelEnd.setForegroundColor(ColorConstants.black);
    	labelEnd.setText("End");
    	add(labelEnd, ToolbarLayout.VERTICAL); 
    	setConstraint(labelEnd, new Rectangle(8, 20, -1, -1)); 
    	
    	/* Create and locate the start rectangle of the loop iterator */
		Shape rectangle1 = new RectangleFigure();
		rectangle1.setSize(5, 5);
		rectangle1.setLocation(new Point(0, 5));
		rectangle1.setBackgroundColor(ColorConstants.blue);
		
		/* Create and locate the incrementation rectangle of the loop iterator */
		Shape rectangle2 = new RectangleFigure();
		rectangle2.setSize(5, 5);
		rectangle2.setLocation(new Point(0, 15));
		rectangle2.setBackgroundColor(ColorConstants.blue);
		
		/* Create and locate the end rectangle of the loop iterator */
		Shape rectangle3 = new RectangleFigure();
		rectangle3.setSize(5, 5);
		rectangle3.setLocation(new Point(0, 25));
		rectangle3.setBackgroundColor(ColorConstants.blue);
		   	
    	add(rectangle1);
    	add(rectangle2);
    	add(rectangle3);
    	
    	setOpaque(true);
    	setBorder(new LineBorder(1));  
        setBackgroundColor(ColorConstants.red);
        setForegroundColor(ColorConstants.black);
    }
    
    
    /**
     * Set the layout of this figure.
     * @param rect The layout to the figure to set
     */
    public void setLayout(Rectangle rect) 
    {
        getParent().setConstraint(this, rect);
    }
    
    
    /**
     * Set the color of the figure depending on a condition.
     * @param cond The selected color condition
     */
    public void setBackgroundColor(boolean cond) 
    {
    	if(cond)
    		setBackgroundColor(ColorConstants.blue);
    	else
    		setBackgroundColor(ColorConstants.white);
    }
}
