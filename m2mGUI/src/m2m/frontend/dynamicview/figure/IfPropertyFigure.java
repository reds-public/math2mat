/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical property representation of a "if".
 *
 * Project:  Math2Mat
 *
 * @file IfPropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to show the graphical property of a "if".
 */
package m2m.frontend.dynamicview.figure;

import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.CheckBox;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;



public class IfPropertyFigure extends Figure implements ActionListener 
{
	/**
	 * The GraphicIf model of the figure.
	 */
	private GraphicIf graphicIf;
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The label containing the name of the represent "if".
	 */
    private Label labelName = new Label();
	/**
	 * The label containing the condition of the figure.
	 */
    private Label labelCondition = new Label();
	/**
	 * The CheckBox for monitoring parameter.
	 */
    private CheckBox checkBoxMonitor = new CheckBox("Monitor mode");
    
    
    /**
     * Default contructor of a PointConnectionPropertyFigure.
     */
    public IfPropertyFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
    	
        /* Initialisation of the labelName */
        labelName.setForegroundColor(ColorConstants.black);
    	add(labelName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelName, new Rectangle(0, 0, -1, -1));
    	labelName.setText("Unknown");
    	
    	/* Initialisation of the labelCondition */
    	labelCondition.setForegroundColor(ColorConstants.black);
    	add(labelCondition, ToolbarLayout.VERTICAL); 
    	setConstraint(labelCondition, new Rectangle(0, 30, -1, -1));
    	labelCondition.setText("Unknown");
    	
    	/* Initialisation of the checkBoxAutoRoute */
    	add(checkBoxMonitor);
    	setConstraint(checkBoxMonitor, new Rectangle(0, 45, -1, -1));
    	checkBoxMonitor.addActionListener(this);
        
    	/* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
    }
    
    
    /**
     * Set the graphical property of a specific GraphicIf.
     * @param graphicIf The specific GraphicIf to the figure to set property
     */
    public void setProperty(GraphicIf graphicIf)
    {
    	this.graphicIf = graphicIf;
    	
    	checkBoxMonitor.setEnabled(!(graphicIf.getParent() instanceof GraphicLoop));
    	
    	/* Initialize the state of the checkBox */
    	checkBoxMonitor.setSelected(graphicIf.getMonitor());
    	
    	/* Get and show the name of name and condition nodes of the GraphicIf */
    	labelName.setText("Name : " + graphicIf.getName());
    	labelCondition.setText("Condition : " + graphicIf.getCondition());
    }


    /**
     * Enable/Disable the monitoring of the condition signal.
     * @param arg The source of the event
     */
	@Override
	public void actionPerformed(ActionEvent arg) 
	{
		/* Enable/Disable the monitoring of the condition signal */
		graphicIf.setMonitor(checkBoxMonitor.isSelected());
		graphicIf.setModified(true);
	}
}
