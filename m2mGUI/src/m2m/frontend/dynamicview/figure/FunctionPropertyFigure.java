/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical property representation of a function.
 *
 * Project:  Math2Mat
 *
 * @file FunctionPropertyFigure.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to show the graphical property of a function.
 */
package m2m.frontend.dynamicview.figure;

import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;


public class FunctionPropertyFigure extends Figure
{
	/**
	 * The layout manager of the figure.
	 */
    private XYLayout layout;
	/**
	 * The label name of the represent function.
	 */
    private Label labelName = new Label();
    
    
    /**
     * Default contructor of a FunctionPropertyFigure.
     */  
    public FunctionPropertyFigure() 
    {
    	/* Set the type of layout */
        layout = new XYLayout();
        setLayoutManager(layout);
    	
        /* Initialisation of the label */
        labelName.setForegroundColor(ColorConstants.black);
    	add(labelName, ToolbarLayout.VERTICAL); 
    	setConstraint(labelName, new Rectangle(0, 0, -1, -1));
    	labelName.setText("Unknown");
    	
    	/* Initialisation of the figure */
        setOpaque(true);     
        setBackgroundColor(ColorConstants.menuBackground);
        setForegroundColor(ColorConstants.menuForeground);
    }
    
    
    /**
     * Set the graphical property of a specific GraphicFunction.
     * @param function The specific GraphicFunction to the figure to set property
     */
    public void setProperty(GraphicFunction graphicfunction)
    {
    	labelName.setText("Name : " + graphicfunction.getName());
    }
}

