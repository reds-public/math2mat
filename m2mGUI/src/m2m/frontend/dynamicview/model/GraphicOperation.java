/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of an operation.
 *
 * Project:  Math2Mat
 *
 * @file GraphicOperation.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a operation. 
 */
package m2m.frontend.dynamicview.model;

import org.eclipse.draw2d.geometry.Rectangle;

public class GraphicOperation extends Node
{
	/**
	 * Output pointConnexion of the operation.
	 */
	private PointConnection outputPointConnection;
	/**
	 * String representation of the operator name.
	 */
	private String operatorName;
	/**
	 * Indicate if all same blocs must be set for all same types of operation.
	 */
	private boolean selectAll;
	
	
	/**
	 * Default contructor.
	 */
	public GraphicOperation ()
	{
		super();
		outputPointConnection = null;
		operatorName = "Unknown";
		selectAll = false;
	}
	
	
	/**
	 * Get the output connection point of the operation.
	 * @return the output pointConnection of the operation
	 */
	public PointConnection getOutputPointConnection()
	{
		return outputPointConnection;
	}
	
	
	/**
	 * Set the output connection point of the operation.
	 * @param pc The connection point to the operation to set
	 */
	public void setOutputPointConnexion(PointConnection pc)
	{
		outputPointConnection = pc;
		pc.setOutputResult(this);
	}
	
	
	/**
	 * Set the operator name of the operation.
	 * @param operatorName The operator name to the operation to set
	 */
	public void setOperatorName(String operatorName)
	{
		this.operatorName = operatorName;
	}
	
	
	/**
	 * Get the operator name of the operation.
	 * @return the operator name of the operation
	 */
	public String getOperatorName()
	{
		return operatorName;
	}

	
	/**
	 * Set the selectAll attribute of the operation.
	 * @param val The value to the selectAll attribute to set
	 */
	public void setSelectAll(boolean val)
	{
		this.selectAll = val;
	}
	
	
	/**
	 * Get the selectAll attribute of the operation.
	 * @return the selectAll attribute of the operation.
	 */ 
	public boolean getSelectAll()
	{
		return selectAll;
	}
	
	
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{
		/* Check if the new layout is inside its parent */
		if(!isInParent(layoutTest))
			return false;
		
		/* Check if the new layout is under connection points */
		for(Node node : getParent().getChildrenArray())
		{
			if(node != this && node != outputPointConnection)
				if(nodeUnderNode(layoutTest, node.getLayout()))
					return false;
		}		
		return true;
	}
}
