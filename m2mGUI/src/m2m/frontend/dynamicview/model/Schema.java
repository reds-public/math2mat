/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Describe a Schema.
 *
 * Project:  Math2Mat
 *
 * @file Schema.java
 * @author Daniel Molla
 *
 * @date: 09.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all atributes who describe a Schema. A Schema is the oldest 
 * node of the view. It hasn't a parent but has children. It usually use to get a particulary 
 * node of the view.
 */
package m2m.frontend.dynamicview.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.project.M2MProject;
import m2m.backend.structure.Element;
import m2m.frontend.dynamicview.model.GraphicLoopIterator;

public class Schema extends Node
{
	/**
	 * Use to raise an event when a property value is changing.
	 */
	public static final String PROPERTY = "Property";
	
	
	/**
	 * The last GraphicIf node selected of the Schema.
	 */
	private GraphicIf lastIfSelected = null;	
	/**
	 * The last GraphicLoopIterator node selected of the Schema.
	 */
	private GraphicLoopIterator lastLoopIteratorSelected = null;	
	/**
	 * The current selected node of the Schema.
	 */
	private Node selectedNode;
	/**
	 * The current selected connection of the Schema.
	 */
	private Connection selectedConnection;
	/**
	 * Specify if a node is selected.
	 */
	private boolean propertyLock = false;
	/**
	 * Use to raise event for changing the color of the node.
	 */
	public int maxDepth = 0;
	/**
	 * The project corresponding to this schema
	 */
	private M2MProject project;

	
	/**
	 * Get the selected node of the Schema.
	 * @return the selected node of the Schema
	 */
	public Node getSelectedNode()
	{
		return selectedNode;
	}	
	
	
	/**
	 * Set the selected node of the Schema.
	 * @param newNode The new selected node to the Schema to set
	 */
	public void setSelectedNode(Node newNode)
	{
		Node oldNode = selectedNode;
		selectedNode = newNode;
		selectedConnection = null;
		firePropertyChange(Schema.PROPERTY, oldNode, newNode);
	}
	
	
	/**
	 * Get the selected connection of the Schema.
	 * @return the selected connection of the Schema
	 */
	public Connection getSelectedConnection()
	{
		return selectedConnection;
	}	
	
	
	/**
	 * Set the selected connection of the Schema.
	 * @param newNode The new selected connection to the Schema to set
	 */
	public void setSelectedConnection(Connection newConn)
	{
		selectedConnection = newConn;
		selectedNode = null;
		firePropertyChange(Schema.PROPERTY, null, newConn);
	}
	
	
	/**
	 * Set the last selected GraphicIf of the Schema.
	 * @param ifSelected The last selected GraphicIf to the Schema to set
	 */
	public void setLastIfSelected(GraphicIf ifSelected)
	{
		lastIfSelected = ifSelected;
	}
	
	
	/**
	 * Get the last selected GraphicIf of the Schema.
	 * @return the last selected GraphicIf of the Schema
	 */
	public GraphicIf getLastIfSelected()
	{
		return lastIfSelected;
	}
	
	
	/**
	 * Set the last selected GraphicLoopIterator of the Schema.
	 * @param iteratorSelected The last selected GraphicLoopIterator to the Schema to set
	 */
	public void setLastLoopIteratorSelected(GraphicLoopIterator iteratorSelected)
	{
		lastLoopIteratorSelected = iteratorSelected;
	}
	
	
	/**
	 * Get the last selected GraphicLoopIterator of the Schema.
	 * @return the last selected GraphicLoopIterator of the Schema
	 */
	public GraphicLoopIterator getLastloopIteratorSelected()
	{
		return lastLoopIteratorSelected;
	}
	
	
	/**
	 * Set the propertyLock attribute of the Schema.
	 * @param val The value to the propertyLock attribute to set
	 */
	public void setPropertyLock(boolean val)
	{
		propertyLock = val;
	}	
	
	
	/**
	 * Get the propertyLock of the Schema.
	 * @return the propertyLock of the Schema
	 */
	public boolean getPropertyLock()
	{
		return propertyLock;
	}	
	
	
	/**
	 * Get All nodes of the Schema.
	 * @return a vector contains all nodes of the schema
	 */
	public Vector<Node> getAllNodes()
	{
		/* Use to get all nodes */
		Node node;
		LinkedList<Node> tempNodeList = new LinkedList<Node>();
		Iterator<Node> iterNode;
		Vector<Node> nodeList = new Vector<Node>();
		
		/* Initialize the nodes list and the node iterator */
		tempNodeList.addAll(getChildrenArray());
		nodeList.addAll(tempNodeList);
		iterNode = tempNodeList.iterator();
			
		/* Get all the node and connection inside the schema */
		while (iterNode.hasNext()) 
		{
			node = iterNode.next();
					
			/* Check if the node has children */
			if(node.getChildrenArray().size() != 0)	
			{
				tempNodeList.addAll(node.getChildrenArray());
				nodeList.addAll(node.getChildrenArray());
			}

			tempNodeList.remove(tempNodeList.indexOf(node));
			iterNode = tempNodeList.iterator();	
		}
		
		return nodeList;
	}
	
	
	/**
	 * Get all internal elements of the Schema.
	 * @return all internal elements
	 */
	public Vector<Element> getInternalElements()
	{
		 Vector<Element> internalElements = new Vector<Element>();
		 Vector<Node> nodes = new Vector<Node>();
		 
		 // Get all nodes of the graphic schema
		 nodes = getAllNodes();
		 
		 /* Select internal pointConnection and get its internal element */
		 for(Node node : nodes)
			 if(node instanceof PointConnection && node.getParent() != this && !internalElements.contains(node.getElement()))
				 internalElements.add(node.getElement());		 
		 
		 return internalElements;
	}
	
	
	/**
	 * Set the depth of all nodes and the max depth of the schema.
	 * @param nodes All nodes of the schema
	 */
	public void setMaxDepth(Vector<Node> nodes)
	{
		for(Node node : nodes)
		{
			node.setDepthLevel();
			if(maxDepth < node.getDepthLevel())
				maxDepth = node.getDepthLevel();
		}
	}


	/**
	 * Set the project corresponding to this view
	 * @param project the project corresponding to this view
	 */
	public void setproject(M2MProject project) {
		this.project = project;
	}
	

	/**
	 * Get the data type of the project corresponding to the schema
	 * @return the data type of the project corresponding to the schema
	 */
	public NumType getDataType() {
		return project.getOptimisationProperties().getOptimisationDataType();
	}

	
	/**
	 * Get the fifo compensation property.
	 * @return the fifo compensation property
	 */
	public boolean getFifoComp() {
		return project.getProperties().getOptimisationProperties().getFifoCompensation();	
	}
	
	/**
	 * Set the fifo compensation property.
	 * @param val the fifo compensation property to set
	 */
	public void setFifoComp(boolean val) {
		project.getProperties().getOptimisationProperties().setFifoCompensation(val);
	}

	/**
	 * Set the data type of the project corresponding to the schema
	 * @param num the data type of the project corresponding to the schema
	 */
	public void setDataType(NumType num) {
		if (num!=project.getOptimisationProperties().getOptimisationDataType()) {
			project.getOptimisationProperties().setOptimisationDataType(num);
			project.getStructTreatment().modifyNumType(num);
		}
	}


	/**
	 * Set the fifo optimisation property
	 * @param selected the fifo optimization property to set
	 */
	public void setFifoOpti(boolean selected) {
		project.getProperties().getOptimisationProperties().setOptimizeFifo(selected);
	}


	/**
	 * Get the fifo optimisation property
	 * @return the fifo compensation property to set
	 */
	public boolean getFifoOpti() {
		return project.getProperties().getOptimisationProperties().getOptimizeFifo();	
	}
}
