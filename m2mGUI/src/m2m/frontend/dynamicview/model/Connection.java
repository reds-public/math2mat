/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a connection.
 *
 * Project:  Math2Mat
 *
 * @file Connection.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a connection. 
 */
package m2m.frontend.dynamicview.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;

public class Connection
{
	/**
	 * Use to raise an event when the visible attribute is changing.
	 */
    public static final String VISIBLE_CONNECTION = "VisibleConnection";
	/**
	 * Use to raise an event when the a bend point is changing.
	 */
    public static final String BENDPOINT_POSITION = "BendPointPosition";
	/**
	 * Use to raise an event when all connections must be route.
	 */
    public static final String ROUTE_CONNECTION   = "RouteConnection";
    
    
    /**
     * List of listeners of the connection.
     */
    private PropertyChangeSupport listeners;
	/**
	 * The line drawing style of the connection.
	 */
	private int lineStyle = Graphics.LINE_SOLID;
	/**
	 * The source node of the connection.
	 */
	private Node source;
	/**
	 * The target node of the connection.
	 */
	private Node target;
    /**
     * Indicate if the connection is visible.
     */
    private boolean visible = true;
    /**
     * Indicate if a connection must be route.
     */
    private boolean mustBeRoute = true;
    /**
     * List of bend points of the conenction
     */
    private Vector<BendPoint> bendPoints = new Vector<BendPoint>();
	
    
	/** 
	 * Create a (solid) connection between two distinct nodes.
	 * @param source The source node for the connection (non null)
	 * @param target The target node for the connection (non null)
	 */
	public Connection(Node source, Node target) 
	{	
		this.source = source;
		this.target = target;
		this.source.addConnection(this);
		this.target.addConnection(this);
		this.listeners = new PropertyChangeSupport(this);
	}	
	
		
	/**
	 * Get the line drawing style of this connection.
	 * @return The int value represented the the line drawing style
	 */
	public int getLineStyle() 
	{
		return lineStyle;
	}
	
	
	/**
	 * Get the source node of the connection.
	 * @return the source node of the connection.
	 */
	public Node getSource() 
	{
		return source;
	}
	
	
	/**
	 * Get the target node of the connection.
	 * @return the target node of the connection.
	 */
	public Node getTarget() 
	{
		return target;
	}
	
	
    /**
     * Get the visible attribute of the connection.
     * @return the visible attribute of the connection
     */
    public boolean getVisible() 
    {
        return this.visible;
    }
    
    
    /**
     * Set the visible attribute of the connection.
     * @param visible the visible attribute to the connection to set
     */
    public void setVisible(boolean visible) 
    {
		boolean odlValue = this.visible;
		this.visible = visible;
		firePropertyChange(Connection.VISIBLE_CONNECTION, odlValue, visible);
    }  
    
    
    /**
     * Get the mustBeRoute attribute of the connection.
     * @return the mustBeRoute attribute of the connection
     */
    public boolean getMustBeRoute() 
    {
        return this.mustBeRoute;
    }
    
    
    /**
     * Set the mustBeRoute attribute of the connection.
     * @param val Value to the mustBeRoute attribute to set
     */
    public void setMustBeRoute(boolean val) 
    {
        this.mustBeRoute = val;
    }
    
    
    /**
     * Get the list of bend points of the connection.
     * @return the list of bend points  of the connection
     */
    public Vector<BendPoint> getBendPoints() 
    {
        return bendPoints;
    }
       
    
    /**
     * Add a bend point in the list of bend points of the connection.
     * @param bp The bend point to the connection to add
     */
    public void addBendPoint(BendPoint bp)
    {
    	bendPoints.add(bp.getIndex(), bp); 	 	
    	firePropertyChange(Connection.BENDPOINT_POSITION, null, bendPoints);
    	firePropertyChange(Schema.PROPERTY, null, bendPoints);
    }
    
    
    /**
     * Remove a bend point in the list of bend points of the connection.
     * @param index The index of the bend point to the connection to remove 
     */
    public void removeBendPoint(int index)
    {
    	bendPoints.remove(index); 	 	
    	firePropertyChange(Connection.BENDPOINT_POSITION, null, bendPoints);
    	firePropertyChange(Schema.PROPERTY, null, bendPoints);
    }
    
    
    /**
     * Remove as bend points of the list of bend points of the connection.
     */
    public void removeAllBendPoint()
    {
    	bendPoints.removeAll(bendPoints); 	 	
    	firePropertyChange(Connection.BENDPOINT_POSITION, null, bendPoints);
    }    
    
    /**
     * Modify the value of a particulary bend point of the list of bend points of the connection.
     * @param index The index of the bend point to modify to the connection
     * @param d1    The new new first dimension of the bend point to modify
     * @param d2    The new new second dimension of the bend point to modify
     */
    public void modifyBendPoint(int index, Dimension d1, Dimension d2)
    {
    	BendPoint oldBp = bendPoints.get(index);
    	BendPoint NewBp = oldBp;
    	
    	NewBp.setHeight(d1.height, d2.height);
    	NewBp.setWidth(d1.width, d2.width);

    	firePropertyChange(Connection.BENDPOINT_POSITION, null, NewBp);
    }
    
    
    /**
     * Route again all connection of the schema.
     */
    public void route()
    {
    	firePropertyChange(Connection.ROUTE_CONNECTION, null, this);
    }
    
    
	/**
	 * Raise an event for each listener of the connection.
	 * @param property the property to the listeners to raise
	 * @param oldValue the old value to the connection to replace
	 * @param newValue the new value to the connection to set
	 */
	protected void firePropertyChange(String property, Object oldValue, Object newValue) 
	{
		if (listeners.hasListeners(property))
			listeners.firePropertyChange(property, oldValue, newValue);
	}
	
	
	/**
	 * Add a listener in the list of listeners of the connection.
	 * @param listener The listener to the connection to add
	 */
    public void addPropertyChangeListener(PropertyChangeListener listener) 
    {
        listeners.addPropertyChangeListener(listener);
	}
	  
    
	/**
	 * Remove a listener in the list of listeners of this connection.
	 * @param listener listener The listener to the connection to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) 
	{
	    listeners.removePropertyChangeListener(listener);
	}
	
	
	/**
	 * Refresh all connections.
	 */
	public void refreshConnection()
	{
		firePropertyChange(Connection.BENDPOINT_POSITION, null, bendPoints);
		firePropertyChange(Schema.PROPERTY, null, bendPoints);
	}    	
}