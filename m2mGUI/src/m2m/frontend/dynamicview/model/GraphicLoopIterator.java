/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a loop iterator.
 *
 * Project:  Math2Mat
 *
 * @file GraphicLoopIterator.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a loop iterator. 
 */
package m2m.frontend.dynamicview.model;

import java.util.Vector;

import m2m.backend.structure.Operation;
import m2m.backend.structure.StructTreatment;
import m2m.frontend.dynamicview.figure.InternPointConnectionFigure;
import m2m.frontend.dynamicview.figure.LoopIteratorFigure;
import m2m.frontend.view.Editor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.ui.PlatformUI;

public class GraphicLoopIterator extends Node
{
	/**
	 * Indicate if all same blocs must be set for all same types of operation.
	 */
	private boolean selectAll;
	/**
	 * Start value of the GraphicLoopIterator.
	 */
	private String startInitialisation;
	/**
	 * Increment of the GraphicLoopIterator.
	 */
	private String incrementation;
	/**
	 * End condition of the GraphicLoopIterator.
	 */
	private String endCondition;
	/**
	 * The out intern connection point of the GraphicLoopIterator.
	 */
	private InternPointConnection outPoint = new InternPointConnection();
	/**
	 * The output connection point of the GraphicLoopIterator.
	 */
	private PointConnection outputPointConnection = null;
    /**
     * List of condition nodes
     */
	private Vector<Node> conditionNodes = new Vector<Node>();
	private Operation iterOperation;
	
	/**
	 * Default contructor.
	 */
	public GraphicLoopIterator ()
	{
		super();
		
		startInitialisation = "Unknown";
		incrementation = "Unknown";
		endCondition = "Unknown";
		
		/* Initialisation of the out intern connection point */
		outPoint.setName("out");
	    outPoint.setParent(this);
	    outPoint.setLayout(new Rectangle (LoopIteratorFigure.OUT_POINT_X_POSITION, LoopIteratorFigure.OUT_POINT_Y_POSITION,
	        		                      InternPointConnectionFigure.WIDTH, InternPointConnectionFigure.HEIGHT));
	    
	    iterOperation = null;
	}
	
	
	/**
	 * Constructor with iterator parameter values.
	 * @param startInit String representation of the start initialisation value
	 * @param inc		String representation of the incrementation value
	 * @param endCond	String representation of the end condition value
	 */
	public GraphicLoopIterator (String startInit, String inc, String endCond, Operation op)
	{
		super();
		
		startInitialisation = startInit;
		incrementation = inc;
		endCondition = endCond;
		
		/* Initialisation of the out intern connection point */
		outPoint.setName("out");
	    outPoint.setParent(this);
	    outPoint.setLayout(new Rectangle (LoopIteratorFigure.OUT_POINT_X_POSITION, LoopIteratorFigure.OUT_POINT_Y_POSITION,
	        		                      InternPointConnectionFigure.WIDTH, InternPointConnectionFigure.HEIGHT));
	    
	    iterOperation = op;
	}
	
	
    /**
     * Get the monitor attribute of the GraphicLoopIterator element.
     * @return the monitor attribute of the GraphicLoopIterator element.
     */
	public boolean getMonitor()
	{
		return getElement().getMonitor();
	}
	
	
	/**
	 * Set the monitor attribute of the GraphicLoopIterator element.
	 * @param monitor Monitor value to the GraphicLoopIterator element to set
	 */
	public void setMonitor(boolean monitor)
	{ 
		Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		StructTreatment treatment = editor.getM2MProject().getStructTreatment();

		getElement().setMonitor(monitor,treatment);
	}
	
	
	/**
	 * Get the start initialisation string representation of the GraphicLoopIterator.
	 * @return the start initialisation string representation of the GraphicLoopIterator
	 */
	public String getStartInitialisation()
	{
		return startInitialisation;
	}
	
	
	/**
	 * Set the start initialisation string representation of the GraphicLoopIterator.
	 * @param startInit Start initialisation string representation to the iterator to set
	 */
	public void setStartInitialisation(String startInit)
	{
		startInitialisation = startInit;
	}
	
	
	/**
	 * Get the incrementation string representation of the GraphicLoopIterator.
	 * @return the incrementation string representation of the GraphicLoopIterator
	 */
	public String getIncrementation()
	{
		return incrementation;
	}
	
	
	/**
	 * Set the incrementation string representation of the GraphicLoopIterator.
	 * @param inc Incrementation string representation to the iterator to set
	 */
	public void setIncrementation(String inc)
	{
		incrementation = inc;
	}
	
	
	/**
	 * Get the end condition string representation of the GraphicLoopIterator.
	 * @return the end condition string representation of the GraphicLoopIterator
	 */
	public String getEndCondition()
	{
		return endCondition;
	}
	
	
	/**
	 * Set the end condition of the of the GraphicLoopIterator.
	 * @param endCond End condition string representation to the iterator to set
	 */
	public void setEndCondition(String endCond)
	{
		endCondition = endCond;
	}
	
	
	/**
	 * Get the outPoint attribute of the GraphicLoopIterator.
	 * @return the outPoint attribute of the GraphicLoopIterator
	 */
	public InternPointConnection getOutPoint()
	{
		return outPoint;
	}
		
	
    /**
     * Get the vector of child of the GraphicLoopIterator.
     * @return  the vector of child of the GraphicLoopIterator
     */
    public Vector<Node> getChildrenArray() 
    {
    	Vector<Node> children = new Vector<Node>();
    	children.add(outPoint);
        return children;
    }
    
    
    /**
	 * Add a node in the vector of condition nodes of the GraphicLoopIterator.
	 * @param node Node to the vector to add
	 */
    public void addConditionNodes(Node node) 
	{
        if (node == null)
            throw new IllegalArgumentException();
        else 
        	conditionNodes.add(node);     
    }
	
    
	/**
	 * Remove a node in the vector of condition nodes of the GraphicLoopIterator.
	 * @param pc Node to the vector to remove
	 */
    public void removeConditionNodes(Node node) 
	{
        if (node == null)
            throw new IllegalArgumentException();
        else 
        	conditionNodes.remove(node);     
	}
    
    
	/**
	 * Get the vector of condition nodes of the GraphicLoopIterator.
	 * @return the vector of condition nodes of the GraphicLoopIterator
	 */
	public Vector<Node> getConditionNodes() 
	{
        return conditionNodes;
	}
	
	
    /**
     * Set the visible attribute of the GraphicLoopIterator.
     * @param visible Visible attribute to the GraphicLoopIterator to set
     */
    public void setVisible(boolean visible) 
    {
		super.setVisible(visible);
		outPoint.setVisible(visible);
    }     
    
    
	/**
	 * Get the output connection point.
	 * @return the output connection point of the GraphicLoopIterator
	 */
	public PointConnection getOutputPointConnection()
	{
		return outputPointConnection;
	}
	
	
	/**
	 * Set the output connection point of the GraphicLoopIterator.
	 * @param pc The connection point to the output connection point to set
	 */
	public void setOutputPointConnexion(PointConnection pc)
	{
		outputPointConnection = pc;
		pc.setOutputResult(this);
	}
	
	
	/**
	 * Get the iterator operation of the loop for.
	 * @return the iterator operation of the loop for
	 */
	public Operation getIterOperation() {
		return iterOperation;	
	}

	
	/**
	 * Set the iterator operation of the loop for.
	 * @param op the iterator operation to set to the loop for
	 */
	public void setIterOperation(Operation op) {
		iterOperation = op;
	}
	
	/**
	 * Set the selectAll attribute of the operation.
	 * @param val The value to the selectAll attribute to set
	 */
	public void setSelectAll(boolean val)
	{
		this.selectAll = val;
	}
	
	
	/**
	 * Get the selectAll attribute of the operation.
	 * @return the selectAll attribute of the operation.
	 */ 
	public boolean getSelectAll()
	{
		return selectAll;
	}
	
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{
		/* Check if the new layout is inside its parent */
		if(!isInParent(layoutTest))
			return false;
		
		/* Check if the new layout is under connection points */
		for(Node node : getParent().getChildrenArray())
		{
			if(node != this && node != outputPointConnection)
				if(nodeUnderNode(layoutTest, node.getLayout()))
					return false;
		}		
		return true;
	}
}
