/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a bend point.
 *
 * Project:  Math2Mat
 *
 * @file BendPoint.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is used to describe the model of a bend point. The connections use bend points to force their routing.
 */
package m2m.frontend.dynamicview.model;

public class BendPoint
{
	/**
	 * The height two dimensions of the bend point.
	 */
	private int [] height = new int[2];
	/**
	 * The width two dimensions of the bend point.
	 */
	private int [] width  = new int[2];
	/**
	 * The connection of the bend point.
	 */
	private Connection conn;
	/**
	 * The index of the bend point in the bend Point list of connections.
	 */
	private int index;
		

	/**
	 * Set the height two dimensions of the bend point.
	 * @param heightDim1 The first dimension to the bend point to set
	 * @param heightDim2 The second dimension to the bend point to set
	 */
	public void setHeight(int heightDim1, int heightDim2) 
	{
		this.height[0] = heightDim1;
		this.height[1] = heightDim2;
	}
	
	/**
	 * Get the height two dimensions of the bend point.
	 * @return the height two dimensions of the bend point.
	 */
	public int []  getHeight() 
	{
		return height;
	}
	
	
	/**
	 * Set the width two dimensions of the bend point.
	 * @param widthDim1 The first dimension to the bend point to set
	 * @param widthDim2 The second dimension to the bend point to set
	 */
	public void setWidth(int widthDim1, int widthDim2)
	{
		this.width[0] = widthDim1;
		this.width[1] = widthDim2;
	}	
	
	
	/**
	 * Get the width two dimensions of the bend point.
	 * @return the width two dimensions of the bend point.
	 */
	public int []  getWidth() 
	{
		return width;
	}
	
	
	/**
	 * Set the index of the bend point.
	 * @param index The index to the bend point to set
	 */
	public void setIndex(int index) 
	{
		this.index = index;
	}
	
	
	/**
	 * Get the index of the bend point.
	 * @return the index of the bend point
	 */
	public int getIndex() 
	{
		return index;
	}
	
	
	/**
	 * Set the connection of the bend point .
	 * @param conn The connection to the bend point to set
	 */
	public void setConnection(Connection conn) 
	{
		this.conn = conn;
	}
	
	
	/**
	 * Get the connection of the bend point.
	 * @return the connection of the bend point
	 */
	public Connection getConnection() 
	{
		return conn;
	}
}
