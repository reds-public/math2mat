/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a function.
 *
 * Project:  Math2Mat
 *
 * @file GraphicFunction.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a function. 
 */
package m2m.frontend.dynamicview.model;

import java.util.Vector;

import org.eclipse.draw2d.geometry.Rectangle;

public class GraphicFunction extends Node
{
	/**
	 * The full size of the function.
	 */
	int fullSizeWidth;
	int fullSizeHeight;
	/**
	 * The reduct size of the function.
	 */
	int reductSizeWidth;
	int reductSizeHeight;
	/**
	 * List of output pointConnexion of the function.
	 */
	private Vector<PointConnection> outputPointsConnection = new Vector<PointConnection>();
	/**
	 * Indicate if the function is reduct or not.
	 */
	private boolean notReduct = true;
	
	/**
	 * Get the width of the reduct size of the function.
	 * @return the width of the reduct size of the function
	 */
	public int getReductSizeWidth()
	{
		return reductSizeWidth;
	}
	
	
	/**
	 * Get the height of the reduct size of the function.
	 * @return the height of the reduct size of the function
	 */
	public int getReductSizeHeight()
	{
		return reductSizeHeight;
	}
	
	
	/**
	 * Get the width of the full size of the function.
	 * @return the width of the full size of the function
	 */
	public int getFullSizeWidth()
	{
		return fullSizeWidth;
	}	
	
	
	/**
	 * Get the height of the full size of the function.
	 * @return the height of the full size of the function
	 */
	public int getFullSizeHeight()
	{
		return fullSizeHeight;
	}	
	
	
	/**
	 * Set the reduct size of the function.
	 * @param width  The width to the reduct size to set 
	 * @param height The height to the reduct size to set
	 */
	public void setReductSize(int width, int height)
	{
		reductSizeWidth = width;
		reductSizeHeight = height;
	}
	
	
	/**
	 * Set the full size of the function.
	 * @param width  The width to the full size to set 
	 * @param height The height to the full size to set
	 */
	public void setFullSize(int width, int height)
	{
		fullSizeWidth = width;
		fullSizeHeight = height;
	}
	
	
	/**
	 * Get output connection points of the function.
	 * @return the output connection points of the function
	 */
	public Vector<PointConnection> getOutputPointsConnection()
	{
		return outputPointsConnection;
	}
	
	
	/**
	 * Add an output connection point in the output connection points list of the function.
	 * @param pc The connection point to the  list to add
	 */
	public void addOutputPointConnection(PointConnection pc)
	{
		outputPointsConnection.add(pc);
		pc.setOutputResult(this);
	}
	
	
	/**
	 * Set the size of the layout of the function.
	 * @param width  The width to the size to set 
	 * @param height The height to the size to set
	 */
	public void setSize(int width, int height)
	{
		Rectangle layout = getLayout();
		Rectangle newLayout = new Rectangle(layout.x, layout.y, width, height);
		if (CanSetLayout(newLayout))
			setLayoutReduction(newLayout);
	}
    
    
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{
		/* Check if the new layout is inside its parent */
		if(!isInParent(layoutTest))
			return false;
		
		/* Check if the new layout is under connection points */
		for(Node node : getParent().getChildrenArray())
		{
			if(node != this && !outputPointsConnection.contains(node))
				if(nodeUnderNode(layoutTest, node.getLayout()))
					return false;
		}	
		
		return true;
	}
	
	
	/**
	 * 
	 * @param reduct
	 */
	public void setNotReduct(boolean notReduct)
	{
		this.notReduct = notReduct;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getNotReduct()
	{
		return notReduct;
	}
}
