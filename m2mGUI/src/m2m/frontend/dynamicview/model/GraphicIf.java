/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a "if".
 *
 * Project:  Math2Mat
 *
 * @file GraphicIf.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a "if". 
 */
package m2m.frontend.dynamicview.model;

import java.util.Vector;

import m2m.backend.structure.IfThenElse;
import m2m.backend.structure.Multiplexer;
import m2m.backend.structure.Operation;
import m2m.backend.structure.StructTreatment;
import m2m.frontend.dynamicview.figure.IfFigure;
import m2m.frontend.dynamicview.figure.InternPointConnectionFigure;
import m2m.frontend.view.Editor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.ui.PlatformUI;



public class GraphicIf extends Node
{
	/**
	 * The condition of the GraphicIf.
	 */
	private String condition;
	/**
	 * The true intern connection point of the GraphicIf.
	 */
	private InternPointConnection truePoint = new InternPointConnection();
	/**
	 * The false intern connection point of the GraphicIf.
	 */
	private InternPointConnection falsePoint = new InternPointConnection();
	/**
	 * The out intern connection point of the GraphicIf.
	 */
	private InternPointConnection outPoint = new InternPointConnection();
	/**
	 * The output connection point of the GraphicIf.
	 */
	private PointConnection outputPointConnection = null;
    /**
     * List of condition connection points.
     */
	private Vector<Node> conditionNodes = new Vector<Node>();
	
	
    /**
     * Default contructor of a GraphicIf.
     */
	public GraphicIf()
	{
		super();
		
		/* Initialisation of the true intern connection point */
        truePoint.setName("true");
        truePoint.setParent(this);
        truePoint.setLayout(new Rectangle (IfFigure.TRUE_POINT_X_POSITION,IfFigure.TRUE_POINT_Y_POSITION,
        		                           InternPointConnectionFigure.WIDTH, InternPointConnectionFigure.HEIGHT));    
        
        /* Initialisation of the false intern connection point */
        falsePoint.setName("false");
        falsePoint.setParent(this);
        falsePoint.setLayout(new Rectangle (IfFigure.FALSE_POINT_X_POSITION,IfFigure.FALSE_POINT_Y_POSITION,
        		                            InternPointConnectionFigure.WIDTH, InternPointConnectionFigure.HEIGHT));       
        
        /* Initialisation of the out intern connection point */
        outPoint.setName("out");
        outPoint.setParent(this);
        outPoint.setLayout(new Rectangle (IfFigure.OUT_POINT_X_POSITION,IfFigure.OUT_POINT_Y_POSITION,
        		                          InternPointConnectionFigure.WIDTH, InternPointConnectionFigure.HEIGHT));      
	}
	
	
	/**
	 * Get the output connection point.
	 * @return the output connection point of the GraphicIf
	 */
	public PointConnection getOutputPointConnection()
	{
		return outputPointConnection;
	}
	
	
	/**
	 * Set the output connection point of the GraphicIf.
	 * @param pc The connection point to the output connection point to set
	 */
	public void setOutputPointConnexion(PointConnection pc)
	{
		outputPointConnection = pc;
		pc.setOutputResult(this);
	}
	
	
    /**
     * Get the monitor attribute of the GraphicIf element.
     * @return the monitor attribute of the GraphicIf element
     */
	public boolean getMonitor()
	{
		if(getElement() instanceof Multiplexer)
			return ((Multiplexer)getElement()).getSel().getMonitor();
		else
			return ((Operation)((IfThenElse)getElement()).getCond().firstElement()).getInputAt(0).getMonitor();
	}
	
	
	/**
	 * Set the monitor attribute of the GraphicIf element.
	 * @param monitor Monitor value to the GraphicIf element to set
	 */
	public void setMonitor(boolean monitor)
	{
		Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		StructTreatment treatment = editor.getM2MProject().getStructTreatment();
		
		if(getElement() instanceof Multiplexer)
			((Multiplexer)getElement()).getSel().setMonitor(monitor,treatment);
		else
			((Operation)((IfThenElse)getElement()).getCond().firstElement()).getInputAt(0).setMonitor(monitor,treatment);
	}
	
	
	/**
	 * Get the condition of the GraphicIf.
	 * @return the condition of the GraphicIf
	 */
	public String getCondition()
	{
		return condition;
	}
	
	
	/**
	 * Set the condition of the GraphicIf.
	 * @param cond The value to the condition to set.
	 */
	public void setCondition(String cond)
	{
		condition = cond;
	}
	
	
	/**
	 * Get the true intern connection point of the GraphicIf.
	 * @return the true intern connection point of the GraphicIf
	 */
	public InternPointConnection getTruePoint()
	{
		return truePoint;
		
	}
	
	
	/**
	 * Get the false intern connection point of the GraphicIf.
	 * @return the false intern connection point of the GraphicIf
	 */
	public InternPointConnection getFalsePoint()
	{
		return falsePoint;
	}
	
	
	/**
	 * Get the out intern connection point of the GraphicIf.
	 * @return the out intern connection pointof the GraphicIf
	 */
	public InternPointConnection getOutPoint()
	{
		return outPoint;
	}
	
	
	/**
	 * Add a connection point in the list of condition nodes.
	 * @param pc The connection point to the list to add
	 */
    public void addConditionNodes(Node node) 
	{
        if (node == null)
            throw new IllegalArgumentException();
        else 
        	conditionNodes.add(node);     
    }
	
    
	/**
	 * Remove a connection point in the list of condition nodes.
	 * @param pc The connection point to the list to remove
	 */
    public void removeConditionNodes(Node node) 
	{
        if (node == null)
            throw new IllegalArgumentException();
        else 
        	conditionNodes.remove(node);     
    }
	
	
	/**
	 * Get the list of condition nodes of the GraphicIf.
	 * @return the list of conditionnodes of the GraphicIf
	 */
	public Vector<Node> getConditionNodes() 
	{
        return conditionNodes;
	}
	
	
    /**
     * Get the list of child of the GraphicIf.
     * @return the list of child of the GraphicIf
     */
    public Vector<Node> getChildrenArray() 
    {
    	Vector<Node> children = new Vector<Node>();
    	children.add(truePoint);
    	children.add(falsePoint);
    	children.add(outPoint);
        return children;
    }
    
    
    /**
     * Set the visible attribute of the GraphicIf.
     * @param visible The value to the visible attribute to set
     */
    public void setVisible(boolean visible) 
    {
		super.setVisible(visible);
		truePoint.setVisible(visible);
		falsePoint.setVisible(visible);
		outPoint.setVisible(visible);
    }     
    
    
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{
		/* Check if the new layout is inside its parent */
		if(!isInParent(layoutTest))
			return false;
		
		/* Check if the new layout is under connection points */
		for(Node node : getParent().getChildrenArray())
		{
			if(node != this && node != outputPointConnection)
				if(nodeUnderNode(layoutTest, node.getLayout()))
					return false;
		}		
		return true;
	}
}
