/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a "loop".
 *
 * Project:  Math2Mat
 *
 * @file GraphicLoop.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a "loop". 
 */
package m2m.frontend.dynamicview.model;

public class GraphicLoop extends GraphicFunction
{
	/**
	 * Indicate if the layout of the GraphicLoop is reduct.
	 */
	private boolean isReduct = false;
	
	
	/**
	 * Get the isReduct attribute of the GraphicLoop.
	 * @return the isReduct attribute of the GraphicLoop
	 */
	public boolean getIsReduct()
	{
		return isReduct;
	}
	
	
	/**
	 * Set the isReduct attribute of the GraphicLoop.
	 * @param val The value to the isReduct attribute to set
	 */
	public void setIsReduct(boolean val)
	{
		isReduct = val;
	}
	
	
    /**
     * Set the visible attribute of the GraphicLoop.
     * @param visible The value to the visible attribute to set
     */
    public void setVisible(boolean visible) 
    {	
		super.setVisible(visible);
		
		if(!isReduct)		
			for(Node node : getChildrenArray())
				node.setVisible(visible);
    }   
    
    
	/**
	 * Mask/Unmask all connections inside the GraphicLoop.
	 * @param visible the value to the visible attribute of connections to set
	 */
	public void setConnectionsVisible(boolean visible)
	{
		for(Node node : getChildrenArray())
		{	
			if(node instanceof GraphicLoop && !((GraphicLoop)node).isReduct)
				((GraphicLoop)node).setConnectionsVisible(visible);
			else
			{
				/* Mask/Unmask the sources connections inside the function */
				for(Connection conn : node.getSourceConnections())
					conn.setVisible(visible);
					
				/* Mask/Unmask the targets connections inside the function */
				for(Connection conn : node.getTargetConnections())
					conn.setVisible(visible);
			}
		}
	}
}
