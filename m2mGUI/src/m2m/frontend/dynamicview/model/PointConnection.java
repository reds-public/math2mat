/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a PointConnection.
 *
 * Project:  Math2Mat
 *
 * @file PointConnection.java
 * @author Daniel Molla
 *
 * @date: 09.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a connection point.  
 * 
 */
package m2m.frontend.dynamicview.model;

import m2m.backend.structure.StructTreatment;
import m2m.frontend.view.Editor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.ui.PlatformUI;

public class PointConnection extends Node
{
    /**
     * The source node if the pointConnection is used as an output result.
     */
    private Node outputResult = null;
    /**
     * Indicate if the pointConnection is a constant value.
     */
    private boolean isConstant = false;
    
    
    /**
     * Get the isConstant attribute of the PointConnection.
     * @return the isConstant attribute of the PointConnection
     */
	public boolean getIsConstant()
	{
		return isConstant;
	}
	
	/**
	 * Set the isConstant attribute of the PointConnection.
	 * @param value The value to the PointConnection to set
	 */
	public void setIsConstant(boolean value)
	{
		this.isConstant = value;
	}
    
    
    /**
     * Get the monitor attribute of the element of the PointConnection.
     * @return the monitor attribute of the element of the PointConnection
     */
	public boolean getMonitor()
	{
		return getElement().getMonitor();
	}
	
	
	/**
	 * Set the monitor attribute of the element of the PointConnection.
	 * @param monitor The value to the element of the PointConnection to set
	 */
	public void setMonitor(boolean monitor)
	{ 
		Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		StructTreatment treatment = editor.getM2MProject().getStructTreatment();

		getElement().setMonitor(monitor,treatment);
	}
	
	
	/**
     * Get the outputResul attribut of the PointConnection.
     * @return the outputResul attribut of the PointConnection
     */
	public Node getOutputResult()
	{
		return outputResult;
	}
	
	
	/**
	 * Set the outputResult attribute of the PointConnection.
	 * @param outputResult The value to the utputResult attribute to set
	 */
	public void setOutputResult(Node outputResult)
	{
		this.outputResult = outputResult;
	}
	
	
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{	
		/* Check if the new layout is inside its parent */
		if(!isInParent(layoutTest))
			return false;
		
		/* Check if the new layout is under connection points */
		for(Node node : getParent().getChildrenArray())
		{
			if(node != this && outputResult != node)
				if(!(outputResult instanceof GraphicFunction && outputResult != null && ((GraphicFunction)outputResult).getOutputPointsConnection().contains(node)))
					if(nodeUnderNode(layoutTest, node.getLayout()))
						return false;
			
		}		
		
		return true;
	}
}