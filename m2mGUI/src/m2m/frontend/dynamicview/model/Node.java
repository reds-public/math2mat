/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Model of a Node.
 *
 * Project:  Math2Mat
 *
 * @file Node.java
 * @author Daniel Molla
 *
 * @date: 09.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributes who describe the model of a node. 

 */
package m2m.frontend.dynamicview.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;

import m2m.backend.structure.Element;
import m2m.frontend.view.Editor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;


public abstract class Node
{
	/**
	 * Use to raise an event when the layout is changing.
	 */
    public static final String LAYOUT = "Layout";
	/**
	 * Use to raise an event when the visible attribute is changing.
	 */
    public static final String VISIBLE_COMPONENT = "VisibleComponent";
	/**
	 * Use to raise event for changing the color of the node.
	 */
	public static final String CHANGE_COLOR = "ChangeColor";
	
	
	/**
	 * Name of the node.
	 */
    private String name;
    /**
     * Layout of the node.
     */
    private Rectangle layout;
    /**
     * Saved layouts when a node is reduct.
     */
    private Vector<Rectangle> layoutReduction = new Vector<Rectangle>();
    /**
     * List of children of the node..
     */
    private Vector<Node> children;
    /**
     * Parent node of the node..
     */
    private Node parent;
    /**
     * Indicate if a node is visible.
     */
    private boolean visible = true;
    /**
     *  Properties listeners of the node.
     */
    private PropertyChangeSupport listeners;
    /**
     * List of source connections of the node.
     */
	private Vector<Connection> sourceConnections = new Vector<Connection>();
    /**
     * List of target connections of the node.
     */
	private Vector<Connection> targetConnections = new Vector<Connection>();
    /**
     * Indicate if the color of the node must be changed.
     */
    private boolean colorConditionSelected = false;
    /**
     * Graphical level of the node.
     */
    private int level;
    /**
     * Graphical depth level of the node.
     */
    private int depthLevel;
    /**
     * Structure element of the node.
     */
    private Element element;
	
	
    /**
     * Default contructor of a Node.
     */
    public Node()
    {
        this.name = "Unknown";
        this.layout = new Rectangle(10, 10, 100, 100);
        this.children = new Vector<Node>();
        this.parent = null;
        this.listeners = new PropertyChangeSupport(this);
    }
    
    
    /**
     * Set the graphical level of the node.
     * @param level The level to the node to set
     */
    public void setLevel(int level)
    {
    	this.level = level;
    }
    
    
	/**
	 *  Get the graphical level of the node.
	 * @return the graphical level of the node
	 */
    public int getLevel()
    {
    	return level;
    }
    
    
    /**
     * Set the structure element of the node.
     * @param element The structure element to the node to set
     */
    public void setElement(Element element)
    {
    	this.element = element;
    }
    
    
    /**
     * Get the structure element of the node.
     * @return the structure element of the node
     */
    public Element getElement()
    {
    	return element;
    }
    
    
	/**
	 * Add a connection in the vector of source connections or the vector of target connections of the node.
	 * @param conn The connection to the node to add
	 */
    public void addConnection(Connection conn) 
	{
        if (conn == null || conn.getSource() == conn.getTarget()) 
            throw new IllegalArgumentException();
        if (conn.getSource() == this) 
            sourceConnections.add(conn);
        else if (conn.getTarget() == this) 
            targetConnections.add(conn);     
    }
	
    
	/**
	 * Remove a connection in the vector of source connections or the vector of target connections of the node.
	 * @param conn The connection to the node to remove
	 */
	public void removeConnection(Connection conn) 
	{
        if (conn == null) 
            throw new IllegalArgumentException();
        if (conn.getSource() == this) 
            sourceConnections.remove(conn);
        else if (conn.getTarget() == this) 
            targetConnections.remove(conn);
    }
	
	
	/**
	 * Get the vector of source connections of the node.
	 * @return the vector of source connections of the node
	 */
	public Vector<Connection> getSourceConnections() 
	{
        return sourceConnections;
    }
    
	
	/**
	 * Get the vector of target connections of the node.
	 * @return the vector of targe connections of the node.
	 */
	public Vector<Connection> getTargetConnections()
	{
        return targetConnections;
    }
   
    
	/**
	 * Set the name of the node.
	 * @param name The name to the node to set
	 */
    public void setName(String name) 
    {
        this.name = name;
    }
    
    
    /**
     * Get the name of the node.
     * @return the name of the node
     */
    public String getName() 
    {
        return this.name;
    }
    

    /**
     * Get the visible attribute of the node.
     * @return the visible attribute of the node
     */
    public boolean getVisible() 
    {
        return this.visible;
    }
       
    
    /**
     * Set the visible attribute of the node.
     * @param visible the value to the visible attribute to set
     */
    public void setVisible(boolean visible) 
    {
		boolean odlValue = this.visible;
		this.visible = visible;
		firePropertyChange(Node.VISIBLE_COMPONENT, odlValue, visible);
    }       
       
    
    /**
     * Set the layout of the node.
     * @param newLayout The new layout to the node to set
     */
    public void setLayout(Rectangle newLayout) 
    {
    	Rectangle oldLayout = this.layout;
    	this.layout = newLayout;
        getListeners().firePropertyChange(LAYOUT, oldLayout, newLayout);
    }
    
    
    /**
     * Set the layout reduction of the node.
     * @param newLayout The layout reduction to the node to set
     */
    public void setLayoutReduction(Rectangle newLayout) 
    {
    	layoutReduction.add(0, new Rectangle(layout));
    	this.layout = newLayout;
        getListeners().firePropertyChange(LAYOUT, layoutReduction.firstElement(), newLayout);
    }
    
    
    /**
     * Get the layout of the node.
     * @return the layout of the node
     */
    public Rectangle getLayout() 
    {
    	return this.layout;
    }
    
    
    /**
     * Get the layout reduction of the node.
     * @return the layout reduction of the node
     */
    public Rectangle getLayoutReduction() 
    {
    	return layoutReduction.firstElement();
    }
    
    
    /**
     * Remove the last used layout reduction of the node.
     */
    public void removeLayoutReduction() 
    {
    	layoutReduction.remove(layoutReduction.firstElement());
    }
    
    
    /**
     * Add a child in the vector of children of the node.
     * @param  child The child to the vector to add
     */
    public void addChild(Node child) 
    {
        child.setParent(this);
        this.children.add(child);
    }
    
    
    /**
     * Remove a child in the list of children of the node.
     * @param  child The child to the vector to remove
     */
    public void removeChild(Node child) 
    {
        this.children.remove(child);
    }
    
    
    /**
     * Get the vector of children of the node.
     * @return the vector of children of the node
     */
    public Vector<Node> getChildrenArray() 
    {
        return this.children;
    }
    
    
    /**
     * Set the parent node of the node.
     * @param parent The parent node to the node to set
     */
    public void setParent(Node parent) 
    {
        this.parent = parent;
    }
    
    
    /**
     * Get the parent node of the node.
     * @return the parent node of the node
     */
    public Node getParent() 
    {
        return this.parent;
    }
       
    
    /**
     * Get the color condition selected atribute of the node.
     * @return the color condition selected atribute of the node
     */
	public boolean getColorConditionSelected()
	{
		return colorConditionSelected;
	}
	
	
	/**
	 * Set the color condition selected attribute of the node.
	 * @param colorConditionSelected The value to the condition selected attribute to set
	 */
	public void setColorConditionSelected(boolean colorConditionSelected)
	{
		boolean odlValue = this.colorConditionSelected;
		this.colorConditionSelected = colorConditionSelected;
		firePropertyChange(Node.CHANGE_COLOR, odlValue, colorConditionSelected);	
	}
	
	
	/**
	 * Add a listener in the list of listeners of the node.
	 * @param listener The listener to the node to add
	 */
    public void addPropertyChangeListener(PropertyChangeListener listener) 
    {
        listeners.addPropertyChangeListener(listener);
	}
	  
    
	/**
	 * Remove a listener in the list of listeners of this node.
	 * @param listener listener The listener to the node to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) 
	{
	    listeners.removePropertyChangeListener(listener);
	}
	
    
    /**
     * Get all change property listeners of the node.
     * @return the list of listeners of the node
     */
	public PropertyChangeSupport getListeners() 
	{  
	    return listeners;
	}
	
	
	/**
	 * Raise an event for each listener of the node.
	 * @param property the property to the listeners to raise
	 * @param oldValue the old value to the connection to replace
	 * @param newValue the new value to the connection to set
	 */
	protected void firePropertyChange(String property, Object oldValue, Object newValue) 
	{
		if (listeners.hasListeners(property))
			listeners.firePropertyChange(property, oldValue, newValue);
	}
	
	
	/**
	 * Indicate if the new layout can be set.
	 * @return if the new layout can be set.
	 */
	public boolean CanSetLayout(Rectangle layoutTest)
	{
		/* Check if the new layout is under a node */
		for(Node node : parent.getChildrenArray())
		{
			if(node != this)
				if(nodeUnderNode(layoutTest, node.getLayout()))
					return false;
		}		
		
		return true;
	}
	
	
	/**
	 * Indicate if a layout superposed an other layout.
	 * @param layoutTest The layout to test
	 * @param layoutNode The layout use for the test
	 * @return the result of the control
	 */
	public boolean nodeUnderNode(Rectangle layoutTest, Rectangle layoutNode)
	{
		Rectangle tempLayoutTest = new Rectangle(layoutTest.x-5, layoutTest.y-5, layoutTest.width+10, layoutTest.height+10); 
		Rectangle tempLayoutNode = new Rectangle(layoutNode.x-5, layoutNode.y-5, layoutNode.width+10, layoutNode.height+10);
		
		return tempLayoutTest.intersects(tempLayoutNode);
	}
	
	
	/**
	 * Check if a node layout is in its parent node layout.
	 * @param  layoutTest Layout of the node to check
	 * @return the result of the control
	 */
	public boolean isInParent(Rectangle layoutTest)
	{
		Rectangle parentLayout = parent.getLayout();
		
		if(layoutTest.x < 0 || layoutTest.y < 0)
			return false;
		
		if(!(parent instanceof Schema) && (layoutTest.x+layoutTest.width > parentLayout.width || layoutTest.y+layoutTest.height > parentLayout.height))
			return false;
		
		return true;
	}
	
	/**
	 * Set the depth level of the node.
	 * @return the depth level of the node
	 */
	public void setDepthLevel()
	{
		Node node = this;
		int level = 0;
		
		while(!(node instanceof Schema))
		{
			if(node instanceof InternPointConnection)
				node = node.getParent();
			
			level++;
			node = node.getParent();
		}
		
		depthLevel = level;
	}
	
	
	/**
	 * Get the depth level of the node.
	 * @return the depth level of the node
	 */
	public int getDepthLevel()
	{
		return depthLevel;
	}
	
	
	/**
	 * Set the value of the modified attribute of the project
	 * @param value the value to set to the modified attribute of the project
	 */
	public void setModified(boolean value){
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editorPart instanceof Editor) {
			((Editor)editorPart).setModified(value);
		}
	}
}