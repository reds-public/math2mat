/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Route necessaries connections of the dynamic view.
 *
 * Project:  Math2Mat
 *
 * @file ThreadRouteConnections.java
 * @author Daniel Molla
 *
 * @date: 25.10.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent a thread who route necessaries 
 * connections of the dynamic view.
 * 
 */
package m2m.frontend.dynamicview;

import m2m.frontend.dynamicview.editPart.SchemaPart;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;


public class ThreadRouteConnections implements Runnable
{	
	/**
	 * Method call for running the new thread
	 */
	public void run() 
	{
		/* Get the view and route connections */
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		MyGraphicalEditor graphEditor = ((MyGraphicalEditor)page.findView(MyGraphicalEditor.ID));
		((SchemaPart)(graphEditor.getGraphicalViewer().getRootEditPart().getContents())).Intersections();
	}
}
