/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Abtract edit part.
 *
 * Project:  Math2Mat
 *
 * @file AbstractEditPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all main methods and attributes of edit part. 
 * It also implements differents kinds of mouse events to set the selected node of the property editor.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeListener;
import java.util.List;

import m2m.frontend.dynamicview.model.GraphicOperation;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.Schema;

import org.eclipse.draw2d.*;
import org.eclipse.gef.*;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;


public abstract class AbstractEditPart extends AbstractGraphicalEditPart implements PropertyChangeListener, NodeEditPart, MouseMotionListener, MouseListener
{	
	/**
	 * Activate all source ConnectionEditParts and listeners. 
	 */
    public void activate() 
    {
        super.activate();
        ((Node) getModel()).addPropertyChangeListener(this);
    }

    
    /**
     * Deactivate all source ConnectionEditParts and listeners. 
     */
    public void deactivate()
    {
        super.deactivate();
        ((Node) getModel()).removePropertyChangeListener(this);
    }
    
    
    /**
     * Get the list of source connections of the node.
     * @return the list of source connections of the node.
     */
    @SuppressWarnings("rawtypes")
	protected List getModelSourceConnections() 
	{
		return ((Node) getModel()).getSourceConnections();
	}
	   
	 
    /**
     * Get the list of target connections of the node.
     * @return the list of target connections of the node.
     */
	@SuppressWarnings("rawtypes")
	protected List getModelTargetConnections() 
	{
	    return ((Node) getModel()).getTargetConnections();
	}
	   
	
    /**
     * Get the figure for the source connection anchor
     * @param  connection The source connection 
     * @return the source connection anchor figure
     */ 
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection) 
	{
		if (getModel() instanceof GraphicOperation)
			return new EllipseAnchor(getFigure());
		else
			return new ChopboxAnchor(getFigure());
	}
	  
	
    /**
     * Get the figure for the source connection anchor.
     * @param  request
     * @return the source connection anchor figure
     */ 
	public ConnectionAnchor getSourceConnectionAnchor(Request request) 
	{
		if (getModel() instanceof GraphicOperation)
			return new EllipseAnchor(getFigure());
		else
			return new ChopboxAnchor(getFigure());
	}
	
	
    /**
     * Get the figure for the target connection anchor.
     * @param  connection The target connectionS
     * @return the target connection anchor
     */ 
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection) 
	{
		if (getModel() instanceof GraphicOperation)
			return new EllipseAnchor(getFigure());
		else
			return new ChopboxAnchor(getFigure());
	}
	
	
    /**
     * Get the figure for the target connection anchor.
     * @param  request
     * @return the target connection anchor
     */ 
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(Request request) 
	{
		if (getModel() instanceof GraphicOperation)
			return new EllipseAnchor(getFigure());
		else
			return new ChopboxAnchor(getFigure());
	}
	
	
	/**
	 * Intercept mouse entered event.
	 * @param evt The mouse entered event
	 */
	@Override
	public void mouseEntered(MouseEvent evt) 
	{
		// Get the root editPart
		Schema schema = (Schema)((SchemaPart)getRoot().getContents()).getModel();
		
		/* Change the selected node if the properties aren't lock */
	    if(!schema.getPropertyLock())
	    	schema.setSelectedNode((Node) getModel());			
	}


	/**
	 * Intercept mouse pressed event.
	 * @param evt The mouse pressed event
	 */
	@Override
	public void mousePressed(MouseEvent evt) 
	{
		// Get the node model
		Node node = (Node) getModel();
		
		// Get the root editPart
		Schema schema = (Schema)((SchemaPart)getRoot().getContents()).getModel();
		
		/* Lock/Unlock properties */
		if(node instanceof Schema)
			((Schema)node).setPropertyLock(false);
		else
		    schema.setPropertyLock(true);
		
		// Change the selected node
		schema.setSelectedNode(node);	
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseHover(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void mouseDoubleClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
}