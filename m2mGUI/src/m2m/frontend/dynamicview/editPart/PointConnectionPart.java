/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a PointConnection.
 *
 * Project:  Math2Mat
 *
 * @file InternPointConnectionPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a PointConnection. 
 * It links the model to the figure and intercepts events. 
 * It allows some user editions by implementing edit policies.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.util.Vector;

import m2m.frontend.dynamicview.editpolicies.EditLayoutPolicy;
import m2m.frontend.dynamicview.figure.PointConnectionFigure;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.PointConnection;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;


public class PointConnectionPart extends AbstractEditPart 
{  	
	/**
	 * Create the represented figure the PointConnection model.
	 * @return the represented figure the PointConnection model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new PointConnectionFigure();
        ((PointConnectionFigure) figure).addMouseMotionListener(this);
        ((PointConnectionFigure) figure).addMouseListener(this);
        return figure;
    }

    
    /**
     * Install edit policies in the edit part.
     */
    @Override
    protected void createEditPolicies() 
    {
    	installEditPolicy(EditPolicy.LAYOUT_ROLE, new EditLayoutPolicy()); 
    }
    
    
    /**
     * Refresh the figure with the model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
        PointConnectionFigure figure = (PointConnectionFigure)getFigure();
        PointConnection model = (PointConnection)getModel();
       
    	/* Update the figure with the model */
        figure.setBackgroundColor(model.getColorConditionSelected());
        figure.setLayout(model.getLayout());
        figure.setVisible(model.getVisible());
    }

    
    /**
    /**
     * Get the list of children of the model.
     * @return the list of children of the  model
     */
    public Vector<Node> getModelChildren()
    {
        return ((PointConnection)getModel()).getChildrenArray();
    }
    
    
	/**
	 * Call when a model property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Node.LAYOUT)) 
	    	refreshVisuals(); 
		 else if(evt.getPropertyName().equals(Node.VISIBLE_COMPONENT))
			 refreshVisuals();
		 else if(evt.getPropertyName().equals(Node.CHANGE_COLOR))
			 refreshVisuals();
	}
}