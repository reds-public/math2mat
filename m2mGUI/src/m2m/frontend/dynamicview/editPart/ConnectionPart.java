/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a connection.
 *
 * Project:  Math2Mat
 *
 * @file ConnectionPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a connecion. 
 * It links the model to the figure and intercepts events. 
 * It allows some user editions by implementing edit policies.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import m2m.frontend.dynamicview.ThreadRouteConnections;
import m2m.frontend.dynamicview.editpolicies.EditBendPointPolicy;
import m2m.frontend.dynamicview.model.BendPoint;
import m2m.frontend.dynamicview.model.Connection;
import m2m.frontend.dynamicview.model.GraphicOperation;
import m2m.frontend.dynamicview.model.Schema;

import org.eclipse.draw2d.BendpointConnectionRouter;

import org.eclipse.draw2d.ConnectionEndpointLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RelativeBendpoint;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.swt.widgets.Display;


public class ConnectionPart extends AbstractConnectionEditPart implements PropertyChangeListener
{     
	/**
	 * Create the figure represented the connection model
	 * @return the figure represented the connection model
	 */
	protected IFigure createFigure()
	{
		PolylineConnection connection = (PolylineConnection) super.createFigure();
		
		/* Set a particulaty target decoration if the target model is a GraphicOperation */	
		if(((Connection)getModel()).getTarget() instanceof GraphicOperation)
			connection.setTargetDecoration(new PolygonDecoration());
		
		/* Initialize the style and the router of the connection */	
		connection.setLineStyle(((Connection)getModel()).getLineStyle());
		connection.setConnectionRouter(new BendpointConnectionRouter());
		
		/* Add a label to the source anchor if the source model is a function input connection point */	
	 	if(((Connection)getModel()).getSource().getParent() instanceof Schema)
		{
	 		Label l = new Label();
			ConnectionEndpointLocator cl = new ConnectionEndpointLocator(connection, false);
			cl.setUDistance(-5);
			cl.setVDistance(-10);
			l.setText(((Connection)getModel()).getSource().getName());
			connection.add(l, cl);
		}
	 	/* Add a label to the source anchor if the source model is a function output connection point */	
		else if(((Connection)getModel()).getTarget().getParent() instanceof Schema)
		{
			Label l = new Label();
			ConnectionEndpointLocator cl = new ConnectionEndpointLocator(connection, true);
			cl.setUDistance(-5);
			cl.setVDistance(-10);
			l.setText(((Connection)getModel()).getTarget().getName());
			connection.add(l, cl);
		}
		
		return connection;
	}

	
	/**
	 * Activate all source ConnectionEditParts and listeners. 
	 */
    public void activate() 
    {
        super.activate();
        ((Connection) getModel()).addPropertyChangeListener(this);
    }

    
    /**
     * Deactivate all source ConnectionEditParts and listeners. 
     */
    public void deactivate()
    {
        super.deactivate();
        ((Connection) getModel()).removePropertyChangeListener(this);
    }
	

    /**
     * Refresh the figure of the connection model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
    	IFigure figure = getFigure();
        Connection model = (Connection)getModel();
        
        /* Update the figure with the model */
        figure.setVisible(model.getVisible());
        refreshBendpoints();
    }
    
    
    /**
     * Refresh the bend points of the connection.
     */  
    private void refreshBendpoints() 
    {
    	Vector<RelativeBendpoint> figureConstraint = new Vector<RelativeBendpoint>();
    	
    	if (((Connection) getModel()).getBendPoints() != null) 
    	{
    		/* Add all contained bend points in the model to the connection figure */
	    	for (BendPoint wbp : ((Connection) getModel()).getBendPoints()) 
	    	{
		    	RelativeBendpoint rbp = new RelativeBendpoint(getConnectionFigure());
		    	Dimension first  = new Dimension((Integer) wbp.getWidth()[0],(Integer) wbp.getHeight()[0]);
		    	Dimension second = new Dimension((Integer) wbp.getWidth()[1],(Integer) wbp.getHeight()[1]);
		    	rbp.setRelativeDimensions(first, second);
		    	figureConstraint.add(rbp);
	    	}
	    	getConnectionFigure().setRoutingConstraint(figureConstraint);
    	}
    }
	
	
	/**
	 * Call when a connection property has changed.
	 * @param evt The raised event 
	 */
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Connection.VISIBLE_CONNECTION)) 
			 refreshVisuals();
		 else if (evt.getPropertyName().equals(Connection.BENDPOINT_POSITION)) 
			 refreshVisuals();
		 else if (evt.getPropertyName().equals(Schema.PROPERTY)) 
		 {
			 Schema schema = (Schema)((SchemaPart)getRoot().getContents()).getModel();
			 schema.setSelectedConnection(schema.getSelectedConnection());
		 }		
		 else if (evt.getPropertyName().equals(Connection.ROUTE_CONNECTION)) 
			 Display.getCurrent().asyncExec(new ThreadRouteConnections());
	}
	

	/**
	 * Call when a connection is selected and set the selected connection of the root model.
	 */
	@Override
	public void setSelected(int value)
	{
		super.setSelected(value);
		
		if(value != EditPart.SELECTED_NONE)
		{
			Connection con = (Connection) getModel();
			Schema schema = (Schema)((SchemaPart)getRoot().getContents()).getModel();
			schema.setPropertyLock(true);
			schema.setSelectedConnection(con);
		}
	}
	
	/**
	 * Install all edit policies of the part.
	 */
	@Override
	protected void createEditPolicies() 
	{
		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new EditBendPointPolicy());
	}
}
