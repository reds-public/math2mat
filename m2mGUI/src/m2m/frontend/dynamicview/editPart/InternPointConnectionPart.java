/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of an InternPointConnection.
 *
 * Project:  Math2Mat
 *
 * @file InternPointConnectionPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of an InternPointConnection. 
 * It links the model to the figure and intercepts events. 
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.util.Vector;

import m2m.frontend.dynamicview.figure.InternPointConnectionFigure;
import m2m.frontend.dynamicview.model.InternPointConnection;
import m2m.frontend.dynamicview.model.Node;

import org.eclipse.draw2d.IFigure;


public class InternPointConnectionPart extends AbstractEditPart 
{
	/**
	 * Create the represented figure the InternPointConnection model.
	 * @return the represented figure the InternPointConnection model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new InternPointConnectionFigure();
        ((InternPointConnectionFigure) figure).addMouseMotionListener(this);
        ((InternPointConnectionFigure) figure).addMouseListener(this);
        return figure;
    }
    

    /**
     * Refresh the figure with the model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
    	InternPointConnectionFigure figure = (InternPointConnectionFigure)getFigure();
    	InternPointConnection model = (InternPointConnection)getModel();
       
    	/* Update the figure with the model */
        figure.setLayout(model.getLayout());
        figure.setVisible(model.getVisible());
    }

    
    /**
     * Get the list of children of the model.
     * @return the list of children of the model
     */
    public Vector<Node> getModelChildren()
    {
        return ((InternPointConnection)getModel()).getChildrenArray();
    }
    
    
	/**
	 * Call when a model property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Node.LAYOUT)) 
			 refreshVisuals();
		 else if(evt.getPropertyName().equals(Node.VISIBLE_COMPONENT))
			 refreshVisuals();
	}
	
	
    @Override
    protected void createEditPolicies() {
		// TODO Auto-generated method stub
    }
}
