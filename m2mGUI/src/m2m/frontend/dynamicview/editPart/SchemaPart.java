/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a Schema.
 *
 * Project:  Math2Mat
 *
 * @file SchemaPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a Schema. 
 * It links the model to the figure and intercepts events. 
 * It allows some user editions by implementing edit policies.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import m2m.frontend.dynamicview.editpolicies.EditLayoutPolicy;
import m2m.frontend.dynamicview.figure.SchemaFigure;
import m2m.frontend.dynamicview.model.BendPoint;
import m2m.frontend.dynamicview.model.Connection;
import m2m.frontend.dynamicview.model.GraphicFunction;
import m2m.frontend.dynamicview.model.GraphicOperation;
import m2m.frontend.dynamicview.model.InternPointConnection;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.Schema;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;


public class SchemaPart extends AbstractEditPart
{    
	/**
	 * The represented value of a busy point of the array.
	 */
	private static int BUSY_POINT = -1;
	/**
	 * The represented value of a free point of the array.
	 */
	private static int FREE_POINT = Integer.MAX_VALUE;
	/**
	 * The represented value of a connection busy point of the array.
	 */
	private static int CONNECTION_POINT = Integer.MAX_VALUE-1;
	/**
	 * The represented value of a function busy point of the array.
	 */
	private static int FUNCTION_POINT = Integer.MAX_VALUE-2;
    /**
     * Tab representing the view of the schema.
     */
    private int [][] tabRouter = new int [2000] [2000];   
    /**
     * Tab use to route all connections.
     */
    private int [][] tabTemp;
    
    
	/**
	 * Create the represented figure of the schema model.
	 * @return the represented figure of the schema model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new SchemaFigure();
        ((SchemaFigure) figure).addMouseMotionListener(this);
        ((SchemaFigure) figure).addMouseListener(this);  
        ((Schema)getModel()).setLayout(((SchemaFigure) figure).getBounds());      
        return figure;
    }


    /**
     * Install edit policies in the edit part.
     */
    @Override
    protected void createEditPolicies() 
    {
    	installEditPolicy(EditPolicy.LAYOUT_ROLE, new EditLayoutPolicy()); 
    } 
    
    
    /**
     * Get the list of children of the model.
     * @return the list of children of the model
     */
    public Vector<Node> getModelChildren() 
    {
        return ((Schema)getModel()).getChildrenArray();
    }
    
    
	/**
	 * Call when a model property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Node.LAYOUT)) 
			 refreshVisuals();
	}
	
	
	/**
	 * Route some specific connections.
	 * @param conList The list of specific connection to route
	 */
	private void routeConnections(Vector<Connection> conList)
	{	
		initTabRouter();	
		
		for(Connection con : conList)
		{			
			if(!(con.getSource() instanceof GraphicOperation))
			{
				// Get and convert the oldest parent of the connection
				Rectangle parentAbsoluteLayout = getAbsoluteLayout(getOldParent(con.getSource(), con.getTarget()));
				
				// Initialize the size of the tabTemp
				tabTemp = new int [parentAbsoluteLayout.width][parentAbsoluteLayout.height];
				
				/* Duplicate the interest part of the tabRouter */
				for(int i = 0; i < tabTemp.length; i++)
					for(int j = 0; j < tabTemp[i].length; j++)
						if ((i == 0 || i == tabTemp.length-1) || (j == 0 || j == tabTemp[i].length-1))
							tabTemp[i][j] = -1;
						else
							tabTemp[i][j] = tabRouter[i+parentAbsoluteLayout.x][j+parentAbsoluteLayout.y];
				
				/* Get the start and end layout connection node */
				Rectangle startLayout = getAbsoluteLayout(con.getSource());
				Rectangle endLayout   = getAbsoluteLayout(con.getTarget());
				
				/* Get the start point outside the node source anchor */
				Point startAnchorPoint = getAnchorPoint(new Point(startLayout.x+startLayout.width/2, startLayout.y+startLayout.height/2));
				startAnchorPoint.performTranslate(-parentAbsoluteLayout.x, -parentAbsoluteLayout.y);	
				
				/* Get the end point outside the node target anchor */
				Point endAnchorPoint = getAnchorPoint(new Point(endLayout.x+endLayout.width/2-1, endLayout.y+endLayout.height/2));
				endAnchorPoint.performTranslate(-parentAbsoluteLayout.x, -parentAbsoluteLayout.y);		
				
				// Initialize the start point of the tabTemp
				tabTemp[startAnchorPoint.x] [startAnchorPoint.y] = 1;
				
				/* Use to go through points of the tabTemp */
				LinkedList<Point> pointList = new LinkedList<Point>();
				pointList.add(startAnchorPoint);
				Point point = new Point(0,0);
				Point tempPoint = new Point(0,0);
				int valeurTab;			
				int maxDepth = ((Schema)getModel()).maxDepth;
				int currentDepthSource;
				int currentDepthTarget;
				int maxCurrentDepth;
				
				Iterator<Point> iterPoint = pointList.iterator();
				while (iterPoint.hasNext())
				{
					point.setLocation(iterPoint.next());
					
					/* Go through the four neighbors of the current point */
					for(int i = 0; i < 4; i++)
					{
						switch(i) 
						{
							case 0: tempPoint.setLocation(point.x-1, point.y); break;
							case 1: tempPoint.setLocation(point.x, point.y-1); break;
							case 2: tempPoint.setLocation(point.x+1, point.y); break;
							case 3: tempPoint.setLocation(point.x, point.y+1); break;
						}
						
						valeurTab = tabTemp[tempPoint.x][tempPoint.y];
						
						currentDepthSource = con.getSource().getDepthLevel();
						currentDepthTarget = con.getTarget().getDepthLevel();
						maxCurrentDepth = currentDepthSource > currentDepthTarget ? currentDepthSource : currentDepthTarget;

						if(!(valeurTab >= FUNCTION_POINT-maxDepth && valeurTab <= FUNCTION_POINT-maxCurrentDepth) && valeurTab != BUSY_POINT && valeurTab > tabTemp[point.x][point.y]+1)
						{
							if(tabRouter[tempPoint.x+parentAbsoluteLayout.x][tempPoint.y+parentAbsoluteLayout.y] == CONNECTION_POINT)
							{
								if(valeurTab > tabTemp[point.x][point.y]+10)
								{
									tabTemp[tempPoint.x][tempPoint.y] = tabTemp[point.x][point.y]+10;
									pointList.add(new Point(tempPoint.x, tempPoint.y));
								}
							}
							else
							{
								tabTemp[tempPoint.x][tempPoint.y] = tabTemp[point.x][point.y]+1;
								pointList.add(new Point(tempPoint.x, tempPoint.y));
							}
						}
					}	
							
					/* Update the list of points ans its iterator */
					pointList.remove(pointList.indexOf(point));
					iterPoint = pointList.iterator();	
				}
				
				// Find the best way of the connection 
				findBestWay(endAnchorPoint, startAnchorPoint,
						    new Point(endLayout.x  +endLayout.width/2-1, endLayout.y + endLayout.height/2),
						    new Point(startLayout.x+startLayout.width/2, startLayout.y + startLayout.height/2),
						    con);
			}
		}
	}
	
	
	/**
	 * Initialisation of the tabRouter.
	 */
	private void initTabRouter()
	{
		Schema schema = (Schema)getModel();
		Node node;
		Rectangle rectTemp = new Rectangle(0,0,0,0);
		                           
		/* Initialize all the cell of tabRouter with Integer.MAX_VALUE and border with -1 */
		for(int i = 0; i < tabRouter.length; i++)
			for(int j = 0; j < tabRouter[i].length; j++)
				if ((i == 0 || i == tabRouter.length-1) || (j == 0 || j == tabRouter[i].length-1))
					tabRouter[i][j] = BUSY_POINT;    
				else
					tabRouter[i][j] = FREE_POINT;                       
		
		LinkedList<Node> nodeList = new LinkedList<Node>();
		nodeList.addAll(schema.getChildrenArray());
		Iterator<Node> iterNode = nodeList.iterator();
		
		/* Initialize the tabRouter with actually node positions */
		while (iterNode.hasNext()) 
		{
			node = iterNode.next();
			
			if(node.getVisible())
			{
				/* Check if the node has children */
				if(node.getChildrenArray().size() != 0)	
					nodeList.addAll(node.getChildrenArray());
				
				/* Get the absolute layout of the node */
				rectTemp = getAbsoluteLayout(node);
				
				if (!(node instanceof GraphicFunction))
				{	
					/* Initialize the tabRouter with the node position */
					for(int i = rectTemp.x-5; i < rectTemp.x+rectTemp.width+5; i++)
						for(int j = rectTemp.y-5; j < rectTemp.y+rectTemp.height+5; j++)
							tabRouter[i][j] = BUSY_POINT;
				}
				else
				{
					int depthLevel = node.getDepthLevel();
					
					/* Initialize the tabRouter with verctical borders of function position */
					for(int i = rectTemp.x-5; i <= rectTemp.x+rectTemp.width+5; i++)
					{		
						tabRouter[i][rectTemp.y-5] = FUNCTION_POINT-depthLevel;
						tabRouter[i][rectTemp.y+rectTemp.height+5] = FUNCTION_POINT-depthLevel;
					}
					
					/* Initialize the tabRouter with horizontal borders of function position */
					for(int j = rectTemp.y-5; j <= rectTemp.y+rectTemp.height+5; j++)
					{
						tabRouter[rectTemp.x-5][j] = FUNCTION_POINT-depthLevel;
						tabRouter[rectTemp.x+rectTemp.width+5][j] = FUNCTION_POINT-depthLevel;
					}
				}
			}
			
			nodeList.remove(nodeList.indexOf(node));
			iterNode = nodeList.iterator();	
		}
	}
	
	
	/**
	 * Get the absolute layout of a node.
	 * @param node The node containing the layout to get
	 * @return the absolute layout
	 */
	private Rectangle getAbsoluteLayout(Node node)
	{
		Rectangle layout = new Rectangle (0, 0, node.getLayout().width, node.getLayout().height);
		Node nodeTemp = node;
		
		/* Go through all parents of the node */
		while(!(nodeTemp instanceof Schema))
		{
			layout.x += nodeTemp.getLayout().x;
			layout.y += nodeTemp.getLayout().y;
			nodeTemp = nodeTemp.getParent();
		}
		
		return layout;
	}
	
	
	/**
	 * Get the nearly anchor point of a layout.
	 * @param layout The node containing the anchor point to get
	 * @return the nearly anchor point
	 */
	private Point getAnchorPoint(Point point)
	{
		/* Create and initialize an array containing the four neighbors of the start point */
		Point[] tabPoint = new Point[4];
		tabPoint[0] = new Point(point.x-1, point.y);
		tabPoint[1] = new Point(point.x, point.y-1);
		tabPoint[2] = new Point(point.x+1, point.y);
		tabPoint[3] = new Point(point.x, point.y+1);

		while(true)
		{
			/* Go through the four neighbors */
			for(int i = 0; i < tabPoint.length; i++)
			{
				/* Check if the anchor point is found */
				if(tabRouter[tabPoint[i].x] [tabPoint[i].y]  != -1)
					return tabPoint[i];
				
				else
					/* Get the four next neighbors of the start point */
					switch(i) 
					{
						case 0: tabPoint[i].x--; break;
						case 1: tabPoint[i].y--; break;
						case 2: tabPoint[i].x++; break;
						case 3: tabPoint[i].y++; break;
					}
			}
		}
	}
	
	
	/**
	 * Find the best way for a connection.
	 * @param endAnchorPoint   The end anchor point of the connection
	 * @param startAnchorPoint The start anchor point of the connection
	 * @param endPoint The end point of the connection
	 * @param startPoint The start point of the connection
	 * @param con The connection
	 */
	private void findBestWay(Point endAnchorPoint, Point startAnchorPoint, Point endPoint, Point startPoint, Connection con)
	{
		Rectangle parentAbsoluteLayout = getAbsoluteLayout(getOldParent(con.getSource(), con.getTarget()));
	
		/* Use to go through all points */
		Point currentPoint = new Point(endAnchorPoint);
		Point nextPoint = new Point(0,0);
		Point tempPoint = new Point(0,0);
		
		/* Convert start and end anchor point with the absolute parent layout */
		startAnchorPoint.performTranslate(parentAbsoluteLayout.x, parentAbsoluteLayout.y);	
		endAnchorPoint.performTranslate(parentAbsoluteLayout.x, parentAbsoluteLayout.y);
		
		int minValue = tabTemp[currentPoint.x][currentPoint.y];		
		int temSens = 0;
		int sens = 0;
		
		// Get the sens of the end point
		sens = getSens(endPoint, endAnchorPoint);
		
		// Remove alle bend points of the connection
		con.getBendPoints().removeAll(con.getBendPoints());
		
		while(minValue != 1)
		{
			/* Get the neighbor who represent the least value */
			for(int i = 0; i < 4; i++)
			{
				switch(i) 
				{
					case 0: tempPoint.setLocation(currentPoint.x-1, currentPoint.y); break;
					case 1: tempPoint.setLocation(currentPoint.x, currentPoint.y-1); break;
					case 2: tempPoint.setLocation(currentPoint.x+1, currentPoint.y); break;
					case 3: tempPoint.setLocation(currentPoint.x, currentPoint.y+1); break;
				}
				
				if(tabTemp[tempPoint.x][tempPoint.y] < minValue && tabTemp[tempPoint.x][tempPoint.y] != -1)
				{
					minValue = tabTemp[tempPoint.x][tempPoint.y];
					nextPoint.setLocation(tempPoint);
					temSens = i;
				}
			}				
			
			/* Set all busy the point of the connection */
			tabRouter[nextPoint.x+parentAbsoluteLayout.x][nextPoint.y+parentAbsoluteLayout.y] = CONNECTION_POINT;
			for(int i = 1; i < 6; i++)
				for(int j = 0; j < 7; j++)
				{
					switch(j) 
					{
						case 0: tempPoint.setLocation(nextPoint.x-i, nextPoint.y);   break;
						case 1: tempPoint.setLocation(nextPoint.x,   nextPoint.y-i); break;
						case 2: tempPoint.setLocation(nextPoint.x+i, nextPoint.y);   break;
						case 3: tempPoint.setLocation(nextPoint.x,   nextPoint.y+i); break;
						case 4: tempPoint.setLocation(nextPoint.x-i, nextPoint.y+i); break;
						case 5: tempPoint.setLocation(nextPoint.x-i, nextPoint.y-i); break;
						case 6: tempPoint.setLocation(nextPoint.x+i, nextPoint.y-i); break;
						case 7: tempPoint.setLocation(nextPoint.x+i, nextPoint.y+i); break;
					}
					tempPoint.performTranslate(parentAbsoluteLayout.x, parentAbsoluteLayout.y);
					if(tempPoint.x < tabRouter.length  && tempPoint.y < tabRouter[0].length && tabRouter[tempPoint.x][tempPoint.y] != -1 && tabRouter[tempPoint.x][tempPoint.y] != FUNCTION_POINT)
						tabRouter[tempPoint.x][tempPoint.y] = CONNECTION_POINT;
				}

			
			/* Add a bendPoint if the sens is different */
			if(sens != temSens)
			{			
				BendPoint bp = new BendPoint();
				currentPoint.performTranslate(parentAbsoluteLayout.x, parentAbsoluteLayout.y);
				bp.setHeight(currentPoint.getDifference(startPoint).height, currentPoint.getDifference(endPoint).height);
				bp.setWidth(currentPoint.getDifference(startPoint).width, currentPoint.getDifference(endPoint).width);
				
				bp.setConnection(con);		
				bp.setIndex(0);
				con.addBendPoint(bp);
				
				sens = temSens;
			}			
			currentPoint.setLocation(nextPoint);
		}
		
		// Get the sens of the start point
		sens = getSens(startPoint, startAnchorPoint);
		
		/* Add the last bendPoint if the sens is different */
		if(sens != temSens)
		{			
			BendPoint bp = new BendPoint();
			currentPoint.performTranslate(parentAbsoluteLayout.x, parentAbsoluteLayout.y);
			bp.setHeight(currentPoint.getDifference(startPoint).height, currentPoint.getDifference(endPoint).height);
			bp.setWidth(currentPoint.getDifference(startPoint).width, currentPoint.getDifference(endPoint).width);
			
			bp.setConnection(con);		
			bp.setIndex(0);
			con.addBendPoint(bp);
			
			sens = temSens;
		}	
	}
	
	
	/**
	 * Route with constraints connections with intersections
	 */
	public void Intersections()
	{
		// Vector of connections who must be route
		Vector<Connection> routeConList = new Vector<Connection>();
		
		/* Use to go through all connections and nodes */
		Vector<Connection> conList = new Vector<Connection>();
		Vector<Node> nodeList = new Vector<Node>();
		
		/* Use to get all nodes and connections */
		Node tempNode;
		LinkedList<Node> tempNodeList = new LinkedList<Node>();
		Iterator<Node> iterNode;
		
		/* Initialize node lists and the node iterator */
		nodeList.addAll(((Schema)getModel()).getChildrenArray());
		tempNodeList.addAll(((Schema)getModel()).getChildrenArray());
		iterNode = tempNodeList.iterator();
			
		/* Get all nodes and connections inside the schema */
		while (iterNode.hasNext()) 
		{
			tempNode = iterNode.next();
			
			/* Check if the node has children */
			if(tempNode.getChildrenArray().size() != 0)	
			{
				nodeList.addAll(tempNode.getChildrenArray());
				tempNodeList.addAll(tempNode.getChildrenArray());
			}
			
			conList.addAll(tempNode.getSourceConnections());			
			tempNodeList.remove(tempNodeList.indexOf(tempNode));
			iterNode = tempNodeList.iterator();	
		}
		
		
		/* Go through all nodes and connections */
		for(Connection con : conList)
		{				
			if(con.getMustBeRoute())
			{
				for(Node node : nodeList)
				{				
					/* Check if there are intersections between some nodes and connections */
					if(node != con.getSource() && node != con.getTarget() && node.getVisible() && con.getTarget().getVisible() && con.getSource().getVisible() && con.getVisible())	
					{
						if(node instanceof GraphicFunction)
						{
							if(node.getDepthLevel() >= con.getSource().getDepthLevel() && node.getDepthLevel() >= con.getTarget().getDepthLevel())
								if(connectionNodeIntersection(con, getAbsoluteLayout(node)) && !routeConList.contains(con))
									routeConList.add(con);
						}
						else if(connectionNodeIntersection(con, getAbsoluteLayout(node)) && !routeConList.contains(con))
							routeConList.add(con);
					}
				}
				
				if(!routeConList.contains(con))
					con.removeAllBendPoint();		
			}
		}		
		
		routeConnections(routeConList);
	}
	
	
	/**
	 * Check if there is an intersection between a connection and a layout node.
	 * @param con The connection to be tested
	 * @param layout The layout to be tested
	 * @return true if there is an intersection between the connection and the node, else false
	 */
	private boolean connectionNodeIntersection(Connection con, Rectangle layout)
	{
		/* Get the start and the end absolute layout of the connection */
		Rectangle startNodeLayout = getAbsoluteLayout(con.getSource());
		Rectangle endNodeLayout   = getAbsoluteLayout(con.getTarget());
		
		/* Get the start and the end anchor point of the connection */
		Point A = new Point(startNodeLayout.x+startNodeLayout.width/2, startNodeLayout.y+startNodeLayout.height/2);
		Point B = new Point(endNodeLayout.x+endNodeLayout.width/2, endNodeLayout.y+endNodeLayout.height/2);
		
		/* Check each segment of the node layout */
		if(segmentIntersection(A, B, new Point(layout.x, layout.y), new Point(layout.x, layout.y+layout.height)))
			if(!(con.getTarget() instanceof InternPointConnection))
				return true;
		if(segmentIntersection(A, B, new Point(layout.x, layout.y),  new Point(layout.x+layout.width, layout.y)))
			return true;
		if(segmentIntersection(A, B, new Point(layout.x, layout.y+layout.height),  new Point(layout.x+layout.width, layout.y+layout.height)))
			return true;
		if(segmentIntersection(A, B, new Point(layout.x+layout.width, layout.y),  new Point(layout.x+layout.width, layout.y+layout.height)))
			if(!(con.getSource() instanceof InternPointConnection))
				return true;
		
		return false;
	}
	
	
	/**
	 * Check if there is an intersection between two segments.
	 * @param a1 The first point of the first segment
	 * @param a2 The second point of the first segment
	 * @param b1 The first point of the second segment
	 * @param b2 The second point of the second segment
	 * @return true if there is an intersection between both segment, else false 
	 */
	private boolean segmentIntersection(Point a1, Point a2, Point b1, Point b2)
	{
		int d = (a1.x-a2.x)*(b1.y-b2.y) - (a1.y-a2.y)*(b1.x-b2.x);
		if (d == 0) 
			return false;
		
		/* Calculate the intersection point */
		int xi = ((b1.x-b2.x)*(a1.x*a2.y-a1.y*a2.x)-(a1.x-a2.x)*(b1.x*b2.y-b1.y*b2.x))/d;
		int yi = ((b1.y-b2.y)*(a1.x*a2.y-a1.y*a2.x)-(a1.y-a2.y)*(b1.x*b2.y-b1.y*b2.x))/d;		 
	
		/* Check if the intersection point is on segments */
		if (xi < Math.min(a1.x,a2.x) || xi > Math.max(a1.x,a2.x))
			return false;
		if (xi < Math.min(b1.x,b2.x) || xi > Math.max(b1.x,b2.x))
			return false;
		if (yi < Math.min(a1.y,a2.y) || yi > Math.max(a1.y,a2.y))
			return false;
		if (yi < Math.min(b1.y,b2.y) || yi > Math.max(b1.y,b2.y))
			return false;
		
		return true;
	}

	
	/**
	 * Get the sens of a point.
	 * @param point The point to be evaluate
	 * @return the sens of the point
	 */
	private int getSens(Point point, Point anchorPoint)
	{
		if(Math.abs(point.x-anchorPoint.x) > 4)
			if(point.x > anchorPoint.x)
				return 0;
			else
				return 2;
		else if(Math.abs(point.y-anchorPoint.y) > 4)
			if(point.y > anchorPoint.y)
				return 1;
			else
				return 3;
		
		return -1;
	}
	
	
	/**
	 * Get the oldest parent layout between a source node and a target node.
	 * @param  nodeSource The source node 
	 * @param  nodeTarget The target node
	 * @return the oldest parent layout
	 */
	private Node getOldParent(Node nodeSource, Node nodeTarget)
	{
		Rectangle parentSourceLayout = getAbsoluteLayout(nodeSource.getParent());
		Rectangle parentTargetLayout = getAbsoluteLayout(nodeTarget.getParent());
		
		return parentSourceLayout.contains(parentTargetLayout) ? nodeSource.getParent()  : nodeTarget.getParent();
	}
	
	
	/**
	 * Get the tabRouter of the edit part.
	 * @return the tabRouter of the edit part.
	 */
	public int [][] getTabRouter()
	{
		return tabRouter;
	}
}