/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a GraphicFor.
 *
 * Project:  Math2Mat
 *
 * @file LoopPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a GraphicLoop. 
 * It's a sub-class of FunctionPart. It redifines two methods for creating and refreshing figures.  
 * 
 */
package m2m.frontend.dynamicview.editPart;

import m2m.frontend.dynamicview.figure.FunctionFigure;
import m2m.frontend.dynamicview.model.GraphicLoop;

import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.IFigure;


public class LoopPart extends FunctionPart implements ActionListener
{
	/**
	 * Create the represented figure of the GraphicLoop model
	 * @return the represented figure of the GraphicLoop model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new FunctionFigure();
        ((FunctionFigure) figure).getBtnMinimize().addActionListener(this);
        ((FunctionFigure) figure).addMouseMotionListener(this);
        ((FunctionFigure) figure).addMouseListener(this);
        return figure;
    }
    
    
    /**
     * Refresh the figure with the model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
    	FunctionFigure figure = (FunctionFigure)getFigure();
        GraphicLoop model = (GraphicLoop)getModel();

        /* Update the figure with the model */
        figure.setName(model.getName());
        figure.setLayout(model.getLayout());
        figure.setVisible(model.getVisible());
    }    
}
