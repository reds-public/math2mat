/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of the property view.
 *
 * Project:  Math2Mat
 *
 * @file PropertyPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of the property view. 
 * It links the model to the figure and intercepts events. 
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import m2m.frontend.dynamicview.figure.PropertyFigure;
import m2m.frontend.dynamicview.model.GraphicIf;
import m2m.frontend.dynamicview.model.GraphicLoopIterator;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.Schema;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;


public class PropertyPart extends AbstractGraphicalEditPart implements PropertyChangeListener
{
	/**
	 * Activate all source ConnectionEditParts and listeners. 
	 */
    public void activate() 
    {
        super.activate();
        ((Node) getModel()).addPropertyChangeListener(this);
    }

    
	/**
	 * Desactivate all source ConnectionEditParts and listeners. 
	 */
    public void deactivate()
    {
        super.deactivate();
        ((Node) getModel()).removePropertyChangeListener(this);
    }
    
    
	/**
	 * Create the represented figure of the schema property model.
	 * @return the represented figure of the schema property model
	 */
	@Override
	protected IFigure createFigure() 
	{
        IFigure figure = new PropertyFigure((Schema)getModel());
        return figure;
	}

	
    /**
     * Refresh the figure with the schema property model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
    	PropertyFigure figure = (PropertyFigure)getFigure();
        Schema model = (Schema)getModel();
        
        /* Set the property of the selected node */
        if(model.getSelectedNode() != null)
        	figure.setProperty(model.getSelectedNode()); 
        /* Set the property of the selected connection */
        else if(model.getSelectedConnection() != null)
        	figure.setProperty(model.getSelectedConnection()); 
        
        /* Set the default color of conditions points selection of a GraphicIf */
        if(model.getLastIfSelected() != null)
        {
	        for(Node node : model.getLastIfSelected().getConditionNodes())
	        	node.setColorConditionSelected(false);
	        model.setLastIfSelected(null);	
        }
        
        /* Change the color of conditions points selection if a GraphicIf is selected */
        if(model.getSelectedNode() instanceof GraphicIf)
        {
        	model.setLastIfSelected((GraphicIf)model.getSelectedNode());		        
        	for(Node node : ((GraphicIf)model.getSelectedNode()).getConditionNodes())
	        	node.setColorConditionSelected(true);
        }
        
        /* Set the default color of conditions points selection of a GraphicLoopIterator */
        if(model.getLastloopIteratorSelected() != null)
        {
        	for(Node node : model.getLastloopIteratorSelected().getConditionNodes())
	        	node.setColorConditionSelected(false);
	        model.setLastLoopIteratorSelected(null);	
        }
        
        /* Change the color of condition point selection if a GraphicLoopIterator is selected */
        if(model.getSelectedNode() instanceof GraphicLoopIterator)
        {
        	model.setLastLoopIteratorSelected((GraphicLoopIterator)model.getSelectedNode());		        
        	for(Node node : ((GraphicLoopIterator)model.getSelectedNode()).getConditionNodes())
	        	node.setColorConditionSelected(true);
        }
    }
    

	/**
	 * Call when a function property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Schema.PROPERTY)) 
			 refreshVisuals();
	}
	
	
	@Override
	protected void createEditPolicies() 
	{
		// TODO Auto-generated method stub		
	}
}
