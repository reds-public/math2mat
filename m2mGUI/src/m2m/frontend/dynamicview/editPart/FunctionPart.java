/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a GraphicFunction.
 *
 * Project:  Math2Mat
 *
 * @file FunctionPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a GraphicFunction. 
 * It links the model to the figure and intercepts events. 
 * It allows some user editions by implementing edit policies.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.beans.PropertyChangeEvent;
import java.util.Vector;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import m2m.backend.structure.Function;
import m2m.frontend.dynamicview.ThreadRouteConnections;
import m2m.frontend.dynamicview.editpolicies.EditLayoutPolicy;
import m2m.frontend.dynamicview.figure.FunctionFigure;
import m2m.frontend.dynamicview.figure.PointConnectionFigure;
import m2m.frontend.dynamicview.model.Connection;
import m2m.frontend.dynamicview.model.GraphicFunction;
import m2m.frontend.dynamicview.model.GraphicLoop;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.PointConnection;
import m2m.frontend.view.Editor;



public class FunctionPart extends AbstractEditPart implements ActionListener
{
	/**
	 * Define the y position of input connection points when the function is reducted.
	 */
	private int redInY;
	/**
	 * Define the y position of output connection points when the function is reducted.
	 */
	private int redOutY;
	
	
	/**
	 * Create the represented figure the GraphicFunction model.
	 * @return the represented figure the GraphicFunction model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new FunctionFigure();
        ((FunctionFigure) figure).getBtnMinimize().addActionListener(this);
        ((FunctionFigure) figure).addMouseMotionListener(this);
        ((FunctionFigure) figure).addMouseListener(this);
        return figure;
    }

    
    /**
     * Install edit policies in the edit part.
     */
    @Override
    protected void createEditPolicies() 
    {
    	installEditPolicy(EditPolicy.LAYOUT_ROLE, new EditLayoutPolicy()); 
    }
    
    
    /**
     * Refresh the figure with the model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
        FunctionFigure figure = (FunctionFigure)getFigure();
        GraphicFunction model = (GraphicFunction)getModel();

        /* Update the figure with the model */
        figure.setName(model.getName());
        figure.setLayout(model.getLayout());
        figure.setVisible(model.getVisible());
    }

    
    /**
     * Get the list of children of the model.
     * @return the list of children of the model
     */
    public Vector<Node> getModelChildren()
    {
        return ((GraphicFunction)getModel()).getChildrenArray();
    }
    
    
    
	/**
	 * Call when a model property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Node.LAYOUT)) 
			 refreshVisuals();
		 else if(evt.getPropertyName().equals(Node.VISIBLE_COMPONENT))
			 refreshVisuals();
	}


	/**
	 * Call when we clic on the function button reduction.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{		
		((GraphicFunction)getModel()).setNotReduct(!((GraphicFunction)getModel()).getNotReduct());
		((GraphicFunction)getModel()).setNotReduct(reductionWindow(((GraphicFunction)getModel()).getNotReduct()));
	}
	
	
	/**
	 * Minimize/Maximize the layout of a function and mask/unmask nodes and connections inside this one. 
	 * @param  visible Indicate a minimization or maximization of the function
	 * @return the succes of the minimization or maximization
	 */
	public boolean reductionWindow(boolean visible)
	{
    	int x;
    	int y;	
    	
		/* Initialize the y reduction position of input and output connection point */
		redInY  = FunctionFigure.SPACE_REDUCTION;
		redOutY = FunctionFigure.SPACE_REDUCTION;
		int maxInOut;
			
		// Get the function model
        GraphicFunction model = (GraphicFunction)getModel();
        
        // Check if the function can be maximize
        if(!visible || (visible && model.CanSetLayout(new Rectangle(model.getLayout().x, model.getLayout().y, model.getLayoutReduction().width, model.getLayoutReduction().height))))
        {		
        	Vector<Node> nodes = new Vector<Node>();
        	
        	/* Reverse the children vector of the function */
			for(Node node : model.getChildrenArray()) 
				nodes.add(0, node);
			
			for(Node node : nodes)
			{
				/* Mask/Unmask the nodes who has no connections outside the function */
				if(!hasExternConnection(node, visible))
					node.setVisible(visible);
				
				if(node instanceof GraphicLoop && !((GraphicLoop)node).getIsReduct())
					((GraphicLoop)node).setConnectionsVisible(visible);
				else
				{
					/* Mask/Unmask the sources connections inside the function */
					for(Connection conn : node.getSourceConnections())
						if((children.contains(conn.getTarget()) || !(conn.getTarget().getParent() == conn.getSource().getParent().getParent())))
							conn.setVisible(visible);
						
					/* Mask/Unmask the targets connections inside the function */
					for(Connection conn : node.getTargetConnections())
						if( (children.contains(conn.getSource()) || !(conn.getSource().getParent() == conn.getTarget().getParent().getParent())))
							conn.setVisible(visible);
				}
			}		
			
			int space = 1;
			
			/* Set the layout of the output connection points and the function */
			if(!visible)
			{				
				for(PointConnection pc : model.getOutputPointsConnection())
				{
		    		x =  model.getLayout().x + model.getReductSizeWidth() + 15;
		    		y =  model.getLayout().y + FunctionFigure.SPACE_REDUCTION*space + 2;	    		
		    		pc.setLayoutReduction(new Rectangle(x, y, 10, 10));
		    		space++;
				}
				maxInOut = Math.max(model.getOutputPointsConnection().size(), ((Function)model.getElement()).getInput().size())+1;
				model.setSize(model.getReductSizeWidth(), maxInOut*FunctionFigure.SPACE_REDUCTION+model.getReductSizeHeight());	
			}		
			else
			{
    			for(PointConnection pc : model.getOutputPointsConnection())
				{
					x =  pc.getLayoutReduction().x + model.getLayout().x - model.getLayoutReduction().x;
		    		y =  pc.getLayoutReduction().y + model.getLayout().y - model.getLayoutReduction().y;
	    			pc.setLayout(new Rectangle(x, y, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
	    			pc.removeLayoutReduction();
				}	
    			model.setSize(model.getFullSizeWidth(), model.getFullSizeHeight());	
			}
			
			if(model instanceof GraphicLoop)
				((GraphicLoop)model).setIsReduct(!((GraphicLoop)model).getIsReduct());
			
			// Route Connections
			Display.getCurrent().asyncExec(new ThreadRouteConnections());
			return visible;
        }
       
        Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
	    MessageDialog.openError(editor.getSite().getShell(), "Error", "This action is not allowed!");
	    
        return !visible;	
	}
	
	
	/**
	 * Check if a node has connection outside the function layout. If it's the case,
	 * change the layout of the node.
	 * @param node The tested node
	 * @param visible Indicate a minimization or maximisation of the functon
	 * @return True if the node has extern connection, else false
	 */
	public boolean hasExternConnection(Node node, boolean visible)
	{
		// Get the list of children of the function
		Vector<Node> children = ((GraphicFunction) getModel()).getChildrenArray();	
				
		/* Check if the node has extern source connection */
		for(Connection conn : node.getSourceConnections())
		{
			if(!children.contains(conn.getTarget()) && (conn.getTarget().getParent() == conn.getSource().getParent().getParent()))
			{
				/* Set the new layout of the PointConnection */
				if(!visible)
				{
					node.setLayoutReduction(new Rectangle(((GraphicFunction) getModel()).getReductSizeWidth()-PointConnectionFigure.WIDTH-12, redOutY, 10, 10));				
					redOutY += FunctionFigure.SPACE_REDUCTION;
				}
				else
				{
					node.setLayout(node.getLayoutReduction());		
					node.removeLayoutReduction();
				}
				return true;
			}
		}
		
		/* Check if the node has extern target connection */
		for(Connection conn : node.getTargetConnections())
		{
			if(!children.contains(conn.getSource()) && (conn.getSource().getParent() == conn.getTarget().getParent().getParent()))
			{
				/* Set the new layout of the PointConnection */
				if(!visible)
				{
					node.setLayoutReduction(new Rectangle(0, redInY, 10, 10));				
					redInY += FunctionFigure.SPACE_REDUCTION;
				}
				else
				{
					node.setLayout(node.getLayoutReduction());		
					node.removeLayoutReduction();
				}
				return true;
			}
		}
		
		return false;
	}
}



