/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part factory of the dynamic view.
 *
 * Project:  Math2Mat
 *
 * @file PropertyEditPartFactory.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part factory of the dynamic view. 
 * It links node edit parts with the node models.  
 * 
 */
package m2m.frontend.dynamicview.editPart;

import m2m.frontend.dynamicview.model.*;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;


public class SchemaEditPartFactory implements EditPartFactory 
{
	/**
	 * Creation of editParts and associations with models.
	 * @param  context (Not use)
	 * @param  model The model of tha ssociation
	 * @return the created editPart 
	 */
    public EditPart createEditPart(EditPart context, Object model) 
    {
        AbstractGraphicalEditPart part = null;  
        
        if (model instanceof Schema) 
            part = new SchemaPart();       
        else if(model instanceof PointConnection)
        	part = new PointConnectionPart();       
        else if(model instanceof GraphicLoop)
        	part = new LoopPart();       
        else if(model instanceof GraphicFunction)
        	part = new FunctionPart();      
        else if(model instanceof GraphicOperation)
        	part = new OperationPart();       
        else if(model instanceof Connection)
        	part = new ConnectionPart();        
        else if(model instanceof GraphicIf)
        	part = new IfPart();
        else if(model instanceof GraphicLoopIterator)
        	part = new LoopIteratorPart();
        else if(model instanceof InternPointConnection)
        	part = new InternPointConnectionPart();    

        part.setModel(model);
        return part;
    }
}