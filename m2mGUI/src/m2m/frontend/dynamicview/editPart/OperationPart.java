/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit part of a GraphicOperation.
 *
 * Project:  Math2Mat
 *
 * @file InternPointConnectionPart.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit part of a GraphicOperation. 
 * It links the model to the figure and intercepts events. 
 * It allows some user editions by implementing edit policies.
 * 
 */
package m2m.frontend.dynamicview.editPart;

import java.util.Vector;

import java.beans.PropertyChangeEvent;

import m2m.frontend.dynamicview.editpolicies.EditLayoutPolicy;
import m2m.frontend.dynamicview.figure.OperationFigure;
import m2m.frontend.dynamicview.model.GraphicOperation;
import m2m.frontend.dynamicview.model.Node;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;


public class OperationPart extends AbstractEditPart 
{
	/**
	 * Create the represented figure the GraphicOperation model.
	 * @return the represented figure the GraphicOperation model
	 */
    @Override
    protected IFigure createFigure() 
    {
        IFigure figure = new OperationFigure();
        ((OperationFigure) figure).addMouseMotionListener(this);
        ((OperationFigure) figure).addMouseListener(this);
        return figure;
    }

    
    /**
     * Install edit policies in the edit part.
     */
    @Override
    protected void createEditPolicies() 
    {
    	installEditPolicy(EditPolicy.LAYOUT_ROLE, new EditLayoutPolicy()); 
    }
    
    
    /**
     * Refresh the figure with the function model.
     */
    protected void refreshVisuals()
    { 
    	/* Get the figure and the model */
    	OperationFigure figure = (OperationFigure)getFigure();
    	GraphicOperation model = (GraphicOperation)getModel();
    	
    	/* Update the figure with the model */
        figure.setName(model.getOperatorName());
        figure.setLayout(model.getLayout());
        figure.setVisible(model.getVisible());
    }

    
    /**
     * Get the list of children of the model.
     * @return the list of children of the  model
     */
    public Vector<Node> getModelChildren()
    {
        return ((GraphicOperation)getModel()).getChildrenArray();
    }

    
	/**
	 * Call when a model property has changed.
	 * @param evt The raised event
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		 if (evt.getPropertyName().equals(Node.LAYOUT)) 
	    	refreshVisuals();  	
		 else if(evt.getPropertyName().equals(Node.VISIBLE_COMPONENT))
			 refreshVisuals();
	}
}