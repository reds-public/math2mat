/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Represent the graphical model of the structure.
 *
 * Project:  Math2Mat
 *
 * @file Model.java
 * @author Daniel Molla
 *
 * @version 1.0
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all attributs and methods allowing the construction of the graphical model.
 *
 */
package m2m.frontend.dynamicview;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import m2m.backend.project.M2MProject;
import m2m.backend.structure.*;
import m2m.frontend.dynamicview.figure.*;
import m2m.frontend.dynamicview.model.*;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;



public class Model 
{
	private Schema schema;
	
	/**
	 * Space between input and output node of a GraphicFunction.
	 */
	public static final int SPACE = 50;
	/**
	 * Space between nodes of a same level.
	 */
	public static final int SPACE_INTERN_BLOC = 40;
	
	
	/**
	 * Constructor of a Model.
	 * @param treatment  The structure to be represented
	 * @param m2mProject The project corresponding to this schema
	 */
	public Model(StructTreatment treatment, M2MProject m2mProject)
	{	
		/* Create the instance of Schema */
		schema = new Schema();
		schema.setName("Prototype Polynome");
		schema.setLayout(new Rectangle(0, 0, 1000, 1000));
		schema.setproject(m2mProject);
		
		// Creation of the main function
		createFunction((Function) treatment.getTop(), schema);
		
		// Set the max depth of the schema
		schema.setMaxDepth(schema.getAllNodes());
	}
	
	
	/**
	 * Create GraphicFunction associated to an Function element.
	 * @param function The Function element use to create GraphicFunction
	 * @param parent   The parent node of the GraphicFunction to create
	 */
	private void createFunction(Function function, Node graphicParent)
	{
		/* Use to locate input and output nodes of the GraphicFunction */
		int positionX = SPACE;
		int positionY = 0;	
		
		// Levels of intern nodes of the GraphicFunction
		Vector<Vector<Element>> elementLevel = new Vector<Vector<Element>>();
		
		// Levels of intern GraphicIf conditions of the GraphicFunction
		Vector<Vector<Element>> conditions   = new Vector<Vector<Element>>();
		
		/* Go through the inputs of the function */
		for (Element element : function.getInput()) 
		{   
			positionY += SPACE;	
		    PointConnection pc = new PointConnection();
		    pc.setName(element.getName());
		    pc.setElement(element);
		    pc.setLayout(new Rectangle(positionX, positionY, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    graphicParent.addChild(pc);    
		}
		
		/* Go through the outputs of the function */
		for (Element element : function.getOutput()) 
		{			
		    PointConnection pc = new PointConnection();
		    pc.setName(element.getName());
		    pc.setElement(element);
		    graphicParent.addChild(pc);
		}
				
		/* Creation of the GraphicFunction */
	    GraphicFunction graphicFunction = new GraphicFunction();
	    graphicFunction.setName(function.getName());
	    graphicFunction.setElement(function);
	    graphicParent.addChild(graphicFunction);
		
		/* Set the location of the GraphicFunction */
		Node firstInputNode = getGraphicalNodeInParent(function.getInput().firstElement(), graphicParent);
		if(firstInputNode != null)
			graphicFunction.getLayout().setLocation(firstInputNode.getLayout().x + firstInputNode.getLayout().width + SPACE, firstInputNode.getLayout().y);
		else
			graphicFunction.getLayout().setLocation(SPACE, SPACE);		
		
		/* Go through the body of the function */
		for (Element element : function.getBody()) 
		{			
			if(element instanceof Assignment)
				createAssignment((Assignment) element, graphicFunction, elementLevel);
			else if(element instanceof IfThenElse)
				conditions.add(createIfThenElse((IfThenElse) element, graphicFunction, elementLevel));
			else if(element instanceof Multiplexer)
				createMultiplexer((Multiplexer) element, graphicFunction, elementLevel, conditions);
			else if(element instanceof Operation)
				createOperation((Operation) element, graphicFunction, elementLevel);
			else if(element instanceof LoopFor)
				createLoopFor((LoopFor) element, graphicFunction, elementLevel);
			else if(element instanceof LoopWhile)
				createLoopWhile((LoopWhile) element, graphicFunction, elementLevel);
		}
		
		/* Set node locations of different levels of the GraphicFunction */
		for (Vector<Element> level : elementLevel) 
			locateNodesLevel(level, graphicFunction);
		
		/* Set the size of the GraphicFunction */
		graphicFunction.setFullSize(graphicFunction.getLayout().width, graphicFunction.getLayout().height);
		graphicFunction.setReductSize(80, 15);		
		
		/* Set output node locations of the GraphicFunction */
		positionX = graphicFunction.getLayout().x + graphicFunction.getLayout().width + SPACE;
		positionY = graphicFunction.getLayout().y + SPACE;
		for (Element element : function.getOutput()) 
		{			
			Node node = getGraphicalNodeInParent(element, graphicParent);
		    node.setLayout(new Rectangle(positionX, positionY, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    positionY += SPACE;
		    graphicFunction.addOutputPointConnection((PointConnection) node);
		}
	}
	
	
	/**
	 * Create GraphicLoop associated to a LoopFor element.
	 * @param loopFor       The LoopFor element use to create GraphicFor
	 * @param graphicParent The parent node of the GraphicFor to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createLoopFor(LoopFor loopFor, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{		
		// Levels of intern nodes of the GraphicFor
		Vector<Vector<Element>> elementLevelFor = new Vector<Vector<Element>>();
		
		// Levels of intern GraphicIf and GraphicLoopIterator conditions of the GraphicFor
		Vector<Vector<Element>> conditions = new Vector<Vector<Element>>();
		
		/* Creation of the GraphicFor */
	    GraphicLoop graphicFor = new GraphicLoop();
	    graphicFor.setName(loopFor.getName());
	    graphicFor.setElement(loopFor);
	    graphicParent.addChild(graphicFor);
	    graphicFor.getLayout().setLocation(0, 0);	
	    
		/* Go through the inputs of the loopFor */
		Node nodeIn;
		int levelMax = 0;
		for (Element element : loopFor.getInput()) 
		{   
			nodeIn = getGraphicalNodeInParent(element, graphicParent);	
			if(nodeIn instanceof GraphicLoopIterator)
				nodeIn = ((GraphicLoopIterator) nodeIn).getOutputPointConnection();
			
			if(nodeIn != null)
			{
				/* Create a copy of an input PointConnection */
				levelMax = Math.max(levelMax, nodeIn.getLevel()+1);
				PointConnection pc = new PointConnection();
				pc.setName(element.getName());
			    pc.setElement(element);
		    	pc.setLayout(new Rectangle (0, Math.max(SPACE_INTERN_BLOC, (nodeIn.getLayout().y - graphicParent.getLayout().y)/10), PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			    pc.setLevel(0);
		    	if(pc.getLevel() > elementLevelFor.size()-1)
		    		elementLevelFor.add(new Vector<Element>());
			    elementLevelFor.elementAt(pc.getLevel()).add(element);
			    graphicFor.addChild(pc);  
			    new Connection(nodeIn, pc);
			} 
		}
		
		/* Set the level of the GraphicLoop */
		graphicFor.setLevel(levelMax);
    	if(graphicFor.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
    	elementLevel.elementAt(graphicFor.getLevel()).add(loopFor);
    	
		/* Go through the outputs of the function */
		for (Element element : loopFor.getOutput()) 
		{	
			if(getGraphicalNodeInParent(element, graphicParent) == null)
			{
				PointConnection pc = new PointConnection();
				pc.setName(element.getName());
				pc.setElement(element);
				graphicParent.addChild(pc);
			}
			getGraphicalNodeInParent(element, graphicParent).setLevel(graphicFor.getLevel());
		}
			
		/* Creation and initialisation of the GraphicLoopIterator */
	    GraphicLoopIterator iter = new GraphicLoopIterator();
	    iter.setEndCondition(loopFor.getEnd().getType().equals("const") ? String.valueOf(((SimpleVariable)loopFor.getEnd()).getVal()) : loopFor.getEnd().getName());
	    iter.setIncrementation(loopFor.getIncr().getType().equals("const") ? String.valueOf(((SimpleVariable)loopFor.getIncr()).getVal()) : loopFor.getIncr().getName());
	    iter.setStartInitialisation(loopFor.getStart().getType().equals("const") ? String.valueOf(((SimpleVariable)loopFor.getStart()).getVal()) : loopFor.getStart().getName());
	    iter.setIterOperation(loopFor.getIterOperation());
	    iter.setName(loopFor.getInternalVars().get(0).getName());
	    iter.setLayout(new Rectangle(0, 0, LoopIteratorFigure.WIDTH, LoopIteratorFigure.HEIGHT));
	    graphicFor.addChild(iter);
	    
	    /* Set the level of the GraphicLoopIterator */
	    iter.setLevel(1);
	    iter.setElement(loopFor.getInternalVars().get(0));
    	if(elementLevelFor.isEmpty())
    		elementLevelFor.add(new Vector<Element>());
    	if(iter.getLevel() > elementLevelFor.size()-1)
    		elementLevelFor.add(new Vector<Element>());
    	elementLevelFor.elementAt(iter.getLevel()).add(iter.getElement());
    	
		/* Creation of the output point of the GraphicLoopIterator */
		PointConnection pc = new PointConnection();
		pc.setName(loopFor.getInternalVars().get(0).getName());
		pc.setElement(loopFor.getInternalVars().get(0));
		pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		graphicFor.addChild(pc);		
		pc.setLevel(iter.getLevel());
		iter.setOutputPointConnexion(pc);
		new Connection(iter.getOutPoint(), pc);
		
		/* Go through the body of the loopFor */
		for (Element element : loopFor.getBody()) 
		{			
			if(element instanceof Assignment)
				createAssignment((Assignment) element, graphicFor, elementLevelFor);
			else if(element instanceof IfThenElse && element.getName().contains("loopfor"))
				createIfThenElseFor((IfThenElse) element, graphicFor, elementLevelFor);
			else if(element instanceof IfThenElse)
				conditions.add(createIfThenElse((IfThenElse) element, graphicFor, elementLevelFor));
			else if(element instanceof Multiplexer)
				createMultiplexer((Multiplexer) element, graphicFor, elementLevelFor, conditions);
			else if(element instanceof Operation)
				createOperation((Operation) element, graphicFor, elementLevelFor);
			else if(element instanceof LoopFor)
				createLoopFor((LoopFor) element, graphicFor, elementLevelFor);
			else if(element instanceof LoopWhile)
				createLoopWhile((LoopWhile) element, graphicFor, elementLevelFor);
		}
		
		/* Set node locations of different levels of the GraphicLoop */
		for (Vector<Element> level : elementLevelFor) 
			locateNodesLevel(level, graphicFor);
		
		/* Set the size of the GraphicLoop */
		graphicFor.setFullSize(graphicFor.getLayout().width, graphicFor.getLayout().height);
		graphicFor.setReductSize(80, 15);		
		
		/* Set condition nodes */
	    if(!loopFor.getEnd().getType().equals("const"))
	    	iter.addConditionNodes(getGraphicalNodeInParent(loopFor.getEnd(), graphicFor));
		if(!loopFor.getIncr().getType().equals("const"))
			iter.addConditionNodes(getGraphicalNodeInParent(loopFor.getIncr(), graphicFor));
		if(!loopFor.getStart().getType().equals("const"))
			iter.addConditionNodes(getGraphicalNodeInParent(loopFor.getStart(), graphicFor));
	}
	
	
	/**
	 * Create GraphicLoop associated to a LoopWhile element.
	 * @param loopWhile     The loopWhile element use to create GraphicFor
	 * @param graphicParent The parent node of the GraphicFor to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createLoopWhile(LoopWhile loopWhile, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{		
		// Levels of intern nodes of the GraphicFor
		Vector<Vector<Element>> elementLevelWhile = new Vector<Vector<Element>>();
		
		// Levels of intern GraphicIf and GraphicLoopIterator conditions of the GraphicFor
		Vector<Vector<Element>> conditions = new Vector<Vector<Element>>();
		
		/* Creation of the GraphicFor */
	    GraphicLoop graphicWhile = new GraphicLoop();
	    graphicWhile.setName(loopWhile.getName());
	    graphicWhile.setElement(loopWhile);
	    graphicParent.addChild(graphicWhile);
	    graphicWhile.getLayout().setLocation(0, 0);	
	    
		/* Go through the inputs of the lopWhile */
		Node nodeIn;
		int levelMax = 0;
		for (Element element : loopWhile.getInput()) 
		{   
			nodeIn = getGraphicalNodeInParent(element, graphicParent);	
			if(nodeIn instanceof GraphicLoopIterator)
				nodeIn = ((GraphicLoopIterator) nodeIn).getOutputPointConnection();
			
			if(nodeIn != null)
			{
				/* Create a copy of an input PointConnection */
				levelMax = Math.max(levelMax, nodeIn.getLevel()+1);
				PointConnection pc = new PointConnection();
				pc.setName(element.getName());
			    pc.setElement(element);
		    	pc.setLayout(new Rectangle (0, Math.max(SPACE_INTERN_BLOC, (nodeIn.getLayout().y - graphicParent.getLayout().y)/10), PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			    pc.setLevel(0);
		    	if(pc.getLevel() > elementLevelWhile.size()-1)
		    		elementLevelWhile.add(new Vector<Element>());
		    	elementLevelWhile.elementAt(pc.getLevel()).add(element);
			    graphicWhile.addChild(pc);  
			    new Connection(nodeIn, pc);
			} 
		}
		
		/* Set the level of the GraphicLoop */
		graphicWhile.setLevel(levelMax);
    	if(graphicWhile.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
    	elementLevel.elementAt(graphicWhile.getLevel()).add(loopWhile);
    	
		/* Go through outputs of the function */
		for (Element element : loopWhile.getOutput()) 
		{	
			if(getGraphicalNodeInParent(element, graphicParent) == null)
			{
				PointConnection pc = new PointConnection();
				pc.setName(element.getName());
				pc.setElement(element);
				graphicParent.addChild(pc);
			}
			getGraphicalNodeInParent(element, graphicParent).setLevel(graphicWhile.getLevel());
		}
    
		/* Go through the body of the loopFor */
		for (Element element : loopWhile.getBody()) 
		{			
			if(element instanceof Assignment)
				createAssignment((Assignment) element, graphicWhile, elementLevelWhile);
			else if(element instanceof IfThenElse && element.getName().contains("loopwhile"))
				createIfThenElseWhile((IfThenElse) element, graphicWhile, elementLevelWhile);
			else if(element instanceof IfThenElse)
				conditions.add(createIfThenElse((IfThenElse) element, graphicWhile, elementLevelWhile));
			else if(element instanceof Multiplexer)
				createMultiplexer((Multiplexer) element, graphicWhile, elementLevelWhile, conditions);
			else if(element instanceof Operation)
				createOperation((Operation) element, graphicWhile, elementLevelWhile);
			else if(element instanceof LoopFor)
				createLoopFor((LoopFor) element, graphicWhile, elementLevelWhile);
			else if(element instanceof LoopWhile)
				createLoopWhile((LoopWhile) element, graphicWhile, elementLevelWhile);
		}
		
		/* Set condition nodes of GraphicIf */
		for (Node node : graphicWhile.getChildrenArray()) 
			if(node instanceof GraphicIf && node.getName().contains("loopwhile"))
			{
			    Node condNode;
			    for(Element e:  loopWhile.getCond()) {
			    	for(Element e1: ((Operation)e).getInput()) {
					    condNode = getGraphicalNodeInParent(e1, graphicWhile);
					    if(condNode != null)
					    	((GraphicIf)node).addConditionNodes(condNode);
					    else 
					    {
					    	condNode = getGraphicalNode(e1);
					    	if(condNode != null)
					    		((GraphicIf)node).addConditionNodes(condNode);
					    }
			    	}
			    }
			}
		
		/* Set node locations of different levels of the GraphicFor */
		for (Vector<Element> level : elementLevelWhile) 
			locateNodesLevel(level, graphicWhile);
		
		/* Set the size of the GraphicFor */
		graphicWhile.setFullSize(graphicWhile.getLayout().width, graphicWhile.getLayout().height);
		graphicWhile.setReductSize(80, 15);		
	}
	

	/**
	 * Create PointConnection associated to an Assignment element.
	 * @param assignment	The Assignment element use to create PointConnection
	 * @param graphicParent The parent node of the PointConnection to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createAssignment(Assignment assignment, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{
		/* Get the input and ouput elements of the assignment */
		Element elementIn  = assignment.getInputAt(0);
		Element elementOut = assignment.getOutputAt(0);	
		
		/* Get the input and ouput nodes of the assignment */
		Node nodeIn  = getGraphicalNode(elementIn);  
		if(nodeIn instanceof GraphicLoopIterator)
			nodeIn = ((GraphicLoopIterator) nodeIn).getOutputPointConnection();
		Node nodeOut = getGraphicalNode(elementOut);
		
		if(nodeOut == null)
		{
			/* Creation of the output point of the assignment */
		    PointConnection pc = new PointConnection();
		    pc.setName(elementOut.getName());
		    pc.setElement(elementOut);
		    graphicParent.addChild(pc);
			
		    /* Case of an assignment between a constant and a variable */
			if(nodeIn == null)
			{
				pc.setLevel(0);
				pc.setIsConstant(true);
			    pc.setName(String.valueOf(((SimpleVariable)elementIn).getVal())); 
			    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			}
			else
			{		
				nodeIn = getGraphicalNodeInParent(elementIn, graphicParent);
				/* Case of an assignment between two intern PointConnection */
			    if(nodeIn != null)
			    {
			    	pc.setLevel(nodeIn.getLevel()+1);
			    	if(pc.getLevel() > elementLevel.size()-1)
			    		elementLevel.add(new Vector<Element>());
			    	pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			    }
			    /* Case of an assignment between an intern and an extern PointConnection */
			    else
			    {
			    	nodeIn = getGraphicalNodeInParent(elementIn, graphicParent.getParent());
			    	pc.setLevel(0);
			    	if(pc.getLevel() > elementLevel.size()-1)
			    		elementLevel.add(new Vector<Element>());
			    	pc.setLayout(new Rectangle (0, Math.max(SPACE_INTERN_BLOC, (nodeIn.getLayout().y - graphicParent.getLayout().y)/10), PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			    }
		    	new Connection(nodeIn, pc);
			    elementLevel.elementAt(pc.getLevel()).add(assignment);
			}
		}
		/* Case of an assignment between a constant and a variable */
		else
			new Connection(nodeIn, nodeOut);
	}
	
	
	/**
	 * Create associated nodes to an IfThenElse element.
	 * @param ifThenElse    The IfThenElse element use to create nodes 
	 * @param graphicParent The parent node of the nodes to create
	 * @param elementLevel  The parent vector of position level
	 * @return
	 */
	private Vector<Element> createIfThenElse(IfThenElse ifThenElse, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{	
		// Levels of intern GraphicIf conditions of the GraphicIf
		Vector<Vector<Element>> conditions = new Vector<Vector<Element>>();
		
		/* Go through the body true of the IfThenElse */	
		for(Element element : ifThenElse.getBodyTrue())
		{
			if(element instanceof Assignment)
				createAssignment((Assignment) element, graphicParent, elementLevel);
			else if(element instanceof IfThenElse)
				conditions.add(createIfThenElse((IfThenElse) element, graphicParent, elementLevel));
			else if(element instanceof Multiplexer)
				createMultiplexer((Multiplexer) element, graphicParent, elementLevel, conditions);
			else if(element instanceof Operation)
				createOperation((Operation) element, graphicParent, elementLevel);
			else if(element instanceof LoopFor)
				createLoopFor((LoopFor) element, graphicParent, elementLevel);
			else if(element instanceof LoopWhile)
				createLoopWhile((LoopWhile) element, graphicParent, elementLevel);
		}
		
		/* Go through the body false of the IfThenElse */
		for(Element element : ifThenElse.getBodyFalse())
		{
			if(element instanceof m2m.backend.structure.Assignment)
				createAssignment((Assignment) element, graphicParent, elementLevel);
			else if(element instanceof IfThenElse)
				conditions.add(createIfThenElse((IfThenElse) element, graphicParent, elementLevel));
			else if(element instanceof Multiplexer)
				createMultiplexer((Multiplexer) element, graphicParent, elementLevel, conditions);
			else if(element instanceof Operation)
				createOperation((Operation) element, graphicParent, elementLevel);
			else if(element instanceof LoopFor)
				createLoopFor((LoopFor) element, graphicParent, elementLevel);
			else if(element instanceof LoopWhile)
				createLoopWhile((LoopWhile) element, graphicParent, elementLevel);
		}
		
		// Return the condition of the IfThenElse
		return ifThenElse.getCond();
	}
	
	
	/**
	 * Create associated nodes to an IfThenElse element of a LoopFor.
	 * @param ifThenElse    The IfThenElse element use to create nodes 
	 * @param graphicParent The parent node of the nodes to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createIfThenElseFor(IfThenElse ifThenElse, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{	
		/* Get the condition of the IfThenElse */
		Vector<Element> condition = new Vector<Element>();
		Operation cond = new Equal();
		cond.addOutput(((Operation)ifThenElse.getCond().firstElement()).getOutputAt(0));
		cond.addInput(((LoopFor)graphicParent.getElement()).getInternalVars().firstElement());
		cond.addInput(((LoopFor)graphicParent.getElement()).getStart());
		condition.add(cond);
		
		/* Creation of the GraphicIf */
	    GraphicIf graphicIf = new GraphicIf();
	    graphicIf.setName(ifThenElse.getName());
	    graphicIf.setLayout(new Rectangle(0, 0, IfFigure.WIDTH, IfFigure.HEIGHT));
	    graphicIf.setElement(ifThenElse);
	    graphicIf.setCondition(getConditionString(condition));
	    graphicParent.addChild(graphicIf);    
	    
	    // Set condition nodes of GraphicIf
	    graphicIf.addConditionNodes(getGraphicalNodeInParent(((Operation)condition.firstElement()).getInput().firstElement(), graphicParent));
	    Node condNode = getGraphicalNode(((Operation)condition.firstElement()).getInputAt(1));
	    if(condNode != null)
	    	graphicIf.addConditionNodes(condNode);
	    
		/* Get input nodes of the IfThenElse */
	    Node nodeIn1 = getGraphicalNodeInParent(((Assignment)ifThenElse.getBodyTrue().firstElement()).getInputAt(0), graphicParent);
		Node nodeIn2 = getGraphicalNodeInParent(((Assignment)ifThenElse.getBodyFalse().firstElement()).getInputAt(0), graphicParent);
		if(nodeIn1 instanceof GraphicLoopIterator)
			nodeIn1 = ((GraphicLoopIterator) nodeIn1).getOutputPointConnection();
		if(nodeIn2 instanceof GraphicLoopIterator)
			nodeIn2 = ((GraphicLoopIterator) nodeIn2).getOutputPointConnection();
		
		if (nodeIn2 == null)
		{
			/* Create the second input */
			Element elementIn = ((Assignment)ifThenElse.getBodyFalse().firstElement()).getInputAt(0);
		    PointConnection pc = new PointConnection();
		    pc.setName(elementIn.getName());
		    pc.setLevel(nodeIn1.getLevel());
		    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    pc.setElement(elementIn);
		    graphicParent.addChild(pc);		
		    nodeIn2 = pc;
		}   
	    
	    /* Set the level of the GraphicIf */
		graphicIf.setLevel(Math.max(nodeIn1.getLevel(), nodeIn2.getLevel())+1);
    	if(graphicIf.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
   	    elementLevel.elementAt(graphicIf.getLevel()).add(ifThenElse);
   	    
		/* Creation of the output point of GraphicIf */
		Element elementOut = ((Assignment)ifThenElse.getBodyTrue().firstElement()).getOutputAt(0);
	    PointConnection pc = new PointConnection();
	    pc.setName(elementOut.getName());
	    pc.setLevel(graphicIf.getLevel());
	    pc.setElement(elementOut);
	    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
	    graphicParent.addChild(pc);		
	    graphicIf.setOutputPointConnexion(pc);
	    	    
		new Connection(nodeIn1, graphicIf.getTruePoint());
		new Connection(nodeIn2, graphicIf.getFalsePoint());
		new Connection(graphicIf.getOutPoint(), pc);
	}
	
	/**
	 * Create associated nodes to an IfThenElse element of a LoopFor.
	 * @param ifThenElse    The IfThenElse element use to create nodes 
	 * @param graphicParent The parent node of the nodes to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createIfThenElseWhile(IfThenElse ifThenElse, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{	
		// Get the condition of the IfThenElse
		Vector<Element> condition = ifThenElse.getCond();
		
		/* Creation of the GraphicIf */
	    GraphicIf graphicIf = new GraphicIf();
	    graphicIf.setName(ifThenElse.getName());
	    graphicIf.setLayout(new Rectangle(0, 0, IfFigure.WIDTH, IfFigure.HEIGHT));
	    graphicIf.setElement(ifThenElse);
	    graphicIf.setCondition(getConditionString(condition));
	    graphicParent.addChild(graphicIf);    
	    
		/* Get input nodes of the IfThenElse */
	    Node nodeIn1 = getGraphicalNodeInParent(((Assignment)ifThenElse.getBodyTrue().firstElement()).getInputAt(0), graphicParent);
		Node nodeIn2 = getGraphicalNodeInParent(((Assignment)ifThenElse.getBodyFalse().firstElement()).getInputAt(0), graphicParent);
		if(nodeIn1 instanceof GraphicLoopIterator)
			nodeIn1 = ((GraphicLoopIterator) nodeIn1).getOutputPointConnection();
		if(nodeIn2 instanceof GraphicLoopIterator)
			nodeIn2 = ((GraphicLoopIterator) nodeIn2).getOutputPointConnection();
		
		if (nodeIn2 == null)
		{
			/* Create the second input */
			Element elementIn = ((Assignment)ifThenElse.getBodyFalse().firstElement()).getInputAt(0);
		    PointConnection pc = new PointConnection();
		    pc.setName(elementIn.getName());
		    pc.setLevel(nodeIn1.getLevel());
		    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    pc.setElement(elementIn);
		    graphicParent.addChild(pc);		
		    nodeIn2 = pc;
		}   
	    
	    /* Set the level of the GraphicIf */
		graphicIf.setLevel(Math.max(nodeIn1.getLevel(), nodeIn2.getLevel())+1);
    	if(graphicIf.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
   	    elementLevel.elementAt(graphicIf.getLevel()).add(ifThenElse);
   	    
		/* Creation of the output point of GraphicIf */
		Element elementOut = ((Assignment)ifThenElse.getBodyTrue().firstElement()).getOutputAt(0);
	    PointConnection pc = new PointConnection();
	    pc.setName(elementOut.getName());
	    pc.setLevel(graphicIf.getLevel());
	    pc.setElement(elementOut);
	    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
	    graphicParent.addChild(pc);		
	    graphicIf.setOutputPointConnexion(pc);
	    
	    
		new Connection(nodeIn1, graphicIf.getTruePoint());
		new Connection(nodeIn2, graphicIf.getFalsePoint());
		new Connection(graphicIf.getOutPoint(), pc);
	}
	
	
	/**
	 * Create nodes associated to an Multiplexer element.
	 * @param mux			The Multiplexer element use to create nodes 
	 * @param graphicParent The parent node of the nodes to create
	 * @param elementLevel  The parent vector of position level
	 * @param conditions    The conditions vector containing Multiplexer condition
	 */
	private void createMultiplexer(Multiplexer mux, Node graphicParent, Vector<Vector<Element>> elementLevel, Vector<Vector<Element>> conditions)
	{
		/* Get the condition of the Multiplexer */
		Vector<Element> condition = new Vector<Element>();
		String variableConditionName = mux.getSel().getName();
		for(Vector<Element> cond : conditions)
			if(((Operation)cond.lastElement()).getOutputAt(0).getName().equalsIgnoreCase(variableConditionName))
				condition.addAll(cond);
			
		/* Creation of the GraphicIf */
	    GraphicIf ifThenElse = new GraphicIf();
	    ifThenElse.setName(mux.getName());
	    ifThenElse.setLayout(new Rectangle(0, 0, IfFigure.WIDTH, IfFigure.HEIGHT));
	    ifThenElse.setElement(mux);
	    ifThenElse.setCondition(getConditionString(condition));
	    graphicParent.addChild(ifThenElse);    
	    
	    /* Set condition nodes of the GraphicIf */
	    Vector<Element> inputCondition  = new Vector<Element>();
	    Vector<Element> outputCondition = new Vector<Element>();
		for(Element element : condition)
		{
			for(Element input : ((Operation)element).getInput())
				if(!inputCondition.contains(input) && !((Variable)input).getType().equals("const"))
					inputCondition.add(input);
			outputCondition.addAll(((Operation)element).getOutput());
		}
		inputCondition.removeAll(outputCondition);
		for(Element element : inputCondition)
			ifThenElse.addConditionNodes(getGraphicalNodeInParent(element, graphicParent));

		/* Get input nodes of the multiplexer */
		Node nodeIn1 = getGraphicalNodeInParent(mux.getInputAt(0), graphicParent);
		Node nodeIn2 = getGraphicalNodeInParent(mux.getInputAt(1), graphicParent);
		if(nodeIn1 instanceof GraphicLoopIterator)
			nodeIn1 = ((GraphicLoopIterator) nodeIn1).getOutputPointConnection();
		if(nodeIn2 instanceof GraphicLoopIterator)
			nodeIn2 = ((GraphicLoopIterator) nodeIn2).getOutputPointConnection();
		
	    /* Set the level of the GraphicIf */
	    ifThenElse.setLevel(Math.max(nodeIn1.getLevel(), nodeIn2.getLevel())+1);
    	if(ifThenElse.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
   	    elementLevel.elementAt(ifThenElse.getLevel()).add(mux);	    
   	    
		/* Creation of the output point of the GraphicIf */
		Element elementOut = mux.getOutputAt(0);
		PointConnection pc = new PointConnection();
		if(getGraphicalNodeInParent(elementOut, graphicParent) != null)
			pc = (PointConnection)getGraphicalNodeInParent(elementOut, graphicParent);
		else
		{   
		    pc.setName(elementOut.getName());
		    pc.setElement(elementOut);
		    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    graphicParent.addChild(pc);		
		}
		ifThenElse.setOutputPointConnexion(pc);
		pc.setLevel(ifThenElse.getLevel());
	    	    
		new Connection(nodeIn1, ifThenElse.getTruePoint());
		new Connection(nodeIn2, ifThenElse.getFalsePoint());
		new Connection(ifThenElse.getOutPoint(), pc);
	}
	
	
	/**
	 * Create nodes associated to an Operation element.
	 * @param operation     The Operation element use to create nodes 
	 * @param graphicParent The parent node of the nodes to create
	 * @param elementLevel  The parent vector of position level
	 */
	private void createOperation(Operation operation, Node graphicParent, Vector<Vector<Element>> elementLevel)
	{	
		Node node;
		int maxLevel = 0;
		Vector<Node> inputs = new Vector<Node>();
		
		/* Get nodes input of the operation */
		for(Element e : operation.getInput()) {
			node = getGraphicalNodeInParent(e, graphicParent);
			if(node instanceof GraphicLoopIterator)
				node = ((GraphicLoopIterator) node).getOutputPointConnection();
			inputs.add(node);
		}
		
		for(int i = 0; i < inputs.size(); i++) {
			node = inputs.get(i);
			if(node == null) {
				/* Create the first constant input */
			    PointConnection pc = new PointConnection();
			    pc.setName(String.valueOf(((SimpleVariable)operation.getInputAt(i)).getVal()));
			    if(i == 0 && inputs.size() > 1)
			    	pc.setLevel(inputs.get(1) == null ? 0 : inputs.get(1).getLevel());
			    else if(i == 1)
			    	pc.setLevel(inputs.get(0) == null ? 0 : inputs.get(0).getLevel());
			    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
			    pc.setElement(operation.getInputAt(i));
			    graphicParent.addChild(pc);		
			    pc.setIsConstant(true);
			    inputs.set(i, pc); 
			}
		}
			
		/* Creation of the GraphicOperation */
	    GraphicOperation o = new GraphicOperation();
	    o.setName(operation.getName());
	    o.setElement(operation);
	    o.setOperatorName(operation.getOpSymbol());
	    graphicParent.addChild(o); 
	    
	    /* Set the level of the GraphicOperation */
	    for(Node in : inputs)
	    	maxLevel = maxLevel < in.getLevel() ? in.getLevel() : maxLevel;
    	o.setLevel(maxLevel+1);
    	if(o.getLevel() > elementLevel.size()-1)
    		elementLevel.add(new Vector<Element>());
   	    elementLevel.elementAt(o.getLevel()).add(operation);
   	    o.setLayout(new Rectangle (0, 0, OperationFigure.WIDTH, OperationFigure.HEIGHT));
	    
		/* Creation of the output point of the GraphicOperation */
		Element elementOut = operation.getOutput().elementAt(0);
		PointConnection pc = new PointConnection();
		if(getGraphicalNodeInParent(elementOut, graphicParent) != null)
			pc = (PointConnection)getGraphicalNodeInParent(elementOut, graphicParent);
		else {   
		    pc.setName(elementOut.getName());
		    pc.setElement(elementOut);
		    pc.setLayout(new Rectangle (0, 0, PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT));
		    graphicParent.addChild(pc);		
		}
	    o.setOutputPointConnexion(pc);
		pc.setLevel(o.getLevel());
		
		for(Node in : inputs)
			new Connection(in, o);
		new Connection(o, pc);
	}
	
	
	/**
	 * Set node locations for a specific level.
	 * @param level The level containing the represented nodes by an element
	 * @param graphicFunction The parent node of nodes represented by an element of the level
	 */
	private void locateNodesLevel(Vector<Element> level, GraphicFunction graphicFunction)
	{	
		Vector<Node> nodes = new Vector<Node>();
		Point bestLocation = new Point();
		Node nodeOut = null; 
		Node nodeIn = null; 
		
		/* Create the node vector */
		for (Element element : level) 
		{
			if(element instanceof Assignment)
				nodes.add(getGraphicalNodeInParent(((Assignment)element).getOutputAt(0), graphicFunction));
			else
				nodes.add(getGraphicalNodeInParent(element, graphicFunction));
		}
		
		/* Get and set the best location for all nodes of the level */
		for (Node node: nodes) 
		{
			if(!(node instanceof PointConnection  && ((PointConnection)node).getIsConstant()) && node.getLevel() != 0)
			{
				int index = nodes.indexOf(node);
				bestLocation = getBestLocation(level.elementAt(index), graphicFunction);
				
				if(node instanceof PointConnection)
					node.getLayout().setLocation(graphicFunction.getLayout().width, Math.max(SPACE_INTERN_BLOC, bestLocation.y-PointConnectionFigure.WIDTH/2));	
				else if(node instanceof GraphicOperation)
					node.getLayout().setLocation(graphicFunction.getLayout().width, Math.max(SPACE_INTERN_BLOC, bestLocation.y-OperationFigure.WIDTH/2));
				else if(node instanceof GraphicIf)
					node.getLayout().setLocation(graphicFunction.getLayout().width, Math.max(SPACE_INTERN_BLOC, bestLocation.y-IfFigure.WIDTH/2));	
				else if(node instanceof GraphicLoop)
					node.getLayout().setLocation(graphicFunction.getLayout().width, Math.max(SPACE_INTERN_BLOC, bestLocation.y-node.getLayout().height/2));
				else if(node instanceof GraphicLoopIterator)
					node.getLayout().setLocation(graphicFunction.getLayout().width, Math.max(SPACE_INTERN_BLOC, bestLocation.y-node.getLayout().height/2));
			}
		}
		
		// Suppression of all superpositions
		elasticLayout(nodes);
		
		/* Update the size of the parent node and locate output and constant node */
		for (Node node: nodes) 
		{
			if(node instanceof PointConnection)
			{
				/* Update the size of the parent node */
				graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  node.getLayout().x + node.getLayout().width  + SPACE_INTERN_BLOC),
                                                    Math.max(graphicFunction.getLayout().height, node.getLayout().y + node.getLayout().height + SPACE_INTERN_BLOC));
			}
			else if(node instanceof GraphicOperation)
			{
				locateConstantInput(node.getElement(), graphicFunction);
				
				/* Locate output of the GraphicOperation */
				nodeOut = ((GraphicOperation)node).getOutputPointConnection();
				nodeOut.getLayout().x = node.getLayout().x + node.getLayout().width + 10;
				nodeOut.getLayout().y = (2*node.getLayout().y + node.getLayout().height)/2 - PointConnectionFigure.HEIGHT/2;
				
				/* Update the size of the parent node */
				graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  nodeOut.getLayout().x + nodeOut.getLayout().width  + SPACE_INTERN_BLOC),
                                             Math.max(graphicFunction.getLayout().height, nodeOut.getLayout().y + nodeOut.getLayout().height + SPACE_INTERN_BLOC));
			}
			else if(node instanceof GraphicIf)
			{
				locateConstantInput(node.getElement(), graphicFunction);
				
				/* Locate output of the GraphicIf */
				nodeOut = ((GraphicIf)node).getOutputPointConnection();
				nodeOut.getLayout().x = node.getLayout().x + node.getLayout().width + 10;
				nodeOut.getLayout().y = (2*node.getLayout().y + node.getLayout().height)/2 - PointConnectionFigure.HEIGHT/2;
				
				/* Update the size of the parent node */
				graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  nodeOut.getLayout().x + nodeOut.getLayout().width  + SPACE_INTERN_BLOC),
                                                    Math.max(graphicFunction.getLayout().height, nodeOut.getLayout().y + nodeOut.getLayout().height + SPACE_INTERN_BLOC));
			}
			else if(node instanceof GraphicLoop)
			{
				Vector<Node> outputNodes = new Vector<Node>();
				
				// Update the size of the parent node
				graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  node.getLayout().x + node.getLayout().width  + SPACE_INTERN_BLOC),
                                                    Math.max(graphicFunction.getLayout().height, node.getLayout().y + node.getLayout().height + SPACE_INTERN_BLOC));

				/* Locate all outputs of the GraphicFor */
				for (Element element : ((Function)node.getElement()).getOutput()) 
				{			
					nodeOut = getGraphicalNodeInParent(element, graphicFunction);
					nodeOut.getLayout().x = node.getLayout().x + node.getLayout().width + 15;
					nodeOut.getLayout().y =	node.getLayout().y + getGraphicalNodeInParent(element, node).getLayout().y;
				    nodeOut.getLayout().setSize(PointConnectionFigure.WIDTH, PointConnectionFigure.HEIGHT);
				    ((GraphicLoop)node).addOutputPointConnection((PointConnection) nodeOut);
				    outputNodes.add(nodeOut);
				    new Connection(getGraphicalNodeInParent(element, node), getGraphicalNodeInParent(element, graphicFunction));
				    
					/* Update the size of the parent node */
					graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  nodeOut.getLayout().x + nodeOut.getLayout().width  + SPACE_INTERN_BLOC),
	                                                    Math.max(graphicFunction.getLayout().height, node.getLayout().y + node.getLayout().height + SPACE_INTERN_BLOC));
				}	
				
				elasticLayout(outputNodes);
				
				/* Locate all constant input node */
				for(Element element : ((Function)node.getElement()).getInput())
				{
					nodeIn = getGraphicalNodeInParent(element, graphicFunction);
					if(((Variable)nodeIn.getElement()).getType().equals("const"))
					{
						nodeIn.getLayout().x = node.getLayout().x - 30;
						nodeIn.getLayout().y = node.getLayout().y + getGraphicalNodeInParent(element, node).getLayout().y+1;
					}
				}
			}
			else if(node instanceof GraphicLoopIterator)
			{
				/* Locate output of the GraphicIf */
				nodeOut = ((GraphicLoopIterator)node).getOutputPointConnection();
				nodeOut.getLayout().x = node.getLayout().x + node.getLayout().width + 10;
				nodeOut.getLayout().y = (2*node.getLayout().y + node.getLayout().height)/2 - PointConnectionFigure.HEIGHT/2 + 2;
				
				/* Update the size of the parent node */
				graphicFunction.getLayout().setSize(Math.max(graphicFunction.getLayout().width,  nodeOut.getLayout().x + nodeOut.getLayout().width  + SPACE_INTERN_BLOC),
                                                    Math.max(graphicFunction.getLayout().height, nodeOut.getLayout().y + nodeOut.getLayout().height + SPACE_INTERN_BLOC));			
			}
		}
	}

	
	/**
	 * Get the best location of nodes represented by an element.
	 * @param element The represented element of nodes
	 * @param graphicFunction The parent node of nodes represented by an element.
	 * @return the best location of a node
	 */
	private Point getBestLocation(Element element, GraphicFunction graphicFunction)
	{
		Node nodeIn1;
		Node nodeIn2;
		Point point  = new Point();
		Node parentNode = getGraphicalNodeInParent(element, graphicFunction);
		
		/* Get the best location of node represented by Assignment element */
		if(element instanceof Operation && ((Operation)element).getInput().size() == 1)
		{ 
			/* Get the input of the Assignment element */
			nodeIn1 = getGraphicalNodeInParent(((Operation)element).getInputAt(0), graphicFunction);		
			point.setLocation(0, (nodeIn1.getLayout().y + nodeIn1.getLayout().height/2));
		}
		
		/* Get the best location of node represented by Operation element */
		else if(element instanceof Operation)
		{
			/* Get the inputs of the Operation element */
			nodeIn1 = getGraphicalNodeInParent(((Operation)element).getInputAt(0), graphicFunction);
			nodeIn2 = getGraphicalNodeInParent(((Operation)element).getInputAt(1), graphicFunction); 
			if(nodeIn1 instanceof GraphicLoopIterator)
				nodeIn1 = ((GraphicLoopIterator) nodeIn1).getOutputPointConnection();
			if(nodeIn2 instanceof GraphicLoopIterator)
				nodeIn2 = ((GraphicLoopIterator) nodeIn2).getOutputPointConnection();
			
			/* Check if there is an constant input */
			if(!((PointConnection)nodeIn1).getIsConstant() && !((PointConnection)nodeIn2).getIsConstant())
				point.setLocation(0, (nodeIn1.getLayout().y + nodeIn2.getLayout().y + nodeIn2.getLayout().height)/2);
			else if(((PointConnection)nodeIn1).getIsConstant() && ((PointConnection)nodeIn2).getIsConstant())
				point.setLocation(0, SPACE_INTERN_BLOC + getGraphicalNodeInParent(element, graphicFunction).getLayout().height/2);
			else if(((PointConnection)nodeIn1).getIsConstant())
				point.setLocation(0, nodeIn2.getLayout().y + nodeIn2.getLayout().height/2);
			else if(((PointConnection)nodeIn2).getIsConstant())
				point.setLocation(0, nodeIn1.getLayout().y + nodeIn1.getLayout().height/2);
		}
		
		/* Get the best location of node represented by IfThenElse element */
		else if(element instanceof IfThenElse)
		{
			/* Get the inputs of the IfThenElse element */
			nodeIn1 = getGraphicalNodeInParent(((Assignment)((IfThenElse)element).getBodyTrue().firstElement()).getInputAt(0), graphicFunction);
			nodeIn2 = getGraphicalNodeInParent(((Assignment)((IfThenElse)element).getBodyFalse().firstElement()).getInputAt(0), graphicFunction);
			if(nodeIn1 instanceof GraphicLoopIterator)
				nodeIn1 = ((GraphicLoopIterator) nodeIn1).getOutputPointConnection();
			if(nodeIn2 instanceof GraphicLoopIterator)
				nodeIn2 = ((GraphicLoopIterator) nodeIn2).getOutputPointConnection();
			
			/* Check if there is an constant input */
			if(nodeIn1.getLevel() > parentNode.getLevel())
				point.setLocation(0, nodeIn2.getLayout().y + nodeIn2.getLayout().height/2);
			else if	(nodeIn2.getLevel() > parentNode.getLevel())
				point.setLocation(0, nodeIn1.getLayout().y + nodeIn1.getLayout().height/2);
		}
		
		/* Get the best location of node represented by LoopFor element */
		else if(element instanceof Function)
		{
			/* Get the first and last input of the LoopFor element */
			Node firstInputNode = getGraphicalNodeInParent(((Function) element).getInput().lastElement(), graphicFunction);
			Node lastInputNode  = getGraphicalNodeInParent(((Function) element).getInput().lastElement(), graphicFunction);
			
			point.setLocation(0, (firstInputNode.getLayout().y+lastInputNode.getLayout().y)/2);
		}
		
		else
			point.setLocation(0, SPACE_INTERN_BLOC);	
		
		return point;	
	}
	

	/**
	 * Set constant node locations represented by an element.
	 * @param element       The represented element of nodes
	 * @param graphicParent The parent node of nodes represented by an element.
	 */
	private void locateConstantInput(Element element, Node graphicParent)
	{	
		if(!(element instanceof Assignment) && !(element instanceof Function))
		{
			
			int value;
			Vector<Node> inputs = new Vector<Node>();
			Rectangle layoutParent  = getGraphicalNodeInParent(element, graphicParent).getLayout();
			
			/* Get nodes input of the operation */
			for(Element e : ((Operation)element).getInput()) {
				Node node = getGraphicalNodeInParent(e, graphicParent);
				if(node instanceof GraphicLoopIterator)
					node = ((GraphicLoopIterator) node).getOutputPointConnection();
				inputs.add(node);
			}
			
			/* Locate constant nodes represented by Multiplexer element */
			if(element instanceof Multiplexer) {
				for(int i = 0; i < inputs.size(); i++){
					Node node = inputs.get(i);
					if(((PointConnection)node).getIsConstant())
						node.getLayout().setLocation(layoutParent.x-20, layoutParent.y+5+i*19);
				}
			}		
			/* Locate constant nodes represented by Operation element */
			else if(element instanceof Operation)
			{
				if(inputs.size() == 2) 
				{
					Node nodeIn1 = inputs.elementAt(0);
					Node nodeIn2 = inputs.elementAt(1);
					/* Locate the first and second node of the Multiplexer element */
					if(((PointConnection)nodeIn1).getIsConstant() && ((PointConnection)nodeIn2).getIsConstant())
					{
						nodeIn1.getLayout().setLocation(layoutParent.x-20, layoutParent.y-5);
						nodeIn2.getLayout().setLocation(layoutParent.x-20, layoutParent.y+15);
					}
					/* Locate the first node of the Multiplexer element */
					else if(((PointConnection)nodeIn1).getIsConstant())
					{
						value = (nodeIn2.getLayout().y > layoutParent.y) ? -5 : +15;
						nodeIn1.getLayout().setLocation(layoutParent.x-20, layoutParent.y+value);
					}
					/* Locate the second node of the Multiplexer element */
					else if(((PointConnection)nodeIn2).getIsConstant())
					{	
						value = (nodeIn1.getLayout().y > layoutParent.y) ? -5 : +15;
						nodeIn2.getLayout().setLocation(layoutParent.x-20, layoutParent.y+value);	
					}
				}
				else if(inputs.size() == 1)
				{
					if(((PointConnection)inputs.elementAt(0)).getIsConstant())
						inputs.elementAt(0).getLayout().setLocation(layoutParent.x-20, layoutParent.y+4);	
				}
			}
		}
	}
	
	/**
	 * Do an elastic effect between superposed nodes.
	 * @param nodes The vector of nodes of a specific level
	 */
	private void elasticLayout(Vector<Node> nodes)
	{
		Node tempNode;
		
		/* Order the node list with the bigger horizontal position */
		for(int i = 0; i < nodes.size(); i++)
			for(int j = i; j < nodes.size(); j++)
				if(nodes.elementAt(i).getLayout().y < nodes.elementAt(j).getLayout().y)
				{
					tempNode = nodes.elementAt(i);
					nodes.setElementAt(nodes.elementAt(j), i);
					nodes.setElementAt(tempNode, j);
				}
		
		/* Delete superpositions in the level */
		for(Node node : nodes)
		{
			if (nodes.indexOf(node) != nodes.size()-1)
				deleteSuperpositionUp(nodes, node, nodes.elementAt(nodes.indexOf(node)+1));

			if (nodes.indexOf(node) != 0)
				deleteSuperpositionDown(nodes, node, nodes.elementAt(nodes.indexOf(node)-1));
		}
	}
	
	
	/**
	 * Delete superposition by moving nodes down.
	 * @param nodes		The vector of nodes of a specific level
	 * @param node		The current node
	 * @param nextNode  the next node
	 */
	private void deleteSuperpositionDown(Vector<Node> nodes, Node node, Node nextNode)
	{
		int dSuperposition;
		Node tempNode;
		
		/* If the current node is the last node, all superositions are deleted */
		if(nextNode == null)
			return;
		
		// Get the distance of superposition
		dSuperposition = distanceSuperposedDown(node, nextNode);
		
		/* Check if there is a superposition */
		if(dSuperposition != 0)
		{	
			// Delete the superposition
			nextNode.getLayout().y += dSuperposition;
			
			/* Get the next node */
			if(nodes.indexOf(nextNode) == 0)
				tempNode = null;
			else
				tempNode = nodes.elementAt(nodes.indexOf(nextNode)-1);
			
			deleteSuperpositionDown(nodes, nextNode, tempNode);
		}	
	}
	
	
	/**
	 * Delete superposition by moving nodes up.
	 * @param nodes    The vector of nodes of a specific level
	 * @param node     The current node
	 * @param nextNode The next node
	 * @return true if the superpositons are deleted, else false
	 */
	private boolean deleteSuperpositionUp(Vector<Node> nodes, Node node, Node nextNode)
	{
		int dSuperposition;
		Node tempNode;
		
		/* If the current node is the last node, check the succes of deleted superpositions */
		if(nextNode == null)
			return node.getLayout().y >= SPACE_INTERN_BLOC;
			
		// Get the distance of superposition
		dSuperposition = distanceSuperposedUp(node, nextNode);
		
		/* Check if there is a superposition */
		if(dSuperposition != 0)
		{	
			// Delete the superposition
			nextNode.getLayout().y -= dSuperposition;
			
			/* Get the next node */
			if(nodes.indexOf(nextNode) == nodes.size()-1)
				tempNode = null;
			else
				tempNode = nodes.elementAt(nodes.indexOf(nextNode)+1);
			
			if(!deleteSuperpositionUp(nodes, nextNode, tempNode))
			{
				// Set the original location
				nextNode.getLayout().y += dSuperposition;
				
				return false;
			}
		}	
		
		return true;
	}


	/**
	 * Check if a node superposed the node above it.
	 * @param nodeTest The node to be tested
	 * @param node The node above the node to be tested
	 * @return the horizontal distance of the superposition
	 */
	private int distanceSuperposedUp(Node nodeTest, Node node)
	{	
		Rectangle nodeLayout     = node.getLayout();
		Rectangle nodeLayoutTest = nodeTest.getLayout();
	
		if(nodeLayoutTest.y < nodeLayout.y+nodeLayout.height+SPACE_INTERN_BLOC)
			return nodeLayout.y + nodeLayout.height - nodeLayoutTest.y + SPACE_INTERN_BLOC;
		else
			return 0;
	}
	
	
	/**
	 * Check if a node superposed the node under it.
	 * @param nodeTest The node to be tested
	 * @param node The node under the node to be tested
	 * @return the horizontal distance of the superposition
	 */
	private int distanceSuperposedDown(Node nodeTest, Node node)
	{	
		Rectangle nodeLayout     = node.getLayout();
		Rectangle nodeLayoutTest = nodeTest.getLayout();
	
		if(nodeLayoutTest.y+nodeLayoutTest.height > nodeLayout.y-SPACE_INTERN_BLOC)
			return nodeLayoutTest.y+nodeLayoutTest.height - nodeLayout.y+SPACE_INTERN_BLOC;
		else
			return 0;
	}
	
	
	/**
	 * Get the string representation of a condition.
	 * @param condition The condition to convert as a string
	 * @return the string representation of the condition
	 */
	private String getConditionString(Vector<Element> condition)
	{
		/* Construct a vector of strings containing all parts of the condition */
	    Vector<Vector<String>> conditionsNames = new Vector<Vector<String>>(); 
		for(Element operation : condition)
		{
			Vector<String> names = new Vector<String>();
			for(Element element : ((Operation)operation).getInput())
			{
				if(!(((Variable)element).getType().equals("const")))
					names.add(element.getName());
				else
					names.add(String.valueOf(((SimpleVariable)element).getVal()));	
			}
			
			names.add(((Operation)operation).getOpSymbol());
			names.add(((Operation)operation).getOutputAt(0).getName());		
			conditionsNames.add(names);
		}
		
		/* Replace all temporary signal of the condition */
		for(Vector<String> names1 : conditionsNames)
		{
			for(Vector<String> names2 : conditionsNames)
			{
				if(names2.lastElement().equalsIgnoreCase(names1.elementAt(0)))
					if(names2.size() != 3)
						names1.setElementAt(names2.elementAt(0) + " " + names2.elementAt(2) + " " +names2.elementAt(1), 0);
					else 
						names1.setElementAt(names2.elementAt(1) + names2.elementAt(0), 0);
				
				if(names1.size() != 3 && names2.lastElement().equalsIgnoreCase(names1.elementAt(1)))
					names1.setElementAt(names2.elementAt(0) + " " + names2.elementAt(2) + " " +names2.elementAt(1), 1);
			}
		}
		
		/* Construct and return the finally string */
		if(conditionsNames.lastElement().size() != 3)
			return conditionsNames.lastElement().elementAt(0) + " " + conditionsNames.lastElement().elementAt(2) + " " + conditionsNames.lastElement().elementAt(1);
		else
			return conditionsNames.lastElement().elementAt(1) + conditionsNames.lastElement().elementAt(0);

	}
	
	
	/**
	 * Get the represented node of an element in the graphical structure.
	 * @param element The research element in the graphical structure
	 * @param The parent node of the graphical structure
	 * @return the represented node of the element
	 */
	private Node getGraphicalNodeInParent(Element element, Node parent)
	{
		for(Node node : parent.getChildrenArray())
			if(element == node.getElement())
				return node;
		
		return null;
	}
	
	
	/**
	 * Get the represented node of an element in the graphical structure.
	 * @param element The research element in the graphical structure
	 * @return the represented node of the element
	 */
	private Node getGraphicalNode(Element element)
	{
		/* Use to go through all nodes */
		LinkedList<Node> nodeList = new LinkedList<Node>();
		nodeList.addAll(schema.getChildrenArray());
		Iterator<Node> iterNode = nodeList.iterator();
		Node node;
		
		while (iterNode.hasNext()) 
		{
			node = iterNode.next();
			
			/* Return the node if elements are equals */
			if(element == node.getElement())
				return node;
			
			if(node.getChildrenArray().size() != 0)	
				nodeList.addAll(node.getChildrenArray());
			
			nodeList.remove(nodeList.indexOf(node));
			iterNode = nodeList.iterator();	
		}	
		
		return null;
	}
	
	
	/**
	 * Get the instance of Schema of the model.
	 * @return the instance of Schema of the model
	 */
	public Schema getSchema()
	{
		return schema;
	}
	
	
	/**
	 * Set the instance of schema of the model.
	 * @param schema the instance of schema to the model to set
	 */
	public void setSchema(Schema schema)
	{
		this.schema = schema;
	}
}
