/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit policy who manages layout modifications.
 *
 * Project:  Math2Mat
 *
 * @file EditLayoutPolicy.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the Edit policy who manages layout modifications.
 * 
 */
package m2m.frontend.dynamicview.editpolicies;

import m2m.frontend.dynamicview.commands.*;
import m2m.frontend.dynamicview.editPart.*;
import m2m.frontend.dynamicview.model.Node;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;


public class EditLayoutPolicy extends XYLayoutEditPolicy 
{
	/**
	 * Create an abstract layout command for a specific child EditPart
	 * @param  the specific child editPart
	 * @param  the constraint of the command
	 * @return the created command
	 */
    protected Command createChangeConstraintCommand(EditPart child, Object constraint) 
    {
	    AbstractLayoutCommand command = null;
	    
	    /* Creation of the command who correspond to the child EditPart */
	    if (child instanceof PointConnectionPart) 
	    	command = new PointConnectionChangeLayoutCommand();
	    else if (child instanceof OperationPart) 
	    	command = new OperationChangeLayoutCommand();   
	    else if (child instanceof FunctionPart) 
	    	command = new FunctionChangeLayoutCommand();
	    else if (child instanceof IfPart) 
	    	command = new IfChangeLayoutCommand();
	    else if (child instanceof LoopPart) 
	    	command = new FunctionChangeLayoutCommand();
	    else if (child instanceof LoopIteratorPart) 
	    	command = new IteratorChangeLayoutCommand();
	    
	    /* Intialisation of the command */
	    command.setModel((Node)child.getModel());
	    command.setLayout((Rectangle)constraint);
	    command.setRootPart((SchemaPart)child.getRoot().getContents());
	    
	    return command;
    }

    
    /**
     * Create the default EditPolicy use for children.
     * @param  the child EditPart (not use)
     * @return the EditPolicy use for children 
     */
    protected EditPolicy createChildEditPolicy(EditPart child)
    {
    	return new NonResizableEditPolicy();
    }
    
    
    protected Command getCreateCommand(CreateRequest request) 
    {
        // TODO Auto-generated method stub
        return null;
    }
}