/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Edit policy who manages bend points.
 *
 * Project:  Math2Mat
 *
 * @file EditBendPointPolicy.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent the edit policy who manages bend points. It create moved, created and deleted command of bend point. 
 * 
 */
package m2m.frontend.dynamicview.editpolicies;

import m2m.frontend.dynamicview.commands.CreateBendpointCommand;
import m2m.frontend.dynamicview.commands.DeleteBendpointCommand;
import m2m.frontend.dynamicview.commands.MoveBendpointCommand;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.BendpointEditPolicy;
import org.eclipse.gef.requests.BendpointRequest;


public class EditBendPointPolicy extends BendpointEditPolicy
{ 
	/**
	 * Create command for mouving a bend point.
	 * @param request The request containing informations about bend point deplacement
	 */
	protected Command getMoveBendpointCommand(BendpointRequest request) 
	{
		MoveBendpointCommand com = new MoveBendpointCommand();

		/* Get the referenced point of source ans target anchor of the connection */
		Point p = request.getLocation();
		Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		Point ref2 = getConnection().getTargetAnchor().getReferencePoint();
		
		/* Initialisation of the command with the request and referenced points */
		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setConnection((m2m.frontend.dynamicview.model.Connection)request.getSource().getModel());
		com.setIndex(request.getIndex());
		
		return com;
	}
	
	
	/**
	 * Create command for creating bend points.
	 * @param request The request containing information about bend point creation
	 */
	@Override
	protected Command getCreateBendpointCommand(BendpointRequest request) 
	{
		CreateBendpointCommand com = new CreateBendpointCommand();
		
		/* Get the referenced point of source ans target anchor of the connection */
		Point p = request.getLocation();
		Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		Point ref2 = getConnection().getTargetAnchor().getReferencePoint();
		
		/* Initialisation of the command  with the request and referenced points */
		com.setIndex(request.getIndex());
		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setConnection((m2m.frontend.dynamicview.model.Connection)request.getSource().getModel());
		
		return com;
	}

	
	/**
	 * Create command for deleting a bend point.
	 * @param request The request containing information about bend point supression
	 */
	@Override
	protected Command getDeleteBendpointCommand(BendpointRequest request) 
	{
		/* Creation and initialisation of the command  with the request */
		DeleteBendpointCommand com = new DeleteBendpointCommand();
		com.setIndex(request.getIndex());
		com.setConnection((m2m.frontend.dynamicview.model.Connection)request.getSource().getModel());
		
		return com;
	}
}