/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Abtract command for layout modifications.
 *
 * Project:  Math2Mat
 *
 * @file AbstractLayoutCommand.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all main methods and attributes of a layout command.
 * 
 */
package m2m.frontend.dynamicview.commands;

import m2m.frontend.dynamicview.editPart.SchemaPart;
import m2m.frontend.dynamicview.model.Node;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;


public abstract class AbstractLayoutCommand extends Command
{
	/**
	 * The root editPart of the command.
	 */
	private SchemaPart schemaPart;
	/**
	 * The layout of the command.
	 */
	private Rectangle layout;
	
	
	/**
	 * Set the model of the command.
	 * @param model The model to the command to set
	 */
    public abstract void setModel(Node model);
    
    
    /**
     * Set eh root part of the commande.
     * @param part The part of the command to set
     */
    public void setRootPart(SchemaPart part)
    {
        this.schemaPart = part;
    }

    
    /**
     * Get the root part of the command.
     * @return the root part of the command
     */
    public SchemaPart getRootPart()
    {
        return schemaPart;
    }
    
    
    /**
     * Set the layout of the command.
     * @param rect The layout to the command to set
     */
    public void setLayout(Rectangle rect)
    {
		this.layout = rect;
    }
    
    
    /**
     * Get the layout of the command.
     * @return the layout of the command
     */
    public Rectangle getLayout()
    {
		return layout;
    }
}