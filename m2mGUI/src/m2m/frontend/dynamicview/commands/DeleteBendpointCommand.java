/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Command use for deleting a bend point.
 *
 * Project:  Math2Mat
 *
 * @file DeleteBendpointCommand.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represent commands used to delete bend points.
 * 
 */
package m2m.frontend.dynamicview.commands;

public class DeleteBendpointCommand extends AbstractBendPointCommand
{
    /**
     * Execution of the command.
     */	
	public void execute() 
	{
		getConnection().removeBendPoint(getIndex());
	}
}
