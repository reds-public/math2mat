/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Command for layout modifications of a PointConnection.
 *
 * Project:  Math2Mat
 *
 * @file PointConnectionChangeLayoutCommand.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains methods and attributes to modify the layout of a PointConnection.
 * 
 */
package m2m.frontend.dynamicview.commands;

import m2m.frontend.dynamicview.ThreadRouteConnections;
import m2m.frontend.dynamicview.model.Node;
import m2m.frontend.dynamicview.model.PointConnection;

import org.eclipse.swt.widgets.Display;


public class PointConnectionChangeLayoutCommand extends AbstractLayoutCommand 
{	     
	/**
	 * The model of the command.
	 */
	private PointConnection model;
	
	
    /**
     * Execution of the command.
     */
    public void execute() 
    {
		// Set the layout of the model
    	model.setLayout(getLayout());
    	
		// Route connections
    	Display.getCurrent().asyncExec(new ThreadRouteConnections());
    }
      
    
  	/**
  	 * Check if we can execute the command.
  	 * @return the succes of the control
  	 */
  	public boolean canExecute() 
  	{
      	return model.CanSetLayout(getLayout());
  	}
  	
  	
	/**
	 * Set the model of the command.
	 * @param model The model of the command to set
	 */
	public void setModel(Node model) 
	{
		this.model = (PointConnection)model;
	}
}