/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Command use for creating a bend point.
 *
 * Project:  Math2Mat
 *
 * @file CreateBendpointCommandjava
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class represented commands used to create bend points.
 * 
 */
package m2m.frontend.dynamicview.commands;

import m2m.frontend.dynamicview.model.BendPoint;

import org.eclipse.draw2d.geometry.Dimension;


public class CreateBendpointCommand extends AbstractBendPointCommand
{
	/**
	 * The bend point of the command.
	 */
	private BendPoint bendPoint = new BendPoint();
	/**
	 * The two dimension of the point of the bend point.
	 */
	private Dimension d1, d2;
	
	
    /**
     * Execution of the command.
     */	
	public void execute() 
	{
		/* Initialisation of the new bend point */
		bendPoint.setHeight(d1.height, d2.height);
		bendPoint.setWidth(d1.width, d2.width);
		bendPoint.setConnection(getConnection());		
		bendPoint.setIndex(getIndex());
		getConnection().addBendPoint(bendPoint);
	}
	
	
	/**
	 * Set the two dimensions of the bend point.
	 * @param dim1 The first  dimension to the bend point to set
	 * @param dim2 The second dimension to the bend point to set
	 */
	public void setRelativeDimensions(Dimension dim1, Dimension dim2) 
	{
		d1 = dim1;
		d2 = dim2;
	}
}
