/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Abtract command for bend point modifications.
 *
 * Project:  Math2Mat
 *
 * @file AbstractBendPointCommand.java
 * @author Daniel Molla
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class contains all main methods and attributes of a bend point command.
 * 
 */
package m2m.frontend.dynamicview.commands;

import m2m.frontend.dynamicview.model.Connection;

import org.eclipse.gef.commands.Command;


public abstract class AbstractBendPointCommand extends Command
{
	/**
	 * The index of the command.
	 */
	private int index;
	/**
	 * The connection of the command.
	 */
	private Connection connection;
	
	
	/**
	 * Set the index of the command.
	 * @param i The index to the command to set
	 */
	public void setIndex(int i) 
	{
		index = i;
	}
	
	/**
	 * Get the index of the command
	 * @return the index of the command
	 */
	public int getIndex() 
	{
		return index;
	}
	
	
	/**
	 * Set the connection of the command.
	 * @param connection The connection to the command to set
	 */
	public void setConnection(Connection connection) 
	{
		this.connection = connection;
	}
	
	
	/**
	 * Get the connection of the command.
	 * @return the connection of the command
	 */
	public Connection getConnection() 
	{
		return connection;
	}
}
