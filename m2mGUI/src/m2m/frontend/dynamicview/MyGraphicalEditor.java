/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Graphical dynamic view editor.
 *
 * Project:  Math2Mat
 *
 * @file MyGraphicalEditor.java
 * @author Daniel Molla
 *
 * @version 1.0
 *
 * @date: 15.07.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class configure and initialize the graphical dynamic view editor.
 *
 */
package m2m.frontend.dynamicview;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import m2m.frontend.dynamicview.editPart.SchemaEditPartFactory;
import m2m.frontend.view.Editor;


public class MyGraphicalEditor extends ViewPart 
{
	/**
	 * The ID of the editor.
	 */
    public static final String ID = "dynamicview.mygraphicaleditor";
    private ScrollingGraphicalViewer graphicalViewer;


	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) 
	{		
		Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		
		/* Initialisation of the graphicalViewer */
        graphicalViewer = new ScrollingGraphicalViewer();
 		graphicalViewer.createControl(parent);
 		graphicalViewer.setEditDomain(new EditDomain());
 		graphicalViewer.setEditPartFactory(new SchemaEditPartFactory());
 		graphicalViewer.setContents(editor.getModel().getSchema());
 		//((SchemaPart)(graphicalViewer.getRootEditPart().getContents())).Intersections();
 		graphicalViewer.getControl().addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent event) 
			{			
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				page.hideView(((MyGraphicalProperty)page.findView(MyGraphicalProperty.ID)));
				if((Editor)page.getActiveEditor() != null)
					((Editor)page.getActiveEditor()).setModel(null);
			}
		});
	}


	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {	
	}
	
	
	/**
	 * Get the graphical viewer of the view
	 * @return the graphical viewer of the view
	 */
	public GraphicalViewer getGraphicalViewer ()
	{
		return this.graphicalViewer;
	}
	
	
	/**
	 * Set the graphical viewer of the view
	 * @param the graphical viewer to set to the view
	 */
	public void setGraphicalViewer(ScrollingGraphicalViewer graphicalViewer) 
	{
		this.graphicalViewer = graphicalViewer;
	}
}