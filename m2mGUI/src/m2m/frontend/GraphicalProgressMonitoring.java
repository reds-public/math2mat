/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file GraphicalProgressMonitoring.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Aug 5, 2011
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.frontend;

import m2m.backend.processing.ProgressMonitoring;
import m2m.backend.project.M2MProject;

import org.eclipse.core.runtime.IProgressMonitor;


/**
 * @author ythoma
 *
 */
public class GraphicalProgressMonitoring extends ProgressMonitoring {

	public IProgressMonitor monitor;
	

	public void workedVhdlGeneration() {
		worked(workingTimeVHDL());
	}
	
	public void workedOctave() {
		worked(workingTimeOctave());
	}
	
	public void workedCompilation() {
		worked(workingTimeCompilation());
	}
	
	public void workedSimulationStep() {
		worked(workingTimeSimulationStep());
	}
	
	public void workedOneProject() {
		worked(1);
	}

	
	public GraphicalProgressMonitoring()
	{
		monitor=null;
	}
	
	public void setIProgressMonitor(IProgressMonitor monitor)
	{
		this.monitor=monitor;
	}
	
	private void worked(int value)
	{
		if (monitor!=null)
			monitor.worked(value);
	}
	
	
	public void subTask(M2MProject project,String taskName)
	{
		if (monitor!=null)
	//		monitor.subTask("Project "+project.getProjectFilename()+" :\n"+taskName);
		monitor.subTask("Project "+project.getStructTreatment().getName()+" : "+taskName);
	}
	
	public boolean isCanceled() {
		if (monitor!=null)
			return monitor.isCanceled();
		return false;
	}
}
