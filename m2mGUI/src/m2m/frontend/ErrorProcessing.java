/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ErrorProcessing.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Dec 9, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.frontend;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import m2m.backend.processing.Errors;

/**
 * @author ythoma
 *
 */
public class ErrorProcessing implements Runnable {

	private Errors error;
	private Display display;
	
	ErrorProcessing(Errors error,Display display) {
		this.error=error;
		this.display=display;
	}
	
	static public void displayError(Display display,String message) {
		MessageBox mess=new MessageBox(display.getActiveShell(), SWT.ICON_ERROR | SWT.OK);
		mess.setMessage(message);
		mess.setText("Error");
		mess.open();
	}
	
	static public void processLastError(Display display) {
		process(Errors.lastError(),display);
	}
	
	static public void process(Errors error,Display display) {
		ErrorProcessing err=new ErrorProcessing(error,display);
		display.asyncExec(err);
	}
	
	public void run() {
		if (error.getNum()!=Errors.ErrorNum.NOERROR) {
			displayError(display,error.errorMessage());
		}
	}
}
