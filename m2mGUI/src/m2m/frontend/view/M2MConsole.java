/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Console for GUI.
 *
 * Project:  Math2Mat
 *
 * @file M2MConsole.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 12.10.2009
 *
 * Author: Sebastien Masle
 *
 * Description: The console shows messages coming from the GUI itself, but also from applications it launches.
 * 
 */

package m2m.frontend.view;

import java.io.PrintStream;

import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.console.TextConsoleViewer;
import org.eclipse.ui.part.ViewPart;

public class M2MConsole extends ViewPart {

	public static final String ID = "m2mgui.view.m2mConsole";
	private MessageConsole console;
	private MessageConsoleStream stream;
	private MessageConsoleStream errStream;
	private MessageConsoleStream warStream;
	private TextConsoleViewer consoleViewer;
	
	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		console = new MessageConsole("Console", null, false);
		stream = console.newMessageStream();
		errStream = console.newMessageStream();
		errStream.setColor(new Color(null, 255, 0, 0));
		warStream = console.newMessageStream();
		warStream.setColor(new Color(null, 0, 0, 255));		
		System.setOut(new PrintStream(stream, true));
		System.setErr(new PrintStream(errStream, true));
		
		consoleViewer = new TextConsoleViewer(parent, console);
		
		Menu popupMenu = new Menu(consoleViewer.getTextWidget());
		MenuItem copyItem = new MenuItem(popupMenu, SWT.PUSH);
		copyItem.setText("Copy\tCtrl+C");
		copyItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				consoleViewer.getTextWidget().copy();
			}
		});
		
		consoleViewer.getTextWidget().setMenu(popupMenu);	
		
		consoleViewer.addTextListener(new ITextListener() {

			@Override
			public void textChanged(TextEvent event) {
				// TODO Auto-generated method stub
				StyledText textWidget = consoleViewer.getTextWidget();
		        if (textWidget != null) {
		            int lineCount = textWidget.getLineCount();
		            textWidget.setTopIndex(lineCount - 1);
		        }
			}
		
			
		});
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub	
	}
	
	public void addText(String text) {
		stream.println(text);
	}
	
	public void clearConsole() {
		console.clearConsole();
	}
	
	public MessageConsoleStream getErrStream() {
		return errStream;
	}
	
	public MessageConsoleStream getwarStream() {
		return warStream;
	}
	
	public void copy(){
		consoleViewer.getTextWidget().copy();
	}
}
