/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Main editor in Math2Mat GUI.
 *
 * Project:  Math2Mat
 *
 * @file Editor.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This editor shows math2mat source files with syntax coloring.
 * 
 */

package m2m.frontend.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;


import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.source.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ExtendedModifyEvent;
import org.eclipse.swt.custom.ExtendedModifyListener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISaveablePart2;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.part.EditorPart;


import m2m.backend.octaveparser.ParsingException;
import m2m.backend.project.M2MProject;
import m2m.backend.project.OptimisationProperties;
import m2m.backend.project.SimulationProperties;
import m2m.backend.utils.FileUtils;
import m2m.frontend.MatlabLineStyler;
import m2m.frontend.actions.TextChange;
import m2m.frontend.dynamicview.Model;
import m2m.frontend.dynamicview.MyGraphicalEditor;
import m2m.frontend.dynamicview.MyGraphicalProperty;
import m2m.frontend.dynamicview.ThreadRouteConnections;


public class Editor extends EditorPart implements ISaveablePart2
{

	public static final String ID = "m2mgui.editor";
	protected static final int UNDO_LIMIT = 200;
	private MatlabLineStyler lineStyler = new MatlabLineStyler();
	private FileInputStream fileToRead = null;
	private BufferedReader br;
	private Composite top;
	private StyledText styledText;
	private String filePath; 
	private File inputFile;
	private boolean parseDone = false;
	private Stack<TextChange> undoChanges;
	private Stack<TextChange> redoChanges;
	private boolean ignoreUndo = false;
	private Model model;
	private M2MProject m2mProject;
	
	public Shell getShell() {
		return this.getSite().getShell();
	}
	
	/**
	 * Create the editor
	 */
	public void createPartControl(final Composite parent) {
		undoChanges = new Stack<TextChange>();
		redoChanges = new Stack<TextChange>();

		top = new Composite(parent, SWT.NONE);
		FillLayout layout = new FillLayout();

		top.setLayout(layout);
		
		//Create rules for the SourceViewer
		CompositeRuler ruler = new CompositeRuler();
        LineNumberRulerColumn rulerColumn = new LineNumberRulerColumn();
        //add the line number decorator to the rules
        rulerColumn.setBackground(new Color(parent.getDisplay(), 230, 230, 230));
		ruler.addDecorator(1, rulerColumn);
		
		//Create the SourceViewer. Contrary to a simple StyledText, the SourceViewer allows to add decorator such as the
		//line number on the left column of the editor
		SourceViewer viewer = new SourceViewer(top, ruler, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		
		
		//set the document of the SourceViewer. If you don't do this, impossible to write text in the viewer... and don't know why
		Document document = new Document();
		viewer.setDocument(document);
		
		//Get the StyledText. The StyledText allows to add syntax coloring for special keywords
		styledText = viewer.getTextWidget();
		
		styledText.setLayoutData(new GridData(GridData.FILL_BOTH));
		//add the syntax coloring
		styledText.addLineStyleListener(lineStyler);
		
		//get the source file path
		filePath = this.getEditorInput().getToolTipText();
		
		try {
			fileToRead = new FileInputStream(filePath);
			br = new BufferedReader(new InputStreamReader(fileToRead));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			//print the text of the source file in the editor
			String strLine;
			while ((strLine = br.readLine()) != null)
				styledText.append(strLine + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

		styledText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) 
			{
				/* Add "*" to the name of the changed file */
				if (!m2mProject.isModified()) 
				{
					m2mProject.setModified(true);
					firePropertyChange(IEditorPart.PROP_DIRTY);
				}
		
				m2mProject.clearStructTreatment();
				setParseDone(true);
								
				/* Clear the console */
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				if((M2MConsole)page.findView(M2MConsole.ID) == null)
				{
					try {
						page.showView(M2MConsole.ID);
					}
					catch (PartInitException e1) {
						e1.printStackTrace();
					}
				}	
				((M2MConsole)page.findView(M2MConsole.ID)).clearConsole();
				
				/* Parsing of the text of the editor */
				try {
					m2mProject.getStructTreatment().parse(styledText.getText());
					m2mProject.getStructTreatment().modifyNumType(m2mProject.getProperties().getOptimisationProperties().getOptimisationDataType());
				}
				catch (ParsingException e1) {
					System.err.println(e1.getMessage());
					setParseDone(false);
				}
				if (parseDone)
				{
					getEditorSite().getActionBars().getStatusLineManager().setMessage("Parsing succesfull.");
				
					/* Get both dynamic views */		
		            MyGraphicalProperty graphProp = ((MyGraphicalProperty)page.findView(MyGraphicalProperty.ID));
		            MyGraphicalEditor graphEditor = ((MyGraphicalEditor)page.findView(MyGraphicalEditor.ID));
		            
		            if(graphProp != null && graphEditor != null)
		            {	
		            	/* Update both dynamic view */
			    		model = new Model (m2mProject.getStructTreatment(), m2mProject);
			            graphProp.getGraphicalViewer().setContents(model.getSchema());
			            graphEditor.getGraphicalViewer().setContents(model.getSchema());
		            	Display.getCurrent().asyncExec(new ThreadRouteConnections());
		            }
				}
				else
					getEditorSite().getActionBars().getStatusLineManager().setMessage("Parsing Error. The dynamic view does not match with the octave code.");	
			}
		});
	
		
		//remove the file entry in the Navigation view
		styledText.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent event) 
			{
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				page.hideView(((MyGraphicalEditor)page.findView(MyGraphicalEditor.ID)));
				page.hideView(((MyGraphicalProperty)page.findView(MyGraphicalProperty.ID)));
				model = null;

				NavigationView.remList(m2mProject.getProperties().getProjectFullFilename());
			}
		});
			
		
		//store undo information
		styledText.addExtendedModifyListener(new ExtendedModifyListener() {
			public void modifyText(ExtendedModifyEvent event) {
				if (!ignoreUndo) {
					undoChanges.push(new TextChange(event.start, event.length, event.replacedText));
					if (undoChanges.size() > UNDO_LIMIT)
						undoChanges.remove(0);
				} else {
					redoChanges.push(new TextChange(event.start, event.length, event.replacedText));
					if (redoChanges.size() > UNDO_LIMIT)
						redoChanges.remove(0);
				}
			}
		});
	
		
		//Right click menu
		Menu popupMenu = new Menu(styledText);
		
		//copy action
		MenuItem copyItem = new MenuItem(popupMenu, SWT.PUSH);
		copyItem.setText("Copy\tCtrl+C");
		copyItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				styledText.copy();
			}
		});
		
		//paste action
		MenuItem pasteItem = new MenuItem(popupMenu, SWT.PUSH);
		pasteItem.setText("Paste\tCtrl+V");
		pasteItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				styledText.paste();
			}
		});
		
		//cut action
		MenuItem cutItem = new MenuItem(popupMenu, SWT.PUSH);
		cutItem.setText("Cut\tCtrl+X");
		cutItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				styledText.cut();
			}
		});
		
		//add a separator
		new MenuItem(popupMenu, SWT.SEPARATOR);
		
		//delete action
		MenuItem deleteItem = new MenuItem(popupMenu, SWT.PUSH);
		deleteItem.setText("Delete\tDelete");
		deleteItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				delete();
			}
		});
		
		//add a separator
		new MenuItem(popupMenu, SWT.SEPARATOR);
		
		//comment/uncomment action
		MenuItem comUncomItem = new MenuItem(popupMenu, SWT.PUSH);
		comUncomItem.setText("Comment/Uncomment");
		comUncomItem.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				Point selRange = styledText.getSelectionRange();
				int firstLine = styledText.getLineAtOffset(selRange.x);
				int lastLine = styledText.getLineAtOffset(selRange.x + selRange.y);
				
				ignoreUndo = true;
				
				StyledText commentText = new StyledText(top, SWT.FILL);
				commentText.setText(styledText.getText(styledText.getOffsetAtLine(firstLine), selRange.x + selRange.y));
				Point commentRange = styledText.getSelectionRange();
				
				for(int i = 0; i <= lastLine - firstLine; i++) {
					//get the first character of the line
					char firstChar = commentText.getText().charAt(commentText.getOffsetAtLine(i));
					if (firstChar == '%') {
						commentText.replaceTextRange(commentText.getOffsetAtLine(i), 1, "");
						if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
							commentRange.x--;
						else
							commentRange.y--;
					} else {
						commentText.replaceTextRange(commentText.getOffsetAtLine(i), 0, "%");
						if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
							commentRange.x++;
						else
							commentRange.y++;
					}
				}
				
				ignoreUndo = false;
				
				styledText.replaceTextRange(styledText.getOffsetAtLine(firstLine), selRange.y + (selRange.x - styledText.getOffsetAtLine(firstLine) + 1), commentText.getText());
				styledText.setSelection(commentRange.x, commentRange.x + commentRange.y);
			}
		});
		
		
		//Add the right click menu
		styledText.setMenu(popupMenu);
	}
	
	public void copy() {
		styledText.copy();
	}
	
	public void cut() {
		styledText.cut();
	}
	
	public void paste() {
		styledText.paste();
	}

	public void selectAll() {
		styledText.selectAll();
	}
	
	public void delete() {
		int start = styledText.getSelectionRange().x;
		int length = styledText.getSelectionRange().y;
		styledText.replaceTextRange(start, length, "");
	}
	
	public void undo() {
		int oldLength = 0;
		int newLength = 0;
		
		if (!undoChanges.empty()) {
			TextChange change = (TextChange)undoChanges.pop();
			
			ignoreUndo = true;
			
			oldLength = styledText.getText().length();
			styledText.replaceTextRange(change.getStart(), change.getLength(), change.getReplacedText());
			newLength = styledText.getText().length();
			
			if ((oldLength - newLength) > 0)
				styledText.setCaretOffset(change.getStart());
			else
				styledText.setCaretOffset(change.getStart() + change.getReplacedText().length());
			
			styledText.setTopIndex(styledText.getLineAtOffset(change.getStart()));
			ignoreUndo = false;
		}
	}
	
	public void redo() {
		int oldLength = 0;
		int newLength = 0;
		
		if (!redoChanges.empty()) {
			TextChange change = (TextChange)redoChanges.pop();

			oldLength = styledText.getText().length();
			styledText.replaceTextRange(change.getStart(), change.getLength(), change.getReplacedText());
			newLength = styledText.getText().length();

			if ((oldLength - newLength) > 0)
				styledText.setCaretOffset(change.getStart());
			else
				styledText.setCaretOffset(change.getStart() + change.getReplacedText().length());
			
			styledText.setTopIndex(styledText.getLineAtOffset(change.getStart()));
		}
	}
	
	public void comUncom() {
		Point selRange = styledText.getSelectionRange();
		int firstLine = styledText.getLineAtOffset(selRange.x);
		int lastLine = styledText.getLineAtOffset(selRange.x + selRange.y);
		
		ignoreUndo = true;
		
		StyledText commentText = new StyledText(top, SWT.FILL);
		commentText.setText(styledText.getText(styledText.getOffsetAtLine(firstLine), selRange.x + selRange.y));
		Point commentRange = styledText.getSelectionRange();
		
		for(int i = 0; i <= lastLine - firstLine; i++) {
			//get the first character of the line
			char firstChar = commentText.getText().charAt(commentText.getOffsetAtLine(i));
			if (firstChar == '%') {
				//uncomment if the line is already commented
				commentText.replaceTextRange(commentText.getOffsetAtLine(i), 1, "");
				if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
					commentRange.x--;
				else
					commentRange.y--;
			} else {
				commentText.replaceTextRange(commentText.getOffsetAtLine(i), 0, "%");
				if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
					commentRange.x++;
				else
					commentRange.y++;
			}
		}
		
		ignoreUndo = false;
		
		styledText.replaceTextRange(styledText.getOffsetAtLine(firstLine), selRange.y + (selRange.x - styledText.getOffsetAtLine(firstLine) + 1), commentText.getText());
		styledText.setSelection(commentRange.x, commentRange.x + commentRange.y);
	}
	
	public void comment() {
		Point selRange = styledText.getSelectionRange();
		int firstLine = styledText.getLineAtOffset(selRange.x);
		int lastLine = styledText.getLineAtOffset(selRange.x + selRange.y);
		
		ignoreUndo = true;
		
		StyledText commentText = new StyledText(top, SWT.FILL);
		commentText.setText(styledText.getText(styledText.getOffsetAtLine(firstLine), selRange.x + selRange.y));
		Point commentRange = styledText.getSelectionRange();
		
		for(int i = 0; i <= lastLine - firstLine; i++) {
			commentText.replaceTextRange(commentText.getOffsetAtLine(i), 0, "%");
			if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
				commentRange.x++;
			else
				commentRange.y++;
		}
		
		ignoreUndo = false;
		
		styledText.replaceTextRange(styledText.getOffsetAtLine(firstLine), selRange.y + (selRange.x - styledText.getOffsetAtLine(firstLine) + 1), commentText.getText());
		styledText.setSelection(commentRange.x, commentRange.x + commentRange.y);
	}
	
	public void uncomment() {
		Point selRange = styledText.getSelectionRange();
		int firstLine = styledText.getLineAtOffset(selRange.x);
		int lastLine = styledText.getLineAtOffset(selRange.x + selRange.y);
		
		ignoreUndo = true;
		
		StyledText commentText = new StyledText(top, SWT.FILL);
		commentText.setText(styledText.getText(styledText.getOffsetAtLine(firstLine), selRange.x + selRange.y));
		Point commentRange = styledText.getSelectionRange();
		
		for(int i = 0; i <= lastLine - firstLine; i++) {
			//get the first character of the line
			char firstChar = commentText.getText().charAt(commentText.getOffsetAtLine(i));
			if (firstChar == '%')
				commentText.replaceTextRange(commentText.getOffsetAtLine(i), 1, "");
			if (i == 0 && selRange.x != styledText.getOffsetAtLine(firstLine))
				commentRange.x--;
			else
				commentRange.y--;
		}
		
		ignoreUndo = false;
		
		styledText.replaceTextRange(styledText.getOffsetAtLine(firstLine), selRange.y + (selRange.x - styledText.getOffsetAtLine(firstLine) + 1), commentText.getText());
		styledText.setSelection(commentRange.x, commentRange.x + commentRange.y);
	}
	
	public void setFocus() {

	}



	@Override
	/**
	 * Define "Save as..." action.
	 */
	public void doSaveAs() {
		String fileName; //name of the file entered in the "Save dialog box"
		FileDialog dialog = new FileDialog(top.getShell(), SWT.SAVE);
		dialog.setText("Save File");
		dialog.setFilterPath(m2mProject.getProperties().getAbsoluteSourceFilename());
        String[] filterExt = { "*.m" };
        dialog.setFilterExtensions(filterExt);
        String path = dialog.open();
        if (path == null)
            return;

        //test if the name of the file the user wants to save contains the chosen extension or not
        //if not, add the extension to the file name
        if ((path.lastIndexOf(".") == -1 ? "" : path.substring(path.lastIndexOf(".")+1, path.length())).equalsIgnoreCase(dialog.getFilterExtensions()[dialog.getFilterIndex()].substring(2))) {
        	fileName = path;
        } else {
        	String filterExtension = dialog.getFilterExtensions()[dialog.getFilterIndex()];
        	fileName = path+filterExtension.substring(1, filterExtension.length());
        }
        
        if (!FileUtils.copyFile(m2mProject.getProperties().getAbsoluteSourceFilename(),fileName))
        return;
        
        File file = new File(fileName);
        m2mProject.getProperties().setSourceFile(file);

        this.setPartName(m2mProject.getSourceFile().getName());
            

		try {
			IFileStore fileStore = EFS.getStore(m2mProject.getSourceFile().toURI());
    		this.setInput(new FileStoreEditorInput(fileStore));
		} catch (CoreException e) {
			e.printStackTrace();
		}
        	
        	saveProject();
        //file has been saved, reset the boolean "fileModified"
        firePropertyChange(IEditorPart.PROP_DIRTY);
        
        	}

	/**
	 * Define save action.
	 */
	public void doSave() {

		//get the file extension
     //   String ext = (m2mProject.getProperties().getProjectFullFilename().lastIndexOf(".") == -1 ? "" :
     //   	m2mProject.getProperties().getProjectFullFilename().substring(m2mProject.getProperties().getProjectFullFilename().lastIndexOf(".")+1, m2mProject.getProperties().getProjectFullFilename().length()));
        
        try {
          	FileWriter fileWriter = new FileWriter(m2mProject.getSourceFile());
           	fileWriter.write(styledText.getText());
           	fileWriter.close();
        } catch (IOException e) {
        	MessageBox messageBox = new MessageBox(top.getShell(), SWT.ICON_ERROR | SWT.OK);
          	messageBox.setMessage("File I/O Error.");
          	messageBox.setText("Error");
          	messageBox.open();
          	return;
        }

        saveProject();
        
        //file has been saved, reset the boolean "fileModified"
        firePropertyChange(IEditorPart.PROP_DIRTY);
	}
	
	public void setModified(boolean modif) {
		/* Add "*" to the name of the changed file */
		if (m2mProject.isModified()!=modif) 
		{
			m2mProject.setModified(modif);
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	
	@Override
	/**
	 * Initialisation of the editor.
	 */
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		if (input != null) { 
			this.setSite(site);
			if (input instanceof M2MFileStoreEditorInput)
			{
				
				this.m2mProject= ((M2MFileStoreEditorInput)input).getProject();

//				this.setPartName(this.m2mProject.getSourceFile().getName());
				this.setPartName(new File(this.m2mProject.getProjectFilename()).getName());
				inputFile = this.m2mProject.getSourceFile();
				try {
					if (inputFile != null) {
						IFileStore fileStore= EFS.getStore(inputFile.toURI());
						this.setInput(new FileStoreEditorInput(fileStore));
					} else {
						
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean isDirty() {
		return m2mProject.isModified();
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}
	
	/**
	 * Get the M2MProject attribute of the editor.
	 * @return the M2MProject attribute of the editor
	 */
	public M2MProject getM2MProject() {
		return m2mProject;
	}
	
	/**
	 * Save the project properties.
	 */
	public void saveProject() {
		m2mProject.save();
	}
	
	/**
	 * Get input file.
	 * 
	 * @return Input file.
	 */
	public File getInputFile() {
		return inputFile;
	}
	
	/**
	 * Get the content of the editor.
	 * 
	 * @return Text in the editor.
	 */
	public String getText() {
		return styledText.getText();
	}
	
	
	/**
	 * Tells if the parsing has been done.
	 * 
	 * @return True if the parsing is done.
	 */
	public boolean getParseDone() {
		return parseDone;
	}
	
	/**
	 * Set optimisation properties.
	 * 
	 * @param optimProp Optimisation properties.
	 */
	public void setOptimisationProperties(OptimisationProperties optimProp) {
		m2mProject.getProperties().setOptimisationProperties(optimProp);
	}
	/**
	 * Set simulation properties.
	 * 
	 * @param simProp Simulation properties.
	 */
	public void setSimulationProperties(SimulationProperties simProp) {
		m2mProject.getProperties().setSimulationProperties(simProp);
	}
	
	/**
	 * Update the boolean that tells if the parsing has been done.
	 * 
	 * @param bool Value to set.
	 */
	public void setParseDone(boolean bool) {
		this.parseDone = bool;
	}
	

	
	
	/**
	 * Set the graphical model of the editor.
	 * @param model the graphical model to the editor to set
	 */
	public void setModel(Model model) {
		this.model = model;
	}
	
	/**
	 * Get the graphical model of the editor.
	 * @return the graphical model of the editor
	 */
	public Model getModel() {
		return model;
	}
	
	/**
	 * Get the styled text of the editor.
	 * @return the styled text of the editor
	 */
	public StyledText getStyledText() {
		return styledText;
	}
	
	/**
	 * Get the file path of the editor.
	 * @return the file path of the editor
	 */
	public String getFilePath()
	{
		return filePath;
	}

	@Override
	/**
	 * Define promptToSaveOnClose action.
	 */
	public int promptToSaveOnClose() 
	{
		MessageDialog dg = new MessageDialog(getEditorSite().getShell(), "Math2Mat", null,
                m2mProject.getSourceFile().getName()+" has changed, do you want to save changes?",
                MessageDialog.QUESTION_WITH_CANCEL, 
                new String[] {IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL, IDialogConstants.CANCEL_LABEL},
                0
                );
		
		int result = dg.open();
		
		if(result == 0)
			doSave();

		return result;
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {
	}
}
