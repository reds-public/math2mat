/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Navigation view of the GUI.
 *
 * Project:  Math2Mat
 *
 * @file NavigationView.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This view shows all opened files in the editor.
 * 
 */

package m2m.frontend.view;

import java.io.File;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.part.ViewPart;

import m2m.backend.project.M2MProject;
import m2m.frontend.ApplicationActionBarAdvisor;
import m2m.frontend.dynamicview.MyGraphicalEditor;
import m2m.frontend.dynamicview.MyGraphicalProperty;


public class NavigationView extends ViewPart {
	public static final String ID = "m2mgui.navigationView";
	private static List list;
	
	/**
	 * Add a file to the list.
	 * 
	 * @param path Path to the file to add.
	 */
	public static void addList (String path) {
		list.add(path);
		ApplicationActionBarAdvisor.getInstance().showOpenMenu(true);
	}
	
	/**
	 * Remove a file from the list.
	 * 
	 * @param path Path to the file to remove.
	 */
	public static void remList (String path) {
		list.remove(path);
		ApplicationActionBarAdvisor.getInstance().showOpenMenu(list.getItemCount()!=0);
	}
	
	/**
	 * Replace an existing file path by an other.
	 * 
	 * @param path Path to the existing file in the list.
	 * @param newPath New path of the file.
	 */
	public static void replaceList (String path, String newPath) {
		int index = list.indexOf(path);
		list.remove(path);
		list.add(newPath, index);
	}
	
	/**
     * This is a callback that will allow us to create the viewer and initialize
     * it.
     */
	public void createPartControl(Composite parent) {
		//create the list
		list = new List(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		
		//add double clic listener to the list
		list.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				File file = new File(list.getItem(list.getFocusIndex()));
				
				M2MProject m2mProject = new M2MProject();
				m2mProject.openProject(file.getAbsolutePath());
				
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IFileStore fileStore= EFS.getStore(m2mProject.getSourceFile().toURI());
					page.openEditor(new FileStoreEditorInput(fileStore), Editor.ID);
				} catch (CoreException e1) {
					e1.printStackTrace();
				}
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//add listener that listen when a part of the page is activated
		getSite().getPage().addPartListener(new IPartListener() {

			@Override
			public void partActivated(IWorkbenchPart part) {
   				if (part instanceof Editor)
   				{	
   					//highlight the list item which correspond with the active editor
   					list.setSelection(list.indexOf(((Editor)part).getM2MProject().getProperties().getProjectFullFilename()));
   					getSite().getShell().getDisplay().asyncExec (new Runnable() {
   						public void run() {					
	       					if (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor() != null)
	       					{			  
	       			    		/* Get the structure of the editor */
	       			    		Editor editor = (Editor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
	       				        try 
	       				        {
	       				        	/* Creation of two editors */
	       				            IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	       				            MyGraphicalProperty graphProp = ((MyGraphicalProperty)page.findView(MyGraphicalProperty.ID));
	       				            MyGraphicalEditor graphEditor = ((MyGraphicalEditor)page.findView(MyGraphicalEditor.ID));
	       				            
	       				            if(editor.getModel() != null && graphProp != null)
	       				            {
	       					            graphProp.getGraphicalViewer().setContents(editor.getModel().getSchema());
	       					            graphEditor.getGraphicalViewer().setContents(editor.getModel().getSchema());
	       				            }
	       				            else if(editor.getModel() != null && graphProp == null)
	       				            {
	       					            page.showView(MyGraphicalEditor.ID);     
	       					            page.showView(MyGraphicalProperty.ID);   
	       					            setFocus();
	       				            }
	       				            else if(editor.getModel() == null && graphProp != null)
	       				            {
	       				            	page.hideView(graphProp);     
	       					            page.hideView(graphEditor);   
	       				            }
	       				        } 
	       				        catch (Exception e) 
	       				        {
	       				            e.printStackTrace();
	       				        } 
	       					}
//	       					else
//	       						System.out.println("Launch of the dynamic view error! Select the octave editor.");
   						}
	                 }); 
                 }
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) {

			}

			@Override
			public void partClosed(IWorkbenchPart part) {

			}

			@Override
			public void partDeactivated(IWorkbenchPart part) {

			}

			@Override
			public void partOpened(IWorkbenchPart part) {
				
			}
			
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		list.setFocus();
	}
}
