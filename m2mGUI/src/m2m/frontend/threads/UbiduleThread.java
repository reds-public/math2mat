/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file UbiduleThread.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Dec 9, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.frontend.threads;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ExternalRuns;
import m2m.frontend.ErrorProcessing;
import m2m.frontend.view.Editor;

/**
 * @author ythoma
 *
 */
public class UbiduleThread extends Thread {

	private Editor editor;
	

	public UbiduleThread (Editor editor) 
	{
		this.editor = editor;
	}
	
	public void run() {
		Errors.clearError();
		ExternalRuns.runUbidule(editor.getM2MProject());
		ErrorProcessing.processLastError(editor.getShell().getDisplay());
	}

}
