/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											STDLogic
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 6 mai 2009
 * Description :
 ******************************************************************************/

package m2m.backend.vhdl;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *	Convert a floating point Java number to his binary representation and
 * vice versa
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class BinaryFloatingPoint {
	
	private static BinaryFloatingPoint instance = null;
	private int sizeMant;
	private int sizeExp;
	
	protected BinaryFloatingPoint() {
		this.sizeMant = 23;
		this.sizeExp = 8;
	}
	
	public static BinaryFloatingPoint getInstance() {
      if(instance == null) {
         instance = new BinaryFloatingPoint();
      }
      return instance;
   }
	
	private String toBinary(double value) {
		String sVal = new String();
		String hex = Double.toHexString(value);
		Pattern p = Pattern.compile("(-?)0x(1|0).(.*)p(.*)");
		Matcher m = p.matcher(hex);
		boolean b = m.matches();
		if (!b) {
			return sVal;
		}
		Boolean sign = m.group(1).equals("-");
		long mant = Long.parseLong(m.group(3), 16);
		long exp = Long.parseLong(m.group(4), 16);
		sVal += (sign)?"1":"0";
		if (m.group(2).equals("1")) {
			sVal += this.createBinaryExp(exp);
			sVal += this.createBinaryMant(mant);
		} else {
			sVal += sizeExp == 8 ? this.createBinaryExp(-127) : this.createBinaryExp(-1023);
			sVal += this.createBinaryMant(0);
		}
		return sVal;
	}
	
	private double toFloat(String valueBit) {
		double val = 0;
		double biais = Math.pow(2,this.sizeExp-1.0)-1.0;
		String sVal = new String();
		String sign = new String();
		String sExp = new String();
		String sMant = new String();
		String hexExp = new String();
		String hexMant = new String();
		try {
			sign = (valueBit.substring(0, 1).equals("1"))?"-":"";
			sExp = valueBit.substring(1,this.sizeExp+1);
			sMant = valueBit.substring(this.sizeExp+1,
					  this.sizeExp+this.sizeMant+1);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("#Not a correct format number");
			e.printStackTrace();
			return 0.0;
		}
		for (int i=0; i<(4-this.sizeMant%4);i++) {
			sMant += "0";
		}
		int exp = Integer.parseInt(sExp,2);
		int mant = Integer.parseInt(sMant,2);
		if (exp == 0 && mant == 0) {
			System.out.println("#Null number");
			sVal = sign+"0x0.0p0";
			val = 0.0;
		} else if (exp == 0) {
			System.out.println("#Denormalized number");
			hexMant = Integer.toHexString(mant);
			sVal = sign+"0x0."+hexMant+"p0";
			val = Double.valueOf(sVal);
		} else {
			hexExp = Integer.toHexString(exp-(int)biais);
			hexMant = Integer.toHexString(mant);
			sVal = sign+"0x1."+hexMant+"p"+hexExp;
			val = Double.valueOf(sVal);
		}
		return val;
	}
	
	private String createBinaryExp(long exp) {
		String sExp = new String();
		double biais = Math.pow(2,this.sizeExp-1.0)-1.0;
		String convert = Long.toBinaryString(exp+(int)biais);
		int length = convert.length();
		if (length < this.sizeExp) {
			for (int i=0; i<(this.sizeExp-length); i++) {
				sExp += "0";
			}
			sExp += convert;
		} else if (length > this.sizeExp) {
			convert = convert.substring(0, this.sizeExp);
			sExp += convert;
		} else {
			sExp = convert;
		}
		return sExp;
	}
	
	private String createBinaryMant(long mant) {
		String sMant = new String();
		String convertTmp = Long.toBinaryString(mant);
		String sMantTmp = Long.toHexString(mant);
		String convert = new String();
		//complete with '0' on the left if convertTmp.length is not a multiple of 4
		if (convertTmp.length() < (sMantTmp.length()*4)) {
			while (convertTmp.length() < (sMantTmp.length()*4)) {
				convertTmp = "0" + convertTmp;
			}
		}
		convert += convertTmp;
		int length = convert.length();
		//complete with '0' on the right if the length of convert is different from the size of the mantissa
		if (length < this.sizeMant) {
			sMant += convert;
			for (int i=0; i<(this.sizeMant-length); i++) {
				sMant += "0";
			}
		} else if (length > this.sizeMant) {
			convert = convert.substring(0, this.sizeMant);
			sMant += convert;
		} else {
			sMant = convert;
		}
		return sMant;
	}
	
	public String getBinary(double value) {
		return this.toBinary(value);
	}
	public double getValue(String valBit) {
		return this.toFloat(valBit);
	}
	public void setSizeMant(int size) {
		this.sizeMant = size;
	}
	public int getSizeMant() {
		return this.sizeMant;
	}
	public void setSizeExp(int size) {
		this.sizeExp = size;
	}
	public int getSizeExp() {
		return this.sizeExp;
	}	
}
