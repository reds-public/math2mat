/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											VHDLCreator
 ******************************************************************************
 * Auteur		: Daniel Molla
 * Date			: 11 avr. 2011
 * Description :
 ******************************************************************************/
package m2m.backend.vhdl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.project.M2MProject;
import m2m.backend.project.ProjectRef;
import m2m.backend.structure.Addition;
import m2m.backend.structure.Assignment;
import m2m.backend.structure.Comparison;
import m2m.backend.structure.Element;
import m2m.backend.structure.Equal;
import m2m.backend.structure.Function;
import m2m.backend.structure.Greater;
import m2m.backend.structure.GreaterEqual;
import m2m.backend.structure.IfThenElse;
import m2m.backend.structure.Less;
import m2m.backend.structure.LessEqual;
import m2m.backend.structure.LogicOperation;
import m2m.backend.structure.Loop;
import m2m.backend.structure.LoopFor;
import m2m.backend.structure.Multiplexer;
import m2m.backend.structure.Negation;
import m2m.backend.structure.Operation;
import m2m.backend.structure.SimpleVariable;

/**
 * Create VHDL code from internal structure
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class VHDLCreator extends ProjectRef 
{	/**
	 * Describe all multi usi-signal in the root structure
	 */
	private class MultiUseSignals 
	{		
		/**
		 * All multi-use variables
		 */
		private Vector<SimpleVariable> variables;
		/**
		 * All cost associated to each variables
		 */
		private Vector<Vector<Integer>> cost;

		
		/**
		 * Constructor of a MultiUseSignals variable.
		 * @param vectorVar the vector who contain all SimpleVariable
		 * @param vectorCost the vector who contain all cost associated to each SimpleVariable
		 */
		public MultiUseSignals(Vector<SimpleVariable> vectorVar, Vector<Vector<Integer>> vectorCost) 
		{
			this.variables = vectorVar;
			this.cost = vectorCost;
		}
		
		
		/**
		 * Set the number of multi-used SimpleVariable.
		 * @param index Index of the SimpleVariable to set information
		 */
		public void setNbrMultSimpleVariable(Function f, SimpleVariable e) 
		{
			Vector<Integer> costs = cost.elementAt(variables.indexOf(e));
			SimpleVariable var;
			boolean flag = false;
			
			for(int i = 0; i < costs.size(); i++) {		
				var = new SimpleVariable(e.getName() + "_" + (i));
				var.setCost(costs.elementAt(i));
				var.setType("mult");
				if(e.getLoopOutputCost(f) != 0 && e.getLoopOutputConnect(f) && !flag) {
					var.setLoopOutputConnect(f, true);
					var.setLoopOutputCost(f, e.getLoopOutputCost(f));	
					var.setInputLoopVariable(e.getInputLoopVariable(f), f);
					flag = true;
				}
				else if(flag) {
					var.setLoopOutputCost(f, e.getLoopOutputCost(f));
				}
				
				e.getVariableMap().get(f).getMultSimpleVar().add(var);
				
				if(f instanceof Loop && costs.elementAt(i) == f.getMaxCost()) {
					var.setSimpleFifo(true);
					var.setSimpleFifoCost(f.getMaxCost()- getMinCost(variables.indexOf(e)));
					
					SimpleVariable loopVar = ((Loop)f).getLoopVariable(e);
					if(loopVar != null)
						var.setOutputLoopVariable(loopVar);
					else
						var.setInputLoopVariable(e, f);
				}
			}
			
			e.setLoopOutputConnect(f, false);
			e.setSimpleFifo(false);
		}
		
		
		/**
		 * Get the min cost of a multiUseSginal
		 * @param index Index of the SimpleVariable to get minimum cost
		 */
		public int getMinCost(int index)
		{
			int minCost = cost.elementAt(index).elementAt(0);
			for(Integer val : cost.elementAt(index))
				if(minCost > val)
					minCost = val;
			
			return minCost;
		}
		
		
		/**
		 * Get the min cost of a multiUseSginal
		 * @param var the SimpleVariable to get minimum cost
		 */
		public int getMinCost(SimpleVariable var) 
		{
			return getMinCost(variables.indexOf(var));
		}
	}
	
	
	/**
	 * Internal structure root
	 */
	private Function top;
	/**
	 * Contain multi-use signal for each function
	 */
	private HashMap<Function, MultiUseSignals> multUseSignal;
	/**
	 * Data type of the project
	 */
	private NumType numType;
	

//	/**
//	 * Significant size
//	 */
//	private int SIGNSIZE;
//	/**
//	 * Exponent size
//	 */
//	private int EXPSIZE;
	
	
	/**
	 * Constructor, set the structure root.
	 * Format number is standard IEEE 754-2008, 23 bits significand and 8
	 * bits exponent.
	 * @param top Structure root
	 * @param project 
	 */
	public VHDLCreator(Function top, M2MProject project) 
	{
		this.top = top;
		setProject(project);
		multUseSignal = new HashMap<Function, MultiUseSignals>();
		findMultiUseSignals(top);
		numType = project.getOptimisationProperties().getOptimisationDataType();
//		SIGNSIZE = 23;
//		EXPSIZE = 8;
// 		this.vectorTypeList = new ArrayList<Integer>();
	}

	
	/**
	 * Constructor, set the structure root, the significant size and the
	 * exponent size.
	 * @param top Structure root
	 * @param significand Significand size 
	 * @param exp Exponent size
	 */
	public VHDLCreator(Function top, M2MProject project ,int significand, int exp) {
		this.top = top;
		setProject(project);
		multUseSignal = new HashMap<Function, MultiUseSignals>();
		findMultiUseSignals(top);
		numType = project.getOptimisationProperties().getOptimisationDataType();
//		SIGNSIZE = significand;
//		EXPSIZE = exp;
// 		this.vectorTypeList = new ArrayList<Integer>();
	}
	
	
	
	/**
	 * Parse the structure before VHDL generation.
	 */
	private void parse(Function f, Function parentFunction)
	{
		/* Update name of Loop inputs for mapping the function */
		if(f instanceof Loop) {	
			int cost;
			int minCost = 0;
			SimpleVariable var;
			
			/* get all inputs of the function */
			Vector<SimpleVariable> inputs = new Vector<SimpleVariable>();
			for(int i = 0; i < f.getInput().size(); i++)
				inputs.add((SimpleVariable) f.getInput().elementAt(i));
			
			/* Check if there is common variables */
			Vector<Boolean> dependFlag = new Vector<Boolean>();
			for(int i = 0; i < f.getInput().size(); i++) {
				dependFlag.add(false);
				for(int j = 0; j < f.getInput().size(); j++) {
					if(j != i) {
						for(Element e : inputs.elementAt(j).getVariableDependencies()) {
							if(inputs.elementAt(i).getVariableDependencies().contains(e))
								dependFlag.setElementAt(true, i);
						}
					}
				}
			}
			
			/* Map the VHDL SimpleVariable with each input of the function */
			cost = ((SimpleVariable)f.getOutput().elementAt(0)).getCost()-f.getRelativeMaxCost();
			MultiUseSignals mus = multUseSignal.get(parentFunction);
			for(int i = 0; i < inputs.size(); i++) {
				var = inputs.elementAt(i);
				if(mus.variables.contains(var)) {
					var.setVarFunctionMapping(var.getVariableMap().get(parentFunction).getMapVHDLSimpleVariable(var.getCost()), f);		
					minCost = mus.getMinCost(mus.variables.indexOf(inputs.elementAt(i)));
				}
				else
					var.setVarFunctionMapping(var, f);		
				
				for(int j = 0 ; j < inputs.size(); j++) {
					if(i != j && inputs.elementAt(i).getCost() < inputs.elementAt(j).getCost()) {
						if(project.getProperties().getOptimisationProperties().getFifoCompensation() ||  parentFunction instanceof Loop || dependFlag.elementAt(i) || (mus.variables.contains(inputs.elementAt(i)) && cost-inputs.elementAt(i).getCost() != minCost))
							var.getVarFunctionMapping(f).setSimpleFifoCost(cost-inputs.elementAt(i).getCost()+1);	
					}
				}
			}
		}
		
		/* Go through all internal SimpleVariable and create MapVHDLSimpleVariable for multiUseSignal */
		for(Element e : f.getInternalVars()) {
			if(e instanceof SimpleVariable && multUseSignal.get(f).variables.contains((SimpleVariable) e)) {
				((SimpleVariable) e).getVariableMap().put(f, ((SimpleVariable) e).new MapVHDLSimpleVariable());
				multUseSignal.get(f).setNbrMultSimpleVariable(f, (SimpleVariable) e);	
			}
		}
		
		parse(f.getBody(), f);
	}

	
	/**
	 * Parse a body contained structure.
	 * @param elements All elements contained in the body
	 * @param parentFunction the parent function of the vector
	 */
	private void parse(Vector<Element> elements, Function parentFunction)
	{
		for(Element e : elements) {
			if(e instanceof Operation)
				parse((Operation)e, parentFunction);
			else if(e instanceof IfThenElse)
				parse((IfThenElse)e, parentFunction);
			else if (e instanceof Function)
				parse((Function)e, parentFunction);
		}			
	}


	/**
	 * Parse an operation contained in the structure.
	 * @param op The operation contained in the structure
	 * @param parentFunction the parent function of the operation 
	 */
	private void parse(Operation op, Function parentFunction)
	{
		int cost;
		int costI;
		int costJ;
		int internCost;
		int minCost = 0;
		Vector<SimpleVariable> inputs = new Vector<SimpleVariable>();
		Vector<Boolean> dependFlag = new Vector<Boolean>();
		
		/* Get the result of Comparison and LogicOperation */
		if(op instanceof Comparison)
			((SimpleVariable) op.getOutputAt(0)).setType("resultComp");
		else if(op instanceof LogicOperation)
			((SimpleVariable) op.getOutputAt(0)).setType("cond");
		
		/* Initialization of inputs */
		for(int i = 0; i < op.getInput().size(); i++)
			inputs.add((SimpleVariable) op.getInputAt(i));
		if(op instanceof Multiplexer)
			inputs.add((SimpleVariable)((Multiplexer)op).getSel());
		
		/* Check if there is common variables */
		for(int i = 0; i < inputs.size(); i++) {
			dependFlag.add(false);
			for(int j = 0; j < inputs.size(); j++) {
				if(j != i) {
					for(Element e : inputs.elementAt(j).getVariableDependencies()) {
						if(inputs.elementAt(i).getVariableDependencies().contains(e))
							dependFlag.setElementAt(true, i);
					}
				}
			}
		}
		
		/* Map the VHDL SimpleVariable with each input of the operation */
		MultiUseSignals mus = multUseSignal.get(parentFunction);
		if(op.getBlock() != null) {
			cost = ((SimpleVariable)op.getOutputAt(0)).getCost()-op.getBlock().latencyTime()-1;
			internCost = parentFunction instanceof Loop && parentFunction.getOutput().contains((SimpleVariable)op.getOutputAt(0)) ? ((Loop)parentFunction).getRelativeMaxCost()-op.getBlock().latencyTime()-2 : cost;
			for(int i = 0 ; i < inputs.size(); i++) {
				costI = parentFunction instanceof Loop && parentFunction.getInput().contains(inputs.elementAt(i)) ? 0 : inputs.elementAt(i).getCost();
				costI = parentFunction instanceof Loop && parentFunction.getOutput().contains(inputs.elementAt(i)) && inputs.elementAt(i).getLoopOutputCost(parentFunction) != 0 ? inputs.elementAt(i).getLoopOutputCost(parentFunction) : costI;
				SimpleVariable var = inputs.elementAt(i);
				if(mus.variables.contains(inputs.elementAt(i))) {
					var = inputs.elementAt(i).getVariableMap().get(parentFunction).getMapVHDLSimpleVariable(cost);
					if(i == 2)
						((Multiplexer)op).setSel(var);
					else
						op.setInputAt(op.getInput().indexOf(inputs.elementAt(i)), var);
					minCost = mus.getMinCost(mus.variables.indexOf(inputs.elementAt(i)));
				}
				for(int j = 0 ; j < inputs.size(); j++) {
					costJ = parentFunction instanceof Loop && parentFunction.getInput().contains(inputs.elementAt(j)) ? 0 : inputs.elementAt(j).getCost();
					costJ = parentFunction instanceof Loop && parentFunction.getOutput().contains(inputs.elementAt(j)) && inputs.elementAt(j).getLoopOutputCost(parentFunction) != 0 ? inputs.elementAt(j).getLoopOutputCost(parentFunction) : costJ;
					if(i != j && costI < costJ) {
						if(project.getProperties().getOptimisationProperties().getFifoCompensation() ||  parentFunction instanceof Loop || dependFlag.elementAt(i) || (mus.variables.contains(inputs.elementAt(i)) && cost-costI != minCost))
							var.setSimpleFifoCost(internCost-costI);	
					}
				}
			}
		}
		else if(op instanceof Assignment) {
			cost = ((SimpleVariable)op.getOutputAt(0)).getCost();
			if(op.getInputAt(0) instanceof SimpleVariable && mus.variables.contains((SimpleVariable) op.getInputAt(0))) {
				SimpleVariable var = ((SimpleVariable)op.getInputAt(0)).getVariableMap().get(parentFunction).getMapVHDLSimpleVariable(cost);
				op.setInputAt(op.getInput().indexOf(op.getInputAt(0)), var);
			}
		}
	}
	
	
	/**
	 * Parse an ifThenElse contained in the structure.
	 * @param op The ifThenElse contained in the structure
	 * @param parentFunction the parent function of the ifThenElse
	 */
	private void parse(IfThenElse ite, Function parentFunction)
	{
		if(ite.getName().contains("loop")) {
			if(parentFunction.getBody().firstElement().equals(ite)) 
				parse(ite.getCond(), parentFunction);
		}
		else
			parse(ite.getCond(), parentFunction);
		
		parse(ite.getBodyTrue(), parentFunction);
		parse(ite.getBodyFalse(), parentFunction);
	}
	
	
	/**
	 * Find if an output signal is used at multiples operator inputs.
	 * @param f the parent function
	 * @return A MultiUseSignals who contains all multi-use signal and their cost
	 */
	private void findMultiUseSignals(Function f)
	{	
		Vector<SimpleVariable> multSignals = new Vector<SimpleVariable>();
		Vector<Vector<Integer>> costMultSignals = new Vector<Vector<Integer>>();	
		MultiUseSignals multiUseSignals = new MultiUseSignals(multSignals, costMultSignals);
		multUseSignal.put(f, multiUseSignals);
		
		/* Get all variables and operations who are contained into the structure */
		Vector<Operation> operations = findOperations(f.getBody());
		Vector<SimpleVariable> variables = findVariables(operations);			
		
		/* Set init loop variables as multiUseSignal */
		SimpleVariable var;	
		SimpleVariable varLoop;	
		Vector<SimpleVariable> loopVariables = new Vector<SimpleVariable>();
		if(f instanceof Loop) {
			for(Element e : f.getInput()) {
				var = (SimpleVariable) e;
				if(!var.getType().equalsIgnoreCase("const") && !var.getType().equalsIgnoreCase("forloopcond") && ((Loop)f).isInitVariable(var) == null) {				
					var.getMultiCost().add(f.getMaxCost());
					loopVariables.add(var);
				}
			}
			
			var = (SimpleVariable)f.getInternalVars().elementAt(0);
			if(var.getType().equalsIgnoreCase("iter")) {
				SimpleVariable startVar =((LoopFor)f).getStart();
				SimpleVariable incrVar =((LoopFor)f).getIncr();
				var.getMultiCost().add(startVar.getType().equalsIgnoreCase("const") ? 0 : startVar.getCost());
				if(!startVar.getType().equalsIgnoreCase("const") && !startVar.getType().equalsIgnoreCase("forloopcond")) {
					startVar.getMultiCost().add(0);
					variables.add(startVar);
				}
				if(!incrVar.getType().equalsIgnoreCase("const") && !incrVar.getType().equalsIgnoreCase("forloopcond")) {
					incrVar.getMultiCost().add(0);
					variables.add(incrVar);
				}
				loopVariables.add(var);
			}
			
			for(Element e : f.getBody()) {
				if(e instanceof IfThenElse && e.getName().contains("loop")) {
					//TODO var = (SimpleVariable) ((IfThenElse)e).getOutput().firstElement();
					var = ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getOutputAt(0));
					varLoop = ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getInputAt(0));
					varLoop.setInputLoopVariable(var, f);					
					if(varLoop.getCost() != f.getRelativeMaxCost()-1 && !f.getOutput().contains(varLoop)) {
						varLoop.setLoopOutputCost(f, varLoop.getCost());
						varLoop.setLoopOutputConnect(f, true);
					}
					// Add cost only for loop output 
					if(f.getOutput().contains(varLoop))
						var.getMultiCost().add(f.getMaxCost());					
					loopVariables.add(var);
				}
			}
			
			/* Set the fifo of the loopFor condition */
			var = (SimpleVariable)((IfThenElse)f.getBody().elementAt(0)).getInternalVars().firstElement();
			if(var.getCost() != f.getMaxCost()) {
				var.setSimpleFifo(true);
				var.setSimpleFifoCost(f.getMaxCost()-var.getCost()+1);
			}
			
			variables.addAll(loopVariables);
			((Loop)f).setLoopVariables(loopVariables);
		}
		
		/* Set causal multiUseSignal */
		for(int i = 0; i < variables.size()-1; i++) {
			for(int j = i + 1; j < variables.size(); j++) {
				if(variables.elementAt(i).equals(variables.elementAt(j)) && !variables.elementAt(i).getType().equalsIgnoreCase("const")) {
					if(!multSignals.contains(variables.elementAt(i))) {					
						multSignals.add(variables.elementAt(i));
						costMultSignals.add(new Vector<Integer>());
						costMultSignals.elementAt(multSignals.indexOf(variables.elementAt(i))).addAll(variables.elementAt(i).getMultiCost());
					}
					variables.remove(j--);
				}
			}
			((SimpleVariable)variables.elementAt(i)).getMultiCost().clear();
		}
	}
	
	
	/**
	 * Get all component who are contained in a vector of elements.
	 * @param elements The vector of elements
	 * @return A vector who contain all components
	 * @throws VHDLException 
	 */
	private Vector<Element> findComponent(Vector<Element> elements) throws VHDLException
	{
		// The vector of operations to return
		Vector<Element> components = new Vector<Element>();
		
		/* Get all Components */
		for (Element e : elements) {
			if (e instanceof IfThenElse) {
				components.addAll(findComponent(((IfThenElse)e).getBodyFalse()));
				components.addAll(findComponent(((IfThenElse)e).getBodyTrue()));
				components.addAll(findComponent(((IfThenElse)e).getCond()));
			}			
			else {
				components.add(e);
			}
		}

		/* Remove operations that are used more than once */
		for (int i = 0; i < components.size() - 1; i++) {
			for (int j = i + 1; j < components.size(); j++) {
				if ( (components.elementAt(i) instanceof Operation) && (components.elementAt(j) instanceof Operation) &&
				    !(components.elementAt(i) instanceof Assignment) && !(components.elementAt(j) instanceof Assignment)) 
				{
					Element op1 = components.elementAt(i);
					Element op2 = components.elementAt(j);
					BuildingBlock blockOp1 = ((Operation)op1).getBlock();
					BuildingBlock blockOp2 = ((Operation)op2).getBlock();
					
					if (blockOp1 == null || blockOp2 == null) {
						if (blockOp1 == null && !(op1 instanceof Function))
							throw new VHDLException("No material description for the operation : " + op1.getName());
						if (blockOp2 == null && !(op2 instanceof Function))
							throw new VHDLException("No material description for the operation : " + op2.getName());
					} else if (((Operation)op1).getBlock().entityName().equalsIgnoreCase(((Operation)op2).getBlock().entityName()))
							components.remove(j--);
				}
			}
		}
		
		return components;
	}
	

	/**
	 * Get all operation who are contained in a vector of elements.
	 * @param elements The vector of elements
	 * @return A vector who contain all operations
	 */
	private Vector<Operation> findOperations(Vector<Element> elements)
	{
		// The vector of operations to return
		Vector<Operation> operations = new Vector<Operation>();
		
		for (Element e : elements) {
			if (e instanceof Function) {
				/* Recursive call for ifThenElse element */
				if (e instanceof IfThenElse) {
					/* Add only one time the loop condition to the list of operations */
					if(!e.getName().contains("loop") || (e.getName().contains("loop")  && elements.get(0).equals(e)))
						operations.addAll(findOperations(((IfThenElse)e).getCond()));
					operations.addAll(findOperations(((IfThenElse)e).getBodyFalse()));
					operations.addAll(findOperations(((IfThenElse)e).getBodyTrue()));
				}			
				/* Recursive call for LoopFor element */
				else if (e instanceof Loop) {
					findMultiUseSignals((Function)e);
					for(Element var : ((Loop)e).getInput()) {
						Assignment ass = new Assignment();
						ass.addInput(var); 
						ass.addOutput(var);
						operations.add(ass);
					}
				}
			} 
			else if (e instanceof Operation) {
				operations.add((Operation)e);
			}
		}
		return operations;
	}
	
	
	/**
	 * Get all variables who are contained in a vector of operations.
	 * @param operations The vector of operations
	 * @return A vector who contain all variables
	 */
	private Vector<SimpleVariable> findVariables(Vector<Operation> operations)
	{	
		
		// The vector of variables to return
		Vector<SimpleVariable> variables = new Vector<SimpleVariable>();
		
		for (Operation op : operations) {
			/* Get the SimpleVariable selection of a Multiplexer */
			if (op instanceof Multiplexer) {
				if (((Multiplexer)op).getSel() instanceof SimpleVariable) {
					variables.add((SimpleVariable)((Multiplexer)op).getSel());
					((SimpleVariable)((Multiplexer)op).getSel()).getMultiCost().add(((SimpleVariable)op.getOutputAt(0)).getCost()-op.getBlock().latencyTime()-1);
				}
			}
			
			/* Get all SimpleVariable inputs of the operation */
			for (int i = 0; i < op.getNbInput(); i++) {
				if (op.getInputAt(i) instanceof SimpleVariable) {
					SimpleVariable var = (SimpleVariable)op.getInputAt(i);
					if(!var.getType().equals("forloopcond") && !(var.getType().equals("iter") && variables.contains(var))){
						variables.add(var);
						if(op.getBlock() != null)
							var.getMultiCost().add((((SimpleVariable)op.getOutputAt(0)).getCost()-op.getBlock().latencyTime())-1);	
						else if(op instanceof Assignment)
							var.getMultiCost().add(((SimpleVariable)op.getOutputAt(0)).getCost());
					}
				}	
			}
		}
		
		return variables;
	}

	
	public void wrapTop(String vhdlPath) throws VHDLException {
		//create new folder into VHDL source folder for the wrappers
		String wrapperPath = vhdlPath+"wrappers/";
		File wrapDir = new File(wrapperPath);
		wrapDir.mkdir();
		
		String common = new String();
		String sWrapVhdl = new String();
		String sPackVhdl = new String();
		String sGenericWrapVhdl = new String();
		String sGenericPackVhdl = new String();
		String sRecomsVhdl = new String();

		String description;
		String wrapFileName="wrapper_"+this.top.getName()+".vhd";
		String packFileName="wrapper_"+this.top.getName()+"_pkg.vhd";
		String genericWrapFileName="m2m_generic_wrapper.vhd";
		String genericPackFileName="m2m_generic_wrapper_pkg.vhd";
		File wrapFile = new File(wrapperPath+wrapFileName);
		FileWriter wrapFw;
		File packFile = new File(wrapperPath+packFileName);
		FileWriter packFw;
		File genericWrapFile = new File(wrapperPath+genericWrapFileName);
		FileWriter genericWrapFw;
		File genericPackFile = new File(wrapperPath+genericPackFileName);
		FileWriter genericPackFw;
		File recomsFile = new File(wrapperPath+"wrapper_Recoms_"+this.top.getName()+".vhd");
		FileWriter recomsFw;

		//create package top
		description="";
		description+="-- Description: This file contains the declaration of 2 constants:\n";
		description+="--                - The number of inputs of the generated core.\n";
		description+="--                - The number of outputs of the generated core.\n";
		description+="--              It also defines generic types that are used by the\n";
		description+="--              specific wrapper.\n";
		sPackVhdl += this.createHeader(packFileName,description);
		
		//create generic package top
		description="";
		description+="-- Description: This file contains the declaration of 2 constants:\n";
		description+="--                - The number of inputs of the generated core.\n";
		description+="--                - The number of outputs of the generated core.\n";
		description+="--              It also defines generic types that are used by the\n";
		description+="--              generic wrapper.\n";
		sGenericPackVhdl += this.createHeader(genericPackFileName,description);
		
		common = "library ieee;\n";
		common += "\tuse ieee.std_logic_1164.all;\n";
		
		sPackVhdl += common;
		sGenericPackVhdl += common;

		sPackVhdl += "package m2m_"+this.top.getName()+"_inout_definition_pkg is\n\n";
		sGenericPackVhdl += "package m2m_inout_definition_pkg is\n\n";

		common = "\tconstant M2M_Nb_Inputs_c  : positive := " + this.top.getInput().size() + ";\n";
		common += "\tconstant M2M_Nb_Outputs_c : positive := " + this.top.getOutput().size() + ";\n";
		common += "\ttype M2M_Data is array(natural range <>) of std_logic_vector(" + NumType.getVHDLType(numType) + ";\n";
		common += "\ttype M2M_Control is array(natural range <>) of std_logic;\n";
		common += "\tconstant M2M_Function_Name_c : String := \""+this.top.getName()+"\";\n";
		
		sPackVhdl += common;
		sGenericPackVhdl += common;
		
		sPackVhdl += "\nend package m2m_"+this.top.getName()+"_inout_definition_pkg;";
		sGenericPackVhdl += "\nend package m2m_inout_definition_pkg;";
		
		//create wrapper top
		description="";
		description+="-- Description: This file contains a wrapper for the generated core.\n";
		description+="--              This wrapper can be used if better suited for a\n";
		description+="--              particular application.\n";
		sWrapVhdl += this.createHeader(wrapFileName,description);
		
		//create generic wrapper top
		description="";
		description+="-- Description: This file contains a wrapper for the generated core.\n";
		description+="--              This wrapper is generic, its name being always the same.\n";
		description+="--              It can be used if only one instance of a Math2mat core\n";
		description+="--              is used in a design.\n";
		sGenericWrapVhdl += this.createHeader(genericWrapFileName,description);
		
		//create Recoms wrapper top
		description="";
		description+="-- Description: This file contains a wrapper for the generated core.\n";
		description+="--              This wrapper is specific for Recoms applications,\n";
		description+="--              it names the clock specifically and adds a clock enable signal.\n";
		sRecomsVhdl += this.createHeader(genericWrapFileName,description);
		
		//load libraries
		common = "library ieee;\n";
		common += "\tuse ieee.std_logic_1164.all;\n";

		sWrapVhdl += common;
		sGenericWrapVhdl += common;
		sRecomsVhdl += common;
		
		sWrapVhdl += "\tuse work.m2m_"+this.top.getName()+"_inout_definition_pkg.all;\n";
		sGenericWrapVhdl += "\tuse work.m2m_inout_definition_pkg.all;\n";
		//sRecomsVhdl += "\tuse work.m2m_"+this.top.getName()+"_inout_definition_pkg.all;\n";
		
		//create entity
		sWrapVhdl += "\nentity wrapper_" + top.getName() + " is\n";
		sGenericWrapVhdl += "\nentity m2m_generic_wrapper is\n";
		sRecomsVhdl += "\nentity wrapper_Recoms_" + top.getName() + " is\n";
		
		common = "\tport( Clock_i          : in std_logic;\n";
		common += "\t      Reset_i        : in std_logic;\n";
		common += "\t      Input_i        : in M2M_Data(M2M_Nb_Inputs_c-1 downto 0);\n";
		common += "\t      Input_Valid_i  : in M2M_Control(M2M_Nb_Inputs_c-1 downto 0);\n";
		common += "\t      Input_Ready_o  : out M2M_Control(M2M_Nb_Inputs_c-1 downto 0);\n";
		common += "\t      Result_o       : out M2M_Data(M2M_Nb_Outputs_c-1 downto 0);\n";
		common += "\t      Result_Valid_o : out M2M_Control(M2M_Nb_Outputs_c-1 downto 0);\n";
		common += "\t      Result_Ready_i : in M2M_Control(M2M_Nb_Outputs_c-1 downto 0)\n";
		common += "\t     );\n";
		
		sWrapVhdl += common;
		sGenericWrapVhdl += common;
		
		sRecomsVhdl += "\tport( m2m_clk         : in std_logic;\n";
		sRecomsVhdl += "\t      Reset_i         : in std_logic;\n";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			sRecomsVhdl += "\t      Input" + (i+1) + "_i        : in std_logic_vector(" + NumType.getVHDLType(numType) + ";\n";
			sRecomsVhdl += "\t      Input" + (i+1) + "_Valid_i  : in std_logic;\n";
			sRecomsVhdl += "\t      Input" + (i+1) + "_Ready_o  : out std_logic;\n";
		}
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			sRecomsVhdl += "\t      Result" + (i+1) + "_o       : out std_logic_vector(" + NumType.getVHDLType(numType) + ";\n";
			sRecomsVhdl += "\t      Result" + (i+1) + "_Valid_o : out std_logic;\n";
			sRecomsVhdl += "\t      Result" + (i+1) + "_Ready_i : in std_logic";
			sRecomsVhdl += i == this.top.getOutput().size()-1 ? ";\n" : "\n";
		}
		sRecomsVhdl += "\t      m2m_ce          : in std_logic\n";
		sRecomsVhdl += "\t     );\n";
		
		sWrapVhdl += "end wrapper_" + top.getName() + ";\n";
		sGenericWrapVhdl += "end m2m_generic_wrapper;\n";
		sRecomsVhdl += "end wrapper_Recoms_" + top.getName() + ";\n";
		
		//create architecture
		sWrapVhdl += "\narchitecture mapping of wrapper_" + top.getName() + " is\n";
		sGenericWrapVhdl += "\narchitecture mapping of m2m_generic_wrapper is\n";
		sRecomsVhdl += "\narchitecture mapping of wrapper_Recoms_" + top.getName() + " is\n";
		
		common = "\ncomponent " + top.getName() + " is\n";
		common += "\tport(\n";
		common += "\t\tClock_i:\tin std_logic;\n";
		common += "\t\tReset_i:\tin std_logic;\n";
		for (Element el : this.top.getInput()) {
			if (el.getName().isEmpty()) {
				throw new NoNameException("input");
			}
			common += "\t\t" + el.getName() + "_i :\tin " + NumType.getVHDLType(numType) + ";\n";
			common += "\t\t" + el.getName() + "_Valid_i : \tin std_logic;\n";
			common += "\t\t" + el.getName() + "_Ready_o : \tout std_logic;\n";
		}
		for (int i=0;i<this.top.getOutput().size();i++) {
			Element el=this.top.getOutput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("output");
			}
			common += "\t\t" + el.getName() + "_o :\tout " + NumType.getVHDLType(numType) + ";\n";
			common += "\t\t" + el.getName() + "_Ready_i : \tin std_logic;\n";
			common += "\t\t" + el.getName() + "_Valid_o : \tout std_logic";
			if (i!=this.top.getOutput().size()-1)
				common += ";";
			common += "\n";
		//	common += this.top.getOutput().lastElement().equals(el) ? ";\n" : "\n";
		}
		common += "\t\t" + ");\n" + "\t" + "end component;\n";
		
		common += "\nbegin\n\n";
		
		//map the top
		common += "top : " + top.getName() + "\n";
		
		sWrapVhdl += common;
		sGenericWrapVhdl += common;
		sRecomsVhdl += common;
		
		sWrapVhdl += "\tport map( Clock_i => Clock_i,\n";
		sGenericWrapVhdl += "\tport map( Clock_i => Clock_i,\n";
		sRecomsVhdl += "\tport map( Clock_i => m2m_clk,\n";
		
		common = "\t          Reset_i => Reset_i,\n";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			Element el = this.top.getInput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("input");
			}
			common += "\t\t" + el.getName() + "_i => Input_i(" + i + "),\n";
			common += "\t\t" + el.getName() + "_Valid_i => Input_Valid_i(" + i + "),\n";
			common += "\t\t" + el.getName() + "_Ready_o => Input_Ready_o(" + i + "),\n";
		}
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			Element el = this.top.getOutput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("output");
			}
			common += "\t\t" + el.getName() + "_o => Result_o(" + i + "),\n";
			common += "\t\t" + el.getName() + "_Valid_o => Result_Valid_o(" + i + "),\n";
			if (i == this.top.getOutput().size() - 1) {
				common += "\t\t" + el.getName() + "_Ready_i => Result_Ready_i(" + i + ")\n";
			} else {
				common += "\t\t" + el.getName() + "_Ready_i => Result_Ready_i(" + i + "),\n";
			}
		}
		sWrapVhdl += common;
		sGenericWrapVhdl += common;

		common = "\t          Reset_i => Reset_i,\n";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			Element el = this.top.getInput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("input");
			}
			common += "\t\t" + el.getName() + " => Input" + (i+1) + "_i,\n";
			common += "\t\t" + el.getName() + "_Valid => Input" + (i+1) + "_Valid_i,\n";
			common += "\t\t" + el.getName() + "_Ready => Input" + (i+1) + "_Ready_o,\n";
		}
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			Element el = this.top.getOutput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("output");
			}
			common += "\t\t" + el.getName() + " => Result" + (i+1) + "_o,\n";
			common += "\t\t" + el.getName() + "_Valid => Result" + (i+1) + "_Valid_o,\n";
			if (i == this.top.getOutput().size() - 1) {
				common += "\t\t" + el.getName() + "_Ready => Result" + (i+1) + "_Ready_i\n";
			} else {
				common += "\t\t" + el.getName() + "_Ready => Result" + (i+1) + "_Ready_i,\n";
			}
		}
		sRecomsVhdl += common;
		
		common = "\t        );\n\n";

		common += "end mapping;\n";
		
		sWrapVhdl += common;
		sGenericWrapVhdl += common;
		sRecomsVhdl += common;

		try {
			wrapFw = new FileWriter(wrapFile);
			packFw = new FileWriter(packFile);
			genericWrapFw = new FileWriter(genericWrapFile);
			genericPackFw = new FileWriter(genericPackFile);
			recomsFw = new FileWriter(recomsFile);
			wrapFw.write(sWrapVhdl);
			packFw.write(sPackVhdl);
			genericWrapFw.write(sGenericWrapVhdl);
			genericPackFw.write(sGenericPackVhdl);
			recomsFw.write(sRecomsVhdl);
			wrapFw.close();
			packFw.close();
			genericWrapFw.close();
			genericPackFw.close();
			recomsFw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the VHDL code.
	 * @return VHDL code on a string
	 */
	public String createVHDL(String fileName) throws VHDLException {
		parse(top, null);
		wrapComponents(top.getBody());
		String description="";
		description+="-- Description: This file contains the top level block generated by the\n";
		description+="--              Math2mat tool. The architecture is composed of instantiations of\n";
		description+="--              the different operatores, as well as of the control logic.\n";
		String sVhdl = new String();
		sVhdl += createHeader(fileName,description);
		sVhdl += createFunctionEntity(top, 0);
		sVhdl += "\n\n";
		sVhdl += createFunctionArchitecture(top, 0);
		return sVhdl;
	}
	

	/**
	 * This function returns the VHDL header that can be included in a generated VHDL file. It
	 * accepts a description. If not empty, it is inserted in the header.
	 * @param fileName Name of the file that will be generated.
	 * @param description the Description of the file. It should follow the style of a header. It
	 * should start with "-- Description: ", and then each line should start with "-- ". Be 
	 * careful not to write lines greater than 80 caracters.
	 * @return the Header that can be included in a generated VHDL file
	 * @author Yann Thoma
	 */
	private String createHeader(String fileName,String description) {
		String sHeader = new String();
		Date curDate= new Date();
		sHeader+="--------------------------------------------------------------------------------\n";
		sHeader+="-- This file was automatically generated by Math2mat\n";
		sHeader+="-- File:   "+fileName+"\n";
		sHeader+="-- Author: Math2mat consortium\n";
		sHeader+="-- Date:   "+curDate.toString()+"\n--\n";
		if (!description.isEmpty())
			sHeader+=description;
		sHeader+="--------------------------------------------------------------------------------\n";
		sHeader+="\n";
		return sHeader;
	}

	
	/**
	 * Add the wrapper around an operator.
	 * @param outFile The VHDL function output file.
	 */
	public void wrapComponents(Vector<Element> body) throws VHDLException {
		String workFolder = project.getProperties().getProjectPath();
		
		/* Go throught all elements contained into the body */
		for (Element el : body) {			
			if (el instanceof IfThenElse) {
				wrapComponents(((IfThenElse)el).getBodyFalse());
				wrapComponents(((IfThenElse)el).getBodyTrue());
				wrapComponents(((IfThenElse)el).getCond());
		    /* Create a loopFor vhdl file */
			} else if (el instanceof Loop) {
				Loop loop = (Loop) el;
				String fileName = workFolder + project.getVHDLSubFolder()+"/loop_" + loop.getName() + ".vhd";
				File file = new File(fileName);
				FileWriter fw;
				try {
					fw = new FileWriter(file);
					String sLoop = new String();
					sLoop = createLoop(loop, 0);
					fw.write(sLoop);
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				wrapComponents(loop.getBody());
				if(loop instanceof LoopFor) {
					Vector<Element> addLoopFor = new Vector<Element>();
					addLoopFor.add(((LoopFor)loop).getIterOperation());
					wrapComponents(addLoopFor);
				}				
			/* Create a wrapper operation vhdl file */
			} else if (el instanceof Operation && !(el instanceof Assignment)) {
				Operation op = (Operation) el;
				BuildingBlock block = op.getBlock();
				if (block == null) {
					throw new VHDLException("No material description for the operation : " +
					  op.getName());
				}
				String fileName = workFolder + project.getVHDLSubFolder() + "/wrapper_" + block.entityName() + ".vhd";
				File file = new File(fileName);
				FileWriter fw;
				try {
					fw = new FileWriter(file);
					String sVhdl = new String();
					try {
						sVhdl = createWrapper(op);
					} catch (NoNameException e) {
						e.printStackTrace();
					}
					fw.write(sVhdl);
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	/**
	 * Generate a wrapper of an operation.
	 * @param op The operation who generated wrapper
	 * @return the string contained the generated wrapper
	 * @throws NoNameException
	 */
	private String createWrapper(Operation op) throws NoNameException {
		String sOperator = new String();		
		BuildingBlock blockOp = op.getBlock();
		
		/* Generate entity */
		sOperator += "library ieee;\n";
		sOperator += "use ieee.std_logic_1164.all;\n";
		if (blockOp.entityName().isEmpty())
			throw new NoNameException("Operator");
		sOperator += "entity wrapper_" + blockOp.entityName() + " is\n";
		sOperator += "generic (\n";
		if (op instanceof Multiplexer)
			sOperator += "\tD_Sel_Fifo_Size : integer := 2;\n";
		for(int i = 0; i < op.getNbInput(); i++) {
			sOperator += "\tD" + (i+1) + "_Fifo_Size : integer := 2";
			sOperator += i == op.getNbInput()-1 ? "\n);\n" : ";\n" ;
		}
		sOperator += "port(\n";
		sOperator += "\tClock_i:\tin std_logic;\n";
		sOperator += "\tReset_i:\tin std_logic;\n";
		
		/* Generate inputs of the entity */
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			if(op instanceof LogicOperation)
				sOperator += tab(1) + "D" + (i + 1) + "_i\t: in Std_Logic;\n";
			else
				sOperator += tab(1) + "D" + (i + 1) + "_i\t: in " + NumType.getVHDLType(numType) + ";\n";
			sOperator += tab(1) + "D" + (i + 1) + "_Valid_i\t: in Std_Logic;\n";
			sOperator += tab(1) + "D" + (i + 1) + "_Ready_o\t: out Std_Logic;\n";
		}
		if (op instanceof Multiplexer) {
			sOperator += tab(1) + "D_Sel_i\t: in Std_Logic;\n";
			sOperator += tab(1) + "D_Sel_Valid_i\t: in Std_Logic;\n";
			sOperator += tab(1) + "D_Sel_Ready_o\t: out Std_Logic;\n";
		}
		
		/* Generate outputs of the entity */
		sOperator += tab(1) + "Ready_i\t: in Std_Logic;\n";
		if (op instanceof Comparison) {
			sOperator += tab(1) + "Smaller_o\t: out Std_Logic;\n";
			sOperator += tab(1) + "Bigger_o\t : out Std_Logic;\n";
			sOperator += tab(1) + "Equal_o\t  : out Std_Logic;\n";
		} else{
			if(op instanceof LogicOperation)
				sOperator += tab(1) + "Result_o\t: out Std_Logic;\n";
			else
				sOperator += tab(1) + "Result_o\t: out " + NumType.getVHDLType(numType) + ";\n";
		}
			
		sOperator += tab(1) + "Valid_o\t: out Std_Logic\n";
		sOperator += ");\n" + "end " + "wrapper_" + blockOp.entityName() + ";\n\n";

		/* Generate Internal signals */
		sOperator += "architecture comp of " + "wrapper_" + blockOp.entityName() + " is\n\n";
		sOperator += tab(1) + "signal Stall_s : std_logic;\n";
		sOperator += tab(1) + "signal Ready_s : std_logic;\n";
		sOperator += tab(1) + "signal Valid_in_s : std_logic;\n";
		sOperator += tab(1) + "signal Op_Ready_s : std_logic;\n";
		sOperator += tab(1) + "signal Valid_s : std_logic;\n";
		sOperator += tab(1) + "signal Valid_Mem_s : std_logic;\n";
		if (op instanceof Comparison) {
			sOperator += tab(1) + "signal Smaller_s : std_logic;\n";
			sOperator += tab(1) + "signal Smaller_Mem_s : std_logic;\n";
			sOperator += tab(1) + "signal Bigger_s : std_logic;\n";
			sOperator += tab(1) + "signal Bigger_Mem_s : std_logic;\n";
			sOperator += tab(1) + "signal Equal_s : std_logic;\n";
			sOperator += tab(1) + "signal Equal_Mem_s : std_logic;\n\n";
		} else {
			if(op instanceof LogicOperation) {
				sOperator += tab(1) + "signal Result_s : std_logic;\n";
				sOperator += tab(1) + "signal Result_Mem_s : std_logic;\n\n";
			} else {
				sOperator += tab(1) + "signal Result_s : " + NumType.getVHDLType(numType) + ";\n";
				sOperator += tab(1) + "signal Result_Mem_s : " + NumType.getVHDLType(numType) + ";\n\n";
			}
		}
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			if(op instanceof LogicOperation) 
				sOperator += tab(1) + "signal D"+ (i + 1) + "_s             : std_logic;\n";
			else 
				sOperator += tab(1) + "signal D"+ (i + 1) + "_s             : " + NumType.getVHDLType(numType) + ";\n";
			sOperator += tab(1) + "signal D"+ (i + 1) + "_fifo_full_s   : std_logic;\n";
			sOperator += tab(1) + "signal D"+ (i + 1) + "_fifo_empty_s  : std_logic;\n";
			sOperator += tab(1) + "signal D"+ (i + 1) + "_Ready_s       : std_logic;\n";
			sOperator += tab(1) + "signal D"+ (i + 1) + "_Valid_s       : std_logic;\n";
		}
		if(op instanceof Multiplexer) {
			sOperator += tab(1) + "signal Sel_s            : std_logic;\n";
			sOperator += tab(1) + "signal Sel_fifo_full_s  : std_logic;\n";
			sOperator += tab(1) + "signal Sel_fifo_empty_s : std_logic;\n";
			sOperator += tab(1) + "signal Sel_Ready_s      : std_logic;\n";
			sOperator += tab(1) + "signal Sel_Valid_s      : std_logic;\n";	
		}
		sOperator += "\n";
		
		/* Generate component to wrap */
		sOperator += tab(1) + "component " + blockOp.entityName() + " is\n";
		sOperator += tab(1) + "generic (\n";
		sOperator += tab(2) + "wValid_g : integer := 1";
		if(op instanceof Multiplexer || op instanceof Comparison)
			sOperator += ";\n" + tab(2) + "m_g : integer := 32\n";
		else if(op instanceof Negation) {
			sOperator += ";\n" + tab(2) + "data_size_g : integer := 32;\n";
			sOperator += tab(2) + "wE_g        : integer := 7;\n";
			sOperator += tab(2) + "wF_g        : integer := 24";
		}
		sOperator += "\n";
		sOperator += tab(1) + ");\n";
		sOperator += tab(1) + "port (\n";
		sOperator += tab(2) + "clk_i   : in std_logic;\n";
		sOperator += tab(2) + "reset_i : in std_logic;\n";
		sOperator += tab(2) + "valid_i : in std_logic_vector(wValid_g-1 downto 0);\n";
		sOperator += tab(2) + "stall_i : in std_logic;\n";
		
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			if(op instanceof LogicOperation) 
				sOperator += tab(2) + "D" + (i + 1) + "_i\t: in Std_Logic;\n";
			else 
				sOperator += tab(2) + "D" + (i + 1) + "_i\t: in " + NumType.getVHDLType(numType) + ";\n";	
		}
		if (op instanceof Multiplexer) {
			sOperator += tab(2) + "D_Sel_i\t: in Std_Logic;\n";
		}
		if (op instanceof Comparison) {
			sOperator += tab(2) + "smaller_o : out Std_Logic;\n";
			sOperator += tab(2) + "bigger_o  : out Std_Logic;\n";
			sOperator += tab(2) + "equal_o   : out Std_Logic;\n";
		} else {
			if(op instanceof LogicOperation) 
				sOperator += tab(2) + "m_o     : out Std_Logic;\n";
			else 
				sOperator += tab(2) + "m_o     : out " + NumType.getVHDLType(numType) + ";\n";	
		}
		sOperator += tab(2) + "valid_o : out std_logic_vector(wValid_g-1 downto 0);\n";
		sOperator += tab(2) + "ready_o : out std_logic\n";
		sOperator += tab(1) + ");\n";
		sOperator += tab(1) + "end component;\n\n";
		
		/* Declare fifo component */
		sOperator += tab(1) + "component fifo_Std is\n";
		sOperator += tab(1) + "generic (\n";
		sOperator += tab(2) + "FIFOSIZE  : integer := " + NumType.getDataSize(numType) + ";\n";
		sOperator += tab(2) + "DATAWIDTH : integer := " + NumType.getDataSize(numType) + "\n";
		sOperator += tab(1) + ");\n";
		sOperator += tab(2) + "port (\n";
		sOperator += tab(2) + "Clock_i      : in Std_Logic;\n";
		sOperator += tab(2) + "Reset_i      : in Std_Logic;\n";
		sOperator += tab(2) + "Wr_Req_i     : in Std_Logic;\n";
		sOperator += tab(2) + "Rd_Req_i     : in Std_Logic;\n";
		sOperator += tab(2) + "Data_i       : in Std_Logic_Vector(DATAWIDTH-1 downto 0);\n";
		sOperator += tab(2) + "Data_o       : out Std_Logic_Vector(DATAWIDTH-1 downto 0);\n";
		sOperator += tab(2) + "Fifo_Full_o  : out Std_Logic;\n";
		sOperator += tab(2) + "Fifo_Empty_o : out Std_Logic\n";
		sOperator += tab(1) + ");\n";
		sOperator += tab(1) + "end component;\n\n";
		sOperator += "begin\n\n";
		
		/* Map fifos */
		for(int i = 0; i < blockOp.nbInputs(); i++) {
			sOperator += tab(1) + "Fifo_D" + (i + 1) + " : fifo_Std\n";
			sOperator += tab(1) + "generic map(\n"; 
			sOperator += tab(2) + "FIFOSIZE => D" + (i+1) + "_Fifo_Size,\n";
			if(op instanceof LogicOperation) 
				sOperator += tab(2) + "DATAWIDTH => 1\n" + tab(1) + ")\n";
			else 
				sOperator += tab(2) + "DATAWIDTH => " + NumType.getDataSize(numType) + "\n" + tab(1) + ")\n";	
			sOperator += tab(1) + "port map(\n";
			sOperator += tab(2) + "Clock_i      => Clock_i,\n";
			sOperator += tab(2) + "Reset_i      => Reset_i,\n";
			sOperator += tab(2) + "Wr_Req_i     => D" + (i + 1) + "_Valid_i,\n";
			sOperator += tab(2) + "Rd_Req_i     => D" + (i + 1) + "_Ready_s,\n";
			if(op instanceof LogicOperation) {
				sOperator += tab(2) + "Data_i(0)    => D" + (i + 1) + "_i,\n";
				sOperator += tab(2) + "Data_o(0)    => D" + (i + 1) + "_s,\n";
			}
			else { 
				sOperator += tab(2) + "Data_i       => D" + (i + 1) + "_i,\n";
				sOperator += tab(2) + "Data_o       => D" + (i + 1) + "_s,\n";
			}   
			sOperator += tab(2) + "Fifo_Full_o  => D" + (i + 1) + "_fifo_full_s,\n";
			sOperator += tab(2) + "Fifo_Empty_o => D" + (i + 1) + "_fifo_empty_s\n";
			sOperator += tab(1) + ");\n\n";
		}
		if(op instanceof Multiplexer) {
			sOperator += tab(1) + "Fifo_Sel : fifo_Std\n";
			sOperator += tab(1) + "generic map(\n";
			sOperator += tab(2) + "FIFOSIZE => D_Sel_Fifo_Size,\n";
			sOperator += tab(2) + "DATAWIDTH => 1\n" + tab(1) + ")\n";
			sOperator += tab(1) + "port map(\n";
			sOperator += tab(1) + "Clock_i      => Clock_i,\n";
			sOperator += tab(2) + "Reset_i      => Reset_i,\n";
			sOperator += tab(2) + "Wr_Req_i     => D_Sel_Valid_i,\n";
			sOperator += tab(2) + "Rd_Req_i     => Sel_Ready_s,\n";
			sOperator += tab(2) + "Data_i(0)    => D_Sel_i,\n";
			sOperator += tab(2) + "Data_o(0)    => Sel_s,\n";
			sOperator += tab(2) + "Fifo_Full_o  => Sel_fifo_full_s,\n";
			sOperator += tab(2) + "Fifo_Empty_o => Sel_fifo_empty_s\n";
			sOperator += tab(1) + ");\n\n";
		}
		
		/* Map operator */
		sOperator += tab(1) + "Op : " + blockOp.entityName() + "\n";
		if(op instanceof Multiplexer || op instanceof Comparison) {
			sOperator += tab(1) + "generic map (\n";
			sOperator += tab(2) + "m_g => " + NumType.getDataSize(numType) + "\n";
			sOperator += tab(1) + ")\n";
		}
		else if(op instanceof Negation ) {
			sOperator += tab(1) + "generic map (\n";
			sOperator += tab(2) + "data_size_g => " + NumType.getDataSize(numType) + ",\n";
			if(NumType.getDataSize(numType) == 32) {
				sOperator += tab(2) + "wE_g        => 7,\n";
				sOperator += tab(2) + "wF_g        => 24\n";		
			}
			else if(NumType.getDataSize(numType) == 64){
				sOperator += tab(2) + "wE_g        => 10,\n";
				sOperator += tab(2) + "wF_g        => 53\n";
			}
			sOperator += tab(1) + ")\n";
		}
		sOperator += tab(1) + "port map (\n";
		sOperator += tab(2) + "clk_i    => Clock_i,\n";
		sOperator += tab(2) + "reset_i    => Reset_i,\n";
		sOperator += tab(2) + "valid_i(0) => Valid_in_s,\n";
		sOperator += tab(2) + "stall_i    => Stall_s,\n";
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			sOperator += tab(2) + "D" + (i + 1) + "_i       => D" + (i + 1) + "_s,\n";
		}
		if (op instanceof Multiplexer) {
			sOperator += tab(2  ) + "D_Sel_i    => Sel_s,\n";
		}
		/* Generate as many signals as we have outputs */
		if (op instanceof Comparison) {
			sOperator += tab(2) + "smaller_o  => Smaller_s,\n";
			sOperator += tab(2) + "bigger_o   => Bigger_s,\n";
			sOperator += tab(2) + "equal_o    => Equal_s,\n";
		} else
			sOperator += tab(2) + "m_o        => Result_s,\n";
		sOperator += tab(2) + "valid_o(0) => Valid_s,\n";
		sOperator += tab(2) + "ready_o    => Op_Ready_s\n";
		sOperator += tab(1) + ");\n\n";
		
		/* Generate combinatorial logic */
		for(int i = 0; i < blockOp.nbInputs(); i++) {
			sOperator += tab(1) + "D" + (i + 1) + "_Ready_o <= not D" + (i + 1) + "_fifo_full_s;\n";
			sOperator += tab(1) + "D" + (i + 1) + "_Valid_s <= not D" + (i + 1) + "_fifo_empty_s;\n\n";
		}
		if(op instanceof Multiplexer) {
			sOperator += tab(1) + "D_Sel_Ready_o <= not Sel_fifo_full_s;\n";
			sOperator += tab(1) + "Sel_Valid_s <= not Sel_fifo_empty_s;\n\n";
		}
		
		sOperator += tab(1) + "Stall_s <= not Ready_s;\n";
		sOperator += tab(1) + "Valid_in_s <= ";
		if (op instanceof Multiplexer) {
			sOperator += "Sel_Valid_s and ";
		}
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			if (i == 0) {
				sOperator += "D" + (i + 1) + "_Valid_s";
			} else {
				sOperator += " and D" + (i + 1) + "_Valid_s";
			}
		}
		sOperator += " and Op_Ready_s;\n";
		for (int i = 0; i < blockOp.nbInputs(); i++) {
			sOperator += tab(1) + "D" + (i + 1) + "_Ready_s <= not D" + (i + 1) + "_Valid_s or (";
			for (int j = 0; j < blockOp.nbInputs(); j++) {
				if (j == 0) {
					sOperator += "(D" + (j + 1) + "_Valid_s";
				} else {
					sOperator += " and D" + (j + 1) + "_Valid_s";
				}
			}
			if(op instanceof Multiplexer)
				sOperator += " and Sel_Valid_s";
			sOperator += ") and Op_Ready_s and (Ready_s or not Valid_s));\n";
		}
		if (op instanceof Multiplexer) {
			sOperator += tab(1) + "Sel_Ready_s <= not Sel_Valid_s or (";
			for (int j = 0; j < blockOp.nbInputs(); j++) {
				if (j == 0) 
					sOperator += "(D" + (j + 1) + "_Valid_s";
			     else 
					sOperator += " and D" + (j + 1) + "_Valid_s";
			}
			sOperator += " and Sel_Valid_s) and Op_Ready_s and (Ready_s or not Valid_s));\n";
		}
		
		/* Generate assignement of outputs */
		sOperator += "\n";
		if (op instanceof Comparison) {
			sOperator += tab(1) + "Smaller_o <= Smaller_s when Ready_s = '1' else Smaller_Mem_s;\n";
			sOperator += tab(1) + "Bigger_o  <= Bigger_s when Ready_s = '1' else Bigger_Mem_s;\n";
			sOperator += tab(1) + "Equal_o   <= Equal_s when Ready_s = '1' else Equal_Mem_s;\n";
		} else
			sOperator += tab(1) + "Result_o <= Result_s when Ready_s = '1' else Result_Mem_s;\n";
		sOperator += tab(1) + "Valid_o <= Valid_s when Ready_s = '1' else Valid_Mem_s;\n";
		sOperator += "\n";
		
		/* Generate process */
		sOperator += tab(1) + "process (Clock_i, Reset_i)\n";
		sOperator += tab(1) + "begin\n";
		sOperator += tab(2) + "if (Reset_i = '1') then\n";
		if (op instanceof Comparison) {
			sOperator += tab(3) + "Smaller_Mem_s <= '0';\n";
			sOperator += tab(3) + "Bigger_Mem_s  <= '0';\n";
			sOperator += tab(3) + "Equal_Mem_s   <= '0';\n";
		} else {
			if(op instanceof LogicOperation) 
				sOperator += tab(3) + "Result_Mem_s <= '0';\n";
			else 
				sOperator += tab(3) + "Result_Mem_s <= (others => '0');\n";
		}
		sOperator += tab(3) + "Valid_Mem_s <= '0';\n";
		sOperator += tab(3) + "Ready_s <= '1';\n";
		sOperator += tab(2) + "elsif Rising_Edge(Clock_i) then\n";
		sOperator += tab(3) + "if (Ready_s = '1') then\n";
		if (op instanceof Comparison) {
			sOperator += tab(4) + "Smaller_Mem_s <= Smaller_s;\n";
			sOperator += tab(4) + "Bigger_Mem_s  <= Bigger_s;\n";
			sOperator += tab(4) + "Equal_Mem_s   <= Equal_s;\n";
		} else
			sOperator += tab(4) + "Result_Mem_s <= Result_s;\n";
		sOperator += tab(4) + "Valid_Mem_s <= Valid_s;\n";
		sOperator += tab(3) + "end if;\n";
		sOperator += tab(3) + "Ready_s <= Ready_i;\n";
		sOperator += tab(2) + "end if;\n";
		sOperator += tab(1) + "end process;\n\n";
		
		/* Assertion for debug 
		sOperator += tab(1) + "assert ";
		if(op instanceof Multiplexer)
			sOperator += "Sel_fifo_full_s = '1' or ";
		for(int i = 0; i < op.getNbInput(); i++) {
			sOperator += "D" + (i+1) + "_fifo_full_s = '1' ";
			sOperator += i == op.getNbInput()-1 ? "report \"Fifo full\";\n\n" : "or ";
		}
		*/
		
		sOperator += "end comp;";

		return sOperator;
	}
	
	private String createFunctionEntity(Function f, int level) throws VHDLException {
		String sEntity = new String();
		sEntity += tab(level) + "library ieee;\n";
		sEntity += tab(level) + "use ieee.std_logic_1164.all;\n";
		sEntity += tab(level) + "use ieee.std_logic_arith.all;\n";
		sEntity += tab(level) + "use ieee.std_logic_unsigned.all;\n";
		sEntity += tab(level) + "use work.pkg_cell.all;\n";
		if (f.getName().isEmpty()) {
			throw new NoNameException("top function");
		}
		sEntity += tab(level) + "entity " + f.getName() + " is\n";
		sEntity += tab(level) + "port(\n";
		sEntity += tab(level) + "\tClock_i:\tin std_logic;\n";
		sEntity += tab(level) + "\tReset_i:\tin std_logic;\n";
		for (Element el : f.getInput()) {
			if (el.getName().isEmpty()) {
				throw new NoNameException("input");
			}
			sEntity += tab(level + 1) + el.getName() + "_i :\tin " + NumType.getVHDLType(numType) + ";\n";
			sEntity += tab(level + 1) + el.getName() + "_Valid_i : \tin std_logic;\n";
			sEntity += tab(level + 1) + el.getName() + "_Ready_o : \tout std_logic;\n";
		}
		for (int i = 0; i < f.getOutput().size(); i++) {
			Element el = f.getOutput().elementAt(i);
			if (el.getName().isEmpty()) {
				throw new NoNameException("output");
			}
			sEntity += tab(level + 1) + el.getName() + "_o :\tout " + NumType.getVHDLType(numType) + ";\n";
			sEntity += tab(level + 1) + el.getName() + "_Ready_i : \tin std_logic;\n";
			sEntity += tab(level + 1) + el.getName() + "_Valid_o : \tout std_logic";
			if (i != f.getOutput().size()-1)
				sEntity += ";";
			sEntity += "\n";
		}
		sEntity += tab(level + 1) + ");\n" + tab(level) + "end " + f.getName() + ";";
		return sEntity;
	}
	
	
	/**
	 * Generate VHDL description for a loop entity. 
	 * @param loop  The loop element of the structure
	 * @param level The level of tab character
	 * @return the string representation of the loop VHDL entity
	 * @throws VHDLException
	 */
	private String createLoopEntity(Loop loop, int level) throws VHDLException {
		String sLoopEntity = new String();
		sLoopEntity += "library ieee;\n";
		sLoopEntity += "\tuse ieee.std_logic_1164.all;\n";
		sLoopEntity += "\tuse work.log_pkg.all;\n";		
		sLoopEntity += "\n" + tab(level) + "entity loop_" + loop.getName() + " is\n";
		
		sLoopEntity += tab(level) + "port (\n";
		sLoopEntity += tab(level + 1) + "Clock_i\t: in Std_Logic;\n";
		sLoopEntity += tab(level + 1) + "Reset_i\t: in Std_Logic;\n";
		
		/* Instantiation of all inputs */
		for (int i = 0; i < loop.getInput().size(); i++) {
			sLoopEntity += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_i\t: in " + NumType.getVHDLType(numType) + ";\n";
			sLoopEntity += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_Valid_i\t: in Std_Logic;\n";
			sLoopEntity += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_Ready_o\t: out Std_Logic;\n";
		}
	
		/* Instantiation of all outputs */
		for (int i = 0; i < loop.getOutput().size(); i++) {
			sLoopEntity += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_o\t: out " + NumType.getVHDLType(numType) + ";\n";
			sLoopEntity += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_Valid_o\t: out Std_Logic;\n";
			sLoopEntity += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_Ready_i\t: in Std_Logic";
			sLoopEntity += i == loop.getOutput().size()-1 ? "\n" : ";\n";
		}
		sLoopEntity += tab(level) + ");\n";
		sLoopEntity += tab(level) + "end loop_" + loop.getName() + ";\n\n";
		
		return sLoopEntity;
	}

	
	/**
	 * Generate the architecture of a function
	 * @param f		The function to generate the architecture
	 * @param level	The level of the function
	 * @return the string representation of the function architecture
	 * @throws VHDLException
	 */
	private String createFunctionArchitecture(Function f, int level) throws VHDLException {
		String sArchitecture = new String();
		sArchitecture += tab(level) + "architecture comp of " + f.getName() + " is\n";
		sArchitecture += "\n" + this.createComponent(f, level + 1);
		sArchitecture += "\n" + this.createInternals(f, level+1);
		sArchitecture += "\n" + tab(level) + "begin\n";
		sArchitecture += this.createBody(level + 1, f.getBody(), f);
		sArchitecture += "\n" + tab(level) + "end comp;";
		return sArchitecture;
	}
	

	/**
	 * Generate VHDL description for all components of a loop. 
	 * @param loop  The loop element of the structure
	 * @param level The level of tab character
	 * @return the string representation of all components VHDL generation
	 * @throws VHDLException
	 */
	private String createLoopComponent(Function loop, int level) throws VHDLException {
		String sLoopComponent = new String();	
		/* Add the fifo component */
		String fifoType = project.getOptimisationProperties().getFifoType();
		if (fifoType.equalsIgnoreCase("Altera"))
			sLoopComponent += tab(level) + "component fifo_altera is\n";
		else if (fifoType.equalsIgnoreCase("Xilinx"))
			sLoopComponent += tab(level) + "component fifo_Xilinx is\n";
		else
			sLoopComponent += tab(level) + "component fifo_Std is\n";
		sLoopComponent += tab(level) + "generic (\n";
		sLoopComponent += tab(level + 1) + "FIFOSIZE  : integer := " + NumType.getDataSize(numType) + ";\n";
		sLoopComponent += tab(level + 1) + "DATAWIDTH : integer := " + NumType.getDataSize(numType) + "\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "port (\n";
		sLoopComponent += tab(level + 1) + "Clock_i      : in Std_Logic;\n";
		sLoopComponent += tab(level + 1) + "Reset_i      : in Std_Logic;\n";
		sLoopComponent += tab(level + 1) + "Wr_Req_i     : in Std_Logic;\n";
		sLoopComponent += tab(level + 1) + "Rd_Req_i     : in Std_Logic;\n";
		sLoopComponent += tab(level + 1) + "Data_i       : in Std_Logic_Vector(DATAWIDTH-1 downto 0);\n";
		sLoopComponent += tab(level + 1) + "Data_o       : out Std_Logic_Vector(DATAWIDTH-1 downto 0);\n";
		sLoopComponent += tab(level + 1) + "Fifo_Full_o  : out Std_Logic;\n";
		sLoopComponent += tab(level + 1) + "Fifo_Empty_o : out Std_Logic\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
	
		/* Instantiation of init component */
		sLoopComponent += tab(level) + "component Init_Input is\n";
		sLoopComponent += tab(level) + "generic(\n";
		sLoopComponent += tab(level) + "\tData_Size_g : integer := " + NumType.getDataSize(numType) + ");\n";
		sLoopComponent += tab(level) + "port(\n";
		sLoopComponent += tab(level) + "\tClock_i             : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tReset_i           : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tData_i            : in  std_logic_vector(data_Size_g-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tData_Valid_i      : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tData_Ready_o      : out std_logic;\n";
		sLoopComponent += tab(level) + "\tData_Loop_i       : in  std_logic_vector(data_Size_g-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tData_Loop_Valid_i : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tData_Loop_Ready_o : out std_logic;\n";
		sLoopComponent += tab(level) + "\tCond_Loop_i       : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tData_o            : out std_logic_vector(Data_Size_g-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tData_Valid_o      : out std_logic;\n";
		sLoopComponent += tab(level) + "\tData_Ready_i      : in  std_logic\n";	
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
		
		/* Instantiation of init counter component */
		sLoopComponent += tab(level) + "component Counter_Wr is\n";
		sLoopComponent += tab(level) + "generic(\n";
		sLoopComponent += tab(level) + "\tData_Size_g : integer := " + NumType.getDataSize(numType) + ";\n";
		sLoopComponent += tab(level) + "\tMem_Size_g  : integer := 1024);\n";
		sLoopComponent += tab(level) + "port(\n";
		sLoopComponent += tab(level) + "\tClock_i      : in  std_logic;\n";
		sLoopComponent += tab(level) + "\tReset_i      : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tData_i       : in  std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);\n";
	    sLoopComponent += tab(level) + "\tData_Valid_i : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tReady_i      : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tCond_i       : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tRead_i       : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tAdr_Rd_i     : in  std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);\n";
	    sLoopComponent += tab(level) + "\tFifo_Ready_i : in  std_logic;\n";
	    sLoopComponent += tab(level) + "\tData_o       : out std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);\n";
	    sLoopComponent += tab(level) + "\tData_Valid_o : out std_logic;\n";
	    sLoopComponent += tab(level) + "\tReady_o     : out std_logic\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
		
		/* Instantiation of loop memory component */
		sLoopComponent += tab(level) + "component Memory is\n";
		sLoopComponent += tab(level) + "generic(\n";
		sLoopComponent += tab(level) + "\tData_Width_g : integer := " + NumType.getDataSize(numType) + ";\n";
		sLoopComponent += tab(level) + "\tMem_Size_g   : integer := 128);\n";
		sLoopComponent += tab(level) + "port(\n";
		sLoopComponent += tab(level) + "\tClock_i      : in std_logic;\n";
		sLoopComponent += tab(level) + "\tReset_i      : in std_logic;\n";
		sLoopComponent += tab(level) + "\tWr_i         : in std_logic;\n";
		sLoopComponent += tab(level) + "\tRd_i         : in std_logic;\n";
		sLoopComponent += tab(level) + "\tAdr_Wr_i     : in std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tAdr_Rd_i     : in std_logic_vector(ilogup(Mem_Size_g)-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tAdr_Valid_i  : in std_logic;\n";
		sLoopComponent += tab(level) + "\tData_i       : in std_logic_vector(Data_Width_g-1 downto 0);\n";
		sLoopComponent += tab(level) + "\tData_Valid_o : out std_logic;\n";
		sLoopComponent += tab(level) + "\tData_o       : out std_logic_vector(Data_Width_g-1 downto 0)\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
		
		/* Instantiation of counter component */
		sLoopComponent += tab(level) + "component Counter_Rd is\n";
		sLoopComponent += tab(level) + "generic(\n";
		sLoopComponent += tab(level) + "\tSize_g : integer := " + NumType.getDataSize(numType) + ");\n";
		sLoopComponent += tab(level) + "port(\n";
		sLoopComponent += tab(level) + "\tClock_i   : in std_logic;\n";
		sLoopComponent += tab(level) + "\tReset_i : in std_logic;\n";
		sLoopComponent += tab(level) + "\tEn_i    : in std_logic;\n";
		sLoopComponent += tab(level) + "\tReady_i : in std_logic;\n";
		sLoopComponent += tab(level) + "\tCount_o : out std_logic_vector(ilogup(Size_g)-1 downto 0)\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
		
		/* Instantiation of read result component */
		sLoopComponent += tab(level) + "component Read_Result is\n";
		sLoopComponent += tab(level) + "generic(\n";
		sLoopComponent += tab(level) + "\tData_Width_g : integer := " + NumType.getDataSize(numType) + ");\n";
		sLoopComponent += tab(level) + "port(\n";
		sLoopComponent += tab(level) + "\tClock_i   : in std_logic;\n";
		sLoopComponent += tab(level) + "\tReset_i : in std_logic;\n";
		sLoopComponent += tab(level) + "\tData_i  : in " + NumType.getVHDLType(numType) + ";\n";
		sLoopComponent += tab(level) + "\tValid_i : in std_logic;\n";
		sLoopComponent += tab(level) + "\tReady_i : in std_logic;\n";
		sLoopComponent += tab(level) + "\tData_o  : out " + NumType.getVHDLType(numType) + ";\n";
		sLoopComponent += tab(level) + "\tValid_o : out std_logic;\n";
		sLoopComponent += tab(level) + "\tReady_o : out std_logic\n";
		sLoopComponent += tab(level) + ");\n";
		sLoopComponent += tab(level) + "end component;\n\n";
		
		sLoopComponent += createComponent(loop, level);	
		
		return sLoopComponent;
	}
	
	
	private String createComponent(Function f, int level) throws VHDLException {
		String sComponent = new String();
	
		/* Get all elements of th body */
		@SuppressWarnings("unchecked")
		Vector<Element> body = (Vector<Element>)f.getBody().clone();
		if(f instanceof LoopFor)
			body.add(((LoopFor)f).getIterOperation());
		Vector<Element> components = findComponent(body);
		
		for (Element el : components) {
			if (el instanceof Operation && !(el instanceof Assignment)) {
				Operation op = (Operation) el;
				BuildingBlock blockOp = op.getBlock();
				if (blockOp == null) {
					throw new VHDLException("No material description for the operation : " +
							  op.getName());
				} else {
					sComponent += tab(level) + "component wrapper_" + blockOp.entityName() + " is\n";
					sComponent += tab(level) + "generic (\n";
					if (op instanceof Multiplexer)
						sComponent += tab(level+1) + "D_Sel_Fifo_Size : integer := 2;\n";
					for(int i = 0; i < op.getNbInput(); i++) {
						sComponent += tab(level+1) +  "D" + (i+1) + "_Fifo_Size : integer := 2";
						sComponent += i == op.getNbInput()-1 ? "\n" + tab(level) + ");\n" : ";\n" ;
					}
					sComponent += tab(level) + "port (\n";
					sComponent += tab(level + 1) + "Clock_i    : in Std_Logic;\n";
					sComponent += tab(level + 1) + "Reset_i    : in Std_Logic;\n";
					for (int i = 0; i < blockOp.nbInputs(); i++) {
						if(op instanceof LogicOperation)
							sComponent += tab(level + 1) + "D" + (i + 1) + "_i       : in Std_Logic;\n";
						else
							sComponent += tab(level + 1) + "D" + (i + 1) + "_i       : in " + NumType.getVHDLType(numType) + ";\n";				
						sComponent += tab(level + 1) + "D" + (i + 1) + "_Valid_i : in Std_Logic;\n";
						sComponent += tab(level + 1) + "D" + (i + 1) + "_Ready_o : out Std_Logic;\n";
					}
					if(op instanceof Multiplexer){
						sComponent += tab(level + 1) + "D_Sel_i       : in Std_Logic;\n";
						sComponent += tab(level + 1) + "D_Sel_Valid_i : in Std_Logic;\n";
						sComponent += tab(level + 1) + "D_Sel_Ready_o : out Std_Logic;\n";
					}
					sComponent += tab(level + 1) + "Ready_i    : in Std_Logic;\n";
					if (op instanceof Comparison) {
						sComponent += tab(level + 1) + "Smaller_o  : out Std_Logic;\n";
						sComponent += tab(level + 1) + "Bigger_o   : out Std_Logic;\n";
						sComponent += tab(level + 1) + "Equal_o    : out Std_Logic;\n";
					} else {
						if(op instanceof LogicOperation)
							sComponent += tab(level + 1) + "Result_o   : out Std_Logic;\n";
						else
							sComponent += tab(level + 1) + "Result_o   : out " + NumType.getVHDLType(numType) + ";\n";
					}
						
					sComponent += tab(level + 1) + "Valid_o    : out Std_Logic\n";
					sComponent += tab(level) + ");\n";
					sComponent += tab(level) + "end component;\n\n";
				}
			} 
			else if (el instanceof Loop) {
				Loop loop = (Loop)el;
				sComponent += tab(level) + "component loop_" + loop.getName() + " is\n";
				sComponent += tab(level) + "port (\n";
				sComponent += tab(level + 1) + "Clock_i\t: in Std_Logic;\n";
				sComponent += tab(level + 1) + "Reset_i\t: in Std_Logic;\n";
				
				/* Generate inputs of the loop component */
				for (int i = 0; i < loop.getInput().size(); i++) {
					if (loop.getInput().elementAt(i) instanceof SimpleVariable) {
						sComponent += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_i\t: in " + NumType.getVHDLType(numType) + ";\n";
						sComponent += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_Valid_i\t: in Std_Logic;\n";
						sComponent += tab(level + 1) + loop.getInput().elementAt(i).getName() + "_Ready_o\t: out Std_Logic;\n";
					}
				}
				
				/* Generate outputs of the loop component */
				for (int i = 0; i < loop.getOutput().size(); i++) {
					sComponent += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_o\t: out " + NumType.getVHDLType(numType) + ";\n";
					sComponent += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_Valid_o\t: out Std_Logic;\n";
					sComponent += tab(level + 1) + loop.getOutput().elementAt(i).getName() + "_Ready_i\t: in Std_Logic";
					sComponent += i == loop.getOutput().size()-1 ? "\n" : ";\n";
				}
				sComponent += tab(level) + ");\n";
				sComponent += tab(level) + "end component;\n\n";
			}
		}

		return sComponent;
	}
	

	/**
	 * Generate VHDL decription for internal declarations of a function. 
	 * @param f     The function element of the struture
	 * @param level The level of tab character
	 * @return the string representation of all internal declarations VHDL generation
	 * @throws VHDLException
	 */
	private String createInternals(Function f, int level) throws VHDLException {
		String sInternals = new String();
	
		/* Go throught all internal elements of the function */
		for (Element var : f.getInternalVars()) 
		{
			if (var instanceof SimpleVariable) 
			{
				if (var.getName().isEmpty())
					throw new NoNameException("Internals");
				
				if (((SimpleVariable)var).getType().equalsIgnoreCase("const"))
					continue;
				
				sInternals += "\n" + tab(level) + "signal " + var.getName() + "_s            : ";
				if (((SimpleVariable)var).getType().equalsIgnoreCase("cond")) {
					sInternals += "std_logic;\n";
				} else if (((SimpleVariable)var).getType().equalsIgnoreCase("resultComp")) {
					sInternals += "std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_smaller_s    : std_logic;\n";		
					sInternals += tab(level) + "signal " + var.getName() + "_bigger_s     : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_equal_s      : std_logic;\n";
				} else {
					sInternals += NumType.getVHDLType(numType) + ";\n";
				}
				sInternals += tab(level) + "signal " + var.getName() + "_Valid_s      : std_logic;\n";		
				sInternals += tab(level) + "signal " + var.getName() + "_Ready_s      : std_logic := '1';\n";
				
				if(((SimpleVariable)var).getSimpleFifo() || (((SimpleVariable)var).getLoopOutputCost(f) != 0 && ((SimpleVariable)var).getLoopOutputConnect(f)))
					sInternals += createFifoSignals((SimpleVariable)var, level);												
				
				/* Create forloopcond internal variable */
				if(f instanceof Loop && ((SimpleVariable)var).getType().equalsIgnoreCase("forloopcond")){
					sInternals += tab(level) + "signal " + var.getName() + "_Init_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_Init_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_Init_Valid_s : std_logic;\n";	
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_Valid_s : std_logic;\n";	
					sInternals += tab(level) + "signal " + var.getName() + "_0_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_0_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_0_Valid_s : std_logic;\n";	
					sInternals += tab(level) + "signal " + var.getName() + "_1_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_1_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_1_Valid_s : std_logic;\n";	
					sInternals += tab(level) + "signal " + var.getName() + "_1_fifo_s         : " + NumType.getVHDLType(numType) + ";\n";
					sInternals += tab(level) + "signal " + var.getName() + "_1_fifo_read_s    : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_1_fifo_write_s   : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_1_fifo_empty_s   : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_1_fifo_full_s    : std_logic;\n";
					sInternals += tab(level) + "signal n_" + var.getName() + "_1_fifo_empty_s : std_logic;\n";
				}
					
				
				/* Create internals variables of init blocs */
				if(f instanceof Loop && ((Loop)f).getLoopVariables().contains(var)) {				
					sInternals += tab(level) + "signal " + var.getName() + "_Init_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_Init_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_Init_Valid_s : std_logic;\n";	
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_s       : " + NumType.getVHDLType(numType) + ";\n";				
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_Ready_s : std_logic;\n";
					sInternals += tab(level) + "signal " + var.getName() + "_Loop_Valid_s : std_logic;\n";	
					
					if(((SimpleVariable)var).getType().equalsIgnoreCase("iter")) {
						sInternals += tab(level) + "signal " + var.getName() + "_Add_s        : " + NumType.getVHDLType(numType) + ";\n";				
						sInternals += tab(level) + "signal " + var.getName() + "_Add_Ready_s  : std_logic;\n";
						sInternals += tab(level) + "signal " + var.getName() + "_Add_Valid_s  : std_logic;\n";	
						
						if(f.getMaxCost() > ((SimpleVariable)((LoopFor)f).getIterOperation().getOutputAt(0)).getCost() || f.containsLoop()) {
							sInternals += createFifoSignals((SimpleVariable)((LoopFor)f).getIterOperation().getOutputAt(0), level);
						}
					}
				}
				
				/* Create mutli-use temporary variable */
				if(((SimpleVariable) var).getVariableMap().get(f) != null) {	
					for(SimpleVariable sv : ((SimpleVariable) var).getVariableMap().get(f).getMultSimpleVar())	{
						if(multUseSignal.get(f).variables.contains(var)) {
							sInternals += tab(level) + "signal " + sv.getName() + "_s       : ";
							if (((SimpleVariable)var).getType().equalsIgnoreCase("cond") || ((SimpleVariable)var).getType().equalsIgnoreCase("resultComp"))
								sInternals += "std_logic;\n";
							else
								sInternals += NumType.getVHDLType(numType) + ";\n";				
							sInternals += tab(level) + "signal " + sv.getName() + "_Ready_s : std_logic;\n";
							sInternals += tab(level) + "signal " + sv.getName() + "_Valid_s : std_logic;\n";	
							
							if(sv.getSimpleFifo() || (sv.getLoopOutputCost(f) != 0 && sv.getLoopOutputConnect(f)))
								sInternals += createFifoSignals(sv, level);
						}
					}	
				}
			} 
			else {
				throw new BadElementException(var.getName(), "Variable", var.getClass().getName());
			}
		}
		
		if(f instanceof Loop) { 	
			/* Generate generic internal signal */
			SimpleVariable var;
			for (SimpleVariable sv : ((Loop)f).getLoopVariables()) {
				if(!sv.getType().equalsIgnoreCase("iter")) {
					if((var = ((Loop)f).getInitVariable(sv)) == null)
						var = sv;
					sInternals += "\n" + tab(level) + "signal D_" + var.getName() + "_s            : " + NumType.getVHDLType(numType) + ";\n";      
					sInternals += tab(level) + "signal D_" + var.getName() + "_fifo_full_s  : std_logic;\n"; 
					sInternals += tab(level) + "signal D_" + var.getName() + "_fifo_empty_s : std_logic;\n"; 
					sInternals += tab(level) + "signal D_" + var.getName() + "_Ready_s      : std_logic;\n"; 
				}
			}
		
			sInternals += "\n" + tab(level) + "signal Data_Valid_s        : std_logic;\n";				
			sInternals += tab(level) + "signal Data_Ready_s        : std_logic;\n";
			sInternals += tab(level) + "signal Data_Init_Valid_s   : std_logic;\n";
			sInternals += tab(level) + "signal Data_Loop_Valid_s   : std_logic;\n";
			sInternals += tab(level) + "signal Cond_Loop_s         : std_logic;\n";
			sInternals += tab(level) + "signal Fifo_Ready_s        : std_logic;\n";
			sInternals += tab(level) + "signal n_Fifo_Ready_s        : std_logic;\n";
			sInternals += tab(level) + "signal Counter_Ready_s     : std_logic;\n\n";
			
			sInternals += tab(level) + "signal Loop_Memory_Ready_s      : std_logic;\n";
			sInternals += tab(level) + "signal Loop_Memory_Valid_s      : std_logic;\n";
			sInternals += tab(level) + "signal Loop_Memory_Write_s      : std_logic;\n";
			sInternals += tab(level) + "signal Loop_Memory_Read_s       : std_logic;\n";
			sInternals += tab(level) + "signal Loop_Memory_Adr_Write_s  : std_logic_vector(ilogup(" + f.getRelativeMaxCost() + ")-1 downto 0);\n";
			sInternals += tab(level) + "signal Loop_Memory_Adr_Read_s   : std_logic_vector(ilogup(" + f.getRelativeMaxCost() + ")-1 downto 0);\n\n";
			
			for(Element e : f.getOutput()) {
				sInternals += tab(level) + "signal Loop_Memory_" + e.getName() + "_Ready_s     : std_logic;\n";
				sInternals += tab(level) + "signal Loop_Memory_" + e.getName() + "_Valid_s     : std_logic;\n";
				sInternals += tab(level) + "signal Loop_Memory_" + e.getName() + "_s           : " + NumType.getVHDLType(numType) + ";\n";	
				sInternals += tab(level) + "signal " + e.getName() + "_Delayed_s               : " + NumType.getVHDLType(numType) + ";\n\n";	
			}
			
			sInternals += tab(level) + "signal Adr_Wr_s              : std_logic_vector(ilogup(" + f.getRelativeMaxCost() + ") -1 downto 0);\n";
			sInternals += tab(level) + "signal Adr_Wr_Valid_s        : std_logic;\n";
			sInternals += tab(level) + "signal Adr_Wr_Ready_s        : std_logic;\n";
			sInternals += tab(level) + "signal Adr_Wr_fifo_s         : std_logic_vector(ilogup(" + f.getRelativeMaxCost() + ") -1 downto 0);\n";
			sInternals += tab(level) + "signal Adr_Wr_fifo_full_s    : std_logic;\n";
			sInternals += tab(level) + "signal Adr_Wr_fifo_empty_s   : std_logic;\n";
			sInternals += tab(level) + "signal Adr_Wr_fifo_write_s   : std_logic;\n";
			sInternals += tab(level) + "signal Adr_Wr_fifo_read_s    : std_logic;\n";
			sInternals += tab(level) + "signal n_Adr_Wr_fifo_empty_s : std_logic;\n\n";
			
	        sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_s              : std_logic_vector(0 downto 0);\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_Valid_s        : std_logic;\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_Ready_s        : std_logic;\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_fifo_s         : std_logic_vector(0 downto 0);\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_fifo_full_s    : std_logic;\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_fifo_empty_s   : std_logic;\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_fifo_write_s   : std_logic;\n";
		    sInternals += tab(level) + "signal Loop_Memory_Adr_Wr_Valid_fifo_read_s    : std_logic;\n";
		    sInternals += tab(level) + "signal n_Loop_Memory_Adr_Wr_Valid_fifo_empty_s : std_logic;\n\n";
		}
		
	
		return sInternals;
	}

	
	/**
	 * Generate VHDL decription for the fifo signal variable.
	 * @param sv 
	 * @param level The level of tab character
	 * @return the string representation of the fifo signal variable VHDL generation
	 */
	private String createFifoSignals(SimpleVariable sv, int level)
	{
		String sFifoSignals = new String();
		
		String type = ((SimpleVariable)sv).getType();
		if (type.equalsIgnoreCase("cond") || type.equalsIgnoreCase("resultComp") )
			sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_s         : std_logic;\n";
		else
			sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_s         : " + NumType.getVHDLType(numType) + ";\n";
	
		sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_read_s    : std_logic;\n";
		sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_write_s   : std_logic;\n";
		sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_empty_s   : std_logic;\n";
		sFifoSignals += tab(level) + "signal " + sv.getName() + "_fifo_full_s    : std_logic;\n";
        sFifoSignals += tab(level) + "signal n_" + sv.getName() + "_fifo_empty_s : std_logic;\n";
		
		return sFifoSignals;
	}
	
	
	/**
	 * Generate VHDL description for the body elements.
	 * @param level The level of tab character
	 * @param body All elements contains into the body
	 * @param parentFunction the parent function of the body
	 * @return the string representation of the body VHDL generation
	 * @throws VHDLException
	 */
	private String createBody(int level, Vector<Element> body, Function parentFunction) throws VHDLException {
		String sBody = new String();
		for (Element el : body) {
			if (el instanceof Assignment) {
				sBody += this.createAssignment((Assignment) el, level, parentFunction);
			} else if (el instanceof Operation) {
				sBody += this.createOp((Operation) el, level, parentFunction);
			} else if (el instanceof Loop) {
				sBody += this.createFunction((Loop) el, level, parentFunction);
			} else if (el instanceof IfThenElse && !el.getName().contains("loop")) {
				sBody += this.createBody(level, ((IfThenElse)el).getBodyFalse(), parentFunction);
				sBody += this.createBody(level, ((IfThenElse)el).getBodyTrue(), parentFunction);
				sBody += this.createBody(level, ((IfThenElse)el).getCond(), parentFunction);
			}
		}
		return sBody;
	}
	
	
	/**
	 * @param el
	 * @param level
	 * @return
	 * @throws VHDLException 
	 */
	private String createFunction(Function el, int level, Function parentFunction) throws VHDLException 
	{
		String sFunc = new String();
		SimpleVariable var;
		
		if (el instanceof Loop)
		{
			/* Instance of classic loop */
			Loop loop = (Loop)el;
			sFunc += "\n" + tab(level) + loop.getName() + "_inst : loop_" + loop.getName() + "\n";
			sFunc += tab(level) + "port map(\n";
			sFunc += tab(level + 1) + "Clock_i\t=> Clock_i,\n";
			sFunc += tab(level + 1) + "Reset_i\t=> Reset_i,\n";
			
			for (int i = 0; i < loop.getInput().size(); i++) {				
				/* Check the validity of the input */
				if (!(loop.getInput().elementAt(i) instanceof SimpleVariable))
					throw new VHDLException("Loop for inputs must be variables.");
				
				var = (SimpleVariable)loop.getInput().elementAt(i);
				
				/* Check if the input is a constant variable */
				if (var.getType().equalsIgnoreCase("const")) {
					sFunc += tab(level + 1) + var.getName() + "_i\t => "  + this.createValues(var.getValues()) + "," + "--=" + var.getValues() + "\n";
					sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> '1',\n";
					sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> open,\n";
				}
				else if(var.getVarFunctionMapping(loop) != null){
					sFunc += tab(level + 1) + var.getName() + "_i\t => " + var.getVarFunctionMapping(loop).getName() + "_s,\n";
					sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> " + var.getVarFunctionMapping(loop).getName() + "_Valid_s,\n";
					sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> " + var.getVarFunctionMapping(loop).getName()  + "_Ready_s,\n";		
				}
				else {
					sFunc += tab(level + 1) + var.getName() + "_i\t => " + var.getName() + "_s,\n";
					sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> " + var.getName() + "_Valid_s,\n";
					sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> " + var.getName() + "_Ready_s,\n";		
				}
			}
			if(loop instanceof LoopFor) {
				/* Check if the start variable is a constant */
				if (!((LoopFor)loop).getStart().getType().equalsIgnoreCase("const") && !loop.getInput().contains(((LoopFor)loop).getStart())) {
					var = ((LoopFor)loop).getStart();
					if(var.getVarFunctionMapping(loop) != null) {	
						sFunc += tab(level + 1) + var.getName() + "_i\t => " + var.getVarFunctionMapping(loop).getName() + "_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> " + var.getVarFunctionMapping(loop).getName() + "_Valid_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> " + var.getVarFunctionMapping(loop).getName()  + "_Ready_s,\n";	
					}
				}
				
				/* Check if the increment variable is a constant */
				if (!((LoopFor)loop).getIncr().getType().equalsIgnoreCase("const") && !loop.getInput().contains(((LoopFor)loop).getIncr())) {
					var = ((LoopFor)loop).getIncr();
					if(var.getVarFunctionMapping(loop) != null) {
						sFunc += tab(level + 1) + var.getName() + "_i\t => " + var.getVarFunctionMapping(loop).getName() + "_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> " + var.getVarFunctionMapping(loop).getName() + "_Valid_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> " + var.getVarFunctionMapping(loop).getName()  + "_Ready_s,\n";	
					}
				}
				
				/* Check if the end variable is a constant */
				if (!((LoopFor)loop).getEnd().getType().equalsIgnoreCase("const") && !loop.getInput().contains(((LoopFor)loop).getEnd())) {
					var = ((LoopFor)loop).getEnd();
					if(var.getVarFunctionMapping(loop) != null) {
						sFunc += tab(level + 1) + var.getName() + "_i\t => " + var.getVarFunctionMapping(loop).getName() + "_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Valid_i\t=> " + var.getVarFunctionMapping(loop).getName() + "_Valid_s,\n";
						sFunc += tab(level + 1) + var.getName() + "_Ready_o\t=> " + var.getVarFunctionMapping(loop).getName()  + "_Ready_s,\n";		
					}
				}
			}
			
			for (int i = 0; i < loop.getOutput().size(); i++) {		
				/* Check the validity of the output */
				if (!(loop.getOutput().elementAt(i) instanceof SimpleVariable))
					throw new VHDLException("Loop for inputs must be variables.");
				
				var = (SimpleVariable)loop.getOutput().elementAt(i);			
				sFunc += tab(level + 1) + this.createVar(var, "_o")       + "\t=> " + this.createVar(var, "_s,") + "\n";
				sFunc += tab(level + 1) + this.createVar(var, "_Valid_o") + "\t=> " + this.createVar(var, "_Valid_s,") + "\n";
				sFunc += tab(level + 1) + this.createVar(var, "_Ready_i") + "\t=> " + this.createVar(var, "_Ready_s");
				sFunc += i == loop.getOutput().size()-1 ? "\n" : ",\n";
			}
			sFunc += tab(level) + ");\n";
			
			SimpleVariable varOutput;
			for (int i = 0; i < loop.getOutput().size(); i++) {		
				varOutput = (SimpleVariable) loop.getOutput().elementAt(i);
				if (!(varOutput instanceof SimpleVariable))
					throw new VHDLException("Loop for inputs must be variables.");
				
				if(multUseSignal.get(parentFunction).variables.contains(varOutput))
					sFunc += "\n" + createTemporaryAssignment(parentFunction, (SimpleVariable) loop.getOutput().elementAt(i), level);
				else if(varOutput.getInputLoopVariable(parentFunction) != null) {
					sFunc += tab(level) + varOutput.getName()  + "_Ready_s <= " + varOutput.getInputLoopVariable(parentFunction).getName() + "_Loop_Ready_s;\n";
					sFunc += tab(level) + varOutput.getInputLoopVariable(parentFunction).getName()  + "_Loop_s <= " +  varOutput.getName() + "_s;\n";
					sFunc += tab(level) + varOutput.getInputLoopVariable(parentFunction).getName()  + "_Loop_Valid_s <= " + varOutput.getName()  + "_Valid_s;\n";
				}
			}
		}
			
		return sFunc;
	}

	/**
	 * Generate VHDL description for a loop.
	 * @param loop  The loop element of the structure
	 * @param level The level of tab character
	 * @return the string representation of the loop VHDL generation
	 * @throws VHDLException
	 */
	private String createLoop(Loop loop, int level) throws VHDLException 
	{	
		String sLoop = new String();
		sLoop += createLoopEntity(loop, level);
		sLoop += "architecture comp of " + "loop_" + loop.getName() + " is\n\n";
		sLoop += createLoopComponent(loop, level+1);
		sLoop += createInternals(loop, level+1);
		sLoop += "begin\n\n";
		
		/* Generate initial assignment */
		SimpleVariable initVar;
		for(SimpleVariable var : loop.getLoopVariables()) {			
			if(var.getType().equalsIgnoreCase("iter")) {
				if(!((LoopFor)loop).getStart().getType().equalsIgnoreCase("const")) {				
					sLoop += tab(level+1) + var.getName() + "_Init_s <= D_" + ((LoopFor)loop).getStart().getName() + "_s;\n";
					sLoop += tab(level+1) + var.getName() + "_Init_Valid_s <= not D_" + ((LoopFor)loop).getStart().getName() + "_fifo_empty_s;\n";
				}
				else {					
					sLoop += tab(level+1) + var.getName() + "_Init_s <= " + createValues(((LoopFor)loop).getStart().getValues()) + ";\n";
					sLoop += tab(level+1) + var.getName() + "_Init_Valid_s <= '1';\n";
				}
			}
			else if((initVar = loop.getInitVariable(var)) != null) {		
				
				String name=initVar.getName();
				SimpleVariable sv=initVar.getVarFunctionMapping(loop);
				int cost=sv.getSimpleFifoCost(project);
				
				//sLoopFor += createFifoInitLoopVariable(initVar.getVarFunctionMapping(loop).getSimpleFifoCost(), initVar.getName(), level);
				sLoop += createFifoInitLoopVariable(cost, name, level);				
				
				sLoop += tab(level+1) + initVar.getName() + "_Ready_o <= not D_" + initVar.getName()  + "_fifo_full_s;\n";
				sLoop += tab(level+1) + var.getName() + "_Init_s <= D_" + initVar.getName()  + "_s;\n";
				sLoop += tab(level+1) + "D_" + initVar.getName()  + "_Ready_s <= not " + var.getName() + "_Init_Valid_s or (Data_Init_Valid_s and " + var.getName() + "_Init_Ready_s";
				if(initVar.getType().equals("forloopcond"))
					sLoop += " and " + initVar.getName() + "_Init_Ready_s";
			    sLoop += ");\n";
				sLoop += tab(level+1) + var.getName() + "_Init_Valid_s <=  not D_" + initVar.getName()  + "_fifo_empty_s;\n";
				
				if(initVar.getType().equals("forloopcond"))
				{
					sLoop += "\n";
					sLoop += tab(level+1) + initVar.getName() + "_Init_s <= D_" + initVar.getName() + "_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_Init_Valid_s <= not D_" + initVar.getName() + "_fifo_empty_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_Ready_s <= " + initVar.getName() + "_0_Ready_s and " + initVar.getName() + "_1_Ready_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_0_s <= " + initVar.getName() + "_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_0_Valid_s <= Data_Ready_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_1_s <= " + initVar.getName() + "_s;\n";
					sLoop += tab(level+1) + initVar.getName() + "_1_Valid_s <= Data_Ready_s;\n\n";
				}
			}	
			else {
				sLoop += createFifoInitLoopVariable(var.getVarFunctionMapping(loop).getSimpleFifoCost(project), var.getName(), level);
				sLoop += tab(level+1) + var.getName() + "_Ready_o <= not D_" + var.getName() + "_fifo_full_s;\n";
				sLoop += tab(level+1) + var.getName() + "_Init_s <= D_" + var.getName() + "_s;\n";
				sLoop += tab(level+1) + "D_" + var.getName() + "_Ready_s <= not " + var.getName() + "_Init_Valid_s or (Data_Init_Valid_s and " + var.getName() + "_Init_Ready_s);\n";
				sLoop += tab(level+1) + var.getName() + "_Init_Valid_s <=  not D_" + var.getName() + "_fifo_empty_s;\n";
			}	
			
			sLoop += tab(level+1) + var.getName() + "_Ready_s <= ";
			if(var.getVariableMap().get(loop) != null)
				for(SimpleVariable sv : var.getVariableMap().get(loop).getMultSimpleVar()) {
					sLoop += sv.getName() + "_Ready_s";
					sLoop += var.getVariableMap().get(loop).getMultSimpleVar().lastElement().equals(sv) ? ";\n" : " and ";
				}
			
			if(var.getVariableMap().get(loop) != null)
				for(SimpleVariable sv : var.getVariableMap().get(loop).getMultSimpleVar()) {
					sLoop += tab(level+1) + sv.getName() + "_s <= " + var.getName() + "_s;\n";
					sLoop += tab(level+1) + sv.getName() + "_Valid_s <= Data_Ready_s;\n";
				}
			sLoop += "\n";
		}
		
		/* Generate global assignment */
		String sDataInitValid = new String();
		String sDataReady = new String();
		String sDataValid = new String();
		String sDataLoopValid = new String();	
		sDataInitValid += tab(level+1) + "Data_Init_Valid_s   <= ";
		sDataValid     += tab(level+1) + "Data_Valid_s        <= ";
		sDataReady     += tab(level+1) + "Data_Ready_s        <= Data_Valid_s and ";
		sDataLoopValid += tab(level+1) + "Data_Loop_Valid_s   <= ";	
		for(SimpleVariable var : loop.getLoopVariables()) {		
			if(!loop.getLoopVariables().lastElement().equals(var)) {
				if((initVar = loop.getInitVariable(var)) != null && initVar.getType().equals("forloopcond")) {
					sDataInitValid += initVar.getName() + "_Init_Valid_s and ";
					sDataValid     += initVar.getName() + "_Valid_s and ";
					sDataReady     += initVar.getName() + "_Ready_s and ";
					sDataLoopValid += initVar.getName() + "_Loop_Valid_s and ";			
				}
				sDataInitValid += var.getName() + "_Init_Valid_s and ";
				sDataValid     += var.getName() + "_Valid_s and ";
				sDataReady     += var.getName() + "_Ready_s and ";
				sDataLoopValid += var.getName() + "_Loop_Valid_s and ";

			}
			else {
				if((initVar = loop.getInitVariable(var)) != null && initVar.getType().equals("forloopcond")) {
					sDataInitValid += initVar.getName() + "_Init_Valid_s and ";
					sDataValid     += initVar.getName() + "_Valid_s and ";
					sDataReady     += initVar.getName() + "_Ready_s and ";
					sDataLoopValid += initVar.getName() + "_Loop_Valid_s and ";			
				}
				sDataInitValid += var.getName() + "_Init_Valid_s;\n";
				sDataValid     += var.getName() + "_Valid_s;\n";
				sDataReady     += var.getName() + "_Ready_s;\n";
				sDataLoopValid += var.getName() + "_Loop_Valid_s;\n";
			}
		}
		sLoop += sDataInitValid + sDataReady + sDataValid + sDataLoopValid;
		
		// TODO Adapter aux cas particuliers
		SimpleVariable cond = (SimpleVariable)((IfThenElse)loop.getBody().elementAt(0)).getInternalVars().elementAt(0);
		if(cond.getSimpleFifo()) {
			sLoop += tab(level+1) + "Fifo_Ready_s        <= n_" + cond.getName() + "_fifo_empty_s and Data_Loop_Valid_s;\n";
			sLoop += tab(level+1) + "n_Fifo_Ready_s      <= not Fifo_Ready_s;\n";		
			sLoop += tab(level+1) + "Cond_Loop_s         <= not ("+ cond.getName() + "_fifo_s and Fifo_Ready_s);\n\n";
			sLoop += tab(level+1) + "Loop_Memory_Write_s <= not " + cond.getName() + "_fifo_s and Fifo_Ready_s;\n";
		}
		else {
			sLoop += tab(level+1) + "Fifo_Ready_s        <= " + cond.getName() + "_Valid_s and Data_Loop_Valid_s;\n";
			sLoop += tab(level+1) + "n_Fifo_Ready_s      <= not Fifo_Ready_s;\n";		
			sLoop += tab(level+1) + "Cond_Loop_s         <= not ("+ cond.getName() + "_s and Fifo_Ready_s);\n\n";
			sLoop += tab(level+1) + "Loop_Memory_Write_s <= not " + cond.getName() + "_s and Fifo_Ready_s;\n";
		}
		
		/* Generate components for reading results */
		String sLoopMemoryRead  = new String();
		String sLoopMemoryValid = new String();
		String sLoopMemoryReady = new String();
		for(Element e : loop.getOutput()) {
			if(!loop.getOutput().lastElement().equals(e)) {
				sLoopMemoryRead  += "Loop_Memory_" + e.getName() + "_Ready_s and ";
				sLoopMemoryValid += "Loop_Memory_" + e.getName() + "_Valid_s and ";
				sLoopMemoryReady += e.getName() + "_Ready_i and ";
			}
			else {
				sLoopMemoryRead  += "Loop_Memory_" + e.getName() + "_Ready_s;\n";
				sLoopMemoryValid += "Loop_Memory_" + e.getName() + "_Valid_s;\n";
				sLoopMemoryReady += e.getName() + "_Ready_i;\n\n";
			}
		}
		sLoop += tab(level+1) + "Loop_Memory_Read_s  <= " + sLoopMemoryRead;
		sLoop += tab(level+1) + "Loop_Memory_Valid_s <= " + sLoopMemoryValid;
		sLoop += tab(level+1) + "Loop_Memory_Ready_s <= " + sLoopMemoryReady;
		
		/* Generate init Blocs */
		for(SimpleVariable var : loop.getLoopVariables()) {		
			sLoop += createInitBlocInstantiation(var, level);	
			if((initVar = loop.getInitVariable(var)) != null && initVar.getType().equals("forloopcond"))
				sLoop += createInitBlocInstantiation(initVar, level);
		}
		
		// Create fifo of loop variable
		sLoop += createFifo(loop);
			
		/* Generate the loop iterator operation */
		if(loop instanceof LoopFor) {
			LoopFor loopFor = (LoopFor) loop;
			Addition add = new Addition();
			add.setBlock(loopFor.getIterOperation().getBlock());
			SimpleVariable iter = (SimpleVariable)loopFor.getInternalVars().elementAt(0);
			int minCost = multUseSignal.get(loopFor).getMinCost(iter);
			add.setName("add_" + loopFor.getIterOperation().getName());
			if(loopFor.getIncr().getType().equalsIgnoreCase("const") || loopFor.getIncr().getType().equalsIgnoreCase("forloopcond"))
				add.addInput(loopFor.getIncr());
			else
				add.addInput(loopFor.getIncr().getVariableMap().get(loopFor).getMapVHDLSimpleVariable(0));
			add.addInput(iter.getVariableMap().get(loopFor).getMapVHDLSimpleVariable(minCost));
			add.addOutput(new SimpleVariable(iter.getName() + "_Add"));
			sLoop += createOp(add, level+1, loopFor);
			if(iter.getCost() + loopFor.getIterOperation().getBlock().latencyTime()+1 < loopFor.getRelativeMaxCost()-1 || loop.containsLoop()) {
				SimpleVariable sv = (SimpleVariable)loopFor.getIterOperation().getOutputAt(0);
				sLoop += createFifoInstanciation(sv, loopFor.getRelativeMaxCost()-add.getBlock().latencyTime(), NumType.getDataSize(numType), false, loop);
				sLoop += tab(1) + iter.getName() + "_Loop_s <= " +  iter.getName() + "_Add_fifo_s;\n";
				sLoop += tab(1) + iter.getName() + "_Loop_Valid_s <= " + "n_" + iter.getName() + "_Add_fifo_empty_s;\n";
			}
			else {
				sLoop += tab(1) + iter.getName() + "_Add_Ready_s <= " +  iter.getName() + "_Loop_Ready_s;\n";
				sLoop += tab(1) + iter.getName() + "_Loop_s <= " +  iter.getName() + "_Add_s;\n";
				sLoop += tab(1) + iter.getName() + "_Loop_Valid_s <= " + iter.getName() + "_Add_Valid_s;\n";
			}
		}
		
		// Generate the condition loop
		sLoop += createBody(level+1, ((IfThenElse)loop.getBody().firstElement()).getCond(), loop);
		
		// Generate the body of the loop
		sLoop += createBody(level+1, loop.getBody(), loop);
		
		/* Generate init counter */
		sLoop += "\n" + tab(level+1) + "Inst_Counter_Wr : Counter_Wr\n";
		sLoop += tab(level+1) + "generic map(\n";
		sLoop += tab(level+2) + "\tData_Size_g => " + NumType.getDataSize(numType) + ",\n";
		sLoop += tab(level+2) + "\tMem_Size_g  => " + loop.getRelativeMaxCost() + "\n";
		sLoop += tab(level+1) + ")\n";
		sLoop += tab(level+1) + "port map(\n";
		sLoop += tab(level+2) + "Clock_i      => Clock_i,\n";
		sLoop += tab(level+2) + "Reset_i      => Reset_i,\n";
		sLoop += tab(level+2) + "Data_i       => Adr_Wr_fifo_s,\n";
		sLoop += tab(level+2) + "Data_Valid_i => Loop_Memory_Adr_Wr_Valid_fifo_s(0),\n";
		sLoop += tab(level+2) + "Read_i       => Loop_Memory_Valid_s,\n";
		sLoop += tab(level+2) + "Adr_Rd_i     => Loop_Memory_Adr_Read_s,\n";
		sLoop += tab(level+2) + "Ready_i      => Data_Init_Valid_s,\n";
		sLoop += tab(level+2) + "Fifo_Ready_i => n_Fifo_Ready_s,\n";
		sLoop += tab(level+2) + "Cond_i       => Cond_Loop_s,\n";
		sLoop += tab(level+2) + "Data_o       => Adr_Wr_s,\n";
		sLoop += tab(level+2) + "Data_Valid_o => Loop_Memory_Adr_Wr_Valid_s(0),\n";
		sLoop += tab(level+2) + "Ready_o      => Counter_Ready_s\n";
		sLoop += tab(level+1) + ");\n";
		
		sLoop += createFifoInstanciation(new SimpleVariable("Adr_Wr"), loop.getRelativeMaxCost(), loop.getRelativeMaxCost(), true, loop);
		sLoop += tab(level+1) + "Adr_Wr_Valid_s <= Data_Ready_s;\n";
		sLoop += tab(level+1) + "Loop_Memory_Adr_Write_s <= Adr_Wr_fifo_s;\n\n";
		sLoop += createFifoInstanciation(new SimpleVariable("Loop_Memory_Adr_Wr_Valid"), loop.getRelativeMaxCost(), 1, false, loop);
		sLoop += tab(level+1) + "Loop_Memory_Adr_Wr_Valid_Valid_s <= Data_Ready_s;\n\n";
		
		
		/* Generate read counter component */
		sLoop += tab(level+1) + "Inst_Counter_Rd : Counter_Rd\n";
		sLoop += tab(level+1) + "generic map(\n";
		sLoop += tab(level+2) + "Size_g => " + loop.getRelativeMaxCost() + "\n";
		sLoop += tab(level+1) + ")\n";
		sLoop += tab(level+1) + "port map(\n";
		sLoop += tab(level+2) + "Clock_i => Clock_i,\n";
		sLoop += tab(level+2) + "Reset_i => Reset_i,\n";
		sLoop += tab(level+2) + "En_i    => Loop_Memory_Valid_s,\n";
		sLoop += tab(level+2) + "Ready_i => Loop_Memory_Ready_s,\n";
		sLoop += tab(level+2) + "Count_o => Loop_Memory_Adr_Read_s\n";
		sLoop += tab(level+1) + ");\n\n";
		
		/* Generate components for reading results */
		for(Element e : loop.getOutput()) {
			String name = e.getName();
			
			/* Generate loop memory component */
			sLoop += tab(level+1) + name + "_Memory : Memory\n";
			sLoop += tab(level+1) + "generic map(\n";
			sLoop += tab(level+2) + "Data_Width_g => " + NumType.getDataSize(numType) + ",\n";
			sLoop += tab(level+2) + "Mem_Size_g   => " + loop.getRelativeMaxCost() + "\n";
			sLoop += tab(level+1) + ")\n";
			sLoop += tab(level+1) + "port map(\n";
			sLoop += tab(level+2) + "Clock_i      => Clock_i,\n";
			sLoop += tab(level+2) + "Reset_i      => Reset_i,\n";
			sLoop += tab(level+2) + "Wr_i         => Loop_Memory_Write_s,\n";
			sLoop += tab(level+2) + "Rd_i         => Loop_Memory_Read_s,\n";
			sLoop += tab(level+2) + "Adr_Wr_i     => Loop_Memory_Adr_Write_s,\n";
			sLoop += tab(level+2) + "Adr_Rd_i     => Loop_Memory_Adr_Read_s,\n";
			sLoop += tab(level+2) + "Adr_Valid_i  => Loop_Memory_Adr_Wr_Valid_fifo_s(0),\n";
			sLoop += tab(level+2) + "Data_i       => " + name + "_Delayed_s,\n";
			sLoop += tab(level+2) + "Data_Valid_o => Loop_Memory_" + name + "_Valid_s,\n";
			sLoop += tab(level+2) + "Data_o       => Loop_Memory_" + name + "_s\n";
			sLoop += tab(level+1) + ");\n\n";
			
			/* Generate read result component */
			sLoop += tab(level+1) + "Read_" + name + "_Mem_Result : Read_Result\n";
			sLoop += tab(level+1) + "generic map(\n";
			sLoop += tab(level+2) + "Data_Width_g => " + NumType.getDataSize(numType) + "\n";
			sLoop += tab(level+1) + ")\n";
			sLoop += tab(level+1) + "port map(\n";
			sLoop += tab(level+2) + "Clock_i => Clock_i,\n";
			sLoop += tab(level+2) + "Reset_i => Reset_i,\n";
			sLoop += tab(level+2) + "Data_i  => Loop_Memory_" + name + "_s,\n";
			sLoop += tab(level+2) + "Valid_i => Loop_Memory_" + name + "_Valid_s,\n";
			sLoop += tab(level+2) + "Ready_o => Loop_Memory_" + name + "_Ready_s,\n";
			sLoop += tab(level+2) + "Data_o  => " + name + "_o,\n";
			sLoop += tab(level+2) + "Valid_o => " + name + "_Valid_o,\n";
			sLoop += tab(level+2) + "Ready_i => " + name + "_Ready_i\n";
			sLoop += tab(level+1) + ");\n\n";
		}	
		
		sLoop += "end comp;";
		
		return sLoop;
	}


	/**
	 * @param simpleFifoCost
	 * @param name
	 * @return
	 */
	private String createFifoInitLoopVariable(int size, String name, int level) {
		String sLoop = new String();	
		
		sLoop += tab(level+1) + "Fifo_D_" + name + ": fifo_Std\n";
		sLoop += tab(level+1) + "generic map (\n";
		sLoop += tab(level+2) + "FIFOSIZE  => " + (2+size) + ",\n";
		sLoop += tab(level+2) + "DATAWIDTH => " + NumType.getDataSize(numType) + "\n";
		sLoop += tab(level+1) + ")\n";
		sLoop += tab(level+1) + "port map(\n";
		sLoop += tab(level+2) + "Clock_i      => Clock_i,\n";
		sLoop += tab(level+2) + "Reset_i      => Reset_i,\n";
		sLoop += tab(level+2) + "Wr_Req_i     => " + name + "_Valid_i,\n";
		sLoop += tab(level+2) + "Rd_Req_i     => D_" + name + "_Ready_s,\n";
		sLoop += tab(level+2) + "Data_i       => " + name + "_i,\n";
		sLoop += tab(level+2) + "Data_o       => D_" + name + "_s,\n";
		sLoop += tab(level+2) + "Fifo_Full_o  => D_" + name + "_fifo_full_s,\n";
		sLoop += tab(level+2) + "Fifo_Empty_o => D_" + name + "_fifo_empty_s\n";
		sLoop += tab(level+1) + ");\n"; 		
		
		return sLoop;
	}


	/**
	 * This method intanciate all fifos needed for all the signals used in multiples inputs.
	 * @return the string representation of the fifo instanciation
	 */
	private String createFifo(Function f)
	{
		String sFifo = new String();	
		SimpleVariable initVar;
		project.setUseFifo(false);
		
		for (Element var : f.getInternalVars()) {
			if (var instanceof SimpleVariable && !((SimpleVariable)var).getType().equals("const")) {	
				/* Instantiation of fifo simple variable */
				if(((SimpleVariable)var).getSimpleFifo())
					sFifo += createFifoInstanciation((SimpleVariable)var, f.getRelativeMaxCost(),  NumType.getDataSize(numType), false, f);		
				
				if(((SimpleVariable)var).getLoopOutputCost(f) != 0 && ((SimpleVariable)var).getLoopOutputConnect(f))
					sFifo += createFifoInstanciation((SimpleVariable)var, f.getRelativeMaxCost()-((SimpleVariable)var).getLoopOutputCost(f)+1,  NumType.getDataSize(numType), false, f);
				
				/* Instantiation of fifo multi-use variable */
				if(((SimpleVariable) var).getVariableMap().get(f) != null) {			
					for(SimpleVariable sv : ((SimpleVariable) var).getVariableMap().get(f).getMultSimpleVar()) {
						if(sv.getSimpleFifo())
							sFifo += createFifoInstanciation(sv, f.getRelativeMaxCost(),  NumType.getDataSize(numType), false, f);
						if(sv.getLoopOutputCost(f) != 0 && sv.getLoopOutputConnect(f))
							sFifo += createFifoInstanciation(sv, f.getRelativeMaxCost()-sv.getLoopOutputCost(f)+1,  NumType.getDataSize(numType), false, f);
					}
				}
				
				if(f instanceof Loop && (initVar = ((Loop)f).getInitVariable(((SimpleVariable)var))) != null 
						             && initVar.getType().equals("forloopcond")) 
					sFifo += createFifoInstanciation(initVar, f.getRelativeMaxCost(),  NumType.getDataSize(numType), false, f);
			} 
		}
		
		return sFifo;
	}
		
	
	private String createInitBlocInstantiation(SimpleVariable sv, int level)
	{
		String sInitBlockInstanciation = new String();
		sInitBlockInstanciation += tab(level+1) + "Init_" + sv.getName() + " : Init_Input\n";
		sInitBlockInstanciation += tab(level+1) + "port map(\n";
		sInitBlockInstanciation += tab(level+2) + "Clock_i           => Clock_i,\n";
		sInitBlockInstanciation += tab(level+2) + "Reset_i           => Reset_i,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_i            => " + sv.getName() + "_Init_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Valid_i      => Data_Init_Valid_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Ready_o      => " + sv.getName() + "_Init_Ready_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Loop_i       => " + sv.getName() + "_Loop_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Loop_Valid_i => Data_Loop_Valid_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Loop_Ready_o => " + sv.getName() + "_Loop_Ready_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Cond_Loop_i       => Counter_Ready_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_o            => " + sv.getName() + "_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Valid_o      => " + sv.getName() + "_Valid_s,\n";
		sInitBlockInstanciation += tab(level+2) + "Data_Ready_i      => Data_Ready_s\n";
		sInitBlockInstanciation += tab(level+1) + ");\n\n";		
		return sInitBlockInstanciation;
	}
	
	private String createFifoInstanciation(SimpleVariable sv, int fifoSize, int dataSize, boolean ilogup, Function parentFunction) 
	{	
		int realFifoSize = project.getOptimisationProperties().getOptimizeFifo() ? project.getOptimisationProperties().getFifoSize() : fifoSize;
		String sFifoInstanciation = new String();

		// Get the type of fifo the user wants (Standard/Altera/Xilinx)
		String fifoType = project.getOptimisationProperties().getFifoType();	
		
		// Get the name of the output signal that is used at multiples inputs
		String sigNameOut = sv.getName();
		if(sv.getType().equals("forloopcond"))
			sigNameOut += "_1"; 

		/* Set the type of fifo */
		sFifoInstanciation += "\n" + tab(1) + "fifo" + "_" + sigNameOut + ": ";
		if (fifoType.equalsIgnoreCase("Altera")) {
			sFifoInstanciation += "fifo_Altera\n";
		} else if (fifoType.equalsIgnoreCase("Xilinx")) {
			sFifoInstanciation += "fifo_Xilinx\n";
		} else {
			sFifoInstanciation += "fifo_Std\n";
		}
		
		/* Set the size of the fifo */
		if (sv.getType().equalsIgnoreCase("cond") || sv.getType().equalsIgnoreCase("resultComp") ) {
			sFifoInstanciation += tab(1) + "generic map(\n";
			sFifoInstanciation += tab(2) + "FIFOSIZE => " +  (realFifoSize) + ",\n";
			sFifoInstanciation += tab(2) + "DATAWIDTH => 1)\n";
		} else {
			sFifoInstanciation += tab(1) + "generic map(\n";
			sFifoInstanciation += tab(2) + "FIFOSIZE => " +  (realFifoSize) + ",\n";
			if(!ilogup)
				sFifoInstanciation += tab(2) + "DATAWIDTH => " +  dataSize + ")\n";
			else
				sFifoInstanciation += tab(2) + "DATAWIDTH => ilogup(" +  dataSize + "))\n";
		}
		
		/* Mapping of the fifo */
		sFifoInstanciation += tab(1) + "port map(\n";
		sFifoInstanciation += tab(2) + "Clock_i      => Clock_i,\n";
		sFifoInstanciation += tab(2) + "Reset_i      => Reset_i,\n";
		sFifoInstanciation += tab(2) + "Wr_Req_i     => " + sigNameOut + "_fifo_write_s,\n";
		sFifoInstanciation += tab(2) + "Rd_Req_i     => " + sigNameOut + "_fifo_read_s,\n";
		if (sv.getType().equalsIgnoreCase("cond") || sv.getType().equalsIgnoreCase("resultComp")) {
			sFifoInstanciation += tab(2) + "Data_i(0)    => " + sigNameOut + "_s,\n";
			sFifoInstanciation += tab(2) + "Data_o(0)    => " + sigNameOut + "_fifo_s,\n";
		} else {
			sFifoInstanciation += tab(2) + "Data_i       => " + sigNameOut + "_s,\n";
			sFifoInstanciation += tab(2) + "Data_o       => " + sigNameOut + "_fifo_s,\n";
		}				
		sFifoInstanciation += tab(2) + "Fifo_Full_o  => " + sigNameOut  + "_fifo_full_s,\n";
		sFifoInstanciation += tab(2) + "Fifo_Empty_o => " + sigNameOut + "_fifo_empty_s\n";
		sFifoInstanciation += tab(1) + ");\n";
		sFifoInstanciation += tab(1) + "n_" + sigNameOut + "_fifo_empty_s <= " + "not " + sigNameOut + "_fifo_empty_s;\n";
		sFifoInstanciation += tab(1) + sigNameOut + "_fifo_write_s   <= " + sigNameOut + "_Valid_s;\n";
		sFifoInstanciation += tab(1) + sigNameOut + "_fifo_read_s    <= Fifo_Ready_s;\n";
		sFifoInstanciation += tab(1) + sigNameOut + "_Ready_s        <= " + "not " + sigNameOut + "_fifo_full_s;\n";
		
		if(sv.getInputLoopVariable(parentFunction) != null) {
			sFifoInstanciation += tab(1) + sv.getInputLoopVariable(parentFunction).getName()  + "_Loop_s <= " +  sigNameOut + "_fifo_s;\n";
			sFifoInstanciation += tab(1) + sv.getInputLoopVariable(parentFunction).getName()  + "_Loop_Valid_s <= " + "n_" + sigNameOut + "_fifo_empty_s;\n";
		}
		else if(sv.getOutputLoopVariable() != null && parentFunction.getOutput().contains(sv.getOutputLoopVariable())) {
			sFifoInstanciation += tab(1) + sv.getOutputLoopVariable().getName()  + "_Delayed_s <= " +  sigNameOut + "_fifo_s;\n";			
		}
		else if (sv.getType().equals("forloopcond")) {
			sFifoInstanciation += tab(1) + sv.getName()  + "_Loop_s <= " +  sigNameOut + "_fifo_s;\n";
			sFifoInstanciation += tab(1) + sv.getName()  + "_Loop_Valid_s <= n_" +  sigNameOut + "_fifo_empty_s;\n";
		}
			
		return sFifoInstanciation;
	}

	
	/**
	 * Generate VHDL assignment.
	 * 
	 * @param aff The assignment to generate
	 * @param level The level of tab character
	 * @param parentFunction the parent function of the assignment
	 * @return the string representation of the assignment VHDL geeration
	 */
	private String createAssignment(Assignment aff, int level, Function parentFunction) 
	{
		String sAssign = new String();
			
		if (aff.getOutputAt(0) instanceof SimpleVariable) 
		{
			/* Get input and output varaible of the assignment */
			SimpleVariable varIn  = (SimpleVariable)aff.getInputAt(0);
			SimpleVariable varOut = (SimpleVariable)aff.getOutputAt(0);
			
			if (!varIn.getType().equals("const")) 
			{
				/* Mapping of usual assignment */
				if(top.getInput().contains(varIn)) {
					sAssign += "\n" + tab(level) + this.createVar(varOut, "_s") + " <= " + this.createVar(varIn, "_i") + ";\n";			
					sAssign += tab(level) + this.createVar(varIn, "_Ready_o") + " <= " + this.createVar(varOut, "_Ready_s") + ";\n";	
					sAssign += tab(level) + this.createVar(varOut, "_Valid_s") + " <= " + this.createVar(varIn, "_Valid_i") + ";\n";
				}
				else if(top.getOutput().contains(varOut)) {
					sAssign += "\n" + tab(level) + this.createVar(varOut, "_o") + " <= " + this.createVar(varIn, "_s") + ";\n";			
					sAssign += tab(level) + this.createVar(varIn, "_Ready_s") + " <= " + this.createVar(varOut, "_Ready_i") + ";\n";	
					sAssign += tab(level) + this.createVar(varOut, "_Valid_o") + " <= " + this.createVar(varIn, "_Valid_s") + ";\n";		
				}
				else {
					sAssign += "\n" + tab(level) + this.createVar(varOut, "_s") + " <= " + this.createVar(varIn, "_s") + ";\n";			
					sAssign += tab(level) + this.createVar(varIn, "_Ready_s") + " <= " + this.createVar(varOut, "_Ready_s") + ";\n";	
					sAssign += tab(level) + this.createVar(varOut, "_Valid_s") + " <= " + this.createVar(varIn, "_Valid_s") + ";\n";
				}				
								
				if(multUseSignal.get(parentFunction).variables.contains(varOut)) {
					sAssign += createTemporaryAssignment(parentFunction, varOut, level);			
				}
			}
		}
			
		return sAssign;
	}

	
	/**
	 * Generate VHDL temporary assignment.
	 * 
	 * @param var The variable of temporary assignment to generate
	 * @param level The level of tab character
	 * @return the string representation of the temporary assignment VHDL geeration
	 */
	private String createTemporaryAssignment(Function parentFunction, SimpleVariable var, int level) 
	{
		String sTempAssign = new String();
			
		/* Mapping of the "ready" output variable for mult variables */
		sTempAssign += tab(level) + this.createVar(var, "_Ready_s") + " <= ";	
		for(SimpleVariable sv : var.getVariableMap().get(parentFunction).getMultSimpleVar()) {
			if(sv.equals(var.getVariableMap().get(parentFunction).getMultSimpleVar().lastElement()))
				sTempAssign += this.createVar(sv, "_Ready_s") + ";\n";
			else
				sTempAssign += this.createVar(sv, "_Ready_s") + " and ";
		}					
					
		/* Mapping of mult variables */
		for(SimpleVariable sv : var.getVariableMap().get(parentFunction).getMultSimpleVar()) {
			sTempAssign += tab(level) + this.createVar(sv, "_s") + " <= " + this.createVar(var, "_s") + ";\n";			
			sTempAssign += tab(level) + this.createVar(sv, "_Valid_s") + " <= " + this.createVar(var, "_Valid_s");
			for(SimpleVariable sv2 : var.getVariableMap().get(parentFunction).getMultSimpleVar())
				if(!sv2.equals(sv))
					sTempAssign += " and " + this.createVar(sv2, "_Ready_s");
			sTempAssign += ";\n";	
		}					

		return sTempAssign;
	}
	
	
	/**
	 * Generate the VHDL representation of an operation.
	 * @param op The operation to generate
	 * @param level The level of tab character
	 * @param parentFunction the parent function of the operation
	 * @return The VHDL representation of an operation.
	 * @throws VHDLException
	 */
	private String createOp(Operation op, int level, Function parentFunction) throws VHDLException 
	{
		String sOp = new String();

		/* Get the building bloc of the operation */
		BuildingBlock blockOp = op.getBlock();
		if (blockOp == null)
			throw new VHDLException("No material description for the operation : " + op.getName());
		
		/* Assignment of temporary signal if the variable is a temporary variable */
		if(multUseSignal.get(parentFunction).variables.contains(op.getOutputAt(0))) {
			sOp += "\n" + createTemporaryAssignment(parentFunction, (SimpleVariable) op.getOutputAt(0), level);
		}	
		
		/* Instance of classic operation */
		sOp += "\n" + tab(level) + "m2m_"+op.getName() + ": wrapper_" + blockOp.entityName() + "\n";
		sOp += tab(level) + "generic map(\n";
		if(op instanceof Multiplexer)
			sOp += tab(level+1) + "D_Sel_Fifo_Size => " + (2+((SimpleVariable)((Multiplexer)op).getSel()).getSimpleFifoCost(project)) + ",\n";
		for(int i = 0; i < op.getNbInput(); i++) {
			SimpleVariable var = (SimpleVariable)op.getInputAt(i);
			sOp += tab(level+1) + "D" + (i+1) + "_Fifo_Size => " + (2+var.getSimpleFifoCost(project));
			sOp += i == op.getNbInput()-1 ? "\n" + tab(level) + ")\n" : ",\n" ;
		}
		
		sOp += tab(level) + "port map(\n";
		sOp += tab(level + 1) + "Clock_i      => Clock_i,\n";
		sOp += tab(level + 1) + "Reset_i      => Reset_i,\n";
		
		/* Mapping of input signals of the operation */
		for (int i = 0; i < blockOp.nbInputs(); i++) 
		{
			if (op.getInputAt(i) instanceof Operation) 
				throw new VHDLException("All operational entries must be variables.");
			
			SimpleVariable var = (SimpleVariable)op.getInputAt(i);
			
			/* If the variable is a constant, just write the value of the constant, not the name */
			if (var.getType().equals("const")) {		
				sOp += tab(level + 1) + "D" + (i + 1) + "_i         => "  + this.createValues(var.getValues()) + "," + "--=" + var.getValues() + "\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Valid_i   => '1',\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Ready_o   => open,\n";
			}
			else if(var.getType().equals("forloopcond")) {
				sOp += tab(level + 1) + "D" + (i + 1) + "_i         => " + this.createVar(var, "_0_s") + ",\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Valid_i   => " + this.createVar(var, "_0_Valid_s") + ",\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Ready_o   => " + this.createVar(var, "_0_Ready_s") + ",\n";
			}
			else {
				sOp += tab(level + 1) + "D" + (i + 1) + "_i         => " + this.createVar(var, "_s") + ",\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Valid_i   => " + this.createVar(var, "_Valid_s") + ",\n";
				sOp += tab(level + 1) + "D" + (i + 1) + "_Ready_o   => " + this.createVar(var, "_Ready_s") + ",\n";
			}
		}	

		/* Mapping of selection signal in case of multiplexer operation */
		if (op instanceof Multiplexer) {
			SimpleVariable var = (SimpleVariable)((Multiplexer)op).getSel();	
			
			/* If the variable is a constant, just write the value of the constant, not the name */
			if (var.getType().equals("const")) {		
				sOp += tab(level + 1) + "D_Sel_i       => '1',\n";
				sOp += tab(level + 1) + "D_Sel_Valid_i => '1',\n";
				sOp += tab(level + 1) + "D_Sel_Ready_o => open,\n";
			}
			else {
				sOp += tab(level + 1) + "D_Sel_i       => " + this.createVar(var, "_s") + ",\n";
				sOp += tab(level + 1) + "D_Sel_Valid_i => "  + this.createVar(var, "_Valid_s") + ",\n";
				sOp += tab(level + 1) + "D_Sel_Ready_o => " + this.createVar(var, "_Ready_s") + ",\n";
			}
		}
		
		/* Mapping of result signals */
		SimpleVariable varOutput = (SimpleVariable) op.getOutputAt(0);
		sOp += tab(level + 1) + "Ready_i      => " + this.createVar(varOutput, "_Ready_s,") + "\n";
		if (op instanceof Comparison) {
			sOp += tab(level + 1) + "Smaller_o    => " + this.createVar(varOutput, "_smaller_s,") + "\n";
			sOp += tab(level + 1) + "Bigger_o     => " + this.createVar(varOutput, "_bigger_s,") + "\n";
			sOp += tab(level + 1) + "Equal_o      => " + this.createVar(varOutput, "_equal_s,") + "\n";
		} else {
			sOp += tab(level + 1) + "Result_o     => "+ this.createVar(varOutput, "_s,") + "\n";
		}		
		sOp += tab(level + 1) + "Valid_o      => " + this.createVar(varOutput, "_Valid_s") + "\n";
		sOp += tab(level) + ");\n";
		
		if (op instanceof Comparison) {
			if(op instanceof Less)
				sOp += tab(level) + this.createVar(varOutput, "_s") + " <= " + createVar(varOutput, "_smaller_s") + ";\n";
			else if(op instanceof LessEqual)
				sOp += tab(level) + this.createVar(varOutput, "_s") + " <= " + createVar(varOutput, "_smaller_s") + " or " + this.createVar(varOutput, "_equal_s") + ";\n";
			else if(op instanceof Greater)
				sOp += tab(level) + this.createVar(varOutput, "_s") + " <= " + createVar(varOutput, "_bigger_s") + ";\n";
			else if(op instanceof GreaterEqual)
				sOp += tab(level) + this.createVar(varOutput, "_s") + " <= " + createVar(varOutput, "_bigger_s") + " or " + this.createVar(varOutput, "_equal_s") + ";\n";
			else if(op instanceof Equal)
				sOp += tab(level) + this.createVar(varOutput, "_s") + " <= " + createVar(varOutput, "_equal_s")  + ";\n";		
		}
		
		if(varOutput.getInputLoopVariable(parentFunction) != null && varOutput.getLoopOutputCost(parentFunction) == 0 && varOutput.getVariableMap().get(parentFunction) == null) {
			sOp += tab(level) + varOutput.getName()  + "_Ready_s <= " + varOutput.getInputLoopVariable(parentFunction).getName() + "_Loop_Ready_s;\n";
			sOp += tab(level) + varOutput.getInputLoopVariable(parentFunction).getName()  + "_Loop_s <= " +  varOutput.getName() + "_s;\n";
			sOp += tab(level) + varOutput.getInputLoopVariable(parentFunction).getName()  + "_Loop_Valid_s <= " + varOutput.getName()  + "_Valid_s;\n";
		}
		
		return sOp;
	}

	
	/**
	 * Generate the VHDL representation of a simple variable.
	 * @param var The simple variable
	 * @param endLineChar The string to append to the VHDL representation
	 * @return The VDHL representation of a simple variable
	 */
	private String createVar(SimpleVariable var, String endLineChar) {
		String sVar = new String();
		if (var.getName().isEmpty()) {
			sVar += this.createValues(var.getValues()) + endLineChar + "--=" + var.getValues();
		} else {
			sVar += var.getName() + endLineChar;
		} 
		return sVar;
	}

	
	/**
	 * Generate the VHDL representation of a value
	 * @param val The value
	 * @return The VHDL representation of a value
	 */
	private String createValues(ArrayList<Double> val) {
		String sVal = new String();
		BinaryFloatingPoint bin = BinaryFloatingPoint.getInstance();
		
		/* Update size of exp and mant */
		if(NumType.getDataSize(numType) == 32) {
			bin.setSizeExp(8);
			bin.setSizeMant(23);
		}
		else if((NumType.getDataSize(numType) == 64)){
			bin.setSizeExp(11);
			bin.setSizeMant(52);
		}
		
		if (val.size() == 1) {
			sVal += "\"" + bin.getBinary(val.get(0)) + "\"";
		} else {
			sVal += "(";
			sVal += "\"" + bin.getBinary(val.get(0)) + "\"";
			for (int i = 1; i < val.size(); i++) {
				sVal += ",\"" + bin.getBinary(val.get(i)) + "\"";
			}
			sVal += ")";
		}
		return sVal;
	}


//	private String convertType(SimpleVariable var) {
//		String type = NumType.getVHDLType(numType);
//		if (var.getType().equals("vector")) {
//			type = "vec_array(" + var.getSize() + " downto 0)";
//		}
//		return type;
//	}

	
	/**
	 * Add a number of tabulation to a string.
	 * @param nb Number of tabulation to add
	 * @return The string who contain tabulations
	 */
	private String tab(int nb) {
		String s = new String();
		for (int i = 0; i < nb; i++) {
			s += "\t";
		}
		return s;
	}
}
