-------------------------------------------------------------------------------
--    This file is part of the Math2mat project.	
--    Copyright (C) 2011  HES-SO/ HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------
--                               Loop_For.vhd                        --
-----------------------------------------------------------------------
-- Auteur      : Trolliet Gregory                                    --
-- Date        : 15.11.2008                                          --
-- Description : Controle du composant boucle                        --
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library fplib;
use fplib.pkg_fplib.all;

entity Loop_For is
	generic(
		LEN		: integer := 4; -- Longueur du corps de la boucle en clk
		NBOCC		: integer := 1	 -- Nombre d'occurence de la boucle
	);
	port(
		clk		: in	std_logic;
		rst		: in	std_logic;
		-- Indique si les valeurs en entree sont valides
		valid		: in	std_logic;
		-- Permet de figer la boucle
		fix		: in	std_logic;
		-- Indique au corps si il faut recommencer un nouveau calcul
		start		: out std_logic;
		-- Indique aux autres operateurs si il faut se figer ou pas
		run		: out std_logic
	);
end Loop_For;

architecture comp of Loop_For is
	type tab_cpt is array (LEN-1 downto 0)
		of unsigned(9 downto 0);
	signal cpt : tab_cpt;
	signal indice : unsigned(9 downto 0);
begin
	run <= '1' when (cpt(to_integer(indice))=0) else '0';
	
	process(clk,rst)
	begin
		if (rst='1') then
			cpt <= (others => (others => '0'));
			indice <= (others => '0');
			start <= '0';
		elsif rising_edge(clk) then
			start <= '0';
			if (cpt(to_integer(indice)) /= 0) then
				cpt(to_integer(indice)) <= cpt(to_integer(indice)) + 1;
				if (cpt(to_integer(indice)) = NBOCC) then
					cpt(to_integer(indice)) <= (others => '0');
				end if;
				if (valid = '1' and
					fix = '0' and
					cpt(to_integer(indice)) = 0) then
					cpt(to_integer(indice)) <= cpt(to_integer(indice)) + 1;
					start <= '1';
				end if;
				if (fix = '0') then
					if (indice = LEN-1) then
						indice <= (others => '0');
					else
						indice <= indice + 1;
					end if;
				end if;
			end if;
		end if;
	end process;
end comp;

