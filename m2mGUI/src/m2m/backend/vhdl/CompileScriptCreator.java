/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.vhdl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;


import m2m.backend.project.ExternalToolsProperties;
import m2m.backend.project.M2MProject;
import m2m.backend.structure.Assignment;
import m2m.backend.structure.Element;
import m2m.backend.structure.Function;
import m2m.backend.structure.IfThenElse;
import m2m.backend.structure.Loop;
import m2m.backend.structure.LoopFor;
import m2m.backend.structure.Operation;
import m2m.backend.utils.FileUtils;

import m2m.backend.buildingblocks.BuildingBlock;


/** \brief Create script for QuestaSim
 * 
 * Creates the script for QuestaSim to compile VHDL and System Verilog files.
 * It also loads the testbench and launches it.
 * 
 * @author Sebastien Masle
 * @author <a href="mailto:sebastien.masle@heig-vd.ch">sebastien.masle@heig-vd.ch</a>
 *
 */
public class CompileScriptCreator {

	private Function top;
	private boolean loop;
	
	public CompileScriptCreator(Function top) {
		this.top = top;
		this.loop = false;
	}
	
	/**
	 * Constructor. Creates the file and writes a string in it.
	 * @param projectPath Path to the project directory.
	 * @param outFile The VDHL output file (produced from the m2m function).
	 * @return True if succeed. False otherwise.
	 */
	public boolean createCompileScript(M2MProject project,String projectPath, String outFile) {		
		String fileName = project.getSimPath()+"/simulation.do";
		File compileFile = new File(fileName);
		FileWriter fw;
		try {
			fw = new FileWriter(compileFile);
			String script = new String();
			script = createScript(project,projectPath, outFile);
			fw.write(script);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (VHDLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/**
	 * Create the string that contains all instructions for QuestaSim.
	 * @param projectPath Path to the project directory.
	 * @param outFile The VDHL output file (produced from the m2m function).
	 * @return The string that contains QuestaSim instructions.
	 * @throws VHDLException 
	 */
	private String createScript(M2MProject project,String projectPath, String outFile) throws VHDLException {
		String script = new String();
		
		//get the type of fifo the user wants (Standard/Altera/Xilinx)
		String fifoType = project.getOptimisationProperties().getFifoType();

	//	String libDir = System.getProperty("user.dir").replace("\\", "/") + "/lib/VHDL/";
		String libDir = M2MProject.getLibPath()+"/VHDL/";
		String vhdlDir = project.getVHDLPath()+"/";
		
//		script += "set Path \"" + projectPath.substring(0, projectPath.length() - 1) + "\"\n";
		script += "set Path \"..\"\n";
		script += "set compPath \"$Path/"+M2MProject.getCompDirName()+"\"\n";
		script += "set srcSVPath \"$Path/"+M2MProject.getSVDirName()+"\"\n";
		script += "set srcVHDLPath \"$Path/"+M2MProject.getVHDLDirName()+"\"\n";
		script += "set workPath \"$Path/"+M2MProject.getCompDirName()+"/work\"\n";
		script += "vlib $workPath\n\n";
		script += "vmap work $workPath\n\n";
		

		if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_UVM) {
			// For independent UVM
			script += "## compile uvm\n";
			script += "set uvmPath \"$Path/"+M2MProject.getCompDirName()+"/m2mUvm\"\n";
			script += "vlib $uvmPath\n";
			script += "vmap m2mUvm $uvmPath\n";
		}
		else {
			// For independent OVM
			script += "## compile ovm\n";
			script += "set ovmPath \"$Path/"+M2MProject.getCompDirName()+"/m2mOvm\"\n";
			script += "vlib $ovmPath\n";
			script += "vmap m2mOvm $ovmPath\n";
		}

		if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_UVM)
			script += "vlog -work m2mUvm +incdir+$srcSVPath/uvm/src $srcSVPath/uvm/src/uvm_pkg.sv\n\n";
		else if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_M2MOVM)
			script += "vlog -work m2mOvm +define+M2M_USE_OVM +incdir+$srcSVPath/ovm/src $srcSVPath/ovm/src/ovm_pkg.sv\n\n";
		else
			script += "vlog -work m2mOvm +define+M2M_USE_OVM +incdir+$srcSVPath/ovm_original/src $srcSVPath/ovm_original/src/ovm_pkg.sv\n\n";

		
		//copy standard VHDL files and add the line in the script to compile them
		FileUtils.copyFile(libDir + "pkg_definition.vhd", vhdlDir + "pkg_definition.vhd");
		FileUtils.copyFile(libDir + "pkg_cellule.vhd", vhdlDir + "pkg_cellule.vhd");
		FileUtils.copyFile(libDir + "misc.vhd", vhdlDir + "misc.vhd");
		FileUtils.copyFile(libDir + "fifo_std.vhd", vhdlDir + "fifo_std.vhd");
		script += "vcom -work \"$workPath\" \"$srcVHDLPath/pkg_definition.vhd\" \"$srcVHDLPath/fifo_std.vhd\" \"$srcVHDLPath/pkg_cellule.vhd\" \"$srcVHDLPath/misc.vhd\"\n";
		//compile other VHDL files
		Vector<Element> body = this.top.getBody();
		
		@SuppressWarnings("unchecked")
		Vector<Element> bodyclone = (Vector<Element>) body.clone();
		Vector<Element> operations = findOperations(bodyclone);
		//delete same elements to keep just one, it avoids to compile n-times the same file
		for (int i = 0; i < operations.size() - 1; i++) {
			for (int j = i + 1; j < operations.size(); j++) {
				if ((operations.elementAt(i) instanceof Operation) && (operations.elementAt(j) instanceof Operation) &&
						!(operations.elementAt(i) instanceof Assignment) && !(operations.elementAt(j) instanceof Assignment)) {
					Operation op1 = (Operation)operations.elementAt(i);
					Operation op2 = (Operation)operations.elementAt(j);
					if (op1.getBlock().entityName().equalsIgnoreCase(op2.getBlock().entityName())) {
						operations.remove(j);
						j--;
					}
				}
			}
		}
		
		//find each operation and add it in the compile script
		for (Element el : operations) {
			if (el instanceof Operation && !(el instanceof Assignment)) {
				Operation op = (Operation) el;
				BuildingBlock block = op.getBlock();
				script += "vcom -work \"$workPath\""; 
				//copy and add dependent files
				if (block.dependentFiles()!=null)
					for (int i = 0; i < block.dependentFiles().size(); i++) {
						FileUtils.copyFile(libDir + block.dependentFiles().get(i), vhdlDir + block.dependentFiles().get(i));
						script += " \"$srcVHDLPath/" + block.dependentFiles().get(i) + "\"";
					}
				//copy and add the operation block file
				FileUtils.copyFile(libDir + block.vhdlFileName(), vhdlDir + block.vhdlFileName());
				script += " \"$srcVHDLPath/" + block.vhdlFileName() + "\"";
				//add the wrapper file
				script += " \"$srcVHDLPath/wrapper_" + block.entityName() + ".vhd\"\n";
			}
		}
		
		Vector<Element> functions = findFunctions(bodyclone);
		for (Element el : functions) {
			if (el instanceof Loop) {
				Loop loop = (Loop) el;
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/log_pkg.vhd\"";
				script += " \"$srcVHDLPath/loop_" + loop.getName() + ".vhd\"\n"; 
				FileUtils.copyFile(libDir + "log_pkg.vhd", vhdlDir + "log_pkg.vhd");
			}
		}
		
		//copy the necessary files for fifo, if there are used
		if (project.getUseFifo()) {
			if (fifoType.equalsIgnoreCase("Altera")) {
				File alteraDir = new File(vhdlDir+"altera_mf");
				alteraDir.mkdir();
				FileUtils.copyFile(libDir + "log_pkg.vhd", vhdlDir + "log_pkg.vhd");
				FileUtils.copyFile(libDir + "fifo_Altera.vhd", vhdlDir + "fifo_Altera.vhd");
				FileUtils.copyFile(libDir + "altera_mf/altera_mf.vhd", vhdlDir + "altera_mf/altera_mf.vhd");
				FileUtils.copyFile(libDir + "altera_mf/altera_mf_components.vhd", vhdlDir + "altera_mf/altera_mf_components.vhd");
				script += "vmap altera_mf work\n";
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/log_pkg.vhd\"\n";
				script += "vcom -work altera_mf \"$srcVHDLPath/altera_mf/altera_mf_components.vhd\" \"$srcVHDLPath/altera_mf/altera_mf.vhd\"\n";
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_Altera.vhd\"\n";
			} else if (fifoType.equalsIgnoreCase("Xilinx")) {
				File xilinxDir = new File(vhdlDir+"XilinxCoreLib");
				xilinxDir.mkdir();
				FileUtils.copyFile(libDir + "XilinxCoreLib/fifo_generator_v4_3.vhd", vhdlDir + "XilinxCoreLib/fifo_generator_v4_3.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_conv.vhd", vhdlDir + "XilinxCoreLib/iputils_conv.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_misc.vhd", vhdlDir + "XilinxCoreLib/iputils_misc.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_std_logic_arith.vhd", vhdlDir + "XilinxCoreLib/iputils_std_logic_arith.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_std_logic_unsigned.vhd", vhdlDir + "XilinxCoreLib/iputils_std_logic_unsigned.vhd");
				FileUtils.copyFile(libDir + "fifo_Xilinx.vhd", vhdlDir + "fifo_Xilinx.vhd");
				script += "vmap XilinxCoreLib work\n";
				script += "vcom -work XilinxCoreLib \"$srcVHDLPath/XilinxCoreLib/iputils_conv.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_misc.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_std_logic_arith.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_std_logic_unsigned.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/fifo_generator_v4_3.vhd\"\n"; 
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_Xilinx.vhd\"\n"; 
			} else {
				FileUtils.copyFile(libDir + "fifo_std.vhd", vhdlDir + "fifo_std.vhd");
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_std.vhd\"\n"; 
			}
		}
		
		//if there is a loop for in the design
		if (loop) {
			FileUtils.copyFile(libDir + "Init_Input.vhd", vhdlDir + "Init_Input.vhd");
			FileUtils.copyFile(libDir + "Counter_Wr.vhd", vhdlDir + "Counter_Wr.vhd");
			FileUtils.copyFile(libDir + "Counter_Rd.vhd", vhdlDir + "Counter_Rd.vhd");
			FileUtils.copyFile(libDir + "Read_Result.vhd", vhdlDir + "Read_Result.vhd");
			FileUtils.copyFile(libDir + "Memory.vhd", vhdlDir + "Memory.vhd");
			script += "vcom -work \"$workPath\" \"$srcVHDLPath/Init_Input.vhd\" \"$srcVHDLPath/Counter_Rd.vhd\" \"$srcVHDLPath/Read_Result.vhd\" \"$srcVHDLPath/Memory.vhd\" \"$srcVHDLPath/Counter_Wr.vhd\"\n";
			if (fifoType.equalsIgnoreCase("Altera")) {
				File alteraDir = new File(vhdlDir+"altera_mf");
				alteraDir.mkdir();
				FileUtils.copyFile(libDir + "log_pkg.vhd", vhdlDir + "log_pkg.vhd");
				FileUtils.copyFile(libDir + "fifo_Altera.vhd", vhdlDir + "fifo_Altera.vhd");
				FileUtils.copyFile(libDir + "altera_mf/altera_mf.vhd", vhdlDir + "altera_mf/altera_mf.vhd");
				FileUtils.copyFile(libDir + "altera_mf/altera_mf_components.vhd", vhdlDir + "altera_mf/altera_mf_components.vhd");
				script += "vmap altera_mf work\n";
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/log_pkg.vhd\"\n";
				script += "vcom -work altera_mf \"$srcVHDLPath/altera_mf/altera_mf_components.vhd\" \"$srcVHDLPath/altera_mf/altera_mf.vhd\"\n";
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_Altera.vhd\"\n";
			} else if (fifoType.equalsIgnoreCase("Xilinx")) {
				File xilinxDir = new File(vhdlDir+"XilinxCoreLib");
				xilinxDir.mkdir();
				FileUtils.copyFile(libDir + "XilinxCoreLib/fifo_generator_v4_3.vhd", vhdlDir + "XilinxCoreLib/fifo_generator_v4_3.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_conv.vhd", vhdlDir + "XilinxCoreLib/iputils_conv.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_misc.vhd", vhdlDir + "XilinxCoreLib/iputils_misc.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_std_logic_arith.vhd", vhdlDir + "XilinxCoreLib/iputils_std_logic_arith.vhd");
				FileUtils.copyFile(libDir + "XilinxCoreLib/iputils_std_logic_unsigned.vhd", vhdlDir + "XilinxCoreLib/iputils_std_logic_unsigned.vhd");
				FileUtils.copyFile(libDir + "fifo_Xilinx.vhd", vhdlDir + "fifo_Xilinx.vhd");
				script += "vmap XilinxCoreLib work\n";
				script += "vcom -work XilinxCoreLib \"$srcVHDLPath/XilinxCoreLib/iputils_conv.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_misc.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_std_logic_arith.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/iputils_std_logic_unsigned.vhd\"" +
						" \"$srcVHDLPath/XilinxCoreLib/fifo_generator_v4_3.vhd\"\n"; 
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_Xilinx.vhd\"\n"; 
			} else {
				FileUtils.copyFile(libDir + "fifo_std.vhd", vhdlDir + "fifo_std.vhd");
				script += "vcom -work \"$workPath\" \"$srcVHDLPath/fifo_std.vhd\"\n";
			} 
		}
		
		//script += "vcom -work \"$workPath\" \"$Math2Mat_srcVHDL/fifo_std.vhd\"\n"; 
				
		script += "vcom -work \"$workPath\" \"$srcVHDLPath/" + this.top.getName()+".vhd\"\n\n";
//		script += "vcom -work \"$workPath\" \"" + outFile.replace("\\", "/") + "\"\n\n";
		//compile System verilog files
//		script += "vlog -work \"$workPath\" \"$srcSVPath/M2M_harness.sv\" \"$srcSVPath/M2M_tb.sv\"\n";

		
		if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_UVM) {
			script += "vlog -work \"$workPath\" +incdir+$srcSVPath -L m2mUvm \"$srcSVPath/M2M_harness.sv\" \"$srcSVPath/M2M_tb.sv\"\n";
			//launch simulation
			script += "vsim -vopt  +incdir+\"$srcSVPath\" -L m2mUvm work.M2M_TB work.M2M_harness\n";
		}
		else {
			script += "vlog -work \"$workPath\" +incdir+$srcSVPath +define+M2M_USE_OVM -L m2mOvm \"$srcSVPath/M2M_harness.sv\" \"$srcSVPath/M2M_tb.sv\"\n";
			//launch simulation
			script += "vsim -vopt  +incdir+\"$srcSVPath\" -L m2mOvm work.M2M_TB work.M2M_harness\n";
		}
		
		script += "run -all";
		return script;
	}
	
	private Vector<Element> findOperations(Vector<Element> elements) throws VHDLException {
		Vector<Element> operations = new Vector<Element>();
		
		for (Element el : elements) {
			if (el instanceof Function) {
				if (el instanceof IfThenElse) {
					operations.addAll(findOperations(((IfThenElse)el).getBodyFalse()));
					operations.addAll(findOperations(((IfThenElse)el).getBodyTrue()));
					operations.addAll(findOperations(((IfThenElse)el).getCond()));
				} else if (el instanceof Loop) {
					operations.addAll(findOperations(((Loop)el).getBody()));
					if (el instanceof LoopFor)
						operations.add(((LoopFor)el).getIterOperation());
					loop = true;
				}
			} else if (el instanceof Operation && !(el instanceof Assignment)) {
				Operation op = (Operation) el;
				BuildingBlock blockOp = op.getBlock();
				if (blockOp == null) {
					throw new VHDLException("No material description for the operation : " +
							  op.getName());
				} else {
					operations.add(op);
					for (int j = 0; j < blockOp.nbInputs(); j++) {
						if (op.getInputAt(j) instanceof Operation) {
							Vector<Element> item = new Vector<Element>();
							item.add(op.getInputAt(j));
							operations.addAll(findOperations(item));
						}
					}
				}
			}
		}
		return operations;
	}
	
	
	private Vector<Element> findFunctions(Vector<Element> elements) throws VHDLException {
		Vector<Element> functions = new Vector<Element>();
		
		for (Element el : elements) {
			if (el instanceof Function) {
				functions.add(el);
				if (el instanceof IfThenElse) {
					functions.addAll(findFunctions(((IfThenElse)el).getBodyFalse()));
					functions.addAll(findFunctions(((IfThenElse)el).getBodyTrue()));
					functions.addAll(findFunctions(((IfThenElse)el).getCond()));
				} else if (el instanceof Loop) {
					functions.addAll(findFunctions(((Loop)el).getBody()));
				}
			} else if (el instanceof Operation && !(el instanceof Assignment)) {
				Operation op = (Operation) el;
				for (int j = 0; j < op.getInput().size(); j++) {
					if (op.getInputAt(j) instanceof Operation) {
						Vector<Element> item = new Vector<Element>();
						item.add(op.getInputAt(j));
						functions.addAll(findFunctions(item));
					}
				}
			}
		}
		return functions;
	}
	
}
