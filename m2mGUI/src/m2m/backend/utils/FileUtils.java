/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file CopyFile.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Nov 23, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.backend.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtils {

	public static String readFileAsString(String filePath){
		byte[] buffer = new byte[(int) new File(filePath).length()];
	    BufferedInputStream f = null;
	    try {
	        f = new BufferedInputStream(new FileInputStream(filePath));
	        f.read(buffer);
	    }
	    catch (FileNotFoundException e) {
	    	return new String("");
	    }
	    catch (IOException e) {
	    	return new String("");
	    }
	    finally {
	        if (f != null) try { f.close(); } catch (IOException ignored) { }
	    }
	    return new String(buffer);
	}

	public static String getStringMd5(String buf) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		    messageDigest.reset();
		    messageDigest.update(buf.getBytes());
		    final byte[] digest = messageDigest.digest();
	
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			return hashtext;
		}
		catch (NoSuchAlgorithmException e) {
			return "";
		}
	}
	
	public static String getMd5(String fileName) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			InputStream is = new FileInputStream(fileName);
			try {
			  is = new DigestInputStream(is, md);
			  byte[] b=new byte[is.available()];
			  is.read(b,0,is.available());
			  // read stream to EOF as normal...
			}
			finally {
			  is.close();
			}
			byte[] digest = md.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			return hashtext;
		}
		catch (NoSuchAlgorithmException e) {
			return "";
		}
		catch (FileNotFoundException e) {
			return "";
		}
		catch (IOException e) {
			return "";
		}
	}
	
	public String getFileExtension(String fileName) {
		//get the file extension
		String ext = (fileName.lastIndexOf(".") == -1 ? "" :
		fileName.substring(fileName.lastIndexOf(".")+1, fileName.length()));
		return ext;
	}

	/**
	 * Delete the content of a directory. Can also keep a file in this directory.
	 * @param file the directory to free
	 * @param fileToKeep the file needed to be kept
	 * @return true if success, false otherwise
	 */
	public static boolean deleteDirContent (File file, String fileToKeep) {
		File[] files = file.listFiles();
		
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().equalsIgnoreCase(fileToKeep))
				continue;
			if (files[i].isDirectory()) {
				deleteDirContent(files[i], fileToKeep);
				files[i].delete();
			} else {
				if (!files[i].delete())
					return false;
			}
		}
		
		return true;
	}

	public static boolean compareDataFiles(File origFile, File regenFile) {
		boolean identical = true;
		
		try {
			FileReader frOrig = new FileReader(origFile);
			FileReader frRegen = new FileReader(regenFile);
			
			int charRead;
			while((charRead = frOrig.read()) != -1) {
				if (charRead != frRegen.read()) {
					identical = false;
					break;
				}
			}
			if (charRead != frRegen.read())
				identical = false;
			
			frOrig.close();
			frRegen.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return identical;
	}
	
	public static boolean copyFile(String orig, String copy) {
		return copyFile(orig,copy,"");
	}

	public static boolean copyFile(String orig,String copy,String ignore) {
		File input = new File(orig);
		File output = new File(copy);
		
		if (input.isDirectory()) {
			output.mkdir();
			File[] fileList = input.listFiles();
			for (int i = 0; i < fileList.length; i++) {
				String name=fileList[i].getName();
				if (!name.equalsIgnoreCase(ignore))
					if (!copyFile(fileList[i].getAbsolutePath(), output.getAbsolutePath() +  "/" + name,ignore))
						return false;
			}
			return true;
		} else {
			int c;
			try {
				FileReader in = new FileReader(input);
				FileWriter out = new FileWriter(output);
				
				while((c = in.read()) != -1) {
					out.write((char)c);
				}
				in.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
	}
	
	/*


	private boolean copyFile(String orig, String copy) {
		File input = new File(orig);
		File output = new File(copy);
		
		if (!input.exists()) {
			System.err.println("File not found : " + input.getAbsolutePath());
			return false;
		}
		
		int c;
		try {
			FileReader in = new FileReader(input);
			FileWriter out = new FileWriter(output);
			
			while((c = in.read()) != -1) {
				out.write((char)c);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean copyFile(String orig, String copy) {
		File input = new File(orig);
		File output = new File(copy);
		
		int c;
		try {
			FileReader in = new FileReader(input);
			FileWriter out = new FileWriter(output);
			
			while((c = in.read()) != -1) {
				out.write((char)c);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void copyFile(String srFile, String dtFile) {
		try {
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);

			//For Append the file.
			//      OutputStream out = new FileOutputStream(f2,true);

			//For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			System.out.println("File copied.");
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	*/
}
