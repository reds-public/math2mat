/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file XMLUtils.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Nov 25, 2010
 *
 * Author: Yann Thoma
 *
 * Description:
 * 
 */
package m2m.backend.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * @author ythoma
 *
 */
public class XMLUtils {
	/**
	 * I take a xml element and the tag name, look for the tag and get
	 * the text content
	 * i.e for <employee><name>John</name></employee> xml snippet if
	 * the Element points to employee node and tagName is 'name' I will return John
	 */
	static public String getTextValue(Element ele, String tagName,String defaultValue) {
		String textVal = defaultValue;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			if (el!=null)
				if (el.getFirstChild()!=null)
					textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	
	static public boolean getBoolValue(Element ele, String tagName,boolean defaultValue) {
		String textVal = "";
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			if (el!=null)
				if (el.getFirstChild()!=null) {
					textVal = el.getFirstChild().getNodeValue();

					if (textVal.equalsIgnoreCase("true"))
						return true;
					else
						return false;
				}
		}
		return defaultValue;
	}


	/**
	 * Calls getTextValue and returns a int value
	 */
	static public int getIntValue(Element ele, String tagName,int defaultValue) {
		//in production application you would catch the exception
		try {
			return Integer.parseInt(getTextValue(ele,tagName,""));
		}
		catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Calls getTextValue and returns a float value
	 */
	static public float getFloatValue(Element ele, String tagName,float defaultValue) {
		//in production application you would catch the exception
		try {
			return Float.parseFloat(getTextValue(ele,tagName,""));
		}
		catch (NumberFormatException e) {
			return defaultValue;
		}
	}


	/**
	 * Calls getTextValue and returns a double value
	 */
	static public double getDoubleValue(Element ele, String tagName,double defaultValue) {
		//in production application you would catch the exception
		try {
			return Double.parseDouble(getTextValue(ele,tagName,""));
		}
		catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	static public Element createTextElement(Document dom,String tagName,String value) {
		Text text;
		if (tagName==null)
			System.out.println("Error in XML creation. Empty tagname\n");
		else if (tagName.isEmpty())
			System.out.println("Error in XML creation. Empty tagname\n");
		Element El = dom.createElement(tagName);
		if (value==null)
			text=dom.createTextNode("");
		else
			text=dom.createTextNode(value);
		El.appendChild(text);
		return El;
	}

	static public Element createIntElement(Document dom,String tagName,int value) {
		if (tagName==null)
			System.out.println("Error in XML creation. Empty tagname\n");
		if (tagName.isEmpty())
			System.out.println("Error in XML creation. Empty tagname\n");
		Element El = dom.createElement(tagName);
		String s="";
		s += value;
		Text text=dom.createTextNode(s);
		El.appendChild(text);
		return El;
	}

	static public Element createFloatElement(Document dom,String tagName,float value) {
		if (tagName==null)
			System.out.println("Error in XML creation. Empty tagname\n");
		if (tagName.isEmpty())
			System.out.println("Error in XML creation. Empty tagname\n");
		Element El = dom.createElement(tagName);
		String s="";
		s += value;
		Text text=dom.createTextNode(s);
		El.appendChild(text);
		return El;
	}

	static public Element createDoubleElement(Document dom,String tagName,double value) {
		if (tagName==null)
			System.out.println("Error in XML creation. Empty tagname\n");
		if (tagName.isEmpty())
			System.out.println("Error in XML creation. Empty tagname\n");
		Element El = dom.createElement(tagName);
		String s="";
		s += value;
		Text text=dom.createTextNode(s);
		El.appendChild(text);
		return El;
	}


	static public Element createBoolElement(Document dom,String tagName,boolean value) {
		if (tagName==null)
			System.out.println("Error in XML creation. Empty tagname\n");
		if (tagName.isEmpty())
			System.out.println("Error in XML creation. Empty tagname\n");
		Element El = dom.createElement(tagName);

		String sValue;
		if (value)
			sValue="true";
		else
			sValue="false";
		Text text=dom.createTextNode(sValue);
		El.appendChild(text);
		return El;
	}
}