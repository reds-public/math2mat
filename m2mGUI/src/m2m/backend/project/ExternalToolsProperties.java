/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Configuration of external tools.
 *
 * Project:  Math2Mat
 *
 * @file ExternalToolsProperties.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 11.11.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Provides methods to configure external tools paths.
 * 
 */

package m2m.backend.project;

import java.util.prefs.*;

public class ExternalToolsProperties {
	
	private String octavePath;
	private String vsimPath;
	private String isePath;
	private String dotPath;
	private int simulationFiles;
	private int simulator;
	
	private String ubiduleIP;
	private String xilinxLicense;
	
	private String libPath;
	
	enum SimulationFilesType {OVM,M2MOVM};
	public final static int SIMULATIONFILES_OVM=0;
	public final static int SIMULATIONFILES_M2MOVM=1;
	public final static int SIMULATIONFILES_UVM=2;
	
	public final static String simulatorTypes[]={"Questa_older","Questa_v6.5","Questa_v6.6","Questa_younger"};
	
	static private ExternalToolsProperties externalToolsProperties;
	
	static public ExternalToolsProperties getReference() {
		if (externalToolsProperties==null)
			externalToolsProperties=new ExternalToolsProperties();
		return externalToolsProperties;
	}
	//constructors
	/**
	 * Default constructors, initialise members to default names.
	 */
	private ExternalToolsProperties() {
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);

		vsimPath=prefs.get("VsimPath","vsim");
		octavePath=prefs.get("OctavePath","octave");
		isePath=prefs.get("IsePath","ise");
		dotPath=prefs.get("DotPath","dot");
		simulationFiles=prefs.getInt("SimulationFiles",1);
		simulator=prefs.getInt("Simulator", 2);
		ubiduleIP=prefs.get("UbiduleIP","ip");
		xilinxLicense=prefs.get("XilinxLicense","licence");
		libPath=prefs.get("LibPath","lib");
	}
	
/*	private static String applicationPath=null;


	public String getApplicationPath() {
		if (applicationPath==null) {
			applicationPath=this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		}
		System.out.println("Application path: "+applicationPath);
		return applicationPath;
	}
*/
	
	protected void finalize() {
		
	}
	
	public String getLibPath() {
		return libPath;
	}
	
	public String getXilinxLicense() {
		return xilinxLicense;
	}
	
	public String getUbiduleIP() {
		return ubiduleIP;
	}

	public int getSimulationFiles() {
		return simulationFiles;
	}
	
	public int getSimulator() {
		return simulator;
	}

	/**
	 * Set Octave path.
	 * 
	 * @param path String that contains Octave path.
	 */
	public void setSimulationFiles(int type) {
		simulationFiles=type;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("SimulationFiles",String.valueOf(simulationFiles));
	}
	
	/**
	 * Get Octave path.
	 * 
	 * @return A string with Octave path.
	 */
	public String getOctavePath() {
		if ((octavePath==null) || (octavePath.isEmpty()))
			return "octave";
		else
			return octavePath;
	}

	
	/**
	 * Get "vsim" path.
	 * 
	 * @return A string with "vsim" path.
	 */
	public String getVsimPath() {
		if ((vsimPath==null) || (vsimPath.isEmpty()))
			return "vsim";
		else
			return vsimPath;
	}
	

	/**
	 * Get Xilinx ISE path.
	 * 
	 * @return A string with ISE Xilinx path.
	 */
	public String getIsePath() {
		if ((isePath==null) || (isePath.isEmpty()))
			return "ise";
		else
			return isePath;
	}

	/**
	 * Get "dot" path.
	 * 
	 * @return A string with "dot" path.
	 */
	public String getDotPath() {
		if ((dotPath==null) || (dotPath.isEmpty()))
			return "dot";
		else
			return dotPath;
	}


	public void setSimulator(int sim) {
		simulator=sim;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("Simulator",String.valueOf(simulator));
	}
	

	
	/**
	 * Set the library path.
	 * 
	 * @param path String that contains the libraries.
	 */
	public void setLibPath(String path) {
		libPath = path;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("LibPath",libPath);
	}
	

	/**
	 * Set Octave path.
	 * 
	 * @param path String that contains Octave path.
	 */
	public void setOctavePath(String path) {
		octavePath = path;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("OctavePath",octavePath);
	}
	
	/**
	 * set "vsim" path.
	 * 
	 * @param path String that contains "vsim" path.
	 */
	public void setVsimPath(String path) {
		vsimPath = path;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("VsimPath",vsimPath);
	}
	

	/**
	 * Set Xilinx ISE path.
	 * 
	 * @param path String that contains Xilinx ISE path.
	 */
	public void setIsePath(String path) {
		isePath = path;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("IsePath",isePath);
	}

	/**
	 * Set dot path.
	 * 
	 * @param path String that contains dot path.
	 */
	public void setDotPath(String path) {
		dotPath = path;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("DotPath",dotPath);
	}
	
	public void setXilinxLicense(String license) {
		xilinxLicense = license;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("XilinxLicense",xilinxLicense);
	}
	
	public void setUbiduleIP(String ip) {
		ubiduleIP = ip;
		final Preferences prefs = Preferences.userNodeForPackage(ExternalToolsProperties.class);
		prefs.put("UbiduleIP",ubiduleIP);
	}
}
