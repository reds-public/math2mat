/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Description class of a Math2Mat command line application.
 *
 * Project:  Math2Mat
 *
 * @file M2MProject.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 14.01.2011
 *
 * Author: Yann Thoma
 *
 * Description: This class provides a main to be used in command line mode.
 *              It can do auto-test of a directory containing .m2m files.
 *
 */
package m2m.backend.project;

import m2m.backend.buildingblocks.BuildingBlocksManager;
import m2m.backend.octaveparser.ParsingException;
import m2m.backend.processing.ExternalRuns;
import m2m.backend.processing.ProgressMonitoring;
import m2m.backend.processing.ProjectsValidator;

public class M2MCmd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String projectFileName="";
		String testDirName="";
		String libPath="";
		// TODO Auto-generated method stub
		for(int i=0;i<args.length;i++) {
			if (args[i].compareTo("-testdir")==0) {
				if (i+1<args.length)
					testDirName=args[i+1];
			}
			if (args[i].compareTo("-lib")==0) {
				if (i+1<args.length)
					libPath=args[i+1];
			}
			if (args[i].compareTo("-project")==0) {
				if (i+1<args.length)
					projectFileName=args[i+1];
			}

			if (args[i].compareTo("-vsim")==0) {
				if (i+1<args.length)
					ExternalToolsProperties.getReference().setVsimPath(args[i+1]);
			}

			if (args[i].compareTo("-octave")==0) {
				if (i+1<args.length)
					ExternalToolsProperties.getReference().setOctavePath(args[i+1]);
			}

			if (args[i].compareTo("-ise")==0) {
				if (i+1<args.length)
					ExternalToolsProperties.getReference().setIsePath(args[i+1]);
			}
			
			
			
		}

//		if (!libPath.isEmpty())
//			M2MProject.setLibPath(libPath);
		if (!libPath.isEmpty())
			ExternalToolsProperties.getReference().setLibPath(libPath);
		String path = M2MProject.getLibPath()+"/blocks";
		BuildingBlocksManager.getInstance().setDirName(path);
		BuildingBlocksManager.getInstance().setPackageName("m2m.backend.buildingblocks.blocks");
		BuildingBlocksManager.getInstance().loadAll();
		
		if (!testDirName.isEmpty()) {
			System.out.println("Test dir");
			ProgressMonitoring monitor=new ProgressMonitoring();
			ProjectsValidator validator=new ProjectsValidator();
			validator.testDir(testDirName,monitor);
			return;
		}
		
		if (!projectFileName.isEmpty()) {
			M2MProject project=new M2MProject();
			project.openProject(projectFileName);
			try{
				project.getStructTreatment().parse(project.getSourceFile());
				project.getStructTreatment().modifyNumType(project.getProperties().getOptimisationProperties().getOptimisationDataType());
				ExternalRuns.runAll(project);
			}	
			catch (ParsingException e1) {
			}
		}
		
		
	}

}
