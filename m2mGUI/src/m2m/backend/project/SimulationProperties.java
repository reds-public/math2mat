/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Properties for simulation.
 *
 * Project:  Math2Mat
 *
 * @file SimulationProperties.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Provides method to get and set properties for simulation.
 * 
 */

package m2m.backend.project;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;

import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class SimulationProperties {

	/**
	 * This private class represent some simulation parameters for inputs
	 * of a function.
	 */
	private class InputSimParam 
	{		
		/**
		 * Minimum value 
		 */
		protected int minValue;
		/**
		 * Maximum value
		 */
		protected int maxValue;	
		
		/**
		 * Constructor of an InputSimParam	
		 * @param minValue default minimum value to set
		 * @param maxValue default maximum value to set
		 */
		public InputSimParam(int minValue, int maxValue) {
			this.minValue = minValue;
			this.maxValue = maxValue;
		}
	}
	
	/**
	 * Specify the precision of calculation.
	 */
	private int simulationCalcPrecision;
	/**
	 * Number of samples to generate.
	 */
	private int nbSamples;
	/**
	 * Specify the frequency of the system.
	 */
	private float systemFrequency;
	/**
	 * Specify the frequency of stimuli.
	 */
	private float inputFrequency;
	/**
	 * specify the frequency of reference signals.
	 */
	private float outputFrequency;
	/**
	 * Specify the minimum and maximum value to generate for each input of the function.
	 */	
	private HashMap<String, InputSimParam> inputsSimParam;


	/**
	 * Specify the inactivity timeout in clock cycles.
	 * If this number of clock cycles show no activity in the 
	 * design output, then the simulation is stopped.
	 */
	private int inactivityTimeout;
	
	//constructors
	/**
	 * Initialize members.
	 */
	public SimulationProperties() {
		simulationCalcPrecision = 0;
		nbSamples = 5000;
		systemFrequency = (float)100.0;
		inputFrequency = (float)100.0;
		outputFrequency = (float)100.0;
		inactivityTimeout = 10000;
		inputsSimParam = new HashMap<String, InputSimParam>();
	}
	
	/**
	 * Initialize members with values in parameter.
	 * 
	 * @param pres Calculation precision.
	 * @param systFreq System frequency.
	 * @param inputFreq Input frequency.
	 * @param outputFreq Output frequency.
	 */
/*	public SimulationProperties(int pres, int samples, float systFreq, float inputFreq, float outputFreq) {
		simulationCalcPrecision = pres;
		nbSamples = samples;
		inputFrequency = inputFreq;
		outputFrequency = outputFreq;
	}
	*/

	public String getXmlTagName() {
		return "Simulation";
	}

	public int getInactivityTimeout() {
		return inactivityTimeout;
	}

	public void setInactivityTimeout(int timeout) {
		inactivityTimeout=timeout;
	}
	
	/**
	 * Initialize all simulation parameters from the xml file
	 * @param el the xml element to add parameters
	 * @return success of the initialization
	 */
	public boolean fromXml(Element el) {
		this.setSimulationCalcPrecision(XMLUtils.getIntValue(el,"Precision",0));
		this.setNumberOfSamples(XMLUtils.getIntValue(el,"NbSamples",5000));
		this.setInputFrequency(XMLUtils.getFloatValue(el,"InputFrequency",(float)100.0));
		this.setOutputFrequency(XMLUtils.getFloatValue(el,"OutputFrequency",(float)100.0));
		this.setSystemFrequency(XMLUtils.getFloatValue(el,"SystemFrequency",(float)100.0));
		this.setInactivityTimeout(XMLUtils.getIntValue(el,"InactivityTimeout",10000));	
		
		/* Create all input simulation parameter from xml */		
		String name;
		int minValue, maxValue;
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Input")) {
				org.w3c.dom.Element inputEl = (org.w3c.dom.Element) node;
				name = XMLUtils.getTextValue(inputEl, "Name", "");
				minValue = XMLUtils.getIntValue(inputEl, "Min", 0);
				maxValue = XMLUtils.getIntValue(inputEl, "Max", 100);
				inputsSimParam.put(name, new InputSimParam(minValue, maxValue));
			}
			node=node.getNextSibling();
		}
			
		return true;
	}

	/**
	 * Convert all simulation parameters to xml
	 * @param dom the document containing the xml representation
	 * @return xml element representing all simulation parameters
	 */
	public Element toXml(Document dom) {
		Element el;
		el=dom.createElement(getXmlTagName());
		el.appendChild(XMLUtils.createIntElement(dom,"Precision",this.getSimulationCalcPrecision()));
		el.appendChild(XMLUtils.createIntElement(dom,"NbSamples",this.getNumberOfSamples()));
		el.appendChild(XMLUtils.createFloatElement(dom,"InputFrequency",this.getInputFrequency()));
		el.appendChild(XMLUtils.createFloatElement(dom,"OutputFrequency",this.getOutputFrequency()));
		el.appendChild(XMLUtils.createFloatElement(dom,"SystemFrequency",this.getSystemFrequency()));
		
		/* Create all xml input simulation parameter from structure */
		org.w3c.dom.Element inputsimParamEl;
		for(Entry<String, InputSimParam> entry : inputsSimParam.entrySet()) {
			String name = entry.getKey();
			InputSimParam valeur = entry.getValue();
			inputsimParamEl = dom.createElement("Input");
			inputsimParamEl.appendChild(XMLUtils.createTextElement(dom,"Name",name));
			inputsimParamEl.appendChild(XMLUtils.createIntElement(dom, "Min",valeur.minValue));
			inputsimParamEl.appendChild(XMLUtils.createIntElement(dom, "Max",valeur.maxValue));
			el.appendChild(inputsimParamEl);
		}
		
		return el;
	}
	
	
	
	//Getters
	/**
	 * Get the calculation precision.
	 * 
	 * @return Calculation precision.
	 */
	public int getSimulationCalcPrecision() {
		return simulationCalcPrecision;
	}
	/**
	 * Get the number of samples to generate.
	 * @return Number of samples to generate.
	 */
	public int getNumberOfSamples() {
		return nbSamples;
	}
	/**
	 * Get the system frequency.
	 * 
	 * @return System frequency.
	 */
	public float getSystemFrequency() {
		return systemFrequency;
	}
	/**
	 * Get the stimuli frequency.
	 * 
	 * @return Stimuli frequency.
	 */
	public float getInputFrequency() {
		return inputFrequency;
	}
	/**
	 * Get the reference signals frequency.
	 * 
	 * @return Reference signals frequency.
	 */
	public float getOutputFrequency() {
		return outputFrequency;
	}
	/**
	 * Get the minimum value to generate for a particular input
	 * @param the string representation of the input 
	 * 
	 * @return The minimum value to generate for a particular input.
	 */
	public int getInputSimParamMin(String input) {
		if(inputsSimParam.get(input) != null)
			return inputsSimParam.get(input).minValue;
		return 0;
	}	
	/**
	 * Get the maximum value to generate for a particular input
	 * @param the string representation of the input 
	 * 
	 * @return The maximum value to generate for a particular input.
	 */
	public int getInputSimParamMax(String input) {
		if(inputsSimParam.get(input) != null)
			return inputsSimParam.get(input).maxValue;
		return 100;
	}	
	
	//Setters
	/**
	 * Set the calculation precision.
	 * 
	 * @param precision Calculation precision.
	 */
	public void setSimulationCalcPrecision(int precision) {
		simulationCalcPrecision = precision;
	}
	/**
	 * Set the number of samples to generate.
	 * @param samples The number of samples to generate.
	 */
	public void setNumberOfSamples(int samples) {
		nbSamples = samples;
	}
	/**
	 * Set the system frequency.
	 * 
	 * @param freq System frequency.
	 */
	public void setSystemFrequency(float freq) {
		systemFrequency = freq;
	}
	/**
	 * Set the stimuli frequency.
	 * 
	 * @param freq Stimuli frequency.
	 */
	public void setInputFrequency(float freq) {
		inputFrequency = freq;
	}
	/**
	 * Set the reference signals frequency.
	 * 
	 * @param freq reference signals frequency.
	 */
	public void setOutputFrequency(float freq) {
		outputFrequency = freq;
	}
	/**
	 * Set the minimum and maximum value to generate for a particular input
	 * @param the string representation of the input 
	 * @param the maximum value to generate for a particular input
	 */
	public void setInputSimParamMax(String input, int maxValue) {
		if(inputsSimParam.get(input) != null)
			inputsSimParam.get(input).maxValue = maxValue;
	}	
	/**
	 * Set the minimum and maximum value to generate for a particular input
	 * @param the string representation of the input 
	 * @param the minimum value to generate for a particular input
	 */
	public void setInputSimParamMin(String input, int minValue) {
		if(inputsSimParam.get(input) != null)
			inputsSimParam.get(input).minValue = minValue;
	}	
	
	/**
	 * Initialize the initInputsSimParam map with top function inputs
	 * @param functionInputs inputs of the top function
	 */
	public void initInputsSimParam(Vector<m2m.backend.structure.Element> functionInputs) {
		
		if(inputsSimParam.size() != functionInputs.size()) {
			inputsSimParam.clear();
			for(m2m.backend.structure.Element el : functionInputs)
				inputsSimParam.put(el.getName(), new InputSimParam(0, 100));
		}
	}
}