/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ProjectRef.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Nov 23, 2010
 *
 * Author: Yann Thoma
 *
 * Description: This is a super class for all classes needing to 
 *              reference a M2MProject
 * 
 */
package m2m.backend.project;


/**
 * @author ythoma
 *
 */
public class ProjectRef {

	protected M2MProject project;
	
	/**
	 * Getter of the reference project
	 * @return Reference project
	 */
	public M2MProject getProject() {
		return this.project;
	}
	
	/**
	 * Setter of the reference project
	 * @param editor Reference project
	 */
	public void setProject (M2MProject project) {
		this.project = project;
	}
}
