/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file SimulationResult.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Feb 28, 2011
 *
 * Author: Yann Thoma
 *
 * Description: The class SimulationResult embeds all the useful information
 *              retrieved from a simulation, such as the latency,
 *              total time, ...
 * 
 */
package m2m.backend.project;

/**
 * @author Yann Thoma
 * This class allows to store useful information during the simulation. It is 
 * exploited by ExternalRuns. It is stored in the M2MProject, as well as used
 * to display a simulation report.
 *
 */
public class SimulationResult {

	// Latency of the calculus
	private int latency;
	
	// Number of samples supplied to the simulation
	private int nbInputs;
	
	// Total execution time of the simulation
	private int TotalTime;
	
	// Prefix used to send info/warning/error/fatal messages from simulation
	private String prefix;

	// Number of simulation errors
	private int nbErrors;
	
	private int maxBitError;
	
	/**
	 * @return the maxBitError
	 */
	public int getMaxBitError() {
		return maxBitError;
	}

	/**
	 * @param maxBitError the maxBitError to set
	 */
	public void setMaxBitError(int maxBitError) {
		this.maxBitError = maxBitError;
	}

	/**
	 * @return the number of errors observed during simulation
	 */
	public int getNbErrors() {
		return nbErrors;
	}

	/**
	 * @param nbErrors the number of errors observed during simulation
	 */
	public void setNbErrors(int nbErrors) {
		this.nbErrors = nbErrors;
	}

	/**
	 * @return the prefix used to send info/warning/error/fatal messages from simulation
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the latency of calculus observed during simulation, in clock cycles
	 */
	public int getLatency() {
		return latency;
	}

	/**
	 * @param latency the latency observed during simulation, in clock cycles
	 */
	public void setLatency(int latency) {
		this.latency = latency;
	}

	/**
	 * @return the number of samples supplied to the system
	 */
	public int getNbInputs() {
		return nbInputs;
	}

	/**
	 * @param nbInputs the number of samples supplied to the system
	 */
	public void setNbInputs(int nbInputs) {
		this.nbInputs = nbInputs;
	}

	/**
	 * @return the total execution time, in clock cycles
	 */
	public int getTotalTime() {
		return TotalTime;
	}

	/**
	 * @param totalTime the total execution time, in clock cycles
	 */
	public void setTotalTime(int totalTime) {
		TotalTime = totalTime;
	}

}
