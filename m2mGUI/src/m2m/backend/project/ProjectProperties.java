/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Math2Mat project properties.
 *
 * Project:  Math2Mat
 *
 * @file Properties.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Provides methods to get and set all project properties.
 * 
 */

package m2m.backend.project;

import java.io.File;

public class ProjectProperties {
	
	/**
	 * Project file.
	 */
	private File file;
	/**
	 * Project path
	 */
	private String path;
	
	/**
	 * Relative path to the source file (relative to the project path).
	 */
	private String relativeSourceFileName;
	
	/**
	 * Optimisation properties.
	 */
	private OptimisationProperties optimisationProperties;
	/**
	 * Simulation properties.
	 */
	private SimulationProperties simulationProperties;
	
	
	public void setProjectFile(File file) {
		this.file = file;
		this.path = file.getAbsolutePath().replace("\\", "/").substring(0, file.getAbsolutePath().replace("\\", "/").lastIndexOf("/") + 1);
	}
	
	public File getProjectFile() {
		return file;
	}
	
	public String getProjectPath() {
		return path;
	}
	
	public String getFunctionName() {
		return file.getName().substring(0, file.getName().length()-4);
	}
	
	

	/**
	 * Set the project path and updates the relative source file path.
	 * @param file Source file of the project.
	 */
	/*
	public void setPath(File file) {
		String sourceFile = new String();
		try { 
			sourceFile= this.getSourceFile().getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.path = file.getAbsolutePath().replace("\\", "/").substring(0, file.getAbsolutePath().replace("\\", "/").lastIndexOf("/") + 1);
		setSourceFile(new File(sourceFile));
	}
	*/

	/**
	 * Set the project path and updates the relative source file path.
	 * @param path Project path.
	 */
	/*
	public void setPath(String path) {
		String sourceFile = new String();
		try { 
			sourceFile= this.getSourceFile().getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.path = path;
		setSourceFile(new File(sourceFile));
	}
	*/
	

	
	/**
	 * Set the source file of the project.
	 * 
	 * @param file Source file.
	 */
	public void setSourceFile(File file) {
		setSourceFilePath(getRelativePath(file.getAbsolutePath().replace("\\", "/")));
	}
	
	/**
	 * Get the source file path relative to the project file path.
	 * @param absolutePath Absolute path of the source file.
	 * @return The source file path relative to the project file path.
	 */
	private String getRelativePath(String absolutePath) {
		String relativePath = new String();
		//String commonPath = new String();
		String subPath = new String();
		int index = 0;
		int subFolders = 0;
		
		for (int i = 0; i < path.length(); i++) {
			if (path.charAt(i) == absolutePath.charAt(i)) {
				index++;
				//commonPath += path.charAt(i);
			}
			else
				break;
		}

		subPath = path.substring(index);
		
		//count the number of sub-folders we have to go back
		while (subPath.indexOf("/") != -1) {
			subFolders++;
			subPath = subPath.substring(subPath.indexOf("/")+1);
		}
		
		for (int i = 0; i < subFolders; i++) {
			relativePath += "../";
		}
		
		subPath = absolutePath.substring(index);
		relativePath += subPath;
		
		return relativePath;
	}
	
	/**
	 * Constructor with inputFunction.
	 * @param functionInputs inputs of the top function
	 */
	public ProjectProperties() {
		relativeSourceFileName = null;
		optimisationProperties = new OptimisationProperties();
		simulationProperties = new SimulationProperties();
	}
	
	/**
	 * Constructor that initialise members with parameters.
	 * 
	 * @param sourceFilePath Path to the source file.
	 * @param optimProp Optimisation properties.
	 * @param simProp Simulation properties.
	 */
	public ProjectProperties(String sourceFilePath, OptimisationProperties optimProp, SimulationProperties simProp) {
		this.relativeSourceFileName = sourceFilePath;
		optimisationProperties = optimProp;
		simulationProperties = simProp;
	}
	
	//Getters
	/**
	 * Get optimisation properties.
	 * 
	 * @return Optimisation properties.
	 */
	public OptimisationProperties getOptimisationProperties() {
		return optimisationProperties;
	}
	/**
	 * Get simulation properties.
	 * 
	 * @return Simulation properties.
	 */
	public SimulationProperties getSimulationProperties() {
		return simulationProperties;
	}
	/**
	 * Get the relative source file path (relative to the project).
	 * @return Source relative file path.
	 */
	public String getRelativeSourceFilename() {
		return relativeSourceFileName;
	}

	/**
	 * Get the source absolute file path.
	 * @return Source absolute file path.
	 */
	public String getAbsoluteSourceFilename() {
		return path+relativeSourceFileName;
	}
	
	public String getProjectShortFilename() {
		return file.getName();
	}
	
	public String getProjectFullFilename() {
		return file.getAbsolutePath();
	}
	
	//Setters
	/**
	 * Set optimisation properties.
	 * 
	 * @param optimProp Optimisation properties.
	 */
	public void setOptimisationProperties(OptimisationProperties optimProp) {
		optimisationProperties = optimProp;
	}
	/**
	 * Set simulation properties.
	 * 
	 * @param simProp Simulation properties.
	 */
	public void setSimulationProperties(SimulationProperties simProp) {
		simulationProperties = simProp;
	}
	/**
	 * Set the source file path.
	 * @param path Source file path.
	 */
	public void setSourceFilePath(String path) {
		relativeSourceFileName = path;
	}
}
