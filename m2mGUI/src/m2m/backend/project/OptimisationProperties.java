/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Properties for optimisation.
 *
 * Project:  Math2Mat
 *
 * @file OptimisationProperties.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: Provides method to get and set properties for optimisation.
 * 
 */

package m2m.backend.project;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.utils.XMLUtils;

public class OptimisationProperties {
	
	/**
	 * Specify the type of optimisation the user wants.
	 */
	private String optimisationType;
	/**
	 * Specify the size of the data.
	 */
	private NumType optimisationDataType;
	/**
	 * Specify the frequency the user wants to reach.
	 */
	private int optimisationFrequency;
	/**
	 * Specify the fifo type the user wants
	 */
	private String fifoType;
	
	/**
	 * Specify if the Fifos have to be optimized by the tool
	 */
	private boolean optimizeFifo;
	/**
	 * If the Fifo are not optimized, this defines the Fifo size
	 */
	private int fifoSize;
	

	/**
	 * boolean used to know if fifo compensation is used
	 */
	private boolean fifoComp;
	
	
	//constructors
	/**
	 * Default constructor. Initialise members to null.
	 */
	public OptimisationProperties() {
		optimisationType = "";
		optimisationDataType = NumType.FLOAT32;
		optimisationFrequency = 0;
		fifoType = "Standard";
		optimizeFifo = true;
		fifoSize = 64;
		fifoComp = true;
	}
	
	/**
	 * Initialise members with values in parameter.
	 * 
	 * @param type Type of optimisation.
	 * @param dataSize Size of data.
	 * @param frequency Wish frequency.
	 * @param fType Type of fifo the user wants (Standard, Altera or Xilinx)
	 * @param optFifo Indicates if the tool has to optimize the Fifo size or not
	 * @param fifoSize If the tool do not optimize the Fifo size, this indicates the Fifo size
	 * @param fifoCOmp If Input Fifos have to be added for delay compensation
	 */
	public OptimisationProperties(String type, NumType dataSize, int frequency, String fType,boolean optFifo,int fifoSize,boolean fifoComp) {
		optimisationType = type;
		optimisationDataType = dataSize;
		optimisationFrequency = frequency;
		fifoType = fType;
		optimizeFifo = optFifo;
		this.fifoSize = fifoSize;
		this.fifoComp = fifoComp;
	}
	
	public String getXmlTagName() {
		return "Optimisation";
	}
	
	public boolean fromXml(Element el) {
		this.setOptimisationType(XMLUtils.getTextValue(el,"Type",""));
		this.setOptimisationDataType(NumType.getNum(XMLUtils.getTextValue(el,"DataType", "FLOAT32")));
		this.setOptimisationFrequency(XMLUtils.getIntValue(el,"Frequency",0));
		this.setFifoType(XMLUtils.getTextValue(el,"FifoType","Standard"));
		this.setFifoSize(XMLUtils.getIntValue(el, "FifoSize",64));
		this.setOptimizeFifo(XMLUtils.getBoolValue(el,"OptFifo",true));
		this.setFifoCompensation(XMLUtils.getBoolValue(el,"FifoCompensation",true));
		return true;
	}
	
	public Element toXml(Document dom) {
		Element el;
		el=dom.createElement(getXmlTagName());
		el.appendChild(XMLUtils.createTextElement(dom,"Type",this.getOptimisationtype()));
		el.appendChild(XMLUtils.createTextElement(dom,"DataType",optimisationDataType.name()));
		el.appendChild(XMLUtils.createIntElement(dom,"Frequency",this.getOptimisationfrequency()));
		el.appendChild(XMLUtils.createTextElement(dom,"FifoType",this.getFifoType()));
		el.appendChild(XMLUtils.createBoolElement(dom,"OptFifo",this.getOptimizeFifo()));
		el.appendChild(XMLUtils.createIntElement(dom,"FifoSize", this.getFifoSize()));
		el.appendChild(XMLUtils.createBoolElement(dom,"FifoCompensation",this.getFifoCompensation()));
		return el;
	}
	
	
	//Getters
	/**
	 * Get the optimisation type.
	 * 
	 * @return The optimisation type if different from null.
	 */
	public String getOptimisationtype() {
		if (optimisationType != null)
			return optimisationType;
		else
			return new String("");
	}
	
	/**
	 * Get the data type.
	 * @return The type of data.
	 */
	public NumType getOptimisationDataType() {
		return optimisationDataType;
	}
	
	/**
	 * Get the wish frequency.
	 * @return The value of the wish frequency.
	 */
	public int getOptimisationfrequency() {
		return optimisationFrequency;
	}
	
	/**
	 * Get the type of fifo.
	 * @return The type of fifo.
	 */
	public String getFifoType() {
		return fifoType;
	}
	
	public boolean getOptimizeFifo() {
		return optimizeFifo;
	}
	
	public boolean getFifoCompensation() {
		return fifoComp;
	}
	
	public int getFifoSize() {
		return fifoSize;
	}
	
	public void setFifoSize(int size) {
		fifoSize = size;
	}
	
	public void setOptimizeFifo(boolean optimize) {
		optimizeFifo = optimize;
	}
	
	public void setFifoCompensation(boolean compensate) {
		fifoComp = compensate;
	}
	
	//Setters
	/**
	 * Set the optmisation type.
	 * 
	 * @param type Optimisation type.
	 */
	public void setOptimisationType(String type) {
		optimisationType = type;
	}
	
	/**
	 * Set the size of data.
	 * 
	 * @param dataSize Size of data.
	 */
	public void setOptimisationDataType(NumType dataType) {
		optimisationDataType = dataType;
	}
	
	/**
	 * Set the wish frequency.
	 * 
	 * @param frequency Wish frequency.
	 */
	public void setOptimisationFrequency(int frequency) {
		optimisationFrequency = frequency;
	}
	
	/**
	 * Set the fifo type.
	 * 
	 * @param fType Fifo type.
	 */
	public void setFifoType(String fType) {
		fifoType = fType;
	}
}