/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
i * @brief Description class of a Math2Mat project for the GUI.
 *
 * Project:  Math2Mat
 *
 * @file M2MProject.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 13.04.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This class provides elements to describe all the parameters of a Math2Mat project.
 * It saves all the parameters needed for the VHDL generation and the simulation.
 *
 */

package m2m.backend.project;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import m2m.backend.octaveparser.ParsingException;
import m2m.backend.processing.OptimizeOperators;
import m2m.backend.structure.StructTreatment;
import m2m.backend.utils.FileUtils;
import m2m.backend.utils.XMLUtils;
import m2m.backend.vhdl.CompileScriptCreator;
import m2m.backend.processing.ProgressMonitoring;


public class M2MProject {
	
	public final static String CURRENTVERSION="0.5.0";
	
	public String subfolder() {
		return "/m2m_"+this.getProperties().getFunctionName()+"/";
	}
	
	/**
	 * This monitoring is used by the graphical interface in order
	 * to display the state of any long-time processing task
	 */
	public ProgressMonitoring monitoring;
	
	/*
	public static String getApplicationPath() {
		return ExternalToolsProperties.getReference().getApplicationPath();
	}
	*/
	
	/**
	 * 
	 */
	private boolean last_monitoring;
	/**
	 * 
	 */
	private boolean modified;	

	
	
	/**
	 * @return the modified
	 */
	public boolean isModified() {
		return modified;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(boolean modified) {
		this.modified = modified;
	}

	private SimulationResult simResult;
	
	/**
	 * @return the simResult
	 */
	public SimulationResult getSimulationResult() {
		return simResult;
	}

	/**
	 * @param simResult the simResult to set
	 */
	public void setSimulationResult(SimulationResult simResult) {
		this.simResult = simResult;
	}

	public void setLastMonitoring(boolean monitor) {
		last_monitoring = monitor;
	}
	
//	private static String libPath="";
	
	public static String getLibPath() {
		return ExternalToolsProperties.getReference().getLibPath();
//		if (libPath.isEmpty())
//			return getApplicationPath()+"/lib";
//		return libPath;
	}
	
	/*
	public static void setLibPath(String path) {
		libPath=path;
	}
	*/

	/**
	 * Name of the subfolder containing the simulation files
	 */
	private final static String SIMPATH="sim";

	/**
	 * Name of the subfolder containing the IO files
	 */
	private final static String IOFILESPATH="sim/iofiles";
	
	/**
	 * Name of the subfolder containing the ubidule source file
	 */
	private final static String UBIDULEPATH="ubidule";
	
	/**
	 * Name of the subfolder containing the source file
	 */
	private final static String SRCPATH="src_m2m";
	
	/**
	 * Name of the subfolder containing the VHDL files
	 */
	private final static String VHDLPATH="src_VHDL";
	
	/**
	 * Name of the subfolder containing the Octave files
	 */
	private final static String OCTPATH="src_OCT";
	
	/**
	 * Name of the subfolder containing the SystemVerilog files
	 */
	private final static String SVPATH="src_SV";
	
	/**
	 * Name of the subfolder containing the report files
	 */
	private final static String REPORTPATH="report";
	
	/**
	 * Name of the subfolder containing the report files
	 */
	private final static String COMPPATH="sim/comp";
	
	/**
	 * Structure containing the parser result, that is the entire description
	 * of the code
	 */
	private StructTreatment structTreatment;
	
	/**
	 * All properties of the project.
	 */
	private ProjectProperties properties;

	/**
	 * boolean used to know if the VHDL design needs fifo
	 */
	private boolean useFifo = false;
	

	public boolean openProject(String fileName) {

		File file = new File(fileName);
		properties.setProjectFile(file);
		
		if (file.exists()) {
			if (!loadFile(file))
				return false;
			if (getStructTreatment().isEmpty()) {

				/* Parsing of the text of the editor */
				try {
					structTreatment.parse(new File(properties.getAbsoluteSourceFilename()));
					structTreatment.modifyNumType(properties.getOptimisationProperties().getOptimisationDataType());
					return true;
				}
				catch (ParsingException e1) {
					return true;
				}
			}
			else 
			{
				if (!checkSourceFile()) {
					checkFolders();
					restoreOctave();	
				}
				return true;
			}
		}
		else
			return false;
	}
	
	
	public boolean CreateEmptyProject(String fileName) {

		File file= new File(fileName);
		properties.setProjectFile(file);
		try {
			String projectPath = file.getAbsolutePath().replace("\\", "/").substring(0, file.getAbsolutePath().replace("\\", "/").lastIndexOf("/")+1);
			
			checkFolders();
			
			File sourceFile = new File(projectPath + subfolder()+ M2MProject.getSourceDirName()+"/" + file.getName().substring(0, file.getName().lastIndexOf(".")+1)+"m");

			if (!checkFolders())
				return false;
	    	FileWriter sourceFileWriter = new FileWriter(sourceFile.getAbsolutePath());
	    	String functionName=file.getName().substring(0, file.getName().lastIndexOf("."));
	    	
	    	String source = "% Here is a simple function that adds two numbers\n";
	    	source += "% Feel free to modify it\n\n";
	    	source += "function s="+functionName+"(a,b)\n\ts=a+b;\nendfunction\n";
	    	char buffer[] = new char[source.length()];
	    	source.getChars(0, source.length(), buffer, 0);
	    	sourceFileWriter.write(buffer);
	        sourceFileWriter.close();
	    	getProperties().setSourceFilePath(subfolder()+M2MProject.getSourceDirName() + "/" + sourceFile.getName());
	    	saveFile(file);
			try {
				structTreatment.parse(sourceFile);
				structTreatment.modifyNumType(properties.getOptimisationProperties().getOptimisationDataType());
				saveFile(file);
			}
			catch (ParsingException e) {
				
			}
		} catch (IOException e) {
		    	e.printStackTrace();
		      //  MessageBox messageBox = new MessageBox(window.getShell(), SWT.ICON_ERROR | SWT.OK);
		      //  messageBox.setMessage("File I/O Error.");
		      //  messageBox.setText("Error");
		      //  messageBox.open();
		        return false;
		    }
    	
		return true;
	}
	
	public boolean CreateNewProjectFromMath(String fileName,String fileNameOctave) {

		File file= new File(fileName);
		properties.setProjectFile(file);
			
			if (!checkFolders())
				return false;
			getProperties().setSourceFilePath(this.subfolder()+getSourceDirName() + "/" + new File(fileNameOctave).getName());
	    	if (!FileUtils.copyFile(fileNameOctave, this.getProperties().getAbsoluteSourceFilename()))
				return false;
			
			
		if (!saveFile(file))
			return false;
		try {
			structTreatment.parse(this.getSourceFile());
			structTreatment.modifyNumType(properties.getOptimisationProperties().getOptimisationDataType());	
			saveFile(file);
		}
		catch (ParsingException e) {
			
		}
		return true;
	}
	
	public M2MProject() {

		last_monitoring = false;
		
		structTreatment = new StructTreatment();
		properties= new ProjectProperties();
		
	}
	
	/**
	 * This constructor make or get informations about the project.
	 * If a project is loaded (*.m2m file), it gets all the information about it,
	 * otherwise it creates new blank instance of properties. The properties are saved in the
	 * M2MProject's instance when modified.
	 * @param file File used to create the project, a m2m project file (*.m2m) or a source file (*.m).
	 */
	/*
	public M2MProject(File file) {

		last_monitoring = false;
		
		structTreatment = new StructTreatment();
		properties= new ProjectProperties();
		properties.setProjectFile(file);

		// get the file extension
        String ext = (file.getName().lastIndexOf(".") == -1 ? "" :
        	file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length()));
		
        // if it is a m2m project file (*.m2m), create the corresponding class to extract information
		if (ext.equalsIgnoreCase("m2m")) {
			if (file.exists()) {
				loadFile(file);
			}
				
		//	}
			
		//	String projectPath = file.getAbsolutePath().replace("\\", "/").substring(0, file.getAbsolutePath().replace("\\", "/").lastIndexOf("/")+1);
		//	File sourceFile = new File(projectPath + M2MProject.getSourceDirName()+"/" + file.getName().substring(0, file.getName().lastIndexOf(".")+1)+"m");
	    	
			//m2mParser = new M2MParser(file);
//			properties = m2mParser.getProperties();
//			optimisationProperties = properties.getOptimisationProperties();
//			simulationProperties = properties.getSimulationProperties();
			//sourceFilePath = 
		//	properties.setSourceFilePath(projectPath + M2MProject.getSourceDirName()+"/" + file.getName().substring(0, file.getName().lastIndexOf(".")+1)+"m");
		}
		// else create a default project
		else {
			properties = new ProjectProperties();
		//	properties.setSourceFilePath(getRelativePath(properties.getProjectFile().getAbsolutePath().replace("\\", "/")));
		}
	}
	*/
	

	/**
	 * Get the useFifo attribute of the project.
	 * @return the useFifo attribute of the project
	 */
	public boolean getUseFifo() {
		return this.useFifo;
	}
	

	/**
	 * Update the boolean that tells if the VHDL design needs fifo.
	 * 
	 * @param bool Value to set.
	 */
	public void setUseFifo (boolean bool) {
		this.useFifo = bool;
	}	
	
	//Getters
	/**
	 * Get all the properties relative to the optimisation.
	 * 
	 * @return The optimisation properties.
	 */
	public OptimisationProperties getOptimisationProperties() {
        return properties.getOptimisationProperties();
	}

	/**
	 * Get all properties relative to the simulation.
	 * 
	 * @return The simulation properties.
	 */
	public SimulationProperties getSimulationProperties() {
		return properties.getSimulationProperties();
	}
	
	//returns the source code file, which is the input file if this input is not a m2m project file
	/**
	 * Get the source file. The source file is the input file if the file loaded is not a Math2Mat project.
	 * 
	 * @return The source file.
	 */
	public File getSourceFile() {
		/*if (m2mParser == null) {
			return file;
		}
		else {
			String dir = ((file.getPath().lastIndexOf("/") == -1 && (file.getPath().lastIndexOf("\\")) == -1) ? "" :
				file.getPath().substring(0, Math.max(file.getPath().lastIndexOf("/"), file.getPath().lastIndexOf("\\"))+1));
			File sourceFile = new File(dir+sourceFilePath);
			if (sourceFile.exists())
				return sourceFile;
			else
				return null;
		}*/
		File sourceFile = new File(properties.getAbsoluteSourceFilename());
		if (sourceFile.exists())
			return sourceFile;
		else
			return null;
	}
	
	/**
	 * Get the input file used to created the instance of M2MProject.
	 * 
	 * @return The path to the input file.
	 */
	public String getProjectFilename() {
		return properties.getProjectFile().getPath();
	}
	

	static public String getUbiduleDirName() {
		return UBIDULEPATH;
	}
	
	/**
	 * Get the name of the subfolder containing the VHDL files
	 * @return The name of the subfolder containing the VHDL files
	 */
	static public String getVHDLDirName() {
		return VHDLPATH;
	}

	/**
	 * Get the directory path of the VHDL files
	 * @return the directory path of the VHDL files
	 */
	public String getVHDLPath() {
		return properties.getProjectPath()+subfolder()+getVHDLDirName();
	}
	
	public String getVHDLSubFolder() {
		return subfolder()+getVHDLDirName();
	}


	/**
	 * Get the directory path of the ubidule files
	 * @return the directory path of the ubidule files
	 */
	public String getUbidulePath() {
		return properties.getProjectPath()+subfolder()+getUbiduleDirName();
	}

	/**
	 * Get the name of the subfolder containing the SystemVerilog files
	 * @return The name of the subfolder containing the SystemVerilog files
	 */
	static public String getSVDirName() {
		return SVPATH;
	}

	/**
	 * Get the directory path of the SystemVerilog files
	 * @return the directory path of the SystemVerilog files
	 */
	public String getSVPath() {
		return properties.getProjectPath()+subfolder()+getSVDirName();
	}

	/**
	 * Get the name of the subfolder containing the octave files
	 * @return The name of the subfolder containing the octave files
	 */
	static public String getOctaveDirName() {
		return OCTPATH;
	}

	/**
	 * Get the directory path of the octave files
	 * @return the directory path of the octave files
	 */
	public String getOctavePath() {
		return properties.getProjectPath()+subfolder()+getOctaveDirName();
	}

	/**
	 * Get the name of the subfolder containing the report files
	 * @return The name of the subfolder containing the report files
	 */
	static public String getReportDirName() {
		return REPORTPATH;
	}

	/**
	 * Get the directory path of the report files
	 * @return the directory path of the report files
	 */
	public String getReportPath() {
		return properties.getProjectPath()+subfolder()+getReportDirName();
	}


	/**
	 * Get the name of the subfolder containing the simulation files
	 * @return The name of the subfolder containing the simulation files
	 */
	static public String getSimDirName() {
		return SIMPATH;
	}

	/**
	 * Get the directory path of the simulation files
	 * @return the directory path of the simulation files
	 */
	public String getSimPath() {
		return properties.getProjectPath()+subfolder()+getSimDirName();
	}

	/**
	 * Get the name of the subfolder containing the IO files
	 * @return The name of the subfolder containing IO files
	 */
	static public String getIOFilesDirName() {
		return IOFILESPATH;
	}

	/**
	 * Get the directory path of the IO files
	 * @return the directory path of the IO files
	 */
	public String getIOFilesPath() {
		return properties.getProjectPath()+subfolder()+getIOFilesDirName();
	}
	
	/**
	 * Get the name of the subfolder containing the compilation files
	 * @return The name of the subfolder containing the compilation files
	 */
	static public String getCompDirName() {
		return COMPPATH;
	}

	/**
	 * Get the directory path of the compilation files
	 * @return the directory path of the compilation files
	 */
	public String getCompPath() {
		return properties.getProjectPath()+subfolder()+getCompDirName();
	}
	
	/**
	 * Get the name of the subfolder containing the source file
	 * @return The name of the subfolder containing the source file
	 */
	static public String getSourceDirName() {
		return SRCPATH;
	}
	
	/**
	 * Get the directory path of the source file
	 * @return the directory path of the source file
	 */
	public String getSourcePath(){
		return properties.getProjectPath()+subfolder()+getSourceDirName();
	}
	
	/**
	 * Get all the project properties.
	 * 
	 * @return The project properties.
	 */
	public ProjectProperties getProperties() {
		return properties;
	}
	
	/**
	 * Get the source file path.
	 * 
	 * @return The source file path.
	 */
	public String getSourceFilePath() {
		return properties.getRelativeSourceFilename();
	}

	//Setters
	/**
	 * Set all the project properties.
	 * 
	 * @param properties The properties to set.
	 */
	public void setProperties(ProjectProperties properties) {
		this.properties = properties;
	}
	
	/**
	 * Set the input file.
	 * 
	 * @param file Input file.
	 */
	public void setFile(File file) {
		properties.setProjectFile(file);
	}
	
	
	/**
	 * Compare two projects.
	 * 
	 * @param project The project to compare with the current.
	 * @return True if the two projects are identical. False otherwise. 
	 */
	public boolean compare (M2MProject project) {
		return (project.properties.getProjectFile().getAbsolutePath().equalsIgnoreCase(this.properties.getProjectFile().getAbsolutePath()) && project.properties.getRelativeSourceFilename().equalsIgnoreCase(this.properties.getRelativeSourceFilename()));
	}

	/**
	 * Checks if the project folders exist, and if not create them.
	 * The folders are the ones for VHDL, SV, Octave, Compilation, and report.
	 * This function is provided for convenience.
	 * @return True if the folders exist or are created correcty, false otherwise.
	 */
	public boolean checkFolders() {
		return checkFolders(properties.getProjectPath()+subfolder());
	}
	
	/**
	 * Checks if the project folders exist, and if not create them.
	 * The folders are the ones for VHDL, SV, Octave, Compilation, and report.
	 * @param basePath base dcirectory of the project
	 * @return True if the folders exist or are created correcty, false otherwise.
	 */
	static public boolean checkFolders(String basePath) {

		try {
			//if the base folder does not exists, create it
			File baseFolder = new File(basePath);
			if (!(baseFolder.exists() && baseFolder.isDirectory())) {
				baseFolder.mkdir();
			}

			//if the VHDL folder does not exists, create it
			File vhdlFolder = new File(basePath+"/"+getVHDLDirName());
			if (!(vhdlFolder.exists() && vhdlFolder.isDirectory())) {
				vhdlFolder.mkdir();
			}
	
			//if the SV folder does not exists, create it
			File svFolder = new File(basePath+"/"+getSVDirName());
			if (!(svFolder.exists() && svFolder.isDirectory())) {
				svFolder.mkdir();
			}
	
			//if the Octave folder does not exists, create it
			File octFolder = new File(basePath+"/"+getOctaveDirName());
			if (!(octFolder.exists() && octFolder.isDirectory())) {
				octFolder.mkdir();
			}
	
			//if the report folder does not exists, create it
			File reportFolder = new File(basePath+"/"+getReportDirName());
			if (!(reportFolder.exists() && reportFolder.isDirectory())) {
				reportFolder.mkdir();
			}

			//if the sim folder does not exists, create it
			File simFolder = new File(basePath+"/"+getSimDirName());
			if (!(simFolder.exists() && simFolder.isDirectory())) {
				simFolder.mkdir();
			}

			//if the sim/iofiles folder does not exists, create it
			File ioFilesFolder = new File(basePath+"/"+getIOFilesDirName());
			if (!(ioFilesFolder.exists() && ioFilesFolder.isDirectory())) {
				ioFilesFolder.mkdir();
			}

			//if the sim/comp folder does not exists, create it
			File compFolder = new File(basePath+"/"+getCompDirName());
			if (!(compFolder.exists() && compFolder.isDirectory())) {
				compFolder.mkdir();
			}

			//if the ubidule folder does not exists, create it
			File ubiduleFolder = new File(basePath+"/"+getUbiduleDirName());
			if (!(ubiduleFolder.exists() && ubiduleFolder.isDirectory())) {
				ubiduleFolder.mkdir();
			}

			//if the src folder does not exists, create it
			File srcFolder = new File(basePath+"/"+getSourceDirName());
			if (!(srcFolder.exists() && srcFolder.isDirectory())) {
				srcFolder.mkdir();
			}
			return true;
		}
		catch (SecurityException e) {
			
			return false;
		}
	}
	
	public void clear() {

		//delete sim directory content
		File simDir = new File(getSimPath());
		if (simDir.exists() && simDir.isDirectory())
			if (!FileUtils.deleteDirContent(simDir, null))
				System.err.println("Cannot delete files");
		
		//delete ubidule directory content
		File ubiduleDir = new File(getUbidulePath());
		if (ubiduleDir.exists() && ubiduleDir.isDirectory())
			if (!FileUtils.deleteDirContent(ubiduleDir, null))
				System.err.println("Cannot delete files");
		
		//delete OCT directory content
		File octSrcDir = new File(getOctavePath());
		if (octSrcDir.exists() && octSrcDir.isDirectory())
			if (!FileUtils.deleteDirContent(octSrcDir, null))
				System.err.println("Cannot delete files");
	
		//delete SV directory content
		File svSrcDir = new File(getSVPath());
		if (svSrcDir.exists() && svSrcDir.isDirectory())
			if (!FileUtils.deleteDirContent(svSrcDir, null))
				System.err.println("Cannot delete files");
	
		//delete VHDL directory content
		File vhdlSrcDir = new File(getVHDLPath());
		if (vhdlSrcDir.exists() && vhdlSrcDir.isDirectory())
			if (!FileUtils.deleteDirContent(vhdlSrcDir, null))
				System.err.println("Cannot delete files");
		
		//delete report directory content
		File reportDir = new File(getReportPath());
		if (reportDir.exists() && reportDir.isDirectory()) {
			if (!FileUtils.deleteDirContent(reportDir, null))
				System.err.println("Cannot delete files");
			reportDir.delete();
		}
	}
	
	public boolean generateVHDL() {
		structTreatment.setProject(this);
		String outFilePath = getVHDLPath() + "/" + structTreatment.getTop().getName() + ".vhd";
		if (!structTreatment.writeVHDL(getVHDLPath() + "/"))
			return false;
		//create compile script
		CompileScriptCreator scriptCreator = new CompileScriptCreator(structTreatment.getTop());
		scriptCreator.createCompileScript(this,properties.getProjectPath(), outFilePath);
		
		return true;
	}


	/**
	 * Get the strucTreatment corresponding to the project. 
	 * 
	 * @return Project's structTreatment.
	 */
	public StructTreatment getStructTreatment() {
		return structTreatment;
	}

	
	/**
	 * Clear the structTreatment.
	 */
	public void clearStructTreatment () {
		structTreatment = new StructTreatment();
	}
	
	public boolean compareOctaveResult() {
		String origOutputPath = getIOFilesPath() + "/file_output";
		String regenOutputPath = getIOFilesPath() + "/file_output_regen";
		
		for (int i = 0; i < getStructTreatment().getTop().getOutput().size(); i++) {
	
			File orig = new File(origOutputPath + (i+1) + ".dat");
			File regenFile = new File(regenOutputPath + (i+1) + ".dat");
			
			if (FileUtils.compareDataFiles(orig, regenFile)) {
				System.out.println("Original file(s) and regenerated file(s) give the same results");
			} else {
				System.err.println("Original file and regenerated file do not give the same results on output " + 
				getStructTreatment().getTop().getOutput().elementAt(i).getName());
			}
		}
		return true;
	}

	public boolean reGenerateOctave() 
	{	
		//get the path of the source file without the extension
		int dotPos = getSourceFile().getAbsolutePath().lastIndexOf(".");
		String srcFile = getSourceFile().getAbsolutePath().substring(0, dotPos);
		String generateFile = srcFile + "_regen.m";
		
		System.out.println("source file path : " + srcFile);
		System.out.println("gen file path : " + generateFile);
		
		String fname=structTreatment.getTop().getName();
		structTreatment.getTop().setName(fname+"_regen");
		
		if (!structTreatment.writeOctaveFile(generateFile))
			return false;
		
		structTreatment.getTop().setName(fname);
		
		System.out.println("Math2Mat file regenerated");
		return true;
	}

	
	public boolean generateFlatDotFile() {

		File file = this.getSourceFile();
		String path = (file.getAbsolutePath().lastIndexOf(".") == -1 ? "" :
        	file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(".")));
		
		String flatFileName = path + "_flat.gv";
		
		StructTreatment treatment = getStructTreatment();
		return treatment.writeSchematic(flatFileName);
	}

	public boolean generateTreeDotFile() {

		StructTreatment struct=new StructTreatment();
		try{
			struct.parseNoFlat(new File(this.getProperties().getAbsoluteSourceFilename()));
		}
		catch (ParsingException e) {
			
		}
		
		
		File file = this.getSourceFile();
		String path = (file.getAbsolutePath().lastIndexOf(".") == -1 ? "" :
        	file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(".")));
		
		String treeFileName = path + "_tree.gv";
		
		//File dotFile = new File(fileName);
		return struct.writeSchematic(treeFileName);
	}

	public boolean generateCopyTreeDotFile() {

		File file = this.getSourceFile();
		String path = (file.getAbsolutePath().lastIndexOf(".") == -1 ? "" :
        	file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(".")));
		
		String treeFileName = path + "_copy_tree.gv";
		
		//File dotFile = new File(fileName);
		StructTreatment treatment = getStructTreatment().copy();
		
		getStructTreatment().getTop().modifAllNames("orig_");
		

	//	treatment.getTop().modifAllNames("new_");
		return treatment.writeSchematic(treeFileName);
	}
	
	public boolean launchDotGeneration() {
		if (!generateTreeDotFile())
			return false;
		return generateFlatDotFile();
	}
	
	public boolean optimize() {

		OptimizeOperators optimise = new OptimizeOperators(structTreatment);
		optimise.opTreeDepth();
		//optimise.optimise();
		return true;
	}
	
	public boolean save() {
//		return saveFile(new File(properties.getProjectFullFilename()));
		File f=new File(properties.getProjectFullFilename());
		saveFile(f);
		this.setModified(false);

//		M2MProject newproj=new M2MProject();
//		newproj.openProject(f.getAbsolutePath());
//		newproj.saveFile(new File(f.getAbsoluteFile()+"_copy.xml"));
		
		return true;
		
	}

	public boolean saveFile(File f) {
		//get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			Document dom = db.newDocument();
		
			dom.appendChild(this.toXml(dom));
			
            /////////////////
            //Output the XML

            //set up a transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            transfac.setAttribute("indent-number", new Integer(4));

            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");



            //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(dom);
            trans.transform(source, result);
            String xmlString = sw.toString();
            
            try{
                // Create file 
                FileWriter fstream = new FileWriter(f);
                    BufferedWriter out = new BufferedWriter(fstream);
                out.write(xmlString);
                //Close the output stream
                out.close();
                }catch (Exception e){//Catch exception if any
                  System.err.println("Error: " + e.getMessage());
                }
            
		}
		catch(ParserConfigurationException pce) {
			//dump it
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			return false;
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public boolean compatibleVersion(String version1,String version2) {
		String splitV1[]=version1.split("\\p{Punct}",3);
		String splitV2[]=version2.split("\\p{Punct}",3);
		int v1[] = new int[3];
		int v2[] = new int[3];
		for(int i=0;i<3;i++) {
			v1[i]=Integer.parseInt(splitV1[i]);
			v2[i]=Integer.parseInt(splitV2[i]);
		}
		if (v1[0]>v2[0])
			return false;
		if (v1[0]<v2[0])
			return true;
		if (v1[1]>v2[1])
			return false;
		if (v1[1]<v2[1])
			return true;
		if (v1[2]>v2[2])
			return false;
		if (v1[2]<v2[2])
			return true;
		return true;
	}
	
	public boolean loadFile(File f) {

		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			Document dom = db.parse(f);

			dom.getDocumentElement().normalize();
		//	  System.out.println("Root element " + dom.getDocumentElement().getNodeName());
			  
			  Element docEl = dom.getDocumentElement();
			  
			  String version=docEl.getAttribute("version");
			  if (!version.isEmpty())
				  if (!compatibleVersion(version,CURRENTVERSION)) {
					  System.out.println("The files corresponds to a newer version of Math2mat. Please update your software.");
					  return false;
				  }
			  this.fromXml(docEl);

//			  getStructTreatment().treeToVector();
//				this.getStructTreatment().postParse(this.getStructTreatment().getTop());
			  

		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		}catch(SAXException se) {
			se.printStackTrace();
			return false;
		}catch(IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void restoreOctave() {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(this.properties.getAbsoluteSourceFilename()));
			out.write(this.structTreatment.getSourceCode());
			out.close();
		}
		catch (IOException e)
		{
			System.out.println("Can not regenerate the Octave source file " +
					"from the project file.");
		}	
	}
	
	public boolean sourceFileExist() {
		File file=this.getSourceFile();
		if (file==null)
			return false;
		if (!file.exists())
			return false;
		return true;
	}
	
	public boolean checkSourceFile() {
		if (this.structTreatment==null)
			return true;
		if (this.structTreatment.getSourceMd5()==null)
			return true;
		if (this.structTreatment.getSourceMd5().isEmpty())
			return true;

		File file=this.getSourceFile();
		if (file==null)
			return false;
		if (file.exists()) {

			String sourceMd5=FileUtils.getMd5(file.getAbsolutePath());
			if (!sourceMd5.equals(structTreatment.getSourceMd5())) {
				return false;
			}
		}
		return true;
	}
	

	public String getXmlTagName() {
		return "M2MProject";
	}
	
	public boolean fromXml(Element el) {
		String source=XMLUtils.getTextValue(el,"SourceFile","");
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase(this.properties.getOptimisationProperties().getXmlTagName())) {
				this.properties.getOptimisationProperties().fromXml((org.w3c.dom.Element)node);
			}
			else if (node.getNodeName().equalsIgnoreCase(this.properties.getSimulationProperties().getXmlTagName())) {
				this.properties.getSimulationProperties().fromXml((org.w3c.dom.Element)node);
			}
			if (node.getNodeName().equalsIgnoreCase(this.structTreatment.getXmlTagName())) {
				this.structTreatment.fromXml((org.w3c.dom.Element)node);
			}
			node=node.getNextSibling();
		}

		File f=new File(source);
	//	this.properties.setSourceFilePath(source);
		this.properties.setSourceFilePath(subfolder()+getSourceDirName()+"/"+f.getName());
		return true;
	}

	public Element toXml(Document dom) {
		Element el;
		el=dom.createElement(getXmlTagName());
		el.setAttribute("version",CURRENTVERSION);
		el.appendChild(XMLUtils.createTextElement(dom,"SourceFile",properties.getRelativeSourceFilename()));
		el.appendChild(this.properties.getOptimisationProperties().toXml(dom));
		el.appendChild(this.properties.getSimulationProperties().toXml(dom));
		if (this.structTreatment!=null)
			el.appendChild(this.structTreatment.toXml(dom));
		return el;
	}
	
	public boolean isUbiduleFilesUpToDate() {
		File file = this.getSourceFile();
		long timeCurrent = file.lastModified();
	//	timeCurrent = this.getStructTreatment().getParseTime();
		long timeFileRef = (new File(getUbidulePath()+"/M2M_XSVF_generated.xsvf")).lastModified();
		
	    return (timeCurrent < timeFileRef);
	}

	public boolean isOctaveUpToDate() {
		if ((!last_monitoring) && this.structTreatment.getMonitor())
			return false;
		File file = this.getSourceFile();
		long timeCurrent = file.lastModified();
	//	timeCurrent = this.getStructTreatment().getParseTime();
		long timeFileDoAll = (new File(getOctavePath()+"/m2m_doall.m")).lastModified();
		long timeFileOutput = (new File(getIOFilesPath()+"/file_output1.dat")).lastModified();
		
	    return ((timeCurrent < timeFileDoAll  && timeCurrent < timeFileOutput));
	}

	public boolean isVHDLUpToDate() {
		File file = this.getSourceFile();
		long timeCurrent = file.lastModified();
	//	timeCurrent = this.getStructTreatment().getParseTime();
		long timeFilepkg = (new File(getVHDLPath()+"/pkg_cellule.vhd")).lastModified();
		
	    return (timeCurrent < timeFilepkg);
	}
	
}

