/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package m2m.backend.buildingblocks;

import java.util.Hashtable;
import java.io.FileInputStream;

public class BuildingBlockLoader extends ClassLoader {
	
	private static BuildingBlockLoader instance = null;
	
	private Hashtable<String,Class<?>> classes = new Hashtable<String,Class<?>>();
	private String m_dir;
	private String m_packageName;
	
	public static BuildingBlockLoader getInstance() {
		if (instance == null) {
			instance = new BuildingBlockLoader();
		}
		return instance;
	}
    
	public void setDir(String dir) {
		this.m_dir = dir;
	}
	public void setPackageName(String name) {
		this.m_packageName = name;
	}

	/**
	* This sample function for reading class implementations reads
	* them from the local file system
	*/
	private byte getClassImplFromDataBase(String className)[] {
		//System.out.println(\t>>>>>> Fetching the implementation of "+className);
		byte result[];
		try {
			FileInputStream fi = 
					  new FileInputStream(this.m_dir+"/"+className+".class");
			result = new byte[fi.available()];
			fi.read(result);
			return result;
		} catch (Exception e) {
			/*
			 * If we caught an exception, either the class wasnt found or it
			 * was unreadable by our process.
			 */
			return null;
		}
	}

	/**
	 * This is a simple version for external clients since they
	 * will always want the class resolved before it is returned
	 * to them.
	 */
	@Override
	public Class<?> loadClass(String className) throws ClassNotFoundException {
		@SuppressWarnings("unchecked")
		Class<?> c=(Class<BuildingBlock>)loadClass(className,true);
		return (c);
	}

	public BuildingBlock newInstance(String className) 
			  throws ClassNotFoundException {
		try { 
			Object o = (loadClass(className)).newInstance();
			BuildingBlock b=((BuildingBlock) o); 
			return b; 
		} catch (Exception e) { 
			//System.out.println("Caught exception : "+e);
			return null; 
		} 
	}
	
	/**
	 * This is the required version of loadClass which is called
	 * both from loadClass above and from the internal function
	 * FindClassFromClass.
	 */
	public synchronized Class<?> loadClass(String className, boolean resolveIt)
			  throws ClassNotFoundException {
		Class<?> result;
		byte  classData[];

		//System.out.println("\t>>>>>> Load class : "+className);

		try{
			Class<?> t = Class.forName(className);
			//System.out.println("The class "+className+" is already loaded!!");
			//System.out.println("");
			return t;
		} catch(Exception e) {
		//System.out.println("The class "+className+" is not loaded yet");
		}

		/* Check our local cache of classes */
		result = (Class<?>)this.classes.get(className);
		if (result != null) {
			//    System.out.println("\t>>>>>> returning cached result.");
			return result;
		}
		//System.out.println("The class does not exist");
		/* Check with the primordial class loader */
		try {
			result = super.findSystemClass(className);
			//System.out.println("\t>>>>>> returning system class (in CLASSPATH).");
			return result;
		} catch (ClassNotFoundException e) {
			//System.out.println("\t>>>>>> Not a system class.");
		}

		/* Try to load it from our repository */
		classData = getClassImplFromDataBase(className);
		if (classData == null) {
			throw new ClassNotFoundException();
		}

		//System.out.println("Class name: " + className);
		/* Define it (parse the class file) */
		/* java 1.6 */
		result = defineClass(this.m_packageName+"."+className,classData, 0, classData.length);
		/* java 1.4 */
		//    result = defineClass(classData, 0, classData.length);
		if (result == null) {
			throw new ClassFormatError();
		}

		if (resolveIt) {
			resolveClass(result);
		}
		try{
			this.classes.put(className, result);
		} catch (NullPointerException e) {
			//System.out.println("Error with a class get");
		}
		//System.out.println("\t>>>>>> Returning newly loaded class.");
		return result;
	}
}
