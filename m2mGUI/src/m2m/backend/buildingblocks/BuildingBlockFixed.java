/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


package m2m.backend.buildingblocks;


/**
 * This class represents the base class for a building block performing 
 * calculation on fixed point numbers. It allows to set the number of bits,
 * as well as the number of decimal bits, in order to generate correct VHDL.
 * By default, the VHDL components are considered to be generic.
 */
public abstract class BuildingBlockFixed extends BuildingBlock {

	/**
	 * Default constructor, setting the data size to 16, and the number of
	 * decimal bits to 8.
	 */
	BuildingBlockFixed() {
		m_size=16;
		m_decimals=8;
	}
	
	/** Returns the numbers type (NumType.FIXED).
	 * @return NumType.FIXED
	 */
	public final NumType numType() {
		return NumType.FIXED;
	}

	/**
	 * Indicates if the operands have a generic size or not.
	 * For fixed point data, the default is yes, as generic building blocks should be
	 * quite easy to describe.
	 * @return true
	 */
	@Override
	public boolean useGenericSize() {
		return true;
	}

	/**
	 * Sets the size of the data, as well as the number of decimal bits.
	 * @param size Total size of the data
	 * @param decimals number of decimal bits
	 * @return false if the number of decimal bits is greater than the total
	 *         number of bits, or if one of these numbers is negative,
	 *         true else.
	 */
	public boolean setSize(int size, int decimals) {
		if (size<0)
			return false;
		if (decimals<0)
			return false;
		if (decimals > size)
			return false;
		m_decimals = decimals;
		m_size = size;
		return true;
	}
    
	/**
	 * Returns the number of bits used to represent the data.
	 * @return the number of bits
	 */
	public int getSize() {
		return m_size;
	}

	/**
	 * Returns the number of decimal bits used to represent the data.
	 * @return the number of decimal bits
	 */
	public int getDecimals() {
		return m_decimals;
	}
    
	/**
	 * Data size
	 */
	protected int m_size;
 
	/**
	 * Number of decimal bits
	 */
	protected int m_decimals;
}

