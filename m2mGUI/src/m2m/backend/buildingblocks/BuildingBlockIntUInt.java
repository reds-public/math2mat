/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package m2m.backend.buildingblocks;

/**
 * This class represents the base class for a building block performing 
 * calculation on signed or unsigned integers. It allows to set the size of the date,
 * something useful for resource and period calculation.
 * By default, such a data is on 16 bits, and the VHDL components are generic.
 */
public abstract class BuildingBlockIntUInt extends BuildingBlock {

	/**
	 * Default constructor setting the size of the data to 16.
	 */
	BuildingBlockIntUInt() {
		m_size=16;
	}
	
	/** Returns the numbers type (NumType.INTUINT).
	 * @return NumType.INTUINT
	 */
	@Override
	public NumType numType() {
		return NumType.INTUINT;
	}
    

	/**
	 * Indicates if the operands have a generic size or not.
	 * For integers, the default is yes, as generic building blocks should be
	 * quite easy to describe.
	 * @return true
	 */
	@Override
	public boolean useGenericSize() {
		return true;
	}
	
	/**
	 * Sets the size of the numbers. It can then be used to evaluate the
	 * resources usage as well as the minimum period.
	 * @param size Size of the numbers
	 * @return false if size is negative, true else
	 */
	public boolean setSize(int size) {
		if (m_size<0)
			return false;
		m_size = size;
		return true;
	}
    
	/**
	 * Size of the numbers
	 */
	protected int m_size;
}

