/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks;

/**
 * This class represents the base class for a building block performing 
 * calculation on 32-bit floats.
 * 
 * It should be subclassed for each 32-bit float function.
 * Example:
 * <br>
 * <code>
 * public class MultiplierPipe5 extends BuildingBlockFloat32 {
 *
 *	@Override
 *    public String vhdlFileName(){
 *        return "multpipe5.vhd";
 *    }
 *
 *	@Override
 *    public ImplType implType() {
 *        return ImplType.PIPELINE;
 *    }
 *
 *	@Override
 *    public int latencyTime() {
 *        return 5;
 *    }
 *
 *	@Override
 *    public int cycleTime() {
 *        return 1;
 *    }
 *
 *	@Override
 *    public int resources() {
 *        return 123;
 *    }
 *
 *	@Override
 *    public int period() {
 *        return 10/(getSignificandSize()+getExponentSize());
 *    }
 *
 *	@Override
 *    public String functionName() {
 *        return "mult";
 *    }
 *
 *	@Override
 *    public String entityName() {
 *    	return "multpipe5";
 *    }
 *
 *	@Override
 *    public String author() {
 *    	return "Yann Thoma";
 *    }
 *
 *	@Override
 *    public int version() {
 *    	return 1;
 *    }
 *
 *        @Override
 *    public String description() {
 *    	return "A simple pipeline multiplier";
 *    }
 *
 *}
 * </code>
 */
public abstract class BuildingBlockFloat32 extends BuildingBlockFloatAbstract {

	/**
	 * Default constructor. It sets the significand size to 24 and the exponent
	 * size to 7.
	 */
	public BuildingBlockFloat32() {
		m_significandSize = 24;
		m_exponentSize = 7;
	}

	/** Returns the numbers type (NumType.FLOAT32).
	 * @return NumType.FLOAT32
	 */
	@Override
	public final NumType numType() {
		return NumType.FLOAT32;
	}
}

