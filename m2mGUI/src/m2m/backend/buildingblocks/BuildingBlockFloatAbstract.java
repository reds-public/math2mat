/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks;

public abstract class BuildingBlockFloatAbstract extends BuildingBlock {

	/**
	 * Default constructor. It sets the significand size to 24 and the exponent
	 * size to 7.
	 */
	BuildingBlockFloatAbstract() {
		m_significandSize = 24;
		m_exponentSize = 7;
	}

	/**
	 * Returns the number of bits used to represent the exponent.
	 * @return the number of bits of the exponent
	 */
	public int getExponentSize() {
		return m_exponentSize;
	}

	/**
	 * Returns the number of decimal bits used to represent the significand.
	 * @return the number of bits of the significand
	 */
	public int getSignificandSize() {
		return m_significandSize;
	}
	/**
	 * Size of the exponent
	 */
	protected int m_exponentSize;
	/**
	 * Size of the significand
	 */
	protected int m_significandSize;
}

