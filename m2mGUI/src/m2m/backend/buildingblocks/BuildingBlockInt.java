/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package m2m.backend.buildingblocks;

/**
 * This class represents the base class for a building block performing 
 * calculation on integers.
 */
public abstract class BuildingBlockInt extends BuildingBlockIntUInt {
	
	/** Returns the numbers type (NumType.INT).
	 * @return NumType.INT
	 */
	@Override
	public final NumType numType() {
		return NumType.INT;
	}
}

