/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks;

import java.util.ArrayList;


/**
 * This class represents a building block for the math2mat project.
 * Its purpose is to supply the description of such a block, by
 * being derived once for each component.
 *
 * @author Yann Thoma
 * @version 1.0
 */
abstract public class BuildingBlock {

	/**
	 * A default empty constructor
	 */
	public BuildingBlock() {
	}

	/**
	 * Type of numbers processed by a building block
	 */
	public enum NumType {

		/** A generic float */
		FLOAT,
		/** A 32-bit IEEE float */
		FLOAT32,
		/** A 64-bit IEEE float */
		FLOAT64,
		/** A fixed-point number */
		FIXED,
		/** For integers or unsigned integers */
		INTUINT,
		/** An signed integer */
		INT,
		/** An unsigned integer */
		UINT,
		/** Another unknown type */
		OTHER;

		
		/**
		 * @param textValue
		 * @return
		 */
		public static NumType getNum(String text) {
			
			for(NumType num : NumType.values())
				if(text.equalsIgnoreCase(num.name()))
					return num; 
	
			return NumType.OTHER;
		}
		
		/**
		 * 
		 * @param numType
		 * @return
		 */
		public static String getVHDLType(NumType numType) {
			switch(numType) {
				case FLOAT32: return "std_logic_vector(31 downto 0)";
				case FLOAT64: return "std_logic_vector(63 downto 0)";
				default: return "std_logic_vector(31 downto 0)";	
			}
		}

		/**
		 * 
		 * @param numType
		 * @return
		 */
		public static int getDataSize(NumType numType) {
			switch(numType) {
				case FLOAT32: return 32;
				case FLOAT64: return 64;
				default: return 32;	
			}
		}
	};

	/**
	 * Implementation type of a building block
	 */
	public enum ImplType {

		/** Sequential, typically with a cycle_time greater than 1 */
		SEQUENTIAL,
		/** Pipeline, typically with a cycle_time equal to 1 */
		PIPELINE,
		/** Combinatorial, with a long combinatorial datapath and an output register */
		COMBINATORIAL
	};

	/**
	 * Indicates if the operands have a generic size or not.
	 * If yes, the methods of subclasses allow to fix the size of the operands.
	 * By default the size is not generic, so this function does not necesseraly needs
	 * to be overriden.
	 * @return true if the size is generic, false else
	 */
	public boolean useGenericSize() {
		return false;
	}

	/**
	 * Returns the number representation, that can be FLOAT, DOUBLE, FIXED, INT, UINT, or INTUINT.
	 * It will allow to extend the tool for other number representations.
	 * @return the number representation
	 */
	public abstract NumType numType();

	/**
	 * Returns the implementation type, that can be SEQUENTIAL, PIPELINE, or COMBINATORIAL.
	 * It may help the optimizer to choose the most appropriate component for a given 
	 * application.
	 * @return The implementation type
	 */
	public abstract ImplType implType();

	/**
	 * Returns the version number of the building block.
	 * @return the version number, as an integer
	 */
	public abstract int version();

	/**
	 * Returns the author of the VHDL file.
	 * @return the author name
	 */
	public abstract String author();

	/**
	 * Indicates the latency time of the building block.
	 * For a pipeline implementation, it returns the number of pipe stages,
	 * while for a sequential implementation it returns the number of clock
	 * cycles required for a processing, if this time is known, 0 else.
	 * For a combinatorial implementation it returns 1.
	 * @return the latency time
	 */
	public int latencyTime() {
		return 1;
	}

	/**
	 * Indicates a hardware resources evaluation of the block.
	 * This value is relative to the other building blocks.
	 * @return an evaluation of the hardware resources required
	 */
	public abstract int resources();

	/**
	 * Indicates the minimum period at which the block is functional.
	 * This value is relative to the other building blocks.
	 * @return an evaluation of the minumum period required
	 */
	public abstract int period();

	/**
	 * Indicates the cycle time of the building block.
	 * For a standard pipeline implementation, it should be 1, as a new
	 * calculation can be launched at every clock cycle.
	 * For a sequential implementation it should return the number of clock
	 * cycles required for a processing, if this is known, 0 else.
	 * For a combinatorial implementation, it is not relevant.
	 * @return the cycle time
	 */
	public int cycleTime() {
		return 1;
	}

	/**
	 * Returns the name of the function implemented by the block.
	 * All blocks performing the same function should return the 
	 * same name. It has to be fixed for the operators, and can be
	 * user defined for functions.
	 *
	 * The basic functions HAVE to respect there names:
	 * <ul><li> multiplication: mult
	 * <li> division:       div
	 * <li> addition:       add
	 * <li> subtraction:    sub
	 * <li> square root:    sqrt
	 * <li> exponential:    exp
	 * </ul>
	 * Other functions can be added, such as sin, cos, ...
	 * The name returned by functionName() should then be the same
	 * as the one used in the mathematical source code.
	 * @return the name of the function calculated by the block
	 */
	public abstract String functionName();

	/**
	 * Returns the name of the VHDL file in which the block is described.
	 * @return the name of the VHDL file
	 */
	public abstract String vhdlFileName();

	/**
	 * Returns the names of the VHDL files the block needs.
	 * @return Names of dependent VHDL files.
	 */
	public abstract ArrayList<String> dependentFiles();

	/**
	 * Returns the name of the VHDL entity describing the block.
	 * @return the name of the VHDL entity
	 */
	public abstract String entityName();

	/**
	 * Returns the number of inputs for the block.
	 * By default it returns 2, as the main building blocks deal with
	 * 2 inputs and 1 output.
	 * @return the number of inputs
	 */
	public int nbInputs(){
		return 2;
	}
	
	/**
	 * Returns the number of outputs for the block.
	 * By default it returns 1, as the main building blocks deal with
	 * 2 inputs and 1 output.
	 * @return the number of outputs
	 */
	public int nbOutputs() {
		return 1;
	}

	/**
	 * Indicates if this block is only valid for a specific target.
	 * If not overriden, then the block is generic, and does not rely
	 * on a specific hardware (no use of DSP blocks, for instance).
	 * @return the target device name if relevant, null else
	 */
	public String device() {
		return null;
	}

	/**
	 * Returns a description of the building block. It can then be
	 * exploited to display a list of available components.
	 * @return a description of the block
	 */
	public abstract String description();
}

