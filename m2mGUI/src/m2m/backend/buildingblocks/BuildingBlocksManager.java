/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks;

import java.util.*;
import java.io.*;

import m2m.backend.utils.FileUtils;


public class BuildingBlocksManager {

	private static BuildingBlocksManager instance = null;
	protected String m_dirName;
	protected String m_packageName;

	private BuildingBlocksManager() {
		this.m_dirName = "m2m/backend/buildingblocks/blocks";
		this.m_packageName = "m2m.backend.buildingblocks.blocks";
		this.m_list = new ArrayList<BuildingBlock>();
		this.m_loader = BuildingBlockLoader.getInstance();
	}

	public static BuildingBlocksManager getInstance() {
		if (instance == null) {
			instance = new BuildingBlocksManager();
		}
		return instance;
	}

	public void setDirName(String dirName) {
		this.m_dirName = dirName;
		this.m_loader.setDir(dirName);
	}

	public void setPackageName(String packageName) {
		this.m_packageName = packageName;
		this.m_loader.setPackageName(packageName);
	}

	public String getDirName() {
		return this.m_dirName;
	}

	public String getPackageName() {
		return this.m_packageName;
	}

	class ClassFilter implements FileFilter {

		public boolean accept(File pathname) {
			try {
				return pathname.getCanonicalPath().endsWith(".class");
			} catch (IOException e) {
				return false;
			}
		}
	}

	class JavaFilter implements FileFilter {

		public boolean accept(File pathname) {
			try {
				return pathname.getCanonicalPath().endsWith(".java");
			} catch (IOException e) {
				return false;
			}
		}
	}
	protected ArrayList<BuildingBlock> m_list;
	protected BuildingBlockLoader m_loader;

	public boolean importFile(String fileName) {
		File f = new File(fileName);
		String destFileName = this.m_dirName + "/" + f.getName();
		FileUtils.copyFile(fileName, destFileName);
		File destFile = new File(destFileName);
		String className;
		if (f.getName().endsWith(".java")) {
			compileFile(destFileName);
			className = destFile.getName().replace(".java", "");
		} else {
			className = destFile.getName().replace(".class", "");
		}
		return load(className);
	}

	public boolean compileFile(String fileName) {
		try {
			System.out.println("File to compile: " + fileName);
			ProcessBuilder pb = new ProcessBuilder("javac", fileName);
			pb.redirectErrorStream(true);
			Process p = pb.start();
			int exitCode = p.waitFor();

			final BufferedReader br = new BufferedReader(new InputStreamReader(
					  p.getInputStream()), 50 /* keep small for testing */);
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			br.close();

			if (exitCode != 0) {
				System.out.println("The file " + fileName + " could not be compiled");
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error with the file compilation. " +
					  "Please check that javac is accessible in the current Path");
			return false;
		}
		return true;
	}

	public boolean load(BuildingBlockLoader loader, String fileName) {
		BuildingBlock block;
		String className = fileName;
		className = className.replaceFirst(".class", "");
		try {
			block = loader.newInstance(className);
			System.out.println("--------------------------------------");
			System.out.println("Adding a new building block:");
			System.out.println("Function name :" + block.functionName());
			System.out.println("Entity name   :" + block.entityName());
			System.out.println("Author        :" + block.author());
			System.out.println("Description   :" + block.description());
			System.out.println("--------------------------------------");
			addBuildingBlock(block);
			return true;
		} catch (Exception e) {
			System.out.println("Caught exception for class " + className + ": " + e);
			return false;
		}
	}

	public boolean load(String fileName) {
		if (this.m_loader != null) {
			return load(this.m_loader, fileName);
		} else {
			return load(this.m_loader = BuildingBlockLoader.getInstance(), fileName);
		}
	}

	public boolean compileAll() {
		File dir = new File(this.m_dirName);
		File[] files = dir.listFiles(new JavaFilter());
		boolean ok = true;
		for (int i = 0; i < files.length; i++) {
			try {
				ok &= compileFile(files[i].getCanonicalPath());
			} catch (IOException e) {
				ok = false;
			}
		}
		return ok;
	}

	public boolean loadAll() {
		File dir = new File(this.m_dirName);
		System.out.println("Load blocks from folder: "+dir.getAbsolutePath());
		File[] files = dir.listFiles(new ClassFilter());
		if (files==null)
			return false;
		for (int i = 0; i < files.length; i++) {
			load(files[i].getName());
		}
		return true;
	}

	public void addBuildingBlock(BuildingBlock block) {
		this.m_list.add(block);
	}

	public List<BuildingBlock> blockNamed(String functionName) {
		List<BuildingBlock> res = new ArrayList<BuildingBlock>();
		for (BuildingBlock block : m_list) {
			if (block.functionName().equals(functionName)) {
				res.add(block);
			}
		}
		return res;
	}
	
	public boolean isABlock(String functionName) {
		for (BuildingBlock block : m_list) {
			if (block.functionName().equals(functionName)) {
				return true;
			}
		}
		return false;
	}
}
