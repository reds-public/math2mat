/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package m2m.backend.buildingblocks;

/**
 * This class represents the base class for a building block performing 
 * calculation on floating point numbers. It allows to set the number of bits
 * for the exponent and the significand, in order to generate correct VHDL.
 * By default, the VHDL components are considered to be generic.
 */
public abstract class BuildingBlockFloat extends BuildingBlockFloatAbstract {
	
	/**
	 * Default constructor. It sets the significand size to 24 and the exponent
	 * size to 7.
	 */
	BuildingBlockFloat() {
		m_significandSize=24;
		m_exponentSize=7;
	}
	
	/** Returns the numbers type (NumType.FLOAT).
	 * @return NumType.FLOAT
	 */
	public final NumType numType() {
		return NumType.FLOAT;
	}

	
	/**
	 * Indicates if the operands have a generic size or not.
	 * For fixed point data, the default is yes, as generic building blocks should be
	 * quite easy to describe.
	 * @return true
	 */
	@Override
	public boolean useGenericSize() {
		return true;
	}
	
	/**
	 * Sets the size of the exponent and the significand, in bits.
	 * @param significandSize Total size of the significant
	 * @param exponentSize Total size of the significant
	 * @return false if the number of decimal bits is greater than the total
	 *         number of bits, or if one of these numbers is negative,
	 *         true else.
	 */
	public boolean setSize(int significandSize, int exponentSize) {
		if (!useGenericSize())
			return false;
		if (significandSize<0)
			return false;
		if (exponentSize<0)
			return false;
		m_exponentSize = exponentSize;
		m_significandSize = significandSize;
		return true;
	}
}

