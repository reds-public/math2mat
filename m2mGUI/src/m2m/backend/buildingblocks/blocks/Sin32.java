/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks.blocks;

import java.util.ArrayList;

import m2m.backend.buildingblocks.*;

public class Sin32 extends BuildingBlockFloat32 {

	@Override
	public String vhdlFileName() {
		return "sinus.vhd";
	}

	@Override
	public ImplType implType() {
		return ImplType.SEQUENTIAL;
	}

	@Override
	public int latencyTime() {
		return 10;
	}
	
	@Override
	public int nbInputs() {
		return 1;
	}

	@Override
	public int cycleTime() {
		return 1;
	}

	@Override
	public int resources() {
		return 50;
	}

	@Override
	public int period() {
		return 10;
	}

	@Override
	public String functionName() {
		return "sin";
	}

	@Override
	public String entityName() {
		return "Sinus";
	}

	@Override
	public String author() {
		return "Daniel";
	}

	@Override
	public int version() {
		return 1;
	}

	@Override
	public String description() {
		return "A simple sinus for testing";
	}

	@Override
	public ArrayList<String> dependentFiles() {
		return null;
	}
}

