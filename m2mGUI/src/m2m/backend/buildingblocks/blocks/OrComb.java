/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file AndComb32.java
 * @author Daniel Molla
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 20 janvier 2011
 *
 * Author: Daniel Molla
 *
 * Description: A simple or logic
 * 
 */
package m2m.backend.buildingblocks.blocks;

import java.util.ArrayList;

import m2m.backend.buildingblocks.BuildingBlockLogic;

public class OrComb extends BuildingBlockLogic {

	@Override
	public String vhdlFileName() {
		return "orLogic.vhd";
	}

	@Override
	public ImplType implType() {
		return ImplType.COMBINATORIAL;
	}

	@Override
	public int latencyTime() {
		return 0;
	}

	@Override
	public int cycleTime() {
		return 0;
	}

	@Override
	public int resources() {
		return 0;
	}

	@Override
	public int period() {
		return 0;
	}

	@Override
	public String functionName() {
		return "or";
	}

	@Override
	public String entityName() {
		return "orLogic";
	}
	
	@Override
	public String author() {
		return "Daniel Molla";
	}

	@Override
	public int version() {
		return 1;
	}

	@Override
	public String description() {
		return "A simple or logic";
	}

	@Override
	public ArrayList<String> dependentFiles() {
		ArrayList<String> files = new ArrayList<String>();
		return files;
	}
}
