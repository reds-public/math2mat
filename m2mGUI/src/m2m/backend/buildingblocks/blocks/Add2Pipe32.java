/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks.blocks;

import java.util.ArrayList;

import m2m.backend.buildingblocks.*;

public class Add2Pipe32 extends BuildingBlockFloat32 {

	@Override
	public String vhdlFileName() {
		return "add.vhd";
	}

	@Override
	public ImplType implType() {
		return ImplType.PIPELINE;
	}

	@Override
	public int latencyTime() {
		return 7;
	}

	@Override
	public int cycleTime() {
		return 1;
	}

	@Override
	public int resources() {
		return 122;
	}

	@Override
	public int period() {
		return 10 / (getSignificandSize() + getExponentSize());
	}

	@Override
	public String functionName() {
		return "add";
	}

	@Override
	public String entityName() {
		return "add2_pipe_32";
	}
	
	@Override
	public String author() {
		return "Samuel Tache";
	}

	@Override
	public int version() {
		return 1;
	}

	@Override
	public String description() {
		return "A simple pipeline adder";
	}

	@Override
	public ArrayList<String> dependentFiles() {
		ArrayList<String> files = new ArrayList<String>();
		files.add("add2.vhd");
		return files;
	}
}

