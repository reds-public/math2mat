/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package m2m.backend.buildingblocks;

public abstract class BuildingBlockLogic extends BuildingBlock {
	/**
	 * Size of the exponent
	 */
	protected int m_exponentSize;
	/**
	 * Size of the significand
	 */
	protected int m_significandSize;
	/**
	 * Numeric type
	 */
	protected NumType numType;
	
	/**
	 * Default constructor. It sets the significand size to 24 and the exponent
	 * size to 7.
	 */
	public BuildingBlockLogic() {
		m_significandSize = 24;
		m_exponentSize = 7;
		numType = NumType.FLOAT32;
	}

	/**
	 * Returns the number of bits used to represent the exponent.
	 * @return the number of bits of the exponent
	 */
	public int getExponentSize() {
		return m_exponentSize;
	}

	/**
	 * Returns the number of decimal bits used to represent the significand.
	 * @return the number of bits of the significand
	 */
	public int getSignificandSize() {
		return m_significandSize;
	}


	/** Returns the numbers type (NumType.FLOAT32).
	 * @return NumType.FLOAT32
	 */
	public NumType numType() {
		return numType;
	}
	
	/**
	 * Set the number of bits used to represent the exponent.
	 * @param value the number of bits of the exponent
	 */
	public void setExponentSize(int value) {
		m_exponentSize = value;
	}

	/**
	 * Set the number of decimal bits used to represent the significand.
	 * @param value the number of bits of the significand
	 */
	public void setSignificandSize(int value) {
		m_significandSize = value;
	}


	/** Set the numbers type (NumType.FLOAT32).
	 * @param type the numbers type
	 */
	public void setNumType(NumType type) {
		numType = type;
	}
}

