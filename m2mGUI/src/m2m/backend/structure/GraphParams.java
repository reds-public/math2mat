/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *									GraphParams
 ******************************************************************************
 * @auteur			: Trolliet Gregory
 * @date				: 8 févr. 2010
 * @description	:
 ******************************************************************************/

package m2m.backend.structure;

public class GraphParams {

	private static String COLORBLUE = "lightskyblue";
	private static String COLORYELLOW = "lightgoldenrod";
	private static String COLORRED = "pink";
	private static String COLORGREEN = "palegreen";

	public static String defaultColor = COLORBLUE;
	public static String inputColor = COLORRED;
	public static String outputColor = COLORGREEN;
	public static String internalColor = COLORYELLOW;
	public static String loopIndColor = COLORYELLOW;
	public static String selColor = COLORRED;
	public static String otherwColor = COLORYELLOW;

	public static String defaultShape = "box";
	public static String inputShape = defaultShape;
	public static String outputShape = defaultShape;
	public static String internalShape = "ellipse";
	public static String selShape = defaultShape;



}
