/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Function
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description : Function
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.utils.XMLUtils;

import org.w3c.dom.*;

/**
 *	Describe a function.
 * Contains inputs, outputs, temp variables and a body.
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class Function extends Element {

	/**
	 * Function input variables
	 */
	protected Vector<Element> inputVars;
	/**
	 * Function output variables
	 */
	protected Vector<Element> outputVars;
	/**
	 * Function temp variables
	 */
	protected Vector<Element> internalVars;
	/**
	 * Function body
	 */
	protected Vector<Element> body;
	/**
	 * Maximum cost of the function
	 */
	protected int maxCost;
	/**
	 * Maximum relative cost of the function
	 */
	protected int maxRelativeCost;
	
	/**
	 * Construct an empty function
	 */
	public Function() {
		this.inputVars = new Vector<Element>();
		this.outputVars = new Vector<Element>();
		this.internalVars = new Vector<Element>();
		this.body = new Vector<Element>();
	}
	
	public void modifyNumType(BuildingBlock.NumType type) {
		for(Element e: body) {
			e.modifyNumType(type);
		}
	}
	
	public String getXmlTagName() {
		return "Function";
	}

	public boolean fromXml(org.w3c.dom.Element el) {
		insideFromXml(el,this);
		return true;
		/*
		NodeList nl = el.getElementsByTagName("Function");
		if(nl != null && nl.getLength() > 0) {
			org.w3c.dom.Element e = (org.w3c.dom.Element)nl.item(0);
			if (e!=null) {
				return insideFromXml(el);
			}
		}
		return false;
		*/
	}
	
	
	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		name=XMLUtils.getTextValue(el,"Name","");
		monitor=XMLUtils.getBoolValue(el,"Monitor",false);

		if (name.isEmpty())
			return true;
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Input")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				if (inputEl.getFirstChild()!=null) {
					Node nodeIn = inputEl.getFirstChild().getNextSibling();
					while(nodeIn!=null) {
						if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
							Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
							if (element!=null)
								inputVars.add(element);
						}
						nodeIn=nodeIn.getNextSibling();
					}
				}
			}

			if (node.getNodeName().equalsIgnoreCase("Output")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				if (inputEl.getFirstChild()!=null) {
					Node nodeIn = inputEl.getFirstChild().getNextSibling();
					while(nodeIn!=null) {
						if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
							Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
							if (element!=null)
								outputVars.add(element);
						}
						nodeIn=nodeIn.getNextSibling();
					}
				}
			}

			if (node.getNodeName().equalsIgnoreCase("Internals")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				if (inputEl.getFirstChild()!=null) {
					Node nodeIn = inputEl.getFirstChild().getNextSibling();
					while(nodeIn!=null) {
						if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
							Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
							if (element!=null)
								internalVars.add(element);
						}
						nodeIn=nodeIn.getNextSibling();
					}
				}
			}

			if (node.getNodeName().equalsIgnoreCase("Body")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				if (inputEl.getFirstChild()!=null) {
					Node nodeIn = inputEl.getFirstChild().getNextSibling();
					while(nodeIn!=null) {
						if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
							Element element=getElement((org.w3c.dom.Element)nodeIn,this);
							if (element!=null)
								body.add(element);
						}
						nodeIn=nodeIn.getNextSibling();
					}
				}
			}
			node=node.getNextSibling();
		}
		return false;
	}
	
	
	public void superInsideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		super.insideToXml(el,dom,shortRef);
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		super.insideToXml(el,dom,shortRef);
		org.w3c.dom.Element inputEl=dom.createElement("Input");
		for(Element e: inputVars) {
			inputEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(inputEl);
		org.w3c.dom.Element outputEl=dom.createElement("Output");
		for(Element e: outputVars) {
			outputEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(outputEl);
		org.w3c.dom.Element statEl=dom.createElement("Internals");
		for(Element e: internalVars) {
			statEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(statEl);
		org.w3c.dom.Element bodyEl=dom.createElement("Body");
		for(Element e: body) {
			bodyEl.appendChild(e.toXml(dom,true));
		}
		el.appendChild(bodyEl);
	}

	/**
	 * Copy entirely the function. 
	 * @return The new function.
	 */
	@Override
	public Element copy(Function newFunc) {
		Function fcopy=new Function();
		// TODO Controler! Pas "copyTo(fcopy,newFunc);" ???
		copyTo(fcopy,fcopy);
		return fcopy;
	}
	
	public void copyTo(Element to, Function newFunc) {
		Function fcopy=(Function) to;
		super.copyTo(fcopy,newFunc);

		for(Element e : inputVars)
			fcopy.inputVars.add((Element)e.copy(newFunc));
		for(Element e : outputVars)
			fcopy.outputVars.add((Element)e.copy(newFunc));
		for(Element e : internalVars)
			fcopy.internalVars.add((Element)e.copy(newFunc));
		for(Element e : body)
			fcopy.body.add((Element)e.copy(fcopy));
	}
	
	public Element findElement(String name) {
		for(Element e : inputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : outputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : internalVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : body)
			if (e.findElement(name)!=null)
				return e.findElement(name);
		return null;
	}
	
	public void modifAllNames(String prefix) {

		this.setName(prefix+this.getName());
		for(Element e : inputVars)
			e.setName(prefix+e.getName());
		for(Element e : outputVars)
			e.setName(prefix+e.getName());
		for(Element e : internalVars)
			e.setName(prefix+e.getName());
		for(Element e : body)
			e.setName(prefix+e.getName());
	}
	

	/**
	 * Add variable to input
	 * @param var Variable
	 */
	public void addInput(Element var) {
		this.inputVars.add(var);
	}

	/**
	 * Set a new input variable list
	 * @param in Variable list
	 */
	public void setInput(Vector<Element> in) {
		this.inputVars = in;
	}

	/**
	 * Get the input variable list
	 * @return Variable list
	 */
	public Vector<Element> getInput() {
		return this.inputVars;
	}

	/**
	 * Add variable to output
	 * @param var Variable
	 */
	public void addOutput(Element var) {
		this.outputVars.add(var);
	}

	/**
	 * Set a new output variable list
	 * @param out Variable list
	 */
	public void setOutput(Vector<Element> out) {
		this.outputVars = out;
	}

	/**
	 * Get the output variable list
	 * @return Variable list
	 */
	public Vector<Element> getOutput() {
		return this.outputVars;
	}

	/**
	 * Add a variable to the internal variables list
	 * @param var Variable
	 */
	public void addInternalVar(Element var) {
		this.internalVars.add(var);
	}

	/**
	 * Set a new internal variable list
	 * @param stat Variable list
	 */
	public void setInternalVars(Vector<Element> list) {
		this.internalVars = list;
	}

	/**
	 * Get the internal variable list
	 * @return List of the internal variables
	 */
	public Vector<Element> getInternalVars() {
		return this.internalVars;
	}

	/**
	 * Get the all the variables that should be monitored
	 * @return all monitored variables
	 */
	public Vector<Element> getMonitoringElement() 
	{
		/* Use to get all nodes */
		Element element;
		LinkedList<Element> tempElementList = new LinkedList<Element>();
		Iterator<Element> iterElement;
		Vector<Element> elementList = new Vector<Element>();
		
		/* Initialize the nodes list and the node iterator */
		tempElementList.addAll(getBody());
		
		for(Element e : getInternalVars())
			if(e.getMonitor() && !elementList.contains(e))
				elementList.add(e);
			
		/* Get all the node and connection inside the schema */
		iterElement = tempElementList.iterator();
		while (iterElement.hasNext()) 
		{
			element = iterElement.next();
					
			/* Check if the node has children */
			if(element instanceof Function)	
			{
				tempElementList.addAll(((Function)element).getBody());
				for(Element e : ((Function)element).getInternalVars())
					if(e.getMonitor() && !elementList.contains(e))
						elementList.add(e);
			}

			tempElementList.remove(tempElementList.indexOf(element));
			iterElement = tempElementList.iterator();	
		}
		
		return elementList;
	}
	
	
	/**
	 * Add an element to the body
	 * @param el Element to add
	 */
	public void addBody(Element el) {
		this.body.add(el);
	}

	/**
	 * Add a vector of Element to the body
	 * @param vec Vector of Element to add
	 */
	public void addBody(Vector<Element> vec) {
		this.body.addAll(vec);
	}

	/**
	 * Add an element to the top of the body
	 * @param el Element to add
	 */
	public void addBodyToTop(Element el) {
		this.body.add(0, el);
	}

	/**
	 * Set a new body element list
	 * @param body Element list
	 */
	public void setBody(Vector<Element> body) {
		this.body = body;
	}

	/**
	 * Get the body element list
	 * @return Element list
	 */
	public Vector<Element> getBody() {
		return this.body;
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[Function]\tName: " + this.name;
		s += "\n" + this.toStringParam(level + 1);
		s += "\n" + tab(level) + ">>==========<<";
		for (Element el : this.body) {
			s += "\n" + el.toString(level + 1);
		}
		return s;
	}

	/**
	 * Convert all the input, output and internal to a String
	 * @param level Indentation level
	 * @return String
	 */
	protected String toStringParam(int level) {
		String s = new String();
		boolean first = true;
		if (!this.inputVars.isEmpty()) {
			first = false;
			s += tab(level) + "Input:";
			for (Element el : this.inputVars) {
				s += "\n" + el.toString(level + 1);
			}
		}
		if (!this.outputVars.isEmpty()) {
			if (!first) {
				s += "\n";
			}
			first = false;
			s += tab(level) + "Output:";
			for (Element el : this.outputVars) {
				s += "\n" + el.toString(level + 1);
			}
		}
		if (!this.internalVars.isEmpty()) {
			if (!first) {
				s += "\n";
			}
			first = false;
			s += tab(level) + "Internal:";
			for (Element el : this.internalVars) {
				s += "\n" + el.toString(level + 1);
			}
		}
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix, String boxFormat,
			  String color) {
		String s = new String();
		int cpt = 0;
		if (prefix.isEmpty()) {
			prefix = "top";
		}
		if (color.isEmpty()) {
			color = GraphParams.defaultColor;
		}
		s += tab(level) + prefix + "[shape = " + boxFormat + ", label = \""
				  + this.getName() + "\" color=\"" + color + "\" style=\"filled\"]";
		if (!this.inputVars.isEmpty()) {
			cpt = 0;
			for (Element var : this.inputVars) {
				s += var.toSchematic(level, prefix + "_i_" + cpt,
						  GraphParams.inputShape, GraphParams.inputColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_i_" + cpt;
				cpt++;
			}
		}
		if (!this.outputVars.isEmpty()) {
			cpt = 0;
			for (Element var : this.outputVars) {
				s += var.toSchematic(level, prefix + "_o_" + cpt,
						  GraphParams.outputShape,
						  GraphParams.outputColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_o_" + cpt;
				cpt++;
			}
		}
		if (!this.internalVars.isEmpty()) {
			cpt = 0;
			for (Element var : this.internalVars) {
				s += var.toSchematic(level, prefix + "_s_" + cpt,
						  GraphParams.internalShape,
						  GraphParams.internalColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_s_" + cpt;
				cpt++;
			}
		}
		if (!this.body.isEmpty()) {
			cpt = 0;
			for (Element el : this.body) {
				s += "\n" + el.toSchematic(level, prefix + "_" + cpt, "box",
						  GraphParams.defaultColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_" + cpt;
				cpt++;
			}
		}
		return s;
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		sOctave += tab(level) + "function ";
		if (this.outputVars.size() == 1) {
			if (this.outputVars.firstElement() instanceof SimpleVariable) {
				sOctave += this.outputVars.firstElement().getName();
			}
		} else {
			sOctave += "[";
			boolean first = true;
			for (Element e : this.outputVars) {
				if (e instanceof SimpleVariable) {
					sOctave += ((first) ? "" : ", ") + e.getName();
					first = false;
				}
			}
			sOctave += "]";
		}
		sOctave += "=" + this.getName() + "(";
		boolean first = true;
		for (Element e : this.inputVars) {
			if (e instanceof SimpleVariable) {
				sOctave += ((first) ? "" : ", ") + e.getName();
				first = false;
			}
		}
		
		if(getMonitoringElement().size() != 0)
		{
			sOctave += ", i_m2m_regen)\n";
			sOctave += "global interns;\n";
		}
		else
			sOctave += ")\n";
		
		for (Element e : this.body) {
			sOctave += e.toOctave(level + 1) + "\n";
		}
		sOctave += tab(level) + "endfunction;";
		return sOctave;
	}
	
	
	/**
	 * Check if an element is an input of the function
	 * @param element the element to check
	 * @return if an element is an input of the function
	 */
	public boolean isInput(Element element)
	{
		if(element instanceof SimpleVariable){
			String trueName = StructTreatment.getTrueName(element.getName());
			for(Element e : getInput()) {
				if(e.getName().equals(trueName))
					return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Check if a string is an output name.
	 * @param name the string to check
	 * @return if a string is an output name
	 */
	public boolean containsOuputName(String name)
	{
		for(Element e : outputVars)
			if(e.getName().equals(name))
				return true;
		
		return false;
	}
	
	
	/**
	 * Check if a variable name is an input name.
	 * @param name the string to check
	 * @return if a string is an input name
	 */
	public boolean containsInputName(String name)
	{
		for(Element e : outputVars)
			if(e.getName().equals(name))
				return true;
		
		return false;
	}
	
	
	/**
	 * Set the maximum cost of the function.
	 * @param cost the maximum cost to set to the function
	 */
	public void setMaxCost(int cost) {
		maxCost = cost;
	}
	
	
	/**
	 * Get the maximum cost of the function.
	 * @return the maximum cost of the function
	 */
	public int getMaxCost(){
		return maxCost;
	}
	
	
	/**
	 * Set the maximum relative cost of the function.
	 */
	public void setRelativeMaxCost(int value) {
		maxRelativeCost = value+1;
	}
	
	
	/**
	 * Get the maximum relative cost of the function.
	 * @return the maximum relative cost of the function
	 */
	public int getRelativeMaxCost(){
		return maxRelativeCost;
	}

	
	/**
	 * Check if the Loop contains a loop
	 * @return if the Loop contains a loop
	 */
	public boolean containsLoop() 
	{
		for(Element e : body) {
			if(e instanceof Loop)
				return true;
			else if(e instanceof IfThenElse)
				if(((IfThenElse)e).containsLoop())
					return true;
		}
		return false;
	}
}
