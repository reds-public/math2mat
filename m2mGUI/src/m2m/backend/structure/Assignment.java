/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Assignment
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description : Assignment
 ******************************************************************************/
package m2m.backend.structure;

/**
 *	Describe an assignment
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class Assignment extends Operation {

	final static String OPNAME="ass";
	//final static String OPNAME="aff";
	
	public Assignment() {
		super(OPNAME);
		this.setName(OPNAME);
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		Assignment newElement=(Assignment)e;
		super.copyTo(newElement,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		Assignment newElement=new Assignment();
		this.copyTo(newElement,newFunc);
		return newElement;
	}
	
	@Override
	public String getOpSymbol() {
		return "=";
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		Element out = this.output.firstElement();
		sOctave += tab(level) + out.getName() + "\t= ";
		if (this.input.firstElement() instanceof SimpleVariable) {
			SimpleVariable var = (SimpleVariable)this.input.firstElement();
			if (var.getName().isEmpty()) {
				sOctave += var.getVal();
			} else if (var.getType().equals("const")) {
				sOctave += var.getVal();
			} else {
				sOctave += this.input.firstElement().getName();
			}
		}
		if (!monitor) {
			sOctave += ";";
		}
		if (out instanceof SimpleVariable & out.monitor) 
		{
			sOctave += "\n" + tab(level) + "for j_m2m_regen=1:length(interns)";
			sOctave += "\n" + tab(level+1) + "if(length(interns(j_m2m_regen).sname) == length(\"" + out.getName() +"\") && interns(j_m2m_regen).sname == \"" + out.getName() + "\")";
			sOctave += "\n" + tab(level+2) + "interns(j_m2m_regen).values(i_m2m_regen,:) = " + out.getName() + ";";
			sOctave += "\n" + tab(level+1) + "endif";
		    sOctave += "\n" + tab(level) + "endfor"; 
		}
		return sOctave;
	}
}
