/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *	Power
 *
 *	Gregory Trolliet
 * 2 nov. 2009
 */

package m2m.backend.structure;


public class Power extends Operation {

	final static String OPNAME="power";
	
	public Power() {
		super(OPNAME);
		this.setName("pow");
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		Power pow=(Power)e;
		super.copyTo(pow,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		Power pow=new Power();
		this.copyTo(pow,newFunc);
		return pow;
	}
	
	@Override
	public String getOpSymbol() {
		return "^";
	}

	@Override
	public String toOctave(int level) {
		return super.toOctave(level, "**");
	}

}
