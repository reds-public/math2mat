/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package m2m.backend.structure;

import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *
 * @author raghnarok
 */
public class Multiplexer extends Operation {

	private int size;
	private Variable sel;

	public Multiplexer() {
		super("mux");
	}

	public String getXmlTagName() {
		return "Multiplexer";
	}


	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		size=XMLUtils.getIntValue(el,"SizeMux",0);
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Sel")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							sel=(Variable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}
			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		// This function replaces the one of Function, so we call Element instead
		// of function
//		superInsideToXml(el,dom,shortRef);
		super.insideToXml(el, dom, shortRef);
		el.appendChild(XMLUtils.createIntElement(dom,"SizeMux",size));
		
		if (sel!=null) {
			org.w3c.dom.Element selEl=dom.createElement("Sel");
			selEl.appendChild(sel.toXml(dom,true));
			el.appendChild(selEl);
		}
	}
	
	@Override
	public void copyTo(Element e,Function newFunc) {
		Multiplexer mult=(Multiplexer)e;
		mult.size=this.size;
		mult.sel=(Variable)this.sel.copy(newFunc);
		super.copyTo(mult,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		Multiplexer mult=new Multiplexer();
		this.copyTo(mult,newFunc);
		return mult;
	}

	public void setSize(int s) {
		this.size = s;
	}

	public int getSize() {
		return this.size;
	}

	public void setSel(Variable var) {
		this.sel = var;
	}

	public Variable getSel() {
		return this.sel;
	}


	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[" + this.opName + "]\tName: " + this.name;
		s += "\n" + tab(level + 1) + "Size: " + this.size;
		s += "\n" + tab(level + 1) + "Select: ";
		if (this.sel != null) {
			s += "\n" + this.sel.toString(level + 2);
		}
		s += "\n" + this.toStringParam(level + 1);
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		if (prefix.isEmpty()) {
			prefix = "top";
		}
		int cpt = 0;
		s += tab(level) + prefix + "[shape = \"" + boxFormat + "\", label = \""
				  + ((this.generic) ? "fun_" : "") + this.opName + " : "
				  + this.getName() + "\" color=\"" + color + "\" style=\"filled\"]";
		if (this.sel != null) {
			s += this.sel.toSchematic(level, prefix + "_sel_" + cpt,
					  GraphParams.selShape, GraphParams.selColor);
			s += "\n" + tab(level) + prefix + " -> " + prefix + "_sel_" + cpt + " [label = \"Sel\"]";
			cpt++;
		}
		if (!this.input.isEmpty()) {
			cpt = 0;
			boolean first = true;
			for (Element var : this.input) {
				if (var instanceof Variable) {
					s += var.toSchematic(level, prefix + "_in_" + cpt,
							  GraphParams.inputShape,
							  GraphParams.inputColor);
				} else {
					s += var.toSchematic(level, prefix + "_in_" + cpt,
							  boxFormat, GraphParams.defaultColor);
				}
				if (first) {
					s += "\n" + tab(level) + prefix + " -> " + prefix + "_in_" + cpt;
					first = false;
				} else {
					s += "\n" + tab(level) + prefix + "_in_" + (cpt - 1) + " -> " + prefix + "_in_" + cpt;
					first = false;
				}
				cpt++;
			}
		}
		if (!this.output.isEmpty()) {
			cpt = 0;
			boolean first = true;
			for (Element var : this.output) {
				if (var instanceof Variable) {
					s += var.toSchematic(level, prefix + "_out_" + cpt,
							  GraphParams.outputShape,
							  GraphParams.outputColor);
				} else {
					s += var.toSchematic(level, prefix + "_out_" + cpt,
							  boxFormat, GraphParams.defaultColor);
				}
				if (first) {
					s += "\n" + tab(level) + prefix + " -> " + prefix + "_out_" + cpt;
					first = false;
				} else {
					s += "\n" + tab(level) + prefix + "_out_" + (cpt - 1) + " -> " + prefix + "_out_" + cpt;
					first = false;
				}
				cpt++;
			}
		}
		return s;
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		Element out = this.output.firstElement();
		sOctave += tab(level) + "switch " + this.sel.getName() + "\n";
		for (int i=0; i<this.size-1; i++) {
			sOctave += tab(level+1) + "case " + i + "\n";
			sOctave += tab(level+2) + out.getName() + "=" + this.input.elementAt(i).getName();
			if (!monitor) {
				sOctave += ";";
			}
			if (out instanceof SimpleVariable & out.monitor) {	
				sOctave += "\n" + tab(level+2) + "for j_m2m_regen=1:length(interns)";
				sOctave += "\n" + tab(level+3) + "if(length(interns(j_m2m_regen).sname) == length(\"" + out.getName() +"\") && interns(j_m2m_regen).sname == \"" + out.getName() + "\")";
				sOctave += "\n" + tab(level+4) + "interns(j_m2m_regen).values(i_m2m_regen,:) = " + out.getName() + ";";
				sOctave += "\n" + tab(level+3) + "endif";
			    sOctave += "\n" + tab(level+2) + "endfor\n"; 
			}
		}
		sOctave += tab(level+1) + "otherwise";
		sOctave += "\n" + tab(level+2) + out.getName() + "=" + this.input.elementAt(size-1).getName();
		if (!monitor) {
			sOctave += ";";
		}
		if (out instanceof SimpleVariable & out.monitor) {	
			sOctave += "\n" + tab(level+2) + "for j_m2m_regen=1:length(interns)";
			sOctave += "\n" + tab(level+3) + "if(length(interns(j_m2m_regen).sname) == length(\"" + out.getName() +"\") && interns(j_m2m_regen).sname == \"" + out.getName() + "\")";
			sOctave += "\n" + tab(level+4) + "interns(j_m2m_regen).values(i_m2m_regen,:) = " + out.getName() + ";";
			sOctave += "\n" + tab(level+3) + "endif";
		    sOctave += "\n" + tab(level+2) + "endfor\n"; 
		}
		sOctave += tab(level) + "end";
		return sOctave;
	}
}
