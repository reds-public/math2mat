/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											LoopFor
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import m2m.backend.buildingblocks.BuildingBlock;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *	Describe a loopFor.
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class LoopFor extends Loop {

	/**
	 * Start of loop interval
	 */
	private SimpleVariable start;
	/**
	 * Increment of loop interval
	 */
	private SimpleVariable incr;
	/**
	 * End of loop interval
	 */
	private SimpleVariable end;
	/**
	 * Iterator operation
	 */
	private Operation iterOperation;
	

	/**
	 * Construct an empty loop (1:1:1)
	 */
	public LoopFor() {
		this.setName("loopfor");
		this.start = new SimpleVariable("", 1.0);
		this.incr = new SimpleVariable("", 1.0);
		this.end = new SimpleVariable("", 1.0);
	}
	
	public String getXmlTagName() {
		return "LoopFor";
	}


	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Start")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,this);
						if (element!=null)
							start=(SimpleVariable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("Incr")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,this);
						if (element!=null)
							incr=(SimpleVariable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("End")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,this);
						if (element!=null)
							end=(SimpleVariable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("IterOp")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,this);
						if (element!=null)
							iterOperation=(Operation)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		// This function replaces the one of Function, so we call Element instead
		// of function
//		superInsideToXml(el,dom,shortRef);
		super.insideToXml(el, dom, shortRef);
		
		org.w3c.dom.Element startEl=dom.createElement("Start");
		startEl.appendChild(start.toXml(dom,false));
		el.appendChild(startEl);
		org.w3c.dom.Element incrEl=dom.createElement("Incr");
		incrEl.appendChild(incr.toXml(dom,false));
		el.appendChild(incrEl);
		org.w3c.dom.Element endEl=dom.createElement("End");
		endEl.appendChild(end.toXml(dom,false));
		el.appendChild(endEl);
		org.w3c.dom.Element iterEl=dom.createElement("IterOp");
		iterEl.appendChild(iterOperation.toXml(dom,false));
		el.appendChild(iterEl);
	}
	

	/**
	 * Copy entirely the for loop. 
	 * @return The new for loop.
	 */
	@Override
	public Element copy(Function newFunc) {
		LoopFor newLoop=new LoopFor();
		newLoop.start = (SimpleVariable)this.start.copy(newFunc);
		newLoop.incr = (SimpleVariable)this.incr.copy(newFunc);
		newLoop.end = (SimpleVariable)this.end.copy(newFunc);
		super.copyTo(newLoop,newFunc);
		newLoop.iterOperation = (Operation)iterOperation.copy(newLoop);
		return newLoop;
	}

	
	public Element findElement(String name) {
		for(Element e : inputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : outputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : internalVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : body)
			if (e.findElement(name)!=null)
				return e.findElement(name);
		if(start.findElement(name)!= null)
			return start.findElement(name);
		if(incr.findElement(name)!= null)
			return incr.findElement(name);
		if(end.findElement(name)!= null)
			return end.findElement(name);
		return null;
	}
	

	/**
	 * Setter for start
	 * @param start New start
	 */
	public void setStart(SimpleVariable start) {
		this.start = start;
	}

	/**
	 * Getter for start
	 * @return Start
	 */
	public SimpleVariable getStart() {
		return this.start;
	}

	/**
	 * Setter for increment
	 * @param incr New increment
	 */
	public void setIncr(SimpleVariable incr) {
		this.incr = incr;
	}

	/**
	 * Getter for increment
	 * @return Increment
	 */
	public SimpleVariable getIncr() {
		return this.incr;
	}

	/**
	 * Setter for end
	 * @param end New end
	 */
	public void setEnd(SimpleVariable end) {
		this.end = end;
	}

	/**
	 * Getter for end
	 * @return End
	 */
	public SimpleVariable getEnd() {
		return this.end;
	}

	/**
	 * Set the iterator operation of the loop for.
	 * @param op the iterator operation to set to the loop for
	 */
	public void setIterOperation(Operation op) {
		iterOperation = op;
	}
	
	/**
	 * Get the iterator operation of the loop for.
	 * @return the iterator operation of the loop for
	 */
	public Operation getIterOperation() {
		return iterOperation;	
	}
	
	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[LoopFor]\tName: " + this.name;
		s += "\tInterval: ";
		if (this.start.getType().equals("const")) {
			s += this.start.getVal();
		} else {
			s += this.start.getName();
		}
		s += " -> ";
		if (this.incr.getType().equals("const")) {
			s += this.incr.getVal();
		} else {
			s += this.incr.getName();
		}
		s += " -> ";
		if (this.end.getType().equals("const")) {
			s += this.end.getVal();
		} else {
			s += this.end.getName();
		}
		s += "\n" + this.toStringParam(level + 1);
		s += "\n" + tab(level) + ">>==========<<";
		for (Element el : this.body) {
			s += "\n" + el.toString(level + 1);
		}
		return s;
	}


	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		s += super.toSchematic(level, prefix, "box", color);
		s += this.start.toSchematic(level, prefix + "_start", "box", GraphParams.loopIndColor);
		s += "\n" + tab(level) + prefix + " -> " + prefix + "_start [label = \"start\"]";
		s += this.incr.toSchematic(level, prefix + "_incr", "box", GraphParams.loopIndColor);
		s += "\n" + tab(level) + prefix + " -> " + prefix + "_incr [label = \"incr\"]";
		s += this.end.toSchematic(level, prefix + "_end", "box", GraphParams.loopIndColor);
		s += "\n" + tab(level) + prefix + " -> " + prefix + "_end [label = \"end\"]";
		return s;
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		sOctave += tab(level) + "for ";
		sOctave += this.internalVars.firstElement().getName() + "=";
		sOctave += ((this.start.getType().equals("const")) ? this.start.getVal() : this.start.getName());
		sOctave += ":" + ((this.incr.getType().equals("const")) ? this.incr.getVal() : this.incr.getName());
		sOctave += ":" + ((this.end.getType().equals("const")) ? this.end.getVal() : this.end.getName()) + "\n";
		/*if (this.output.size() == 1) {
		if (this.output.firstElement() instanceof SimpleVariable) {
		sOctave += this.output.firstElement().getName();
		}
		} else {
		sOctave += "[";
		boolean first = true;
		for (Element e : this.output) {
		if (e instanceof SimpleVariable) {
		sOctave += ((first) ? "" : ", ") + e.getName();
		first = false;
		}
		}
		sOctave += "]";
		}
		sOctave += "=" + this.getName() + "(";
		boolean first = true;
		for (Element e : this.input) {
		if (e instanceof SimpleVariable) {
		sOctave += ((first) ? "" : ", ") + e.getName();
		first = false;
		}
		}
		sOctave += ")\n";*/
		for(Element e : body) {
			if(e instanceof IfThenElse && e.getName().contains("loopfor")) {
				SimpleVariable varIn   = ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyTrue().firstElement()).getInputAt(0)); 
				SimpleVariable varOut  = ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyTrue().firstElement()).getOutputAt(0)); 
				SimpleVariable varloop = ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getInputAt(0)); 
				sOctave += tab(level+1)   + "if (" + this.internalVars.firstElement().getName() + " == " + ((this.start.getType().equals("const")) ? this.start.getVal() : this.start.getName()) + ")\n";
				sOctave += tab(level+2) + varOut.getName() + " = " + varIn.getName() + ";\n";
				sOctave += tab(level+1)   + "else\n";
				sOctave += tab(level+2) + varOut.getName() + " = " + varloop.getName() + ";\n";
				sOctave += tab(level+1)   + "endif;";
			}
			sOctave += e.toOctave(level + 1) + "\n";
		}
		sOctave += tab(level) + "endfor;";
		return sOctave;
	}
	
	
	/**
	 * 
	 */
	public void modifyNumType(BuildingBlock.NumType type) {
		iterOperation.modifyNumType(type);
		super.modifyNumType(type);
	}
}
