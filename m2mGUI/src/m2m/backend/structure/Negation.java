/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Negation
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description :
 ******************************************************************************/

package m2m.backend.structure;

/**
 *	Describe a negation
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class Negation extends Operation {

	final static String OPNAME="neg";
	
	public Negation() {
		super(OPNAME);
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		Negation newElement=(Negation)e;
		super.copyTo(newElement,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		Negation newElement=new Negation();
		this.copyTo(newElement,newFunc);
		return newElement;
	}
	
	@Override
	public String getOpSymbol() {
		return "-";
	}
	
	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		sOctave += tab(level) + this.output.firstElement().getName() + "\t= -" + this.input.firstElement().getName();
		if (!monitor) {
			sOctave += ";";
		}
		return sOctave;
	}
	
}
