/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *	VectorVariable
 *
 *	Gregory Trolliet
 * 6 nov. 2009
 */
package m2m.backend.structure;

import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class VectorVariable extends Variable {
	

	private Variable var;
	private int indice;

	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		indice=XMLUtils.getIntValue(el,"Indice",0);
		

		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("InntVar")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							var=(Variable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}
		}
		if (name.isEmpty())
			return false;
		return true;
	}
	
	public String getXmlTagName() {
		return "VectorVariable";
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		if (shortRef)
			el.appendChild(XMLUtils.createTextElement(dom,"Name",name));
		else {
			super.insideToXml(el,dom,shortRef);
			el.appendChild(XMLUtils.createIntElement(dom,"Indice",indice));
			org.w3c.dom.Element v=dom.createElement("IntVar");
			v.appendChild(var.toXml(dom,shortRef));
			el.appendChild(v);
		}
	}
	
	@Override
	public Element copy(Function newFunc) {
		VectorVariable newVect=new VectorVariable();
		newVect.var = (Variable)var.copy(newFunc);
		newVect.indice = this.indice;
		return newVect;
	}


	public VectorVariable() {
		this.var = null;
		this.indice = 0;
	}

	public VectorVariable(Variable var, int i) {
		this.var = var;
		this.indice = i;
	}

	public void setVar(Variable var) {
		this.var = var;
	}

	public Variable getVar() {
		return this.var;
	}

	public void setInd(int i) {
		this.indice = i;
	}

	public int getInd() {
		return this.indice;
	}

	public void setVal(double val) {
		this.var.setVal(val);
	}

	public double getVal() {
		return this.var.getVal();
	}

	public void setType(String t) {
		this.var.setType(t);
	}

	public String getType() {
		return this.var.getType();
	}

	public int getSize() {
		return 1;
	}

	@Override
	public String getName() {
		return this.var.getName();
	}

	@Override
	public void setName(String name) {
		this.var.setName(name);
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += this.tab(level) + "[VectorVariable]\tIndice: " + this.indice + "\n";
		s += this.var.toString(level + 1);
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		if (prefix.isEmpty()) {
			prefix = "var";
		}
		if (this.getName().isEmpty()) {
			s += "\n" + tab(level) + prefix + "[shape=\"" + boxFormat + "\" label = \"" + this.getVal() + "\" color=\"" + color + "\" style=\"filled\"]";
		} else {
			s += "\n" + tab(level) + prefix + "[shape=\"" + boxFormat + "\" label = \"" + this.getName() + "[" + this.indice + "]" + "\" color=\"" + color + "\" style=\"filled\"]";
		}
		return s;
	}
}
