/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Case
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 5 janv. 2010
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *	Represent one case for a switch operation
 * @author Trolliet Gregory
 */
public class Case extends Element {

	/**
	 * All the values accepted for this case
	 */
	private Vector<Variable> cond;
	/**
	 * Code to execute for this case
	 */
	private Vector<Element> body;

	/**
	 * Builder
	 */
	public Case() {
		this.cond = null;
		this.body = null;
	}
	

	public void modifyNumType(BuildingBlock.NumType type) {
		super.modifyNumType(type);
		for(Element e: cond) {
			e.modifyNumType(type);
		}
		for(Element e: body) {
			e.modifyNumType(type);
		}
	}

	public String getXmlTagName() {
		return "Case";
	}

	/**
	 * Copy entirely the function. 
	 * @return The new function.
	 */
	@Override
	public Element copy(Function newFunc) {
		Case ccopy=new Case();
		copyTo(ccopy,newFunc);
		return ccopy;
	}
	
	public void copyTo(Element to, Function newFunc) {
		Case ccopy=(Case) to;
		super.copyTo(ccopy,newFunc);

		for(Variable e : cond)
			ccopy.cond.add((Variable)e.copy(newFunc));
		for(Element e : body)
			ccopy.body.add((Element)e.copy(newFunc));
	}
	
	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		name=XMLUtils.getTextValue(el,"Name","");
		monitor=XMLUtils.getBoolValue(el,"Monitor",false);

		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Cond")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							cond.add((Variable)element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("Body")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							body.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		super.insideToXml(el,dom,shortRef);
		org.w3c.dom.Element condEl=dom.createElement("Cond");
		for(Element e: cond) {
			condEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(condEl);
		org.w3c.dom.Element bodyEl=dom.createElement("Body");
		for(Element e: body) {
			bodyEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(bodyEl);
	}


	/**
	 * Set accepted values
	 * @param cond Accepted values
	 */
	public void setCond(Vector<Variable> cond) {
		this.cond = cond;
	}

	/**
	 * Get accepted values
	 * @return Accepted values
	 */
	public Vector<Variable> getCond() {
		return this.cond;
	}

	/**
	 * Set the body
	 * @param body Body
	 */
	public void setBody(Vector<Element> body) {
		this.body = body;
	}

	/**
	 * Get the body
	 * @return Body
	 */
	public Vector<Element> getBody() {
		return this.body;
	}

	/**
	 * Convert the object to a string that contains all the parametres
	 * @param level Indent of the string
	 * @return String
	 */
	public String toString(int level) {
		String s = new String();
		s += tab(level) + ">>===COND===<<";
		if (this.cond == null) {
			s += "\n" + tab(level + 1) + "Otherwise";
		} else {
			for (Element el : this.cond) {
				s += "\n" + el.toString(level + 1);
			}
		}
		s += "\n" + tab(level) + ">>===BODY===<<";
		if (this.body != null) {
			for (Element el : this.body) {
				s += "\n" + el.toString(level + 1);
			}
		}
		return s;
	}


	/**
	 * Create the schematic file, use with graphviz to generate schematic
	 * @param level Level of tab
	 * @param prefix Prefix for the element name
	 * @param boxFormat Format of box (rectangle, triangle, ...)
	 * @param color Color of the block
	 * @return Generate string
	 */
	public String toSchematic(int level, String prefix,
			  String boxFormat, String color) {
		String s = new String();
		if (this.cond == null || this.cond.isEmpty()) {
			s += tab(level) + prefix + "[shape = " + boxFormat
					  + ", label = \"Otherwise\" color=\""
					  + GraphParams.otherwColor + "\" style=\"filled\"]";
		} else {
			String list = new String();
			boolean first = true;
			for (Variable var : this.cond) {
				if (!first) {
					list += ", ";
				}
				if (var.getName().isEmpty()) {
					list += var.getVal();
				} else {
					list += var.getName();
				}
				first = false;
			}
			s += tab(level) + prefix + "[shape = " + boxFormat + ", label = \""
					  + list + "\" color=\"" + GraphParams.otherwColor
					  + "\" style=\"filled\"]";
		}
		if (!this.body.isEmpty()) {
			int cpt = 0;
			for (Element el : this.body) {
				s += "\n" + el.toSchematic(level, prefix + "_" + cpt, "box",
						  GraphParams.defaultColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_" + cpt;
				cpt++;
			}
		}
		return s;
	}

	
	/**
	 * Create a string that contains a number of tab
	 * @param nb Tab number
	 * @return String
	 */
	/*
	private String tab(int nb) {
		String sTab = new String();
		for (int i = 0; i < nb; i++) {
			sTab += "\t";
		}
		return sTab;
	}
	*/
}
