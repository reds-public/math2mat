/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *										GenericOperation
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 12 novembre 2009
 * Description : Generic operation
 ******************************************************************************/
package m2m.backend.structure;


public class GenericOperation extends Operation {
	
	public String getXmlTagName() {
		return "GenericOperation";
	}
	
	public GenericOperation() {
		super("",true);
	}
	public GenericOperation(String name) {
		super(name, true);
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		GenericOperation newElement=(GenericOperation)e;
		super.copyTo(newElement,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		GenericOperation newElement=new GenericOperation(this.name);
		this.copyTo(newElement,newFunc);
		return newElement;
	}
	
	@Override
	public String toOctave(int level) {
		String s = new String();
		s += tab(level);
		if (this.output != null & !this.output.isEmpty()) {
			if (this.output.firstElement() instanceof Variable
					  & !this.output.firstElement().getName().isEmpty()) {

				s += this.output.firstElement().getName();
			}
		}
		s += " = " + this.opName + "(";
		boolean first = true;
		for (Element e : this.input) {
			if (first) {
				first = false;
			} else {
				s += ", ";
			}
			if (e instanceof Variable) {
				if (e.getName().isEmpty()) {
					s += ((Variable) e).getVal();
				} else {
					s += e.getName();
				}
			}
		}
		s += ")" + ((this.monitor) ? "" : ";");
		return s;
	}
}
