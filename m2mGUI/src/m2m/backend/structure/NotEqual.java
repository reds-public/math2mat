/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											NotEqual
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 14 avr. 2009
 * Description :
 ******************************************************************************/

package m2m.backend.structure;

/**
 *	Describe a logical not equal
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class NotEqual extends Comparison {
	
	public NotEqual() {
		super("neq");
		this.setName("notequal");
	}

	@Override
	public String getOpSymbol() {
		return "!=";
	}

	@Override
	public String toOctave(int level) {
		return super.toOctave(level, "!=");
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		NotEqual newElement=(NotEqual)e;
		super.copyTo(newElement,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		NotEqual newElement=new NotEqual();
		this.copyTo(newElement,newFunc);
		return newElement;
	}
}
