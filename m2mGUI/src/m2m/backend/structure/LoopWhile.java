/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											LoopFor
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *	Describe a loop for.
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class LoopWhile extends Loop {

	/**
	 * Conditional test
	 */
	private Vector<Element> cond;

	/**
	 * Construct an empty loop (1:1:1)
	 */
	public LoopWhile() {
		this.setName("loopwhile");
		this.cond = new Vector<Element>();
	}
	

	public String getXmlTagName() {
		return "LoopWhile";
	}


	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Cond")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,this);
						if (element!=null)
							cond.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}
			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		// This function replaces the one of Function, so we call Element instead
		// of function
        // superInsideToXml(el,dom,shortRef);
		super.insideToXml(el, dom, shortRef);

		org.w3c.dom.Element condEl=dom.createElement("Cond");
		for(Element e: cond) {
			condEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(condEl);
	}
	
	
	/**
	 * Copy entirely the while loop. 
	 * @return The new while loop.
	 */
	@Override
	public Element copy(Function newFunc) {
		LoopWhile newLoop=new LoopWhile();
		super.copyTo(newLoop, newFunc);
		for(Element e : cond)
			newLoop.cond.add((Element)e.copy(newLoop));	
		return newLoop;
	}

	
	/**
	 * Set the conditional test element list
	 * @param cond Element list
	 */
	public void setCond(Vector<Element> cond) {
		this.cond = cond;
	}

	/**
	 * Get the conditional test element list
	 * @return Element list
	 */
	public Vector<Element> getCond() {
		return this.cond;
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[LoopWhile]\tName: " + this.name;
		s += "\n" + this.toStringParam(level + 1);
		s += "\n" + tab(level) + ">>===COND===<<";
		for (Element el : this.cond) {
			s += "\n" + el.toString(level + 1);
		}
		s += "\n" + tab(level) + ">>==========<<";
		for (Element el : this.body) {
			s += "\n" + el.toString(level + 1);
		}
		return s;
	}


	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		int cpt = 0;
		s += super.toSchematic(level, prefix, "box", color);
		if (!this.cond.isEmpty()) {
			cpt = 0;
			for (Element el : this.cond) {
				s += "\n" + el.toSchematic(level, prefix + "_cond_"
						  + cpt, "box", GraphParams.defaultColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_cond_" + cpt;
				cpt++;
			}
		}
		return s;
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		sOctave += tab(level) + "while " + "";//this.internalVars.firstElement().getName() + "=";
		sOctave += "\n";
		for (Element e : this.body) {
			sOctave += e.toOctave(level + 1) + "\n";
		}
		/*
		sOctave += tab(level + 1) + "if ("
		+ ((Variable) this.internalVars.firstElement()).getName() + " == "
		+ ((this.start.getName().isEmpty()) ? ((Variable) this.start).getVal() : this.start.getName())
		+ ")\n";
		sOctave += */
		sOctave += tab(level) + "end";
		return sOctave;
	}
}
