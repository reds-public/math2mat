/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file Comparison.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 25 mai 2010
 *
 * Author: Sebastien Masle
 *
 * Description: this class groups all comparison operations together.
 * 
 */
package m2m.backend.structure;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.buildingblocks.BuildingBlockLogic;

/**
 * @author masles
 *
 */
public abstract class Comparison extends Operation {

	public Comparison(String opName) {
		super(opName);
	}
	
	@Override
	public void copyTo(Element e,Function newFunc) {
		Comparison newElement=(Comparison)e;
		super.copyTo(newElement,newFunc);
	}
/*	
	@Override
	public Element copy(Function newFunc) {
		Comparison newElement=new Comparison(this.opName);
		this.copyTo(newElement,newFunc);
		return newElement;
	}
	*/
	
	/**
	 * Generate the octave code corresponding to the operation, the operator is
	 * set by the parameter op.
	 * @param level Tabulation level
	 * @param op Operator
	 * @return String
	 */
	public String toOctave(int level, String op) {
		String sOctave = new String();
		Element out = this.output.firstElement();
		sOctave += tab(level) + out.getName() + "\t= ";
		boolean first = true;
		for (Element e : this.input) {
			if (e instanceof SimpleVariable) {
				sOctave += ((first) ? "" : " " + op + " ")
						  + ((((SimpleVariable)e).getType().equals("const")) ? ((SimpleVariable) e).getVal() : e.getName());
			}
			first = false;
		}
		/// If octave need to display the resultat of the operation
		if (!monitor) {
			sOctave += ";";
		}
		if (out instanceof SimpleVariable & out.monitor) 
		{	
			sOctave += "\n" + tab(level) + "for j_m2m_regen=1:length(interns)";
			sOctave += "\n" + tab(level+1) + "if(length(interns(j_m2m_regen).sname) == length(\"" + out.getName() +"\") && interns(j_m2m_regen).sname == \"" + out.getName() + "\")";
			sOctave += "\n" + tab(level+2) + "if(" + out.getName() + ")"; 
			if(block.numType() == NumType.FLOAT32)
				sOctave += "\n" + tab(level+3) + "interns(j_m2m_regen).values(i_m2m_regen,:) = 1.4013*10^-45;";
			else
				sOctave += "\n" + tab(level+3) + "interns(j_m2m_regen).values(i_m2m_regen,:) = 5.0e-324;";
			sOctave += "\n" + tab(level+2) + "else";
			sOctave += "\n" + tab(level+3) + "interns(j_m2m_regen).values(i_m2m_regen,:) = 0.0;";
			sOctave += "\n" + tab(level+2) + "endif";
			sOctave += "\n" + tab(level+1) + "endif";
		    sOctave += "\n" + tab(level) + "endfor"; 
		}
		return sOctave;
	}
	
	public void modifyNumType(BuildingBlock.NumType type) {
		((BuildingBlockLogic) block).setNumType(type);
	}	
}
