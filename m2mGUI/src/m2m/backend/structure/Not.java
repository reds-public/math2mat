/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											NotEqual
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 14 avr. 2009
 * Description :
 ******************************************************************************/

package m2m.backend.structure;

/**
 *	Describe a logical not equal
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class Not extends LogicOperation {

	final static String OPNAME="not";
	
	public Not() {
		super(OPNAME);
		this.setName(OPNAME);
	}
	
	@Override
	public String getOpSymbol() {
		return "!";
	}
	
}
