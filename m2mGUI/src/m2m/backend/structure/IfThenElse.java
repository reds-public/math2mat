/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											IfThenElse
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 7 avr. 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *	Describe a conditional structure if/then/else
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class IfThenElse extends Function {

	/**
	 * Conditional test
	 */
	private Vector<Element> cond;
	/**
	 * Body if test is true
	 */
	private Vector<Element> bodyTrue;
	/**
	 * Body if test is false
	 */
	private Vector<Element> bodyFalse;

	/**
	 * Constructor empty
	 */
	public IfThenElse() {
		this.setName("ifthen");
		this.cond = new Vector<Element>();
		this.bodyTrue = new Vector<Element>();
		this.bodyFalse = new Vector<Element>();
	}

	public void modifyNumType(BuildingBlock.NumType type) {
		super.modifyNumType(type);
		for(Element e: cond) {
			e.modifyNumType(type);
		}
		for(Element e: bodyTrue) {
			e.modifyNumType(type);
		}
		for(Element e: bodyFalse) {
			e.modifyNumType(type);
		}
	}

	public String getXmlTagName() {
		return "IfThenElse";
	}

	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		name=XMLUtils.getTextValue(el,"Name","");
		monitor=XMLUtils.getBoolValue(el,"Monitor",false);

		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Cond")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							cond.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("BodyTrue")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							bodyTrue.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("BodyFalse")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				if (inputEl!=null)
					if (inputEl.getFirstChild()!=null) {
						Node nodeIn = inputEl.getFirstChild().getNextSibling();
						while(nodeIn!=null) {
							if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
								Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
								if (element!=null)
									bodyFalse.add(element);
							}
							nodeIn=nodeIn.getNextSibling();
						}
					}
				
			}

			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		super.insideToXml(el,dom,shortRef);
		org.w3c.dom.Element condEl=dom.createElement("Cond");
		for(Element e: cond) {
			condEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(condEl);
		org.w3c.dom.Element bodyTEl=dom.createElement("BodyTrue");
		for(Element e: bodyTrue) {
			bodyTEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(bodyTEl);
		org.w3c.dom.Element bodyFEl=dom.createElement("BodyFalse");
		for(Element e: bodyFalse) {
			bodyFEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(bodyFEl);
	}



	/**
	 * Copy entirely the function. 
	 * @return The new function.
	 */
	@Override
	public Element copy(Function newFunc) {
		IfThenElse newIf=new IfThenElse();
		super.copyTo(newIf,newFunc);
		
		for(Element e : cond)
			newIf.cond.add((Element)e.copy(newFunc));
		for(Element e : bodyTrue)
			newIf.bodyTrue.add((Element)e.copy(newFunc));
		for(Element e : bodyFalse)
			newIf.bodyFalse.add((Element)e.copy(newFunc));
		return newIf;
	}

	/**
	 * Set the true body element list
	 * @param body Element list
	 */
	public void setBodyTrue(Vector<Element> body) {
		this.bodyTrue = body;
	}

	/**
	 * Get the true body element list
	 * @return Element list
	 */
	public Vector<Element> getBodyTrue() {
		return this.bodyTrue;
	}

	/**
	 * Set the false body element list
	 * @param body Element list
	 */
	public void setBodyFalse(Vector<Element> body) {
		this.bodyFalse = body;
	}

	/**
	 * Get the false body element list
	 * @return Element list
	 */
	public Vector<Element> getBodyFalse() {
		return this.bodyFalse;
	}

	/**
	 * Set the conditional test element list
	 * @param cond Element list
	 */
	public void setCond(Vector<Element> cond) {
		this.cond = cond;
	}

	/**
	 * Get the conditional test element list
	 * @return Element list
	 */
	public Vector<Element> getCond() {
		return this.cond;
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[IfThenElse]\tName: " + this.name;
		s += "\n" + this.toStringParam(level+1);
		s += "\n" + tab(level) + ">>===COND===<<";
		for (Element el : this.cond) {
			s += "\n" + el.toString(level + 1);
		}
		s += "\n" + tab(level) + ">>===TRUE===<<";
		for (Element el : this.bodyTrue) {
			s += "\n" + el.toString(level + 1);
		}
		if (!this.bodyFalse.isEmpty()) {
			s += "\n" + tab(level) + ">>===FALSE==<<";
			for (Element el : this.bodyFalse) {
				s += "\n" + el.toString(level + 1);
			}
		}
		return s;
	}


	@Override
	public String toSchematic(int level, String prefix,
			  String boxFormat, String color) {
		String s = new String();
		int cpt = 0;
		s += super.toSchematic(level, prefix, "box", color);
		if (!this.cond.isEmpty()) {
			cpt = 0;
			for (Element el : this.cond) {
				s += "\n" + el.toSchematic(level, prefix + "_cond_"
						  + cpt, "box", GraphParams.defaultColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_cond_" + cpt;
				cpt++;
			}
		}
		if (!this.bodyTrue.isEmpty()) {
			cpt = 0;
			for (Element el : this.bodyTrue) {
				s += "\n" + el.toSchematic(level, prefix + "_bodyTrue_"
						  + cpt, "box", GraphParams.defaultColor);
				s += "\n" + tab(level) + prefix + " -> " + prefix
						  + "_bodyTrue_" + cpt + "[label = \"bodyTrue\"]";
				cpt++;
			}
		}
		if (!this.bodyFalse.isEmpty()) {
			cpt = 0;
			for (Element el : this.bodyFalse) {
				s += "\n" + el.toSchematic(level, prefix + "_bodyFalse_"
						  + cpt, "box", GraphParams.defaultColor);
					s += "\n" + tab(level) + prefix + " -> " + prefix
							  + "_bodyFalse_" + cpt + "[label = \"bodyFalse\"]";
				cpt++;
			}
		}
		return s;
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		if(!name.contains("loopfor")) {		
			for (Element e : this.cond)
				sOctave += e.toOctave(level) + "\n";
			for (Element e : this.bodyTrue)
				sOctave += e.toOctave(level) + "\n";
			for (Element e : this.bodyFalse)
				sOctave += e.toOctave(level) + "\n";
		}
		return sOctave;
	}
	
	
	public Element findElement(String name) {
		for(Element e : inputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : outputVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : internalVars)
			if (e.getName().equals(name))
				return e;
		for(Element e : body)
			if (e.findElement(name)!=null)
				return e.findElement(name);
		return null;
	}
	
	
	/**
	 * Check if the IfThenElse contains a loop
	 * @return if the IfThenElse contains a loop
	 */
	public boolean containsLoop() 
	{
		/* Check the body true */
		for(Element e : bodyTrue) {
			if(e instanceof Loop)
				return true;
			else if(e instanceof IfThenElse)
				if(((IfThenElse)e).containsLoop())
					return true;
		}
		
		/* Check the body false */
		for(Element e : bodyFalse) {
			if(e instanceof Loop)
				return true;
			else if(e instanceof IfThenElse)
				if(((IfThenElse)e).containsLoop())
					return true;
		}
		
		return false;
	}
	
}
