/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											StructTreatment
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import org.w3c.dom.*;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.buildingblocks.BuildingBlock.NumType;

import java.util.Vector;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.HashMap;
import java.util.ArrayList;



import m2m.backend.octaveparser.*;
import m2m.backend.project.ProjectRef;
import m2m.backend.utils.FileUtils;
import m2m.backend.utils.XMLUtils;
import m2m.backend.vhdl.*;
import m2m.backend.structure.Assignment;

/**
 *	Contains all the treatments make on the structure
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class StructTreatment extends ProjectRef 
{	
	/**
	 * Temporary variable counter
	 */
	private int tmpCpt;
	/**
	 * Operation counter
	 */
	private int opCpt;
	/**
	 * Internal structure root
	 */
	private Function top;
	/**
	 * List of negative variables
	 */
	private Vector<String> negList;
	/**
	 * Pragma list
	 */
	private HashMap<String, String> pragmaMap;
	/**
	 * List of variables created, allows a single instance for each variable
	 */
	private HashMap<String, SimpleVariable> varMap;
	/**
	 * Use to reprensent temporary simple variable
	 */
	public static final String TMP_NAME = "m2mtmp";	
	/**
	 * Stores the number of elements to be monitored in the structure
	 */
	private int nbMonitor = 0;
	/**
	 * Stores the time of the last parsing (file or internal structure)
	 */
	private long parseTime;
	
	/**
	 * Stores the md5 value of the source file. Useful to check consistency.
	 */
	private String sourceMd5;
	
	/**
	 * Stores the original source code
	 */
	private String sourceCode;
	
	/**
	 * Constructor
	 */
	public StructTreatment() {
		this.tmpCpt = 0;
		this.opCpt = 0;
		this.top = null;
		this.negList = new Vector<String>();
		this.pragmaMap = new HashMap<String, String>();
		this.varMap = new HashMap<String, SimpleVariable>();
		this.nbMonitor=0;
	}
	

	/**
	 * Copy entirely the structure. 
	 * @return The new structure.
	 */
	@SuppressWarnings("unchecked")
	public StructTreatment copy() {
		StructTreatment clone=new StructTreatment();
		clone.tmpCpt=this.tmpCpt;
		clone.opCpt=this.opCpt;
		clone.top=new Function();
		clone.top=(Function)this.top.copy(null);
		clone.negList = (Vector<String>)this.negList.clone();
		clone.pragmaMap = (HashMap<String, String>)this.pragmaMap.clone();
		clone.varMap = (HashMap<String, SimpleVariable>)this.varMap.clone();
		clone.nbMonitor = this.nbMonitor;
		return clone;
	}
	
	public void modifyNumType(NumType type) {
		top.modifyNumType(type);
	}
	
	/**
	 * Returns the time of the last parsing. This can be the parsing of a file
	 * or of an internal buffer.
	 * @return Time of the last parsing.
	 */
	public long getParseTime() {
		return parseTime;
	}

	/**
	 * Setter of the internal structure root
	 * @param top Internal structure root
	 */
	public void setTop(Function top) {
		this.top = top;
	}

	/**
	 * Getter of the internal structure root
	 * @return Internal structure root
	 */
	public Function getTop() {
		return this.top;
	}

	/**
	 * Getter of the input for internal structure
	 * @return Input list
	 */
	public Vector<Element> getInput() {
		return ((Function) this.top).getInput();
	}
		
	/**
	 * Getter of the output for internal structure
	 * @return Input list
	 */
	public Vector<Element> getOutput() {
		return ((Function) this.top).getOutput();
	}

	/**
	 * Getter of the name of the function
	 * @return Name
	 */
	public String getName() {
		return top.getName();
	}

	/**
	 * Run the parsing of the octave file
	 * @param file Octave file
	 * @throws ParsingException When parser can't parse the input file
	 */
	public void parse(File file) throws ParsingException 
	{
		InputStream in = null;
		try {
			in = new FileInputStream(file.getPath());
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas!");
			e.printStackTrace();
			System.exit(0);
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		OctaveLexer lexer = new OctaveLexer(in);
		OctaveParser parser = new OctaveParser(lexer);
		// Create the top entity of the structure
		this.top = new Function();
		parser.setTop(this.top);
		parser.setTreat(this);
		try {
			parser.entry();
			if (!parser.isSuccessful())
				throw new ParsingException("Parsing error");
			this.treeToVector();
			this.postParse(top);
			Calendar cal = Calendar.getInstance();
			parseTime = cal.getTime().getTime();
			this.modifyNumType(project.getOptimisationProperties().getOptimisationDataType());
		} catch (Exception e) {
			throw new ParsingException(e);
		}
		sourceMd5=FileUtils.getMd5(file.getAbsolutePath());

		sourceCode = FileUtils.readFileAsString(file.getAbsolutePath());
	}
	/**
	 * Run the parsing of the octave file
	 * @param file Octave file
	 * @throws ParsingException When parser can't parse the input file
	 */
	public void parseNoFlat(File file) throws ParsingException 
	{
		InputStream in = null;
		try {
			in = new FileInputStream(file.getPath());
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas!");
			e.printStackTrace();
			System.exit(0);
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		OctaveLexer lexer = new OctaveLexer(in);
		OctaveParser parser = new OctaveParser(lexer);
		// Create the top entity of the structure
		this.top = new Function();
		parser.setTop(this.top);
		parser.setTreat(this);
		try {
			parser.entry();
			if (!parser.isSuccessful())
				throw new ParsingException("Parsing error");
			//OptimizeOperators opt=new OptimizeOperators(this);
			//opt.optimise();
			this.postParse(top);
			Calendar cal = Calendar.getInstance();
			parseTime = cal.getTime().getTime();
//			long timeCurrent = cal.getTime().getTime() / 100000;
		} catch (Exception e) {
			throw new ParsingException(e);
		}
		sourceMd5=FileUtils.getMd5(file.getAbsolutePath());

		sourceCode = FileUtils.readFileAsString(file.getAbsolutePath());
	}
	
	public String getSourceMd5() {
		return sourceMd5;
	}
	
	public String getSourceCode() {
		return sourceCode;
	}
	
	/**
	 * Run the parsing of the octave file
	 * @param file Text of the octave file
	 * @throws ParsingException When parser can't parse the input file
	 */
	public void parse(String code) throws ParsingException 
	{

		InputStream in = null;
		in = new ByteArrayInputStream(code.getBytes());
		
		OctaveLexer lexer = new OctaveLexer(in);
		OctaveParser parser = new OctaveParser(lexer);
		// Create the top entity of the structure
		this.top = new Function();
		parser.setTop(this.top);
		parser.setTreat(this);
		try {
			parser.entry();
			if (!parser.isSuccessful())
				throw new ParsingException("Parsing error");
		//	OptimizeOperators opt=new OptimizeOperators(this);
		//	opt.optimise();
			this.treeToVector();
			this.postParse(top);
			Calendar cal = Calendar.getInstance();
			parseTime = cal.getTime().getTime();
		} catch (Exception e) {
			throw new ParsingException(e);
		}
		sourceMd5=FileUtils.getStringMd5(code);
		sourceCode=code;
	}

	/**
	 * Create operation and temporary variables if necessary
	 * @param a First operation list
	 * @param b Second operation list
	 * @param op Operation
	 * @return New operation list
	 */
	public Vector<Element> createOp(
			  Vector<Element> a, Vector<Element> b, Operation op) {
		Vector<Element> vec = new Vector<Element>();
		// Add all the elements of a and remove the last. That's the one to
		// treat.
		vec.addAll(a);
		vec.remove(vec.lastIndexOf(a.lastElement()));
		if (a.lastElement() instanceof Negation) {
			vec.add(a.lastElement());
			op.addInput(((Negation) a.lastElement()).getOutputAt(0));
		} else {
			op.addInput(a.lastElement());
		}
		// Same operation that with a
		vec.addAll(b);
		vec.remove(vec.lastIndexOf(b.lastElement()));
		if (b.lastElement() instanceof Negation) {
			vec.add(b.lastElement());
			op.addInput(((Negation) b.lastElement()).getOutputAt(0));
		} else {
			op.addInput(b.lastElement());
		}
		op.setName(op.getName() + this.getNextOpCpt());
		vec.add(op);
		if (op instanceof DotDivision || op instanceof DotMultiplication) {
			for (Element e : op.getInput()) {
				if (e instanceof Variable) {
					this.addPragma(getTrueName(e.getName()), "vector");
				}
			}
		}
		return vec;
	}

	/**
	 * Create a "not" operation
	 * @param a Precedent operation list
	 * @param not Operation "not"
	 * @return New operation list
	 */
	public Vector<Element> createNot(Vector<Element> a, Not not) {
		Vector<Element> vec = new Vector<Element>();
		vec.addAll(a);
		vec.remove(vec.lastIndexOf(a.lastElement()));
		if (a.lastElement() instanceof Negation) {
			vec.add(a.lastElement());
			not.addInput(((Operation) a.lastElement()).getOutputAt(0));
		} else {
			not.addInput(a.lastElement());
		}
		not.setName(not.getName() + this.getNextOpCpt());
		vec.add(not);
		return vec;
	}

	/**
	 * Operation counter
	 * @return next value for the counter
	 */
	public int getNextOpCpt() {
		return this.opCpt++;
	}

	/**
	 * Convert the tree structure of the operation in a chain of simple
	 * operations :
	 * s=a+b+c
	 *               -- c                  -- c
	 *         -- + --           tmp -- + --
	 * s -- + --     -- b    ==>           -- b
	 *	        -- a                        -- tmp
	 *                           s   -- + --
	 *                                     -- a
	 */
	public void treeToVector() {
		this.treeToVector(top);
	}

	/**
	 * Convert the top Element from a tree structure to a chain of operations.
	 * @param top Root of the structure to convert
	 */
	public void treeToVector(Function parent) {
		parent.setBody(this.convertBody(parent.getBody(), parent));
		if (parent instanceof LoopWhile) {
			((LoopWhile) parent).setCond(this.convertBody(((LoopWhile) parent).getCond(), parent));
		}
	}

	/**
	 * Convert the tree structures on a body to a chain of operations.
	 * @param vec Vector that contains operations
	 * @return Vector with new structure
	 */
	private Vector<Element> convertBody(Vector<Element> vec, Element parent) {
		Vector<Element> newel = new Vector<Element>();
		if (vec == null || vec.isEmpty()) {
			return vec;
		}
		for (Element e : vec) {
			if (e instanceof Operation) {
				newel.addAll(this.convertOperation((Operation) e, parent));
			} 
			else if (e instanceof Switch) { 
				Switch sw = (Switch) parent;
				for (Case c : sw.getCases()) {
					c.setBody(this.convertBody(c.getBody(), parent));
				}
			}
			else if (e instanceof IfThenElse) { 
				newel.add(e);
				IfThenElse ite = (IfThenElse) e;
				ite.setBodyTrue(this.convertBody(ite.getBodyTrue(), ite));
				ite.setBodyFalse(this.convertBody(ite.getBodyFalse(), ite));
				ite.setCond(this.convertBody(ite.getCond(), ite));
				for(Element element : ite.internalVars) {
					if(!((Function)parent).internalVars.contains(element))
						((Function)parent).addInternalVar(element);
				}
			}
			else if(e instanceof Function ) {
				newel.add(e);
				treeToVector((Function) e);
			}
		}
		return newel;
	}

	/**
	 * Convert one operation to its chain representation
	 * @param op Operation to convert
	 * @return Vector that contains the new structure
	 */
	private Vector<Element> convertOperation(Operation op, Element parent) {
		Vector<Element> vec = new Vector<Element>();
		Vector<Element> input;
		SimpleVariable tmp;
		input = op.getInput();
		op.setInput(new Vector<Element>());
		for (Element e : input) {
			if (e instanceof Operation) {
				vec.addAll(this.convertOperation((Operation) e, parent));
				// if operation need to be separate, create a temporary value
				tmp = new SimpleVariable(TMP_NAME + tmpCpt++ + "_m2m");
				((Operation) e).addOutput(tmp);
				((Function)parent).addInternalVar(tmp);
				op.addInput(tmp);
				
			} else if (e instanceof Variable) {
				if(parent instanceof Loop && ((Variable) e).getType().equalsIgnoreCase("const") && !((Loop)parent).getInternalVars().contains(e))
					((Loop)parent).addInternalVar(e);
				op.addInput(e);
			}
		}
		vec.add(op);
		return vec;
	}

	/**
	 * Create an assignment
	 * @param var SimpleVariable, target of assignment
	 * @param expr Operation list, the last element is to affect
	 * @return New operation list
	 */
	public Vector<Element> createAff(Variable var, Vector<Element> expr) {
		Vector<Element> vec = new Vector<Element>();
		Operation result = new Assignment();
		if (expr.lastElement() instanceof Variable) {
			// Add all element of expr to vec except the last one
			vec.addAll(expr);
			vec.removeElementAt(vec.lastIndexOf(expr.lastElement()));
			// If that's a SimpleVariable, juste add to the assignment
			result.addInput((Variable) expr.lastElement());
			result.setName("aff" + this.opCpt++);
			var.setType(((Variable) expr.lastElement()).getType());
			if (var instanceof SimpleVariable) {
				((SimpleVariable) var).setSize(((Variable) expr.lastElement()).getSize());
			}
			if (((Variable) expr.lastElement()).getType().equals("const")) {
				((SimpleVariable) var).setVal((((Variable) expr.lastElement()).getVal()));
			}
		} else if (expr.lastElement() instanceof Negation) {
			// Add all elements of expr to vec
			vec.addAll(expr);
			result.addInput(((Negation) expr.lastElement()).getOutputAt(0));
			result.setName("aff" + this.opCpt++);
			var.setType(((Variable) ((Negation) expr.lastElement()).getOutputAt(0)).getType());
		} else if (expr.lastElement() instanceof Operation) {
			vec.addAll(expr);
			vec.removeElementAt(vec.lastIndexOf(expr.lastElement()));
			result = ((Operation) expr.lastElement());
			//var.setType(((Variable)((Operation)expr.lastElement()).getOutputAt(0)).getType());
		}
		result.addOutput(var);
		vec.add(result);
		return vec;
	}

	/**
	 * Create an in or out variable
	 * @param name Name of the variable
	 * @return New variable
	 */
	public SimpleVariable createInOut(String name) {
		SimpleVariable var = null;
		if (this.varMap.containsKey(name)) {
			var = this.varMap.get(name);
		} else {
			var = new SimpleVariable(name);
			this.varMap.put(name, var);
		}
		if (this.pragmaMap.containsKey(getTrueName(name))) {
			((SimpleVariable) var).setType(this.pragmaMap.get(getTrueName(name)));
		}
		return var;
	}

	/**
	 * Create a variable. Check if existing, if yes just return the 
	 * adresse. If the variable is negative, check if the negation is already
	 * do, and if not create it.
	 * Crée une variable, vérifie si elle existe déjà et récupère l'objet
	 * dans ce cas, sinon en crée une nouvelle. Si la variable est négative elle
	 * vérifie si la négation est déjà créé, si non elle la crée.
	 * @param name Name
	 * @param val Value
	 * @param type Type
	 * @param minus Negative (true if the variable is negative)
	 * @return New variable
	 */
	public Vector<Element> createVar(String name, ArrayList<Double> val,
			  String type,int size, boolean minus) {
		//int size = val.size();
		SimpleVariable var = null;
		Vector<Element> vec = new Vector<Element>();
		if (name.isEmpty()) {
			// If it as no name, it don't exist so we simply create
			vec.add(new SimpleVariable(val));
		} else if (minus && !negList.contains(name)) {
			Negation neg = null;
			// If it's negative and never declared before
			neg = new Negation();
			var = (SimpleVariable) this.createVar(getTrueName(name) + "_m2m_n",
					  0.0, "", false).lastElement();
			if (this.pragmaMap.containsKey(name)) {
				var.setType(this.pragmaMap.get(name));
			}
			neg.addOutput(var);
			neg.addInput(this.createVar(name, 0.0, "", false).lastElement());
			neg.setName("neg" + this.opCpt++);
			this.negList.add(name);
			vec.add(neg);
			vec.add(var);
		} else if (minus) {
			// If negative and already declared
			vec.add(this.createVar(name + "_m2m_n", 0.0, "", false).lastElement());
		} else {
			// If normal variable
			if (this.varMap.containsKey(name)) {
				var = this.varMap.get(name);
			} else {
				var = new SimpleVariable(name, val, type);
				var.setSize(size);
				this.top.addInternalVar(var);
				this.varMap.put(name, var);
			}
			if (this.pragmaMap.containsKey(getTrueName(name))) {
				((SimpleVariable) var).setType(this.pragmaMap.get(getTrueName(name)));
			}
			vec.add(var);
		}
		return vec;
	}

	/**
	 * Remove the m2m extension to the name
	 * a_m2m_2 -> a
	 * @param name Name to analyse
	 * @return Name without m2m extension
	 */
	public static String getTrueName(String name) {
		StringTokenizer st = new StringTokenizer(name, "_");
		String trueName = new String();
		String tmp = new String();
		if (st.hasMoreTokens()) {
			trueName = st.nextToken();
		}
		while (st.hasMoreTokens()) {
			tmp = st.nextToken();
			if (tmp.equals("m2m")) {
				return trueName;
			}
			trueName += "_" + tmp;
		}
		return trueName;
	}

	/**
	 * Call the complete methode createVar, use an existing variable
	 * @param var SimpleVariable
	 * @return New variable
	 */
	public Vector<Element> createSimpleVar(SimpleVariable var) {
		return this.createVar(var.getName(), var.getValues(), var.getType(), var.getSize(), false);
	}

	/**
	 * Check if the variable is an instance of VectorVariable or SimpleVariable
	 * and call the methode.
	 * @param var Variable
	 * @return New variable.
	 */
	public Vector<Element> createVar(Variable var) {
		Vector<Element> vec = null;
		if (var instanceof VectorVariable) {
			vec = this.createVar(((VectorVariable) var).getVar());
		} else if (var instanceof SimpleVariable) {
			vec = this.createSimpleVar((SimpleVariable) var);
		}
		return vec;
	}

	/**
	 * Create the variable with the parameters
	 * @param name Name
	 * @param val Value
	 * @param type Type
	 * @param minus Negative (true if the variable is negative)
	 * @return New variable into a Vector
	 */
	public Vector<Element> createVar(String name, double val,
			  String type, boolean minus) {
		ArrayList<Double> values = new ArrayList<Double>();
		values.add(val);
		return this.createVar(name, values, type, 1, minus);
	}

	/**
	 * Create the variable with the parameters
	 * @param name Name
	 * @return New variable into a Vector
	 */
	public Vector<Element> createVar(String name) {
		ArrayList<Double> values = new ArrayList<Double>();
		//values.add(0.0);
		return this.createVar(name, values, "", 0, false);
	}

	/**
	 * Add a counter to the name of the Element
	 * @param el Element
	 */
	public void count(Element el) {
		el.setName(el.getName() + this.opCpt++);
	}

	/**
	 * Create the final affectation for each in/out variables
	 * All input variables need to be affect to the first use of the variable:
	 *		a_m2m_0 = a
	 * All output variables need to be affect to the last usw of the variable:
	 *		s = s_m2m_17
	 * @param map Liste of the variables
	 */
	public void createInOutAff(Function top, HashMap<String, ReadWriteIndex> map) {
		Assignment aff;
		String name;
		int cpt = 0;
		if (top == null) {
			top = this.top;
		}
		// Input aff
		for (Element var : top.getInput()) {
			name = var.getName();
			if (this.varMap.containsKey(name + "_m2m_0")) {
				aff = new Assignment();
				aff.setName("aff_final_" + cpt++);
				aff.addInput(var);
				aff.addOutput(this.varMap.get(name + "_m2m_0"));
				top.addBodyToTop(aff);
			} else {
				aff = new Assignment();
				aff.setName("aff_final_" + cpt++);
				Variable tmp = (Variable) this.createVar(name + "_m2m_0", 0.0, "", false).lastElement();
				aff.addInput(var);
				aff.addOutput(tmp);
				top.addBodyToTop(aff);
			}
		}
		// Output aff
		for (Element var : top.getOutput()) {
			name = var.getName();
			if(map.containsKey(name))
			if (this.varMap.containsKey(name + "_m2m_" + ((ReadWriteIndex)map.get(name)).getReadIndex())) {
				aff = new Assignment();
				aff.setName("aff_final_" + cpt++);
				aff.addInput(this.varMap.get(name + "_m2m_" +((ReadWriteIndex)map.get(name)).getReadIndex()));
				aff.addOutput(var);
				top.addBody(aff);
			}
		}
	}

	/**
	 * Add a pragma and modify all the occurences that correspond to the pragma.
	 * It must be like : "%m2m name:type"
	 * @param pragma Pragma
	 */
	public void addPragma(String pragma) {
		StringTokenizer st = new StringTokenizer(pragma, " :\t");
		String name = null;
		String type = null;
		if (st.countTokens() == 3 & st.nextToken().equals("%m2m")) {
			name = st.nextToken();
			type = st.nextToken();
			//System.out.println(name+"\t"+type);
			this.pragmaMap.put(name, type);
			this.changeType(this.top, name, type);
			//System.out.println("Nombre d'occurences de la variable modifiée :"
			//		  + this.changeType(this.top, name, type));
		} else {
			System.out.println("#Le pragma ne respecte pas le standard!");
		}
	}

	/**
	 * Add a pragma and modify all the occurences that correspond to the pragma.
	 * It must be a name and a type.
	 * @param name Name
	 * @param type Type
	 */
	public void addPragma(String name, String type) {
		this.pragmaMap.put(name, type);
		this.changeType(this.top, name, type);
		System.out.println("Nombre d'occurences de la variable modifiée :" + this.changeType(this.top, name, type));
	}

	/**
	 * Change the type of all variable corresponds to the parameters
	 * @param top Structure root
	 * @param name Name
	 * @param type New type
	 * @return Number of variable found
	 */
	private int changeType(Element top, String name, String type) {
		int nb = 0;
		if (top instanceof Function) {
			if (top instanceof LoopFor) {
				nb += this.changeType(((LoopFor) top).getStart(), name, type);
				nb += this.changeType(((LoopFor) top).getIncr(), name, type);
				nb += this.changeType(((LoopFor) top).getEnd(), name, type);
			}
			for (Element el : ((Function) top).getInput()) {
				nb += this.changeType(el, name, type);
			}
			for (Element el : ((Function) top).getOutput()) {
				nb += this.changeType(el, name, type);
			}
			for (Element el : ((Function) top).getInternalVars()) {
				nb += this.changeType(el, name, type);
			}
			for (Element el : ((Function) top).getBody()) {
				nb += this.changeType(el, name, type);
			}
		} else if (top instanceof Operation) {
			for (Element el : ((Operation) top).getInput()) {
				nb += this.changeType(el, name, type);
			}
			for (Element el : ((Operation) top).getOutput()) {
				nb += this.changeType(el, name, type);
			}
		} else if (top instanceof SimpleVariable) {
			if (getTrueName(top.getName()).equals(name)) {
				((SimpleVariable) top).setType(type);
				return 1;
			}
		} else if (top instanceof VectorVariable) {
			if (getTrueName(top.getName()).equals(name)) {
				((SimpleVariable) ((VectorVariable) top).getVar()).setType(type);
				return 1;
			}
		}
		return nb;
	}

	/**
	 * Switch a variable by another in the structure. Change each instance.
	 * @param top Structure root
	 * @param varin Variable to remplace
	 * @param varout New variable
	 */
	public void changeVar(Element top, Variable varin, Variable varout) {
		if (top instanceof Function) {
			for (Element el : ((Function) top).getBody()) {
				this.changeVar(el, varin, varout);
			}
			while (((Function) top).getInput().indexOf(varin) != -1) {
				((Function) top).getInput().set(((Function) top).getInput().indexOf(varin), varout);
			}
			while (((Function) top).getOutput().indexOf(varin) != -1) {
				((Function) top).getOutput().set(((Function) top).getOutput().indexOf(varin), varout);
			}
			if (top instanceof IfThenElse) {
				for (Element el : ((IfThenElse) top).getCond()) {
					this.changeVar(el, varin, varout);
				}
				for (Element el : ((IfThenElse) top).getBodyTrue()) {
					this.changeVar(el, varin, varout);
				}
				for (Element el : ((IfThenElse) top).getBodyFalse()) {
					this.changeVar(el, varin, varout);
				}
			}
		} else if (top instanceof Operation) {
			while (((Operation) top).getInput().indexOf(varin) != -1) {
				((Operation) top).getInput().set(((Operation) top).getInput().indexOf(varin), varout);
			}
			while (((Operation) top).getOutput().indexOf(varin) != -1) {
				((Operation) top).getOutput().set(((Operation) top).getOutput().indexOf(varin), varout);
			}
		}
	}

	public void findInBody(Element top, HashMap<String, ReadWriteIndex> cpt) {
		HashMap<String, Integer> list = new HashMap<String, Integer>();
		if (top instanceof Loop) {
			for (Element el : ((Loop) top).getBody()) {
				if (el instanceof Operation) {
					for (Element in : ((Operation) el).getInput()) {
						String name = getTrueName(in.getName());
						if (in instanceof Variable
								  & !((Variable) in).getType().equals("const")
								  & !((Variable) in).getType().equals("iter")
								  & !((Loop) top).getInput().contains(in)
								  & !name.contains(TMP_NAME)) {
							list.put(name, ((ReadWriteIndex)cpt.get(name)).getReadIndex());
						}
					}
				} else if (el instanceof Function) {
					for (Element in : ((Function) el).getInput()) {
						String name = getTrueName(in.getName());
						if (in instanceof Variable
								  & !((Variable) in).getType().equals("const")
								  & !((Loop) top).getInput().contains(in)
								  & !name.contains(TMP_NAME) 
								  & !((Variable) in).getType().equals("iter")) {
							list.put(name, ((ReadWriteIndex)cpt.get(name)).getReadIndex());
							//((LoopFor) top).addInput(in);
						}
					}
				}
			}
		}

		for (String s : list.keySet()) 
		{
			Element e = this.createVar(s + "_m2m_" + ((ReadWriteIndex)cpt.get(s)).getReadIndex()).lastElement();
			if(!((Loop) top).getInput().contains(e) && ((Variable) e).getType().equals("iter"))
				((Loop) top).addInput(e);
		}
	}

	/**
	 * Check if the name is a already declared variable
	 * @param name Name of the variable
	 * @return True if name is a variable
	 */
	public boolean isVariable(String name) {
		for (String var : this.varMap.keySet()) {
			if (getTrueName(var).equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the list of all monitored variables.
	 * @return List of the variables
	 */
	public Vector<Variable> monitoredVar() {
		Vector<Variable> vec = new Vector<Variable>();
		for (SimpleVariable var : varMap.values()) {
			if (var.monitor) {
				vec.add(var);
			}
		}
		return vec;
	}

	/**
	 * Set the material block who define the classe of element
	 * @param top Root of the structure
	 * @param opName Name of the operation to change the material block
	 * @param block Material block
	 */
	public void setBlock(Element top, String opName, BuildingBlock block) {
		if (top instanceof Function) {
			for (Element el : ((Function) top).getBody()) {
				this.setBlock(el, opName, block);
			}
		} else if (top instanceof Operation) {
			if (((Operation) top).getOpName().equals(opName)) {
				((Operation) top).setBlock(block);
			}
		}
	}

	/**
	 * Write the internal structure to a VHDL file
	 * @param outFile File name
	 * @throws IOException 
	 * @throws VHDLException 
	 */
	public boolean writeVHDL(String vhdlPath) {
		File file = new File(vhdlPath+this.top.getName()+".vhd");
		FileWriter fw = null;
		try {
			// Get a copy of the structure
			StructTreatment structCopy = this;//.copy();
			
			// Initialization of the cost of all SimpleVariable
			setCost(structCopy.top);

			fw = new FileWriter(file);
			String sVhdl = new String();
			
			VHDLCreator vhdl = new VHDLCreator(structCopy.top, project);
			vhdl.wrapTop(vhdlPath);
			sVhdl = vhdl.createVHDL(file.getName());	
			
			fw.write(sVhdl);
			fw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (VHDLException e) {
			System.out.println("#Unable to create VHDL file! "+e.getMessage());
			return false;
		}		
		// TODO remettre en place une fois le debug termine
		//catch (Exception e) {
			//System.out.println("#Unable to create VHDL file! "+e.getMessage());
			//return false;
		//}
		System.out.println("VHDL file written!");
		return true;
	}

	/**
	 * Print the internal structure
	 */
	public void print() {
		System.out.println(this.top.toString(0));
	}

	/**
	 * Write the schematic file. Use graphviz to generate the schematic.
	 * dot -Tpdf outfile -o schematic.pdf
	 * @param outFile Name of the file
	 * @param blockList True if you want have blocs for the Vector
	 */
	public boolean writeSchematic(String outFile) {
		File file = new File(outFile);
		FileWriter fw;
		try {
			fw = new FileWriter(file);
			String str = this.createSchematic();
			fw.write(str);
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("Schematic file written!");
		return true;
	}

	/**
	 * Launche the schematic creation
	 * @param blockList True if you want have blocs for the Vector
	 * @return String contains the schematic file
	 */
	private String createSchematic() {
		String sSchema = new String();
		sSchema += "digraph G  {\n";
		sSchema += top.toSchematic(1, "", GraphParams.defaultShape, "");
		sSchema += "\n}";
		return sSchema;
	}

	/**
	 * Write the structure into an octave file
	 */
	public void writeOctaveFile() {
		this.writeOctaveFile(null);
	}

	/**
	 * Write the octave code to a file
	 * @param outFile Name of the output file, if empty the file will be named "test/name_of_the_function.m"
	 */
	public boolean writeOctaveFile(String outFile) {
		File file;
		if (outFile == null) {
			file = new File("test/" + this.top.getName() + ".m");
			System.out.println(file.getName());
		} else {
			file = new File(outFile);
		}
		FileWriter fw;
		try {
			fw = new FileWriter(file);
			String str = this.createOctave();
			fw.write(str);
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("Octave file written!");
		return true;
	}

	/**
	 * Create the octave code
	 * @return Octave code
	 */
	private String createOctave() {
		String sOctave = new String();
		sOctave += this.top.toOctave(0);
		return sOctave;
	}
	
	
	/**
	 * Clean the structure by deleting all unused variable
	 * @param body the vector of element to clean
	 * @param varVector the vector of used variables of the parent
	 */
	public void clean(Vector<Element> body, Vector<Element> varVector) 
	{	
		Vector<Function> functionVector = new Vector<Function>();
		Element element;
		Vector<Element> elementList = new Vector<Element>();
		Iterator<Element> iterElement;
		
		/* Get all children functions and read simple variable */
		elementList.addAll(body);
		iterElement = elementList.iterator();
		while (iterElement.hasNext()) {
			element = iterElement.next();
			
			/* Add input of operations to the varVector */
			if (element instanceof Operation) {
				for(Element input : ((Operation)element).getInput()) {
					if(input instanceof Operation)	
						elementList.add(input);
					else if(!varVector.contains(input))
						varVector.add(input);
				}
			}
			/* Add input of function to the varVector */
			else if (element instanceof Function) {
				for(Element input : ((Function)element).getInput())	
					if(!varVector.contains(input))
						varVector.add(input);
				functionVector.add((Function)element);	
			}
					
			/* Get the next element of the body */
			elementList.remove(elementList.indexOf(element));
			iterElement = elementList.iterator();	
		}
		
		
		Vector<Element> tmpVarVector = new Vector<Element>();
		for(Function function : functionVector)
		{
			/* Collect all unused output */
			for(Element variable : function.outputVars)
				if(!varVector.contains(variable))
					tmpVarVector.add(variable);	
			
			// Delete All unused output
			function.outputVars.removeAll(tmpVarVector);
			
			/* Delete loop ifThenElse who are unused */
			if(function instanceof IfThenElse &&  function.getName().contains("loop")) {
				SimpleVariable outputLoopIf = ((SimpleVariable)((Assignment)((IfThenElse)function).getBodyTrue().firstElement()).getOutputAt(0));
				SimpleVariable inputLoopIf = ((SimpleVariable)((Assignment)((IfThenElse)function).getBodyTrue().firstElement()).getInputAt(0));
				if(!varVector.contains(outputLoopIf)) {
					body.remove(function);
					varVector.remove(outputLoopIf);
					varVector.remove(inputLoopIf);
				}
			}
			/* Clean children IfThenElse */
			else if(function instanceof IfThenElse) {
				clean(((IfThenElse)function).getBodyTrue(), varVector); 
				clean(((IfThenElse)function).getBodyFalse(), varVector); 
			}
			/* Clean children Function */
			else {
				clean(function.body, varVector); 
				
				/* Clean input if loop ifThenElse who are unused */
				tmpVarVector.clear();
				for(Element variable : function.inputVars)
					if(!varVector.contains(variable)) 
						tmpVarVector.add(variable);
				function.inputVars.removeAll(tmpVarVector);	
				function.internalVars.removeAll(tmpVarVector);	
			}
		}
	}
	
	/**
	 * Update condition simple varables for a "loopFor".
	 * @param tmpVar  the temporary conter variable of the structure before the "loopFor"
	 * @param cptVar  the compter variable of the structure
	 * @param loopFor the loopFor to update condition 
	 */
	public void updateConditionsFor(HashMap<String, ReadWriteIndex> tmpVar, HashMap<String, ReadWriteIndex> cptVar, LoopFor loopFor) 
	{	
		/* Update the end simple variable if it's used into the loop 
		String name = getTrueName(loopFor.getEnd().getName());
		if(cptVar.get(name) != null && ((ReadWriteIndex)cptVar.get(name)).getReadIndex()-((ReadWriteIndex)tmpVar.get(name)).getReadIndex() != ((ReadWriteIndex)tmpVar.get(name)).getDiff())
			loopFor.setEnd((SimpleVariable)createVar(name +"_m2m_"+(((ReadWriteIndex)cptVar.get(name)).getReadIndex()-1)).lastElement());
		*/
		
		/* Update the increment simple variable if it's used into the loop 
		String name = getTrueName(loopFor.getIncr().getName());
		if(cptVar.get(name) != null && ((ReadWriteIndex)cptVar.get(name)).getReadIndex()-((ReadWriteIndex)tmpVar.get(name)).getReadIndex() != ((ReadWriteIndex)tmpVar.get(name)).getDiff())
			loopFor.setIncr((SimpleVariable)createVar(name +"_m2m_"+(((ReadWriteIndex)cptVar.get(name)).getReadIndex()-1)).lastElement());
		*/
	}
	
	
	/**
	 * Update condition simple varables for a "loopWhile".
	 * @param tmpVar  	the temporary compter variable of the structure before the "loopWhile"
	 * @param cptVar  	the compter variable of the structure
	 * @param loopWhiel the loopWhile to update condition 
	 */
	public void updateConditionsWhile(HashMap<String, ReadWriteIndex> tmpVar, HashMap<String, ReadWriteIndex> cptVar, LoopWhile loopWhile) 
	{	
		String name;
		Element e;
		
		/* Go through all operation of the condition */
		for(Element op : loopWhile.getCond())
		{
			/* Update the first input of the operation */
			for(int i = 0; i < ((Operation)op).getInput().size(); i++)
			{
				e = ((Operation)op).getInputAt(i);
				name = getTrueName(e.getName());
				if(cptVar.get(name) != null && !name.equals(TMP_NAME) && ((ReadWriteIndex)tmpVar.get(name)).getReadIndex() != ((ReadWriteIndex)cptVar.get(name)).getReadIndex()-1)
					((Operation)op).setInputAt(i, ((SimpleVariable)createVar(name +"_m2m_"+(((ReadWriteIndex)cptVar.get(name)).getReadIndex()-1)).lastElement()));
			}
		}
	}
	
	
	/**
	 * Set the cost of Function elements.
	 * @param func 
	 */
	private void setCost(Function func)
	{
		/* Initialize the cost of input varaible */
		for(Element e : func.getInput())
			((SimpleVariable)e).setCost(0);

		// Set set the cost of body elements
		func.setMaxCost(setCost(func.getBody()));
	}
	
	/**
	 * Set the cost a body contained structure.
	 * @param elements All elements contained in the body
	 */
	private int setCost(Vector<Element> elements)
	{
		int maxCost = 0;
		int tempMaxCost = 0;
		
		for(Element e : elements) {
			if(e instanceof Operation)
				tempMaxCost = setCost((Operation)e);
			else if(e instanceof IfThenElse)
				tempMaxCost = setCost((IfThenElse)e);
			else if (e instanceof Loop) {
				Loop loop = (Loop)e;
				Vector<Integer> costs = new Vector<Integer>();
				int maxLoopCost = 0;
				
				/* Save all costs and get the max input cost */
				for(Element sv : loop.getInput()) {
					costs.add(((SimpleVariable)sv).getCost());
					maxLoopCost = ((SimpleVariable)sv).getCost() > maxLoopCost ? ((SimpleVariable)sv).getCost(): maxLoopCost;
					((SimpleVariable)sv).setCost(0);
				}
				
				/* Set all internal body cost */
				tempMaxCost = setCost(loop.getBody());
				if (loop instanceof LoopFor) {
					((SimpleVariable)((LoopFor)loop).getIterOperation().getOutputAt(0)).setCost(((LoopFor)loop).getIterOperation().getBlock().latencyTime()+1);
					tempMaxCost = tempMaxCost > ((LoopFor)loop).getIterOperation().getBlock().latencyTime()+1 ? tempMaxCost : ((LoopFor)loop).getIterOperation().getBlock().latencyTime()+1;
				}
				loop.setMaxCost(tempMaxCost+maxLoopCost);
				loop.setRelativeMaxCost(tempMaxCost);
				
				/* Restore all costs */
				for(Element sv : loop.getInput())
					((SimpleVariable)sv).setCost(costs.elementAt(loop.getInput().indexOf(sv)));
				
				/* Set all output cost */
				for(Element sv : loop.getOutput()) {
					if(((SimpleVariable)sv).getCost() != tempMaxCost) {
						((SimpleVariable)sv).setLoopOutputCost(loop, ((SimpleVariable)sv).getCost());
						((SimpleVariable)sv).setLoopOutputConnect(loop, true);
					}
					((SimpleVariable)sv).setCost(tempMaxCost+maxLoopCost);
				}
			}
			
			maxCost = maxCost < tempMaxCost ? tempMaxCost : maxCost;
		}			
		
		return maxCost;
	}
	
	/**
	 * Set the cost of Operation elements.
	 * @param  op
	 * @return The maximum cost of the operation
	 */
	private int setCost(Operation op)
	{
		int maxCost = 0;
		int tmpCost = 0;
		Vector<SimpleVariable> inputs = new Vector<SimpleVariable>();
		Vector<Vector<Element>> variablesDependencies = new Vector<Vector<Element>>();
		Vector<Element> resultDependencies = new Vector<Element>();
		
		/* Initialization of inputs */
		for(int i = 0; i < op.getInput().size(); i++)
			inputs.add((SimpleVariable) op.getInputAt(i));
		if(op instanceof Multiplexer)
			inputs.add((SimpleVariable)((Multiplexer)op).getSel());
			
		/* Get the maximum input cost */
		for(int i = 0; i < inputs.size(); i++) {
			SimpleVariable sv = inputs.elementAt(i);
			variablesDependencies.add(sv.getVariableDependencies());
			variablesDependencies.elementAt(i).add(sv);
			
			if(op.getBlock() == null) {
				maxCost = sv.getCost();
				break;
			} else {
				tmpCost = sv.getCost() + op.getBlock().latencyTime() + 1;
				maxCost = tmpCost > maxCost ? tmpCost : maxCost;
			}
		}	
		
		/* Create result variable dependencies */
		for(int i = 0; i < variablesDependencies.size(); i++) {
			for(int j = 0; j < variablesDependencies.elementAt(i).size(); j++) {
				if(!resultDependencies.contains(variablesDependencies.elementAt(i).elementAt(j)))
					resultDependencies.add(variablesDependencies.elementAt(i).elementAt(j));
			}
		}
			
		/* Set the maximum latency cost of outputs */
		for(Element e : op.getOutput()) {
			if(((SimpleVariable)e).getCost() < maxCost)
				((SimpleVariable)e).setCost(maxCost);
			((SimpleVariable)e).getVariableDependencies().addAll(resultDependencies);		
		}
		
		return maxCost;
	}
	
	
	/**
	 * Set the cost of IfThenElse elements.
	 * @param op
	 */
	private int setCost(IfThenElse ite)
	{
		int maxCost = 0;
		int tempMaxCost = 0;
		
		tempMaxCost = setCost(ite.getCond());
		maxCost = maxCost < tempMaxCost ? tempMaxCost : maxCost;
		
		tempMaxCost = setCost(ite.getBodyTrue());
		maxCost = maxCost < tempMaxCost ? tempMaxCost : maxCost;
		
		tempMaxCost = setCost(ite.getBodyFalse());
		maxCost = maxCost < tempMaxCost ? tempMaxCost : maxCost;
		
		return maxCost;
	}
	
	
	/** 
	 * Indicates if at least an element is monitored 
	 * @return the monitor attribute of the structure 
	 */
	public boolean getMonitor() { 
		return (nbMonitor>0); 
	} 
	
	
	/** 
	 * Set the monitor attribute of the structure. 
	 * @param monitor The value to the monitor attribute to set 
	 */ 
	public void modifyMonitor(boolean monitor) {
		if (monitor)
			nbMonitor++;
		else
			nbMonitor--; 
	}
	
	public boolean isEmpty() {
		if (top==null)
			return true;
		if (top.name.isEmpty())
			return true;
		return false;
	}

	public boolean fromXml(org.w3c.dom.Element el) {
		
		this.sourceMd5 = XMLUtils.getTextValue(el, "sourceMd5","");
		this.nbMonitor = XMLUtils.getIntValue(el, "nbMonitor", 0);
		this.sourceCode = XMLUtils.getTextValue(el,"SourceCode","");
		this.top=new Function();
		

		NodeList nl = el.getElementsByTagName("Function");
		if(nl != null && nl.getLength() > 0) {
			org.w3c.dom.Element e = (org.w3c.dom.Element)nl.item(0);
			if (e!=null) {
				this.top.fromXml(e);
			}
			else
				this.top=null;
		}
		
	//	this.setSimulationCalcPrecision(XMLUtils.getIntValue(el,"Precision",0));
	//	this.setNumberOfSamples(XMLUtils.getIntValue(el,"NbSamples",5000));
	//	this.setInputFrequency(XMLUtils.getFloatValue(el,"InputFrequency",(float)100.0));
	//	this.setOutputFrequency(XMLUtils.getFloatValue(el,"OutputFrequency",(float)100.0));
	//	this.setSystemFrequency(XMLUtils.getFloatValue(el,"SystemFrequency",(float)100.0));
		return true;
	}

	public String getXmlTagName() {
		return "Struct";
	}
	
	public org.w3c.dom.Element toXml(Document dom) {
		org.w3c.dom.Element el;
		el=dom.createElement(getXmlTagName());
		el.appendChild(XMLUtils.createTextElement(dom,"sourceMd5",sourceMd5));
		el.appendChild(XMLUtils.createIntElement(dom,"nbMonitor",nbMonitor));
		el.appendChild(XMLUtils.createTextElement(dom, "SourceCode",sourceCode));	
		if (top!=null)
			el.appendChild(top.toXml(dom,false));
		/*int monindex=0;
		for(m2m.backend.structure.Element e : top.inputVars) {
			if (e.getMonitor()) {
				el.appendChild(XMLUtils.createTextElement(dom,"Monitor"+monindex,e.getName()));
				monindex++;
			}
		}
		for(m2m.backend.structure.Element e : top.outputVars) {
			if (e.getMonitor()) {
				el.appendChild(XMLUtils.createTextElement(dom,"Monitor"+monindex,e.getName()));
				monindex++;
			}
		}
		for(m2m.backend.structure.Element e : top.internalVars) {
			if (e.getMonitor()) {
				el.appendChild(XMLUtils.createTextElement(dom,"Monitor"+monindex,e.getName()));
				monindex++;
			}
		}
		*/
		return el;
	}
	
	
	/**
	 * Post parse a function.
	 * @param function the function to parse
	 */
	public Vector<Element> postParse(Function function) 
	{
		Vector<Element> elements = new Vector<Element>();
		Vector<Element> removeElements = new Vector<Element>();
		
		/* Add inputs variables to internals variables */
		if(function instanceof Loop) {
			for(Element e : function.getInput())
				function.addInternalVar(e);
			if(function instanceof LoopFor) {
				LoopFor loopFor = (LoopFor)function;
				postParseLoopForCond(loopFor, loopFor.getStart());
				postParseLoopForCond(loopFor, loopFor.getIncr());
				postParseLoopForCond(loopFor, loopFor.getEnd());
			}
		}
		
		/* Remove all unused element in the function level */
 		elements = postParse(function.getBody()); 		
		for(int i = 0; i < function.getInternalVars().size(); i++)
			if(!elements.contains(function.getInternalVars().elementAt(i)) && !function.inputVars.contains(function.getInternalVars().elementAt(i)))
				removeElements.add(function.getInternalVars().elementAt(i));
		function.getInternalVars().removeAll(removeElements);
		
		return elements;
	}
	
	
	/**
	 * Post parse the one variable of the loopFor condition.
	 * @param loopFor the loopFor to parse the condition
	 * @param condVar The condition variable to parse
	 */
	private void postParseLoopForCond(LoopFor loopFor, SimpleVariable condVar)
	{
		if(!condVar.getType().equalsIgnoreCase("const") && loopFor.getInput().contains(condVar)) {						
			IfThenElse ite;
			if((ite = loopFor.isInitVariable(condVar)) != null) {
				SimpleVariable var = ((SimpleVariable)((Assignment)ite.getBodyTrue().firstElement()).getInputAt(0));
				var.setType("forloopcond");
			}
			if(!loopFor.getInternalVars().contains(condVar))
				loopFor.addInternalVar(condVar);
		}
		else if(!condVar.getType().equalsIgnoreCase("const") && !loopFor.getInput().contains(condVar)) {						
			loopFor.addInput(condVar);
			if(!loopFor.getInternalVars().contains(condVar))
				loopFor.addInternalVar(condVar);
		}
	}
	
	
	/**
	 * Post parse a vector of elements.
	 * @param vector the vector element to parse
	 * @return The new parsed vector element 
	 */
	public Vector<Element> postParse(Vector<Element> vector) 
	{
		Vector<Element> elements = new Vector<Element>();
		
		for(Element e : vector) {
			if(e instanceof Operation) {
				for(Element input : ((Operation)e).getInput()) {
					if(!elements.contains(input))
						elements.add(input);
				}
				for(Element output : ((Operation)e).getOutput()) {
					if(!elements.contains(output))
						elements.add(output);
				}
			}
			else if (e instanceof IfThenElse) {
				elements.addAll(postParse(((IfThenElse)e).getBodyFalse()));
				elements.addAll(postParse(((IfThenElse)e).getBodyTrue()));
				elements.addAll(postParse(((IfThenElse)e).getCond()));
			}
			else if(e instanceof Function){
				postParse((Function)e);
				elements.addAll(((Function)e).getInput());
				elements.addAll(((Function)e).getOutput());
			}
		}
		
		return elements;
	}
}
