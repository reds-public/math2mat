/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Element
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 8. janvier 2008, 16:11
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import java.util.HashMap;

import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.utils.XMLUtils;


/**
 *	Describe an element, all objects of the structure must extends this class
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public abstract class Element implements Cloneable {

	/**
	 * Name
	 */
	protected String name;
	/**
	 * Used to know if the element need to be monitor or not
	 */
	protected boolean monitor;

	public abstract String getXmlTagName();
	
	public org.w3c.dom.Element toXml(org.w3c.dom.Document dom, boolean shortRef) {
		org.w3c.dom.Element el;
		el=dom.createElement(getXmlTagName());
		insideToXml(el,dom,shortRef);
		return el;
	}
/*
	class myMap extends HashMap<String,Class<?>> {
	
		public myMap() {
			put("add",Addition.class);
			put("aff",Assignment.class);
			put("delay",Delay.class);
			put("div",Division.class);
			put("dotdiv",DotDivision.class);
			put("dotmult",DotMultiplication.class);
		}
	}

	static myMap map=new myMap();
	*/
	
	public Element getElement(org.w3c.dom.Element el,Function newFunc) {
		HashMap<String,Class<?>> OperationMap=new HashMap<String,Class<?>>();
		OperationMap.put(Addition.OPNAME,Addition.class);
		OperationMap.put(Assignment.OPNAME,Assignment.class);
		OperationMap.put(Delay.OPNAME,Delay.class);
		OperationMap.put(Division.OPNAME,Division.class);
		OperationMap.put(DotDivision.OPNAME,DotDivision.class);
		OperationMap.put(DotMultiplication.OPNAME,DotMultiplication.class);
		OperationMap.put(Equal.OPNAME,Equal.class);
		OperationMap.put(Greater.OPNAME,Greater.class);
		OperationMap.put(GreaterEqual.OPNAME,GreaterEqual.class);
		OperationMap.put(Less.OPNAME,Less.class);
		OperationMap.put(LessEqual.OPNAME,LessEqual.class);
		OperationMap.put(Multiplication.OPNAME,Multiplication.class);
		OperationMap.put(Negation.OPNAME,Negation.class);
		OperationMap.put(Power.OPNAME,Power.class);
		OperationMap.put(SQRT.OPNAME,SQRT.class);
		OperationMap.put(Subtraction.OPNAME,Subtraction.class);
		

		HashMap<String,Class<?>> LogicOperationMap=new HashMap<String,Class<?>>();
		LogicOperationMap.put(And.OPNAME,And.class);
		LogicOperationMap.put(Or.OPNAME,Or.class);
		LogicOperationMap.put(Not.OPNAME,Not.class);
		
		// A traiter: Comparison

		if (el.getTagName().equalsIgnoreCase("Operation")) {
			String name=XMLUtils.getTextValue(el,"OpName","");
			Class<?> cl=OperationMap.get(name);
			if (cl!=null) {
				try {
					Element e=(Element)cl.newInstance();
					e.insideFromXml(el,newFunc);
					return e;
				}
				catch (InstantiationException e) {
					System.out.println("Error at instantiation");
				}
				catch (IllegalAccessException e) {
					System.out.println("Error at instantiation 2");
				}
			}
		}
		else if (el.getTagName().equalsIgnoreCase("LogicOperation")) {
			String name=XMLUtils.getTextValue(el,"OpName","");
			Class<?> cl=LogicOperationMap.get(name);
			if (cl!=null) {
				try {
					Element e=(Element)cl.newInstance();
					e.insideFromXml(el,newFunc);
					return e;
				}
				catch (InstantiationException e) {
					System.out.println("Error at instantiation");
				}
				catch (IllegalAccessException e) {
					System.out.println("Error at instantiation 2");
				}
			}
		}
		else if (el.getTagName().equalsIgnoreCase("Case")) {
			Case var=new Case();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("IfThenElse")) {
			IfThenElse var=new IfThenElse();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("LoopFor")) {
			LoopFor var=new LoopFor();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("LoopWhile")) {
			LoopWhile var=new LoopWhile();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("Multiplexer")) {
			Multiplexer var=new Multiplexer();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("Switch")) {
			Switch var=new Switch();
			var.insideFromXml(el,newFunc);
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("SimpleVariable")) {
			SimpleVariable var=new SimpleVariable();
			var.insideFromXml(el,newFunc);
			Element newEl=newFunc.findElement(var.getName());
			// The variable already exist
			if (newEl!=null)
				return newEl;
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("VectorVariable")) {
			VectorVariable var=new VectorVariable();
			var.insideFromXml(el,newFunc);
			Element newEl=newFunc.findElement(var.getName());
			// The variable already exist
			if (newEl!=null)
				return newEl;
			return var;
		}
		else if (el.getTagName().equalsIgnoreCase("GenericOperation")) {
			GenericOperation var=new GenericOperation();
			var.insideFromXml(el,newFunc);
			return var;
			// Should be a generic operation
		}
		return null;
	}
	
	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		name=XMLUtils.getTextValue(el,"Name","");
		monitor=XMLUtils.getBoolValue(el,"Monitor",false);
		if (name.isEmpty())
			return false;
		return true;
	}
	
	public void insideToXml(org.w3c.dom.Element el,org.w3c.dom.Document dom,boolean shortRef) {
		el.appendChild(XMLUtils.createTextElement(dom,"Name",name));
		el.appendChild(XMLUtils.createBoolElement(dom, "Monitor",monitor));
	}
	
	/**
	 * Constructor empty
	 */
	public Element() {
		this.name = new String();
		// Don't monitor the element by default
		this.monitor = false;
	}

	abstract public Element copy(Function newFunc);
	
	public Element findElement(String name) {
		if (this.name.equals(name))
			return this;
		return null;
	}
	 
	protected void copyTo(Element e,Function newFunc) {
		e.name=this.name;
		e.monitor=this.monitor;
	}

	/**
	 * Set the name of the element
	 * @param n Name
	 */
	public void setName(String n) {
		this.name = n;
	}

	/**
	 * Get the name of element
	 * @return Name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Create a string that contains a number of tab
	 * @param nb Tab number
	 * @return String
	 */
	protected String tab(int nb) {
		String sTab = new String();
		for (int i = 0; i < nb; i++) {
			sTab += "\t";
		}
		return sTab;
	}

	/**
	 * Convert the object to a string that contains all the parametres
	 * @param level Indent of the string
	 * @return String
	 */
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[Element]\tName: " + this.name + "\n";
		return s;
	}

	/**
	 * Create the schematic file, use with graphviz to generate schematic
	 * @param level Level of tab
	 * @param prefix Prefix for the element name
	 * @param boxFormat Format of box (rectangle, triangle, ...)
	 * @param color Color of the block
	 * @return Generate string
	 */
	public String toSchematic(int level, String prefix, String boxFormat,
			  String color) {
		String s = new String();
		return s;
	}

	/**
	 * Generate the octave code for the element
	 * @param level Indent level
	 * @return Octave code
	 */
	public String toOctave(int level) {
		String s = new String();
		return s;
	}
	
	/** 
	 * Get the monitor attribute of the element. 
	 * @return the monitor attribute of the element 
	 */
	public boolean getMonitor() { 
		return monitor; 
	} 
	
	/** 
	 * Set the monitor attribute of the element. 
	 * @param monitor The value to the monitor attribute to set 
	 */ 
	public void setMonitor(boolean monitor,StructTreatment struct) {
		if (this.monitor!=monitor)
			struct.modifyMonitor(monitor);
		this.monitor = monitor; 
	}
	
	public void modifyNumType(BuildingBlock.NumType type) {
	}	
}
