/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											SimpleVariable
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description : SimpleVariable
 ******************************************************************************/
package m2m.backend.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import m2m.backend.project.M2MProject;
import m2m.backend.utils.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Describe a VHDL variable
 * Contains 3 parametres, the value, the type and the name (inherits from Element)
 * 
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class SimpleVariable extends Variable {

	public class MapVHDLSimpleVariable
	{
		/**
		 * Multi-use variable at same lowest level
		 */
		private Vector<SimpleVariable> multSimpleVar;
		private int multSimpleVarIndex;
		
		/**
		 * Default contructeur of the private class.
		 */
		public MapVHDLSimpleVariable() {			
			multSimpleVar = new Vector<SimpleVariable>();
			multSimpleVarIndex = 0;
		}		

		/**
		 * Get a multi-use variable that corresponds to a cost
		 * @param cost the corresponding cost to the seached variable
		 * @return the variable that corresponds to the cost
		 */
		public SimpleVariable getMapVHDLSimpleVariable(int cost) {			
			for(int i = multSimpleVarIndex ; i < multSimpleVar.size(); i++){
				if(multSimpleVar.elementAt(i).getCost() == cost) {
					multSimpleVar.add(0, multSimpleVar.elementAt(i));
					multSimpleVar.removeElementAt(i+1);
					multSimpleVarIndex++;
					return multSimpleVar.elementAt(0);
				}
			}			
			return null;
		}		
		
		/**
		 * Get the vector of aviable multi-use SimpleVariable.
		 * @return the vector of aviable multi-use SimpleVariable.
		 */
		public Vector<SimpleVariable> getMultSimpleVar() {
			return multSimpleVar;		
		}
	}
	
	
	/**
	 * Value of the variable
	 */
	private ArrayList<Double> values;
	/**
	 * Type of the variable (use for VHDL generation)
	 */
	private String type;
	/**
	 * Size of the variable, 1 for simple variable, -1 for vector with not
	 * known length and n if length is known
	 */
	private int size;
	/**
	 * Latency cost of the variable
	 */
	private int cost;
	/**
	 * Map a SimpleVaraible with an internal VHDL signal and a function
	 */
	private HashMap<Function, MapVHDLSimpleVariable> variableMap = new HashMap<Function, SimpleVariable.MapVHDLSimpleVariable>();
	/**
	 * All cost of the signal after operation
	 */
	private Vector<Integer> multiCost = new Vector<Integer>();
	/**
	 * All variables dependencies
	 */
	private Vector<Element> variablesDependencies = new Vector<Element>();
	/**
	 * Use simple fifo
	 */
	private boolean simpleFifo = false;
	/**
	 * Use simple fifo
	 */
	private int simpleFifoCost = 0;
	/**
	 * 
	 */
	private HashMap<Function, SimpleVariable> inputLoopVariable = new HashMap<Function, SimpleVariable>();;
	/**
	 * 
	 */
	private SimpleVariable outputLoopVariable;
	/**
	 * Simple variable used for mapping the function
	 */
	private HashMap<Function, SimpleVariable> varFunctionMapping = new HashMap<Function, SimpleVariable>();
	/**
	 * 
	 */
	private HashMap<Function, Integer> loopOutputCost = new HashMap<Function, Integer>();
	/**
	 * 
	 */
	private HashMap<Function, Boolean> loopOutputConnect = new HashMap<Function, Boolean>();

	
	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		size=XMLUtils.getIntValue(el,"Size",0);
	//	simpleFifo=XMLUtils.getBoolValue(el,"SimpleFifo",false);
		type=XMLUtils.getTextValue(el, "Type", "");
		if (name.isEmpty())
			return false;
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Value")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;

				String textVal = inputEl.getFirstChild().getNodeValue();
				values.add(Double.parseDouble(textVal));
			}
			node=node.getNextSibling();
		}

		return true;
	}
	
	public String getXmlTagName() {
		return "SimpleVariable";
	}
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		if (shortRef && (!type.equals("const")))
			el.appendChild(XMLUtils.createTextElement(dom,"Name",name));
		else {
			super.insideToXml(el,dom,shortRef);
			el.appendChild(XMLUtils.createIntElement(dom, "Size",size));
		//	el.appendChild(XMLUtils.createBoolElement(dom, "SimpleFifo", simpleFifo));
			el.appendChild(XMLUtils.createTextElement(dom, "Type", type));
			for(double v : values) {
				el.appendChild(XMLUtils.createDoubleElement(dom, "Value", v));
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public Element copy(Function newFunc) {
		Element e=newFunc.findElement(this.name);
		if (e!=null)
			return e;
		SimpleVariable newVar= new SimpleVariable();
		newVar.name=this.name;
		newVar.values=(ArrayList<Double>)this.values.clone();
		newVar.type=this.type;
		newVar.size=this.size;
		newVar.cost=this.cost;
		newVar.variablesDependencies= this.variablesDependencies;
		return newVar;
	}
	
	/**
	 * Empty constructor.
	 * Set an empty name, the value to 0 and the type to "none"
	 */
	public SimpleVariable() {
		this.name = new String();
		this.values = new ArrayList<Double>();
		this.type = "";
		this.size = 0;
	}

	/**
	 * Constructor, set the name of the object
	 * @param n Name
	 */
	public SimpleVariable(String n) {
		this.name = n;
		this.values = new ArrayList<Double>();
		this.type = "";
		//this.size = 1;
	}

	/**
	 * Constructor, set the value of the object and an empty name
	 * @param v Value
	 */
	public SimpleVariable(double v) {
		this.values = new ArrayList<Double>();
		this.values.add(v);
		this.name = "";
		this.type = "";
		this.size = 1;
	}

	public SimpleVariable(ArrayList<Double> v) {
		this.values = v;
		this.name = "";
		this.type = "";
		this.size = v.size();
	}

	/**
	 * Constructor, set the name and the value.
	 * @param n Name
	 * @param v Value
	 */
	public SimpleVariable(String n, double v) {
		this.values = new ArrayList<Double>();
		this.values.add(v);
		this.name = n;
		this.type = "";
	}

	/**
	 * Constructor, the new object contains values of the parametre variable
	 * @param var SimpleVariable to copy
	 */
	public SimpleVariable(SimpleVariable var) {
		this.values = new ArrayList<Double>(var.values);
		this.type = var.getType();
		this.name = var.getName();
		this.size = var.size;
	}

	/**
	 * Construct a complete variable
	 * @param n Name
	 * @param v Value
	 * @param t Type
	 */
	public SimpleVariable(String n, double v, String t) {
		this.values = new ArrayList<Double>();
		this.values.add(v);
		this.name = n;
		if (t == null) {
			this.type = "";
		} else {
			this.type = t;
		}
		this.size = 1;
	}

	public SimpleVariable(String n, ArrayList<Double> v, String t) {
		this.values = v;
		this.name = n;
		this.type = t;
		this.size = v.size();
	}

	/**
	 * Setter of value
	 * @param v New value
	 */
	public void setVal(double v) {
		this.values.clear();
		this.values.add(v);
		this.size = 1;
	}

	public void setVal(ArrayList<Double> v) {
		this.values = v;
		this.size = v.size();
	}

	/**
	 * Getter of value
	 * @return Value
	 */
	@Override
	public double getVal() {
		if (this.values == null || !this.values.isEmpty()) {
			return this.values.get(0);
		}
		return Double.NaN;
	}

	public double getVal(int pos) {
		if (pos < this.values.size()) {
			this.values.get(pos);
		}
		return Double.NaN;
	}

	public ArrayList<Double> getValues() {
		return this.values;
	}

	public void addVal(double v) {
		this.values.add(v);
		this.size++;
	}

	/**
	 * Setter of type
	 * @param t Nouveau type
	 */
	@Override
	public void setType(String t) {
		this.type = t;
	}

	/**
	 * Getter of type
	 * @return Type
	 */
	public String getType() {
		return this.type;
	}

	public void setSize(int n) {
		this.size = n;
	}

	public int getSize() {
		return this.size;
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += this.tab(level) + "[SimpleVariable]";
		if (!this.type.equals("const")) {
			s += "\tName: " + this.name + "\tType: " + this.type;
		} else {
			s += "\tName: " + this.name + "\tVal: " + this.values;
		}
		s += "\tSize: " + this.size;
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		if (prefix.isEmpty()) {
			prefix = "var";
		}
		if (this.type.equals("const")) {
			s += "\n" + tab(level) + prefix + "[shape=\"" + boxFormat + "\" label = \"" + this.getName() + ":\\n" + this.getVal() + "\" color=\"" + color + "\" style=\"filled\"]";
		} else {
			s += "\n" + tab(level) + prefix + "[shape=\"" + boxFormat + "\" label = \"" + this.getName() + "\" color=\"" + color + "\" style=\"filled\"]";
		}
		return s;
	}
	
	
	/**
	 * Set the latency cost of the variable.
	 * @param cost the latency cost to set to the variable.
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
	/**
	 * Get the latency cost of the variable.
	 * @return the latency cost of the variable
	 */
	public int getCost() {
		return this.cost;
	}
	
	
	/**
	 * Get the multiCostBeforeOp vector of the variable.
	 * @return the multiCostBeforeOp vector of the variable
	 */
	public Vector<Integer> getMultiCost() {
		return multiCost;
	}

	
	/**
	 * Get the varaibleMap of the variable.
	 * @return the varaibleMap of the variable
	 */
	public HashMap<Function, MapVHDLSimpleVariable> getVariableMap() {
		return variableMap;
	}
	
	
	/**
	 * Get the variablesDependencies of the variable.
	 * @return the variablesDependencies of the variable
	 */
	public Vector<Element> getVariableDependencies() {
		return variablesDependencies;
	}
	
	
	/**
	 * Set the simpleFifo attribut of the variable.
	 * @param value the value to set to the attribut
	 */
	public void setSimpleFifo(boolean value) {
		simpleFifo = value;
	}
	
	
	/**
	 * Get the simpleFifo attribut of the variable.
	 * @return the simpleFifo attribut of the variable.
	 */
	public boolean getSimpleFifo() {
		return simpleFifo;
	}
	
	
	/**
	 * Get the simpleFifoCost attribut of the variable.
	 * @return the simpleFifoCost attribut of the variable.
	 */
	public int getSimpleFifoCost(M2MProject project) {
		if(project.getOptimisationProperties().getOptimizeFifo())
			return simpleFifo ? simpleFifoCost+1 : simpleFifoCost;
		else
			return project.getOptimisationProperties().getFifoSize()-2;
	}
	
	
	/**
	 * Set the simpleFifoCost attribut of the variable.
	 * @param value the value to set to the attribut
	 */
	public void setSimpleFifoCost(int value) {
		simpleFifoCost = value;
	}
	
	
	/**
	 * Set the output loop variable of the simple variable
	 * @param var The variable to set
	 */
	public void setOutputLoopVariable(SimpleVariable var){
		outputLoopVariable = var;
	}
	
	/**
	 * Get the output loop variable of the simple variable
	 * @return the loop variable of the simple variable
	 */
	public SimpleVariable getOutputLoopVariable(){
		return outputLoopVariable;
	}
	
	/**
	 * Set the input loop variable of the simple variable
	 * @param var The variable to set
	 */
	public void setInputLoopVariable(SimpleVariable var, Function f){
		inputLoopVariable.put(f, var);
	}
	
	/**
	 * Get the input loop variable of the simple variable
	 * @return the loop variable of the simple variable
	 */
	public SimpleVariable getInputLoopVariable(Function f){
		return inputLoopVariable.get(f);
	}

	/**
	 * Set the simple variable used for mapping the function
	 * @param var
	 */
	public void setVarFunctionMapping(SimpleVariable var, Function f) {
		varFunctionMapping.put(f, var);
	}

	/**
	 * Get the simple variable used for mapping the function
	 *  @return the simple variable used for mapping the function
	 */
	public SimpleVariable getVarFunctionMapping(Function f) {
		return varFunctionMapping.get(f);
	}

	/**
	 * @param value
	 */
	public void setLoopOutputCost(Function f, int value) {
		loopOutputCost.put(f, value);
	}

	/**
	 * @return
	 */
	public int getLoopOutputCost(Function f) {
		if(loopOutputCost.get(f) != null)
			return loopOutputCost.get(f);
		else
			return 0;
	}

	
	/**
	 * @param value
	 */
	public void setLoopOutputConnect(Function f, boolean value) {
		loopOutputConnect.put(f, value);
	}
	
	
	/**
	 * @return
	 */
	public boolean getLoopOutputConnect(Function f){
		if(loopOutputConnect.get(f) != null)
			return loopOutputConnect.get(f);
		else
			return false;
	}
}
