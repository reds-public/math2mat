/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											SQRT
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 29 avr. 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

/**
 *	Describe a square root
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class SQRT extends Operation {

	final static String OPNAME="sqrt";
	
	public SQRT() {
		super(OPNAME);
	}

	@Override
	public void copyTo(Element e,Function newFunc) {
		SQRT newElement=(SQRT)e;
		super.copyTo(newElement,newFunc);
	}
	
	@Override
	public Element copy(Function newFunc) {
		SQRT newElement=new SQRT();
		this.copyTo(newElement,newFunc);
		return newElement;
	}
	
	@Override
	public String getOpSymbol() {
		return "sqrt";
	}

	@Override
	public String toOctave(int level) {
		String sOctave = new String();
		sOctave += tab(level) + this.input.firstElement().getName();
		if (this.output.firstElement() instanceof Variable) {
			if (this.output.firstElement().getName().isEmpty()) {
				sOctave += "\t= sqrt(" + ((Variable)this.output.firstElement()).getVal() + ")";
			} else {
				sOctave += "\t= sqrt(" + this.output.firstElement().getName() + ")";
			}
		}
		return sOctave;
	}
}
