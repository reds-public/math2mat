/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file Loop.java
 * @author Molla Daniel
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 24 avr. 2012
 *
 * Author: Molla Daniel
 *
 * Description: Describe a loop.
 * 
 */
package m2m.backend.structure;
import java.util.Vector;


public abstract class Loop extends Function
{
	/**
	 * Loop variables
	 */
	private Vector<SimpleVariable> loopVariables;
	
	
	/**
	 * Set loop variables of the loop for.
	 * @param vector Loop variables to set to the loop for
	 */
	public void setLoopVariables(Vector<SimpleVariable> vector) {
		loopVariables = vector;
	}
	
	
	/**
	 * Get all loop variables of the loop for.
	 * @return all loop variables of the loop for
	 */
	public Vector<SimpleVariable> getLoopVariables() {
		return loopVariables;
	}
	
	
	/**
	 * Get the loop variable if the parameter is a loop output variable.
	 * @param var the loop output variable
	 * @return the loop variable
	 */
	public SimpleVariable getLoopVariable(SimpleVariable var) 
	{	
		for(Element e : body) {
			if(e instanceof IfThenElse && e.getName().contains("loop"))
				// TODO if(((SimpleVariable)((IfThenElse)e).getOutput().firstElement()).equals(var))
				if(((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getOutputAt(0)).equals(var))
					return ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getInputAt(0));
		}
			
		return null;
	}
	
	
	/**
	 * Get the init variable if the parameter is a loop output variable.
	 * @param var the loop output variable
	 * @return the init variable
	 */
	public SimpleVariable getInitVariable(SimpleVariable var) 
	{	
		for(Element e : body) {			
			if(e instanceof IfThenElse && e.getName().contains("loop"))
				//TODO if(((SimpleVariable)((IfThenElse)e).getOutput().firstElement()).equals(var))
				if(((SimpleVariable)((Assignment)((IfThenElse)e).getBodyFalse().firstElement()).getOutputAt(0)).equals(var))
					return ((SimpleVariable)((Assignment)((IfThenElse)e).getBodyTrue().firstElement()).getInputAt(0));
		}
			
		return null;
	}
	
	
	/**
	 * Check if a variable is an init variable of a loop.
	 * @param var the loop variable to check
	 * @return the ifThenElse of the init variable
	 */
	public IfThenElse isInitVariable(SimpleVariable var) 
	{	
		for(Element e : body) {
			if(e instanceof IfThenElse && e.getName().contains("loop"))
				if(((SimpleVariable)((Assignment)((IfThenElse)e).getBodyTrue().firstElement()).getInputAt(0)).equals(var))
					return (IfThenElse)e;
		}
			
		return null;
	}
}
