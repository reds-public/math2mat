/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											Operation
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 4 mars 2009
 * Description : Operation
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Vector;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import m2m.backend.buildingblocks.*;
import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.utils.XMLUtils;

/**
 *	Describe an operation
 *
 * @param <T> Type of element (Operation or SimpleVariable)
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public abstract class Operation extends Element {

	/**
	 * Operand list
	 */
	protected Vector<Element> input;
	protected Vector<Element> output;
	/**
	 * Define what material block use
	 */
	protected BuildingBlock block;
	/**
	 * Save the name of the operation (add, sub, mult, etc)
	 */
	protected String opName;
	protected boolean generic = false;
	

	public String getXmlTagName() {
		return "Operation";
	}
	

	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Input")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							input.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}
	
			if (node.getNodeName().equalsIgnoreCase("Output")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							output.add(element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}
			node=node.getNextSibling();
		}

		String bl=XMLUtils.getTextValue(el,"Block","");
		
		BuildingBlocksManager manager = BuildingBlocksManager.getInstance();
		List<BuildingBlock> list = null;
		block=null;
		if (this.opName != null) {
			list = manager.blockNamed(this.opName);
			for (BuildingBlock b : list) {
				if (b.getClass().getName().equals(bl)) {
					block=b;
				}
			}
		}
		return true;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		super.insideToXml(el,dom, shortRef);
		el.appendChild(XMLUtils.createTextElement(dom, "OpName",opName));
		el.appendChild(XMLUtils.createBoolElement(dom, "Generic", generic));
		if (block!=null)
			el.appendChild(XMLUtils.createTextElement(dom, "Block", block.getClass().getName()/*block.toString()*/));
		org.w3c.dom.Element inputEl=dom.createElement("Input");
		for(Element e: input) {
			inputEl.appendChild(e.toXml(dom, shortRef));
		}
		el.appendChild(inputEl);
		org.w3c.dom.Element outputEl=dom.createElement("Output");
		for(Element e: output) {
			outputEl.appendChild(e.toXml(dom, shortRef));
		}
		el.appendChild(outputEl);
	}

	/**
	 * Construct an empty operation
	 */
	public Operation() {
		this.input = new Vector<Element>();
		this.output = new Vector<Element>();
		this.block = null;
		this.opName = "void";
	}
	
	public Element findElement(String name) {
		Element result=super.findElement(name);
		if (result!=null)
			return result;
		for(Element e : input) {
			result=e.findElement(name);
			if (result!=null)
				return result;
		}
		for(Element e : output) {
			result=e.findElement(name);
			if (result!=null)
				return result;
		}
		return null;
	}
	
	public void copyTo(Element e,Function newFunc) {
		Operation op=(Operation)e;
		super.copyTo(op, newFunc);
		op.opName=this.opName;
		op.block=this.block;
		for(Element el: input) {
			Element found=newFunc.findElement(el.getName());
			if(el.getName().contains("Add"))
				found= new SimpleVariable(el.getName(), 0.0, "");
			op.input.add(found);
		}

		for(Element el: output) {
			Element found=newFunc.findElement(el.getName());
			if(el.getName().contains("Add"))
				found= new SimpleVariable(el.getName(), 0.0, "");
			op.output.add(found);
		}
	}
	
/*	public Element copy(Function newFunc) {
		Operation op=new Operation();
		
		super.copyTo(op, newFunc);
		op.opName=this.opName;
		op.block=this.block;
		for(Element e: input) {
			Element found=newFunc.findElement(e.getName());
			op.input.add(found);
		}

		for(Element e: output) {
			Element found=newFunc.findElement(e.getName());
			op.output.add(found);
		}
		return op;
	}
*/
	
	public Operation(String opName) {
		this();
		this.opName = opName;
		this.block = this.getFirstPipelined(NumType.FLOAT32);
	}

	public Operation(String opName, boolean gen) {
		this();
		this.generic = gen;
		this.opName = opName;
		this.block = this.getFirstPipelined(NumType.FLOAT32);
	}

	/**
	 * Get the first pipelined material block if exist, or the first block
	 * who correspond to the opName
	 * @return Pointer on the block
	 */
	public BuildingBlock getFirstPipelined(NumType type) {
		BuildingBlocksManager manager = BuildingBlocksManager.getInstance();
		List<BuildingBlock> list = null;
		if (this.opName != null) {
			list = manager.blockNamed(this.opName);
			for (BuildingBlock b : list) {
				if (b.numType()==type)
				if (b.implType() == BuildingBlock.ImplType.PIPELINE) {
					return b;
				}
			}
			return this.getFirst();
		}
		return null;
	}
	
	public void modifyNumType(BuildingBlock.NumType type) {
		setBlock(this.getFirstPipelined(type));
	}	

	/**
	 * Get the first material block who correspond to the opName
	 * @return Pointer on the block
	 */
	public BuildingBlock getFirst() {
		BuildingBlocksManager manager = BuildingBlocksManager.getInstance();
		List<BuildingBlock> list = null;
		if (this.opName != null) {
			list = manager.blockNamed(this.opName);
			if (!list.isEmpty()) {
				return list.get(0);
			}
		}
		return null;
	}

	/**
	 * Add an operand to the operand list
	 * @param op Operand
	 */
	public void addInput(Element op) {
		this.input.add(op);
	}

	/**
	 * Set the operand list
	 * @param op Operand list
	 */
	public void setInput(Vector<Element> op) {
		this.input = op;
	}

	/**
	 * Set the operand to the wanted position
	 * @param i Position
	 * @param op Operand
	 */
	public void setInputAt(int i, Element op) {
		this.input.set(i, op);
	}

	/**
	 * Get the operand list
	 * @return Operand list
	 */
	public Vector<Element> getInput() {
		return this.input;
	}

	/**
	 * Get the number of operand
	 * @return Number of operand
	 */
	public int getNbInput() {
		return this.input.size();
	}

	/**
	 * Get the operand to the wanted position
	 * @param i Position
	 * @return Operand
	 */
	public Element getInputAt(int i) {
		Element el = null;
		try {
			el = this.input.elementAt(i);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return el;
	}

	/**
	 * Add an operand to the operand list
	 * @param op Operand
	 */
	public void addOutput(Element op) {
		this.output.add(op);
	}

	/**
	 * Set the operand list
	 * @param op Operand list
	 */
	public void setOutput(Vector<Element> op) {
		this.output = op;
	}

	/**
	 * Set the operand to the wanted position
	 * @param i Position
	 * @param op Operand
	 */
	public void setOutputAt(int i, Element op) {
		this.output.set(i, op);
	}

	/**
	 * Get the operand list
	 * @return Operand list
	 */
	public Vector<Element> getOutput() {
		return this.output;
	}

	/**
	 * Get the number of operand
	 * @return Number of operand
	 */
	public int getNbOutput() {
		return this.output.size();
	}

	/**
	 * Get the operand to the wanted position
	 * @param i Position
	 * @return Operand
	 */
	public Element getOutputAt(int i) {
		Element el = null;
		try {
			el = this.output.elementAt(i);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return el;
	}

	/**
	 * Set the block to use
	 * @param block Block to use
	 */
	public void setBlock(BuildingBlock block) {
		this.block = block;
	}

	/**
	 * Get the block
	 * @return Block
	 */
	public BuildingBlock getBlock() {
		return this.block;
	}

	/**
	 * Set the name of the operation
	 * @param name Name
	 */
	public void setOpName(String name) {
		this.opName = name;
	}

	/**
	 * Get the name of operation
	 * @return Name
	 */
	public String getOpName() {
		return this.opName;
	}

	/**
	 * Get the operation symbol of the operation
	 * @return the operation symbol
	 */
	public String getOpSymbol() {
		return "op";
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[" + ((this.generic) ? "fun_" : "")
				  + this.opName + "]\tName: " + this.name;
		if (this.block != null) {
			s += tab(level) + "BlockName : " + this.block.entityName();
		}
		s += "\n" + this.toStringParam(level + 1);
		return s;
	}
	
	/**
	 * Convert input and output to String
	 * @param level Indent of the string
	 * @return String
	 */
	protected String toStringParam(int level) {
		String s = new String();
		if (!this.output.isEmpty()) {
			s += tab(level) + "Output:";
			for (Element el : this.output) {
				if (el != null) {
					s += "\n" + el.toString(level + 1);
				}
			}
		}
		if (!this.input.isEmpty()) {
			s += "\n" + tab(level) + "Input:";
			for (Element el : this.input) {
				if (el != null) {
					s += "\n" + el.toString(level + 1);
				}
			}
		}
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix, String boxFormat, String color) {
		String s = new String();
		if (prefix.isEmpty()) {
			prefix = "top";
		}
		int cpt = 0;
		s += tab(level) + prefix + "[shape = \"" + boxFormat + "\", label = \""
				  + ((this.generic) ? "fun_" : "") + this.opName + " : "
				  + this.getName() + "\" color=\"" + color + "\" style=\"filled\"]";
		if (!this.input.isEmpty()) {
			cpt = 0;
			for (Element var : this.input) {
				if (var instanceof Variable) {
					s += var.toSchematic(level, prefix + "_in_" + cpt,
							  GraphParams.inputShape, GraphParams.inputColor);
				} else {
					s += var.toSchematic(level, prefix + "_in_" + cpt,
							  boxFormat, GraphParams.defaultColor);
				}
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_in_" + cpt;
				cpt++;
			}
		}
		if (!this.output.isEmpty()) {
			cpt = 0;
			for (Element var : this.output) {
				if (var instanceof Variable) {
					s += var.toSchematic(level, prefix + "_out_" + cpt,
							  GraphParams.outputShape,
							  GraphParams.outputColor);
				} else {
					s += var.toSchematic(level, prefix + "_out_" + cpt,
							  boxFormat, GraphParams.defaultColor);
				}
				s += "\n" + tab(level) + prefix + " -> " + prefix + "_out_" + cpt;
				cpt++;
			}
		}
		return s;
	}

	/**
	 * Generate the octave code corresponding to the operation, the operator is
	 * set by the parameter op.
	 * @param level Tabulation level
	 * @param op Operator
	 * @return String
	 */
	public String toOctave(int level, String op) {
		String sOctave = new String();
		Element out = this.output.firstElement();
		sOctave += tab(level) + out.getName() + "\t= ";
		boolean first = true;
		for (Element e : this.input) {
			if (e instanceof SimpleVariable) {
				sOctave += ((first) ? "" : " " + op + " ")
						  + ((((SimpleVariable)e).getType().equals("const")) ? ((SimpleVariable) e).getVal() : e.getName());
			}
			first = false;
		}
		/// If octave need to display the resultat of the operation
		if (!monitor) {
			sOctave += ";";
		}
		if (out instanceof SimpleVariable & out.monitor) 
		{	
			sOctave += "\n" + tab(level) + "for j_m2m_regen=1:length(interns)";
			sOctave += "\n" + tab(level+1) + "if(length(interns(j_m2m_regen).sname) == length(\"" + out.getName() +"\") && interns(j_m2m_regen).sname == \"" + out.getName() + "\")";
			sOctave += "\n" + tab(level+2) + "interns(j_m2m_regen).values(i_m2m_regen,:) = " + out.getName() + ";";
			sOctave += "\n" + tab(level+1) + "endif";
		    sOctave += "\n" + tab(level) + "endfor"; 
		}
		return sOctave;
	}
}
