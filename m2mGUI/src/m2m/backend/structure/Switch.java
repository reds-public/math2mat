/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/******************************************************************************
 *											IfThenElse
 ******************************************************************************
 * Auteur		: Trolliet Gregory
 * Date			: 7 avr. 2009
 * Description :
 ******************************************************************************/
package m2m.backend.structure;

import java.util.Vector;

import m2m.backend.buildingblocks.BuildingBlock;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *	Describe a conditional structure if/then/else
 *
 * @author Trolliet Gregory
 * @author <a href="mailto:gregory.trolliet@hesge.ch">gregory.trolliet@hesge.ch</a>
 * @version 0.1
 */
public class Switch extends Function {

	/**
	 * List of the different case
	 */
	private Vector<Case> cases;
	/**
	 * Variable on which to test
	 */
	private Variable switchVar;

	/**
	 * Constructor empty
	 */
	public Switch() {
		this.setName("switch");
		this.cases = new Vector<Case>();
	}
	

	public void modifyNumType(BuildingBlock.NumType type) {
		super.modifyNumType(type);
		for(Element e: cases) {
			e.modifyNumType(type);
		}
	}

	public String getXmlTagName() {
		return "Switch";
	}


	public boolean insideFromXml(org.w3c.dom.Element el,Function newFunc) {
		super.insideFromXml(el,newFunc);
		
		Node node = el.getFirstChild();
		while(node!=null) {
			if (node.getNodeName().equalsIgnoreCase("Cases")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							cases.add((Case)element);
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			if (node.getNodeName().equalsIgnoreCase("SwitchVar")) {
				org.w3c.dom.Element inputEl=(org.w3c.dom.Element)node;
				Node nodeIn = inputEl.getFirstChild().getNextSibling();
				while(nodeIn!=null) {
					if (nodeIn.getNodeType()==org.w3c.dom.Node.ELEMENT_NODE) {
						Element element=getElement((org.w3c.dom.Element)nodeIn,newFunc);
						if (element!=null)
							switchVar=(Variable)element;
					}
					nodeIn=nodeIn.getNextSibling();
				}
			}

			node=node.getNextSibling();
		}
		return false;
	}
	
	public void insideToXml(org.w3c.dom.Element el,Document dom,boolean shortRef) {
		// This function replaces the one of Function, so we call Element instead
		// of function
//		superInsideToXml(el,dom,shortRef);
		super.insideToXml(el, dom, shortRef);
		
		org.w3c.dom.Element casesEl=dom.createElement("Cases");
		for(Element e: inputVars) {
			casesEl.appendChild(e.toXml(dom,false));
		}
		el.appendChild(casesEl);
		
		org.w3c.dom.Element switchVarEl=dom.createElement("SwitchVar");
		switchVarEl.appendChild(switchVar.toXml(dom,false));
		el.appendChild(switchVarEl);
	}

	/**
	 * Set the switch variable
	 * @param var Switch variable
	 */
	public void setSwitchVar(Variable var) {
		this.switchVar = var;
	}

	/**
	 * Get the switch variable
	 * @return Switch variable
	 */
	public Variable getSwitchVar() {
		return this.switchVar;
	}

	/**
	 * Add a new case to the switch
	 * @param cond Condition
	 * @param body Instructions
	 */
	public void addCase(Vector<Variable> cond, Vector<Element> body) {
		Case c = new Case();
		c.setBody(body);
		c.setCond(cond);
		this.cases.add(c);
	}

	/**
	 * Add a new case to the switch
	 * @param cas New case
	 */
	public void addCase(Case cas) {
		this.cases.add(cas);
	}

	/**
	 * Set all the cases
	 * @param cases Cases
	 */
	public void setCases(Vector<Case> cases) {
		this.cases = cases;
	}

	/**
	 * Get all the cases
	 * @return Cases
	 */
	public Vector<Case> getCases() {
		return this.cases;
	}

	/**
	 * Get the case at the selected position
	 * @param ind Position
	 * @return Case
	 */
	public Case getCaseAt(int ind) {
		if (ind < 0 || ind >= this.cases.size()) {
			return null;
		}
		return this.cases.elementAt(ind);
	}

	/**
	 * Get the number of cases
	 * @return Number of cases
	 */
	public int getSize() {
		return this.cases.size();
	}

	@Override
	public String toString(int level) {
		String s = new String();
		s += tab(level) + "[Switch]\tName: " + this.name;
		if (this.switchVar != null) {
			s += "\n" + tab(level + 1) + "Switch Var:";
			s += "\n" + this.switchVar.toString(level + 2);
		}
		s += "\n" + tab(level + 1) + ">>===CASES===<<";
		for (Case cas : this.cases) {
			s += "\n" + cas.toString(level + 2);
		}
		return s;
	}

	@Override
	public String toSchematic(int level, String prefix,
			  String boxFormat, String color) {
		String s = new String();
		int cpt = 0;
		s += tab(level) + prefix + "[shape = " + boxFormat + ", label = \""
				  + this.getName() + "\" color=\"" + color + "\" style=\"filled\"]";
		if (this.switchVar != null) {
			s += "\n" + tab(level) + prefix + "_switchVar [shape = "
					  + GraphParams.inputShape + ", label = \""
					  + this.switchVar.getName() + "\" color=\""
					  + GraphParams.inputColor + "\" style=\"filled\"]";
			s += "\n" + tab(level) + prefix + "->" + prefix
					  + "_switchVar [label = \"Sel\"]";
		}
		for (Case c : this.cases) {
			s += "\n" + c.toSchematic(level, prefix + "_case_" + cpt, boxFormat, color);
			s += "\n" + tab(level) + prefix + " -> " + prefix + "_case_" + cpt;
			cpt++;
		}
		return s;
	}
}
