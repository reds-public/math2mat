/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file Errors.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Dec 9, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.backend.processing;

/**
 * @author ythoma
 *
 */
public class Errors {
	
	static private Errors lastError=new Errors();
	
	private ErrorNum errorNum;
	
	private String message;
	
	static public Errors lastError() {
		return lastError;
	}
	
	public ErrorNum getNum() {
		return errorNum;
	}
	
	public String errorMessage() {
		switch (errorNum) {
		case QUESTALICENSE: return "Can not find QuestaSim license. Please check your license.";
		case QUESTANOTFOUND: return "Can not find vsim, the executable of QuestaSim. Please check the external tools properties.";
		case OCTAVENOTFOUND: return "Can not find Octave. Please check the external tools properties.";
		case ISENOTFOUND: return "Can not find Xilinx tools. Please check the external tools properties.";
		case UBIDULECONNECTIONERROR: return "Can not connect to Ubidule. Please check the connection.";
		case DOTNOTFOUND: return "Can not find dot tool. Please check the external tools properties.";
		case OCTAVEGEN: return "Can not run Octave generation. Please save the projet before launching octave generation.";
		case DIVERSE: return message;
		}
		return "Strange error...";
	}
	
	static public void setLastError(ErrorNum error) {
		lastError.errorNum=error;
	}
	
	static public void setLastErrorMessage(String mess) {
		lastError.message = mess;
		lastError.errorNum = ErrorNum.DIVERSE;
	}

	static public void clearError() {
		lastError.errorNum = ErrorNum.NOERROR;
	}

	public enum ErrorNum {
		NOERROR,
		OCTAVELICENSE,
		OCTAVENOTFOUND,
		QUESTALICENSE,
		QUESTANOTFOUND,
		ISENOTFOUND,
		UBIDULECONNECTIONERROR,
		DOTNOTFOUND,
		OCTAVEGEN,
		DIVERSE
	}
}
