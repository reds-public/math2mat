/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ProgressMonitoring.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Aug 5, 2011
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.backend.processing;

import m2m.backend.project.M2MProject;

/**
 * @author ythoma
 *
 */
public class ProgressMonitoring {

	public ProgressMonitoring() {	
	}
	
	public void workedVhdlGeneration() {	
	}
	
	public void workedOctave() {
	}
	
	public void workedCompilation() {
	}
	
	public void workedSimulationStep() {
	}
	
	public void workedOneProject() {
	}
	
	public void subTask(M2MProject project,String taskName) {
	}
	
	public boolean isCanceled() {
		return false;
	}
	
	
	static private final int TIMEVHDL=20;
	
	static public int workingTimeVHDL() {
		return TIMEVHDL;
	}
	
	static private final int TIMEOCTAVE = 80;
	
	static public int workingTimeOctave() {
		return TIMEOCTAVE;
	}
	
	static private final int TIMECOMPILATION = 100;
	
	static public int workingTimeCompilation() {
		return TIMECOMPILATION;
	}
	
/*
	static private final int TIMEOCTAVEFILE = 20;

	static int workingTimeOctave(M2MProject project) {
		return TIMEOCTAVEFILE*project.getStructTreatment().getInput().size()+
		TIMEOCTAVEFILE*project.getStructTreatment().getOutput().size();
	}
	*/
	
	static final int TIMESIMULATIONSTEP = 10;
	
	static public int workingTimeSimulationStep() {
		return TIMESIMULATIONSTEP;
	}
	
	static public int workingTimeSimulation(M2MProject project) {
		return (int)(TIMESIMULATIONSTEP*(double)project.getSimulationProperties().getNumberOfSamples()/50);
	}

	static public int workingTimeAll(M2MProject project) {
		return workingTimeCompilation()+
			   workingTimeSimulation(project)+
			   workingTimeOctave()+
			   workingTimeVHDL();
	}
}
