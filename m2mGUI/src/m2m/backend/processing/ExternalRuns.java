/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ExternalRuns.java
 * @author Sebastien Masle, Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Nov 24, 2010
 *
 * Author: Sebastien Masle, Yann Thoma
 *
 * Description: This class is used to launch the execution of QuestaSim and Octave.
 *              It is also used to destroy the processes if something went wrong.
 * 
 */
package m2m.backend.processing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

//import org.eclipse.ui.console.MessageConsoleStream;
import m2m.backend.buildingblocks.BuildingBlock;
import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.processing.Errors;
import m2m.backend.processing.Errors.ErrorNum;
import m2m.backend.processing.SystemVerilogCreator;
import m2m.backend.project.M2MProject;
import m2m.backend.project.SimulationResult;
import m2m.backend.structure.StructTreatment;
import m2m.backend.utils.FileUtils;
import m2m.backend.verifpga.ISETCLGenerator;
import m2m.backend.verifpga.M2MClient;
import m2m.backend.project.ExternalToolsProperties;

/**
 * @author ythoma
 *
 */
public class ExternalRuns {

	private static ExternalRuns instance=null;
	
	public static ExternalRuns getInstance() {
		if (instance==null)
			instance = new ExternalRuns();
		return instance;
	}
	
	
	private ExternalRuns() {
	}
	
	
	static public boolean runAll(M2MProject project) {

		project.checkFolders();
		project.monitoring.subTask(project,"Generating VHDL...");
		if (!project.generateVHDL()) {
			m2m.backend.processing.Errors.setLastErrorMessage("VHDL files could not be created. Please check the Math2mat library location with the menu Configure...External tools.");
			return false;
		}
		project.monitoring.workedVhdlGeneration();
			project.monitoring.subTask(project,"Generating Octave...");
		if (project.getStructTreatment().getMonitor()) {
			if (!project.reGenerateOctave())
				return false;
					project.monitoring.subTask(project,"Running Octave...");
			if (!runOctave(project, true))
				return false;
		}
		else {
					project.monitoring.subTask(project,"Running Octave...");
			if (!runOctave(project,false))
				return false;
		}
		project.monitoring.workedOctave();
		if (!runQuesta(project))
			return false;
		
		return true;
	}
	
	
	static public boolean runOctave(M2MProject project, boolean codeRegen) {
		
		if(project.isModified()) {
			Errors.setLastError(Errors.ErrorNum.OCTAVEGEN);
			return false;
		}
		
		final String projectPath = project.getProperties().getProjectPath();
		final ExternalToolsProperties externalToolsProperties = ExternalToolsProperties.getReference();
		

		BufferedReader brOctave;
		BufferedReader brErrOctave;
		
		String line = "";
		
		Process process = null;
		
		//copy the necessary m2m files into the m2m source directory
		FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_doall.m", project.getOctavePath()+"/m2m_doall.m");
		FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_generatesamples.m", project.getOctavePath()+"/m2m_generatesamples.m");
		if (project.getProperties().getOptimisationProperties().getOptimisationDataType()==BuildingBlock.NumType.FLOAT32) {
			FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_readvarfromfile.m", project.getOctavePath()+"/m2m_readvarfromfile.m");
			FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_writevartofile.m", project.getOctavePath()+"/m2m_writevartofile.m");
		}
		else if (project.getProperties().getOptimisationProperties().getOptimisationDataType()==BuildingBlock.NumType.FLOAT64) {
			FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_readvarfromfile_float64.m", project.getOctavePath()+"/m2m_readvarfromfile.m");
			FileUtils.copyFile(M2MProject.getLibPath()+"/m2m/m2m_writevartofile_float64.m", project.getOctavePath()+"/m2m_writevartofile.m");	
		}
		else {
			System.out.println("Error with the type of data to be processed.");
		}
		StructTreatment struct = project.getStructTreatment();
		OctaveScriptCreator octaveScriptCreator = new OctaveScriptCreator(struct.getTop(), project.getSimulationProperties(), codeRegen);
		octaveScriptCreator.createOctaveScript(project, project.getSourceFile());
		
		project.checkFolders();
		
		/* Execute the "octave" command */
		try
		{
			System.out.println("Starting Octave...\n");
			if (externalToolsProperties.getOctavePath() != null)
				process = Runtime.getRuntime().exec(externalToolsProperties.getOctavePath()+" -q");
			else
//				if (System.getProperty("os.name").contains("Windows"))
//					process = Runtime.getRuntime().exec("octave.exe");
//				else if (System.getProperty("os.name").contains("Linux"))
					process = Runtime.getRuntime().exec("octave -q");
					
				ProcessWatcher.getInstance().addProcess(process);
				//create streams to communicate with Octave application
				BufferedWriter bwOctave;
				bwOctave  = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
				brOctave = new BufferedReader(new InputStreamReader(process.getInputStream()));
				brErrOctave = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			
				try {
					bwOctave.write("cd "+"\""+project.getOctavePath()+"\""+"\n");
					bwOctave.flush();

					File m2mScript = new File(project.getOctavePath()+"/m2m_doall.m");
					if (!m2mScript.exists()) {
						System.err.println("Octave script error: cannot find \"m2m_doall.m\" script in "+projectPath);
						return false;
					}
					
					bwOctave.write("m2m_doall\n");
					bwOctave.flush();
					
					bwOctave.write("quit\n");
					bwOctave.flush();
					
					bwOctave.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (brOctave == null || brErrOctave == null)
					return false;
 
				while (true) {
					try {
						if ((line = brOctave.readLine()) != null)
							System.out.println(line);
						else
							break;
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				while (true) {
					try {
						if ((line = brErrOctave.readLine()) != null)
							System.err.println(line);
						else {
							System.out.println("Octave generation finished");
							project.setLastMonitoring(project.getStructTreatment().getMonitor());
							break;
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				
		} catch (IOException ioe) {
			System.err.println("Error launching Octave\nOctave path is not well configured, configure it correctly before continue");
			Errors.setLastError(ErrorNum.OCTAVENOTFOUND);
			return false;
		}

		ProcessWatcher.getInstance().removeProcess(process);
		
		return true;
	}
	
	static public boolean runDot(String dotFile,String pngFile) {

		Process dotProcess = null;

		try {
			String cmdArray[]= new String[5];
			cmdArray[0]=ExternalToolsProperties.getReference().getDotPath();
			cmdArray[1]="-Tpng";
			cmdArray[2]=dotFile;
			cmdArray[3]="-o";
			cmdArray[4]=pngFile;
			dotProcess = new ProcessBuilder(cmdArray[0],cmdArray[1],cmdArray[2],cmdArray[3],cmdArray[4]).start();

		} catch (IOException e1) {
			System.out.println("Error lauching dot");
			Errors.setLastError(Errors.ErrorNum.DOTNOTFOUND);
			return false;
		}

		try {
			dotProcess.waitFor();
		}
		catch (InterruptedException e) {
			System.out.println("dot interrupted\n");
			Errors.setLastErrorMessage("Dot process interrupted");
			return false;
		}
		if (dotProcess.exitValue()!=0) {
			Errors.setLastErrorMessage("Error with Dot execution. exit code is "+dotProcess.exitValue());
			return false;
		}
		return true;
	}
	
	
	
	static private int nbSimErrors;
	
	static public int getNbSimErrors() {
		return nbSimErrors;
	}
	static public boolean runQuesta(M2MProject project/*,MessageConsoleStream warStream*/) {

		nbSimErrors=0;
		
		SimulationResult simResult=new SimulationResult();
		
		final String projectPath = project.getProperties().getProjectPath();
		final String reportPath = project.getReportPath()+"/";
		

		final SystemVerilogCreator SVCreator;
		BufferedReader brQuesta;
		BufferedReader brErrQuesta;

		SVCreator = new SystemVerilogCreator(project.getStructTreatment().getTop());
		
		String line = "";
		
		Process process = null;
		FileWriter fwNote = null;
		FileWriter fwError = null;
		FileWriter fwFailure = null;
		FileWriter fwWarning = null;
		

		//create the report folder, and the files in it
		File reportFolder = new File(reportPath);
		if ((reportFolder.exists() && reportFolder.isDirectory())) {
			FileUtils.deleteDirContent(reportFolder,null);
		}
		
		project.checkFolders();
		
		//create the "note" file
		File noteFile = new File(reportPath+"Note_Report.txt");
		//create the "error" file
		File errorFile = new File(reportPath+"Error_Report.txt");
		//create the "failure" file
		File failureFile = new File(reportPath+"Failure_Report.txt");
		//create the "warning" file
		File warningFile = new File(reportPath+"Warning_Report.txt");
		try {
			fwNote = new FileWriter(noteFile);
			fwError = new FileWriter(errorFile);
			fwFailure = new FileWriter(failureFile);
			fwWarning = new FileWriter(warningFile);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		
		SVCreator.createSystemVerilog(project);
		
		File simulation = new File(project.getSimPath()+"/simulation.do");
		if (!simulation.exists()) {
			System.err.println("File not Found\nFile \"simulation.do\" not found in "+projectPath);
			return false;
		}

		try {
			//get questa path
			ExternalToolsProperties externalToolsProperties = ExternalToolsProperties.getReference();
			String vsimPath = externalToolsProperties.getVsimPath();
			ProcessBuilder pb=new ProcessBuilder(vsimPath,"-c","-l",project.getSimPath()+"/transcript.txt");
			pb.directory(new File(project.getSimPath()));
			process = pb.start();
		} catch (IOException ioe) {
			System.err.println("Error launching vsim\nvsim path is not well configured, configure it correctly before continue");
			Errors.setLastError(ErrorNum.QUESTANOTFOUND);
			return false;
		}
		

		ProcessWatcher.getInstance().addProcess(process);

		//create streams to communicate with Questa application
		BufferedWriter bwQuesta  = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
		brQuesta = new BufferedReader(new InputStreamReader(process.getInputStream()));
		brErrQuesta = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		try {
			bwQuesta.write("do "+"\""+project.getSimPath()+"/simulation.do"+"\"");
			bwQuesta.flush();
			
			bwQuesta.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (brQuesta == null || brErrQuesta == null)
			return false;
		
		String prefix;
		if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_UVM)
			prefix="UVM_";
		else if (ExternalToolsProperties.getReference().getSimulationFiles()==ExternalToolsProperties.SIMULATIONFILES_M2MOVM)
			prefix="M2M_";
		else
			prefix="OVM_";
//		prefix="";
		simResult.setPrefix(prefix);

		project.monitoring.subTask(project,"Compiling...");
		while (true) {
			try {
				if ((line = brQuesta.readLine()) != null) {
					//System.out.println(line);
					if (line.contains(prefix+"WARNING") || line.contains("Warning")) {
						System.out.println(line);
						if (line.contains(prefix+"WARNING"))
							fwWarning.write(line + "\r\n");
					}
					else if (line.contains(prefix+"FATAL")) {
						nbSimErrors++;
						System.err.println(line);
						fwFailure.write(line + "\r\n");
					}
					else if (line.contains(prefix+"ERROR")) {
						nbSimErrors++;
						System.err.println(line);
						fwError.write(line + "\r\n");
					}
					else if (line.contains(prefix+"INFO")) {
						fwNote.write(line + "\r\n");
					}
					else if (line.contains("M2MVar(")) {
						String variable;
						String value;
						int pos1=line.indexOf("(");
						int pos2=line.indexOf(")");
						variable=line.substring(pos1+1,pos2);
						value=line.substring(pos2+2);
						int valueInt;
						try {
							valueInt=Integer.parseInt(value);
						}
						catch (NumberFormatException e) {
							valueInt=0;
						}
						if (variable.equals("Latency")) {
							simResult.setLatency(valueInt);
						}
						else if (variable.equals("TotalTime")) {
							simResult.setTotalTime(valueInt);
						}
						else if (variable.equals("NbInputs")) {
							simResult.setNbInputs(valueInt);
						}
						else if (variable.equals("MaxBitError")) {
							simResult.setMaxBitError(valueInt);
						}
						else if (variable.equals("Progress")) {
							project.monitoring.workedSimulationStep();
						}
						else if (variable.equals("StartSim")) {
							project.monitoring.workedCompilation();
							project.monitoring.subTask(project,"Simulating...");
						}
					}
					else {
						System.out.println(line);
					}
				}
				else
					break;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		while (true) {
			try {
				if ((line = brErrQuesta.readLine()) != null)
				{}//	System.err.println(line);
				else {
					System.out.println("Questa verification finished");
					break;
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		try {
			fwNote.close();
			fwError.close();
			fwFailure.close();
			fwWarning.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			process.waitFor();
		}
		catch (InterruptedException e) {
		}
		
		System.out.println("Questa return value: "+process.exitValue()+"\n");
		if (process.exitValue()==4) {
			Errors.setLastError(ErrorNum.QUESTALICENSE);
			return false;
		}
		
		ProcessWatcher.getInstance().removeProcess(process);
		simResult.setNbErrors(nbSimErrors-2);
		project.setSimulationResult(simResult);
		System.out.println("-- Simulation results ---------------------------------------");
		if (simResult.getNbErrors()==0)
			System.out.println("Number of errors : "+simResult.getNbErrors());
		else
			System.err.println("Number of errors : "+simResult.getNbErrors());
		System.out.println("Number of wrong bits (from Lowest) : "+simResult.getMaxBitError());
		if (simResult.getMaxBitError()!=0) {
			if (project.getProperties().getOptimisationProperties().getOptimisationDataType()==NumType.FLOAT32) {
				System.out.print("    ");
			
				for(int i=0;i<32+4;i++)
					System.out.print("-");
				System.out.print("\n");
				System.out.print("    ");
				System.out.print("|");
				for(int i=0;i<32;i++) {
					if (i<32-simResult.getMaxBitError())
						System.out.print("v");
					else
						System.err.print("x");
					if (i==0)
						System.out.print("|");
					if (i==8)
						System.out.print("|");
				}
				System.out.print("|\n");
				System.out.print("    ");
				for(int i=0;i<32+4;i++)
					System.out.print("-");
				System.out.print("\n\n");
			}
			else {
				System.out.print("    ");
			
				for(int i=0;i<64+4;i++)
					System.out.print("-");
				System.out.print("\n");
				System.out.print("    ");
				System.out.print("|");
				for(int i=0;i<64;i++) {
					if (i<64-simResult.getMaxBitError())
						System.out.print("v");
					else
						System.err.print("x");
					if (i==0)
						System.out.print("|");
					if (i==11)
						System.out.print("|");
				}
				System.out.print("|\n");
				System.out.print("    ");
				for(int i=0;i<64+4;i++)
					System.out.print("-");
				System.out.print("\n\n");
			}
		}
		System.out.println("Latency          : "+simResult.getLatency()+ " clock cycles");
		System.out.println("Total time for "+simResult.getNbInputs()+" inputs : "+simResult.getTotalTime() + " clock cycles");
		System.out.println("-- End of simulation ----------------------------------------");
		return true;
	}
	
	static public boolean runXilinxUbidule(M2MProject project) {
		ISETCLGenerator iseTclGen = new  ISETCLGenerator();
		if (! iseTclGen.generateTCLISE(project))
			return false;
		if (!M2MClient.generateXSVF(project))
			return false;
		return true;
	}
	
	static public boolean runAllUbidule(M2MProject project) {
		if (!runXilinxUbidule(project))
			return false;

		return runUbidule(project);
	}
	
	static public boolean runUbidule(M2MProject project) {

		// Get the number of input of the function
		int nbrInput = project.getStructTreatment().getInput().size();
			
		// Get the number of output of the function
		int nbrOutput = project.getStructTreatment().getOutput().size();
			
		// Get the number of sample of the M2M project
		int nbrSamples = project.getSimulationProperties().getNumberOfSamples();
			
		M2MClient client=new M2MClient();
		return client.testOnUbidule(nbrInput, nbrOutput, nbrSamples, project);
	}

	
}
