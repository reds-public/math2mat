/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ProcessWatcher.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Dec 3, 2010
 *
 * Author: Sebastien Masle
 *
 * Description:
 * 
 */
package m2m.backend.processing;

import java.util.ArrayList;

/**
 * @author ythoma
 *
 */
public class ProcessWatcher {


	private static ProcessWatcher instance=null;
	
	public static ProcessWatcher getInstance() {
		if (instance==null)
			instance = new ProcessWatcher();
		return instance;
	}
	
	public ArrayList<Process> processes;
	
	private ProcessWatcher() {
		processes = new ArrayList<Process>();
	}
	
	public void deleteProcesses() {
		for(int i=0;i<processes.size();i++) {
			processes.get(i).destroy();
		}
	}
	
	public void addProcess(Process process) {
		processes.add(process);
	}
	
	public void removeProcess(Process process) {
		processes.remove(process);
	}
	
}
