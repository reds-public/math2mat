/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Complete SystemVerilog harness file.
 *
 * Project:  Math2Mat
 *
 * @file OctaveScriptCreator.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 12.10.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This class completes the SystemVerilog harness file with the connections with the DUT.
 * 
 */

package m2m.backend.processing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


import m2m.backend.buildingblocks.BuildingBlock.NumType;
import m2m.backend.project.ExternalToolsProperties;
import m2m.backend.project.M2MProject;
import m2m.backend.structure.Element;
import m2m.backend.structure.Function;
import m2m.backend.structure.SimpleVariable;
import m2m.backend.utils.FileUtils;

public class SystemVerilogCreator {
	
	private Function top;
	
	public SystemVerilogCreator (Function top) {
		this.top = top;
	}
	
	public boolean createSystemVerilog(M2MProject project) {
		
		FileUtils.copyFile(new File(M2MProject.getLibPath()+"/SV/").getAbsolutePath(), project.getSVPath(),".svn");
		
		String harnessName = project.getSVPath()+"/M2M_harness.sv";
		String commonName = project.getSVPath()+"/common.sv";
		File harnessFile = new File(harnessName);
		File commonFile = new File(commonName);
		int instanceIndex = 0;
		//instance the DUT in the harness file
		if ((instanceIndex = findString("<instance DUT>", harnessFile)) == -1)
			return false;
		else {
			String instance = instanceDUT();
			writeFile(instanceIndex, instance, harnessFile);
		}
		//create the common file with all the necessary constants
		String constants = createConstants(project);
		FileWriter fw;
		try {
			fw = new FileWriter(commonFile);
			fw.write(constants);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private String createConstants(M2MProject project) {
		
		
//		double period = 0.0000001f;
//		NumberFormat formatter = new DecimalFormat("#0.00000000000000000000000000000000000000000000000000000000000#");
//		if (this.simProps.getSimulationStimFrequency() != 0)
//			period = 1/this.simProps.getSimulationStimFrequency();
		
		String sConstants = new String();
		sConstants += "`define NUMBER_BIT_PRECISION " + project.getProperties().getSimulationProperties().getSimulationCalcPrecision() + "\n";
		sConstants += "`define INACTIVITY_TIMEOUT " + project.getProperties().getSimulationProperties().getInactivityTimeout() + "\n";
		sConstants += "`define SYSTEM_FREQ " + project.getProperties().getSimulationProperties().getSystemFrequency() + "\n";
		sConstants += "`define INPUT_FREQ " + project.getProperties().getSimulationProperties().getInputFrequency() + "\n";
		sConstants += "`define OUTPUT_FREQ " + project.getProperties().getSimulationProperties().getOutputFrequency() + "\n";
		sConstants += "`define M2M_BUS_SIZE " + NumType.getDataSize(project.getOptimisationProperties().getOptimisationDataType()) + "\n\n";
		sConstants += "`define CLOCK_TICKS 10\n\n";
		sConstants += "`define NUMBER_OF_INPUTS " + this.top.getInput().size() + "\n";
		sConstants += "`define NUMBER_OF_DUT_INPUTS " + this.top.getInput().size() + "\n";
		sConstants += "`define READ_INPUT_WAY 1\n";
		sConstants += "bit INPUT_PARALLEL [1:`NUMBER_OF_INPUTS] = '{";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			if ( i == 0)
				sConstants += "0";
			else
				sConstants += ", 0";
		}
		sConstants += "};\n\n";
		sConstants += "`define NUMBER_OF_OUTPUTS " + this.top.getOutput().size() + "\n";
		sConstants += "`define NUMBER_OF_DUT_OUTPUTS " + this.top.getOutput().size() + "\n";
		sConstants += "`define READ_OUTPUT_WAY 1\n";
		sConstants += "bit OUTPUT_PARALLEL [1:`NUMBER_OF_OUTPUTS] = '{";
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			if ( i == 0)
				sConstants += "0";
			else
				sConstants += ", 0";
		}
		sConstants += "};\n\n";
		sConstants += "`define NUMBER_OF_INTERNALS " + this.top.getMonitoringElement().size() + "\n";
		sConstants += "`define NUMBER_OF_DUT_INTERNALS " + this.top.getMonitoringElement().size() + "\n";
		sConstants += "`define READ_INTERNAL_WAY 1\n";
		sConstants += "bit INTERNAL_PARALLEL [1:`NUMBER_OF_INTERNALS]";
		if(this.top.getMonitoringElement().size() != 0)
		{
			sConstants += " = '{";
			for (int i = 0; i < this.top.getMonitoringElement().size(); i++) {
				if ( i == 0)
					sConstants += "0";
				else
					sConstants += ", 0";
			}
			sConstants += "}";
		}
		sConstants += ";\n";
		
		return sConstants;
	}

	private String instanceDUT() {
		String sInstance = new String();
		sInstance += this.top.getName() + " DUT(\n";
		sInstance += tab(1) + ".clock_i" + tab(1) + "(CLK),\n";
		sInstance += tab(1) + ".reset_i" + tab(1) + "(reset),\n";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			Element el = this.top.getInput().get(i);
			if (el.getName().isEmpty()) {
				System.err.println("No name found for the DUT input n�" + (i + 1));
			}
			sInstance += tab(1) + "." + el.getName() + "_i" + tab(1) + "(M2M_in_intf.Input_Value[" + (i + 1) + "]),\n";
			sInstance += tab(1) + "." + el.getName() + "_Valid_i" + tab(1) + "(M2M_in_intf.Input_Valid[" + (i + 1) + "]),\n";
			sInstance += tab(1) + "." + el.getName() + "_Ready_o" + tab(1) + "(M2M_in_intf.Input_Ready[" + (i + 1) + "]),\n";
		}
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			sInstance += tab(1) + "." + this.top.getOutput().get(i).getName() + "_o" + tab(1) + "(M2M_out_intf.Result[" + (i + 1) + "]),\n";
			sInstance += tab(1) + "." + this.top.getOutput().get(i).getName() + "_Valid_o" + tab(1) + "(M2M_out_intf.Result_Valid[" + (i + 1) + "]),\n";
			if (i == this.top.getOutput().size() - 1)
				sInstance += tab(1) + "." + this.top.getOutput().get(i).getName() + "_Ready_i" + tab(1) + "(M2M_in_intf.Result_Ready["+(i+1)+"])\n";
			else
				sInstance += tab(1) + "." + this.top.getOutput().get(i).getName() + "_Ready_i" + tab(1) + "(M2M_in_intf.Result_Ready["+(i+1)+"]),\n";
		}
		sInstance += ");\n";
			
		/* Instance internal Dut monitoring signal */

		if (ExternalToolsProperties.getReference().getSimulator()>=2) {
			for (int i = 0; i < this.top.getMonitoringElement().size(); i++) 
			{
				Element el = this.top.getMonitoringElement().get(i);

				
				
				if(((SimpleVariable)el).getType().equalsIgnoreCase("cond")) {
					sInstance += tab(1) + "initial begin\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_s\"\t\t\t\t, \"" + "Internal_Data["       + (i+1) + "][0]\" ,1,1);\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_Ready_s\"\t\t\t, \"" + "Internal_Data_Ready["       + (i+1) + "]\" ,1,1);\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_Valid_s\"\t\t\t, \"" + "Internal_Data_Valid["       + (i+1) + "]\",1,1);\n";
					sInstance += tab(1) + "end\n\n";
					sInstance += tab(1) + "assign M2M_int_intf.Data[" + (i+1) + "]         = Internal_Data[" + (i+1) + "][0];\n";		

				}
				else {
					sInstance += tab(1) + "initial begin\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_s\"\t\t\t\t, \"" + "Internal_Data["       + (i+1) + "]\" ,1,1);\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_Ready_s\"\t\t\t, \"" + "Internal_Data_Ready["       + (i+1) + "]\" ,1,1);\n";
					sInstance += tab(2) + "$init_signal_spy(\"DUT/" + el.getName() + "_Valid_s\"\t\t\t, \"" + "Internal_Data_Valid["       + (i+1) + "]\",1,1);\n";
					sInstance += tab(1) + "end\n\n";
					sInstance += tab(1) + "assign M2M_int_intf.Data[" + (i+1) + "]         = Internal_Data[" + (i+1) + "];\n";
				}
			}

			
			sInstance += tab(1) + "assign M2M_int_intf.Data_Ready   = Internal_Data_Ready;\n";
			sInstance += tab(1) + "assign M2M_int_intf.Data_Valid   = Internal_Data_Valid;\n";
			
		}
		else {
			for (int i = 0; i < this.top.getMonitoringElement().size(); i++) 
			{
				Element el = this.top.getMonitoringElement().get(i);

				sInstance += tab(1) + "assign " + "M2M_int_intf.mon_mp.Data["       + (i+1) + "]\t\t\t = " + "DUT." + el.getName() + "_s;\n";
				sInstance += tab(1) + "assign " + "M2M_int_intf.mon_mp.Data_Ready[" + (i+1) + "]\t = "     + "DUT." + el.getName() + "_Ready_s" + ";\n";
				sInstance += tab(1) + "assign " + "M2M_int_intf.mon_mp.Data_Valid[" + (i+1) + "]\t = "     + "DUT." + el.getName() + "_Valid_s" + ";\n";
			}
		}
		
		return sInstance;
	}

	private void writeFile(int instanceIndex, String instance, File file) {
		try {
			FileReader fr = new FileReader(file);
			String beginOfFile = new String();
			String endOfFile = new String();
			int readChar;
			for (int i = 0; i <= instanceIndex; i++) {
				beginOfFile += (char)fr.read();
			}
			while ((readChar = fr.read()) != -1) {
				endOfFile += (char)readChar;
			}
			FileWriter fw = new FileWriter(file);
			fw.write(beginOfFile);
			fw.write(instance);
			fw.write(endOfFile);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private int findString (String toFind, File file) {
		int readChar;
		String readText = new String();
		FileReader fr;
		try {
			fr = new FileReader(file);
			while ((readChar = fr.read()) != -1) {
				readText += (char)readChar;
			}
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return -1;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}

		return readText.indexOf(toFind) + toFind.length();
	}
	
	private String tab(int nb) {
		String s = new String();
		for (int i = 0; i < nb; i++) {
			s += "\t";
		}
		return s;
	}
	
}
