/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Optimises the tree of operators.
 *
 * Project:  Math2Mat
 *
 * @file OptimiseOperators.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 01.03.2010
 *
 * Author: Sebastien Masle
 *
 * Description: This class re-organise the tree to optimise the position of operators.
 * 
 */

package m2m.backend.processing;

import java.util.Vector;

import m2m.backend.structure.Addition;
import m2m.backend.structure.Assignment;
import m2m.backend.structure.Element;
import m2m.backend.structure.Operation;
import m2m.backend.structure.StructTreatment;
import m2m.backend.structure.Subtraction;


public class OptimizeOperators {

	private StructTreatment struct;
	
	public OptimizeOperators (StructTreatment baseStruct) {
		struct = baseStruct;
	}
	
	public void optimise() {
		//optimiseOperators(struct.getTop().getBody());
		optimiseAddSub(struct.getTop().getBody());
	}
	
	/*
	private void optimiseOperators (Vector<Element> el) {
		if (optimiseAddSub(el) != 0) {
			optimiseOperators(el);
		}
		else {
			for (int i = 0; i < el.size(); i++) {
				if (el.elementAt(i) instanceof Operation)
					optimiseOperators(((Operation)el.elementAt(i)).getInput());
			}
		}
	}
	*/
	
	private int optimiseAddSub (Vector<Element> inputs) {
		//Function tree = struct.getTop();
		
		//Vector<Element> inputs = tree.getBody();
		
		for (int i = 0; i < inputs.size(); i++) {
			//try to find a first addition or subtraction
			if ((inputs.elementAt(i) instanceof Addition) || (inputs.elementAt(i) instanceof Subtraction)) {
				Vector<Element> inputs2 = ((Operation)inputs.elementAt(i)).getInput();
				for (int j = 0; j < inputs2.size(); j++) {
					//try to find a second addition or subtraction
					if ((inputs2.elementAt(j) instanceof Addition) || (inputs2.elementAt(j) instanceof Subtraction)) {
						Vector<Element> inputs3 = ((Operation)inputs2.elementAt(j)).getInput();
						for (int k = 0; k < inputs3.size(); k++) {
							//try to find a third addition or subtraction
							if ((inputs3.elementAt(k) instanceof Addition) || (inputs3.elementAt(k) instanceof Subtraction)) {
								//Element firstOpInput;
								Element input;
								Element output;
								//Element secondOpOutput;
								
								output = ((Operation)inputs.elementAt(i)).getOutput().firstElement();
								//secondOpOutput = ((Operation)inputs2.elementAt(j)).getOutput().firstElement();

								if (k > 0) {
									input = inputs3.elementAt(0);
									((Operation)inputs2.elementAt(j)).setInputAt(0, inputs.elementAt(i));
								} else {
									input = inputs3.elementAt(1);
									((Operation)inputs2.elementAt(j)).setInputAt(1, inputs.elementAt(i));
									//((Operation)inputs2.elementAt(j)).setOutputAt(0, output);
								}
								try {
									((Operation)inputs2.elementAt(j)).addOutput(output);
								} catch (Exception e) {
									e.printStackTrace();
									System.err.println(e.getMessage());
								}
								
								inputs.setElementAt(inputs2.elementAt(j), i);

								if (k > 0) {
									((Operation)((Operation)inputs2.elementAt(j)).getInputAt(0)).getOutput().removeElementAt(0);
									((Operation)((Operation)inputs2.elementAt(j)).getInputAt(0)).setInputAt(j, input);
								} else {
									((Operation)((Operation)inputs2.elementAt(j)).getInputAt(0)).getOutput().removeElementAt(0);
									//((Operation)((Operation)inputs2.elementAt(j)).getInputAt(1)).setOutputAt(0, null);
									((Operation)((Operation)inputs2.elementAt(j)).getInputAt(1)).setInputAt(j, input);
								}
								
								return 1;
							}
						}
					}
				}
			}
		}
		return 0;
	}
	
	public int opTreeDepth () {
		int depth = 0;
		
		Vector<Element> elements = struct.getTop().getBody();
		for (int i = 0; i < elements.size(); i ++) {
			if (elements.elementAt(i) instanceof Operation && !(elements.elementAt(i) instanceof Assignment)) {
				depth = treeDepth(elements.elementAt(i));
			}
		}
		System.out.println("profondeur de l'arbre : "+depth);
		return depth;
	}
	
	public int treeDepth (Element el) {
		int depth = 0;
		int subDepth = 0;
		
		if (el instanceof Operation) {
			Operation op = (Operation) el;
			depth = 1;
/*			if (op.getNbInput() == 1) {
				if (op.getInputAt(0) instanceof Operation) {
					return depth + treeDepth(op.getInputAt(0));
				} else {
					return depth + 1;
				}
			} else if (op.getNbInput() == 2) {
				if ((op.getInputAt(0) instanceof Operation) && (op.getInputAt(1) instanceof Operation)) {
					return depth + java.lang.Math.max(treeDepth(op.getInputAt(0)), treeDepth(op.getInputAt(1)));
				} else {
					if (op.getInputAt(0) instanceof Operation) {
						return depth + treeDepth(op.getInputAt(0));
					} else if (op.getInputAt(1) instanceof Operation) {
						return depth + treeDepth(op.getInputAt(1));
					} else {
						return depth;
					}
				}
			} else if (op.getNbInput() > 2) {
				int subTreeDepth = 0;
*/				for (int i = 0; i < op.getNbInput(); i++) {
//					if (op.getInputAt(i) instanceof Operation) {
					subDepth = java.lang.Math.max(subDepth, treeDepth(op.getInputAt(i)));
//					}
				}
				depth += subDepth;
/*				return depth;
			} else {
				return depth;
			}
		} else {
			return depth;
*/		}
		//System.out.println("profondeur interm�diaire de l'arbre : "+depth);
		
		return depth;
	}
	
}
