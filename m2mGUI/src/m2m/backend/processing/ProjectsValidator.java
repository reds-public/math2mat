/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ProjectsValidator.java
 * @author Yann Thoma
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: Jan 17, 2011
 *
 * Author: Yann Thoma
 *
 * Description: This class allows to recursively test all the projects
 *              found in a specified directory.
 *              Very useful for validation purposes.
 * 
 */
package m2m.backend.processing;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import m2m.backend.octaveparser.ParsingException;
import m2m.backend.project.M2MProject;

/**
 * @author ythoma
 *
 */
public class ProjectsValidator {
	
	public int nbProjects;
	
	protected String curDir;
	protected LinkedList<M2MProject> plist;

	
	private int internalNbWorkingSteps(String dir) {
		File dirFile = new File(dir);
		File[] files = dirFile.listFiles();
				
		int result=0;
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				result+=internalNbWorkingSteps(files[i].getAbsolutePath());
			}
			else {
				File file=files[i];
				// get the file extension
			    String ext = (file.getName().lastIndexOf(".") == -1 ? "" :
			    file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length()));
					
			    // if it is a m2m project file (*.m2m), create the corresponding class to extract information
				if (ext.equalsIgnoreCase("m2m")) {
					if (file.exists()) {
						nbProjects=nbProjects+1;
						M2MProject project = new M2MProject();
						project.openProject(file.getAbsolutePath());
						plist.add(project);
						result+=ProgressMonitoring.workingTimeAll(project);						
					}
				}		
			}
		}
		return result;
	}
	
	public int nbWorkingSteps(String dir) {
		nbProjects=0;
		plist = new LinkedList<M2MProject>();
		curDir=dir;
		return internalNbWorkingSteps(dir);
	}
	
	private void testDirInternal(String dir,Vector<String> errors, ProgressMonitoring monitor) {

		File dirFile = new File(dir);
		File[] files = dirFile.listFiles();
				
		for (int i = 0; i < files.length; i++) {
			if (monitor.isCanceled()) {
				return;
			}
			if (files[i].isDirectory()) {
				testDirInternal(files[i].getAbsolutePath(),errors,monitor);
			}
			else {
				File file=files[i];
				// get the file extension
			    String ext = (file.getName().lastIndexOf(".") == -1 ? "" :
			    file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length()));
					
			    // if it is a m2m project file (*.m2m), create the corresponding class to extract information
				if (ext.equalsIgnoreCase("m2m")) {
					if (file.exists()) {
						System.out.println("********************************************************************************");
						System.out.println("Testing project "+file.getAbsolutePath());
						System.out.println("--------------------------------------------------------------------------------");
						M2MProject project = new M2MProject();
						project.openProject(file.getAbsolutePath());
						try{
							project.getStructTreatment().parse(project.getSourceFile());
							project.getStructTreatment().modifyNumType(project.getProperties().getOptimisationProperties().getOptimisationDataType());
							project.clear();
							project.monitoring=monitor;
//							project.monitoring=new ProgressMonitoring();
							if (ExternalRuns.runAll(project)) {
								if (project.getSimulationResult().getNbErrors()>0)
									errors.add("Number of simulation errors for "+project.getProperties().getProjectFullFilename()+" : "+project.getSimulationResult().getNbErrors());

								System.out.println("--------------------------------------------------------------------------------");
								System.out.println("End of test for project "+file.getAbsolutePath());
								System.out.println("********************************************************************************");
							}
							else {
								errors.add("Can not launch simulation for "+project.getProperties().getProjectFullFilename());
							}
						}	
						catch (ParsingException e1) {
							errors.add("Can not parse file of "+project.getProperties().getProjectFullFilename());
						}
						project.clear();
//						monitor.workedOneProject();
					}
				}		
			}
		}
	}
	
	private void testProjects(Vector<String> errors,ProgressMonitoring monitor) {

		Iterator<M2MProject> iter=plist.listIterator();
		
		while (iter.hasNext()) {
			M2MProject project=(M2MProject)iter.next();
			if (monitor.isCanceled()) {
				return;
			}
			System.out.println("********************************************************************************");
			System.out.println("Testing project "+project.getProjectFilename());
			System.out.println("--------------------------------------------------------------------------------");
		//	try{
//				project.getStructTreatment().parse(project.getSourceFile());
				project.clear();
				project.monitoring=monitor;
//				project.monitoring=new ProgressMonitoring();
				if (ExternalRuns.runAll(project)) {
					if (project.getSimulationResult().getNbErrors()>0)
						errors.add("Number of simulation errors for "+project.getProperties().getProjectFullFilename()+" : "+project.getSimulationResult().getNbErrors());
						System.out.println("--------------------------------------------------------------------------------");
					System.out.println("End of test for project "+project.getProjectFilename());
					System.out.println("********************************************************************************");
				}
				else {
					errors.add("Can not launch simulation for "+project.getProperties().getProjectFullFilename());
				}
		//	}	
		//	catch (ParsingException e1) {
		//		errors.add("Can not parse file of "+project.getProperties().getProjectFullFilename());
		//	}
			project.clear();
//						monitor.workedOneProject();
		}
	}

	public boolean testDir(String dir,ProgressMonitoring monitor) {
		Vector<String> errors=new Vector<String>();
		if (curDir!=dir)
			nbWorkingSteps(dir);
			
		testProjects(errors,monitor);
		//testDirInternal(dir,errors,monitor);	
		if (monitor.isCanceled()) {
			System.out.println("********************************************************************************");
			System.out.println("Test canceled");
			System.out.println("********************************************************************************");
			return false;
		}
		System.out.println("********************************************************************************");
		System.out.println("Simulation results");
		System.out.println("--------------------------------------------------------------------------------");
		if (errors.size()==0)
			System.out.println("There are no errors in the different projects");
		for (int i=0;i<errors.size();i++) {
			System.out.println(errors.elementAt(i)+"\n");
		}
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("End of Simulation results");
		System.out.println("********************************************************************************");
		return true;
	}	

}
