/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**

 * @brief Creates Octave files.
 *
 * Project:  Math2Mat
 *
 * @file OctaveScriptCreator.java
 * @author Sebastien Masle
 * @par Team: <a href="http://www.reds.ch">ReDS Institute</a>
 *
 * @version 1.0
 *
 * @date: 12.10.2009
 *
 * Author: Sebastien Masle
 *
 * Description: This class creates the Octave file with all the project parameters needed for Octave computation,
 * and the file that will call the Math2Mat function.
 * 
 */

package m2m.backend.processing;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import m2m.backend.project.M2MProject;
import m2m.backend.project.SimulationProperties;
import m2m.backend.structure.Element;
import m2m.backend.structure.Function;


public class OctaveScriptCreator {
	
	private Function top;
	private SimulationProperties simProp;
	private boolean codeRegen;
	
	public OctaveScriptCreator(Function top, SimulationProperties simProp, boolean regen) {
		this.top = top;
		this.simProp = simProp;
		this.codeRegen = regen;
	}
	
	public boolean createOctaveScript(M2MProject project, File sourceFile) {
		
		String parameterPath = project.getOctavePath()+"/m2m_parameters.m";
		String samplesPath  = project.getOctavePath()+"/m2m_computesample.m";
		final String projectPath = project.getProperties().getProjectPath();
		
		writeParameters(parameterPath, projectPath+project.subfolder(), sourceFile);
		writeSamples(samplesPath, sourceFile);
		if (codeRegen)
			completeMainScript(project);
		
		return true;
	}

	/**
	 * 
	 */
	private void completeMainScript(M2MProject project) {
		
		File mainScript = new File(project.getOctavePath()+"/m2m_doall.m");
		String sOutputRegen = new String();
		
		sOutputRegen += "\n\nprintf(\"\\nCreation of output_regen verification files:\\n\");\n";
		sOutputRegen += "printf(\"--------------------------------------------\\n\");\n";
		sOutputRegen += "\nfor i=1:length(outputs_regen)\n";
		sOutputRegen += "\tfname=sprintf(\"%sfile_%s.dat\",filedir,outputs_regen(i).name);\n";
		sOutputRegen += "\tm2m_writevartofile(fname,outputs_regen(i).values);\n";
		sOutputRegen += "\tprintf(\"file_%s.dat was written\\n\", outputs_regen(i).name);\n";
		sOutputRegen += "endfor\n";
		
		if(top.getMonitoringElement().size() != 0)
		{
			sOutputRegen += "\nprintf(\"\\nCreation of intern verification files:\\n\");\n";
			sOutputRegen += "printf(\"--------------------------------------\\n\");\n";
			sOutputRegen += "\nfor i=1:length(interns)\n";
			sOutputRegen += "\tfname=sprintf(\"%sfile_%s.dat\",filedir,interns(i).name);\n";
			sOutputRegen += "\tm2m_writevartofile(fname,interns(i).values);\n";
			sOutputRegen += "\tprintf(\"file_%s.dat was written\\n\", interns(i).name);\n";
			sOutputRegen += "endfor\n\n";
		}
			
		try {
			FileReader fr = new FileReader(mainScript);
			int readChar;
			String readText = new String();
			//read all the content of the main Octave script (m2m_doall.m)
			while ((readChar = fr.read()) != -1) {
				readText += (char)readChar;
			}
			fr.close();
			
			//add the few lines to generates regenerated output files
			//sOutputRegen += readText + sOutputRegen;
			
			//write all in the main script file
			FileWriter fw = new FileWriter(mainScript);
			fw.write(readText + sOutputRegen);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private void writeParameters(String samplesPath, String path,File sourceFile) {
		File file = new File(samplesPath);
		try {
			String samples = createParameters(path,sourceFile);
			FileWriter fw = new FileWriter(file);
			fw.write(samples);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Cannot open file : " + samplesPath);
		}
	}

	private String createParameters(String path, File sourceFile) {
		String sSamples = new String();
		sSamples += "%inputs\n";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			String name = this.top.getInput().elementAt(i).getName();
			sSamples += "inputs(" + (i + 1) + ").name=\"input" + (i + 1) +"\";\n";
			sSamples += "inputs(" + (i + 1) + ").size=1;\n";	
			sSamples += "inputs(" + (i + 1) + ").min=" + simProp.getInputSimParamMin(name) + ";\n";
			sSamples += "inputs(" + (i + 1) + ").max=" + simProp.getInputSimParamMax(name) + ";\n";
		}
		sSamples += "%outputs\n";
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			sSamples += "outputs(" + (i + 1) + ").name=\"output" + (i + 1) + "\";\n";
			sSamples += "outputs(" + (i + 1) + ").size=1;\n";
		}
		if (this.codeRegen) {
			for (int i = 0; i < this.top.getOutput().size(); i++) {
				sSamples += "outputs_regen(" + (i + 1) + ").name=\"output_regen" + (i + 1) + "\";\n";
				sSamples += "outputs_regen(" + (i + 1) + ").size=1;\n";
			}
		}
		Vector<Element> internals = this.top.getMonitoringElement();
		sSamples += "%interns\n";
		for (int i = 0; i < internals.size(); i++) {
			sSamples += "interns(" + (i + 1) + ").name=\"internal" + (i + 1) + "\";\n";
			sSamples += "interns(" + (i + 1) + ").sname=\"" + internals.get(i).getName() + "\";\n";
			sSamples += "interns(" + (i + 1) + ").size=1;\n";
		}
		sSamples += "% number of samples\n";
		sSamples += "nbsamples=" + simProp.getNumberOfSamples() + ";\n\n";
		sSamples += "filedir=\"" + path + M2MProject.getIOFilesDirName()+"/\";\n";
		sSamples += "generate_input=true;\n";
		sSamples += "addpath(\""+sourceFile.getParent().replace("\\", "/")+"\");";
		
		return sSamples;
	}

	private void writeSamples(String parameterPath, File sourceFile) {
		File file = new File(parameterPath);

		String sParameters = new String();
		sParameters += "[";
		for (int i = 0; i < this.top.getOutput().size(); i++) {
			if ( i == 0)
				sParameters += "outputs(" + (i + 1) + ").values(i,:)";
			else
				sParameters += ", outputs(" + (i + 1) + ").values(i,:)";
		}
		sParameters += "]=" + sourceFile.getName().substring(0, sourceFile.getName().lastIndexOf(".")) + "(";
		for (int i = 0; i < this.top.getInput().size(); i++) {
			if (i == 0)
				sParameters += "inputs(" + (i + 1) + ").values(i,:)";
			else
				sParameters += ", inputs(" + (i + 1) + ").values(i,:)";
		}
		sParameters += ");\n";
		
		if (codeRegen) {
			sParameters += "[";
			for (int i = 0; i < this.top.getOutput().size(); i++) {
				if ( i == 0)
					sParameters += "outputs_regen(" + (i + 1) + ").values(i,:)";
				else
					sParameters += ", outputs_regen(" + (i + 1) + ").values(i,:)";
			}
			sParameters += "]=" + sourceFile.getName().substring(0, sourceFile.getName().lastIndexOf(".")) + "_regen(";
			for (int i = 0; i < this.top.getInput().size(); i++) {
				if (i == 0)
					sParameters += "inputs(" + (i + 1) + ").values(i,:)";
				else
					sParameters += ", inputs(" + (i + 1) + ").values(i,:)";
			}
			
			if(top.getMonitoringElement().size() != 0)
				sParameters += ", i";
			
			sParameters += ");\n";
		}
		
		try {
			FileWriter fw = new FileWriter(file);
			fw.write(sParameters);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Cannot open file : " + parameterPath);
		}
	}
}
