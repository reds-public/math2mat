/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file ISETCLGenerator.java
 * @author Mikhail Nagoga
 *
 * @version 1.0
 *
 * @date: Nov 21, 2010
 *
 * Author: Mikhail Nagoga
 *
 * Description:
 * 
 * 
 * 
 */
package m2m.backend.verifpga;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.LinkedList;

import m2m.backend.project.ExternalToolsProperties;
import m2m.backend.project.M2MProject;
import m2m.backend.utils.FileUtils;


public class ISETCLGenerator {

	public final static String PROJECT_PATH_TCL = "<PROJECTPATH>";
	
	private String projectPath;
	private String templatesPath;
	private String ubiduleSourcePath;
	private String isePath;
	private String vhdlPath;
	
	/**
	 * Constructor
	 */
	public ISETCLGenerator() {
	}
	

	/**
	 * Generate a full tcl script for ISE
	 * 
	 */
	public boolean generateTCLISE(M2MProject project) {
		//put out a 'heartbeat' - so we know something's happening
		System.out.println("Generation ise tcl script...");
		
		project.checkFolders();
		
		templatesPath = M2MProject.getLibPath()+"/ubidule/templates/";
		ubiduleSourcePath = M2MProject.getLibPath()+"/ubidule/";
		projectPath=project.getProperties().getProjectPath();
		isePath=project.getProperties().getProjectPath()+M2MProject.getUbiduleDirName()+"/";
		vhdlPath=project.getVHDLPath()+"/";

		FileUtils.copyFile(ubiduleSourcePath+"M2M_Test", project.getVHDLPath()+"/M2M_Test",".svn");
		
		processTemplate("script_gen_xsvf","");
		FileUtils.copyFile(templatesPath+"part1.tcl",isePath+"part1.tcl");
		processTemplate("part2",".tcl");
		generateTCL("part3");
		FileUtils.copyFile(templatesPath+"part4.tcl",isePath+"part4.tcl");
		processTemplate("part5",".tcl");
		FileUtils.copyFile(templatesPath+"part6.tcl",isePath+"part6.tcl");
		concatenateTCLParts();

		FileUtils.copyFile(ubiduleSourcePath+"M2M_Test.ucf",isePath+"M2M_Test.ucf");
		FileUtils.copyFile(ubiduleSourcePath+"PROM2_xcf32p.mcs",isePath+"PROM2_xcf32p.mcs");
		FileUtils.copyFile(ubiduleSourcePath+"batch_test.cmd",isePath+"batch_test.cmd");
		return true;
		
	}

	/**
	 * Replace the particular sequence (defined by PROJECT_PATH_TCL) 
	 * by the path of the current M2M viewer project in the template file.
	 * @param tclName The name of the generated tcl file
	 * @param outputExtension The extension of the output file.
	 */
	private void processTemplate(String tclName, String outputExtension) {
		System.out.println("processTemplate" + templatesPath + tclName);
		String newline = System.getProperty("line.separator");
		String strLine;
		LinkedList<String> list = new LinkedList<String>();
		
		// read template, line by line in a linked list
		try{

			FileInputStream fstream = new FileInputStream(templatesPath + tclName);

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			while ((strLine = br.readLine()) != null)
			  list.add(strLine);

			in.close();
			
		}catch (Exception e){
		  System.err.println("Error: " + e.getMessage());
		}

		// Replace particular sequence (PROJECT_PATH_TCL) in the each line of the linked list.
		// Write then the processed line in the new file 
		try{
			
			Writer output = new BufferedWriter(new FileWriter(isePath + tclName + outputExtension));	
			
			for (String line: list) {
				//System.out.println("before: " + line);
				line = line.replace(PROJECT_PATH_TCL, projectPath);
				line = line.replace("<XILINXDIR>", ExternalToolsProperties.getReference().getIsePath());
				line = line.replace("<UBIDULEDIRNAME>", M2MProject.getUbiduleDirName());
				line = line.replace("<XILINXLICENSE>", ExternalToolsProperties.getReference().getXilinxLicense());
				output.write(line + newline);
				System.out.println("after: " + line);
			}
				output.close();
		}catch (Exception e){
		  System.err.println("Error: " + e.getMessage() );
		}
	}

	
	private void populateVHDL(String dirName,LinkedList<String> list) {

		File dir = new File(dirName);
		String[] children = dir.list();
		if (children == null) {
		    // Either dir does not exist or is not a directory
		} else {
		    for (int i=0; i<children.length; i++) {
		        // Get filename of file or directory
		        String filename = children[i];
		        if (filename.contains(".vhd")) {
		        	//System.out.println(filename);
		        	list.add(dirName+filename);
		        }
		        File subdir=new File(dirName+filename);
		        if (subdir.isDirectory())
		        	populateVHDL(dirName+filename+"/",list);
		    }
		}

	}
	
	/**
	 * Generate the tcl file. This file contains the tcl commands for
	 * adding the generated VHDL files  to the ise project. VHDL files
	 * are taken in the standard directory defined by SRC_VHDL_PATH
	 * @param tclName temp The name of the generated tcl file
	 */
	private void generateTCL(String tclName) {
		LinkedList<String> listVHDLFiles = new LinkedList<String>();
		String newline = System.getProperty("line.separator");
		
		populateVHDL(vhdlPath,listVHDLFiles);		

		try{
			
			Writer output = new BufferedWriter(new FileWriter(isePath + tclName + ".tcl"));	
			
			// add a line for each VHDL file
			for (String line: listVHDLFiles) {
				output.write("   xfile add \"" + line + "\"" + newline);
			}
			output.close();
		}catch (Exception e){
		  System.err.println("Error: " + e.getMessage() );
		}
	}

	/**
	 * Concatenates 6 tcl parts into M2M_Test.tcl. This script
	 * create a bit file using generated VHDL.
	 */
	private void concatenateTCLParts() {
		//System.out.println(templatesPath);
		LinkedList<String> partsList = new LinkedList<String>();
		partsList.add(isePath + "part1.tcl");
		partsList.add(isePath + "part2.tcl");
		partsList.add(isePath + "part3.tcl");
		partsList.add(isePath + "part4.tcl");
		partsList.add(isePath + "part5.tcl");
		partsList.add(isePath + "part6.tcl");
		
		concatenateTCLParts(partsList);
	}

	/**
	 * Concatenates all files present in the list into M2M_Test.tcl. This script
	 * create a bit file using generated VHDL.
	 */
	private void concatenateTCLParts(LinkedList<String> list) {
		//System.out.println(tclPath);
		String strLine;
		String newline = System.getProperty("line.separator");
		try{
			
			Writer output = new BufferedWriter(new FileWriter(isePath + "M2M_Test.tcl"));
			//System.out.println("lalfsw: "+projectPath + "ise/" + "M2M_Test.tcl");
			
			// for all tcl parts (1 to 6), concatenate theirs contents to M2M_Test.tcl
			for (String line: list) {
				BufferedReader reader = new BufferedReader(new FileReader(line));
				
				while ((strLine = reader.readLine()) != null)   {
					output.write(strLine + newline);
				}
			}
			
			output.close();
		}catch (Exception e){
		  System.err.println("Error: " + e.getMessage() );
		}		
	}
		
}
