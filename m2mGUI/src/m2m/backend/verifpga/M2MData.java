/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file M2MClient.java
 * @author Mikhail Nagoga
 *
 * @version 1.0
 *
 * @date: Nov 21, 2010
 *
 * Author: Mikhail Nagoga
 *
 * Description:
 * 
 * <p>
 * This class represents the abstraction model of the M2M treatment system.
 * The system is composed by input data, number of tests and so on.
 * 
 */

package m2m.backend.verifpga;

import java.util.Vector;

public class M2MData {
	
	//private String inputFileName;
	private int nbTests;
	private int nbInputs;
	private int nbOutputs;
	
	private String dataPath;
	
	private Vector<Float>[] inputData;

	@SuppressWarnings("unchecked")
	public M2MData(int nbTests, int nbInputs, int nbOutputs, String dataPath) {
		//this.inputFileName = inputFileName;
		this.dataPath = dataPath;
		this.nbTests = nbTests;
		this.nbInputs = nbInputs;
		this.nbOutputs = nbOutputs;
		this.inputData = new Vector[nbInputs];
		for (int i=0; i<nbInputs; i++)
			inputData[i] = new Vector<Float>(nbTests);
	}
	
	/**
	 * <p>
	 * Reads the files with input data for M2M. Files were generated previously by Octave
	 */
	public void readInputData() {
		float entry;
		
		BinaryIn[] isFiles = new BinaryIn[nbInputs];
		
		for (int i=0; i<nbInputs; i++)
			isFiles[i] = new BinaryIn(dataPath + "file_input"+(i+1)+".dat"); // enumeration of input data files starts at one
		
		for (int i=0; i<nbInputs; i++) {
			System.out.println("file_input" +(i+1)+ ".dat, nbsamples: " + isFiles[i].readInt());
			System.out.println("file_input" +(i+1)+ ".dat, size: " + isFiles[i].readInt());
		}
		
		for (int i=0; i<nbInputs; i++) {
			for (int j=0; j<nbTests; j++) {
				entry = isFiles[i].readFloat();
				inputData[i].insertElementAt(entry, j);
			}
		}
	}


	public void setNbTests(int nbTests) {
		this.nbTests = nbTests;
	}

	public int getNbTests() {
		return nbTests;
	}
	
	
	
	public void setNbInputs(int nbInputs) {
		this.nbInputs = nbInputs;
	}

	public int getNbInputs() {
		return nbInputs;
	}
	
	
	
	public void setNbOutputs(int nbOutputs) {
		this.nbOutputs = nbOutputs;
	}

	public int getNbOutputs() {
		return nbOutputs;
	}

//	public void setInputData(Vector inputData) {
//		this.inputData = inputData;
//	}

	public Vector<Float>[] getInputData() {
		return inputData;
	}

}
