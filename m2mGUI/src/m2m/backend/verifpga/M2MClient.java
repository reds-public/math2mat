/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief 
 *
 * Project:  Math2Mat
 *
 * @file M2MClient.java
 * @author Mikhail Nagoga
 *
 * @version 1.0
 *
 * @date: Nov 21, 2010
 *
 * Author: Mikhail Nagoga
 *
 * Description:
 * 
 * <p>
 * This class contains all attributes and functions for 
 * communication with the embedded QT server
 * 
 */
package m2m.backend.verifpga;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;

import m2m.backend.processing.Errors;
import m2m.backend.processing.ProcessWatcher;
import m2m.backend.project.ExternalToolsProperties;
import m2m.backend.project.M2MProject;

public class M2MClient {

	public final static int M2M_TEST_PORT = 55444;
	
	public final static int COMMAND_DEBUG = 2;
	public final static int COMMAND_NBTESTS = 3;
	public final static int COMMAND_INPUTDATA = 4;
	public final static int COMMAND_STARTM2M = 5;
	public final static int COMMAND_XSVF = 6;
	public final static int COMMAND_NBINPUTS = 7;
	public final static int COMMAND_NBOUTPUTS = 8;
	
	
	//public final static String server_name = "localhost";
//	public final static String server_name = "10.192.51.40";
	
	private M2MData m2mData;
	private String isePath;
		
	private M2MProject project;
	

	/**
	 * Constructor
	 */
	public M2MClient() {
	}

	/**
	 * <p>
	 * This function is executed when the user press on the button
	 * 'Launch ubidule verification'
	 * <p>
	 * One can (un)comment desired lines. For example, if you don't want
	 * to send and program the XSVF file, you should to comment 'sendXSVF(os)' 
	 */
	public boolean testOnUbidule(int nbrInput, int nbrOutput, int nbrSamples, M2MProject project) {
		m2mData = new M2MData(nbrSamples,nbrInput,nbrOutput, project.getIOFilesPath()+"/");
		
		m2mData.readInputData();
		this.isePath = project.getProperties().getProjectPath() + "/"+ M2MProject.getUbiduleDirName();
		this.project = project;

//		System.out.println("Generating XSVF file");
//		generateXSVF();
//		System.out.println("XSVF is generated...");
		Socket sock;
		try {
			sock = new Socket(ExternalToolsProperties.getReference().getUbiduleIP(), M2M_TEST_PORT);

		}catch (IOException e) {
				System.err.println(e);
				Errors.setLastError(Errors.ErrorNum.UBIDULECONNECTIONERROR);
				return false;
		}
		
		BinaryOut os = new BinaryOut(sock);
		BinaryIn is = new BinaryIn(sock);

		sendXSVF(os);
			
		sendNbInputs(os);
		sendNbOutputs(os);
		sendNbTests(os);
		sendInputData(os);

			
		sendM2MStart(os);
		
		os.flush();
		
			 
		receiveM2MData(is);

			
		compareOctaveFPGA();
			
		try {
			sock.close();
		}
		catch (IOException e) {
			
		}
		
		return true;
	}


	
	/**
	 * <p>
	 * Waits for treated data from M2M
	 * @param is Client's socket input stream
	 */
	private void receiveM2MData(BinaryIn is) {
		float data;
		LinkedList<Float> dataList = new LinkedList<Float>();

		System.out.println("Waiting for the reception of M2MData...");
		int sizeBlock = is.readInt();
		//os.writeInt(sizeBlock/4-1);
//		System.out.println("Blocksize: " + (sizeBlock/4-1));
		
		// there is a *one* in the files produced by Octave...
		//os.writeInt(1);
		
		/*int command = */is.readInt();
//		System.out.println("Command: " + command);
		
		int nbData=is.readInt();
		for (int i=0; i<nbData; i++) {
			data = is.readFloat();
			//os.writeFloat(data);
			dataList.add(data);
//			System.out.println("From server " + i + " " + data);
		}
		int time=is.readInt();
		System.out.println("Time of treatement: " + time);
		
//		int latency=is.readInt();
		is.readInt();
//		System.out.println("Latency: " + latency);
		
		
		// create files with results of M2M treatment
		DataOutputStream[] os = new DataOutputStream[m2mData.getNbOutputs()];
		try {
			//fileoutputstream = new FileOutputStream("file.dat");

			for (int i=0; i<m2mData.getNbOutputs(); i++) {
				os[i] = new DataOutputStream(new FileOutputStream(project.getIOFilesPath() + "/file_output" + (i+1) + "_FPGA.dat"));
				os[i].writeInt((sizeBlock/4-1)/m2mData.getNbOutputs());
				os[i].writeInt(1);
			}
			
			for (int i=0; i<dataList.size(); i++) {
				os[0].writeFloat(dataList.get(i));
			}
			
		
	    } catch (IOException e) {
			e.printStackTrace();
			System.err.println("There is a problem into M2MClient.receiveM2MData...");
		}
	}

	/**
	 * <p>
	 * Compares the output M2M data. Octave vs FPGA
	 * @param is Client's socket input stream
	 */
	private boolean compareOctaveFPGA() {
		int cnt = 0;
		
		BinaryIn[] isFiles = new BinaryIn[m2mData.getNbOutputs()];
		BinaryIn[] isFilesFPGA = new BinaryIn[m2mData.getNbOutputs()];
		
		for (int i=0; i<m2mData.getNbOutputs(); i++) {
			isFiles[i] = new BinaryIn(project.getIOFilesPath() + "/file_output"+(i+1)+".dat"); // enumeration of input data files starts at one
			isFilesFPGA[i] = new BinaryIn(project.getIOFilesPath() + "/file_output"+(i+1)+"_FPGA.dat"); // enumeration of input data files starts at one
		}
		
		int nbTests;
		
		System.out.println("file_output" +(1)+ ".dat, nbsamples: " + isFiles[0].readInt());
		System.out.println("file_output" +(1)+ ".dat, size: " + isFiles[0].readInt());
		nbTests = isFilesFPGA[0].readInt();
		System.out.println("file_output" +(1)+ "_FPGA.dat, nbsamples: " + nbTests);
		System.out.println("file_output" +(1)+ "_FPGA.dat, size: " + isFilesFPGA[0].readInt());

//		System.out.println("file_output" +(2)+ ".dat, nbsamples: " + isFiles[1].readInt());
//		System.out.println("file_output" +(2)+ ".dat, size: " + isFiles[1].readInt());
//
//		System.out.println("file_output" +(2)+ "_FPGA.dat, nbsamples: " + isFilesFPGA[1].readInt());
//		System.out.println("file_output" +(2)+ "_FPGA.dat, size: " + isFilesFPGA[1].readInt());		

//		if (nbTests != m2mData.getNbTests()) {
//			System.err.println("Error, number of samples is wrong...");
//			return false;
//		}
		
		for (int currentNbOut=0; currentNbOut<m2mData.getNbOutputs(); currentNbOut++) {
			for (int currentNbSample=0; currentNbSample<m2mData.getNbTests()-1; currentNbSample++) {
				float dataOctave;
				float dataFPGA;
				float absVal;
				dataOctave  = isFiles[currentNbOut].readFloat();
				dataFPGA 	= isFilesFPGA[currentNbOut].readFloat();
				absVal = dataOctave - dataFPGA;
				
				if (absVal < 0 )
					absVal = -absVal;
				
				if (absVal > 0.1) {
					System.err.println("Files nbr " + currentNbOut + " ,sample nbr " + currentNbSample + " " + dataOctave + " vs " + dataFPGA);
					++cnt;
				}
				
			}
			
			
		}
		
		System.err.println("There are " + cnt + " errors in " + m2mData.getNbOutputs()*m2mData.getNbTests() + " samples");


		return true;
	}

	/**
	 * <p>
	 * Send the number of tests to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendNbTests(BinaryOut os) {
		System.out.println("sendNbTest...");
		os.write(COMMAND_NBTESTS);
		os.write(4);
		os.write(m2mData.getNbTests());
		os.flush();
	}

	/**
	 * <p>
	 * Send the number of inputs to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendNbInputs(BinaryOut os) {
		System.out.println("sendNbInputs...");
		os.write(COMMAND_NBINPUTS);
		os.write(4);
		os.write(m2mData.getNbInputs());
		os.flush();
	}
	
	/**
	 * <p>
	 * Send the number of outputs to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendNbOutputs(BinaryOut os) {
		System.out.println("sendNbOutputs...");
		os.write(COMMAND_NBOUTPUTS);
		os.write(4);
		os.write(m2mData.getNbOutputs());
		os.flush();
	}
	
	/**
	 * <p>
	 * Send the input data for treatment to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendInputData(BinaryOut os) {
		System.out.println("sendInputData...");
		float entry;
		
		int inputSize = m2mData.getNbTests()*m2mData.getNbInputs()*4;
		
		os.write(COMMAND_INPUTDATA);	
		os.write(inputSize);

		for (int currentTest=0; currentTest<m2mData.getNbTests(); currentTest++) {
			for (int currentIn=0; currentIn<m2mData.getNbInputs(); currentIn++) {
				entry = (Float) m2mData.getInputData()[currentIn].get(currentTest);
				os.write(entry);
			}
		}
		
		os.flush();
	}

	
	/**
	 * <p>
	 * Send the signal to start to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendM2MStart(BinaryOut os) {
		System.out.println("sendM2MStart...");
		os.write(COMMAND_STARTM2M);
		os.write(0);
		os.flush();
	}

	/**
	 * <p>
	 * Send the xsvf programming file to the embedded server
	 * @param os Client's socket output stream
	 */
	private void sendXSVF(BinaryOut os) {
		System.out.println("sendXSVF...");
		os.write(COMMAND_XSVF);
		
		// FIXME if the file doesn't exist ...
		File myFile = new File (isePath + "/M2M_XSVF_generated.xsvf");
		//File myFile = new File ("data.dat");
		byte [] mybytearray  = new byte [(int)myFile.length()];
		System.out.println("File length: " + myFile.length());
		
		// block size
		os.write((int)myFile.length());
	  
		try
		{
			FileInputStream fis = new FileInputStream(myFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bis.read(mybytearray,0,mybytearray.length);
  
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	  
		System.out.println("Sending xsvf...");
	
		// write xsvf file to socket
		for (int i=0; i<mybytearray.length; i++) {
			os.write(mybytearray[i]);
		}

		os.flush();
		
	}
	
	
	public static boolean generateXSVF(M2MProject project) {
		String isePath = project.getProperties().getProjectPath() + M2MProject.getUbiduleDirName();


		String line = "";
		BufferedReader brOctave;
		BufferedReader brErrOctave;
		
		Errors.clearError();
		try
		{         

			// change write access of script to execute, otherwise an error
			File scriptGenXSVF = new File(isePath  + "/script_gen_xsvf");
			scriptGenXSVF.setExecutable(true);
						 
		    Process process = new ProcessBuilder(isePath  + "/script_gen_xsvf").start();
		    
		    ProcessWatcher.getInstance().addProcess(process);
		    
			//create streams to communicate with Octave application
	//		BufferedWriter bwOctave;
	//		bwOctave  = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			brOctave = new BufferedReader(new InputStreamReader(process.getInputStream()));
			brErrOctave = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		

			if (brOctave == null || brErrOctave == null)
				return false;

			while (true) {
				try {
					if ((line = brOctave.readLine()) != null)
						System.out.println(line);
					else
						break;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			while (true) {
				try {
					if ((line = brErrOctave.readLine()) != null)
						System.err.println(line);
					else {
						System.out.println("XSVF generating finished...");
						break;
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			process.waitFor();
			System.out.println("Process exit code: "+process.exitValue()+"\n");
			if (process.exitValue()==127)
				Errors.setLastError(Errors.ErrorNum.ISENOTFOUND);
			
		    ProcessWatcher.getInstance().removeProcess(process);
  
		} catch (Throwable t)
		  {
		    t.printStackTrace();
		  }
		
		
		return true;
	}
	
	/**
	 * <p>
	 * Launch <tt>script_gen_xsvf<tt> script. The script can create the bit
	 * and xsvf files
	 * 
	 */
	public void generateXSVF() {
		generateXSVF(this.project);
	}
	

}