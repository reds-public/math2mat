/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 2.7.7 (20060906): "octaveParser.g" -> "OctaveParser.java"$

package m2m.backend.octaveparser;

import m2m.backend.buildingblocks.BuildingBlocksManager;
import m2m.backend.structure.*;
import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;


import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;

public class OctaveParser extends antlr.LLkParser       implements OctaveParserTokenTypes
 {

	/**
	 *  Store the instance of the object who wFill treat the stucture
	 */
	StructTreatment treat;
	/**
	 *  Root of the structure
	 */
	Function top;
	/**
	 *  Indicates if the parsing was successful
	 */
	boolean successful;
	 
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 *  Define the object who treat the structure
	 */
	public void setTreat(StructTreatment treat) {
		this.treat = treat;
	}
	/**
	 *  Define the root of the structure
	 */
	public void setTop(Function top) {
		this.top = top;
	}

	public void reportError(RecognitionException e) //throws ParsingException
	{
		System.out.println("Parsing error at line " + e.line);
		successful=false;
	//	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	//	Editor editor = ((Editor)page.getActiveEditor());
	//	editor.setParseDone(false);
	}

	private HashMap<String, ReadWriteIndex> varCpt = new HashMap<String, ReadWriteIndex>();

protected OctaveParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public OctaveParser(TokenBuffer tokenBuf) {
  this(tokenBuf,1);
}

protected OctaveParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public OctaveParser(TokenStream lexer) {
  this(lexer,1);
}

public OctaveParser(ParserSharedInputState state) {
  super(state,1);
  tokenNames = _tokenNames;
}

/**
 *  Main entry for the parsing
 *  entry = { pragma } , function
 *  Entry is a list of pragma followed by only one function
 */
	public final void entry() throws RecognitionException, TokenStreamException, ParsingException {
		
		
				Function func = null;
				successful=true;
			
		
		try {      // for error handling
			{
			_loop3:
			do {
				if ((LA(1)==M2M)) {
					pragma();
				}
				else {
					break _loop3;
				}
				
			} while (true);
			}
			func=function();
			
					this.treat.setTop(func);
					this.treat.clean(func.getBody(), new Vector<Element>());
				
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
	}
	
/**
 * Describe a pragma
 * pragma = "%m2m", ident, ':', ident
 * Pragma is used for pass some parameters to from the octave code to
 * the vhdl generator
 */
	public final void pragma() throws RecognitionException, TokenStreamException {
		
		
				String i1,i2;
			
		
		try {      // for error handling
			match(M2M);
			i1=ident();
			match(COLON);
			i2=ident();
			
						System.out.println("#Pramga détécté!"+i1+" "+i2);
						treat.addPragma(i1,i2);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_1);
		}
	}
	
/**
 * Define a function
 * function = function_begin, body, function_end
 * At the moment, can only be used tu define the main function
 */
	public final Function  function() throws RecognitionException, TokenStreamException, ParsingException {
		Function func = null;
		
		
				Vector<Element> b = null;
				Function tmpTop = this.top;
			
		
		try {      // for error handling
			func=function_begin();
			b=body();
			function_end();
			
						if(func == null)
							throw new ParsingException("Invalid function");
							
						func.addBody(b);
						// Create all affectations for the input and output variables
						this.treat.createInOutAff(func, this.varCpt);
						this.top = tmpTop;
						this.treat.setTop(this.top);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		return func;
	}
	
/**
 *  Defines one affectation
 *  affect = name , [ tab_ind ] , "=" , expr_lvl1 , ","
 *  The tab_ind rule is used for vector.
 *  expr_lvl1 is the root rule for an expression.
 */
	public final Vector<Element>  affect() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				String name = null;
				Vector<Element> exp = null;
				Element var = null;
				boolean vector = false;
				Integer ind = null;
			
		
		try {      // for error handling
			name=ident();
			{
			switch ( LA(1)) {
			case LPAR:
			{
				ind=tab_ind();
				vector=true;
				break;
			}
			case EQUAL:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(EQUAL);
			exp=expr_lvl1();
			{
			switch ( LA(1)) {
			case SEMI:
			{
				match(SEMI);
				break;
			}
			case FUNCTION:
			case END:
			case ENDFUNCTION:
			case IF:
			case ENDIF:
			case ELSEIF:
			case ELSE:
			case STRING:
			case ENDFOR:
			case FOR:
			case ENDWHILE:
			case WHILE:
			case PRINTF:
			case ERROR:
			case RETURN:
			case SWITCH:
			case CASE:
			case OTHERW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			
						if(exp == null)
							throw new ParsingException("Invalid assignment expression");
					
						if (!this.varCpt.containsKey(name)) {
							this.varCpt.put(name, new ReadWriteIndex());
						} else {
							this.varCpt.put(name, ((ReadWriteIndex)this.varCpt.get(name)).getNextVariable());
						}
						var = treat.createVar(name+"_m2m_"+((ReadWriteIndex)this.varCpt.get(name)).getReadIndex(), 
								0.0, "", false).lastElement();
						// If variable contains an indice, we need to treat it as a
						// VectorVariable and set the pragma for this variable
						if (vector) {
							if(ind == null)
								throw new ParsingException("Invalid vector index");
							var = new VectorVariable((Variable)var, ind);
							treat.addPragma(name, "vector");
						}
						
						vec = treat.createAff((Variable)var, exp);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return vec;
	}
	
/**
 * Define an ident wich is a string
 */
	public final String  ident() throws RecognitionException, TokenStreamException {
		String i=new String();
		
		Token  s = null;
		
		try {      // for error handling
			s = LT(1);
			match(STRING);
			i=s.getText();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_4);
		}
		return i;
	}
	
/**
 * Intercept the tab index used for access a variable in a vector
 * tab_ind = '(', NUMBER, ')'
 */
	public final Integer  tab_ind() throws RecognitionException, TokenStreamException {
		Integer ind = null;
		
		Token  n = null;
		
		try {      // for error handling
			match(LPAR);
			n = LT(1);
			match(NUMBER);
			match(RPAR);
			ind = Integer.parseInt(n.getText());
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_5);
		}
		return ind;
	}
	
/**
 * Used to describe the first level operation wich contains addition and dot addition
 * expr_lvl1 = expr_lvl2, { ( "+" | ".+" ), expr_lvl2 }
 */
	public final Vector<Element>  expr_lvl1() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=expr_lvl2();
			{
			_loop17:
			do {
				if ((LA(1)==PLUS||LA(1)==DPLUS)) {
					{
					switch ( LA(1)) {
					case PLUS:
					{
						match(PLUS);
						break;
					}
					case DPLUS:
					{
						match(DPLUS);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					tmp=expr_lvl2();
					
								try {
									op = new Addition();
									vec = treat.createOp(vec,tmp,op);
								} 
								catch (Exception e){
									throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
								}
							
				}
				else {
					break _loop17;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_6);
		}
		return vec;
	}
	
/**
 * Defines a body
 * body = { instruction }
 */
	public final Vector<Element>  body() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				vec = new Vector<Element>();
			
		
		try {      // for error handling
			
						for (String s : varCpt.keySet()) {
							((ReadWriteIndex)this.varCpt.get(s)).addReadIndex();
						}
					
			{
			_loop9:
			do {
				if ((_tokenSet_7.member(LA(1)))) {
					tmp=instruction();
					
									if (tmp != null) {
										vec.addAll(tmp);
									}
								
				}
				else {
					break _loop9;
				}
				
			} while (true);
			}
			
						for (String s : varCpt.keySet()) {
							((ReadWriteIndex)this.varCpt.get(s)).removeReadIndex();
						}
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_8);
		}
		return vec;
	}
	
/**
 * Describe all the instructions
 * instruction = affect | function | loop_for | loop_while 
 *               | op_switch | ifthenelse | not_instruction_cmd
 */
	public final Vector<Element>  instruction() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Element el = null;
			
		
		try {      // for error handling
			switch ( LA(1)) {
			case STRING:
			{
				vec=affect();
				break;
			}
			case FUNCTION:
			case FOR:
			case WHILE:
			case SWITCH:
			{
				{
				switch ( LA(1)) {
				case FUNCTION:
				{
					el=function();
					break;
				}
				case FOR:
				{
					el=loop_for();
					break;
				}
				case WHILE:
				{
					el=loop_while();
					break;
				}
				case SWITCH:
				{
					el=op_switch();
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				
							vec = new Vector<Element>();
							vec.add(el);
						
				break;
			}
			case IF:
			{
				vec=ifthenelse();
				break;
			}
			case PRINTF:
			case ERROR:
			case RETURN:
			{
				not_instruction_cmd();
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return vec;
	}
	
	public final Vector<Element>  body_function() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				vec = new Vector<Element>();
			
		
		try {      // for error handling
			{
			_loop12:
			do {
				if ((_tokenSet_7.member(LA(1)))) {
					tmp=instruction();
					
									if (tmp != null) {
										vec.addAll(tmp);
									}
								
				}
				else {
					break _loop12;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_9);
		}
		return vec;
	}
	
/**
 * Defines all the comparison operand
 * cmp_op = "==" | "!=" | "<" | ">" | "<=" | ">="
 */
	public final Operation  cmp_op() throws RecognitionException, TokenStreamException {
		Operation op = null;
		
		
		try {      // for error handling
			switch ( LA(1)) {
			case CEQUAL:
			{
				match(CEQUAL);
				op = new Equal();
				break;
			}
			case CNOTEQ:
			{
				match(CNOTEQ);
				op = new NotEqual();
				break;
			}
			case CLESS:
			{
				match(CLESS);
				op = new Less();
				break;
			}
			case CGREAT:
			{
				match(CGREAT);
				op = new Greater();
				break;
			}
			case CLESEQ:
			{
				match(CLESEQ);
				op = new LessEqual();
				break;
			}
			case CGREEQ:
			{
				match(CGREEQ);
				op = new GreaterEqual();
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_10);
		}
		return op;
	}
	
/**
 * Used to describe the second level operation wich contains subtraction and dot subtraction
 * expr_lvl2 = expr_lvl3, { ( "-" | ".-" ), expr_lvl3 }
 */
	public final Vector<Element>  expr_lvl2() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=expr_lvl3();
			{
			_loop21:
			do {
				if ((LA(1)==MINUS||LA(1)==DMINUS)) {
					{
					switch ( LA(1)) {
					case MINUS:
					{
						match(MINUS);
						break;
					}
					case DMINUS:
					{
						match(DMINUS);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					tmp=expr_lvl3();
							
								try{
									op = new Subtraction();
									vec = treat.createOp(vec,tmp,op);
								} 
								catch (Exception e){
									throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
								}
							
				}
				else {
					break _loop21;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_11);
		}
		return vec;
	}
	
/**
 * Used to describe the third level operation wich contains multiplication and dot multiplication
 * expr_lvl3 = expr_lvl4, } ( "*" | ".*" ), expr_lvl4 }
 */
	public final Vector<Element>  expr_lvl3() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=expr_lvl4();
			{
			_loop25:
			do {
				if ((LA(1)==MUL||LA(1)==DMUL)) {
					{
					switch ( LA(1)) {
					case MUL:
					{
						match(MUL);
						op = new Multiplication();
						break;
					}
					case DMUL:
					{
						match(DMUL);
						op = new DotMultiplication();
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					tmp=expr_lvl4();
					
								
								try {
									vec = treat.createOp(vec,tmp,op);
								} 
								catch (Exception e){
									throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
								}
							
				}
				else {
					break _loop25;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_12);
		}
		return vec;
	}
	
/**
 * Used to describe the fourth level operation wich contains division and dot division
 * expr_lvl4 = expr_lvl5, { ( "/" | "./" ), expr_lvl5 }
 */
	public final Vector<Element>  expr_lvl4() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=expr_lvl5();
			{
			_loop29:
			do {
				if ((LA(1)==DIV||LA(1)==DDIV)) {
					{
					switch ( LA(1)) {
					case DIV:
					{
						match(DIV);
						op = new Division();
						break;
					}
					case DDIV:
					{
						match(DDIV);
						op = new DotDivision();
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					tmp=expr_lvl5();
					
								try{
									vec = treat.createOp(vec,tmp,op);
								} 
								catch (Exception e){
									throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
								}
							
				}
				else {
					break _loop29;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_13);
		}
		return vec;
	}
	
/**
 * Used to describe the fifth level operation wich contains power and parenthese
 * expr_lvl5 = ( func_var, { ( "^" | "**" ), func_var } | "(", expr_lvl1, ")" )
 */
	public final Vector<Element>  expr_lvl5() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Operation op = null;
				Vector<Element> tmp;
			
		
		try {      // for error handling
			switch ( LA(1)) {
			case MINUS:
			case LBRAK:
			case STRING:
			case NUMBER:
			{
				vec=func_var();
				{
				_loop33:
				do {
					if ((LA(1)==PWR1||LA(1)==PWR2)) {
						{
						switch ( LA(1)) {
						case PWR1:
						{
							match(PWR1);
							break;
						}
						case PWR2:
						{
							match(PWR2);
							break;
						}
						default:
						{
							throw new NoViableAltException(LT(1), getFilename());
						}
						}
						}
						tmp=func_var();
						
									try {
										op = new Power();
										vec = treat.createOp(vec, tmp, op);
									} 
									catch (Exception e){
										throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
									}
								
					}
					else {
						break _loop33;
					}
					
				} while (true);
				}
				break;
			}
			case LPAR:
			{
				match(LPAR);
				vec=expr_lvl1();
				match(RPAR);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_14);
		}
		return vec;
	}
	
/**
 * Describe a variable or a function call
 * func_var = ( [ "-" ], ( ident, [ "(", func_param, ")" ] | number )
 *            | "[", [ [ "-" ], number, { [ "," ] , [ "-" ], number } ] )
 * Accept all the variables or function call like:
 *   1.0
 *   -2.45
 *   sqrt(10)
 *   [ 10 -4, 2 ]
 */
	public final Vector<Element>  func_var() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				boolean minus = false;
				boolean param = false;
				String s = null;
				Vector<Element> par = null;
				Element el = null;
				Double val = null;
				SimpleVariable var = null;
			
		
		try {      // for error handling
			switch ( LA(1)) {
			case MINUS:
			case STRING:
			case NUMBER:
			{
				{
				switch ( LA(1)) {
				case MINUS:
				{
					match(MINUS);
					minus = true;
					break;
				}
				case STRING:
				case NUMBER:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				{
				switch ( LA(1)) {
				case STRING:
				{
					s=ident();
					{
					switch ( LA(1)) {
					case LPAR:
					{
						match(LPAR);
						par=func_param();
						match(RPAR);
						param = true;
						break;
					}
					case SEMI:
					case CEQUAL:
					case CNOTEQ:
					case CLESS:
					case CGREAT:
					case CLESEQ:
					case CGREEQ:
					case PLUS:
					case DPLUS:
					case MINUS:
					case DMINUS:
					case MUL:
					case DMUL:
					case DIV:
					case DDIV:
					case PWR1:
					case PWR2:
					case RPAR:
					case COMMA:
					case FUNCTION:
					case END:
					case ENDFUNCTION:
					case IF:
					case ENDIF:
					case ELSEIF:
					case ELSE:
					case STRING:
					case COR:
					case CAND:
					case ENDFOR:
					case FOR:
					case COLON:
					case ENDWHILE:
					case WHILE:
					case PRINTF:
					case ERROR:
					case RETURN:
					case SWITCH:
					case CASE:
					case RBRAC:
					case OTHERW:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					
									// Unable to call a function wich have the same name that a variable
									if (treat.isVariable(s) && param && par.size() > 1) {
										throw new RecognitionException("error");
									// If the name is a variable
									} else if (treat.isVariable(s)) {
										if (!this.varCpt.containsKey(s)) {
											this.varCpt.put(s, new ReadWriteIndex());
										}
										vec = new Vector<Element>();
										vec = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", minus);
										if (param && par.size() == 1) {
											el = new VectorVariable((Variable)vec.lastElement(), (int)((Variable)par.lastElement()).getVal());
											((Variable)vec.lastElement()).setType("vector");
											vec.remove(vec.lastElement());
											vec.add(el);
										}
									// If there is a function with no minus in front of
									} else if (param && !minus) {
					if(BuildingBlocksManager.getInstance().isABlock(s)) {
					el = new GenericOperation(s);
					el.setName(s+treat.getNextOpCpt());
					((Operation)el).setInput(par);
					vec = new Vector<Element>();
					vec.add(el);
					}
					else {
					throw new ParsingException("Operation generic \""+s+"\" non definie.");
					}
									} else {
										throw new ParsingException("Variable "+s+" non déclarée au préalable.");
									}
								
					break;
				}
				case NUMBER:
				{
					val=number();
					
									// If this is a number, create a normal variable
									if (minus) {
										val = -val;
									}
									if (!this.varCpt.containsKey("const")) {
										this.varCpt.put("const", new ReadWriteIndex());
									} else {
										this.varCpt.put("const", ((ReadWriteIndex)this.varCpt.get("const").getNextVariable()));
									}
									try {
										vec = treat.createVar("m2m_const_"+((ReadWriteIndex)this.varCpt.get("const")).getReadIndex(), val, "const", false);
									} 
									catch (Exception e){
										throw new ParsingException("Invalid character for a constant variable");
									}			
								
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				break;
			}
			case LBRAK:
			{
				match(LBRAK);
				
							// If there is values inside brackets, we need to create a vector
							vec = new Vector<Element>();
							var = new SimpleVariable();
							var.setType("vector");
							vec.add(var);
						
				{
				switch ( LA(1)) {
				case MINUS:
				case NUMBER:
				{
					{
					switch ( LA(1)) {
					case MINUS:
					{
						match(MINUS);
						minus = true;
						break;
					}
					case NUMBER:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					val=number();
					
								if (minus)
									val = -val;
								var.addVal(val);
								minus = false;
							
					{
					_loop46:
					do {
						if ((LA(1)==MINUS||LA(1)==COMMA||LA(1)==NUMBER)) {
							{
							switch ( LA(1)) {
							case COMMA:
							{
								match(COMMA);
								break;
							}
							case MINUS:
							case NUMBER:
							{
								break;
							}
							default:
							{
								throw new NoViableAltException(LT(1), getFilename());
							}
							}
							}
							{
							switch ( LA(1)) {
							case MINUS:
							{
								match(MINUS);
								minus = true;
								break;
							}
							case NUMBER:
							{
								break;
							}
							default:
							{
								throw new NoViableAltException(LT(1), getFilename());
							}
							}
							}
							val=number();
							
										if (minus)
												val = -val;
										var.addVal(val);
										minus = false;
									
						}
						else {
							break _loop46;
						}
						
					} while (true);
					}
					break;
				}
				case RBRAK:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				match(RBRAK);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_15);
		}
		return vec;
	}
	
/**
 * Rule for the inputs of any function
 * func_param = expr_lvl1, { ",", expr_lvl1 }
 * Returns the list of all input variables
 */
	public final Vector<Element>  func_param() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> par = null;
		
		
				Vector<Element> vec = null;
			
		
		try {      // for error handling
			vec=expr_lvl1();
			
						par = vec;
					
			{
			_loop36:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					vec=expr_lvl1();
					
								par.add(vec.lastElement());
							
				}
				else {
					break _loop36;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_16);
		}
		return par;
	}
	
/**
 * Parse a simple number
 * number = NUMBER, [ '.', NUMBER ]
 */
	public final Double  number() throws RecognitionException, TokenStreamException {
		Double val = null;
		
		Token  n1 = null;
		Token  n2 = null;
		
				String s = null;
				Boolean minus = false;
			
		
		try {      // for error handling
			n1 = LT(1);
			match(NUMBER);
			
						s = new String();
						if (minus) {
							s += "-";
						}
						s += n1.getText();
					
			{
			switch ( LA(1)) {
			case DOT:
			{
				match(DOT);
				n2 = LT(1);
				match(NUMBER);
				s += "."+n2.getText();
				break;
			}
			case SEMI:
			case CEQUAL:
			case CNOTEQ:
			case CLESS:
			case CGREAT:
			case CLESEQ:
			case CGREEQ:
			case PLUS:
			case DPLUS:
			case MINUS:
			case DMINUS:
			case MUL:
			case DMUL:
			case DIV:
			case DDIV:
			case PWR1:
			case PWR2:
			case RPAR:
			case COMMA:
			case RBRAK:
			case FUNCTION:
			case END:
			case ENDFUNCTION:
			case IF:
			case ENDIF:
			case ELSEIF:
			case ELSE:
			case STRING:
			case COR:
			case CAND:
			case ENDFOR:
			case FOR:
			case COLON:
			case ENDWHILE:
			case WHILE:
			case PRINTF:
			case ERROR:
			case RETURN:
			case NUMBER:
			case SWITCH:
			case CASE:
			case RBRAC:
			case OTHERW:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			
						val = Double.parseDouble(s);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_17);
		}
		return val;
	}
	
/**
 * Describe the begin of a function
 * function_begin = "function", ( '[', ident, { ',', ident }, ']' | ident ),
 *                  "=", ident, '(', param, ')'
 */
	public final Function  function_begin() throws RecognitionException, TokenStreamException, ParsingException {
		Function func = null;
		
		
				String name,out = null;
				Vector<Element> par = null;
			
		
		try {      // for error handling
			match(FUNCTION);
			func = new Function(); this.top = func; this.treat.setTop(this.top);
			{
			switch ( LA(1)) {
			case LBRAK:
			{
				match(LBRAK);
				out=ident();
				
								Variable var = (Variable)treat.createInOut(out);
								//this.top.addOutput(var);
								func.addOutput(var);
							
				{
				_loop51:
				do {
					if ((LA(1)==COMMA)) {
						match(COMMA);
						out=ident();
						
											var = (Variable)treat.createInOut(out);
											func.addOutput(var);
										
					}
					else {
						break _loop51;
					}
					
				} while (true);
				}
				match(RBRAK);
				break;
			}
			case STRING:
			{
				out=ident();
				
								Variable var = (Variable)treat.createInOut(out);
								func.addOutput(var);
							
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(EQUAL);
			name=ident();
			match(LPAR);
			par=param();
			match(RPAR);
			
						func.setName(name);
						func.setInput(par);
						for (Element var : par) {
							if (var instanceof Variable) {
								this.varCpt.put(treat.getTrueName(var.getName()), new ReadWriteIndex());
							}
						}
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_18);
		}
		return func;
	}
	
/**
 * Describe the end of a function
 * function_end = ( "end", "function", ident, ';' | "endfunction", [ ';' ] )
 */
	public final void function_end() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			switch ( LA(1)) {
			case END:
			{
				match(END);
				match(FUNCTION);
				match(SEMI);
				break;
			}
			case ENDFUNCTION:
			{
				match(ENDFUNCTION);
				{
				switch ( LA(1)) {
				case SEMI:
				{
					match(SEMI);
					break;
				}
				case EOF:
				case FUNCTION:
				case END:
				case ENDFUNCTION:
				case IF:
				case ENDIF:
				case ELSEIF:
				case ELSE:
				case STRING:
				case ENDFOR:
				case FOR:
				case ENDWHILE:
				case WHILE:
				case PRINTF:
				case ERROR:
				case RETURN:
				case SWITCH:
				case CASE:
				case OTHERW:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
	}
	
/**
 * Define the parameters for, in example, a function
 * param = ident, { ',', ident }
 */
	public final Vector<Element>  param() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> par = null;
		
		
				String s;
				Variable var;
			
		
		try {      // for error handling
			s=ident();
			
						par = new Vector<Element>();
						var = treat.createInOut(s);
						par.add(var);
					
			{
			_loop96:
			do {
				if ((LA(1)==COMMA)) {
					match(COMMA);
					s=ident();
					
								var = treat.createInOut(s);
								par.add(var);
							
				}
				else {
					break _loop96;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_16);
		}
		return par;
	}
	
/**
 * Define the main part for the if-then-else instruction
 * ifthenelse = "if", ifthenelsegen, ( "end" | "endif" )
 */
	public final Vector<Element>  ifthenelse() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
		try {      // for error handling
			match(IF);
			vec=ifthenelsegen();
			{
			switch ( LA(1)) {
			case END:
			{
				match(END);
				break;
			}
			case ENDIF:
			{
				match(ENDIF);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return vec;
	}
	
/**
 * Define the second part of the if-then-else instruction.
 * ifthenelsegen = '(', logexpr ')', [ "then" ], body, [ "elseif", ifthenelsegen | "else", body ]  
 */
	public final Vector<Element>  ifthenelsegen() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				IfThenElse ite = null;
				Vector<Element> cond = null;
				Vector<Element> tmp = null;
				IfThenElse next = null;
				Element var;
				Vector<Element> b = null;
				HashMap<String, Element> list1 = new HashMap<String, Element>();
				HashMap<String, Element> list2 = new HashMap<String, Element>();
				
				/* Use to go through the body */
				Element element;
				Vector<Element> elementList = new Vector<Element>();
				Iterator<Element> iterElement;
			
		
		try {      // for error handling
			match(LPAR);
			cond=logexpr();
			match(RPAR);
			{
			switch ( LA(1)) {
			case THEN:
			{
				match(THEN);
				break;
			}
			case FUNCTION:
			case END:
			case IF:
			case ENDIF:
			case ELSEIF:
			case ELSE:
			case STRING:
			case FOR:
			case WHILE:
			case PRINTF:
			case ERROR:
			case RETURN:
			case SWITCH:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			b=body();
			
						// Simple check to avoid errors on condition
						if(cond == null)
							throw new ParsingException("Invalid \"if\" condition");
					
						vec = new Vector<Element>();
						ite = new IfThenElse();
						vec.add(ite);
						treat.count(ite);
						// Create the condition output variable
						Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
						sv.setType("cond");
						// Assign the right element to the condition variable
						if (cond.lastElement() instanceof Variable) {
							Assignment aff = new Assignment();
							aff.addInput(cond.lastElement());
							aff.addOutput(sv);
							cond = new Vector<Element>();
							cond.add(aff);
						} else {
							((Operation)cond.lastElement()).addOutput(sv);
			
			/* Add condition input */
			elementList.addAll(((Operation)cond.lastElement()).getInput());
			iterElement = elementList.iterator();
			while (iterElement.hasNext()) 
			{
			element = iterElement.next();
			
			if(element instanceof Variable && !ite.getInput().contains(element) && !((Variable)element).getType().equals("const"))
			ite.addInput(element);
			else if(element instanceof Variable && !ite.getInternalVars().contains(element) && ((Variable)element).getType().equals("const"))
			ite.addInternalVar(element);
			else if(element instanceof Operation)
			elementList.addAll(((Operation)element).getInput());
			
			elementList.remove(elementList.indexOf(element));
			iterElement = elementList.iterator();	
			}	
						}
						// Add the condition variable if there isn't on temp or output
						if (!ite.getInternalVars().contains(sv)) {
							ite.addInternalVar(sv);
						}
						if (!ite.getOutput().contains(sv)) {
							ite.addOutput(sv);
						}
			
						ite.setCond(cond);
						ite.setBodyTrue(b);
			
						elementList.addAll(b);
						iterElement = elementList.iterator();
						while (iterElement.hasNext()) 
						{
							element = iterElement.next();
							// Add all the input/output/internal variables of elements inside the body
							// to the if/then/else element
							if (element instanceof Operation) 
							{
								/* Add internal and output variables to the "if" */	
								if(!((Operation)element).getOutput().isEmpty()) 
								{
									var = ((Operation)element).getOutputAt(0);
									if(!ite.getInternalVars().contains(var))
										ite.addInternalVar(var);
									if(!ite.getOutput().contains(var))
										ite.addOutput(var);
									list1.put(treat.getTrueName(var.getName()), var);
								}
								
								/* Add input variables to the "if" */
			for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
			var = ((Operation)element).getInputAt(i);									
			if(var instanceof Operation)	
			elementList.add(var);
			else
			if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
			ite.addInput(var);		
			else if(!ite.getInternalVars().contains(var) && ((Variable)var).getType().equals("const"))
			ite.addInternalVar(var);
			}
							} 
							else if (element instanceof GenericOperation) 
							{
								var = ((GenericOperation)element).getOutputAt(0);
								ite.addInternalVar(var);
								list1.put(treat.getTrueName(var.getName()), var);
							}
							else if (element instanceof Function)
							{		
								Element e1;
								for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getInput().elementAt(i);
									if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
										ite.addInput(e1);
								}		
								
								for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getOutput().elementAt(i);
									if(!ite.getInternalVars().contains(e1))
										ite.addInternalVar(e1);
									if(!ite.getOutput().contains(e1))
										ite.addOutput(e1);
									list1.put(treat.getTrueName(e1.getName()), e1);
								}
							}
													
							elementList.remove(elementList.indexOf(element));
							iterElement = elementList.iterator();	
						}
						
						for(Element e: ite.getOutput())
							ite.getInput().remove(e);
			
					
			{
			switch ( LA(1)) {
			case ELSEIF:
			{
				{
				match(ELSEIF);
				
									for (String s : varCpt.keySet()) {
										((ReadWriteIndex)this.varCpt.get(s)).addReadIndex();
									}
								
				{
				tmp=ifthenelsegen();
				}
				
									for (String s : varCpt.keySet()) {
										((ReadWriteIndex)this.varCpt.get(s)).removeReadIndex();
									}
									ite.setBodyFalse(tmp);
											
									elementList.addAll(tmp);
									iterElement = elementList.iterator();
									// Add input, output and internal variables to the structure
									while (iterElement.hasNext()) 
									{
										element = iterElement.next();
										
										// Add all the input/output/internal variables of elements inside the body
										// to the if/then/else element
										if (element instanceof Operation) 
										{
											/* Add internal and output variables to the "if" */	
											if(!((Operation)element).getOutput().isEmpty()) 
											{
												var = ((Operation)element).getOutputAt(0);
												if(!ite.getInternalVars().contains(var))
													ite.addInternalVar(var);
												if(!ite.getOutput().contains(var))
													ite.addOutput(var);
											}
											
											/* Add input variables to the "if" */
				for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
				var = ((Operation)element).getInputAt(i);									
				if(var instanceof Operation)	
				elementList.add(var);
				else
				if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
				ite.addInput(var);							
				}
										} 
										else if (element instanceof GenericOperation) 
										{
											var = ((GenericOperation)element).getOutputAt(0);
											ite.addInternalVar(var);
										}
										else if (element instanceof Function)
										{		
											Element e1;
											for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
											{
												e1 = ((Function)element).getInput().elementAt(i);
												if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
													ite.addInput(e1);
											}		
											
											for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
											{
												e1 = ((Function)element).getOutput().elementAt(i);
												if(!ite.getInternalVars().contains(e1))
													ite.addInternalVar(e1);
												if(!ite.getOutput().contains(e1))
													ite.addOutput(e1);
											}
										}
																
										elementList.remove(elementList.indexOf(element));
										iterElement = elementList.iterator();	
									}
									
									for(Element e: ite.getOutput())
										ite.getInput().remove(e);
									
									next = (IfThenElse)tmp.firstElement();
									for (Element e : next.getInternalVars()) {
										list2.put(treat.getTrueName(e.getName()), e);
									}
									for (Element e : tmp) {
										if (e instanceof Operation) {
											list2.put(treat.getTrueName(((Operation)e).getOutputAt(0).getName()), ((Operation)e).getOutputAt(0));
										}
									}
									// Create multiplexers for all the variables used into the if
									for (String s : list1.keySet()) {
										Multiplexer mux = new Multiplexer();
										mux.setName("mux_"+ite.getName());
										treat.count(mux);
										mux.setSize(2);
										mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
										// If the variable is used in both branches of the if, we have
										// to create a multiplexer with the two input
										if (list2.containsKey(s)) {
											mux.addInput(list2.get(s));
											mux.addInput(list1.get(s));
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
								0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										// If the variable is used only in the first branche of the if,
										// we need to multiplex it with the variable used before the if
										} else if (varCpt.containsKey(s)) {
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
											mux.addInput(var);
											mux.addInput(list1.get(s));
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
								0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										}
									}
									// If the variable is used only in the second branche of the if,
									// we need to multiplex it with the variable used before the if
									for (String s : list2.keySet()) {
										if (!list1.containsKey(s) & varCpt.containsKey(s)) {
											Multiplexer mux = new Multiplexer();
											mux.setName("mux_"+ite.getName());
											treat.count(mux);
											mux.setSize(2);
											mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
											mux.addInput(list2.get(s));
											mux.addInput(var);
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										}
									}					
								
				}
				break;
			}
			case ELSE:
			{
				{
				match(ELSE);
				b=body();
					
									ite.setBodyFalse(b);
											
									elementList.addAll(b);
									iterElement = elementList.iterator();
									while (iterElement.hasNext()) 
									{
										element = iterElement.next();
										
										// Add all the input/output/internal variables of elements inside the body
										// to the if/then/else element
										if (element instanceof Operation) 
										{
											/* Add internal and output variables to the "if" */	
											if(!((Operation)element).getOutput().isEmpty()) 
											{
												var = ((Operation)element).getOutputAt(0);
												if(!ite.getInternalVars().contains(var))
													ite.addInternalVar(var);
												if(!ite.getOutput().contains(var))
													ite.addOutput(var);
												list2.put(treat.getTrueName(var.getName()), var);
											}
											
											/* Add input variables to the "if" */
				for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
				var = ((Operation)element).getInputAt(i);									
				if(var instanceof Operation)	
				elementList.add(var);
				else
				if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
				ite.addInput(var);							
				}
										} 
										else if (element instanceof GenericOperation) 
										{
											var = ((GenericOperation)element).getOutputAt(0);
											ite.addInternalVar(var);
											list2.put(treat.getTrueName(var.getName()), var);
										}
										else if (element instanceof Function)
										{		
										Element e1;
											for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
											{
												e1 = ((Function)element).getInput().elementAt(i);
												if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
													ite.addInput(e1);
											}		
											
											for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
											{
												e1 = ((Function)element).getOutput().elementAt(i);
												if(!ite.getInternalVars().contains(e1))
													ite.addInternalVar(e1);
												if(!ite.getOutput().contains(e1))
													ite.addOutput(e1);
												list2.put(treat.getTrueName(e1.getName()), e1);
											}
										}
																
										elementList.remove(elementList.indexOf(element));
										iterElement = elementList.iterator();	
									}
									
									for(Element e: ite.getOutput())
										ite.getInput().remove(e);
									
									
									// Create multiplexers for all the variables used into the if
									for (String s : list1.keySet()) {
										Multiplexer mux = new Multiplexer();
										mux.setName("mux_"+ite.getName());
										treat.count(mux);
										mux.setSize(2);
										mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
										// If the variable is used in both branches of the if, we have
										// to create a multiplexer with the two input
										if (list2.containsKey(s)) {
											mux.addInput(list2.get(s));
											mux.addInput(list1.get(s));
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
								0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										// If the variable is used only in the first branche of the if,
										// we need to multiplex it with the variable used before the if
										} else if (varCpt.containsKey(s)) {
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
											mux.addInput(var);
											mux.addInput(list1.get(s));
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
								0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										}
									}
									// If the variable is used only in the second branche of the if,
									// we need to multiplex it with the variable used before the if
									for (String s : list2.keySet()) {
										if (!list1.containsKey(s) & varCpt.containsKey(s)) {
											Multiplexer mux = new Multiplexer();
											mux.setName("mux_"+ite.getName());
											treat.count(mux);
											mux.setSize(2);
											mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
											mux.addInput(list2.get(s));
											mux.addInput(var);
											this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
											var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
								0.0, "", false).lastElement();
											mux.addOutput(var);
											vec.add(mux);
										}
									}
								
				}
				break;
			}
			case END:
			case ENDIF:
			{
				
								for (String s : list1.keySet()) {
									Multiplexer mux = new Multiplexer();
									mux.setName("mux_"+ite.getName());
									treat.count(mux);
									mux.setSize(2);
									mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
									
									// If the variable is used before the if, we have to multiplex it with the new value
									if (varCpt.containsKey(s)) {
										var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
										mux.addInput(var);
										mux.addInput(list1.get(s));
										this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
										var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
							0.0, "", false).lastElement();
										mux.addOutput(var);
										vec.add(mux);
									}
								}
							
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_19);
		}
		return vec;
	}
	
/**
 * Define a logical expression
 * logexpr = logterm, [ log_op, logexpr ]
 * First level of the logical expression
 */
	public final Vector<Element>  logexpr() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=logterm();
			{
			switch ( LA(1)) {
			case COR:
			case CAND:
			{
				op=log_op();
				tmp=logexpr();
				
							try {
								vec = treat.createOp(vec,tmp,op);
							} 
							catch (Exception e){
								throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
							}
						
				break;
			}
			case RPAR:
			case FUNCTION:
			case END:
			case IF:
			case STRING:
			case FOR:
			case ENDWHILE:
			case WHILE:
			case PRINTF:
			case ERROR:
			case RETURN:
			case SWITCH:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_20);
		}
		return vec;
	}
	
/**
 * Define a loop for
 * loop_for = loop_for_begin, body_function, ( "end" | "endfor" )
 */
	public final LoopFor  loop_for() throws RecognitionException, TokenStreamException, ParsingException {
		LoopFor loop = null;
		
		
				HashMap<String, ReadWriteIndex> tmpCpt = new HashMap<String, ReadWriteIndex>();
				Vector<Element> vec = null;
				
				/* Use to go through the body */
				Element element;
				Vector<Element> elementList = new Vector<Element>();
				Iterator<Element> iterElement;
				Element tempElement;
			
		
		try {      // for error handling
			loop=loop_for_begin();
			
						if(loop == null)
							throw new ParsingException("Invalid \"loop\" condition");
							
						// Save the values of all index
						for (String var : varCpt.keySet()) 
							tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
						// Increment all index
						for (String var : varCpt.keySet()) {
							if (!var.equals("const")) {
								varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getNextVariable());
								treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex(), 0.0, "", false);
				((ReadWriteIndex)tmpCpt.get(var)).setDiff(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex());
							}
						}
					
			vec=body_function();
			{
			switch ( LA(1)) {
			case END:
			{
				match(END);
				break;
			}
			case ENDFOR:
			{
				match(ENDFOR);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			
						/* Create new temporary variable include in the body  */
						for (String var : varCpt.keySet()) {
							if (!tmpCpt.containsKey(var)) {
								tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
								((ReadWriteIndex)tmpCpt.get(var)).setDiff(-1);
							}
						}		
						
						loop.setBody(vec);					
						this.treat.treeToVector(loop);
						this.treat.updateConditionsFor(tmpCpt, varCpt, loop);
			
						// Check if the variables as been modified during the loop. If not, replace the old variables.
						for (String var : tmpCpt.keySet()) {
							if (var.equals("const")) {
							// If there is no change on the variable
			} else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() == ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
			this.treat.changeVar(loop, (Variable)treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement(), (Variable)treat.createVar(var+"_m2m_"+(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getDiff())).lastElement());
			varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getPreviousVariable());
			
			// If there is change on the variable, we have to create an "if"
			// instruction for choose which one we want to
			// use between the old one and the new recalculated inside
			// the loop. For example, in the case of a = a + 1,
			// in first iteration we have to use the old "a" and after
			// we have to use the new value.
			
			} else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() > ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
								IfThenElse ite = new IfThenElse();
								ite.setName("ifThenElse_"+loop.getName()+"_aff");
								treat.count(ite);
								Operation cond = new LessEqual();
								cond.addOutput(treat.createVar(ite.getName()+"_cond").lastElement());
								cond.addInput(loop.getInternalVars().firstElement());
								cond.addInput(loop.getEnd());
								Vector<Element> tmp = new Vector<Element>();
								tmp.add(cond);
								ite.setCond(tmp);
								for(Element e : cond.getInput()) {
									if(!((SimpleVariable)e).getType().equalsIgnoreCase("const")) {
										ite.addInput(e);
									}
								}
								if(!((SimpleVariable)cond.getOutputAt(0)).getType().equalsIgnoreCase("const")) {
									ite.addInternalVar(cond.getOutputAt(0));
									ite.addOutput(cond.getOutputAt(0));
								}
								Assignment aff = new Assignment();
								treat.count(aff);
								aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
								aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)tmpCpt.get(var)).getReadIndex()).lastElement());
								if(!((Variable) aff.getInput().elementAt(0)).getType().equals("iter"))
									loop.addInput(aff.getInput().elementAt(0));
								tmp = new Vector<Element>();
								tmp.add(aff);
								ite.setBodyTrue(tmp);
								if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
									ite.addInternalVar(aff.getOutputAt(0));
								if(!ite.getOutput().contains(aff.getOutputAt(0)))
									ite.addOutput(aff.getOutputAt(0));
								aff = new Assignment();
								treat.count(aff);
								aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
								aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)varCpt.get(var)).getReadIndex()).lastElement());
								tmp = new Vector<Element>();
								tmp.add(aff);
								ite.setBodyFalse(tmp);
								if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
									ite.addInternalVar(aff.getOutputAt(0));
								if(!ite.getOutput().contains(aff.getOutputAt(0)))
									ite.addOutput(aff.getOutputAt(0));
								loop.getBody().add(0,ite);
								loop.addOutput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement());
							}
						}
						this.treat.findInBody(loop, tmpCpt);
								
						elementList.addAll(loop.getBody());
						iterElement = elementList.iterator();
						// Add all the input, output and internal variables to the loop
						while (iterElement.hasNext()) 
						{
							element = iterElement.next();
							
							if (element instanceof Operation) 
							{					
			for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
			tempElement = ((Operation)element).getInputAt(i);									
			if(tempElement instanceof Operation)	
			elementList.add(tempElement);
			else
			if(!loop.getInput().contains(tempElement) && !((Variable)tempElement).getType().equals("const") && !((Variable)tempElement).getType().equals("iter"))
			loop.addInput(tempElement);					
			}
								
								if(!((Operation)element).getOutput().isEmpty()) 
								{
									tempElement = ((Operation)element).getOutputAt(0);
									if(!loop.getInternalVars().contains(tempElement))
										loop.addInternalVar(tempElement);
									if(!loop.getOutput().contains(tempElement))
										loop.addOutput(tempElement);
								}
							} 
							else if (element instanceof GenericOperation) 
							{
								tempElement = ((GenericOperation)element).getOutputAt(0);
								loop.addInternalVar(tempElement);
							}
							else if (element instanceof Function)
							{				
								Element e1;
								for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getInput().elementAt(i);
									if(!loop.getInput().contains(e1) && !((Variable)e1).getType().equals("const")  && !((Variable)e1).getType().equals("iter"))
										loop.addInput(e1);
								}		
								
								for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getOutput().elementAt(i);
									if(!loop.getInternalVars().contains(e1))
										loop.addInternalVar(e1);
									if(!loop.getOutput().contains(e1))
										loop.addOutput(e1);
								}
							}
													
							elementList.remove(elementList.indexOf(element));
							iterElement = elementList.iterator();	
						}
						
						elementList.addAll(loop.getOutput());
						for(Element e: elementList)
						{
							if(loop.getInput().contains(e)) 
								loop.getInput().remove(e);
							if(((Variable)e).getType().equals("cond") || ((Variable)e).getType().equals("const") )
								loop.getOutput().remove(e);
						}
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return loop;
	}
	
/**
 * Define a loop while
 * loop_while = loop_while_begin, body_function, ( "end" | "endwhile" )
 */
	public final LoopWhile  loop_while() throws RecognitionException, TokenStreamException, ParsingException {
		LoopWhile loop = null;
		
		
				HashMap<String, ReadWriteIndex> tmpCpt = new HashMap<String, ReadWriteIndex>();
				Vector<Element> vec = null;
				
				/* Use to go through the body */
				Element element;
				Vector<Element> elementList = new Vector<Element>();
				Iterator<Element> iterElement;
				Element tempElement;
			
		
		try {      // for error handling
			loop=loop_while_begin();
			
						for (String var : varCpt.keySet()) 
							tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
						
						// Increment all index
						for (String var : varCpt.keySet()) {
							if (!var.equals("const")) {
								varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getNextVariable());
								treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex(), 0.0, "", false);
			((ReadWriteIndex)tmpCpt.get(var)).setDiff(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex());
							}
						}
					
			vec=body_function();
			{
			switch ( LA(1)) {
			case END:
			{
				match(END);
				break;
			}
			case ENDWHILE:
			{
				match(ENDWHILE);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			
						/* Create new temporary variable include in the body  */
						for (String var : varCpt.keySet()) {
							if (!tmpCpt.containsKey(var)) {
								tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
								((ReadWriteIndex)tmpCpt.get(var)).setDiff(-1);
							}
						}		
						
						loop.setBody(vec);
						treat.count(loop);
						this.treat.treeToVector(loop);
						this.treat.updateConditionsWhile(tmpCpt, varCpt, loop);
			
						// Check if the variables as been modified during the loop.
						// If not, replace the old variables.
			
						for (String var : tmpCpt.keySet()) {
							if (var.equals("const")) {
							// If there is no change on the variable
			} else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() == ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
			this.treat.changeVar(loop, (Variable)treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement(), (Variable)treat.createVar(var+"_m2m_"+(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getDiff())).lastElement());
			varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getPreviousVariable());
			
			// If there is change on the variable, we have to create an "if"
			// instruction for choose which one we want to
			// use between the old one and the new recalculated inside
			// the loop. For example, in the case of a = a + 1,
			// in first iteration we have to use the old "a" and after
			// we have to use the new value.
			
			} else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() > ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
								IfThenElse ite = new IfThenElse();
								ite.setName("ifThenElse_"+loop.getName()+"_aff");
								treat.count(ite);
								Vector<Element> tmp = new Vector<Element>();
								if (loop.getCond().lastElement() instanceof Variable) {
			Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
			sv.setType("cond");
									Assignment aff = new Assignment();
									aff.addInput(loop.getCond().lastElement());
									aff.addOutput(sv);
									loop.setCond(new Vector<Element>()); 
									loop.getCond().add(aff);
								} else if(((Operation)loop.getCond().lastElement()).getOutput().isEmpty()) {
			Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
			sv.setType("cond");
									((Operation)loop.getCond().lastElement()).addOutput(sv);      
								}
			ite.setCond(loop.getCond());
			if(!((SimpleVariable)((Operation)loop.getCond().lastElement()).getOutputAt(0)).getType().equalsIgnoreCase("const")) {
			ite.addInternalVar(((Operation)loop.getCond().lastElement()).getOutputAt(0));
			ite.addOutput(((Operation)loop.getCond().lastElement()).getOutputAt(0));
			}					
			for(Element e : loop.getCond())
			for(Element iteVar : ((Operation)e).getInput())
			if(!(((SimpleVariable)iteVar).getType().equalsIgnoreCase("const")) && !ite.getInput().contains(iteVar))
			ite.addInput(iteVar);
			else
							ite.addInternalVar(iteVar);
							
					for(Element e : loop.getCond())
			for(Element iteVar : ((Operation)e).getOutput())
			if(ite.getInput().contains(iteVar)) {
							ite.getInput().remove(iteVar);
							ite.addInternalVar(iteVar);
						  }
							
								Assignment aff = new Assignment();
								treat.count(aff);
								aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
								aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)tmpCpt.get(var)).getReadIndex()).lastElement());
								if(!((Variable) aff.getInput().elementAt(0)).getType().equals("iter"))
									loop.addInput(aff.getInput().elementAt(0));
								tmp = new Vector<Element>();
								tmp.add(aff);
								ite.setBodyTrue(tmp);
								if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
									ite.addInternalVar(aff.getOutputAt(0));
								if(!ite.getOutput().contains(aff.getOutputAt(0)))
									ite.addOutput(aff.getOutputAt(0));
								aff = new Assignment();
								treat.count(aff);
								aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
								aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)varCpt.get(var)).getReadIndex()).lastElement());
								tmp = new Vector<Element>();
								tmp.add(aff);
								ite.setBodyFalse(tmp);
								if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
									ite.addInternalVar(aff.getOutputAt(0));
								if(!ite.getOutput().contains(aff.getOutputAt(0)))
									ite.addOutput(aff.getOutputAt(0));
								loop.getBody().add(0,ite);
								loop.addOutput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement());
							}
						}
						this.treat.findInBody(loop, tmpCpt);
								
						elementList.addAll(loop.getBody());
						iterElement = elementList.iterator();
						// Add all the input, output and internal variables to the loop
						while (iterElement.hasNext()) 
						{
							element = iterElement.next();
							
							if (element instanceof Operation) 
							{					
			for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
			tempElement = ((Operation)element).getInputAt(i);									
			if(tempElement instanceof Operation)	
			elementList.add(tempElement);
			else
			if(!loop.getInput().contains(tempElement) && !((Variable)tempElement).getType().equals("const") && !((Variable)tempElement).getType().equals("iter"))
			loop.addInput(tempElement);					
			}
								
								if(!((Operation)element).getOutput().isEmpty()) 
								{
									tempElement = ((Operation)element).getOutputAt(0);
									if(!loop.getInternalVars().contains(tempElement))
										loop.addInternalVar(tempElement);
									if(!loop.getOutput().contains(tempElement))
										loop.addOutput(tempElement);
								}
							} 
							else if (element instanceof GenericOperation) 
							{
								tempElement = ((GenericOperation)element).getOutputAt(0);
								loop.addInternalVar(tempElement);
							}
							else if (element instanceof Function)
							{				
								Element e1;
								for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getInput().elementAt(i);
									if(!loop.getInput().contains(e1) && !((Variable)e1).getType().equals("const")  && !((Variable)e1).getType().equals("iter"))
										loop.addInput(e1);
								}		
								
								for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
								{
									e1 = ((Function)element).getOutput().elementAt(i);
									if(!loop.getInternalVars().contains(e1))
										loop.addInternalVar(e1);
									if(!loop.getOutput().contains(e1))
										loop.addOutput(e1);
								}
							}
													
							elementList.remove(elementList.indexOf(element));
							iterElement = elementList.iterator();	
						}
						
						elementList.addAll(loop.getOutput());
						for(Element e: elementList)
						{
							if(loop.getInput().contains(e)) 
								loop.getInput().remove(e);
							if(((Variable)e).getType().equals("cond") || ((Variable)e).getType().equals("const") )
								loop.getOutput().remove(e);
						}
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return loop;
	}
	
/**
 * Define a switch-case operation
 * op_switch = "switch", func_var,
 *             { "case", ( func_var | '[', func_var, { ',', func_var } ),
 *               [ ',' ], body },
 *             [ "otherwise", [ ',' ], body ], "end"
 */
	public final Switch  op_switch() throws RecognitionException, TokenStreamException, ParsingException {
		Switch sw = null;
		
		
				Vector<Element> vec = null;
				Vector<Element> var = null;
				Vector<Variable> cond = null;
				Vector<Element> b = null;
			
		
		try {      // for error handling
			match(SWITCH);
			sw = new Switch(); treat.count(sw);
			vec=func_var();
			sw.setSwitchVar((Variable)vec.lastElement());
			{
			_loop91:
			do {
				if ((LA(1)==CASE)) {
					match(CASE);
					{
					switch ( LA(1)) {
					case MINUS:
					case LBRAK:
					case STRING:
					case NUMBER:
					{
						var=func_var();
						
												cond = new Vector<Variable>();
												cond.add((Variable)var.lastElement());
											
						break;
					}
					case LBRAC:
					{
						match(LBRAC);
						var=func_var();
						
												cond = new Vector<Variable>();
												cond.add((Variable)var.lastElement());
											
						{
						_loop89:
						do {
							if ((LA(1)==COMMA)) {
								match(COMMA);
								var=func_var();
								
														cond.add((Variable)var.lastElement());
													
							}
							else {
								break _loop89;
							}
							
						} while (true);
						}
						match(RBRAC);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					switch ( LA(1)) {
					case COMMA:
					{
						match(COMMA);
						break;
					}
					case FUNCTION:
					case END:
					case IF:
					case STRING:
					case FOR:
					case WHILE:
					case PRINTF:
					case ERROR:
					case RETURN:
					case SWITCH:
					case CASE:
					case OTHERW:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					b=body();
					sw.addCase(cond,b);
				}
				else {
					break _loop91;
				}
				
			} while (true);
			}
			{
			switch ( LA(1)) {
			case OTHERW:
			{
				match(OTHERW);
				{
				switch ( LA(1)) {
				case COMMA:
				{
					match(COMMA);
					break;
				}
				case FUNCTION:
				case END:
				case IF:
				case STRING:
				case FOR:
				case WHILE:
				case PRINTF:
				case ERROR:
				case RETURN:
				case SWITCH:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				b=body();
				sw.addCase(null,b);
				break;
			}
			case END:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(END);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return sw;
	}
	
/**
 * Describe all the non-insctruction cmd 
 * not_instruction_cmd = ( ( "printf" | "error" ), '(', ANY_STRING,
 *                         ')', [ ';' ]
 *                       | "return", [ ';' ] )
 * All this instructions isn't needed for generation of vhdl code
 */
	public final void not_instruction_cmd() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			switch ( LA(1)) {
			case PRINTF:
			case ERROR:
			{
				{
				switch ( LA(1)) {
				case PRINTF:
				{
					match(PRINTF);
					break;
				}
				case ERROR:
				{
					match(ERROR);
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				match(LPAR);
				match(ANY_STRING);
				match(RPAR);
				{
				switch ( LA(1)) {
				case SEMI:
				{
					match(SEMI);
					break;
				}
				case FUNCTION:
				case END:
				case ENDFUNCTION:
				case IF:
				case ENDIF:
				case ELSEIF:
				case ELSE:
				case STRING:
				case ENDFOR:
				case FOR:
				case ENDWHILE:
				case WHILE:
				case PRINTF:
				case ERROR:
				case RETURN:
				case SWITCH:
				case CASE:
				case OTHERW:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				break;
			}
			case RETURN:
			{
				match(RETURN);
				{
				switch ( LA(1)) {
				case SEMI:
				{
					match(SEMI);
					break;
				}
				case FUNCTION:
				case END:
				case ENDFUNCTION:
				case IF:
				case ENDIF:
				case ELSEIF:
				case ELSE:
				case STRING:
				case ENDFOR:
				case FOR:
				case ENDWHILE:
				case WHILE:
				case PRINTF:
				case ERROR:
				case RETURN:
				case SWITCH:
				case CASE:
				case OTHERW:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
	}
	
/**
 * Define a logical expression
 * logterm = logfactor, [ cmp_op, logterm ]
 * Second level of the logical expression
 */
	public final Vector<Element>  logterm() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Vector<Element> tmp = null;
				Operation op = null;
			
		
		try {      // for error handling
			vec=logfactor();
			{
			switch ( LA(1)) {
			case CEQUAL:
			case CNOTEQ:
			case CLESS:
			case CGREAT:
			case CLESEQ:
			case CGREEQ:
			{
				op=cmp_op();
				tmp=logterm();
				
							try {
								vec = treat.createOp(vec,tmp,op);
							}
							catch (Exception e){
								throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
							}
						
				break;
			}
			case RPAR:
			case FUNCTION:
			case END:
			case IF:
			case STRING:
			case COR:
			case CAND:
			case FOR:
			case ENDWHILE:
			case WHILE:
			case PRINTF:
			case ERROR:
			case RETURN:
			case SWITCH:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_21);
		}
		return vec;
	}
	
/**
 * All the logical operations
 * log_op = '|' | '&' 
 */
	public final Operation  log_op() throws RecognitionException, TokenStreamException {
		Operation op = null;
		
		
		try {      // for error handling
			switch ( LA(1)) {
			case COR:
			{
				match(COR);
				op = new Or();
				break;
			}
			case CAND:
			{
				match(CAND);
				op = new And();
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_10);
		}
		return op;
	}
	
/**
 * Define a logical expression
 * logfactor = [ '!' ], ( func_var | '(', logexpr, ')' )
 * Third level of the logical expression
 */
	public final Vector<Element>  logfactor() throws RecognitionException, TokenStreamException, ParsingException {
		Vector<Element> vec = null;
		
		
				Not not = null;
			
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case NOT:
			{
				match(NOT);
				not = new Not();
				break;
			}
			case MINUS:
			case LPAR:
			case LBRAK:
			case STRING:
			case NUMBER:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			{
			switch ( LA(1)) {
			case MINUS:
			case LBRAK:
			case STRING:
			case NUMBER:
			{
				vec=func_var();
				break;
			}
			case LPAR:
			{
				match(LPAR);
				vec=logexpr();
				match(RPAR);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			
						if (not != null) {
							vec = treat.createNot(vec, not);
						}
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_22);
		}
		return vec;
	}
	
/**
 * Define the start of a loop for
 * loop_for_begin = "for", ident, '=', func_var, ':', func_var,
 *                  ':', func_var, ':', func_var
 */
	public final LoopFor  loop_for_begin() throws RecognitionException, TokenStreamException, ParsingException {
		LoopFor loop = null;
		
		
				String name = null;
		SimpleVariable iterOut = null;
				Vector<Element> vec = null;
				Vector<Element> tmp = null;
				Operation op = new Addition();
		op.setName("AddIter");
		String var;
			
		
		try {      // for error handling
			match(FOR);
			name=ident();
			match(EQUAL);
			vec=func_var();
			match(COLON);
			
						if(vec == null)
							throw new ParsingException("Invalid \"loop\" condition");
							
						loop = new LoopFor();
						treat.count(loop);
						var = treat.getTrueName(vec.lastElement().getName());
						loop.setStart((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
						// If the loop indice isn't already in the table
						if (varCpt.get(name) == null) {
							varCpt.put(name, new ReadWriteIndex());
						}
						// Create the loop indice variable
						tmp = treat.createVar(name+"_m2m_"+((ReadWriteIndex)this.varCpt.get(name)).getReadIndex(), 0.0, "", false);
						loop.addInternalVar((Variable)tmp.lastElement());
						((Variable)tmp.lastElement()).setType("iter"); 
					
			vec=func_var();
			
						if(vec == null)
							throw new ParsingException("Invalid \"loop\" condition");
							
						// Set the variable to the end position for the case where
						// there isn't another loop indice next.
						// The increment value is automaticly set to 1
						if (vec.size() == 1 && vec.lastElement() instanceof Variable)
						{
							var = treat.getTrueName(vec.lastElement().getName());
							loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
						} 
						else
						{
							this.top.addBody(vec);
							var = treat.getTrueName(((Operation)vec.lastElement()).getOutput().lastElement().getName());
							loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
						}
					
			match(COLON);
			vec=func_var();
			
						if(vec == null)
							throw new ParsingException("Invalid \"loop\" condition");
							
						// If there is a third variable, move the end variable to the
						// increment variable and set the new end variable
						loop.setIncr(loop.getEnd());
						this.top.addBody(vec);
						loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
			
			/* Create the iterOperation */
			iterOut = new SimpleVariable(loop.getInternalVars().firstElement().getName()+"_Add", 0.0, "");
			loop.addInternalVar((Variable)iterOut);
			op.getInput().add(loop.getInternalVars().firstElement());
			op.getInput().add(loop.getIncr());
			op.getOutput().add(iterOut); 
			loop.setIterOperation(op);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_23);
		}
		return loop;
	}
	
/**
 * Define the start of a loop while
 * loop_while_begin = "while", logexpr
 */
	public final LoopWhile  loop_while_begin() throws RecognitionException, TokenStreamException, ParsingException {
		LoopWhile loop = null;
		
		
				Vector<Element> cond = null;
			
		
		try {      // for error handling
			match(WHILE);
			cond=logexpr();
			
						if(cond == null)
							throw new ParsingException("Invalid \"while\" condition");
						loop = new LoopWhile();
						treat.count(loop);
						loop.setCond(cond);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_24);
		}
		return loop;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"EQUAL",
		"SEMI",
		"CEQUAL",
		"CNOTEQ",
		"CLESS",
		"CGREAT",
		"CLESEQ",
		"CGREEQ",
		"PLUS",
		"DPLUS",
		"MINUS",
		"DMINUS",
		"MUL",
		"DMUL",
		"DIV",
		"DDIV",
		"PWR1",
		"PWR2",
		"LPAR",
		"RPAR",
		"COMMA",
		"LBRAK",
		"RBRAK",
		"\"function\"",
		"\"end\"",
		"\"endfunction\"",
		"\"if\"",
		"\"endif\"",
		"\"then\"",
		"\"elseif\"",
		"\"else\"",
		"STRING",
		"NOT",
		"COR",
		"CAND",
		"\"endfor\"",
		"\"for\"",
		"COLON",
		"\"endwhile\"",
		"\"while\"",
		"\"printf\"",
		"\"error\"",
		"ANY_STRING",
		"\"return\"",
		"NUMBER",
		"DOT",
		"\"switch\"",
		"\"case\"",
		"LBRAC",
		"RBRAC",
		"\"otherwise\"",
		"M2M",
		"\"break\"",
		"DIGIT",
		"CHAR",
		"WS",
		"COMMENT",
		"ML_COMMENT"
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 36028797153181696L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 21600519973765122L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 21600519973765120L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { 66639127688249328L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { 16L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { 21600519998930976L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = { 1329345125679104L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = { 20266227045761024L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = { 4948070760448L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = { 281578093690880L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = { 21600519998943264L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = { 21600519998992416L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = { 21600519999189024L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = { 21600519999975456L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = { 30610330597982176L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	private static final long[] mk_tokenSet_16() {
		long[] data = { 8388608L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_16 = new BitSet(mk_tokenSet_16());
	private static final long[] mk_tokenSet_17() {
		long[] data = { 30891805641801696L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_17 = new BitSet(mk_tokenSet_17());
	private static final long[] mk_tokenSet_18() {
		long[] data = { 1329345930985472L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_18 = new BitSet(mk_tokenSet_18());
	private static final long[] mk_tokenSet_19() {
		long[] data = { 2415919104L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_19 = new BitSet(mk_tokenSet_19());
	private static final long[] mk_tokenSet_20() {
		long[] data = { 1333743449014272L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_20 = new BitSet(mk_tokenSet_20());
	private static final long[] mk_tokenSet_21() {
		long[] data = { 1334155765874688L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_21 = new BitSet(mk_tokenSet_21());
	private static final long[] mk_tokenSet_22() {
		long[] data = { 1334155765878720L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_22 = new BitSet(mk_tokenSet_22());
	private static final long[] mk_tokenSet_23() {
		long[] data = { 1329895149928448L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_23 = new BitSet(mk_tokenSet_23());
	private static final long[] mk_tokenSet_24() {
		long[] data = { 1333743440625664L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_24 = new BitSet(mk_tokenSet_24());
	
	}
