header {
package m2m.backend.octaveparser;

import m2m.backend.buildingblocks.BuildingBlocksManager;
import m2m.backend.structure.*;
import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;

}						


class OctaveParser extends Parser;

{
	/**
	 *  Store the instance of the object who wFill treat the stucture
	 */
	StructTreatment treat;
	/**
	 *  Root of the structure
	 */
	Function top;
	/**
	 *  Indicates if the parsing was successful
	 */
	boolean successful;
	 
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 *  Define the object who treat the structure
	 */
	public void setTreat(StructTreatment treat) {
		this.treat = treat;
	}
	/**
	 *  Define the root of the structure
	 */
	public void setTop(Function top) {
		this.top = top;
	}

	public void reportError(RecognitionException e) //throws ParsingException
	{
		System.out.println("Parsing error at line " + e.line);
		successful=false;
	//	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	//	Editor editor = ((Editor)page.getActiveEditor());
	//	editor.setParseDone(false);
	}

	private HashMap<String, ReadWriteIndex> varCpt = new HashMap<String, ReadWriteIndex>();
}

/**
 *  Main entry for the parsing
 *  entry = { pragma } , function
 *  Entry is a list of pragma followed by only one function
 */
entry throws ParsingException
	{
		Function func = null;
		successful=true;
	}
	:	(pragma)* func=function 
	{
		this.treat.setTop(func);
		this.treat.clean(func.getBody(), new Vector<Element>());
	}
	;

/**
 *  Defines one affectation
 *  affect = name , [ tab_ind ] , "=" , expr_lvl1 , ","
 *  The tab_ind rule is used for vector.
 *  expr_lvl1 is the root rule for an expression.
 */
affect returns [Vector<Element> vec = null] throws ParsingException
	{
		String name = null;
		Vector<Element> exp = null;
		Element var = null;
		boolean vector = false;
		Integer ind = null;
	}
	:	name=ident (ind=tab_ind { vector=true; })? EQUAL exp=expr_lvl1 (SEMI)?
		{
			if(exp == null)
				throw new ParsingException("Invalid assignment expression");
		
			if (!this.varCpt.containsKey(name)) {
				this.varCpt.put(name, new ReadWriteIndex());
			} else {
				this.varCpt.put(name, ((ReadWriteIndex)this.varCpt.get(name)).getNextVariable());
			}
			var = treat.createVar(name+"_m2m_"+((ReadWriteIndex)this.varCpt.get(name)).getReadIndex(), 
					0.0, "", false).lastElement();
			// If variable contains an indice, we need to treat it as a
			// VectorVariable and set the pragma for this variable
			if (vector) {
				if(ind == null)
					throw new ParsingException("Invalid vector index");
				var = new VectorVariable((Variable)var, ind);
				treat.addPragma(name, "vector");
			}
			
			vec = treat.createAff((Variable)var, exp);
		}
	;

/**
 * Defines a body
 * body = { instruction }
 */
body returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		vec = new Vector<Element>();
	}
	:	
		{
			for (String s : varCpt.keySet()) {
				((ReadWriteIndex)this.varCpt.get(s)).addReadIndex();
			}
		}
		(tmp=instruction 
			{
				if (tmp != null) {
					vec.addAll(tmp);
				}
			}
		)*
		{
			for (String s : varCpt.keySet()) {
				((ReadWriteIndex)this.varCpt.get(s)).removeReadIndex();
			}
		}
	;
	
body_function returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		vec = new Vector<Element>();
	}
	:	
		(tmp=instruction 
			{
				if (tmp != null) {
					vec.addAll(tmp);
				}
			}
		)*
	;

/**
 * Defines all the comparison operand
 * cmp_op = "==" | "!=" | "<" | ">" | "<=" | ">="
 */
cmp_op returns [Operation op = null]
	:	CEQUAL {op = new Equal();}
	|	CNOTEQ {op = new NotEqual();}
	|	CLESS {op = new Less();}
	|	CGREAT {op = new Greater();}
	|	CLESEQ {op = new LessEqual();}
	|	CGREEQ {op = new GreaterEqual();}
	;

/**
 * Used to describe the first level operation wich contains addition and dot addition
 * expr_lvl1 = expr_lvl2, { ( "+" | ".+" ), expr_lvl2 }
 */
expr_lvl1 returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=expr_lvl2 ((PLUS | DPLUS) tmp=expr_lvl2
		{
			try {
				op = new Addition();
				vec = treat.createOp(vec,tmp,op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)*
	;
	
/**
 * Used to describe the second level operation wich contains subtraction and dot subtraction
 * expr_lvl2 = expr_lvl3, { ( "-" | ".-" ), expr_lvl3 }
 */
expr_lvl2 returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=expr_lvl3 ((MINUS | DMINUS) tmp=expr_lvl3
		{		
			try{
				op = new Subtraction();
				vec = treat.createOp(vec,tmp,op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)*
	;

/**
 * Used to describe the third level operation wich contains multiplication and dot multiplication
 * expr_lvl3 = expr_lvl4, } ( "*" | ".*" ), expr_lvl4 }
 */
expr_lvl3 returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=expr_lvl4 (
			(	MUL {op = new Multiplication();}
			|	DMUL {op = new DotMultiplication();}
			) tmp=expr_lvl4
		{
			
			try {
				vec = treat.createOp(vec,tmp,op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)*
	;

/**
 * Used to describe the fourth level operation wich contains division and dot division
 * expr_lvl4 = expr_lvl5, { ( "/" | "./" ), expr_lvl5 }
 */
expr_lvl4 returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=expr_lvl5 (
			(	DIV {op = new Division();}
			|	DDIV {op = new DotDivision();}
			) tmp=expr_lvl5
		{
			try{
				vec = treat.createOp(vec,tmp,op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)*
	;

/**
 * Used to describe the fifth level operation wich contains power and parenthese
 * expr_lvl5 = ( func_var, { ( "^" | "**" ), func_var } | "(", expr_lvl1, ")" )
 */
expr_lvl5 returns [Vector<Element> vec = null] throws ParsingException
	{
		Operation op = null;
		Vector<Element> tmp;
	}
	:	vec=func_var ((PWR1 | PWR2) tmp=func_var
		{
			try {
				op = new Power();
				vec = treat.createOp(vec, tmp, op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)*
	|	LPAR vec=expr_lvl1 RPAR
	;

/**
 * Rule for the inputs of any function
 * func_param = expr_lvl1, { ",", expr_lvl1 }
 * Returns the list of all input variables
 */
func_param returns [Vector<Element> par = null] throws ParsingException
	{
		Vector<Element> vec = null;
	}
	: 	vec=expr_lvl1
		{
			par = vec;
		}
		(COMMA vec=expr_lvl1
		{
			par.add(vec.lastElement());
		}
		)*
	;

/**
 * Describe a variable or a function call
 * func_var = ( [ "-" ], ( ident, [ "(", func_param, ")" ] | number )
 *            | "[", [ [ "-" ], number, { [ "," ] , [ "-" ], number } ] )
 * Accept all the variables or function call like:
 *   1.0
 *   -2.45
 *   sqrt(10)
 *   [ 10 -4, 2 ]
 */
func_var returns [Vector<Element> vec = null] throws ParsingException
	{
		boolean minus = false;
		boolean param = false;
		String s = null;
		Vector<Element> par = null;
		Element el = null;
		Double val = null;
		SimpleVariable var = null;
	}
	:	(MINUS {minus = true;})? (
			s=ident (LPAR par=func_param RPAR {param = true;})?
			{
				// Unable to call a function wich have the same name that a variable
				if (treat.isVariable(s) && param && par.size() > 1) {
					throw new RecognitionException("error");
				// If the name is a variable
				} else if (treat.isVariable(s)) {
					if (!this.varCpt.containsKey(s)) {
						this.varCpt.put(s, new ReadWriteIndex());
					}
					vec = new Vector<Element>();
					vec = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", minus);
					if (param && par.size() == 1) {
						el = new VectorVariable((Variable)vec.lastElement(), (int)((Variable)par.lastElement()).getVal());
						((Variable)vec.lastElement()).setType("vector");
						vec.remove(vec.lastElement());
						vec.add(el);
					}
				// If there is a function with no minus in front of
				} else if (param && !minus) {
          if(BuildingBlocksManager.getInstance().isABlock(s)) {
            el = new GenericOperation(s);
            el.setName(s+treat.getNextOpCpt());
            ((Operation)el).setInput(par);
            vec = new Vector<Element>();
            vec.add(el);
          }
          else {
            throw new ParsingException("Operation generic \""+s+"\" non definie.");
          }
				} else {
					throw new ParsingException("Variable "+s+" non déclarée au préalable.");
				}
			}
		|	val=number
			{
				// If this is a number, create a normal variable
				if (minus) {
					val = -val;
				}
				if (!this.varCpt.containsKey("const")) {
					this.varCpt.put("const", new ReadWriteIndex());
				} else {
					this.varCpt.put("const", ((ReadWriteIndex)this.varCpt.get("const").getNextVariable()));
				}
				try {
					vec = treat.createVar("m2m_const_"+((ReadWriteIndex)this.varCpt.get("const")).getReadIndex(), val, "const", false);
				} 
				catch (Exception e){
					throw new ParsingException("Invalid character for a constant variable");
				}			
			}
		)
	|	LBRAK 
		{
			// If there is values inside brackets, we need to create a vector
			vec = new Vector<Element>();
			var = new SimpleVariable();
			var.setType("vector");
			vec.add(var);
		}
		((MINUS {minus = true;})? val=number 
		{
			if (minus)
				val = -val;
			var.addVal(val);
			minus = false;
		}
		( (COMMA)? (MINUS {minus = true;})? val=number
		{
			if (minus)
					val = -val;
			var.addVal(val);
			minus = false;
		}
		)*)? RBRAK
	;


/**
 * Define a function
 * function = function_begin, body, function_end
 * At the moment, can only be used tu define the main function
 */
function returns [Function func = null] throws ParsingException
	{
		Vector<Element> b = null;
		Function tmpTop = this.top;
	}
	:	func=function_begin b=body function_end
		{
			if(func == null)
				throw new ParsingException("Invalid function");
				
			func.addBody(b);
			// Create all affectations for the input and output variables
			this.treat.createInOutAff(func, this.varCpt);
			this.top = tmpTop;
			this.treat.setTop(this.top);
		}
	;

/**
 * Describe the begin of a function
 * function_begin = "function", ( '[', ident, { ',', ident }, ']' | ident ),
 *                  "=", ident, '(', param, ')'
 */
function_begin returns [Function func = null] throws ParsingException
	{
		String name,out = null;
		Vector<Element> par = null;
	}
	:	FUNCTION {func = new Function(); this.top = func; this.treat.setTop(this.top);} (
			LBRAK out=ident
			{
				Variable var = (Variable)treat.createInOut(out);
				//this.top.addOutput(var);
				func.addOutput(var);
			}
			( COMMA out=ident
				{
					var = (Variable)treat.createInOut(out);
					func.addOutput(var);
				}
			)* RBRAK
			| out=ident
			{
				Variable var = (Variable)treat.createInOut(out);
				func.addOutput(var);
			}
			) EQUAL name=ident LPAR par=param RPAR
		{
			func.setName(name);
			func.setInput(par);
			for (Element var : par) {
				if (var instanceof Variable) {
					this.varCpt.put(treat.getTrueName(var.getName()), new ReadWriteIndex());
				}
			}
		}
	;

/**
 * Describe the end of a function
 * function_end = ( "end", "function", ident, ';' | "endfunction", [ ';' ] )
 */
function_end
	:	END FUNCTION SEMI
	|	ENDFUNCTION (SEMI)?
	;

/**
 * Define the main part for the if-then-else instruction
 * ifthenelse = "if", ifthenelsegen, ( "end" | "endif" )
 */
ifthenelse returns [Vector<Element> vec = null] throws ParsingException
	:	IF vec=ifthenelsegen
		(END | ENDIF)
	;

/**
 * Define the second part of the if-then-else instruction.
 * ifthenelsegen = '(', logexpr ')', [ "then" ], body, [ "elseif", ifthenelsegen | "else", body ]  
 */
ifthenelsegen returns [Vector<Element> vec = null] throws ParsingException
	{
		IfThenElse ite = null;
		Vector<Element> cond = null;
		Vector<Element> tmp = null;
		IfThenElse next = null;
		Element var;
		Vector<Element> b = null;
		HashMap<String, Element> list1 = new HashMap<String, Element>();
		HashMap<String, Element> list2 = new HashMap<String, Element>();
		
		/* Use to go through the body */
		Element element;
		Vector<Element> elementList = new Vector<Element>();
		Iterator<Element> iterElement;
	}
	:	LPAR cond=logexpr RPAR (THEN)? b=body
		{
			// Simple check to avoid errors on condition
			if(cond == null)
				throw new ParsingException("Invalid \"if\" condition");
		
			vec = new Vector<Element>();
			ite = new IfThenElse();
			vec.add(ite);
			treat.count(ite);
			// Create the condition output variable
			Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
			sv.setType("cond");
			// Assign the right element to the condition variable
			if (cond.lastElement() instanceof Variable) {
				Assignment aff = new Assignment();
				aff.addInput(cond.lastElement());
				aff.addOutput(sv);
				cond = new Vector<Element>();
				cond.add(aff);
			} else {
				((Operation)cond.lastElement()).addOutput(sv);
        
        /* Add condition input */
        elementList.addAll(((Operation)cond.lastElement()).getInput());
        iterElement = elementList.iterator();
        while (iterElement.hasNext()) 
        {
          element = iterElement.next();
          
          if(element instanceof Variable && !ite.getInput().contains(element) && !((Variable)element).getType().equals("const"))
            ite.addInput(element);
          else if(element instanceof Variable && !ite.getInternalVars().contains(element) && ((Variable)element).getType().equals("const"))
            ite.addInternalVar(element);
          else if(element instanceof Operation)
            elementList.addAll(((Operation)element).getInput());
            
          elementList.remove(elementList.indexOf(element));
          iterElement = elementList.iterator();	
        }	
			}
			// Add the condition variable if there isn't on temp or output
			if (!ite.getInternalVars().contains(sv)) {
				ite.addInternalVar(sv);
			}
			if (!ite.getOutput().contains(sv)) {
				ite.addOutput(sv);
			}

			ite.setCond(cond);
			ite.setBodyTrue(b);

			elementList.addAll(b);
			iterElement = elementList.iterator();
			while (iterElement.hasNext()) 
			{
				element = iterElement.next();
				// Add all the input/output/internal variables of elements inside the body
				// to the if/then/else element
				if (element instanceof Operation) 
				{
					/* Add internal and output variables to the "if" */	
					if(!((Operation)element).getOutput().isEmpty()) 
					{
						var = ((Operation)element).getOutputAt(0);
						if(!ite.getInternalVars().contains(var))
							ite.addInternalVar(var);
						if(!ite.getOutput().contains(var))
							ite.addOutput(var);
						list1.put(treat.getTrueName(var.getName()), var);
					}
					
					/* Add input variables to the "if" */
          for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
            var = ((Operation)element).getInputAt(i);									
            if(var instanceof Operation)	
              elementList.add(var);
            else
              if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
                ite.addInput(var);		
              else if(!ite.getInternalVars().contains(var) && ((Variable)var).getType().equals("const"))
                ite.addInternalVar(var);
          }
				} 
				else if (element instanceof GenericOperation) 
				{
					var = ((GenericOperation)element).getOutputAt(0);
					ite.addInternalVar(var);
					list1.put(treat.getTrueName(var.getName()), var);
				}
				else if (element instanceof Function)
				{		
					Element e1;
					for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getInput().elementAt(i);
						if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
							ite.addInput(e1);
					}		
					
					for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getOutput().elementAt(i);
						if(!ite.getInternalVars().contains(e1))
							ite.addInternalVar(e1);
						if(!ite.getOutput().contains(e1))
							ite.addOutput(e1);
						list1.put(treat.getTrueName(e1.getName()), e1);
					}
				}
										
				elementList.remove(elementList.indexOf(element));
				iterElement = elementList.iterator();	
			}
			
			for(Element e: ite.getOutput())
				ite.getInput().remove(e);

		}
		(	(ELSEIF 
				{
					for (String s : varCpt.keySet()) {
						((ReadWriteIndex)this.varCpt.get(s)).addReadIndex();
					}
				}
				(tmp=ifthenelsegen)
				{
					for (String s : varCpt.keySet()) {
						((ReadWriteIndex)this.varCpt.get(s)).removeReadIndex();
					}
					ite.setBodyFalse(tmp);
							
					elementList.addAll(tmp);
					iterElement = elementList.iterator();
					// Add input, output and internal variables to the structure
					while (iterElement.hasNext()) 
					{
						element = iterElement.next();
						
						// Add all the input/output/internal variables of elements inside the body
						// to the if/then/else element
						if (element instanceof Operation) 
						{
							/* Add internal and output variables to the "if" */	
							if(!((Operation)element).getOutput().isEmpty()) 
							{
								var = ((Operation)element).getOutputAt(0);
								if(!ite.getInternalVars().contains(var))
									ite.addInternalVar(var);
								if(!ite.getOutput().contains(var))
									ite.addOutput(var);
							}
							
							/* Add input variables to the "if" */
              for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
                var = ((Operation)element).getInputAt(i);									
                if(var instanceof Operation)	
                  elementList.add(var);
                else
                  if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
                    ite.addInput(var);							
              }
						} 
						else if (element instanceof GenericOperation) 
						{
							var = ((GenericOperation)element).getOutputAt(0);
							ite.addInternalVar(var);
						}
						else if (element instanceof Function)
						{		
							Element e1;
							for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
							{
								e1 = ((Function)element).getInput().elementAt(i);
								if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
									ite.addInput(e1);
							}		
							
							for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
							{
								e1 = ((Function)element).getOutput().elementAt(i);
								if(!ite.getInternalVars().contains(e1))
									ite.addInternalVar(e1);
								if(!ite.getOutput().contains(e1))
									ite.addOutput(e1);
							}
						}
												
						elementList.remove(elementList.indexOf(element));
						iterElement = elementList.iterator();	
					}
					
					for(Element e: ite.getOutput())
						ite.getInput().remove(e);
					
					next = (IfThenElse)tmp.firstElement();
					for (Element e : next.getInternalVars()) {
						list2.put(treat.getTrueName(e.getName()), e);
					}
					for (Element e : tmp) {
						if (e instanceof Operation) {
							list2.put(treat.getTrueName(((Operation)e).getOutputAt(0).getName()), ((Operation)e).getOutputAt(0));
						}
					}
					// Create multiplexers for all the variables used into the if
					for (String s : list1.keySet()) {
						Multiplexer mux = new Multiplexer();
						mux.setName("mux_"+ite.getName());
						treat.count(mux);
						mux.setSize(2);
						mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
						// If the variable is used in both branches of the if, we have
						// to create a multiplexer with the two input
						if (list2.containsKey(s)) {
							mux.addInput(list2.get(s));
							mux.addInput(list1.get(s));
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
				0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						// If the variable is used only in the first branche of the if,
						// we need to multiplex it with the variable used before the if
						} else if (varCpt.containsKey(s)) {
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
							mux.addInput(var);
							mux.addInput(list1.get(s));
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
				0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						}
					}
					// If the variable is used only in the second branche of the if,
					// we need to multiplex it with the variable used before the if
					for (String s : list2.keySet()) {
						if (!list1.containsKey(s) & varCpt.containsKey(s)) {
							Multiplexer mux = new Multiplexer();
							mux.setName("mux_"+ite.getName());
							treat.count(mux);
							mux.setSize(2);
							mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
							mux.addInput(list2.get(s));
							mux.addInput(var);
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						}
					}					
				}
			)
		|	(ELSE b=body
				{	
					ite.setBodyFalse(b);
							
					elementList.addAll(b);
					iterElement = elementList.iterator();
					while (iterElement.hasNext()) 
					{
						element = iterElement.next();
						
						// Add all the input/output/internal variables of elements inside the body
						// to the if/then/else element
						if (element instanceof Operation) 
						{
							/* Add internal and output variables to the "if" */	
							if(!((Operation)element).getOutput().isEmpty()) 
							{
								var = ((Operation)element).getOutputAt(0);
								if(!ite.getInternalVars().contains(var))
									ite.addInternalVar(var);
								if(!ite.getOutput().contains(var))
									ite.addOutput(var);
								list2.put(treat.getTrueName(var.getName()), var);
							}
							
							/* Add input variables to the "if" */
              for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
                var = ((Operation)element).getInputAt(i);									
                if(var instanceof Operation)	
                  elementList.add(var);
                else
                  if(!ite.getInput().contains(var) && !((Variable)var).getType().equals("const"))
                    ite.addInput(var);							
              }
						} 
						else if (element instanceof GenericOperation) 
						{
							var = ((GenericOperation)element).getOutputAt(0);
							ite.addInternalVar(var);
							list2.put(treat.getTrueName(var.getName()), var);
						}
						else if (element instanceof Function)
						{		
						Element e1;
							for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
							{
								e1 = ((Function)element).getInput().elementAt(i);
								if(!ite.getInput().contains(e1) && !((Variable)e1).getType().equals("const"))
									ite.addInput(e1);
							}		
							
							for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
							{
								e1 = ((Function)element).getOutput().elementAt(i);
								if(!ite.getInternalVars().contains(e1))
									ite.addInternalVar(e1);
								if(!ite.getOutput().contains(e1))
									ite.addOutput(e1);
								list2.put(treat.getTrueName(e1.getName()), e1);
							}
						}
												
						elementList.remove(elementList.indexOf(element));
						iterElement = elementList.iterator();	
					}
					
					for(Element e: ite.getOutput())
						ite.getInput().remove(e);
					
					
					// Create multiplexers for all the variables used into the if
					for (String s : list1.keySet()) {
						Multiplexer mux = new Multiplexer();
						mux.setName("mux_"+ite.getName());
						treat.count(mux);
						mux.setSize(2);
						mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
						// If the variable is used in both branches of the if, we have
						// to create a multiplexer with the two input
						if (list2.containsKey(s)) {
							mux.addInput(list2.get(s));
							mux.addInput(list1.get(s));
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
				0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						// If the variable is used only in the first branche of the if,
						// we need to multiplex it with the variable used before the if
						} else if (varCpt.containsKey(s)) {
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
							mux.addInput(var);
							mux.addInput(list1.get(s));
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
				0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						}
					}
					// If the variable is used only in the second branche of the if,
					// we need to multiplex it with the variable used before the if
					for (String s : list2.keySet()) {
						if (!list1.containsKey(s) & varCpt.containsKey(s)) {
							Multiplexer mux = new Multiplexer();
							mux.setName("mux_"+ite.getName());
							treat.count(mux);
							mux.setSize(2);
							mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
							mux.addInput(list2.get(s));
							mux.addInput(var);
							this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
							var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
				0.0, "", false).lastElement();
							mux.addOutput(var);
							vec.add(mux);
						}
					}
				}
			)
		|
			// Case when there isn't else instruction
			{
				for (String s : list1.keySet()) {
					Multiplexer mux = new Multiplexer();
					mux.setName("mux_"+ite.getName());
					treat.count(mux);
					mux.setSize(2);
					mux.setSel((Variable)((Operation)ite.getCond().lastElement()).getOutputAt(0));
					
					// If the variable is used before the if, we have to multiplex it with the new value
					if (varCpt.containsKey(s)) {
						var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(), 0.0, "", false).lastElement();
						mux.addInput(var);
						mux.addInput(list1.get(s));
						this.varCpt.put(s, ((ReadWriteIndex)this.varCpt.get(s)).getNextVariable());
						var = treat.createVar(s+"_m2m_"+((ReadWriteIndex)this.varCpt.get(s)).getReadIndex(),
			0.0, "", false).lastElement();
						mux.addOutput(var);
						vec.add(mux);
					}
				}
			}
			
		)
	;

/**
 * Define an ident wich is a string
 */
ident returns [String i=new String()]
	:	s:STRING {i=s.getText();}
	;

/**
 * Describe all the instructions
 * instruction = affect | function | loop_for | loop_while 
 *               | op_switch | ifthenelse | not_instruction_cmd
 */
instruction returns [Vector<Element> vec = null] throws ParsingException
	{
		Element el = null;
	}
	:	vec=affect
	|	(	el=function
		|	el=loop_for
		|	el=loop_while
		|	el=op_switch
		)
		{
			vec = new Vector<Element>();
			vec.add(el);
		}
	|	vec=ifthenelse
	
	|	not_instruction_cmd
	;

/**
 * Define a logical expression
 * logexpr = logterm, [ log_op, logexpr ]
 * First level of the logical expression
 */
logexpr returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=logterm (op=log_op tmp=logexpr
		{
			try {
				vec = treat.createOp(vec,tmp,op);
			} 
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)?
	;

/**
 * Define a logical expression
 * logterm = logfactor, [ cmp_op, logterm ]
 * Second level of the logical expression
 */
logterm returns [Vector<Element> vec = null] throws ParsingException
	{
		Vector<Element> tmp = null;
		Operation op = null;
	}
	:	vec=logfactor (op=cmp_op tmp=logterm
		{
			try {
				vec = treat.createOp(vec,tmp,op);
			}
			catch (Exception e){
				throw new ParsingException("Incomplete operation \"" + op.getOpSymbol() + "\"");
			}
		}
		)?
	;

/**
 * Define a logical expression
 * logfactor = [ '!' ], ( func_var | '(', logexpr, ')' )
 * Third level of the logical expression
 */
logfactor returns [Vector<Element> vec = null] throws ParsingException
	{
		Not not = null;
	}
	:	(NOT {not = new Not();})?
		(	vec=func_var
		|	LPAR vec=logexpr RPAR
		)
		{
			if (not != null) {
				vec = treat.createNot(vec, not);
			}
		}
	;

/**
 * All the logical operations
 * log_op = '|' | '&' 
 */
log_op returns [Operation op = null]
	:	COR {op = new Or();}
	|	CAND {op = new And();}
	;

/**
 * Define a loop for
 * loop_for = loop_for_begin, body_function, ( "end" | "endfor" )
 */
loop_for returns [LoopFor loop = null] throws ParsingException
	{
		HashMap<String, ReadWriteIndex> tmpCpt = new HashMap<String, ReadWriteIndex>();
		Vector<Element> vec = null;
		
		/* Use to go through the body */
		Element element;
		Vector<Element> elementList = new Vector<Element>();
		Iterator<Element> iterElement;
		Element tempElement;
	}
	:	loop=loop_for_begin 
		{
			if(loop == null)
				throw new ParsingException("Invalid \"loop\" condition");
				
			// Save the values of all index
			for (String var : varCpt.keySet()) 
				tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
			// Increment all index
			for (String var : varCpt.keySet()) {
				if (!var.equals("const")) {
					varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getNextVariable());
					treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex(), 0.0, "", false);
         	((ReadWriteIndex)tmpCpt.get(var)).setDiff(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex());
				}
			}
		}
		vec=body_function (END | ENDFOR)
		{
			/* Create new temporary variable include in the body  */
			for (String var : varCpt.keySet()) {
				if (!tmpCpt.containsKey(var)) {
					tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
					((ReadWriteIndex)tmpCpt.get(var)).setDiff(-1);
				}
			}		
			
			loop.setBody(vec);					
			this.treat.treeToVector(loop);
			this.treat.updateConditionsFor(tmpCpt, varCpt, loop);

			// Check if the variables as been modified during the loop. If not, replace the old variables.
			for (String var : tmpCpt.keySet()) {
				if (var.equals("const")) {
				// If there is no change on the variable
        } else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() == ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
          this.treat.changeVar(loop, (Variable)treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement(), (Variable)treat.createVar(var+"_m2m_"+(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getDiff())).lastElement());
          varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getPreviousVariable());

        // If there is change on the variable, we have to create an "if"
        // instruction for choose which one we want to
        // use between the old one and the new recalculated inside
        // the loop. For example, in the case of a = a + 1,
        // in first iteration we have to use the old "a" and after
        // we have to use the new value.

        } else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() > ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
					IfThenElse ite = new IfThenElse();
					ite.setName("ifThenElse_"+loop.getName()+"_aff");
					treat.count(ite);
					Operation cond = new LessEqual();
					cond.addOutput(treat.createVar(ite.getName()+"_cond").lastElement());
					cond.addInput(loop.getInternalVars().firstElement());
					cond.addInput(loop.getEnd());
					Vector<Element> tmp = new Vector<Element>();
					tmp.add(cond);
					ite.setCond(tmp);
					for(Element e : cond.getInput()) {
						if(!((SimpleVariable)e).getType().equalsIgnoreCase("const")) {
							ite.addInput(e);
						}
					}
					if(!((SimpleVariable)cond.getOutputAt(0)).getType().equalsIgnoreCase("const")) {
						ite.addInternalVar(cond.getOutputAt(0));
						ite.addOutput(cond.getOutputAt(0));
					}
					Assignment aff = new Assignment();
					treat.count(aff);
					aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
					aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)tmpCpt.get(var)).getReadIndex()).lastElement());
					if(!((Variable) aff.getInput().elementAt(0)).getType().equals("iter"))
						loop.addInput(aff.getInput().elementAt(0));
					tmp = new Vector<Element>();
					tmp.add(aff);
					ite.setBodyTrue(tmp);
					if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
						ite.addInternalVar(aff.getOutputAt(0));
					if(!ite.getOutput().contains(aff.getOutputAt(0)))
						ite.addOutput(aff.getOutputAt(0));
					aff = new Assignment();
					treat.count(aff);
					aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
					aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)varCpt.get(var)).getReadIndex()).lastElement());
					tmp = new Vector<Element>();
					tmp.add(aff);
					ite.setBodyFalse(tmp);
					if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
						ite.addInternalVar(aff.getOutputAt(0));
					if(!ite.getOutput().contains(aff.getOutputAt(0)))
						ite.addOutput(aff.getOutputAt(0));
					loop.getBody().add(0,ite);
					loop.addOutput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement());
				}
			}
			this.treat.findInBody(loop, tmpCpt);
					
			elementList.addAll(loop.getBody());
			iterElement = elementList.iterator();
			// Add all the input, output and internal variables to the loop
			while (iterElement.hasNext()) 
			{
				element = iterElement.next();
				
				if (element instanceof Operation) 
				{					
          for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
            tempElement = ((Operation)element).getInputAt(i);									
            if(tempElement instanceof Operation)	
              elementList.add(tempElement);
            else
              if(!loop.getInput().contains(tempElement) && !((Variable)tempElement).getType().equals("const") && !((Variable)tempElement).getType().equals("iter"))
                loop.addInput(tempElement);					
          }
					
					if(!((Operation)element).getOutput().isEmpty()) 
					{
						tempElement = ((Operation)element).getOutputAt(0);
						if(!loop.getInternalVars().contains(tempElement))
							loop.addInternalVar(tempElement);
						if(!loop.getOutput().contains(tempElement))
							loop.addOutput(tempElement);
					}
				} 
				else if (element instanceof GenericOperation) 
				{
					tempElement = ((GenericOperation)element).getOutputAt(0);
					loop.addInternalVar(tempElement);
				}
				else if (element instanceof Function)
				{				
					Element e1;
					for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getInput().elementAt(i);
						if(!loop.getInput().contains(e1) && !((Variable)e1).getType().equals("const")  && !((Variable)e1).getType().equals("iter"))
							loop.addInput(e1);
					}		
					
					for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getOutput().elementAt(i);
						if(!loop.getInternalVars().contains(e1))
							loop.addInternalVar(e1);
						if(!loop.getOutput().contains(e1))
							loop.addOutput(e1);
					}
				}
										
				elementList.remove(elementList.indexOf(element));
				iterElement = elementList.iterator();	
			}
			
			elementList.addAll(loop.getOutput());
			for(Element e: elementList)
			{
				if(loop.getInput().contains(e)) 
					loop.getInput().remove(e);
				if(((Variable)e).getType().equals("cond") || ((Variable)e).getType().equals("const") )
					loop.getOutput().remove(e);
			}
		}
	;

/**
 * Define the start of a loop for
 * loop_for_begin = "for", ident, '=', func_var, ':', func_var,
 *                  ':', func_var, ':', func_var
 */
loop_for_begin returns [LoopFor loop = null] throws ParsingException
	{
		String name = null;
    SimpleVariable iterOut = null;
		Vector<Element> vec = null;
		Vector<Element> tmp = null;
		Operation op = new Addition();
    op.setName("AddIter");
    String var;
	}
	:	FOR name=ident EQUAL vec=func_var COLON
		{
			if(vec == null)
				throw new ParsingException("Invalid \"loop\" condition");
				
			loop = new LoopFor();
			treat.count(loop);
			var = treat.getTrueName(vec.lastElement().getName());
			loop.setStart((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
			// If the loop indice isn't already in the table
			if (varCpt.get(name) == null) {
				varCpt.put(name, new ReadWriteIndex());
			}
			// Create the loop indice variable
			tmp = treat.createVar(name+"_m2m_"+((ReadWriteIndex)this.varCpt.get(name)).getReadIndex(), 0.0, "", false);
			loop.addInternalVar((Variable)tmp.lastElement());
			((Variable)tmp.lastElement()).setType("iter"); 
		}
		vec=func_var
		{
			if(vec == null)
				throw new ParsingException("Invalid \"loop\" condition");
				
			// Set the variable to the end position for the case where
			// there isn't another loop indice next.
			// The increment value is automaticly set to 1
			if (vec.size() == 1 && vec.lastElement() instanceof Variable)
			{
				var = treat.getTrueName(vec.lastElement().getName());
				loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
			} 
			else
			{
				this.top.addBody(vec);
				var = treat.getTrueName(((Operation)vec.lastElement()).getOutput().lastElement().getName());
				loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
			}
		}
		COLON vec=func_var
		{
			if(vec == null)
				throw new ParsingException("Invalid \"loop\" condition");
				
			// If there is a third variable, move the end variable to the
			// increment variable and set the new end variable
			loop.setIncr(loop.getEnd());
			this.top.addBody(vec);
			loop.setEnd((SimpleVariable)this.treat.createVar((Variable)vec.lastElement()).lastElement());
      
      /* Create the iterOperation */
      iterOut = new SimpleVariable(loop.getInternalVars().firstElement().getName()+"_Add", 0.0, "");
      loop.addInternalVar((Variable)iterOut);
      op.getInput().add(loop.getInternalVars().firstElement());
      op.getInput().add(loop.getIncr());
      op.getOutput().add(iterOut); 
      loop.setIterOperation(op);
		}
	;

/**
 * Define a loop while
 * loop_while = loop_while_begin, body_function, ( "end" | "endwhile" )
 */
loop_while returns [LoopWhile loop = null] throws ParsingException
	{
		HashMap<String, ReadWriteIndex> tmpCpt = new HashMap<String, ReadWriteIndex>();
		Vector<Element> vec = null;
		
		/* Use to go through the body */
		Element element;
		Vector<Element> elementList = new Vector<Element>();
		Iterator<Element> iterElement;
		Element tempElement;
	}
	:	loop=loop_while_begin 
		{
			for (String var : varCpt.keySet()) 
				tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
			
			// Increment all index
			for (String var : varCpt.keySet()) {
				if (!var.equals("const")) {
					varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getNextVariable());
					treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex(), 0.0, "", false);
          ((ReadWriteIndex)tmpCpt.get(var)).setDiff(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex());
				}
			}
		}
		
		vec=body_function (END | ENDWHILE)	
		{
			/* Create new temporary variable include in the body  */
			for (String var : varCpt.keySet()) {
				if (!tmpCpt.containsKey(var)) {
					tmpCpt.put(var, new ReadWriteIndex(varCpt.get(var)));
					((ReadWriteIndex)tmpCpt.get(var)).setDiff(-1);
				}
			}		
			
			loop.setBody(vec);
			treat.count(loop);
			this.treat.treeToVector(loop);
			this.treat.updateConditionsWhile(tmpCpt, varCpt, loop);

			// Check if the variables as been modified during the loop.
			// If not, replace the old variables.

			for (String var : tmpCpt.keySet()) {
				if (var.equals("const")) {
				// If there is no change on the variable
      } else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() == ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
          this.treat.changeVar(loop, (Variable)treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement(), (Variable)treat.createVar(var+"_m2m_"+(((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getDiff())).lastElement());
          varCpt.put(var, ((ReadWriteIndex)this.varCpt.get(var)).getPreviousVariable());

        // If there is change on the variable, we have to create an "if"
        // instruction for choose which one we want to
        // use between the old one and the new recalculated inside
        // the loop. For example, in the case of a = a + 1,
        // in first iteration we have to use the old "a" and after
        // we have to use the new value.

        } else if (((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()-((ReadWriteIndex)tmpCpt.get(var)).getReadIndex() > ((ReadWriteIndex)tmpCpt.get(var)).getDiff()) {
					IfThenElse ite = new IfThenElse();
					ite.setName("ifThenElse_"+loop.getName()+"_aff");
					treat.count(ite);
					Vector<Element> tmp = new Vector<Element>();
					if (loop.getCond().lastElement() instanceof Variable) {
            Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
            sv.setType("cond");
						Assignment aff = new Assignment();
						aff.addInput(loop.getCond().lastElement());
						aff.addOutput(sv);
						loop.setCond(new Vector<Element>()); 
						loop.getCond().add(aff);
					} else if(((Operation)loop.getCond().lastElement()).getOutput().isEmpty()) {
            Variable sv = (Variable) treat.createVar(ite.getName()+"_cond").lastElement();
            sv.setType("cond");
						((Operation)loop.getCond().lastElement()).addOutput(sv);      
					}
          ite.setCond(loop.getCond());
          if(!((SimpleVariable)((Operation)loop.getCond().lastElement()).getOutputAt(0)).getType().equalsIgnoreCase("const")) {
            ite.addInternalVar(((Operation)loop.getCond().lastElement()).getOutputAt(0));
            ite.addOutput(((Operation)loop.getCond().lastElement()).getOutputAt(0));
          }					
          for(Element e : loop.getCond())
            for(Element iteVar : ((Operation)e).getInput())
              if(!(((SimpleVariable)iteVar).getType().equalsIgnoreCase("const")) && !ite.getInput().contains(iteVar))
                ite.addInput(iteVar);
              else
				ite.addInternalVar(iteVar);
				
		for(Element e : loop.getCond())
            for(Element iteVar : ((Operation)e).getOutput())
              if(ite.getInput().contains(iteVar)) {
				ite.getInput().remove(iteVar);
				ite.addInternalVar(iteVar);
			  }
				
					Assignment aff = new Assignment();
					treat.count(aff);
					aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
					aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)tmpCpt.get(var)).getReadIndex()).lastElement());
					if(!((Variable) aff.getInput().elementAt(0)).getType().equals("iter"))
						loop.addInput(aff.getInput().elementAt(0));
					tmp = new Vector<Element>();
					tmp.add(aff);
					ite.setBodyTrue(tmp);
					if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
						ite.addInternalVar(aff.getOutputAt(0));
					if(!ite.getOutput().contains(aff.getOutputAt(0)))
						ite.addOutput(aff.getOutputAt(0));
					aff = new Assignment();
					treat.count(aff);
					aff.addOutput(treat.createVar(var+"_m2m_"+(((ReadWriteIndex)tmpCpt.get(var)).getWriteIndex())).lastElement());
					aff.addInput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)varCpt.get(var)).getReadIndex()).lastElement());
					tmp = new Vector<Element>();
					tmp.add(aff);
					ite.setBodyFalse(tmp);
					if(!ite.getInternalVars().contains(aff.getOutputAt(0)))
						ite.addInternalVar(aff.getOutputAt(0));
					if(!ite.getOutput().contains(aff.getOutputAt(0)))
						ite.addOutput(aff.getOutputAt(0));
					loop.getBody().add(0,ite);
					loop.addOutput(treat.createVar(var+"_m2m_"+((ReadWriteIndex)this.varCpt.get(var)).getReadIndex()).lastElement());
				}
			}
			this.treat.findInBody(loop, tmpCpt);
					
			elementList.addAll(loop.getBody());
			iterElement = elementList.iterator();
			// Add all the input, output and internal variables to the loop
			while (iterElement.hasNext()) 
			{
				element = iterElement.next();
				
				if (element instanceof Operation) 
				{					
          for(int i = 0; i < ((Operation)element).getInput().size(); i++) {
            tempElement = ((Operation)element).getInputAt(i);									
            if(tempElement instanceof Operation)	
              elementList.add(tempElement);
            else
              if(!loop.getInput().contains(tempElement) && !((Variable)tempElement).getType().equals("const") && !((Variable)tempElement).getType().equals("iter"))
                loop.addInput(tempElement);					
          }
					
					if(!((Operation)element).getOutput().isEmpty()) 
					{
						tempElement = ((Operation)element).getOutputAt(0);
						if(!loop.getInternalVars().contains(tempElement))
							loop.addInternalVar(tempElement);
						if(!loop.getOutput().contains(tempElement))
							loop.addOutput(tempElement);
					}
				} 
				else if (element instanceof GenericOperation) 
				{
					tempElement = ((GenericOperation)element).getOutputAt(0);
					loop.addInternalVar(tempElement);
				}
				else if (element instanceof Function)
				{				
					Element e1;
					for(int i = ((Function)element).getInput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getInput().elementAt(i);
						if(!loop.getInput().contains(e1) && !((Variable)e1).getType().equals("const")  && !((Variable)e1).getType().equals("iter"))
							loop.addInput(e1);
					}		
					
					for(int i = ((Function)element).getOutput().size()-1; i >= 0; i --)
					{
						e1 = ((Function)element).getOutput().elementAt(i);
						if(!loop.getInternalVars().contains(e1))
							loop.addInternalVar(e1);
						if(!loop.getOutput().contains(e1))
							loop.addOutput(e1);
					}
				}
										
				elementList.remove(elementList.indexOf(element));
				iterElement = elementList.iterator();	
			}
			
			elementList.addAll(loop.getOutput());
			for(Element e: elementList)
			{
				if(loop.getInput().contains(e)) 
					loop.getInput().remove(e);
				if(((Variable)e).getType().equals("cond") || ((Variable)e).getType().equals("const") )
					loop.getOutput().remove(e);
			}
		}
	;

/**
 * Define the start of a loop while
 * loop_while_begin = "while", logexpr
 */
loop_while_begin returns [LoopWhile loop = null] throws ParsingException
	{
		Vector<Element> cond = null;
	}
	:	WHILE cond=logexpr
		{
			if(cond == null)
				throw new ParsingException("Invalid \"while\" condition");
			loop = new LoopWhile();
			treat.count(loop);
			loop.setCond(cond);
		}
	;

/**
 * Describe all the non-insctruction cmd 
 * not_instruction_cmd = ( ( "printf" | "error" ), '(', ANY_STRING,
 *                         ')', [ ';' ]
 *                       | "return", [ ';' ] )
 * All this instructions isn't needed for generation of vhdl code
 */
not_instruction_cmd
	:	(PRINTF | ERROR) LPAR ANY_STRING RPAR (SEMI)?
	|	RETURN (SEMI)?
	;

/**
 * Parse a simple number
 * number = NUMBER, [ '.', NUMBER ]
 */
number returns [Double val = null]
	{
		String s = null;
		Boolean minus = false;
	}
	:	n1:NUMBER
		{
			s = new String();
			if (minus) {
				s += "-";
			}
			s += n1.getText();
		}
		(DOT n2:NUMBER {s += "."+n2.getText();}
		)?
		{
			val = Double.parseDouble(s);
		}
	;

/**
 * Define a switch-case operation
 * op_switch = "switch", func_var,
 *             { "case", ( func_var | '[', func_var, { ',', func_var } ),
 *               [ ',' ], body },
 *             [ "otherwise", [ ',' ], body ], "end"
 */
op_switch returns [Switch sw = null] throws ParsingException
	{
		Vector<Element> vec = null;
		Vector<Element> var = null;
		Vector<Variable> cond = null;
		Vector<Element> b = null;
	}
	:	SWITCH {sw = new Switch(); treat.count(sw); }
		vec=func_var {sw.setSwitchVar((Variable)vec.lastElement());}
		(CASE (
				var=func_var
					{
						cond = new Vector<Variable>();
						cond.add((Variable)var.lastElement());
					}
			|	LBRAC var=func_var
					{
						cond = new Vector<Variable>();
						cond.add((Variable)var.lastElement());
					}
				(COMMA var=func_var
					{
						cond.add((Variable)var.lastElement());
					}
					)* RBRAC
			) (COMMA)? b=body {sw.addCase(cond,b);}
		)*
		(OTHERW (COMMA)? b=body {sw.addCase(null,b);})?
		END
	;

/**
 * Define the parameters for, in example, a function
 * param = ident, { ',', ident }
 */
param returns [Vector<Element> par = null] throws ParsingException
	{
		String s;
		Variable var;
	}
	: 	s=ident
		{
			par = new Vector<Element>();
			var = treat.createInOut(s);
			par.add(var);
		}
		(COMMA s=ident
		{
			var = treat.createInOut(s);
			par.add(var);
		}
		)*
	;

/**
 * Describe a pragma
 * pragma = "%m2m", ident, ':', ident
 * Pragma is used for pass some parameters to from the octave code to
 * the vhdl generator
 */
pragma
	{
		String i1,i2;
	}
	:	M2M i1=ident COLON i2=ident
		{
			System.out.println("#Pramga détécté!"+i1+" "+i2);
			treat.addPragma(i1,i2);
		}
	;

/**
 * Intercept the tab index used for access a variable in a vector
 * tab_ind = '(', NUMBER, ')'
 */
tab_ind returns [Integer ind = null]
	:	LPAR n:NUMBER RPAR {ind = Integer.parseInt(n.getText());}
	;


class OctaveLexer extends Lexer;

options {
    k=2;
}

tokens {
	BREAK			= "break";
	CASE			= "case";
	ELSE			= "else";
	ELSEIF			= "elseif";
	END				= "end";
	ENDIF			= "endif";
	ENDFOR			= "endfor";
	ENDWHILE		= "endwhile";
	ENDFUNCTION		= "endfunction";
	ERROR			= "error";
	FOR				= "for";
	FUNCTION		= "function";
	IF				= "if";
	OTHERW			= "otherwise";
	PRINTF			= "printf";
	SWITCH			= "switch";
	THEN			= "then";
	RETURN			= "return";
	WHILE			= "while";
}

protected DIGIT:	'0'..'9';
NUMBER:				(DIGIT)+;
protected CHAR:	'a'..'z'|'A'..'Z';
STRING:				CHAR (DIGIT|CHAR|'_')*;

DOT	  : '.' ;
EQUAL	: '=' ;
LPAR	: '(' ;
RPAR	: ')' ;
SEMI	: ';' ;
COMMA	: ',' ;
PLUS	: '+' ;
DPLUS	: ".+";
MINUS	: '-' ;
DMINUS: ".-";
MUL	  : '*' ;
DMUL	: ".*";
DIV	  : '/' ;
DDIV	: "./";
NOT	  : '!'	;
COLON	: ':' ;
PWR1	: '^'	;
PWR2	: "**";

CEQUAL: "==";
CNOTEQ: "!=";
COR	  : '|'	;
CAND	: '&'	;
CLESS : '<'	;
CGREAT: '>'	;
CLESEQ: "<=";
CGREEQ: ">=";

LBRAK	: '['	;
RBRAK	: ']'	;
LBRAC	: '{' ;
RBRAC	: '}' ;

WS
	:	(' ' 
	|	'\t' 
	|	'\r' '\n' { newline(); } 
	|	'\n'      { newline(); }
	)	{ $setType(Token.SKIP); } 
	;

// Octave comment
COMMENT
	:	'%'
	(	options {
			generateAmbigWarnings=false;
		}
		:	"m2m" {$setType(M2M);}
		|	(~('\n'|'\r'))* ('\n'|'\r'('\n')?)
			{$setType(Token.SKIP); newline();}
	)
	;

// String
ANY_STRING
	:	'"'
	(		/* '\r' '\n' can be matched in one alternative or by matching
				'\r' in one iteration and '\n' in another. I am trying to
				handle any flavor of newline that comes in, but the language
				that allows both "\r\n" and "\r" and "\n" to all be valid
				newline is ambiguous. Consequently, the resulting grammar
				must be ambiguous. I'm shutting this warning off.
			*/
		options {
			generateAmbigWarnings=false;
		}
		:	'\r' '\n' {newline();}
		|	'\r' {newline();}
		|	'\n' {newline();}
		|	~('"'|'\n'|'\r')
	)* '"'
	;

// Multiline comment
ML_COMMENT
	: "/*"
	(		/* '\r' '\n' can be matched in one alternative or by matching
				'\r' in one iteration and '\n' in another. I am trying to
				handle any flavor of newline that comes in, but the language
				that allows both "\r\n" and "\r" and "\n" to all be valid
				newline is ambiguous. Consequently, the resulting grammar
				must be ambiguous. I'm shutting this warning off.
			*/
		options {
			generateAmbigWarnings=false;
		}
		:	{ LA(2)!='/' }? '*'
		|	'\r' '\n' {newline();}
		|	'\r' {newline();}
		|	'\n' {newline();}
		|	~('*'|'\n'|'\r')
	)*
	"*/" {$setType(Token.SKIP);}
	;
