/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 2.7.7 (20060906): "octaveParser.g" -> "OctaveParser.java"$

package m2m.backend.octaveparser;

import m2m.backend.buildingblocks.BuildingBlocksManager;
import m2m.backend.structure.*;
import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;


public interface OctaveParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int EQUAL = 4;
	int SEMI = 5;
	int CEQUAL = 6;
	int CNOTEQ = 7;
	int CLESS = 8;
	int CGREAT = 9;
	int CLESEQ = 10;
	int CGREEQ = 11;
	int PLUS = 12;
	int DPLUS = 13;
	int MINUS = 14;
	int DMINUS = 15;
	int MUL = 16;
	int DMUL = 17;
	int DIV = 18;
	int DDIV = 19;
	int PWR1 = 20;
	int PWR2 = 21;
	int LPAR = 22;
	int RPAR = 23;
	int COMMA = 24;
	int LBRAK = 25;
	int RBRAK = 26;
	int FUNCTION = 27;
	int END = 28;
	int ENDFUNCTION = 29;
	int IF = 30;
	int ENDIF = 31;
	int THEN = 32;
	int ELSEIF = 33;
	int ELSE = 34;
	int STRING = 35;
	int NOT = 36;
	int COR = 37;
	int CAND = 38;
	int ENDFOR = 39;
	int FOR = 40;
	int COLON = 41;
	int ENDWHILE = 42;
	int WHILE = 43;
	int PRINTF = 44;
	int ERROR = 45;
	int ANY_STRING = 46;
	int RETURN = 47;
	int NUMBER = 48;
	int DOT = 49;
	int SWITCH = 50;
	int CASE = 51;
	int LBRAC = 52;
	int RBRAC = 53;
	int OTHERW = 54;
	int M2M = 55;
	int BREAK = 56;
	int DIGIT = 57;
	int CHAR = 58;
	int WS = 59;
	int COMMENT = 60;
	int ML_COMMENT = 61;
}
