/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @brief Value of the octave parser hashMap variable.
 *
 * Project:  Math2Mat
 *
 * @file ReadWriteIndex.java
 * @author Daniel Molla
 *
 * @version 1.0
 *
 * @date: 04.09.2010
 *
 * Author: Daniel Molla
 *
 * Description: This class is use to define the value of the octave parser hashMap variable.
 *
 */
package m2m.backend.octaveparser;

import java.util.Vector;

public class ReadWriteIndex
{
	/**
	 * Index use for the next variable creation.
	 */
	private int writeIndex;
	/**
	 * Vector indexes use for the next read access of a variable.
	 * Each level represent an imbrication of body level.
	 */
	private Vector<Integer>  readIndex;
	
	private int diff;
	
	
    /**
     * Default contructor of a ReadWriteIndex.
     */
	public ReadWriteIndex() 
	{
		writeIndex = 1;
		readIndex = new Vector<Integer>();
		diff = 0;
	}
	
    /**
     * Default constructor of a ReadWriteIndex.
     */
	public ReadWriteIndex(ReadWriteIndex rwi) 
	{
		writeIndex = rwi.writeIndex;
		readIndex = new Vector<Integer>(rwi.readIndex);
		diff = 0;
	}
	
	/**
	 * Get the next ReadWriteIndex for a new variable.
	 * @return the next ReadWriteIndex for a new variable
	 */
	public ReadWriteIndex getNextVariable() {
		readIndex.setElementAt(writeIndex, readIndex.size()-1);
		writeIndex++;
		return this;
	}
	
	
	/**
	 * Get the previous ReadWriteIndex for a new variable.
	 * @return the next ReadWriteIndex for a new variable
	 */
	public ReadWriteIndex getPreviousVariable() {
		writeIndex--;
		readIndex.setElementAt(writeIndex-1, readIndex.size()-1);
		return this;
	}
	
	
	/**
	 * Add an index representing the last body imbrication at the end of the vector.
	 */
	public void addReadIndex() 
	{
		if(readIndex.size() == 0)
			readIndex.add(0);
		else
			readIndex.add(readIndex.lastElement());
	}
	
	
	/**
	 * Remove the index of the last body imbrication.
	 * @param the number of index to remove
	 */
	public void removeReadIndex() 
	{
		if(readIndex.size() > 1)
			readIndex.remove(readIndex.size()-1);
	}

	
	/**
	 * Get the next index for a read acces of a variable.
	 * @return the next index for a read acces of a variable
	 */
	public int getReadIndex()
	{
		if(readIndex.size() == 0)
			readIndex.add(0);
		return readIndex.lastElement();
	}
	
	
	/**
	 * Get the size of the read index acces of a variable.
	 * @return the size of the read index acces of a variable
	 */
	public int getReadIndexSize()
	{
		return readIndex.size();
	}
	
	
	/**
	 * Get the next index for a read acces of a variable.
	 * @return the next index for a read acces of a variable
	 */
	public int getWriteIndex()
	{
		return writeIndex;
	}
	
	
	public void setDiff(int value){
		diff = value;
	}
	
	
	public int getDiff() {
		return diff;
	}
}	