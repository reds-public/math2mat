/*
    This file is part of the Math2mat project.	
    Copyright (C) 2011  HES-SO (HEIG-VD, HES-SO//VS, EIA-FR, hepia, HE-Arc)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *	ParsingException
 *
 *	Gregory Trolliet
 * 30 oct. 2009
 */

package m2m.backend.octaveparser;


public class ParsingException extends Exception {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -1186651222955286571L;
	
	public ParsingException(String error) {
		super(error);
	}
	public ParsingException(String error, Throwable cause) {
		super(error, cause);
	}
	public ParsingException(Throwable cause) {
		super(cause);
	}

}
